import cook = require('./cook');
import path_ext = require("./path_ext");
import path = require('path');
import fs = require('fs');

// shader tool

function get_unique_includes(source:string, otherIncludes:string[]){
    let ret:string[] = [];
    let incIdx = source.indexOf("#pragma bolts include");
    while (incIdx > -1){
        let eol  = source.indexOf('\n', incIdx);
        let begin = source.indexOf('\"', incIdx);
        let end = source.lastIndexOf('\"', eol);
        if (begin && end && end > begin) {
            let filename = source.slice(begin + 1, end) + '.glh';
            if (!(otherIncludes.indexOf(filename) !== -1) && !(ret.indexOf(filename) !== -1)){
                ret.push(filename);
            }
        }
        incIdx = source.indexOf("#pragma bolts include", eol);
    }
    return ret;
}

class ShaderTool implements cook.ITool
{
    constructor() {
        this.name = "shaders";
        this.implicitExtensions = [".glsl"];
        this.inputExtensions = [".glsl",".glh"];
    }

    init(settings: cook.Settings) {}

    prepare( filename:string, config:string, cooker:cook.Cooker){
        let job = new cook.BuildJob(this.name, config);
        job.inputs.push(filename);
        let includes = job.inputs;
        let i = 0;
        while (i < includes.length) {
            let shaderContents = fs.readFileSync( cooker.vfs.getAbsPath( includes[i] ), "ascii");
            let others = get_unique_includes(shaderContents, includes);
            includes.push(...others);
            i++;
        }

        job.outputs.push( path_ext.trimExt(filename) + ".gp");
        cooker.register_job(job);
    }

    build( job:cook.BuildJob, cooker:cook.Cooker){
        let shaderSource = ""
        for (let i = 0; i < job.inputs.length; i++) {
            const filename = job.inputs[i];
            let newContent = fs.readFileSync( cooker.vfs.getAbsPath(filename), "ascii");
            let includeText = "#pragma bolts include \"" + filename.slice(0,-4) + "\"";
            let includeStart = shaderSource.indexOf(includeText);
            if(includeStart > -1) {
                let srcEnd = shaderSource.slice(includeStart + includeText.length);
                shaderSource = shaderSource.slice(0, includeStart) + newContent + srcEnd;
            }
            else
                shaderSource = newContent;
        }

        fs.writeFileSync( job.outputAbsPaths[0], shaderSource);
        return cook.BuildResultCode.OK;
    }

    name:string;
    implicitExtensions:string[];
    inputExtensions: string[];
}

//register tool
cook.toolRegistry.push( new ShaderTool());