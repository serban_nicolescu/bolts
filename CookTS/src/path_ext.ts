import path = require("path")

export function trimExt(abspath:string){
    let dotIdx = abspath.lastIndexOf('.');
    if (dotIdx > -1)
        return abspath.substring(0, dotIdx)
    else
        return abspath;
}

export function makeAbsPath( pth: string, rel:string):string {
    if (path.isAbsolute(pth))
        return path.normalize(pth)
    else
        return path.normalize( path.join( rel, pth ) )
}