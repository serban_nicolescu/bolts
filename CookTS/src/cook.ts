
import { crc32 } from 'crc';
import { VFS, pathPair } from "./VFS";
export { VFS, pathPair };
import fs = require('fs');
import path = require('path');
import path_ext = require('./path_ext');
import events = require('events');
import sqlite = require('sqlite3');
import { performance } from 'perf_hooks';

// Config Files

export function makeFolderSync(pth:string) {
    if (!fs.existsSync(pth))
        fs.mkdirSync(pth)
}

export interface ProjectConfig 
{
    CACHE_FOLDER_PATH: string;
    ASSET_FOLDERS: string[];
    TOOLS_FOLDER:string;
}

export class Settings 
{
    constructor( cfgPath:string, prj : ProjectConfig){
        this.CACHE_FOLDER_PATH =  path_ext.makeAbsPath( prj.CACHE_FOLDER_PATH, cfgPath)
        makeFolderSync(this.CACHE_FOLDER_PATH);
        this.TOOLS_FOLDER = path_ext.makeAbsPath( prj.TOOLS_FOLDER, cfgPath)
        this.TEMP_PATH = path.join( this.CACHE_FOLDER_PATH, "temp");
        makeFolderSync(this.TEMP_PATH);
        this.ASSET_FOLDERS = prj.ASSET_FOLDERS.map( function (pth:string) {
            return path_ext.makeAbsPath(pth, cfgPath);
        } );
        //TODO: Ensure paths exist
    }

    CACHE_FOLDER_PATH: string;
	ASSET_FOLDERS: string[];
    TEMP_PATH:string;
    TOOLS_FOLDER:string;
}

export class BuildConfig 
{
    ignoreCache:boolean = false;
    OUT_PATH:string;
}

export class BuildStats
{
    cacheHits:number = 0;
    preparedJobs:number = 0;
    builtJobs:number = 0;
}

// Event emitter

export enum BuildEvents {
    PREPARE_START = "prep_start",
    PREPARE_END = "prep_end",
    BUILD_START = "build_start",
    BUILD_END = "build_end"
};
export let buildEvents = new events.EventEmitter();

///

function set_and_push<T>(map: Map<string, T[]>, key:string, val:T){
    var entry = map.get(key);
    if (!entry){
        entry = [];
        map.set(key, entry);
    }
    entry.push(val);
}

export class BuildCache
{
    constructor(vfs:VFS, dbFilepath:string){
        this.vfs = vfs;
        this.dbPath = dbFilepath;
    }

    async openWriteAsync() {
        if (this.cacheDB)
            await this.closeAsync();
        return new Promise( (resolve, reject) => {
            console.log(`Connecting to cache DB at ${this.dbPath}`);
            this.cacheDB = new sqlite.Database(this.dbPath, (err) => {
                if (err) {
                    console.error(`\tError opening build cache DB ${err.message} \n\t ${this.dbPath}`);
                    reject(err);
                }
                this.cacheDB.serialize();
                this.cacheDB.run(`CREATE TABLE IF NOT EXISTS OutputCacheV1 (
                    outputFilename TEXT NOT NULL,
                    inputCRC TEXT NOT NULL,
                    outputCRC TEXT NOT NULL,
                    PRIMARY KEY ( outputFilename, inputCRC));`,
                    err => {
                        if (err){
                            console.error(`Error initialising build cache DB ${err.message} \n\t ${this.dbPath}`);
                            reject(err);
                        }
                        else {
                            resolve();
                            console.log(`\tDB Connected`);
                        }
                    });
            });
        });
    }

    closeAsync() {
        return new Promise( (resolve, reject) => {
            if (this.cacheDB)
                this.cacheDB.close((err) => {
                    if (err) reject(err);
                    console.log("\tDB Disconnected");
                    resolve();
                });
            this.cacheDB = null;
            resolve();
        });
    }

    get_file_crc(filepath:string):number {
        let fstats = fs.statSync(filepath);
        let cacheName = filepath + fstats.mtimeMs;
        let crc = this.crcCache.get(cacheName);
        if (crc !== undefined)
            return crc;
        const fileContents = fs.readFileSync( filepath);
        const fileCrc = crc32(fileContents);
        this.crcCache.set(cacheName, fileCrc);
        return fileCrc;
    }

    private dbGetAsync(sql:string, params: any): Promise<any>
    {
        return new Promise( (resolve, reject) => {
            this.cacheDB.get(sql, params, (err, result) => {
                if (err) reject(err);
                resolve(result);
            });
        });
    }

    private dbRunAsync(sql:string, params: any) : Promise<void>
    {
        return new Promise( (resolve, reject) => {
            this.cacheDB.get(sql, params, (err, result) => {
                if (err) reject(err);
                resolve();
            });
        });
    }

    async is_job_cached(job:BuildJob): Promise<boolean> {
        // first, check if all output files exist on disk
        for (let idx = 0; idx < job.outputAbsPaths.length; idx++) {
            if (!fs.existsSync( job.outputAbsPaths[idx] ))
                return false;// output missing on disk. job needs rebuilding
        }
        // check DB for output CRCs
        let sqlGetOutputCRC = `SELECT outputCRC FROM OutputCacheV1 WHERE outputFilename=? AND inputCRC=?`;
        // check output files to see if they match desired crcs
        for (let idx = 0; idx < job.outputs.length; idx++) {
            let crcResult = await this.dbGetAsync(sqlGetOutputCRC, [ job.outputs[idx], job.inputsCrc ]);
            if (!crcResult)
                return false;
            const targetOutputCRC = parseInt(crcResult.outputCRC);
            const currentOutputCRC = this.get_file_crc( job.outputAbsPaths[idx] );
            if (targetOutputCRC != currentOutputCRC)
                return false; // mismatch found between file on disk and desired output. job needs to be rebuilt
        }
        // if all crcs matched, job is DONE
        return true;
    }

    async add_to_cache(job:BuildJob) {
        let sqlCacheJobResult = `INSERT OR REPLACE INTO OutputCacheV1 VALUES (?,?,?)`;

        for (let idx = 0; idx < job.outputs.length; idx++) {
            const outputFilename = job.outputs[idx];
            let outputCRC = this.get_file_crc( job.outputAbsPaths[idx]);
            await this.dbRunAsync(sqlCacheJobResult, [outputFilename, job.inputsCrc, outputCRC]);
        }
        /*
        let set = this.outputCache.get(job.toolName)
        if (set === undefined){
            set = new Set;
            this.outputCache.set(job.toolName, set);
        }
        set.add(job.inputsCrc);
        */
    }

    vfs:VFS;
    //outputCache:Map<string, Set<number>> = new Map;
    crcCache:Map<string, number> = new Map;
    cacheDB:sqlite.Database;
    dbPath:string;
}

export class Cooker 
{
    constructor(vfs:VFS, settings:Settings) {
        this.vfs = vfs;
        this.settings = settings;
        toolRegistry.forEach(tool => {
            tool.init(settings);
            this.tools.set( tool.name, tool);

            tool.implicitExtensions.forEach(ext => {
                set_and_push(this.implicits, ext, tool);
            });
            tool.inputExtensions.forEach(ext => {
                set_and_push(this.extensions, ext, tool);
            });
        });       
    }

    prepare_implicit_assets( cfg: BuildConfig, buildCache:BuildCache) {
        buildEvents.emit(BuildEvents.PREPARE_START);
        // call appropriate implicit tools for every asset found
        this.vfs.files.forEach( (absPath,filename) => {
            let ext = path.extname(filename);
            let tools = this.implicits.get(ext.toLowerCase());
            if (tools){
                for (let i = 0; i < tools.length; i++) {
                    const tool = tools[i];
                    tool.prepare( filename, "", this);
                }
            }
        });
        // initialise job fields
        this.jobs.forEach( (job) => {
            // work out input CRC
            let inputCrc = job.configCrc;
            for (let i = 0; i < job.inputs.length; i++) {
                const filename = job.inputs[i];
                const filepath = this.vfs.getAbsPath(filename);
                if (filepath)
                    inputCrc = inputCrc ^ buildCache.get_file_crc(filepath);
            }
            job.inputsCrc = inputCrc;
            // set job output paths
            for (let outIdx = 0; outIdx < job.outputs.length; outIdx++) {
                const outFilename = job.outputs[outIdx];
                job.outputAbsPaths.push( path.join( cfg.OUT_PATH, job.toolName, outFilename) );
            }
        });

        buildEvents.emit(BuildEvents.PREPARE_END);
    }
    
    ///> Called by tools when preparing jobs
    register_job(job:BuildJob) {
        this.jobs.push(job);
    }

    async start_build(cfg: BuildConfig, cache:BuildCache) {
        buildEvents.emit(BuildEvents.BUILD_START);

        makeFolderSync( cfg.OUT_PATH);
        this.tools.forEach( (tool:ITool, name:string) => {
            makeFolderSync( path.join(cfg.OUT_PATH, name));
        })

        this.stats.preparedJobs = this.jobs.length;
        for (let jobI = 0; jobI < this.jobs.length; jobI++) {
            const job = this.jobs[jobI];
            // check cache
            if (!cfg.ignoreCache && await cache.is_job_cached(job)) {
                this.stats.cacheHits++;
                continue;
            }
            var msStart = performance.now();
            // build
            const buildResult = this.tools.get(job.toolName).build(job, this);
            this.stats.builtJobs++;
            var durationSec = ((performance.now() - msStart) / 1000).toFixed(2);
            // cache it
            if (buildResult == BuildResultCode.OK)
                await cache.add_to_cache(job);
            console.log("Finished %ds\t:%s", durationSec, job.inputs[0]);
        }

        buildEvents.emit(BuildEvents.BUILD_END, this);
    }

    //
    vfs:VFS;
    settings:Settings;
    //
    stats:BuildStats = new BuildStats;
    tools:Map<string, ITool> = new Map;
    implicits:Map<string, ITool[]> = new Map;
    extensions:Map<string, ITool[]> = new Map; // Maps extension strings to interested tools. ".shd" -> ["shader"]
    //
    jobs:BuildJob[] = [];
}

export class BuildJob
{
    constructor(toolName:string, cfgJson:string){
        this.config = cfgJson;
        this.configCrc = crc32(cfgJson);
        this.toolName = toolName;
    }

    config:string;
    configCrc:number = 0;
    toolName:string;
    inputs: string[] = [];
    inputsCrc: number = 0;
    outputs: string[] = [];
    outputAbsPaths:string[] = [];
}

export enum BuildResultCode {
    OK,
    ERROR
}

export interface ITool
{
    init(settings:Settings):void;
    prepare( filename:string, config:string, cooker:Cooker):void;
    build( job:BuildJob, cooker:Cooker):BuildResultCode;

    name:string;// tool name. used for caching
    implicitExtensions:string[]; // files with these extensions spawn prepare jobs automatically for this tool
    inputExtensions: string[]; // tool will only interact with files that have these extensions. Used for dependency & parallelism tracking

}

export let toolRegistry:ITool[] = [];
