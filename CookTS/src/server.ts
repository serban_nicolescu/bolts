//https://www.npmjs.com/package/tssqlite


//import sqlite = require('tssqlite')
import fs = require('fs')
import path = require('path');
import process = require('process')
import notifier = require("node-notifier") //https://github.com/mikaelbr/node-notifier
import SysTray, { ClickEvent } from 'systray'

import path_ext = require('./path_ext');
import * as cook from "./cook"

import "./tool_shaders";
import "./tool_meshes";

// parse commandline options

//https://www.npmjs.com/package/command-line-args
const optionDefinitions = [
    { name: 'config', alias: 'c', type: String },
    { name: 'output', alias: 'o', type: String }, // Relative to config file
    { name: 'watch', alias: 'w', type: Boolean, defaultOption: false },
    //{ name: 'src', type: String, multiple: true, defaultOption: true },
    //{ name: 'timeout', alias: 't', type: Number }
]
const commandLineArgs = require('command-line-args')
const options = commandLineArgs(optionDefinitions)

// setup server UI

let cookIcon = fs.readFileSync( path.join(__dirname, "../icons/hat_green.ico"), 'base64');
let wallIcon = fs.readFileSync( path.join(__dirname, "../icons/wall.ico"), 'base64');

//read config

let cfgPath = path_ext.makeAbsPath(options.config, process.cwd());
let prjContent = fs.readFileSync(cfgPath).toString();
let prj: cook.ProjectConfig = JSON.parse(prjContent);
let settings = new cook.Settings(path.dirname(cfgPath), prj)
let config = new cook.BuildConfig()
config.OUT_PATH = path_ext.makeAbsPath(options.output, path.win32.dirname(cfgPath));

let vfs = new cook.VFS()
let cache = new cook.BuildCache(vfs, path.join( settings.CACHE_FOLDER_PATH, "_BuildDB_V1.sqlite"));

function scan_asset_folders() {
    vfs.clear();
    vfs.scanFolder(settings.ASSET_FOLDERS[0], undefined)
    vfs.reportDuplicates();
}

async function build_all()
{
    await cache.openWriteAsync();
    let cooker = new cook.Cooker(vfs, settings);
    cooker.prepare_implicit_assets(config,cache);
    await cooker.start_build(config,cache);
    await cache.closeAsync();
}

//server hooks
cook.buildEvents.on( cook.BuildEvents.PREPARE_START, () => {
    console.log("Build started");
    console.time("Build done");
});
cook.buildEvents.on( cook.BuildEvents.BUILD_END, (cooker:cook.Cooker) => {
    console.timeEnd("Build done");
    let msg = `Built: ${cooker.stats.builtJobs} (${cooker.stats.cacheHits} cached) / ${cooker.stats.preparedJobs}`;
    console.log(msg);
    // systray notification
    notifier.notify( {
        message: msg,
        title:"Cook",
        sound:false,
        wait:false
    });
});

// Do an initial build
scan_asset_folders();
build_all();

//watch for changes
if (options.watch) {
    //setup tray icon
    let tray = new SysTray({
        menu: {
            // you should using .png icon in macOS/Linux, but .ico format in windows
            icon: cookIcon,
            title: "Cook",
            tooltip: "Cook",
            items: [{
                title: "Rebuild All",
                tooltip: "Build everything, ignoring cache",
                checked: false,
                enabled: true
            },{
                title: "Exit",
                tooltip: "bla",
                checked: false,
                enabled: true
            }]
        },
        debug: false,
        copyDir: true, // copy go tray binary to outside directory, useful for packing tool like pkg.
    })
    tray.onClick( (action:ClickEvent) => {
        if ( action.item.title == 'Exit'){
            tray.kill();
        }else if ( action.item.title == 'Rebuild All'){
            config.ignoreCache = true;
            build_all();
            config.ignoreCache = false;
        }
    });
    //
    let buildTriggered = false;
    //https://nodejs.org/docs/latest/api/fs.html#fs_fs_watch_filename_options_listener
    let watcher = fs.watch(settings.ASSET_FOLDERS[0], { recursive: true }, (event: string, filename: string) => {
        if (path.extname(filename) == '') // Ignore folders or files without an extension
            return;
        let absPath = path.join(settings.ASSET_FOLDERS[0], filename);
        if (event == 'change') {
            console.log("File changed: " + filename);
        }
        if (event == 'rename') {
            // File added or removed, need folder rescan
            scan_asset_folders();
            if (fs.existsSync(absPath))
                console.log("File added: " + filename);
            else
                console.log("File removed: " + filename);
        }
        // trigger build if not already queued
        if (!buildTriggered) {
            buildTriggered = true;
            setTimeout( () => { 
                build_all();
                buildTriggered = false;
            }, 200);
        }
    });
}