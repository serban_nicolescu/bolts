import fs = require('fs');
import path = require('path');

export type pathPair = ReadonlyArray<string>; // Path pairs are sets of 2 path strings, the file's virtual name & its abs path

export class VFS 
{
    public scanFolder(pth:string, extensions:Set<string>){
        let files:string[] = fs.readdirSync(pth, "utf8");
        for (let i = 0; i < files.length; i++) {
            const entry = files[i];
            let fullpath = path.join(pth, entry);
            if (fs.statSync(fullpath).isDirectory()) {
                this.scanFolder(fullpath, extensions);
            }else{
                //console.log(fullpath)
                if (this.files.has(entry)){
                    let dups = this.duplicateFiles.get(entry)
                    if (dups === undefined) {
                        dups = [];
                        this.duplicateFiles.set(entry, dups);
                    }
                    dups.push(fullpath);
                }else{
                    this.files.set( entry, fullpath);
                }
            }
        }
    }

    public clear() {
        this.duplicateFiles.clear();
        this.files.clear();
    }

    public reportDuplicates() {
        this.duplicateFiles.forEach( (value:string[], key:string) => {
            console.error("Duplicate filename found: ", key);
            console.group();
            console.error("Path: ", this.files.get(key));
            this.files.delete(key);
            value.forEach( (fullpath:string)=> {
                console.error("Path: ", fullpath);
            });
            console.groupEnd();
        } );
    }

    public getAbsPath(filename:string) {
        return this.files.get(filename);
    }

    public hideFile(filename:string) {
        this.files.delete(filename);
    }

    duplicateFiles: Map<string, Array<string>> = new Map;
    files: Map<string, string> = new Map;
}