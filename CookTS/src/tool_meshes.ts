import cook = require('./cook');
import path_ext = require("./path_ext");
import path = require('path');
import fs = require('fs');
import child_process = require('child_process')

class MeshTool implements cook.ITool
{
    constructor() {
        this.name = "mesh";
        this.implicitExtensions = [".obj", ".dae", ".fbx"];
        this.inputExtensions = this.implicitExtensions.slice();
        this.inputExtensions.push(".json");
    }

    init(settings: cook.Settings) {
        this.meshToolPath = path.join(settings.TOOLS_FOLDER, "MeshConverter", "MeshConverter.exe");
    }

    prepare( filename:string, config:string, cooker:cook.Cooker){
        let job = new cook.BuildJob(this.name, config);
        job.inputs.push(filename);
        if (cooker.vfs.getAbsPath(filename + ".json") !== undefined)
            job.inputs.push(filename + ".json");
        job.outputs.push( path_ext.trimExt(filename) + ".mesh");
        cooker.register_job(job);
    }

    build( job:cook.BuildJob, cooker:cook.Cooker){
        try {
            if (job.inputs.length == 1)
                child_process.execFileSync( this.meshToolPath, [ cooker.vfs.getAbsPath( job.inputs[0]), job.outputAbsPaths[0] ]);
            else
                child_process.execFileSync( this.meshToolPath, [ cooker.vfs.getAbsPath( job.inputs[0]), job.outputAbsPaths[0], "-o", cooker.vfs.getAbsPath( job.inputs[1])]);
            return cook.BuildResultCode.OK;
        } 
        catch (error) {
            //TODO: Report error
            error.status;  // Might be 127 in your example.
            error.message; // Holds the message you typically want.
            error.stderr;  // Holds the stderr output. Use `.toString()`.
            error.stdout;  // Holds the stdout output. Use `.toString()`.
            return cook.BuildResultCode.ERROR;
        }
    }

    name:string;
    implicitExtensions:string[];
    inputExtensions: string[];
    meshToolPath:string;
}

//register tool
cook.toolRegistry.push( new MeshTool());