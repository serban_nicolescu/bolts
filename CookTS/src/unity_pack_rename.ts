import fs = require('fs')
import path = require('path');
import process = require('process')

function ensureDirectoryExistence(filePath:string) {
    var dirname = path.dirname(filePath);
    if (fs.existsSync(dirname)) {
      return;
    }
    ensureDirectoryExistence(dirname);
    fs.mkdirSync(dirname);
  }

// Abs folder path of double unzipped unitypackage
let pack = process.argv[2];


let assetDir = path.join(pack, '_Assets')
if (!fs.existsSync(assetDir))
    fs.mkdirSync( assetDir);

let files:string[] = fs.readdirSync(pack, "utf8");
for (let i = 0; i < files.length; i++) {
    const entry = files[i];
    if (entry == "_Assets")
        continue;
    let dirpath = path.join(pack, entry);
    if (fs.statSync(dirpath).isDirectory()){
        let pathfile = fs.readFileSync( path.join(dirpath, 'pathname')).toString();
        let assetpath = pathfile.slice(0, pathfile.indexOf('\n'));
        assetpath = path.join(assetDir, assetpath.slice( assetpath.indexOf('/',assetpath.indexOf('/',assetpath.indexOf('/') + 1)+1)+1));
        if (fs.existsSync(path.join(dirpath, 'asset'))) { 
            ensureDirectoryExistence(assetpath);
            fs.copyFileSync(path.join(dirpath, 'asset'), assetpath);
        }
    }
}