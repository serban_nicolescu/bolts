uniform sampler2D u_color1;

in vec2 v_uv;
out vec4 outColor;

void main()
{
	float time = 0.1;
	vec2 uv = v_uv + 0.005*vec2( sin(time+1024.0*v_uv.x),cos(time+768.0*v_uv.y));
	outColor = texture(u_color1, uv);	
}