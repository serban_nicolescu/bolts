uniform vec2 	u_offset;
uniform float 	u_scale;

in vec3 a_posL;
in vec2 a_uv0;
out vec2 v_uv;

void main()
{
	gl_Position = vec4((a_posL*u_scale) + vec3(u_offset,0.0) , 1.0);
	v_uv = a_uv0;
}
