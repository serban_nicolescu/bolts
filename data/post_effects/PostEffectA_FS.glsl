uniform sampler2D u_color1;

in vec2 v_uv;
out vec4 outColor;

void main()
{
	outColor = vec4( vec3(texture(u_color1, v_uv).a), 1.0);	
}