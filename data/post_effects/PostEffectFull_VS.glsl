uniform mat4 u_wvp;

in vec3 a_posL;
out vec2 v_uv;

void main()
{
	gl_Position = vec4(a_posL , 1.0);
	v_uv = a_posL.xy*0.5 + 0.5;
}
