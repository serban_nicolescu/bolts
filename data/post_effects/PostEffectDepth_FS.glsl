uniform sampler2D 	u_depth;
uniform vec2		u_cameraPlanes; // x is near plane dist, y is far plane dist

in vec2 v_uv;
out vec4 outColor;

void main()
{
	float farPlaneDist = u_cameraPlanes.y;
	float nearPlaneDist = u_cameraPlanes.x;
	float linearZ = texture(u_depth, v_uv).z;//(2 * nearPlaneDist) / (farPlaneDist + nearPlaneDist - texture(u_depth, v_uv).z * (farPlaneDist - nearPlaneDist));
	outColor = vec4( vec3(linearZ), 1.0);	
}