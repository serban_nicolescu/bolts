
smooth in vec3 v_normalV;
smooth in vec3 v_normalW;
smooth in vec3 v_eye;


uniform vec4 u_color;

out vec4 outColor;

void main()
{
	vec3 normalV = normalize(v_normalV);
	outColor = vec4(u_color.rgb, u_color.a * normalV.z * normalV.z * normalV.z);
}