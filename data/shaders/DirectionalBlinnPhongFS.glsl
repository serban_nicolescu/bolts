
uniform vec3 			u_color;
uniform float 			u_gloss = 0.3;

#if defined(ALBEDO_MAP) || defined(NORMAL_MAP) || defined(ROUGHNESS_MAP) || defined(METALNESS_MAP)
	in vec2 v_uv;
#endif

#if defined(ROUGHNESS_MAP)
	uniform sampler2D	u_roughnessMap;
#endif

#if defined(METALNESS_MAP)
	uniform sampler2D	u_metalnessMap;
#else
	uniform float	u_metalness;
#endif

#if defined(ALBEDO_MAP)
	uniform sampler2D	u_albedoMap;
#endif

#pragma bolts include "blinn_phong_common_fs"

out vec4 outColor;

void main()
{
#if defined(ALPHA_TEST)
	float alpha;
#endif

	float gloss;
#ifdef ROUGHNESS_MAP
	gloss = 1.0 - (texture(u_roughnessMap, v_uv).r * 1.8);
#else
	gloss = u_gloss;
#endif

	vec3 albedo;
#ifdef ALBEDO_MAP
	
	vec4 albedoMap = texture(u_albedoMap, v_uv);
	albedo = albedoMap.rgb;
	#if defined(ALPHA_TEST)
		alpha = albedoMap.w;
	#endif
	//albedo = pow(albedo, vec3(1 / 2.2));
#else
	albedo = u_color;
#endif

	#if defined(ALPHA_TEST)
		if (alpha < 0.9)
			discard;
	#endif

	float metalness = 0.0;
#if defined(METALNESS_MAP)
	metalness = texture(u_metalnessMap, v_uv).r;
#else
	metalness = u_metalness;
#endif

	vec3 normalW = normalize(v_normalW);
	vec3 viewW;
	getCommonLightingVars( normalW, viewW);

	vec3 ambientDiffuse = ambientDiffuse(normalW);
	vec3 ambientSpecular = vec3(0.0);
#ifdef INDIRECT_SPECULAR
	vec3 f0 = mix( vec3(0.04), albedo, metalness);
	ambientSpecular += indirectSpecular( normalW, viewW, f0, gloss);
	albedo *= 1.0 - metalness;
#endif
	vec3 totalLight = ambientDiffuse;

	//g_dynamicLightCount
	for (int i = 0; i < g_dynamicLightCount; i++) {
		vec3 lightDir = g_dynamicLightPosR[i].xyz - v_positionW;
		float distToLight = length(lightDir);
		lightDir /= distToLight;
		float normDist = distToLight / g_dynamicLightPosR[i].w;
		float atten = clamp( 1.0 - normDist*normDist, 0.0, 1.0);
		totalLight += atten * evalPhongLight(normalW, viewW, lightDir, gloss) * g_dynamicLightColor[i].rgb;
	}

	float directionalShadowTerm = 1;
#ifdef SHADOWS
	directionalShadowTerm = getDirectionalShadowTerm();
#endif
	float directionalLightIntensity = directionalShadowTerm * evalPhongLight(normalW, viewW, g_directionalLightDir.xyz, gloss);
	totalLight += directionalLightIntensity * g_directionalLightColor.rgb;
	outColor = vec4( albedo * totalLight, 1.0);
	outColor.rgb += ambientSpecular;
	 
	//outColor.rgb = normalW.rgb;
	//outColor.rgb = albedo.rgb; 
	//outColor.rgb = vec3(shadowTerm);
	//outColor.g = 0.0;
	// Gamma correct
	//TODO: This doesn't belong here
	//outColor.rgb = ACESFilm(outColor.rgb);
	outColor = pow( outColor, vec4(1.0 / 2.2));
	
	//outColor = vec4( totalLight, 1.0);

	//#ifdef NORMAL_MAPPED
	//outColor = vec4(normalW, 1.0);
	//#endif
}
