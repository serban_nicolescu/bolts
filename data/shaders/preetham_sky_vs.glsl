in vec3 a_posL;

uniform mat4 u_w;
uniform mat4 u_v;
uniform mat4 u_p;
 
out vec3 worldSpacePosition;

void main(void)
{
	gl_Position = u_p * u_v * u_w * vec4(a_posL, 1.0);
	worldSpacePosition = a_posL;
}

