
#ifndef SHOW_NORMAL
in vec4 v_color;
#else
in vec3 v_normalW;
#endif

out vec4 outColor;

void main()
{
#ifndef SHOW_NORMAL
	outColor = v_color; 
#else
	vec3 norm = normalize(v_normalW); 
	outColor = vec4(norm, 1.0);
#endif
}