uniform samplerCube u_skybox;
uniform float		u_mip;

smooth in vec3 v_normalW;

out vec4 outColor;

void main()
{
	vec3 normalW = normalize(v_normalW);
	outColor = textureLod( u_skybox, normalW, u_mip);
}