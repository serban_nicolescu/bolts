
uniform sampler2D 	u_texAlpha;
uniform float		u_threshold;

in vec2 v_uv;

out vec4 outColor;

void main()
{
	vec4 c = texture(u_texAlpha, v_uv);
	if (c.r < u_threshold)
		discard;
	outColor = c;//vec4(abs(v_uv), 0,1);
}