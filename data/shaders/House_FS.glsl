uniform sampler2D u_diffuse;
uniform sampler2D u_lightmap;
uniform sampler2D u_specular;
uniform sampler2D u_normal;

in vec2 v_uv;

out vec4 outColor;

void main()
{
	//outColor = vec4( v_uv, 0.0, 1.0);
	//outColor = texture(u_texColor, vec2(0.25, 0.25));
	vec2 uv = v_uv;
	vec4 lightmap = texture(u_lightmap, uv);
#ifdef LIGHTMAP_ONLY
	outColor = lightmap;
#else
	uv.y = 1.0 - uv.y;
	//outColor = texture(u_diffuse, uv);
	outColor = texture(u_diffuse, uv) * lightmap;
#endif
}