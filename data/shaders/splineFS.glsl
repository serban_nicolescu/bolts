
uniform vec3 u_color;

in vec2 v_uv;

out vec4 outColor;

void main()
{
	outColor = vec4( vec3( v_uv.x*v_uv.x - v_uv.y > 0 ? 1.0 : 0.0), 1.0);
	//outColor = vec4( 1.0);
}