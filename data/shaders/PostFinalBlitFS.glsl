in vec2 v_uv;

uniform vec4 		u_outputResolution; // xy width height, zw rcp width height (i.e. texel size)
uniform sampler2D 	u_scene;
uniform sampler2D 	u_bloom;

out vec3 outColor;

void main()
{
	vec3 c = texture(u_scene, v_uv).rgb;
#ifdef BLOOM
	c += texture(u_bloom, v_uv).rgb * 1.0;
#endif
	
	outColor = c;
}