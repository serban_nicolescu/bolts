

uniform vec4 u_rtResolution; // xy resolution | zw inv res
uniform sampler2D u_irr;

out vec4 outColor;

void main()
{
	vec2 pixel = u_rtResolution.zw;
	vec2 uv = gl_FragCoord.xy * pixel;
	vec3 color = texture(u_irr, uv).rgb;
	outColor = vec4( color , (1.0 / 16));
}