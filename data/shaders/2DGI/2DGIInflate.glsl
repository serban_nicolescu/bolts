

uniform vec4 u_rtResolution; // xy resolution | zw inv res
uniform sampler2D u_walls;
uniform sampler2D u_irr;

out vec4 outColor;

void main()
{
	vec2 pixel = u_rtResolution.zw;
	vec2 uv = (gl_FragCoord.xy - vec2(0.5))* pixel;
	bool isWall = textureLod(u_walls, uv, 0.0).r > 0.0;
	if (!isWall)
		discard;
		
	vec3 maxL = textureLod(u_irr, pixel + uv, 0.0).rgb;
	maxL = max(maxL, textureLod(u_irr, pixel*vec2(1.0,0.0) + uv, 0.0).rgb);
	maxL = max(maxL, textureLod(u_irr, pixel*vec2(1.0,-1.0) + uv, 0.0).rgb);
	maxL = max(maxL, textureLod(u_irr, pixel*vec2(0.0,1.0) + uv, 0.0).rgb);
	maxL = max(maxL, textureLod(u_irr, pixel*vec2(-1.0,1.0) + uv, 0.0).rgb);
	maxL = max(maxL, textureLod(u_irr, pixel*vec2(-1.0,-1.0) + uv, 0.0).rgb);
	maxL = max(maxL, textureLod(u_irr, pixel*vec2(0.0,-1.0) + uv, 0.0).rgb);
	maxL = max(maxL, textureLod(u_irr, pixel*vec2(-1.0,0.0) + uv, 0.0).rgb);
	outColor = vec4( maxL, 1.0);
}