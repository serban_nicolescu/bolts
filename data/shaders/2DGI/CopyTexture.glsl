
uniform vec4 u_rtResolution; // xy resolution | zw inv res
uniform sampler2D u_tex;
uniform sampler2D u_tex2;
uniform float u_multiplier;

#ifdef DST_ALPHA
	vec4 toOut(vec4 val)
	{
		return val;
	}
	out vec4 outColor;
#else
	vec3 toOut(vec4 val)
	{
		return val.rgb;
	}
	out vec3 outColor;
#endif

void main()
{
	vec2 pixel = u_rtResolution.zw;
	vec2 uv = gl_FragCoord.xy * pixel;
	
	vec4 outVal;
#ifdef SRC_R
	//outVal = vec4( textureLod( u_tex, uv, 0.0).r == 1.0 ? 1.0 : 0.0, 0.0,0.0,1.0); 
	outVal = vec4( fract(textureLod( u_tex, uv, 0.0).r / 30.0), 0.0,0.0,1.0); 
	//outVal = vec4( textureLod( u_tex, uv, 0.0).r, 0.0,0.0,1.0);
#elif defined(SRC_RG)
	outVal = vec4( textureLod( u_tex, uv, 0.0).rg,0.0,1.0);
#elif defined(MULT_SRC)
	outVal = vec4( textureLod( u_tex, uv, 0.0) * u_multiplier);
#else
	outVal = textureLod( u_tex, uv, 0.0);
	outVal /= 6.0;
	outVal.rgb = pow(outVal.rgb, vec3(0.4545));
#endif

	outColor = toOut(outVal);
}