

uniform vec4 u_rtResolution; // xy resolution | zw inv res
uniform vec3 u_rayInfo; //xy ray dir, z raytraceContribution
uniform sampler2D u_scene;
uniform sampler2D u_trace;

//#define TRACE_SCENE

bool isHit(vec4 c)
{
#ifdef TRACE_SCENE
	return (c.x + c.y + c.z) > 0.0f;
#else
	return c.x > 0.0f;
#endif
}

//TODO: No test for out-of-bounds
bool isOut(vec2 uv)
{
	return (uv.x < 0.f || uv.y < 0.f || uv.x > 1.f || uv.y > 1.f);
}

vec3 trace(vec2 start, vec2 step)
{
	vec2 uv = start;
	float distance = 0.0;
	float range = 1024.0;
#define HI
#ifdef HI
	float stepSize = 1.0;
	int numSteps = 0;
	const int maxSteps = 1000;
	while (numSteps < maxSteps && !isOut(uv))
	{
		bool hit = isHit(textureLod(u_trace, uv, stepSize - 1.0));
		if (hit && (stepSize == 1.0))
			break;
		numSteps++;
		uv += step * stepSize;
		if (hit)
		{
			uv -= step * stepSize;
			stepSize -= 1.0;
		} else {
			stepSize += 1.0;
		}
	}
	distance = isOut(uv) ? 5000.0 : length(start - uv);
#else
	while (distance < range)
	{
		if (isHit(textureLod(u_trace, uv, 0.0)))
			break;
		uv += step;
		distance += 1.0;
	}
#endif
	return vec3(uv, distance);
}

#ifdef DIST
	out float outColor;
#else
		
	vec3 fake2DLight(vec2 hitUV, float hitDistance)
	{
		float range = 512.0;
		hitDistance /= range;
		hitDistance = clamp(hitDistance, 0, 1);
		hitDistance = 1.0 - hitDistance;
		float falloff = hitDistance*hitDistance;
		vec4 tap = textureLod(u_scene, hitUV, 0.0);
		return vec3( tap.rgb * falloff);
	}

	out vec4 outColor;
#endif

void main()
{
	vec2 pixel = u_rtResolution.zw;
	vec2 uv = ( gl_FragCoord.xy) * pixel;
	vec2 step = u_rayInfo.xy;
	vec3 traceResult = trace( uv, step * pixel);
	
#ifdef DIST
	outColor = traceResult.z * u_rtResolution.x;
#else
	vec3 hit = fake2DLight(traceResult.xy, traceResult.z);
	outColor = vec4( hit * u_rayInfo.z, 1.0);
#endif
	
	//outColor = vec4( vec3(textureLod(u_walls, uv, 8.0).r) * u_rayInfo.z, 1.0);
	//outColor = texture(u_scene, uv);
	//outColor = vec4(0.5);
}