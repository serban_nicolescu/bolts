

uniform vec4 u_rtResolution; // xy resolution | zw inv res
uniform vec3 u_rayInfo; //xy ray dir, z raytraceContribution
uniform float u_lightIntensity;

uniform sampler2D u_radiance;
uniform sampler2D u_sdf;

bool isOut(vec2 uv)
{
	return (uv.x < 0.f || uv.y < 0.f || uv.x > 1.f || uv.y > 1.f);
}

vec3 trace(vec2 start, vec2 step)
{
	vec2 uv = start;
	int numSteps = 0;
	const int maxSteps = 200;
	while (numSteps < maxSteps && !isOut(uv))
	{
		float sdf = textureLod(u_sdf, uv, 0.0).r;
		if (sdf <= 0.0)
			break;
		sdf = max(sdf, 2.0);
		uv += step * sdf;
		numSteps++;
	}
	return vec3(uv, length(uv - start));
}

vec3 fake2DLight(vec2 hitUV, float hitDistance)
{
	float range = 1024.0;
	hitDistance /= range;
	hitDistance = clamp(hitDistance, 0, 1);
	hitDistance = 1.0 - hitDistance;
	float falloff = hitDistance*hitDistance;
	vec3 tap = textureLod(u_radiance, hitUV, 0.0).rgb;
	return vec3( tap * falloff);
}

out vec4 outColor;

void main()
{
	vec2 pixel = u_rtResolution.zw;
	vec2 uv = ( gl_FragCoord.xy) * pixel;
	vec2 step = u_rayInfo.xy;
	vec3 traceResult = trace( uv, step * pixel);
	float distInPx = traceResult.z * u_rtResolution.x;
	vec3 hit = fake2DLight(traceResult.xy, distInPx);
	outColor = vec4( hit * u_rayInfo.z, 1.0);
}