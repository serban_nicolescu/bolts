

uniform vec4 u_rtResolution; // xy resolution | zw inv res
uniform sampler2D u_scene;

bool isHit(vec4 c)
{
	//return (c.x + c.y + c.z) > 0.0f;
	return c.a > 0.0f;
}

out float outColor;

void main()
{
	vec2 pixel = u_rtResolution.zw;
	vec2 uv = gl_FragCoord.xy * pixel;
	//uv.y = 1.0 - uv.y;
	outColor = isHit( textureLod(u_scene, uv, 0.0)) ? 1.0 : 0.0;
}