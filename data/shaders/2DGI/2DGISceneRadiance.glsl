

uniform vec4 u_rtResolution; // xy resolution | zw inv res
uniform sampler2D u_scene;
uniform sampler2D u_sceneEmissive;
uniform sampler2D u_irr;

out vec4 outColor;

void main()
{
	vec2 pixel = u_rtResolution.zw;
	vec2 uv = gl_FragCoord.xy * pixel;
	vec3 mat = textureLod(u_scene, uv,0.0).rgb;
	//mat = pow(mat, vec3(2.2));
	vec3 light = (textureLod(u_sceneEmissive, uv,0.0).rgb * 3.0) + (mat * textureLod(u_irr, uv,0.0).rgb);
	//vec4 emsv = textureLod(u_sceneEmissive, uv,0.0);
	//light = light * 0.000000001 + textureLod(u_sceneEmissive, uv,0.0).rgb;
	//light = sqrt(light);
	//light = pow(light, vec3(0.4545));
	outColor = vec4( light , 1.0);
	//outColor = vec4( color, 1.0);
}