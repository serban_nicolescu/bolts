
#define VERTEX_NORMALS
#define WORLD_NORMALS
#define WORLD_POSITION

#if defined(ALBEDO_MAP) || defined(NORMAL_MAP) || defined(ROUGHNESS_MAP) || defined(METALNESS_MAP)
#	define VERTEX_UV0
#endif

#ifdef NORMAL_MAP
#	define WORLD_TANGENTS
#endif

#pragma bolts include "standard_vs"
#pragma bolts include "blinn_phong_common"

#ifdef SHADOWS
	smooth out vec4 v_posShadow;
#endif

void main()
{
	VSOut vsOut;
	vsOut.posW = getPositionW( getPositionL());
#ifdef VERTEX_NORMALS
	vsOut.normalW = getNormalW( getNormalL());
#endif
	outputStandardAttribs(vsOut);
#ifdef SHADOWS
	v_posShadow = g_directionalShadowM * vec4(vsOut.posW,1.0);
#endif
}