#pragma bolts include "standard_vs"

void main()
{
	VSOut vsOut;
	vsOut.posW = getPositionW( getPositionL());
#ifdef VERTEX_NORMALS
	vsOut.normalW = getNormalW( getNormalL());
#endif

	outputStandardAttribs(vsOut);
}