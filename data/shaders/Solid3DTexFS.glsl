uniform sampler3D u_texColor;

in vec3 v_norm;
in vec2 v_uv;

out vec4 outColor;

void main()
{
	outColor = texture(u_texColor, vec3(v_uv, 0.5f));	
}