uniform sampler2D u_texColor;

in vec2 v_uv;

out vec4 outColor;

void main()
{
	outColor = texture(u_texColor, v_uv);
}