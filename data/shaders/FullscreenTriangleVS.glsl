
out mediump	vec2 v_uv;

void main(void)
{
	//vec2 pos[] = vec2[](
	//	vec2(  0.0, 0.0 ),
	//	vec2(  0.5, 0.0 ),
	//	vec2( -0.5, 0.0 )
	//);
	
	float x = -1.0 + float((gl_VertexID & 1) << 2);
	float y = -1.0 + float((gl_VertexID & 2) << 1);
	v_uv.x = (x + 1.0) * 0.5;
	v_uv.y = (y + 1.0) * 0.5;
	gl_Position = vec4(x, y, 0, 1);
}
