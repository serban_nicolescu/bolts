uniform mat4 u_wvp;

in vec3 a_posL;
in vec2 a_uv0;

out vec2 v_uv;

void main()
{
	v_uv = a_uv0;
	gl_Position = u_wvp * vec4( a_posL, 1.0);
}
