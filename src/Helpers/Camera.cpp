//#include "stdafx.h"
#include "Helpers/Camera.h"
#include <bolts_assert.h>
#include <glm/gtc/matrix_transform.hpp>

Bolts::Camera::Camera() : m_viewDirty(true), m_projectionDirty(true), m_position(1.f), m_target(0.f), m_up(0.f, 0.f, 1.f),m_zNear(0.1f), m_zFar(15.f)
{
	p.m_aspectRatio = 1.f;
	p.m_fov = degToRad(30.f);
}

void Bolts::Camera::CopyFrom(const Camera* pOther)
{
	memcpy(this, pOther, sizeof(Bolts::Camera));
}

const Bolts::mat4& Bolts::Camera::GetViewMatrix() const
{
	if (m_viewDirty) {
		UpdateViewMatrix();
	}

	return m_viewMatrix;
}

const Bolts::mat4& Bolts::Camera::GetInverseViewMatrix() const
{
	if (m_viewDirty) {
		UpdateViewMatrix();
	}

	return m_inverseViewMatrix;
}

void Bolts::Camera::UpdateViewMatrix() const
{
	UpdateInverseViewMatrix();
	//TODO: UGH
	m_viewMatrix = glm::inverse(m_inverseViewMatrix);
	m_viewDirty = false;
}

const Bolts::mat4& Bolts::Camera::GetProjectionMatrix() const
{
	if (m_projectionDirty) {
		UpdateProjMatrix();
	}

	return m_projMatrix;
}

const Bolts::mat4& Bolts::Camera::GetInverseProjectionMatrix() const
{
	return m_inverseProjMatrix;
}

void Bolts::Camera::UpdateProjMatrix() const
{
	if (!m_camIsOrtho)
		m_projMatrix = glm::perspective(p.m_fov, p.m_aspectRatio, m_zNear, m_zFar);
	else {
		m_projMatrix = glm::ortho(o.m_orthoHExtents.x, o.m_orthoHExtents.y, o.m_orthoVExtents.x, o.m_orthoVExtents.y, m_zNear, m_zFar);
	}
	m_inverseProjMatrix = glm::inverse(m_projMatrix);
	m_projectionDirty = false;
}

void Bolts::Camera::UpdateInverseViewMatrix() const
{
	vec3 vx, vy, vz;
	vz = glm::normalize(m_position - m_target);
	vx = glm::normalize(glm::cross(m_up, vz));
	vy = glm::cross(vz, vx);
	m_inverseViewMatrix[0] = vec4(vx, 0.f);
	m_inverseViewMatrix[1] = vec4(vy, 0.f);
	m_inverseViewMatrix[2] = vec4(vz, 0.f);
	m_inverseViewMatrix[3] = vec4(m_position, 1.f);
}


void Bolts::Camera::MoveForward(float amount)
{
	const Bolts::vec3 delta = glm::normalize(m_target - m_position) * amount;
	m_position += delta;
	m_target += delta;
	m_viewDirty = true;
}

void Bolts::Camera::MoveSideways(float amount)
{
	const Bolts::vec3 delta = glm::normalize(glm::cross(m_up, (m_target - m_position))) * amount;
	m_position += delta;
	m_target += delta;
	m_viewDirty = true;
}

void Bolts::Camera::MoveUp(float amount)
{
	m_position += m_up * amount;
	m_target += m_up * amount;
	m_viewDirty = true;
}

void Bolts::Camera::LookUp(float rads)
{
	const vec3 forward = glm::normalize(m_target - m_position);
	const vec3 right = glm::normalize(glm::cross(forward, m_up));
	const Bolts::quat rot = glm::angleAxis(-rads, right);
	const vec3 newFw = rot * forward;
	m_target = m_position + newFw;

	m_viewDirty = true;
}

void Bolts::Camera::LookRight(float rads)
{
	const vec3 forward = glm::normalize(m_target - m_position);
	const Bolts::quat rot = glm::angleAxis(-rads, m_up);
	const vec3 newFw = rot * forward;
	m_target = m_position + newFw;

	m_viewDirty = true;
}

void Bolts::Camera::TiltRight(float rads)
{
	const vec3 forward = glm::normalize(m_target - m_position);
	const vec3 right = glm::cross(forward, m_up);
	const vec3 up = glm::normalize(glm::cross(right, forward));
	Bolts::mat4 rot;
	rot = glm::rotate(rot, rads, forward);
	const vec3 newUp = glm::vec3((rot * glm::vec4(up, 1.0)));
	m_up = newUp;

	m_viewDirty = true;
}

void Bolts::Camera::OrbitRight(float rads)
{
	const vec3 forward = m_position - m_target;
	const Bolts::quat rot = glm::angleAxis(rads, m_up);
	const vec3 newFw = rot * forward;
	m_position = m_target + newFw;

	m_viewDirty = true;
}

void Bolts::Camera::OrbitUp(float rads)
{
	const vec3 forward = m_position - m_target;
	const vec3 right = glm::normalize(glm::cross(forward, m_up));
	const Bolts::quat rot = glm::angleAxis(-rads, right);
	const vec3 newFw = rot * forward;
	m_position = m_target + newFw;

	m_viewDirty = true;
}

Bolts::Segment Bolts::Camera::ProjectScreenCoords(vec2 screenUV) const
{
	//*
	vec2 delta = screenUV;
	delta.x *= -p.m_aspectRatio;
	delta *= tanf(p.m_fov *0.5f);//TODO: Store this value on FOV change

	vec3 start = vec3(delta, 1.0)*-m_zNear;
	vec3 end = vec3(delta, 1.0)*-m_zFar * 0.8f;
	const mat4 invView = GetInverseViewMatrix();
	const vec4 startW = invView * vec4(start, 1.0);
	const vec4 endW = invView * vec4(end, 1.0);
	return { vec3(startW), vec3(endW) };
	/*/
	/*
	vec4 clipRay = GetInverseProjectionMatrix() * vec4( screenUV, -1.f, 1.f);//Near-plane ray pos
	const vec3 eyeRay = glm::normalize( mat3(GetInverseViewMatrix ()) * vec3( clipRay.x, clipRay.y, -1.f));
	Ray l_outRay;
	l_outRay.start = m_position + eyeRay * m_zNear;
	l_outRay.end = m_position + eyeRay * m_zFar;
	return l_outRay;
	*/
}

void Bolts::Camera::ExtractFrustumWSCorners(vec3 pts[8]) const
{
	const mat4& camWS = GetInverseViewMatrix();
	const vec3 camX = vec3(camWS[0]);
	const vec3 camY = vec3(camWS[1]);
	const vec3 camZ = -vec3(camWS[2]);
	//
	const vec3 centerNear = vec3(camWS[3]) + (camZ * m_zNear);
	const vec3 centerFar = vec3(camWS[3]) + (camZ * m_zFar);
	//Viewport extents
	float nearXMin, nearXMax, nearYMin, nearYMax;
	float farXMin, farXMax, farYMin, farYMax;
	if (m_camIsOrtho) {
		nearXMin = farXMin = o.m_orthoHExtents[0];
		nearXMax = farXMax = o.m_orthoHExtents[1];
		nearYMin = farYMin = o.m_orthoVExtents[0];
		nearYMax = farYMax = o.m_orthoVExtents[1];
	}
	else {
		float bla = tanf(p.m_fov * 0.5f);
		nearYMax = bla * m_zNear;
		nearYMin = -nearYMax;
		farYMax = bla * m_zFar;
		farYMin = -farYMax;
		nearXMax = nearYMax * p.m_aspectRatio;
		nearXMin = nearYMin * p.m_aspectRatio;
		farXMax = farYMax * p.m_aspectRatio;
		farXMin = farYMin * p.m_aspectRatio;
	}
	//
	pts[0] = centerNear + (camX * nearXMin) + (camY * nearYMin);
	pts[1] = centerNear + (camX * nearXMin) + (camY * nearYMax);
	pts[2] = centerNear + (camX * nearXMax) + (camY * nearYMax);
	pts[3] = centerNear + (camX * nearXMax) + (camY * nearYMin);
	pts[4] = centerFar + (camX * farXMin) + (camY * farYMin);
	pts[5] = centerFar + (camX * farXMin) + (camY * farYMax);
	pts[6] = centerFar + (camX * farXMax) + (camY * farYMax);
	pts[7] = centerFar + (camX * farXMax) + (camY * farYMin);
}

//Bolts::vec4	Bolts::Camera::CenteredEnclosingSphere() const
//{
//	using namespace Bolts;
//	float radius = m_zFar /cosf(p.m_fov);
//	if (m_camIsOrtho) {
//		float farExt = o.m_orthoVExtents[1];
//		radius = sqrtf(farExt*farExt + m_zFar * m_zFar);
//	}
//	return vec4(m_position, radius);
//}

Bolts::vec4	Bolts::Camera::TightEnclosingSphere(float zfarOverride) const
{
	BASSERT(!m_camIsOrtho); // We could easily mplement support for ortho mode
	float f = zfarOverride;
	float n = m_zNear;
	const float fn = f - n;
	const float bla = tanf(p.m_fov * 0.5f);
	const float blu = sqrtf(1 + p.m_aspectRatio*p.m_aspectRatio);
	const float fd = bla * f * blu;
	const float nd = bla * n * blu;
	const float c = n + ((fd*fd + fn * fn - nd * nd) / (2 * fn)); // sphere center, distance along view
	const float R = sqrtf(fd*fd + (f - c)*(f - c)); // Sphere radius

	const vec3 center = m_position + (GetForward() * c);
	return vec4(center, R);
}

Bolts::vec3	Bolts::Camera::SnapToViewSpaceGrid(vec3 worldPosition, vec2 gridSize) const
{
	vec3 viewPos( GetViewMatrix() * vec4(worldPosition, 1.f));
	vec2 viewXY(viewPos.x, viewPos.y);
	viewXY /= gridSize;
	viewXY = glm::floor(viewXY);
	viewXY *= gridSize;
	viewPos.x = viewXY.x;
	viewPos.y = viewXY.y;
	
	//viewPos /= gridSize.x;
	//viewPos = glm::floor(viewPos);
	//viewPos *= gridSize.x;
	return vec3(GetInverseViewMatrix()* vec4(viewPos, 1.f));
}

void Bolts::Camera::ExtractFrustumPlanes(const mat4 & m, vec4 (&planes)[6])
{
	//NOTE: planes[i].xyz is the plane normal. For plane eq ax + by + cz + d = 0, planes[i] = [a,b,c,d]
	// Plane order is: Left Right Top Bottom Near Far
	planes[0].x = m[0].w + m[0].x;
	planes[1].x = m[0].w - m[0].x;
	planes[2].x = m[0].w + m[0].y;
	planes[3].x = m[0].w - m[0].y;
	planes[4].x = m[0].w + m[0].z;
	planes[5].x = m[0].w - m[0].z;
	planes[0].y = m[1].w + m[1].x;
	planes[1].y = m[1].w - m[1].x;
	planes[2].y = m[1].w + m[1].y;
	planes[3].y = m[1].w - m[1].y;
	planes[4].y = m[1].w + m[1].z;
	planes[5].y = m[1].w - m[1].z;
	planes[0].z = m[2].w + m[2].x;
	planes[1].z = m[2].w - m[2].x;
	planes[2].z = m[2].w + m[2].y;
	planes[3].z = m[2].w - m[2].y;
	planes[4].z = m[2].w + m[2].z;
	planes[5].z = m[2].w - m[2].z;
	planes[0].w = m[3].w + m[3].x;
	planes[1].w = m[3].w - m[3].x;
	planes[2].w = m[3].w + m[3].y;
	planes[3].w = m[3].w - m[3].y;
	planes[4].w = m[3].w + m[3].z;
	planes[5].w = m[3].w - m[3].z;
	// Normalise and flip so they point outwards
	planes[0] /= -glm::length(vec3(planes[0]));
	planes[1] /= -glm::length(vec3(planes[1]));
	planes[2] /= -glm::length(vec3(planes[2]));
	planes[3] /= -glm::length(vec3(planes[3]));
	planes[4] /= -glm::length(vec3(planes[4]));
	planes[5] /= -glm::length(vec3(planes[5]));
}

void Bolts::Camera::BlendFrom(const Camera* other, float factor)
{
	BASSERT(!m_camIsOrtho); // this isn't implemented
	BASSERT(!other->m_camIsOrtho); // this isn't implemented
	m_viewDirty = true;
	m_projectionDirty = true;

#define BOLTS__MIXTHIS(varName) varName = glm::mix(varName, other->varName, factor);

	BOLTS__MIXTHIS(m_position);
	BOLTS__MIXTHIS(m_target);
	BOLTS__MIXTHIS(m_up);
	BOLTS__MIXTHIS(m_zNear);
	BOLTS__MIXTHIS(m_zFar);
	if (!m_camIsOrtho) {
		BOLTS__MIXTHIS(p.m_fov);
		BOLTS__MIXTHIS(p.m_aspectRatio);
	}

#undef BOLTS__MIXTHIS
}

