#pragma once

#include "Rendering/GPUBuffers.h"
#include <bolts_core.h>

namespace Bolts {
	namespace MeshBuilder {

		void							CubeVertices(Rendering::RCB&, Rendering::VertexBuffer* outVbuffer, Bolts::vec3 halfExtents);
		Rendering::PrimitiveBufferPtr	CubeIndexed(Rendering::RCB&, Bolts::vec3 halfExtents);

		/// A square with Z = 0.0 that maps between -1 and 1 on X and Y
		Rendering::PrimitiveBufferPtr	FullscreenQuad(Rendering::RCB&);
		/// A square with Z = 0.0 that maps between -1 and 1 on X and Y
		///		And uvs from [ 1.0, 1.0] at [ 1.0, 1.0, 0.0] to [ 0.0, 0.0] at [ -1.0, -1.0, 0.0]
		Rendering::PrimitiveBufferPtr	FullscreenQuadWithUVS(Rendering::RCB&);

		Rendering::PrimitiveBufferPtr	SphereIndexed(Rendering::RCB&, float size, uint8_t hSlices, uint8_t vSlices);
		Rendering::PrimitiveBufferPtr	CylinderIndexed(Rendering::RCB&, Bolts::vec3 direction, Bolts::vec3 biNorm, float radius, float height, uint8_t hSlices, uint8_t vSlices);

		Rendering::PrimitiveBufferPtr	GridIndexed(Rendering::RCB&, int resolution);

	};
};
