#pragma once

#include <Input/IOEvents.h>
#include <Rendering/RenderBackend.h>

namespace Bolts {
	namespace System {
		typedef uint64_t windowHandle_t;
	};

	namespace Rendering
	{
		class Renderer;
	}
};

bool	ImGui_Init( void* hWin, Bolts::Rendering::RCB& commands, const char* iniFilename = nullptr);
void	ImGui_Shutdown(Bolts::Rendering::RCB& commands);
void	ImGui_NewFrame( float dtSec, Bolts::System::windowHandle_t window, int w, int h, int display_w, int display_h );
void	ImGui_RenderDrawData(Bolts::Rendering::RenderBackend& backend);

bool		ImGui_MouseEvent(Bolts::Input::mouseEvent_t evt);
bool		ImGui_KeyEvent(Bolts::Input::keyEvent_t evt);
bool		ImGui_CharCallback(unsigned int chrCode);

//void        ImGui_ImplBoltsGL3_MouseButtonCallback( Bolts::Input::mouseButton_t mb, Bolts::Input::mouseState_t ms );
//void        ImGui_ImplBoltsGL3_MousePosition( int mx, int my);
//void        ImGui_ImplBoltsGL3_ScrollCallback( double xoffset, double yoffset );
//void        ImGui_ImplBoltsGL3_KeyCallback( Bolts::Input::keyCode_t kc, Bolts::Input::keyState_t ks);
