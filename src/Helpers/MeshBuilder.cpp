#include "Helpers/MeshBuilder.h" 
#include <bolts_assert.h>
#include <glm/gtc/matrix_transform.hpp>

using namespace Bolts;

//Helper structs
namespace {
	using namespace Bolts;

	struct PosOnly {
		vec3 pos;
	};
	struct PosNorm {
		vec3 pos;
		vec3 norm;
	};
};

namespace BuildingPrimitives
{
	unsigned RingIndicesNum(unsigned slices)
	{
		return (slices * 6);
	}

	template< typename IndexType>
	IndexType RingIndices(IndexType* bufferStart, IndexType firstVertId, uint8_t slices)
	{
		IndexType extraIndices = static_cast<IndexType> (RingIndicesNum(slices));
		IndexType currentIndex = 0;
		IndexType patchVertStart = firstVertId + slices;
		for (uint8_t patchIdx = 0; patchIdx < slices; patchIdx++) {
			bufferStart[currentIndex] = patchVertStart;
			bufferStart[currentIndex + 2] = patchVertStart + 1;
			bufferStart[currentIndex + 1] = patchVertStart - slices;
			currentIndex += 3;
			bufferStart[currentIndex] = patchVertStart + 1;
			bufferStart[currentIndex + 1] = patchVertStart - slices;
			bufferStart[currentIndex + 2] = patchVertStart - slices + 1;
			currentIndex += 3;
			patchVertStart++;
		}
		//Close last patch in ring
		bufferStart[currentIndex - 4] = firstVertId + slices;
		bufferStart[currentIndex - 3] = firstVertId + slices;
		bufferStart[currentIndex - 1] = firstVertId;

		return extraIndices;
	}

	template< typename IndexType>
	IndexType ConeIndices(IndexType* bufferStart, IndexType firstVertId, IndexType tipVertId, uint8_t slices, bool ccw)
	{
		IndexType extraIndices = 3 * slices;
		for (IndexType triIndex = 0; triIndex < slices; triIndex++) {
			bufferStart[triIndex * 3] = tipVertId;
			if (ccw) {
				bufferStart[triIndex * 3 + 1] = firstVertId + triIndex + 1;
				bufferStart[triIndex * 3 + 2] = firstVertId + triIndex;
			} else {
				bufferStart[triIndex * 3 + 1] = firstVertId + triIndex;
				bufferStart[triIndex * 3 + 2] = firstVertId + triIndex + 1;
			}
		}
		bufferStart[slices * 3 - 2] = firstVertId;

		return extraIndices;
	}

	unsigned CircleVerticesNum(unsigned subdivisions)
	{
		return subdivisions;
	}

	template< typename VertDataType>
	unsigned CircleVertices(VertDataType* bufferStart, VertDataType* bufferEnd, vec3 centerPosition, vec3 normal, vec3 biNorm, float radius, unsigned subdivisions)
	{
		bufferEnd;//to get rid of warning for "unreferenced formal parameter" on Release
		unsigned addedVertices = CircleVerticesNum(subdivisions);
		BASSERT_MSG((bufferEnd - bufferStart + 1u) > addedVertices, "Vertex buffer too small.");

		float angleIncrement = 360.f / subdivisions;
		//float currentAngle = 0.f;//angleIncrement;
		quat rotationStep = glm::angleAxis(angleIncrement, normal);
		quat rot = rotationStep;
		biNorm *= radius;
		VertDataType* currentVert = bufferStart;
		for (unsigned i = subdivisions; i > 0; i--) {
			//quat rot = glm::angleAxis ( currentAngle, normal);
			vec3 vertPos = rot * biNorm;
			currentVert->norm = glm::normalize(vertPos);
			vertPos += centerPosition;
			currentVert->pos = vertPos;
			//currentAngle += angleIncrement;
			rot = rot * rotationStep;
			currentVert++;
		}
		return addedVertices;
	}

	unsigned CapIndicesNum(unsigned circleVerts)
	{
		return 3 * (circleVerts - 2);
	}

	template< typename IndexType>
	IndexType CapIndices(IndexType* bufferStart, IndexType* bufferEnd, IndexType startVertID, unsigned circleVerts, bool flipped = false)
	{
		bufferEnd;//to get rid of warning for "unreferenced formal parameter" on Release
		//Closes a circle of verts
		BASSERT_MSG(circleVerts >= 3, "Need at least 3 verts for a triangle");
		IndexType extraIndices = static_cast<IndexType> (CapIndicesNum(circleVerts));
		BASSERT_MSG((bufferEnd - bufferStart + 1) > extraIndices, "Index buffer too small.");
		IndexType* bufferPos = bufferStart;
		IndexType lastVertID = static_cast<IndexType> (startVertID + circleVerts - 1);
		IndexType flip = (flipped ? 1 : 0);
		for (IndexType vertIndex = startVertID + 2; vertIndex <= lastVertID; bufferPos += 3, vertIndex++) {
			bufferPos[0] = startVertID;
			bufferPos[1] = vertIndex - flip;
			bufferPos[2] = vertIndex - 1 + flip;
		}

		return extraIndices;
	}

};

namespace Bolts {
	namespace MeshBuilder {

		using namespace Rendering;

		void CubeVertices(RCB& commands, VertexBuffer* outVbuffer, vec3 halfExtents) //, bool withUV, bool withNormals)
		{
			VertexLayout desc;
			desc.AddStream({ BGSS_POSITION, BGVT_FLOAT, 3 });
			desc.AddStream({ BGSS_NORMALS, BGVT_FLOAT, 3 });

			dataBuffer_t dataOut = outVbuffer->SetData(commands, BUH_STATIC_DRAW, desc, 6 * 6);
			float cubeVerts[6 * 6 * 6] = {
				//Right +X
				1.f, 1.f, 1.f, 1.f, 0.f, 0.f,
				1.f, -1.f, 1.f, 1.f, 0.f, 0.f,
				1.f, 1.f, -1.f, 1.f, 0.f, 0.f,
				1.f, -1.f, -1.f, 1.f, 0.f, 0.f,
				1.f, 1.f, -1.f, 1.f, 0.f, 0.f,
				1.f, -1.f, 1.f, 1.f, 0.f, 0.f,
				//Front +Z
				1.f, 1.f, 1.f, 0.f, 0.f, 1.f,
				-1.f, 1.f, 1.f, 0.f, 0.f, 1.f,
				1.f, -1.f, 1.f, 0.f, 0.f, 1.f,
				-1.f, 1.f, 1.f, 0.f, 0.f, 1.f,
				-1.f, -1.f, 1.f, 0.f, 0.f, 1.f,
				1.f, -1.f, 1.f, 0.f, 0.f, 1.f,
				//Top +Y
				1.f, 1.f, 1.f, 0.f, 1.f, 0.f,
				1.f, 1.f, -1.f, 0.f, 1.f, 0.f,
				-1.f, 1.f, 1.f, 0.f, 1.f, 0.f,
				-1.f, 1.f, -1.f, 0.f, 1.f, 0.f,
				-1.f, 1.f, 1.f, 0.f, 1.f, 0.f,
				1.f, 1.f, -1.f, 0.f, 1.f, 0.f,
				//Left -X
				-1.f, 1.f, 1.f, -1.f, 0.f, 0.f,
				-1.f, 1.f, -1.f, -1.f, 0.f, 0.f,
				-1.f, -1.f, 1.f, -1.f, 0.f, 0.f,
				-1.f, -1.f, -1.f, -1.f, 0.f, 0.f,
				-1.f, -1.f, 1.f, -1.f, 0.f, 0.f,
				-1.f, 1.f, -1.f, -1.f, 0.f, 0.f,
				//Back -Z
				1.f, -1.f, -1.f, 0.f, 0.f, -1.f,
				-1.f, 1.f, -1.f, 0.f, 0.f, -1.f,
				1.f, 1.f, -1.f, 0.f, 0.f, -1.f,
				-1.f, -1.f, -1.f, 0.f, 0.f, -1.f,
				-1.f, 1.f, -1.f, 0.f, 0.f, -1.f,
				1.f, -1.f, -1.f, 0.f, 0.f, -1.f,
				//Bottom -Y
				1.f, -1.f, 1.f, 0.f, -1.f, 0.f,
				-1.f, -1.f, 1.f, 0.f, -1.f, 0.f,
				1.f, -1.f, -1.f, 0.f, -1.f, 0.f,
				-1.f, -1.f, 1.f, 0.f, -1.f, 0.f,
				-1.f, -1.f, -1.f, 0.f, -1.f, 0.f,
				1.f, -1.f, -1.f, 0.f, -1.f, 0.f
			};
			// 	float cubeVerts[ 6 * 6 * 3] = {
			// 		//Right +X
			// 		1.f, 1.f, 1.f,
			// 		1.f, -1.f, 1.f,
			// 		1.f, 1.f, -1.f,
			// 		1.f, -1.f, -1.f,
			// 		1.f, 1.f, -1.f,
			// 		1.f, -1.f, 1.f,
			// 		//Front +Z	   ,
			// 		1.f, 1.f, 1.f, 
			// 		-1.f, 1.f, 1.f,
			// 		1.f, -1.f, 1.f,
			// 		-1.f, 1.f, 1.f,
			// 		-1.f, -1.f, 1.f,
			// 		1.f, -1.f, 1.f,
			// 		//Top +Y	   ,
			// 		1.f, 1.f, 1.f,
			// 		1.f, 1.f, -1.f,
			// 		-1.f, 1.f, 1.f,
			// 		-1.f, 1.f, -1.f,
			// 		-1.f, 1.f, 1.f,
			// 		1.f, 1.f, -1.f,
			// 		//Left -X	   ,
			// 		-1.f, 1.f, 1.f,
			// 		-1.f, 1.f, -1.f,
			// 		-1.f, -1.f, 1.f,
			// 		-1.f, -1.f, -1.,
			// 		-1.f, -1.f, 1.f,
			// 		-1.f, 1.f, -1.f,
			// 		//Back -Z	   ,
			// 		1.f, -1.f, -1.f,
			// 		-1.f, 1.f, -1.f,
			// 		1.f, 1.f, -1.f,
			// 		-1.f, -1.f, -1.,
			// 		-1.f, 1.f, -1.f,
			// 		1.f, -1.f, -1.f,
			// 		//Bottom -Y	   ,
			// 		1.f, -1.f, 1.f,
			// 		-1.f, -1.f, 1.f,
			// 		1.f, -1.f, -1.f,
			// 		-1.f, -1.f, 1.f,
			// 		-1.f, -1.f, -1.f,
			// 		1.f, -1.f, -1.f
			// 	};
				//Scale to extents
			for (unsigned i = 0; i < 6 * 6 * 6; i += 6)
				//for ( unsigned i = 0; i < 6*6*3; i += 3)
			{
				cubeVerts[i] *= halfExtents.x;
				cubeVerts[i + 1] *= halfExtents.y;
				cubeVerts[i + 2] *= halfExtents.z;
			}
			BASSERT(sizeof(cubeVerts) == dataOut.dataSize);
			memcpy(dataOut.data, cubeVerts, sizeof(cubeVerts));
		}

		PrimitiveBufferPtr CubeIndexed(RCB& commands, vec3 halfExtents)
		{
			VertexBufferPtr oVBuffer = new VertexBuffer;
			CubeVertices(commands, oVBuffer.get(), halfExtents);
			IndexBufferPtr oIBuffer = new IndexBuffer();
			uint8_t cubeIndices[6 * 6] = {
				//Right +X
				0,1,2,
				3,4,5,
				//Front +Z
				6,7,8,
				9,10,11,
				//Top +Y
				12,13,14,
				15,16,17,
				//Left -X
				18,19,20,
				21,22,23,
				//Back -Z
				24,25,26,
				27,28,29,
				//Bottom -Y
				30,31,32,
				33,34,35
			};
			dataBuffer_t dataOut = oIBuffer->SetData(commands, BUH_STATIC_DRAW, 36, BGVT_U8);
			BASSERT(dataOut.dataSize == sizeof(cubeIndices));
			memcpy(dataOut.data, cubeIndices, sizeof(cubeIndices));

			auto outPbuffer = new PrimitiveBuffer;
			outPbuffer->m_indexBuffer = oIBuffer;
			outPbuffer->Build(commands, oVBuffer, oIBuffer, BGPT_TRIANGLES);
			return outPbuffer;
		}

		PrimitiveBufferPtr SphereIndexed(RCB& commands, float size, uint8_t hSlices, uint8_t vSlices)
		{
			//Min h slices: 1 | Min v slices 3
			uint32_t l_numVertices = 2 + hSlices * vSlices;
			PosNorm* l_vertData = new PosNorm[l_numVertices];
			//Pole verts
			l_vertData[0].pos = vec3(0.f, size, 0.f);
			l_vertData[0].norm = vec3(0.f, 1.f, 0.f);
			l_vertData[l_numVertices - 1].pos = vec3(0.f, -size, 0.f);
			l_vertData[l_numVertices - 1].norm = vec3(0.f, -1.f, 0.f);

			float hStep = c_PI * 2.f / vSlices;
			float vStep = c_PI / (hSlices + 1);
			float azimuth = 0.f;
			float inclination = vStep;
			uint32_t vertIndex = 1;
			for (uint32_t i = 0; i < hSlices; i++) {
				const float sinincl = sin(inclination);
				const float cosincl = cos(inclination);
				for (uint32_t j = 0; j < vSlices; j++) {
					l_vertData[vertIndex].pos = vec3(size*sinincl*cos(azimuth), size*cosincl, size*sinincl*sin(azimuth));
					l_vertData[vertIndex].norm = glm::normalize(l_vertData[vertIndex].pos);
					azimuth += hStep;
					vertIndex++;
				}
				inclination += vStep;
				azimuth = (i % 2 == 1 ? hStep / 2 : 0.f);
			}

			typedef uint16_t indexData_t;

			const indexData_t numPrimitives = vSlices * (2 + (hSlices - 1) * 2);
			IndexBufferPtr	ibuffer = new IndexBuffer();
			{
				dataBuffer_t dataOut = ibuffer->SetData(commands, BUH_STATIC_DRAW, 3 * numPrimitives, BGVT_U16);
				BASSERT(dataOut.dataSize == 3 * numPrimitives * sizeof(indexData_t));
				indexData_t* l_indexData = (indexData_t*)dataOut.data;
				{ //Index data
					//Pole Caps
					indexData_t currentIdx = BuildingPrimitives::ConeIndices(l_indexData, indexData_t(1), indexData_t(0), vSlices, true);
					//Rings
					indexData_t ringStartIndex = currentIdx;
					indexData_t vertStartId = 1;
					for (uint8_t ringIdx = 0; ringIdx < hSlices - 1; ringIdx++) {
						ringStartIndex += static_cast<indexData_t> (BuildingPrimitives::RingIndices(l_indexData + ringStartIndex, vertStartId, vSlices));
						vertStartId += vSlices;
					}
					//Pole Caps
					BuildingPrimitives::ConeIndices(l_indexData + ringStartIndex, vertStartId, indexData_t(l_numVertices - 1), vSlices, false);
				}
			}

			VertexLayout desc;
			desc.AddStream({ BGSS_POSITION, BGVT_FLOAT, 3 });
			desc.AddStream({ BGSS_NORMALS, BGVT_FLOAT, 3 });
			VertexBufferPtr vbuffer = new VertexBuffer();
			dataBuffer_t dataOut = vbuffer->SetData(commands, BUH_STATIC_DRAW, desc, l_numVertices);
			BASSERT(dataOut.dataSize == sizeof(PosNorm)*l_numVertices);
			memcpy(dataOut.data, l_vertData, sizeof(PosNorm)*l_numVertices);

			auto outPbuffer = new PrimitiveBuffer;
			outPbuffer->Build(commands, vbuffer, ibuffer, BGPT_TRIANGLES);
			return outPbuffer;
		}

		PrimitiveBufferPtr CylinderIndexed(RCB& commands, vec3 direction, vec3 biNorm, float diameter, float height, uint8_t hSlices, uint8_t vSlices)
		{
			using BuildingPrimitives::CircleVerticesNum;
			using BuildingPrimitives::CapIndicesNum;
			using BuildingPrimitives::RingIndicesNum;
			typedef uint16_t indexData_t;

			BASSERT(vSlices >= 3);//Less than 3 slices is a plane
			const indexData_t l_numVertices = indexData_t((2 + hSlices)*CircleVerticesNum(vSlices));

			BASSERT_MSG(l_numVertices == (2 + hSlices)*CircleVerticesNum(vSlices), "Vertex data does not fit in 16bit indices");

			direction = glm::normalize(direction);//Just in case
			PosNorm* l_vertData = new PosNorm[l_numVertices];
			indexData_t l_freeVertID = 0;
			const float radius = diameter * 0.5f;
			{//Generate vertex data by creating (2 + num of horizontal slices) concentric circles
				PosNorm* l_vertDataPos = l_vertData;
				vec3 vStep = (height / (hSlices + 1)) * direction;
				vec3 circlePos = -direction * (height*0.5f);
				for (unsigned hSlice = 0u; hSlice < (hSlices + 2u); ++hSlice) {
					l_freeVertID += static_cast<indexData_t> (BuildingPrimitives::CircleVertices(l_vertDataPos, l_vertData + l_numVertices,
						circlePos, direction, biNorm, radius, vSlices));

					l_vertDataPos += CircleVerticesNum(vSlices);
					circlePos += vStep;
				}
				BASSERT(l_vertDataPos == (l_vertData + l_numVertices));
			}
			BASSERT(l_freeVertID == l_numVertices);

			const indexData_t l_numIndices = static_cast<indexData_t> (2 * CapIndicesNum(vSlices) + (1 + hSlices) * RingIndicesNum(vSlices));
			IndexBufferPtr	ibuffer = new IndexBuffer();
			{
				dataBuffer_t dataOut = ibuffer->SetData(commands, BUH_STATIC_DRAW, l_numIndices, BGVT_U16);
				BASSERT(dataOut.dataSize == l_numIndices * sizeof(indexData_t));
				indexData_t* l_indexData = (indexData_t*)dataOut.data;
				{ //Index data
					//Top Cap
					indexData_t l_currentIdx = static_cast<indexData_t> (BuildingPrimitives::CapIndices(l_indexData, l_indexData + l_numIndices, indexData_t(0), vSlices));
					//Rings
					indexData_t l_ringStartID = 0;
					for (unsigned ringID = 0u; ringID < (1u + hSlices); ++ringID) {
						l_currentIdx += static_cast<indexData_t> (BuildingPrimitives::RingIndices(l_indexData + l_currentIdx, l_ringStartID, vSlices));
						l_ringStartID += static_cast<indexData_t> (CircleVerticesNum(vSlices));
					}
					BASSERT(l_ringStartID == (1 + hSlices) * CircleVerticesNum(vSlices));
					//Bottom Cap
					l_currentIdx += BuildingPrimitives::CapIndices(l_indexData + l_currentIdx, l_indexData + l_numIndices, l_ringStartID, vSlices, true);
					BASSERT(l_currentIdx == l_numIndices);
				}
			}

			VertexLayout desc;
			desc.AddStream({ BGSS_POSITION, BGVT_FLOAT, 3 });
			desc.AddStream({ BGSS_NORMALS, BGVT_FLOAT, 3 });
			VertexBufferPtr vbuffer = new VertexBuffer();
			dataBuffer_t dataOut = vbuffer->SetData(commands, BUH_STATIC_DRAW, desc, l_numVertices);
			BASSERT(dataOut.dataSize == sizeof(PosNorm)*l_numVertices);
			memcpy(dataOut.data, l_vertData, sizeof(PosNorm)*l_numVertices);

			auto outPbuffer = new PrimitiveBuffer;
			outPbuffer->Build(commands, vbuffer, ibuffer, BGPT_TRIANGLES);
			return outPbuffer;
		}

		PrimitiveBufferPtr GridIndexed(RCB& commands, int resolution)
		{
			const int numVerts = resolution * resolution;
			const int numSquares = resolution - 1;
			const int numIndices = numSquares * numSquares * 6;

			// verts
			VertexBufferPtr oVBuffer = new VertexBuffer;
			VertexLayout desc;
			desc.AddStream({ BGSS_POSITION, BGVT_FLOAT, 3 });
			dataBuffer_t dataOut = oVBuffer->SetData(commands, BUH_STATIC_DRAW, desc, numVerts);
			vec3* vertOut = (vec3*)dataOut.data;
			const float stepSize = 1.f / resolution;
			for (int i = 0; i < resolution; i++) {
				for (int j = 0; j < resolution; j++) {
					*vertOut = { i * stepSize, 0.f, j*stepSize};
					vertOut++;
				}
			}
			// indices 
			IndexBufferPtr oIBuffer = new IndexBuffer();
			dataBuffer_t indDataOut = oIBuffer->SetData(commands, BUH_STATIC_DRAW, numIndices, BGVT_U32);
			uint32_t* indOut = (uint32_t*)indDataOut.data;
			for (int i = 0; i < resolution - 1; i++) {
				for (int j = 0; j < resolution - 1; j++) {
					indOut[0] = i * resolution + j;
					indOut[1] = i * resolution + j + resolution;
					indOut[2] = i * resolution + j + resolution + 1;
					indOut[3] = i * resolution + j;
					indOut[4] = i * resolution + j + resolution + 1;
					indOut[5] = i * resolution + j + 1;
					indOut += 6;
				}
			}

			auto outPbuffer = new PrimitiveBuffer;
			outPbuffer->m_indexBuffer = oIBuffer;
			outPbuffer->Build(commands, oVBuffer, oIBuffer, BGPT_TRIANGLES);
			return outPbuffer;
		}

		PrimitiveBufferPtr FullscreenQuad(RCB& commands)
		{
			VertexLayout desc;
			desc.AddStream({ BGSS_POSITION, BGVT_FLOAT, 3 });
			float l_vertData[3 * 6] = {
				1.f, 1.f,0.f,
				-1.f,-1.f,0.f,
				1.f, -1.f,0.f,
				1.f, 1.f,0.f,
				-1.f, 1.f,0.f,
				-1.f,-1.f,0.f,
			};
			VertexBufferPtr vbuffer = new VertexBuffer();
			dataBuffer_t dataOut = vbuffer->SetData(commands, BUH_STATIC_DRAW, desc, 6);
			BASSERT(dataOut.dataSize == sizeof(l_vertData));
			memcpy(dataOut.data, l_vertData, sizeof(l_vertData));

			auto outPbuffer = new PrimitiveBuffer;
			outPbuffer->Build(commands, vbuffer, nullptr, BGPT_TRIANGLES);
			return outPbuffer;
		}

		PrimitiveBufferPtr FullscreenQuadWithUVS(RCB& commands)
		{
			VertexLayout desc;
			desc.AddStream({ BGSS_POSITION, BGVT_FLOAT, 3 });
			desc.AddStream({ BGSS_TEXTURE01, BGVT_FLOAT, 2 });
			float l_vertData[(3 + 2) * 6] = {
				1.f, 1.f,0.f,  1.f, 1.f,
				-1.f,-1.f,0.f, 0.f, 0.f,
				1.f, -1.f,0.f, 1.f, 0.f,
				1.f, 1.f,0.f,  1.f, 1.f,
				-1.f, 1.f,0.f, 0.f, 1.f,
				-1.f,-1.f,0.f, 0.f, 0.f
			};
			VertexBufferPtr vbuffer = new VertexBuffer();
			dataBuffer_t dataOut = vbuffer->SetData(commands, BUH_STATIC_DRAW, desc, 6);
			BASSERT(dataOut.dataSize == sizeof(l_vertData));
			memcpy(dataOut.data, l_vertData, sizeof(l_vertData));

			auto outPbuffer = new PrimitiveBuffer;
			outPbuffer->Build(commands, vbuffer, nullptr, BGPT_TRIANGLES);
			return outPbuffer;
		}

	}
}