#pragma once

#include <bolts_core.h>

namespace Bolts {

	class Camera : public RefCountT<Camera> {
	public:
		Camera();
		virtual ~Camera() {}

		void	CopyFrom(const Camera*);
		void	BlendFrom(const Camera*, float factor);

		void	SetPosition(const vec3 &newPos) {
			m_position = newPos;
			m_viewDirty = true;
		}
		void	SetTarget(const vec3 &newTgt) {
			m_target = newTgt;
			m_viewDirty = true;
		}
		void	SetUpVector(const vec3 &newUp) {
			m_up = newUp;
			m_viewDirty = true;
		}

		void SetCameraOrtho() { m_camIsOrtho = true; m_projectionDirty = true; }
		void SetCameraPersp() { m_camIsOrtho = false; m_projectionDirty = true; }
		bool IsCameraOrtho() const { return m_camIsOrtho; }

		float	GetFov() const { return p.m_fov; }
		float	GetFovDeg() const { return radToDeg(p.m_fov); }
		void	SetFov(float fovDegrees) {
			p.m_fov = degToRad(fovDegrees);
			m_projectionDirty = true;
		}
		void	SetOrthoData(float minX, float maxX, float minY, float maxY)
		{
			o.m_orthoHExtents.x = minX;
			o.m_orthoHExtents.y = maxX;
			o.m_orthoVExtents.x = minY;
			o.m_orthoVExtents.y = maxY;
			m_projectionDirty = true;
		}

		float	GetZNear() const { return m_zNear; }
		void	SetZNear(float znear) {
			m_zNear = znear;
			m_projectionDirty = true;
		}
		float	GetZFar() const { return m_zFar; }
		void	SetZFar(float zfar) {
			m_zFar = zfar;
			m_projectionDirty = true;
		}
		void	SetAspectRatio(float aspRatio) {
			p.m_aspectRatio = aspRatio;
			m_projectionDirty = true;
		}
		// Getters
		const mat4&	GetViewMatrix() const;
		const mat4&	GetInverseViewMatrix() const;
		const mat4&	GetProjectionMatrix() const;
		const mat4&	GetInverseProjectionMatrix() const;

		vec3	GetPosition() const { return m_position; }
		vec3	GetTarget() const { return m_target; }
		vec3	GetForward() const { return glm::normalize(m_target - m_position); }
		vec3	GetUp() const { return m_up; }

		// Movement
		void	MoveForward(float amount);
		void	MoveSideways(float amount);
		void	MoveUp(float amount);
		void	LookUp(float rads); // rotate around view right
		void	LookRight(float rads); // rotate around view up
		void	TiltRight(float rads); // rotate along view direction
		void	OrbitRight(float rads);
		void	OrbitUp(float rads);

		Segment	ProjectScreenCoords( vec2 normalizedScreenUV) const;
		void	ExtractFrustumWSCorners(vec3 corners[8]) const;
		vec4	TightEnclosingSphere(float zfar) const;
		vec3	SnapToViewSpaceGrid(vec3 worldPosition, vec2 gridSize) const;

		static void ExtractFrustumPlanes(const mat4& m, vec4(&planes)[6]); //NOTE: Planes are Left Right Up Down Near Far

		protected:
			void	UpdateViewMatrix() const;
			void	UpdateInverseViewMatrix() const;
			void	UpdateProjMatrix() const;

			mutable mat4	m_viewMatrix;
			mutable mat4	m_inverseViewMatrix;
			mutable mat4	m_projMatrix;
			mutable mat4	m_inverseProjMatrix;
			//View-related
			vec3			m_position;
			vec3			m_target;
			vec3			m_up;
			//Projection-related
			float			m_zNear;
			float			m_zFar;
			union {
				struct {
					float	m_fov;
					float	m_aspectRatio;
				} p;
				struct {
					vec2	m_orthoVExtents;
					vec2	m_orthoHExtents;
				} o;
			};
			bool			m_camIsOrtho = false;

			mutable bool	m_viewDirty;
			mutable bool	m_projectionDirty;
	};

	typedef intrusivePtr_t< Camera> CameraPtr;
	typedef intrusivePtr_t< const Camera> CameraConstPtr;
};
