#pragma once

#include <Assets/Assets.h>
#include <Assets/AnimationFile.h>
#include <bolts_core.h>

namespace Bolts
{
	namespace Rendering {

		class AnimationSet : public assets::assetMixin
		{
		public:
			static const int c_assetType = assets::EAT_ANIMATION_SET;
			typedef intrusivePtr_t<AnimationSet> Ptr;

			AnimationSet(assetID_t id);
			~AnimationSet();
			
			bool IsValid() const { return data != nullptr; }
			bool DrawProperties();
			void SetData(const AnimHeader* d) { data = d; }

			int						GetNumClips() const { return data->clips->count; }
			int						FindClip( hash32_t ) const;
			const AnimClip&			GetClipInfo(int clipIdx) const { return data->clips->Get(clipIdx); }

			const AnimTrack&		GetTrackData(int trackIdx) const { return data->tracks->Get(trackIdx); }
			const AnimTransform&	GetKeyTransform(int keyIdx) const { return data->transforms->Get(keyIdx); }

			int						GetNumBones() const { return data->boneNameHashes->count; }
			hash32_t				GetBoneNameHash(int boneIdx) const { return data->boneNameHashes->Get(boneIdx); }

		protected:
			const AnimHeader* data; /// non-owning reference to file data
		};
	};
};
