#pragma once

#include <Assets/Assets.h>
#include "Rendering/TextureFwd.h"
#include "Rendering/RenderBackend.h"
#include "Rendering/GPUProgramFwd.h"
#include <bolts_binaryserialization.h>
#include <bolts_assert.h>
#include <memory>

namespace Bolts {
	namespace Rendering {
		//> Describes the contents of a Constant Buffer
		struct CBufferDescription {

			CBufferDescription() = default;

			CBufferDescription(CBufferDescription&& other) {
				memcpy(this, &other, sizeof(CBufferDescription));
				other.storage = nullptr;
				other.storageSize = 0;
				other.numEntries = 0;
			}

			CBufferDescription& operator=(CBufferDescription&& other) {
				memcpy(this, &other, sizeof(CBufferDescription));
				other.storage = nullptr;
				other.storageSize = 0;
				other.numEntries = 0;
				return *this;
			}

			~CBufferDescription() {
				Clear();
			}

			void Clear();//> Frees buffer memory & resets the buffer to "unused"
			void SetSize(uint16_t numberOfEntries); //> Allocates memory to fit this number of uniforms
			void CopyFrom(const CBufferDescription& other) {
				memcpy(this, &other, sizeof(*this));
				storage = nullptr;
				storageSize = 0;
				SetSize(numEntries);
				memcpy(storage, other.storage, other.storageSize);
			}

			BOLTS_INLINE bool IsUsed() const { return numEntries > 0; }

			int FindParamIdx(hash32_t name) const {
				BASSERT(storage);
				for (int i = 0; i < numEntries; i++)
					if (names[i] == name)
						return i;
				return -1;
			}

			template<typename T>
			bool TypeMatch(int paramIdx, int arraySize = 1)
			{
				if (paramIdx < 0 || paramIdx >= numEntries)
					return false;
				return (arraySizes[paramIdx] == arraySize) && (types[paramIdx] == detail::TypeToSPT<T>::SPT);
			}

			hash32_t* names = nullptr;
			ShaderParamType* types = nullptr;
			uint16_t* arraySizes = nullptr; // Number of elements in array ( 0 for non-array elems )
			uint32_t* offsets = nullptr; //Offset from data buffer start to place value
			void* storage = nullptr;
			uint16_t storageSize = 0; // size of the description data buffer, not of the actual data
			uint16_t numEntries = 0;
			//
			hash32_t bufferName = 0;
			uint32_t exactDataSize = 0; // The size in bytes that data matching this description would take
			uint32_t bufferDataSize = 0; // The required size of the buffer data. additional padding may be added after data members
			char bindingLocation = -1; // The slot this buffer is bound to in the shader
		};

		//>Stores data for a cbuffer
		struct CBufferData {
			~CBufferData()
			{
				Clear();
			}

			void Clear() {
				free(data);
				data = nullptr;
			}

			void Resize(const CBufferDescription& desc)
			{
				if (dataSize < desc.bufferDataSize) {
					Clear();
					dataSize = desc.bufferDataSize;
					data = malloc(dataSize);
					memset(data, 0, dataSize);
				}
			}

			BOLTS_INLINE void* Store(const CBufferDescription& desc, int paramIdx) {
				return (char*)(data)+desc.offsets[paramIdx];
			}

			void* data = nullptr;
			size_t dataSize = 0;
		};

		class GPUProgram : public assets::assetMixin
		{
		public:
			static const int c_assetType = assets::EAT_GPU_PROGRAM;
			typedef typename intrusivePtr_t<GPUProgram> Ptr;
		public:
			static const int c_maxNumUniformBuffers = 7;

			GPUProgram(assetID_t rid):assetMixin(rid) {}
			~GPUProgram() {}
	
			bool DrawProperties();

			void QueueLoad(ResourceCommandBuffer& commands, Blob::Reader vsSource, Blob::Reader fsSource, const char* firstDefine, uint32_t definesSize) {
				if (!shaderHandle.IsValid()) {
					shaderHandle = commands.CreateResource(RenderResourceType::SHADER_PROGRAM);
				}
				auto cmd = commands.AddCommand<command::CompileAndLoadProgram>(shaderHandle);
				cmd->owningProgram = this;
				cmd->shaderCode[0] = (const char*)vsSource.Head();
				cmd->shaderCodeSize[0] = vsSource.Remaining();
				cmd->shaderTypes[0] = BGST_VERTEX;
				cmd->shaderCode[1] = (const char*)fsSource.Head();
				cmd->shaderCodeSize[1] = fsSource.Remaining();
				cmd->shaderTypes[1] = BGST_FRAGMENT;
				cmd->defines = firstDefine; // Sequence of defines, as null-terminated strings
				cmd->definesSize = definesSize;
			}

			void QueueLoadCompute(ResourceCommandBuffer& commands, const Blob& csSource, const char* defines) {
				if (!shaderHandle.IsValid()) {
					shaderHandle = commands.CreateResource(RenderResourceType::SHADER_PROGRAM);
				}
				auto cmd = commands.AddCommand<command::CompileAndLoadProgram>(shaderHandle);
				cmd->owningProgram = this;
				cmd->shaderCode[0] = (const char*)csSource.Data();
				cmd->shaderCodeSize[0] = csSource.Size();
				cmd->shaderTypes[0] = BGST_COMPUTE;
				cmd->defines = defines;
				cmd->definesSize = (unsigned)strlen(defines);
			}

			void OnLoadFinished() {
				// Count texture params
				int numTextures = 0;
				for (int i = 0; i < c_maxNumUniformBuffers; i++) {
					auto& cbuffer = cbuffers[i];
					if (cbuffer.IsUsed()) {
						for (int idx = 0; idx < cbuffer.numEntries; idx++) {
							if (cbuffer.types[idx] == SPT_TEX) {
								numTextures++;
							}
						}
					}
				}
				//Fill in texParamNames vec
				textureParamsNames.clear();
				textureParamsNames.reserve(numTextures);
				for (int i = 0; i < c_maxNumUniformBuffers; i++) {
					auto& cbuffer = cbuffers[i];
					if (cbuffer.IsUsed()) {
						for (int idx = 0; idx < cbuffer.numEntries; idx++) {
							if (cbuffer.types[idx] == SPT_TEX) {
								textureParamsNames.push_back(cbuffer.types[idx]);
							}
						}
					}
				}
				SetAssetState(assets::EAS_READY);
			}

			void Unload(ResourceCommandBuffer& commands) {
				commands.ReleaseResource(shaderHandle);
			}

			CBufferDescription	cbuffers[c_maxNumUniformBuffers];
			vec_t<hash32_t>		textureParamsNames; // Helper for determining used texture parameters
			resHandle_t			shaderHandle;
		};


	}
}