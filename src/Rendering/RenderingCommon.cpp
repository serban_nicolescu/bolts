#include "RenderingCommon.h"

void Bolts::Rendering::RenderState::SetBlendMode(BlendMode bm)
{
	switch (bm) {
	case BGBM_NONE:
		blendEq = BGBE_DISABLED;
		break;
	case BGBM_NORMAL:
		blendEq = BGBE_ADD;
		srcBlendFunc = BGBF_SRC_ALPHA;
		dstBlendFunc = BGBF_OM_SRC_ALPHA;
		break;
	case BGBM_NORMAL_PREMULT:
		blendEq = BGBE_ADD;
		srcBlendFunc = BGBF_ONE;
		dstBlendFunc = BGBF_OM_SRC_ALPHA;
		break;
	case BGBM_ADD:
		blendEq = BGBE_ADD;
		srcBlendFunc = BGBF_SRC_ALPHA;
		dstBlendFunc = BGBF_ONE;
		break;
	case BGBM_ADD_PREMULT:
		blendEq = BGBE_ADD;
		srcBlendFunc = BGBF_ONE;
		dstBlendFunc = BGBF_ONE;
		break;
	}
}
