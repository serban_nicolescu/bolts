#include "Rendering/MaterialNew.h"
#include <Rendering/GPUProgram.h>
#include <bolts_hash.h>

namespace Bolts {
	namespace Rendering {

		Material::Material(assetID_t id) :
			assetMixin(id)
		{
			BASSERT_MSG(id > 0, "Material ID is invalid when constructing material");
		}

		Material::~Material() {}

		void Material::CloneFrom(Material& other)
		{
			m_parameters = other.m_parameters;//TODO: This makes a deep copy of a vector. Should be more explicit
			m_rstate = other.m_rstate;
			m_gpuProgram = other.m_gpuProgram;
			m_gpuProgramId = other.m_gpuProgramId;
			m_baseMaterialId = other.m_baseMaterialId;
		}

		void Material::Unload()
		{
		}

		void Material::SetProgram(GPUProgramPtr newProgram)
		{
			BASSERT(m_gpuProgramId);
			BASSERT_MSG(newProgram, "nullptr passed as new Program Ptr");
			BASSERT(newProgram && (m_gpuProgramId == newProgram->GetAssetID()));
			m_gpuProgram = newProgram;
		}

		GPUProgram* Material::GetProgram() const
		{
			return m_gpuProgram.get();
		}

		void Material::SetProgramID(assetID_t id) 
		{
			if (m_gpuProgramId != id)
				m_gpuProgram = nullptr; // changing id means we need to wait to be given new shader
			m_gpuProgramId = id; 
		}

	};//Rendering
};//Bolts