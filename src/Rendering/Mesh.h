#pragma once

#include <Assets/Assets.h>
#include <Assets/MeshFile.h>
#include <Rendering/MeshFwd.h>
#include <Rendering/MaterialNewFwd.h>
#include <Rendering/GPUBuffers.h>
#include <Rendering/GPUProgram.h>
#include <bolts_core.h>

namespace Bolts
{
	namespace Rendering {
		//TODO: One Mesh can hold an entire scene
		//	Multiple prim-buffers, one for each vert layout used
		//	Sub-meshes: index into prim buffers + start & offset, mat & node idx
		//TODO: Should still allow easy way to make Mesh assets programatically
		//	Nodes structure makes this very complicated
		class Mesh : public assets::assetMixin
		{
		public:
			static const int c_assetType = assets::EAT_MESH;
			typedef intrusivePtr_t< Mesh> Ptr;

			Mesh(assetID_t id);
			~Mesh();
			
			bool IsValid();//TODO: Define what this means. Right now it's "my asset state is OK & all buffers are loaded and have valid materials"
			bool DrawProperties();
			void Unload(ResourceCommandBuffer&);

			struct SubMat
			{
				str_t					name;
				hash32_t				nameHash;
			};

			struct SubMesh{
				SubMesh();
				SubMesh(SubMesh&&);
				SubMesh& operator=(SubMesh&&);
				SubMesh(const SubMesh&);
				SubMesh& operator=(const SubMesh&);
				~SubMesh();
				SubMesh(Rendering::PrimitiveBufferPtr, regHash_t);


				PrimitiveBufferPtr		primitiveBuffer;
				AABB					aabb;
				regHash_t				name;
				uint32_t				nodeIdx = 0;
				uint32_t				matIdx = 0;
			};

			vec_t<SubMat>		materials;
			vec_t<SubMesh>		subMeshes;
			AABB				aabb;
			Blob				nodeData;
			MeshNodes*			nodes = nullptr; //NOTE: points to nodeData
		};
	};
};
