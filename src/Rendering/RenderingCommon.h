#pragma once

#include <bolts_core.h>
#include "Rendering/TextureFwd.h"

namespace Bolts {
	namespace Rendering {

		enum GPUPrimitiveType{
			BGPT_TRIANGLES = 0, BGPT_TRIANGLE_STRIP, BGPT_TRIANGLE_FAN, BGPT_LINES, BGPT_LINE_STRIP, BGPT_POINTS, _BGPT_COUNT,BGPT_INVALID
		};

		enum CullingMode{
			BGCM_NONE = 0, BGCM_FRONT, BGCM_BACK, BGCM_BOTH, _BGCM_COUNT
		};

		enum BlendEq {
			BGBE_DISABLED, BGBE_ADD, BGBE_SUBTRACT, BGBE_REVERSE_SUBTRACT, BGBE_MIN, BGBE_MAX, _BGBE_COUNT
		};

		enum BlendFunc	{
			BGBF_ZERO, BGBF_ONE, BGBF_SRC_ALPHA, BGBF_OM_SRC_ALPHA, _BGBF_COUNT
		};

		enum ClearFlags {
			FLAG_COLOR = 1, 
			FLAG_DEPTH = 2, 
			FLAG_STENCIL = 4,
			FLAG_COLOR_DEPTH = 3,
			FLAG_ALL = 7
		};

		enum BlendMode {
			BGBM_NONE,
			BGBM_NORMAL,
			BGBM_NORMAL_PREMULT,
			BGBM_ADD,
			BGBM_ADD_PREMULT
		};

		struct RenderState
		{
			CullingMode		cullingMode = BGCM_BACK;
			BlendEq			blendEq = BGBE_DISABLED;
			BlendFunc		srcBlendFunc = BGBF_SRC_ALPHA;
			BlendFunc		dstBlendFunc = BGBF_OM_SRC_ALPHA;
			bool			isDepthTestEnabled = true;
			bool			isDepthWriteEnabled = true;

			void			SetBlendMode(BlendMode bm);
		};

		// Resources
		class ResourceCommandBuffer;
		typedef ResourceCommandBuffer RCB;

		enum RenderResourceType {
			SHADER_PROGRAM,
			GENERIC_BUFFER,
			IMAGE_BUFFER,
			FRAMEBUFFER,
			PRIMITIVE_BUFFER, // A set of vertex, index and vertex descriptions. A VAO in OGL terms
			_COUNT
		};

		struct resHandle_t {
			uint32_t d = 0;

			inline bool operator==(const resHandle_t& r) const { return r.d == d; }
			inline bool operator!=(const resHandle_t& r) const { return r.d != d; }

			uint32_t GetIndex() const { return d & 0x00FFFFFF; }
			RenderResourceType GetType() const { return RenderResourceType(d >> 24); }
			bool IsValid() const { return d != 0; }
			void Invalidate() { d = 0; }

			static resHandle_t Make(RenderResourceType type, int index) { return resHandle_t{ uint32_t(type) << 24 | index }; }
		};

		struct dataBuffer_t
		{
			template<typename T>
			void CopyFrom(const vec_t<T>& v) {
				memcpy(data, v.data(), v.size() * sizeof(T));
			}

			void* data;
			size_t dataSize;
		};

		// Buffers (Index,Vertex,Constant)

		enum BufferType: uint8_t {
			BUFFER_VERTEX, BUFFER_INDEX, BUFFER_CONSTANT, _BUFFER_COUNT
		};

		enum BufferUsageHint {
			BUH_STATIC_DRAW, BUH_STATIC_READ, BUH_STATIC_COPY,
			BUH_DYNAMIC_DRAW, BUH_DYNAMIC_READ, BUH_DYNAMIC_COPY,
			BUH_STREAM_DRAW, BUH_STREAM_READ, BUH_STREAM_COPY,
			_BUH_COUNT
		};

		enum BufferVarType: uint8_t {
			BGVT_FLOAT, BGVT_I8, BGVT_U8, BGVT_I16, BGVT_U16, BGVT_I32, BGVT_U32, _BGVT_LAST
		};

		enum StreamSemantic: uint8_t {
			BGSS_INVALID = 0, BGSS_POSITION, BGSS_NORMALS, BGSS_TANGENTS, BGSS_COLOR0, BGSS_COLOR1, BGSS_TEXTURE01, BGSS_TEXTURE23, BGSS_BONEI, BGSS_BONEW, _BGSS_LAST_SEMANTIC
		};

		// Shaders

		enum ShaderType {
			BGST_VERTEX = 0, BGST_FRAGMENT, BGST_GEOMETRY, BGST_COMPUTE, _BGST_COUNT
		};

		enum ShaderParamType:uint8_t { SPT_FLOAT = 0, SPT_DOUBLE, SPT_INT32, SPT_VEC2, SPT_VEC3, SPT_VEC4, SPT_MAT4, SPT_MAT34, SPT_MAT3, SPT_TEX, _SPT_COUNT };

		extern const char* const c_SPTNames[_SPT_COUNT];

		namespace detail {

			// Type -> Enum
			template<ShaderParamType PT>
			struct TypeToSPTBind {
				static const ShaderParamType SPT = PT;
			};
			template<typename T>
			struct TypeToSPT {};
			template<> 
			struct TypeToSPT<float>:public TypeToSPTBind<SPT_FLOAT>	{};
			template<>
			struct TypeToSPT<double> :public TypeToSPTBind<SPT_DOUBLE> {};
			template<>
			struct TypeToSPT<vec2> :public TypeToSPTBind<SPT_VEC2> {};
			template<>
			struct TypeToSPT<vec3> :public TypeToSPTBind<SPT_VEC3> {};
			template<>
			struct TypeToSPT<vec4> :public TypeToSPTBind<SPT_VEC4> {};
			template<>
			struct TypeToSPT<mat3> :public TypeToSPTBind<SPT_MAT3> {};
			template<>
			struct TypeToSPT<mat34> :public TypeToSPTBind<SPT_MAT34> {};
			template<>
			struct TypeToSPT<mat4> :public TypeToSPTBind<SPT_MAT4> {};
			template<>
			struct TypeToSPT<int32_t> :public TypeToSPTBind<SPT_INT32> {};
			template<>
			struct TypeToSPT<resHandle_t> :public TypeToSPTBind<SPT_TEX> {};

			// Enum -> Type
			template<typename T>
			struct SPTToTypeBind {
				typedef T t;
			};
			template<ShaderParamType PT>
			struct SPTToType {};
			template<>
			struct SPTToType<SPT_FLOAT>:public SPTToTypeBind<float> {};
			template<>
			struct SPTToType<SPT_DOUBLE> :public SPTToTypeBind<double> {};
			template<>
			struct SPTToType<SPT_VEC2> :public SPTToTypeBind<vec2> {};
			template<>
			struct SPTToType<SPT_VEC3> :public SPTToTypeBind<vec3> {};
			template<>
			struct SPTToType<SPT_VEC4> :public SPTToTypeBind<vec4> {};
			template<>
			struct SPTToType<SPT_MAT3> :public SPTToTypeBind<mat3> {};
			template<>
			struct SPTToType<SPT_MAT34> :public SPTToTypeBind<mat34> {};
			template<>
			struct SPTToType<SPT_MAT4> :public SPTToTypeBind<mat4> {};
			template<>
			struct SPTToType<SPT_INT32> :public SPTToTypeBind<int32_t> {};
			template<>
			struct SPTToType<SPT_TEX> :public SPTToTypeBind<resHandle_t> {};

			// Size in bytes of value of corresponding SPT type
			static const char c_typeSize[] = {
				sizeof(SPTToType<ShaderParamType(0)>::t),
				sizeof(SPTToType<ShaderParamType(1)>::t),
				sizeof(SPTToType<ShaderParamType(2)>::t),
				sizeof(SPTToType<ShaderParamType(3)>::t),
				sizeof(SPTToType<ShaderParamType(4)>::t),
				sizeof(SPTToType<ShaderParamType(5)>::t),
				sizeof(SPTToType<ShaderParamType(6)>::t),
				sizeof(SPTToType<ShaderParamType(7)>::t),
				sizeof(SPTToType<ShaderParamType(8)>::t),
				sizeof(SPTToType<ShaderParamType(9)>::t),
			};

			static_assert(_SPT_COUNT == countof(c_typeSize), "resize this");
				
		};

		// Queries
		enum QueryType { BQT_TIMESTAMP };

		typedef uint64_t queryId_t;
		typedef uint64_t queryResult_t;

		// Settings structs
		struct ShadowMapSettings
		{
			int resolution = 1024;
			TextureFormat format = BTF_DEPTH16;
			bool resChanged = false;
			float depthBias = 1.f;
			float slopeBias = 2.f;
			float shadowDistance = 30.f;
			bool useAutoShadowFrustum = true;
			bool useTightFrustumFit = false;
		};
	};

	template<>
	struct is_pod< Rendering::resHandle_t> : public std::true_type {};
};
