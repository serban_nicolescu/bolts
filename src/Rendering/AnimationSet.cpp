#pragma once

#include <Assets/Assets.h>
#include <Assets/MeshFile.h>
#include <Rendering/MeshFwd.h>
#include <Rendering/MaterialNewFwd.h>
#include <Rendering/GPUBuffers.h>
#include <Rendering/GPUProgram.h>
#include <bolts_core.h>
#include "AnimationSet.h"

namespace Bolts
{
	namespace Rendering {

		AnimationSet::AnimationSet(assetID_t id):assetMixin(id)
		{
		}
		AnimationSet::~AnimationSet()
		{
		}

		int AnimationSet::FindClip(hash32_t h) const
		{
			if (!IsValid())
				return -1;

			const auto& clips = *data->clips;
			for (unsigned i = 0; i < clips.count; i++)
				if (clips[i].nameHash == h)
					return i;
			return -1;
		}
	};
};
