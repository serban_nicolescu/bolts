#include "Rendering/Texture.h"
#include <bolts_assert.h>

namespace Bolts
{
	namespace Rendering
	{

		static const int c_btfCount = 18;
		static_assert(c_btfCount == _BTF_COUNT, "You need to update the array below with the right info for the new format");
		
		static const unsigned g_texFormatImplicitBPP[c_btfCount] = {
			24, //RGB
			32, //RGBA
			8,	//R
			16,	//RG
			sizeof(float), //BTF_DEPTH16
			sizeof(float), //BTF_DEPTH24
			sizeof(float),// BTF_DEPTH32
			sizeof(float),// BTF_DEPTH32F
			4,//BTF_DXT1_RGB
			8,//BTF_DXT3_RGBA
			8,//BTF_DXT5_RGBA
			4,//BTF_BC4_R
			8,//BTF_BC5_RG
			2 * 1 * 8,//BTF_R16F
			2 * 2 * 8,//BTF_RG16F
			2 * 3 * 8,//BTF_RGB16F
			2 * 4 * 8,//BTF_RGBA16F
			32,//BTF_R11G11B10F
		};

		static const unsigned g_texFormatNumChannels[c_btfCount] = {
			3, //RGB
			4, //RGBA
			1,	//R
			2,	//RG
			1, //BTF_DEPTH16
			1, //BTF_DEPTH24
			1,// BTF_DEPTH32
			1,// BTF_DEPTH32F
			3,//BTF_DXT1_RGB
			4,//BTF_DXT3_RGBA
			4,//BTF_DXT5_RGBA
			1,//BTF_BC4_R
			2,//BTF_BC5_RG
			1,//BTF_R16F
			2,//BTF_RG16F
			3,//BTF_RGB16F
			4,//BTF_RGBA16F
			3,//BTF_R11G11B10F
		};

		const char* c_texFormatNames[c_btfCount] = {
			"RGB",
			"RGBA",
			"R",
			"RG",
			"DEPTH16",
			"DEPTH24",
			"DEPTH32",
			"DEPTH32F",
			"DXT1_RGB",
			"DXT3_RGBA",
			"DXT5_RGBA",
			"BC4_R",
			"BC5_RG",
			"R16F",
			"RG16F",
			"RGB16F",
			"RGBA16F",
			"R11G11B10F",
		};

		static_assert(4 == _BTT_COUNT, "You need to update the array below with the right info for the new format");
		const char* c_texTypeNames[4] = {
			"1D",
			"2D",
			"3D",
			"CUBE",
		};

		static const int c_btdfCount = 6;
		static_assert(c_btdfCount == _BTDF_COUNT, "You need to update the array below with the right info for the new format");

		static const unsigned g_texDataFormatChannelBits[c_btfCount] = {
			0 , //BTDF_IMPLICIT
			32, //BTDF_FLOAT
			16, //BTDF_HALF_FLOAT
			8 , //BTDF_UBYTE
			16, //BTDF_USHORT
			32, //BTDF_UINT
		};

		unsigned GetMipCount(uvec3 dimensions)
		{
			auto d = dimensions;
			unsigned maxDim = (d.x > d.y && d.x > d.z ? d.x : (d.y > d.z ? d.y : d.z));
			unsigned r = 1;
			while (maxDim >>= 1)
				r++;
			return r;
		}

		unsigned GetMipSizeInBytes(TextureFormat format, TextureDataFormat dataFormat, uvec3 mipSize)
		{
			if (IsCompressedFormat(format)) {
				BASSERT(dataFormat == BTDF_IMPLICIT);//Can't convert to compressed formats at runtime
				BASSERT(mipSize.z == 1);
				//NOTE: Only implemented to match DXT compression
				//NOTE: 2D only. 4x4 block compression
				const unsigned blockSize = 2 * g_texFormatImplicitBPP[format]; //In bytes
				return ((mipSize.x + 3) / 4)*((mipSize.y + 3) / 4)*blockSize;
			} else {
				unsigned bpp = g_texFormatImplicitBPP[format];
				if (dataFormat != BTDF_IMPLICIT)
					bpp = g_texFormatNumChannels[format] * g_texDataFormatChannelBits[dataFormat];
				return (mipSize.x * mipSize.y * mipSize.z * bpp) / 8;
			}
		}

		Texture::Texture(assetID_t rid):assetMixin(rid)
		{
		}

		Texture::~Texture() {
			BASSERT(!m_handle.IsValid());
		}

		void Texture::Init(RCB & commands)
		{
			BASSERT(!m_handle.IsValid());
			if (!m_handle.IsValid()) {
				m_handle = commands.CreateResource(IMAGE_BUFFER);
			}
		}


		void Texture::SetRTTexture(RCB& commands, TextureType type, uvec3 dim, TextureFormat format)
		{
			BASSERT(m_handle.IsValid());
			SetStuffInternal(type, dim, format);
			m_hasMips = false;
			m_memUsage = GetMipSizeInBytes(format, BTDF_IMPLICIT, dim);
			UpdateDataInternal(commands, nullptr, 0, BTDF_IMPLICIT);
			SetAssetState(assets::EAS_READY);
		}

		dataBuffer_t Texture::SetTexture(RCB& commands, TextureType type, uvec3 dim, TextureFormat format, TextureDataFormat dataFormat, bool withMips)
		{
			using namespace Rendering;
			BASSERT(m_handle.IsValid());
			SetStuffInternal(type, dim, format);
			m_hasMips = withMips;
			size_t dataSize = 0;

			const unsigned mipCount = withMips ? GetMipCount(dim) : 1;
			for (unsigned i = 0; i < mipCount; i++) {
				const uvec3 mipDim = GetMipRes(dim, i);
				dataSize += GetMipSizeInBytes(format, dataFormat, mipDim);
			}
			if (m_type == BTT_CUBE)
				dataSize *= 6;
			void* dataStorage = nullptr;
			//if (willStream) {
			//	if (m_dataSize != dataSize) {
			//		//TODO: Assert that this isn't called twice in one frame, since we'd be releasing mem used in a command
			//		ReleaseData();
			//		dataStorage = malloc(dataSize);
			//		m_dataSize = dataSize;
			//		m_data = dataStorage;
			//	}
			//}
			//else {
			dataStorage = commands.AllocScratchMemory(m_handle, dataSize);
			//}
			m_memUsage = dataSize;
			// Command
			UpdateDataInternal(commands, dataStorage, dataSize, dataFormat);

			return { dataStorage, dataSize };
		}

		void Texture::SetSampler(RCB& commands, TextureSamplerSettings newSettings)
		{
			BASSERT(m_handle.IsValid());
			auto cmd = commands.AddCommand<command::UpdateTextureFiltering>(m_handle);
			cmd->newSettings = newSettings;
			m_defaultSettings = newSettings;
		}

		void Texture::GenerateMipmaps(RCB& commands)
		{
			commands.AddCommand<command::GenerateTextureMips>(m_handle);
			m_hasMips = true;
		}

		void Texture::SetStuffInternal(TextureType type, uvec3 dim, TextureFormat format)
		{
			m_type = type;
			m_dimensions = dim;
			m_format = format;
			m_hasMips = false;
		}

		void Texture::UpdateDataInternal(RCB& commands, void * data, size_t size, TextureDataFormat dataFormat)
		{
			auto cmdPtr = commands.AddCommand<command::UpdateTextureData>(m_handle);
			cmdPtr->dimensions = m_dimensions;
			cmdPtr->type = m_type;
			cmdPtr->format = m_format;
			cmdPtr->dataPtr = data;
			cmdPtr->dataSize = size;
			cmdPtr->dataFormat = dataFormat;
		}

		void Texture::Unload(RCB& commands)
		{
			commands.ReleaseResource(m_handle);
		}

	}
}