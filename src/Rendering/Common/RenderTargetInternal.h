#pragma once

#include "Rendering/TextureFwd.h"
#include <bolts_core.h>
#include <vector>

namespace Bolts{
	namespace Rendering{
	//TODO: Move to detail namespace
	//TODO: Need to set filtering to nearest when textures are used as RTs
	//		But restore it afterwards
class RenderTargetInternal: public Bolts::RefCountT
{
public:
	struct RTSetup{
		std::vector< TexturePtr> colorRTTextures;
		uvec2		textureDimensions; 
		TexturePtr	depthRTTexture;
		bool		withDepth;
	};

	RenderTargetInternal();

	void			Setup( RTSetup&&);
	void			Resize(uvec2 newSize);
	TextureWeakPtr	GetColorTexture(unsigned index);
	TextureWeakPtr	GetDepthTexture();
	uvec2			GetResolution() { return m_resolution; }

	bool			IsValid() const {};//Always overwritten by implementation. Here as reference
	void			Clear();//Always overwritten by implementation. Here as reference
protected:
	std::vector< TexturePtr>	m_colorTextures;
	uvec2						m_resolution;
	TexturePtr					m_depthTexture;
};

	};//Rendering
};//Bolts
