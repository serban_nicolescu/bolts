#pragma once

#include "Rendering/TextureFwd.h"
#include "Rendering/RenderBackend.h"
#include <bolts_core.h>

namespace Bolts{
namespace Rendering{

	//TODO: IMPORTANT this thing leaks Texture references. they are never released
	class MatParameterHolder
	{
	public:
		typedef uint16_t	dataOffset_t; // max 16kb
		struct Info {
			hash32_t		key;
			dataOffset_t	dataOffset;
			ShaderParamType	type;
			uint8_t			_padding;
		};
		typedef vec_t<Info> infoArray_t;

		struct TextureParam
		{
			TextureWeakPtr			tex;// = nullptr; only used by UI code
			resHandle_t				handle;
			TextureSamplerSettings	sampler;
			hash32_t				texAssetID; // only used for loading
			bool					useDefault;// = true;
		};

		MatParameterHolder() = default;
		MatParameterHolder(const MatParameterHolder& other) = default;
		MatParameterHolder& operator=(const MatParameterHolder&) = default;
		MatParameterHolder(MatParameterHolder&& other) = default;
		MatParameterHolder& operator=(MatParameterHolder&&) = default;

		void		Clear();

		void		Set(regHash_t paramName, int32_t value);
		void		Set(regHash_t paramName, float value);
		void		Set(regHash_t paramName, double value);
		void		Set(regHash_t paramName, vec2 value);
		void		Set(regHash_t paramName, vec3 value);
		void		Set(regHash_t paramName, vec4 value);
		void		Set(regHash_t paramName, const mat3& value);
		void		Set(regHash_t paramName, const mat4& value);
		void		Set(regHash_t paramName, const mat34& value);
		void		Set(regHash_t paramName, Texture* tex); //BUG: holds reference forever
		void		Set(regHash_t paramName, TexturePtr& tex) { Set(paramName, tex.get()); }
		void		Set(regHash_t paramName, Texture* tex, TextureSamplerSettings sampler);
		void		Set(regHash_t paramName, TextureSamplerSettings paramValue);

		//const TexParamInfo& GetTex(ParamData paramIdx) const;

		// direct data access
		void		SetTexToBeLoaded_Internal(regHash_t paramName, hash32_t texAsset);
		hash32_t	GetTexAssetID_Internal(dataOffset_t dataOffset);
		void*		EditDataUnsafe(dataOffset_t offset) { return m_data.data() + offset; }
		const void*	GetDataUnsafe(dataOffset_t offset) const { return m_data.data() + offset; }
		const infoArray_t&	GetInfo() const { return m_params; };

		BOLTS_INLINE const Info* TryGetInfo(stringHash_t paramName) const {
			auto info = m_params.data();
			auto end = info + m_params.size();
			while (info < end) {
				if (info->key == paramName.m_hash)
					return info;
				info++;
			}
			return nullptr;
		}

		template<typename T>
		BOLTS_INLINE T* TryEdit(stringHash_t paramName) {
			if (auto pInfo = TryGetInfo(paramName)) {
				if (pInfo->type == detail::TypeToSPT<T>::SPT)
					return reinterpret_cast<T*> (EditDataUnsafe(pInfo->dataOffset));
			}
			return nullptr;
		}

		template<typename T>
		BOLTS_INLINE const T* TryGet(stringHash_t paramName) const {
			if (auto pInfo = TryGetInfo(paramName)) {
				if (pInfo->type == detail::TypeToSPT<T>::SPT)
					return reinterpret_cast<const T*> (GetDataUnsafe(pInfo->dataOffset));
			}
			return nullptr;
		}

	private:
		template <typename T>
		BOLTS_INLINE T* SetImpl(hash32_t paramName, bool& wasAdded);

		template <typename T>
		BOLTS_INLINE void SetByValue(hash32_t paramName, T val);
		template <typename T>
		BOLTS_INLINE void SetByRef(hash32_t paramName, const T& val);

		infoArray_t		m_params;
		vec_t<uint8_t>	m_data;
	};

	namespace detail {
	template<>
	struct TypeToSPT< MatParameterHolder::TextureParam> :public TypeToSPTBind<SPT_TEX> {};
	};
};
};
