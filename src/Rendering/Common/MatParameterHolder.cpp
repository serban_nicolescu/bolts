#include "MatParameterHolder.h"
#include "Rendering/Texture.h"
#include "Rendering/RenderingCommon.h"
#include <bolts_helpers.h>

using namespace Bolts;
using namespace Bolts::Rendering;

template<typename T>
inline BOLTS_INLINE T* MatParameterHolder::SetImpl(hash32_t nameHash, bool& wasAdded)
{
	uint8_t* pData{};
	if (auto pInfo = TryGetInfo(nameHash)) {
		BASSERT_MSG(pInfo->type == detail::TypeToSPT<T>::SPT, "Attempt to set material parameter of wrong type");
		pData = m_data.data() + pInfo->dataOffset;
		wasAdded = false;
	}
	else {
		Info info;
		info.key = nameHash;
		info.type = detail::TypeToSPT<T>::SPT;
		// allocate data
		pData = stack_addsize(m_data, sizeof(T));
		info.dataOffset = dataOffset_t(pData - m_data.data());
		m_params.emplace_back(info);
		wasAdded = true;
	}
	//NOTE: This is really bad. No alignment considerations ...
	//TODO: alignment bug
	return reinterpret_cast<T*>(pData);
}

template<typename T>
BOLTS_INLINE void MatParameterHolder::SetByValue(hash32_t paramName, T value)
{
	bool wasAdded;
	memcpy(SetImpl<T>(paramName, wasAdded), &value, sizeof(value));
}

template<typename T>
BOLTS_INLINE void MatParameterHolder::SetByRef(hash32_t paramName, const T& value)
{
	bool wasAdded;
	memcpy(SetImpl<T>(paramName, wasAdded), &value, sizeof(value));
}


void MatParameterHolder::Set(regHash_t paramName, int32_t value)
{
	SetByValue(paramName, value);
}

void MatParameterHolder::Set(regHash_t paramName, float value)
{
	SetByValue(paramName, value);
}

void MatParameterHolder::Set(regHash_t paramName, double value)
{
	SetByValue(paramName, value);
}

void Bolts::Rendering::MatParameterHolder::Set(regHash_t paramName, vec2 value)
{
	SetByValue(paramName, value);
}

void Bolts::Rendering::MatParameterHolder::Set(regHash_t paramName, vec3 value)
{
	SetByValue(paramName, value);
}

void Bolts::Rendering::MatParameterHolder::Set(regHash_t paramName, vec4 value)
{
	SetByValue(paramName, value);
}

void Bolts::Rendering::MatParameterHolder::Set(regHash_t paramName, const mat3 & value)
{
	SetByRef(paramName, value);
}

void Bolts::Rendering::MatParameterHolder::Set(regHash_t paramName, const mat4 & value)
{
	SetByRef(paramName, value);
}

void Bolts::Rendering::MatParameterHolder::Set(regHash_t paramName, const mat34 & value)
{
	SetByRef(paramName, value);
}

void MatParameterHolder::Set(regHash_t paramName, Texture* texPtr)
{
	BASSERT(texPtr);
	bool wasAdded;
	TextureParam& texInfo = *SetImpl<TextureParam>(paramName, wasAdded);
	if (wasAdded) {
		//manually adjust refcounts
		texPtr->onReferenceAdd();
		texInfo = TextureParam{ texPtr, texPtr->GetHandle(),{}, texPtr->GetAssetID(), true };
	}
	else 
	{
		if (texInfo.tex != texPtr) {
			//manually adjust refcounts
			if (texInfo.tex)
				texInfo.tex->onReferenceRelease();
			texPtr->onReferenceAdd();
			texInfo.tex = texPtr;
		}
		texInfo.handle = texPtr->GetHandle();
		texInfo.texAssetID = texPtr->GetAssetID();
	}
}

void MatParameterHolder::Set(regHash_t paramName, Texture* texPtr, TextureSamplerSettings samplerSettings)
{
	BASSERT(texPtr);
	bool wasAdded;
	TextureParam& texInfo = *SetImpl<TextureParam>(paramName, wasAdded);
	if (wasAdded) {
		//manually adjust refcounts
		texPtr->onReferenceAdd();
		texInfo = TextureParam{ texPtr, texPtr->GetHandle(), samplerSettings, texPtr->GetAssetID(), false};
	}
	else
	{
		if (texInfo.tex != texPtr) {
			//manually adjust refcounts
			if (texInfo.tex)
				texInfo.tex->onReferenceRelease();
			texPtr->onReferenceAdd();
			texInfo.tex = texPtr;
		}
		texInfo.handle = texPtr->GetHandle();
		texInfo.texAssetID = texPtr->GetAssetID();
		texInfo.sampler = samplerSettings;
		texInfo.useDefault = false;
	}
}

void MatParameterHolder::Set(regHash_t paramName, TextureSamplerSettings samplerSettings)
{
	bool wasAdded;
	TextureParam& texInfo = *SetImpl<TextureParam>(paramName, wasAdded);
	if (wasAdded) {
		texInfo = TextureParam{ nullptr, {}, samplerSettings, 0, false };
	}
	else
	{
		texInfo.sampler = samplerSettings;
		texInfo.useDefault = false;
	}
}

void Bolts::Rendering::MatParameterHolder::SetTexToBeLoaded_Internal(regHash_t paramName, hash32_t texAsset)
{
	bool wasAdded;
	TextureParam& texInfo = *SetImpl<TextureParam>(paramName, wasAdded);
	if (wasAdded) {
		texInfo = TextureParam{ nullptr, {}, {}, texAsset, true};
	}
	else
	{
		texInfo.texAssetID = texAsset;
	}
}

hash32_t Bolts::Rendering::MatParameterHolder::GetTexAssetID_Internal(dataOffset_t dataOffset)
{
	return reinterpret_cast<TextureParam*>(m_data.data() + dataOffset)->texAssetID;
}

void MatParameterHolder::Clear()
{
	m_params.clear();
	m_data.clear();
}
