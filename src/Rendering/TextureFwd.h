#pragma once
#include <bolts_core.h>

namespace Bolts {
	namespace Rendering {

	enum TextureFormat {
		BTF_RGB, BTF_RGBA, BTF_R, BTF_RG,
		BTF_DEPTH16, BTF_DEPTH24, BTF_DEPTH32, BTF_DEPTH32F,
		BTF_DXT1_RGB, BTF_DXT3_RGBA, BTF_DXT5_RGBA, BTF_BC4_R, BTF_BC5_RG,
		_BTF_FIRST_COMPRESSED = BTF_DXT1_RGB, _BTF_LAST_COMPRESSED = BTF_BC5_RG,
		BTF_R16F, BTF_RG16F, BTF_RGB16F, BTF_RGBA16F, BTF_R11G11B10F,
		_BTF_COUNT
	};
	extern const char* c_texFormatNames[_BTF_COUNT];

	enum TextureDataFormat {
		BTDF_IMPLICIT, BTDF_FLOAT, BTDF_HALF_FLOAT, BTDF_UBYTE, BTDF_USHORT, BTDF_UINT, _BTDF_COUNT
	};

	enum TextureType{
		BTT_1D, BTT_2D, BTT_3D, BTT_CUBE, _BTT_COUNT
	};
	extern const char* c_texTypeNames[_BTT_COUNT];

	enum class TextureFilterType :unsigned char {
		BTFT_NONE = 0, BTFT_NEAREST, BTFT_LINEAR, _BTFT_COUNT, BTFT_ANISO
	};

	enum class TextureWrapMode : unsigned char{
		BTWM_CLAMP_EDGE = 0, BTWM_REPEAT, BTWM_CLAMP_BORDER, BTWM_REPEAT_MIRROR, _BTWM_COUNT
	};

	enum class TextureCompareFunc : unsigned char {
		BTCF_OFF = 0, BTCF_LESS, BTCF_LEQUAL, BTCF_GREATER, BTCF_GEQUAL, BTCF_EQUAL, BTCF_NEQUAL, BTCF_ALWAYS, _BTCF_COUNT
	};

	enum TextureUsage{
		BTU_STATIC, //Only set once, read often
		BTU_DYNAMIC,//Changed every frame
		_BTU_COUNT
	};

	struct TextureSamplerSettings {
		TextureFilterType	filteringType = TextureFilterType::BTFT_NEAREST;
		TextureFilterType	mipFilteringType = TextureFilterType::BTFT_NONE;
		TextureWrapMode		wrapMode = TextureWrapMode::BTWM_CLAMP_EDGE;
		TextureCompareFunc	texCompareMode = TextureCompareFunc::BTCF_OFF;
	};

	inline bool IsCompressedFormat(TextureFormat tf)
	{
		return (tf >= _BTF_FIRST_COMPRESSED && tf <= _BTF_LAST_COMPRESSED);
	}

	inline uvec2	GetNextMipRes(uvec2 in) {
		return uvec2(in.x / 2, in.y / 2);
	}

	inline uvec3	GetNextMipRes(uvec3 in) {
		return uvec3(in.x / 2, in.y / 2, in.z / 2);
	}

	inline uvec3 GetMipRes(uvec3 level0Res, unsigned mipLevel)
	{
		uvec3 mipRes;
		mipRes.x = level0Res.x >> mipLevel;
		mipRes.y = level0Res.y >> mipLevel;
		mipRes.z = level0Res.z >> mipLevel;
		mipRes.x = (mipRes.x ? mipRes.x : 1);
		mipRes.y = (mipRes.y ? mipRes.y : 1);
		mipRes.z = (mipRes.z ? mipRes.z : 1);
		return mipRes;
	}

	unsigned GetMipCount(uvec3 dimensions);
	///> Returns the storage size of source texture data, not the internal format stored on the GPU
	unsigned GetMipSizeInBytes(TextureFormat format, TextureDataFormat dataFormat, uvec3 dimensions); 

	typedef int				textureUnitIdx_t;//TODO: This doesn't belong here

	class Texture;

	typedef intrusivePtr_t<Texture>	TexturePtr;
	typedef Texture*				TextureWeakPtr;

	};
};