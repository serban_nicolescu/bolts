#include "GPUBuffers.h"
#include "RenderBackend.h"

namespace Bolts {
	namespace Rendering {

		static constexpr uint8_t c_GPUVarSize[] = { 4, 1, 1, 2, 2, 4, 4 };
		static_assert(  sizeof(c_GPUVarSize) == _BGVT_LAST, "Fill in the sizes please");

		void GenericBufferBase::InitBuffer(ResourceCommandBuffer& commands)
		{
			if (!m_bufferHandle.IsValid())
				m_bufferHandle = commands.CreateResource(GENERIC_BUFFER);
		}

		inline void GenericBufferBase::ReleaseBuffer(ResourceCommandBuffer & cmd)
		{ 
			cmd.ReleaseResource(m_bufferHandle); 
		}

		/*void BufferLayout::ComputeOffsetsStride()
		{
			//std::sort(m_streams.begin(), m_streams.end(), [](StreamDesc l, StreamDesc r) {
			//	return l.semantic < r.semantic;
			//});
			uint8_t offset = 0;
			for (int i = 0; i < m_numStreams; i++) {
				m_streams[i].offset = offset;
				offset += m_streams[i].componentNum * c_GPUVarSize[m_streams[i].attributeType];
			}
			m_stride = offset;
		}*/

		dataBuffer_t VertexBuffer::SetData(ResourceCommandBuffer& commands, BufferUsageHint usage, const VertexLayout& layout, uint32_t numVertices)
		{
			InitBuffer(commands);
			uint8_t perVertSize = 0;
			for (int i = 0; i < layout.m_numStreams; i++) {
				perVertSize += layout.m_streams[i].componentNum * c_GPUVarSize[layout.m_streams[i].attributeType];
			}
			m_numVertices = numVertices;
			m_layout = layout;
			//TODO: Data might need padding between arrays
			const uint32_t numBytes = perVertSize * numVertices;
			void* dataStorage = numBytes >0 ? commands.AllocScratchMemory(m_bufferHandle, numBytes) : nullptr;
			auto resizeCmd = commands.AddCommand<command::ResizeBuffer>(m_bufferHandle);
			resizeCmd->type = BufferType::BUFFER_VERTEX;
			resizeCmd->usage = usage;
			resizeCmd->dataSize = numBytes;
			resizeCmd->dataPtr = dataStorage;
			return { dataStorage , numBytes};
		}

		dataBuffer_t IndexBuffer::SetData(ResourceCommandBuffer& commands, BufferUsageHint usage, uint32_t numIndices, BufferVarType indexType)
		{
			InitBuffer(commands);
			m_indexType = indexType;
			m_numIndices = numIndices;
			const uint32_t numBytes = c_GPUVarSize[indexType] * numIndices;
			void* dataStorage = numBytes > 0 ? commands.AllocScratchMemory(m_bufferHandle, numBytes) : nullptr;
			auto resizeCmd = commands.AddCommand<command::ResizeBuffer>(m_bufferHandle);
			resizeCmd->type = BufferType::BUFFER_INDEX;
			resizeCmd->usage = usage;
			resizeCmd->dataSize = numBytes;
			resizeCmd->dataPtr = dataStorage;
			return { dataStorage , numBytes };
		}

		void PrimitiveBuffer::Build(ResourceCommandBuffer& commands, VertexBufferPtr vbuffer, IndexBufferPtr ibuffer, GPUPrimitiveType prim)
		{
			m_primitiveType = prim;
			m_vertexBuffer = vbuffer;
			m_indexBuffer = ibuffer;
			if (!m_pbufferHandle.IsValid())
				m_pbufferHandle = commands.CreateResource(PRIMITIVE_BUFFER);

			auto cmd = commands.AddCommand<command::UpdatePrimitiveBuffer>(m_pbufferHandle);
			if (ibuffer) {
				BASSERT(ibuffer->GetHandle().IsValid());
				cmd->indexBuffer = ibuffer->GetHandle();
				cmd->indexType = ibuffer->m_indexType;
				m_numElements = ibuffer->m_numIndices;
			}
			else {
				m_numElements = vbuffer->m_numVertices;
			}
			BASSERT(vbuffer->GetHandle().IsValid());
			cmd->vertexBuffers[0] = vbuffer->GetHandle();
			cmd->vertexLayouts[0] = vbuffer->m_layout;

			switch (prim) {
			case BGPT_TRIANGLES:
				m_numPrimitives = m_numElements / 3;
				break;
			case BGPT_TRIANGLE_STRIP:
			case BGPT_TRIANGLE_FAN:
				m_numPrimitives = m_numElements - 2;
				break;
			case BGPT_POINTS:
				m_numPrimitives = m_numElements;
				break;
			case BGPT_LINES:
				m_numPrimitives = m_numElements / 2;
				break;
			case BGPT_LINE_STRIP:
				m_numPrimitives = m_numElements - 1;
				break;
			}
		}

		void PrimitiveBuffer::Release(ResourceCommandBuffer& commands)
		{
			commands.ReleaseResource(m_pbufferHandle);
			if (m_indexBuffer)
				m_indexBuffer->ReleaseBuffer(commands);
			if (m_vertexBuffer)
				m_vertexBuffer->ReleaseBuffer(commands);

			m_pbufferHandle.Invalidate();
			m_indexBuffer = nullptr;
			m_vertexBuffer = nullptr;
		}

	};
};