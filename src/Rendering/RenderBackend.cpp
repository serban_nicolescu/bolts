#pragma once

#include "Rendering/RenderBackend.h"

namespace Bolts {
	namespace Rendering {

		namespace command
		{
			processCommand_t CreateResource::s_processFunction = nullptr;
			processCommand_t ReleaseResource::s_processFunction = nullptr;
			processCommand_t UpdateTextureData::s_processFunction = nullptr;
			processCommand_t UpdateTextureFiltering::s_processFunction = nullptr;
			processCommand_t GenerateTextureMips::s_processFunction = nullptr;
			processCommand_t CompileAndLoadProgram::s_processFunction = nullptr;
			processCommand_t UpdateRTBindings::s_processFunction = nullptr;
			processCommand_t ResizeRT::s_processFunction = nullptr;
			processCommand_t ResizeBuffer::s_processFunction = nullptr;
			processCommand_t UpdateSubBuffer::s_processFunction = nullptr;
			processCommand_t UpdatePrimitiveBuffer::s_processFunction = nullptr;
		}

		RenderBackend::~RenderBackend()
		{
		}

		ResourceCommandBuffer* RenderBackend::CreateCommandBuffer()
		{
			ResourceCommandBuffer* r = nullptr;
			if (!m_commandBufferPool.empty()) {
				r = m_commandBufferPool.back();
				m_commandBufferPool.pop_back();
			}
			else {
				r = new ResourceCommandBuffer();
				r->m_owner = this;
			}
			return r;
		}

		void RenderBackend::ReturnCommandBuffer(ResourceCommandBuffer* commandBuffer)
		{
			commandBuffer->Reset();
			m_commandBufferPool.push_back(commandBuffer);
		}

		void RenderBackend::ExecuteResourceCommands(ResourceCommandBuffer* commandBuffer)
		{
			// Send commands to backend impl
			for(command::Header* commandHeader: commandBuffer->m_commands)
			{
				//TODO: what about mem allocated for command ? who cleans it up ?
				commandHeader->pFunc(this, commandHeader);
			}
			// Reset buffer
			commandBuffer->Reset();
		}
	}
}

