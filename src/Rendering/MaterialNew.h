#pragma once

#include "Rendering/GPUProgramFwd.h"
#include "Rendering/MaterialNewFwd.h"
#include "Rendering/RenderingCommon.h"
#include "Rendering/Common/MatParameterHolder.h"
#include <Assets/Assets.h>
#include <bolts_core.h>

namespace Bolts{
namespace Rendering{

	class Material : public assets::assetMixin
	{
	public:
		static const int c_assetType = assets::EAT_MATERIAL;
		typedef intrusivePtr_t< Material> Ptr;

		Material(assetID_t id);
		~Material();

		void			CloneFrom(Material& other);
		void			Unload();

		RenderState		GetRenderState() const { return m_rstate; }
		void			SetRenderState(RenderState nrs) { m_rstate = nrs; }
		void			SetProgram(GPUProgramPtr newProgram);
		GPUProgram*		GetProgram() const;
		void			SetProgramID(assetID_t id);
		assetID_t		GetProgramID() const { return m_gpuProgramId; }
		void			SetBaseMaterialID(assetID_t id) { m_baseMaterialId = id; }
		assetID_t		GetBaseMaterialID() const { return m_baseMaterialId; }

		MatParameterHolder&	EditParams() { return m_parameters; }//Use this to set and add parameters
		const MatParameterHolder& GetParams() const { return m_parameters; }
		// State checking
		//TODO: This kind of function shouldn't be necessary. The resource state should reflect this
		bool			IsValid() const { return ((m_gpuProgram != nullptr) && assetMixin::IsValid()); }

		bool			DrawProperties();
	private:
		MatParameterHolder	m_parameters;
		GPUProgramPtr	m_gpuProgram;
		RenderState		m_rstate;
		assetID_t		m_gpuProgramId = 0;
		assetID_t		m_baseMaterialId = 0;
	};

};//Rendering
};//Bolts
