#pragma once

#include <bolts_core.h>

namespace Bolts {
	namespace Rendering {
		class GPUProgram;
		typedef intrusivePtr_t<GPUProgram>	GPUProgramPtr;
	};
};
