#pragma once

#include "GPUProgram.h"
#include <memory>

namespace Bolts {
	namespace Rendering {

		const char* const c_SPTNames[_SPT_COUNT] = {
			"float",
			"double",
			"int32",
			"float2",
			"float3",
			"float4",
			"mat4x4",
			"mat3x4",
			"mat3x3",
			"texture"
		};

		void CBufferDescription::Clear()
		{
			free(storage);
			storage = 0;
			storageSize = 0;
			numEntries = 0;
		}

		void CBufferDescription::SetSize(uint16_t numberOfEntries) {
			numEntries = numberOfEntries;
			//TODO: It's not just the offset that needs to be aligned, but the mem itself.
			//	So we should request mem aligned to the largest elem size
			uint16_t reqDescriptionSize = 0;
			reqDescriptionSize += sizeof(*names) * numberOfEntries;
			reqDescriptionSize += (uint16_t)alignAdjust(reqDescriptionSize, sizeof(*arraySizes));
			int arraySizeStart = reqDescriptionSize;
			reqDescriptionSize += sizeof(*arraySizes) * numberOfEntries;
			reqDescriptionSize += (uint16_t)alignAdjust(reqDescriptionSize, sizeof(*offsets));
			int offsetsStart = reqDescriptionSize;
			reqDescriptionSize += sizeof(*offsets) * numberOfEntries;
			reqDescriptionSize += (uint16_t)alignAdjust(reqDescriptionSize, sizeof(*types));
			int typesStart = reqDescriptionSize;
			reqDescriptionSize += sizeof(*types) * numberOfEntries;
			//
			if (reqDescriptionSize > storageSize) {
				free(storage);
				storage = malloc(reqDescriptionSize);
				storageSize = reqDescriptionSize;
			}
			char* schar = (char*)storage;
			names = (hash32_t*) schar;
			types = (ShaderParamType*)(schar + typesStart);
			arraySizes = (uint16_t*)(schar + arraySizeStart);
			offsets = (uint32_t*)(schar + offsetsStart);
		}
	}
}