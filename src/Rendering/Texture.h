﻿#pragma once

#include <Assets/Assets.h>
#include "Rendering/TextureFwd.h"
#include "Rendering/RenderBackend.h"

namespace Bolts
{
	namespace Rendering
	{
		class Texture: public assets::assetMixin
		{
		public:
			static const int c_assetType = assets::EAT_TEXTURE;
			typedef typename intrusivePtr_t<Texture> Ptr;

			Texture(assetID_t rid);
			~Texture();

			bool			DrawProperties() { return false; }
			void			Init(RCB& commands);
			void			SetRTTexture(RCB& commands, TextureType type, uvec3 dim, TextureFormat format);
			dataBuffer_t	SetTexture(RCB& commands, TextureType type, uvec3 dim, TextureFormat format, TextureDataFormat dataFormat, bool withMips);
			void			SetSampler(RCB& commands, TextureSamplerSettings newSettings);
			void			GenerateMipmaps(RCB& commands);
			void			Unload(RCB& commands);

			resHandle_t		GetHandle() const { return m_handle; }

			uvec3			GetDimensions() const { return m_dimensions; }
			TextureFormat	GetFormat() const { return m_format; }
			TextureType		GetType() const { return m_type; }
			bool			HasMipmaps() const { return m_hasMips; }
			size_t			GetMemoryUsageBytes() const { return m_memUsage; }

			TextureSamplerSettings GetSampler() const { return m_defaultSettings; }
		private:
			void SetStuffInternal(TextureType type, uvec3 dim, TextureFormat format);
			void UpdateDataInternal(RCB& commands, void* data, size_t size, TextureDataFormat dataFormat);
			

			resHandle_t		m_handle;
			size_t			m_memUsage = 0; //GPU Memory used, in bytes
			uvec3			m_dimensions;
			TextureType		m_type = BTT_2D;
			TextureFormat	m_format = BTF_RGBA;
			bool			m_hasMips = false;

			TextureSamplerSettings	m_defaultSettings;
		};

	};
};