#pragma once

#include "Rendering/RenderingCommon.h"
#include <bolts_core.h>

namespace Bolts
{
	namespace Rendering
	{
		class VertexLayout {
		public:
			struct StreamDesc {
				StreamSemantic	semantic = BGSS_INVALID;
				BufferVarType	attributeType = BGVT_U8;
				uint8_t			componentNum = 0;
			};
			static_assert(sizeof(StreamDesc) <= 4, "Just make sure this is small-ish");

			bool IsValid() const { return m_numStreams != 0; }
			void AddStream(StreamDesc newStream)
			{
				m_streams[m_numStreams] = newStream;
				m_numStreams++;
			}

			const static int c_maxStreams = 8;
			typedef array_t<StreamDesc, c_maxStreams> streamArray_t;

			streamArray_t	m_streams;
			uint8_t			m_numStreams = 0;
			bool			m_isSOA = false;// If this is false, values are interleaved in the buffer. SOA stands for structure-of-arrays
		};

		class GenericBufferBase :public RefCountT<GenericBufferBase>
		{
		public:
			~GenericBufferBase() { BASSERT(!m_bufferHandle.IsValid()); }

			void			InitBuffer(ResourceCommandBuffer& cmd);
			void			ReleaseBuffer(ResourceCommandBuffer& cmd);

			uint32_t		GetSize() const { return m_bufferSize; } // In Bytes
			resHandle_t		GetHandle() const { return m_bufferHandle; }
		protected:
			resHandle_t		m_bufferHandle;
			uint32_t		m_bufferSize = 0;// Allocated size, in bytes
		};

		class VertexBuffer : public GenericBufferBase {
		public:
			dataBuffer_t	SetData(ResourceCommandBuffer& commands, BufferUsageHint usage, const VertexLayout& layout,  uint32_t numVertices);

			// these are read-only
			VertexLayout	m_layout;
			uint32_t		m_numVertices;
		};

		class IndexBuffer :public GenericBufferBase
		{
		public:
			dataBuffer_t	SetData(ResourceCommandBuffer&, BufferUsageHint usage, uint32_t numIndices, BufferVarType indexType);
			
			// these are read-only
			uint32_t		m_numIndices = 0;
			BufferVarType	m_indexType;
		};

		typedef intrusivePtr_t< IndexBuffer> IndexBufferPtr;
		typedef intrusivePtr_t< VertexBuffer> VertexBufferPtr;

		class PrimitiveBuffer : public RefCountT<PrimitiveBuffer> {
		public:
			~PrimitiveBuffer() { BASSERT(!m_pbufferHandle.IsValid()); }

			bool				IsValid() const { return m_pbufferHandle.IsValid(); }
			void				Build(ResourceCommandBuffer&, VertexBufferPtr , IndexBufferPtr, GPUPrimitiveType);
			void				Release(ResourceCommandBuffer&);

			// these are read-only
			VertexBufferPtr		m_vertexBuffer;
			IndexBufferPtr		m_indexBuffer;
			resHandle_t			m_pbufferHandle;
			uint32_t			m_numElements = 0; // number of indices, or vertices if no index buffer
			uint32_t			m_numPrimitives = 0; // approximate, based on numElements & prim type
			GPUPrimitiveType	m_primitiveType = BGPT_INVALID;
		};

		typedef intrusivePtr_t< PrimitiveBuffer> PrimitiveBufferPtr;
	};
};