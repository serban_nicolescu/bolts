#pragma once

#include <Rendering/RenderingCommon.h>
#include <Rendering/GPUBuffers.h>
#include <Rendering/TextureFwd.h>
#include <bolts_core.h>
#include <bolts_assert.h>
#include <bolts_hash.h>
#include <bolts_alloc.h>
#include <memory>
#include <atomic>
#include <mutex>
#include <thread>

namespace Bolts
{
	namespace Rendering {
		class GPUProgram;
		class Material;
		struct CBufferDescription;
		class ResourceCommandBuffer;

		class RenderBackend: public nonCopyable_t
		{
		public:
			virtual ~RenderBackend();

			virtual void Init() = 0;
			virtual resHandle_t	ReserveResourceID(RenderResourceType) = 0; // NOTE: Can be called from multiple threads
			// TEMP API
			virtual void UpdateUniformData(const CBufferDescription& description, void* data, size_t dataSize) = 0;
			virtual void BindTextureList(const resHandle_t* handles, TextureSamplerSettings* samplers, int arrayLen) = 0;
			virtual void BindUniformBuffer(unsigned bindingPoint, resHandle_t bufferHandle, size_t offset, size_t size) = 0;
			//virtual void BindIndexBuffer(const IndexBuffer*) = 0;
			//virtual void BindVertexBuffer(const VertexBuffer*) = 0;
			// Render API
			virtual void BindProgram(resHandle_t gpuProgramHandle) = 0;
			virtual void BindPrimitives(resHandle_t primBufferHandle) = 0;
			virtual void Draw(GPUPrimitiveType primitiveType, unsigned primitiveCount, unsigned primitiveOffset, unsigned numInstances) = 0;

			virtual void SetRenderState(const RenderState& rs) = 0;
			virtual void EnableRT(resHandle_t rtID) = 0;
			virtual void DisableRT() = 0;
			//
			virtual void SetViewport(Bolts::uvec2 start, Bolts::uvec2 size) = 0;
			virtual void Clear(ClearFlags flg = FLAG_ALL) = 0;
			virtual void SetClearColor(vec4 newColor) = 0;
			virtual void SetDepthClearValue(float newVal) = 0;
			virtual void SetDepthBias(float slope, float scale) = 0;
			// Queries
			virtual queryId_t		CreateQueryObject(QueryType) = 0;
			virtual bool			IsQueryFinished(queryId_t) = 0;
			virtual queryResult_t	GetQueryResult(queryId_t) = 0; //NOTE: Also deletes the query object
			// 
			virtual void			SetBuiltinUniformBufferSlot(hash32_t bufferNameHash, unsigned slot, unsigned bufferSize = 0) = 0;
			//
			virtual void ClearShadowstate() = 0;
			// Resource Commands
			//TODO: Unique ptr for GPUResourceCommands ownership transfer
			ResourceCommandBuffer* CreateCommandBuffer();
			void ReturnCommandBuffer(ResourceCommandBuffer*);
			void ExecuteResourceCommands(ResourceCommandBuffer*);

		protected:
			vec_t<ResourceCommandBuffer*> m_commandBufferPool;
		};

		namespace command
		{
			typedef void(*processCommand_t)(void* backendInstance, void* commandPtr);
			struct Header
			{
				processCommand_t pFunc = nullptr;
				resHandle_t	handle;
			};

			//Generic
			struct CreateResource :public Header
			{
				static processCommand_t s_processFunction;
			};

			struct ReleaseResource :public Header
			{
				static processCommand_t s_processFunction;
			};

			//Textures
			struct UpdateTextureData:public Header
			{
				static processCommand_t s_processFunction;

				uvec3					dimensions{1,1,1};
				TextureType				type = BTT_2D;
				TextureFormat			format = BTF_RGBA;
				void*					dataPtr = nullptr;
				uint64_t				dataSize = 0;
				TextureDataFormat		dataFormat = BTDF_IMPLICIT;
			};

			struct UpdateTextureFiltering :public Header
			{
				static processCommand_t s_processFunction;

				TextureSamplerSettings	newSettings;
			};

			struct GenerateTextureMips :public Header
			{
				static processCommand_t s_processFunction;
			};

			//Programs
			struct CompileAndLoadProgram :public Header
			{
				static processCommand_t s_processFunction;

				GPUProgram* owningProgram = nullptr; // After loading, this ptr will be used to store param info
				const char* shaderCode[4]{}; // Not null-terminated
				uint32_t	shaderCodeSize[4]{};
				ShaderType	shaderTypes[4]{};
				const char* defines = nullptr; // Not null-terminated
				uint32_t	definesSize = 0;
			};
			
			//Render targets
			struct UpdateRTBindings :public Header
			{
				static processCommand_t s_processFunction;

				static const int c_maxColorBind = 6;

				//NOTE: These just need to be valid image ids. They don't need storage assigned
				resHandle_t colorBindings[c_maxColorBind];
				resHandle_t depthBinding;
				uint8_t		boundTarget[c_maxColorBind]{}; // for cubemap textures, target is the cubemap face ( +x -x +y -y +z -z )
				uvec2 resolution;
				bool wantDepth = false; // Set this to true if you want a render-only depth attachment
			};

			//> Resizes the render target, affecting only bound renderbuffers, but not attached textures
			struct ResizeRT :public Header 
			{
				static processCommand_t s_processFunction;

				uvec2 newSize;
			};

			// Buffers
			struct UpdatePrimitiveBuffer :public Header
			{
				static processCommand_t s_processFunction;

				static const int c_maxVertBind = 2;
				typedef array_t<resHandle_t, c_maxVertBind>		vertexBufferArray_t;
				typedef array_t<VertexLayout, c_maxVertBind>	layoutArray_t;

				vertexBufferArray_t	vertexBuffers;
				layoutArray_t		vertexLayouts;
				resHandle_t			indexBuffer;
				BufferVarType		indexType;
			};

			struct ResizeBuffer :public Header // Replace entire buffer contents or init new buffer
			{
				static processCommand_t s_processFunction;

				BufferType		type;
				BufferUsageHint	usage = BUH_STATIC_DRAW;
				void*			dataPtr = nullptr;
				uint32_t		dataSize = 0;
			};

			struct UpdateSubBuffer :public Header
			{
				static processCommand_t s_processFunction;

				uint32_t		offset = 0;
				void*			dataPtr = nullptr;
				uint32_t		dataSize = 0;
			};

		};

		//http://bitsquid.blogspot.ro/2017/02/stingray-renderer-walkthrough-2.html
		//http://bitsquid.blogspot.ro/2017/02/stingray-renderer-walkthrough-3-render.html
		//http://ourmachinery.com/post/the-machinery-shader-system-part-1/
		//http://bitsquid.blogspot.ro/2016/09/state-reflection.html
		//https://blog.molecular-matters.com/2014/12/16/stateless-layered-multi-threaded-rendering-part-3-api-design-details/
		// ! https://www.dropbox.com/sh/4uvmgy14je7eaxv/AABboa6UE5Pzfg3d9updwUexa?dl=0
		class ResourceCommandBuffer:public nonCopyable_t
		{
		public:
			friend class RenderBackend;

			resHandle_t CreateResource(RenderResourceType rt)
			{
				//Call into backend to get resource handle
				resHandle_t handle = m_owner->ReserveResourceID(rt);
				//add create resource command
				AddCommand<command::CreateResource>(handle);
				return handle;
			}

			void ReleaseResource(resHandle_t& rh)
			{
				if (rh.IsValid()) {
					AddCommand<command::ReleaseResource>(rh);
					rh.Invalidate();
				}
			}

			template<typename T>
			T* AddCommand(resHandle_t handle)
			{
				BASSERT(T::s_processFunction);
				T* commandPtr = BOLTS_NEW( &m_commandAllocator, T);
				BASSERT_MSG(commandPtr, "Resource command allocator has run out of memory. Increase pool size.");
				commandPtr->pFunc = T::s_processFunction;
				commandPtr->handle = handle;
				m_commands.push_back(commandPtr);
				return commandPtr;
			}

			//NOTE: Scratch memory is released when commands are processed
			void* AllocScratchMemory(resHandle_t, size_t numBytes)
			{
				//TODO: Track usage based on handle, for debugging
				return m_dataAllocator.Alloc(numBytes, 16);
			}

			void Reset()
			{
				m_commandAllocator.Reset();
				m_dataAllocator.Reset();//TODO: IMPORTANT We don't recycle memory here. this isn't great
				m_commands.clear();
			}

			bool HasCommands() const { return m_commands.size() > 0; }

		private:
			vec_t<command::Header*> m_commands;
			FixedAllocator m_commandAllocator = FixedAllocator{ 1024 * 1024 }; //To be used for allocating command data. Cleared all at once
			StupidLeakingAllocator m_dataAllocator; //TODO: Switch to an actual OK allocator
			RenderBackend* m_owner;
		};

		typedef ResourceCommandBuffer RCB;

		namespace detail
		{
			template<typename T>
			struct resourceContainer_t
			{
				resourceContainer_t() = default;
				resourceContainer_t(resourceContainer_t&&) = default;
				resourceContainer_t& operator=(resourceContainer_t&&) = default;
				~resourceContainer_t() = default;

				vec_t<T>			resources;
				//
				std::mutex			handleListMutex;
				vec_t<resHandle_t>	freeHandles;
				int					lastFreeID = 1;

				resHandle_t GetNewHandle()
				{
					std::lock_guard<std::mutex> lk(handleListMutex);
					if (freeHandles.empty())
						return  resHandle_t::Make(T::c_resourceType, lastFreeID++);
					else {
						return pop_get(freeHandles);
					}
				}

				// Makes handle available for another resource
				void ReturnHandle(resHandle_t freeHandle)
				{
					std::lock_guard<std::mutex> lk(handleListMutex);
					freeHandles.push_back(freeHandle);
				}

				//Called on one thread
				void Resize(resHandle_t handle)
				{
					BASSERT(handle.GetType() == T::c_resourceType);
					auto idx = handle.GetIndex();
					if (idx >= resources.size())
						resources.resize(idx + 1);
				}

				T& Get(resHandle_t handle)
				{
					BASSERT(handle.GetType() == T::c_resourceType);
					auto idx = handle.GetIndex();
					BASSERT(idx < resources.size());
					return resources[idx];
				}
			};
		};

		// Example GPUProgram data
		//		Blend state
		//		Raster state
		//		Primitive type
		//		RT/Depth/Stencil formats & count
		//		Sampler states
		//		Vertex format info
		//		Constant input buffer info
	};
};