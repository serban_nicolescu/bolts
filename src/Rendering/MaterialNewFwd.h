#pragma once

namespace Bolts{
namespace Rendering{
	class Material;
	typedef intrusivePtr_t<Material> MaterialPtr;
};//Rendering
};//Bolts
