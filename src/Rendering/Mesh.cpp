#include "Mesh.h"
#include "Rendering/MaterialNew.h"
#include <bolts_assert.h>

namespace Bolts
{
	namespace Rendering {

		Mesh::SubMesh::SubMesh() = default;
		Mesh::SubMesh::SubMesh(SubMesh&& other) = default;
		Mesh::SubMesh::SubMesh(const SubMesh& other) = default;
		Mesh::SubMesh& Mesh::SubMesh::operator=(SubMesh&& other) = default;
		Mesh::SubMesh& Mesh::SubMesh::operator=(const SubMesh& other) = default;
		Mesh::SubMesh::SubMesh(PrimitiveBufferPtr _pb, regHash_t _name) :
			primitiveBuffer(_pb),
			name(_name)
		{}

		Mesh::SubMesh::~SubMesh() {}

		Mesh::Mesh(assetID_t id) :assetMixin(id) {}
		Mesh::~Mesh() {}

		bool Mesh::IsValid()
		{
			bool isValid = assetMixin::IsValid();
			for (auto& subMesh : subMeshes)
				isValid &= subMesh.primitiveBuffer != nullptr;
			return isValid;
		}

		void Mesh::Unload(ResourceCommandBuffer& commands)
		{
			SetAssetState(assets::EAS_UNLOADED);
			//NOTE: this doesn't free memory
			nodeData.Clear();
			materials.clear();
			for (auto& subMesh : subMeshes)
				subMesh.primitiveBuffer->Release(commands);
			subMeshes.clear();
		}

	};
};