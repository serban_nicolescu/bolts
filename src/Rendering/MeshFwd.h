#pragma once 

namespace Bolts
{
	namespace Rendering {
		class Mesh;
		typedef intrusivePtr_t<Mesh> MeshPtr;
	};
};