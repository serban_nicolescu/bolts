#include "GL35Backend.h"
#include <GL/glew.h>

namespace Bolts {
	namespace Rendering {

		void CheckForGLErrors();
#define GL_ERROR_CHECK CheckForGLErrors

		namespace {

			constexpr GLenum c_glNativeVarType[_BGVT_LAST] = {
				GL_FLOAT,
				GL_BYTE, GL_UNSIGNED_BYTE,
				GL_SHORT, GL_UNSIGNED_SHORT,
				GL_INT, GL_UNSIGNED_INT
			};
			static_assert(c_glNativeVarType[_BGVT_LAST - 1] != 0, "Fill this in");

			static constexpr uint8_t c_glVarSize[_BGVT_LAST] = { sizeof(float), 1, 1, 2, 2, 4, 4 };
			static_assert(c_glVarSize[_BGVT_LAST - 1] != 0, "Fill in the sizes please");

			constexpr GLenum c_glBufferType[_BUFFER_COUNT] = {
				GL_ARRAY_BUFFER,
				GL_ELEMENT_ARRAY_BUFFER,
				GL_UNIFORM_BUFFER
			};
			static_assert(c_glBufferType[_BUFFER_COUNT - 1] != 0, "Fill this in");

			constexpr GLenum c_glUsageHint[_BUH_COUNT] = {
				GL_STATIC_DRAW,//BUH_STATIC_DRAW
				GL_STATIC_READ,//BUH_STATIC_READ
				GL_STATIC_COPY,//BUH_STATIC_COPY
				GL_DYNAMIC_DRAW,//BUH_DYNAMIC_DRAW
				GL_DYNAMIC_READ,//BUH_DYNAMIC_READ
				GL_DYNAMIC_COPY,//BUH_DYNAMIC_COPY
				GL_STREAM_DRAW,//BUH_STREAM_DRAW
				GL_STREAM_READ,//BUH_STREAM_READ
				GL_STREAM_COPY,//BUH_STREAM_COPY
			};
			static_assert(c_glUsageHint[_BUH_COUNT - 1] != 0, "Fill this in");
		};

		// Commands

		template<>
		void BackendOpenGL35::ProcessResourceCommand(command::ResizeBuffer* cmd)
		{
			GL35Buffer& buffer = buffers.Get(cmd->handle);
			buffer.Reinit(*this, *cmd);
		}

		template<>
		void BackendOpenGL35::ProcessResourceCommand(command::UpdateSubBuffer* cmd)
		{
			GL35Buffer& buffer = buffers.Get(cmd->handle);
			buffer.UpdateSubBuffer(*this, *cmd);
		}

		template<>
		void BackendOpenGL35::ProcessResourceCommand(command::UpdatePrimitiveBuffer* cmd)
		{
			GL35PrimitiveBuffer& prim = primBuffers.Get(cmd->handle);
			prim.Reinit(*this, *cmd);
		}

		// END Commands

		void GL35Buffer::Reinit(BackendOpenGL35&, const command::ResizeBuffer& cmd)
		{
			if (!bufferID)
				glGenBuffers(1, &bufferID.v);
			bufferGLType = c_glBufferType[cmd.type];
			//glBindVertexArray(0); //TODO: ?
			//backend.shadowstate.boundVAO = resHandle_t();
			GL_ERROR_CHECK();
			glBindBuffer(bufferGLType, bufferID);
			glBufferData(bufferGLType, cmd.dataSize, cmd.dataPtr, c_glUsageHint[cmd.usage]);
			glBindBuffer(bufferGLType, 0);
			GL_ERROR_CHECK();
		}

		void GL35Buffer::UpdateSubBuffer(BackendOpenGL35&, const command::UpdateSubBuffer&)
		{
			BASSERT(false);
			BASSERT(bufferID);
		}

		void GL35Buffer::Bind(BackendOpenGL35&, unsigned bindingIndex, size_t offset, size_t size)
		{
			//https://www.khronos.org/opengl/wiki/Buffer_Object#Binding_indexed_targets
			//https://www.khronos.org/opengl/wiki/Uniform_Buffer_Object
			//TODO: Ensure offset is multiple of GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT
			if (offset == 0 && size == 0)
				glBindBufferBase(bufferGLType, bindingIndex, bufferID);
			else
				glBindBufferRange(bufferGLType, bindingIndex, bufferID, (GLintptr)offset, size);
			GL_ERROR_CHECK();
		}

		void GL35Buffer::Unload()
		{
			if (bufferID) {
				glDeleteBuffers(1, &bufferID.v);
				bufferID = 0;
			}
		}

		void GL35PrimitiveBuffer::Reinit(BackendOpenGL35& backend, const command::UpdatePrimitiveBuffer& cmd)
		{
			if (vaoID == 0) {
				glGenVertexArrays(1, &vaoID.v);
			}

			indexBuffer = cmd.indexBuffer;
			indexType = c_glNativeVarType[cmd.indexType];
			vertexBuffers = cmd.vertexBuffers;
			//vertexLayouts = cmd.vertexLayouts;

			glBindVertexArray(vaoID);
			
			if (cmd.indexBuffer.IsValid())
			{
				auto& buffer = backend.buffers.Get(cmd.indexBuffer);
				BASSERT(buffer.bufferGLType == GL_ELEMENT_ARRAY_BUFFER);
				BASSERT(buffer.bufferID);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer.bufferID);
				indexID = buffer.bufferID.v;
				backend.CheckForErrors();
			}
			else {
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
				indexType = 0;
				indexID = 0;
			}


			if (cmd.vertexBuffers[0].IsValid())
			{
				auto& buffer = backend.buffers.Get(cmd.vertexBuffers[0]);
				BASSERT(buffer.bufferGLType == GL_ARRAY_BUFFER);
				BASSERT(buffer.bufferID);
				glBindBuffer(GL_ARRAY_BUFFER, buffer.bufferID);
				backend.CheckForErrors();
				auto& layout = cmd.vertexLayouts[0];
				BASSERT(!layout.m_isSOA);
				uint8_t offset = 0;
				for (int i = 0; i < layout.m_numStreams; i++) {
					auto const& stream = layout.m_streams[i];
					offset += stream.componentNum * c_glVarSize[stream.attributeType];
				}
				const uint8_t stride = offset;
				offset = 0;
				for (int i = 0; i < layout.m_numStreams; i++) {
					auto const& stream = layout.m_streams[i];
					glEnableVertexAttribArray(stream.semantic);
					//https://www.khronos.org/opengl/wiki/Vertex_Specification#Vertex_Array_Object
					if (c_glNativeVarType[stream.attributeType] == GL_FLOAT)
						glVertexAttribPointer(stream.semantic,
							stream.componentNum,
							c_glNativeVarType[stream.attributeType],
							GL_TRUE,
							stride,
							(const GLvoid *)offset);
					else 
						glVertexAttribIPointer(stream.semantic,
							stream.componentNum,
							c_glNativeVarType[stream.attributeType],
							stride,
							(const GLvoid *)offset);
					offset += stream.componentNum * c_glVarSize[stream.attributeType];
				}
			}

			glBindVertexArray(0);
			backend.shadowstate.boundVAO = {};
		}

		void GL35PrimitiveBuffer::Unload()
		{
			if (vaoID) {
				glDeleteVertexArrays(1, &vaoID.v);
				vaoID = 0;
			}
		}

	};
};
