#include "GL35Backend.h"

//TEMP
#include <Rendering/MaterialNew.h>
#include <Rendering/GPUProgram.h>

#include <GL/glew.h>

namespace Bolts {
	namespace Rendering {
			static bool g_glErrorChecking = true;

			void CheckForGLErrors()
			{
				if (!g_glErrorChecking)
					return;
				GLenum err(glGetError());

				while (err != GL_NO_ERROR) {
					BOLTS_LERROR() << "[GL] OpenGL error: " << err;
					BASSERT(false);
					err = glGetError();
				}
			}

	#define GL_ERROR_CHECK CheckForGLErrors

		namespace {
			
			// GL Constant matching

			constexpr GLenum c_GLCullFace[_BGCM_COUNT] = {
				0, // Disable culling
				GL_FRONT,
				GL_BACK,
				GL_FRONT_AND_BACK
			};


			constexpr GLenum c_GLBlendFunc[_BGBE_COUNT] = {
				GL_FUNC_ADD,// BGBE_DISABLED, 
				GL_FUNC_ADD,// BGBE_ADD,
				GL_FUNC_SUBTRACT, //BGBE_SUBTRACT, 
				GL_FUNC_REVERSE_SUBTRACT, //BGBE_REVERSE_SUBTRACT
				GL_MIN,
				GL_MAX
			};

			constexpr GLenum c_GLBlendFactor[_BGBF_COUNT] = {
				GL_ZERO, //BGBF_ZERO, 
				GL_ONE,//BGBF_ONE, 
				GL_SRC_ALPHA,//BGBF_SRC_ALPHA,
				GL_ONE_MINUS_SRC_ALPHA,// BGBF_OM_SRC_ALPHA
			};

			//
			void __stdcall glDebugBla(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei /*length*/, const GLchar *message, GLvoid* /*userParam*/)
			{
				const char *sourceFmt = "UNDEFINED";
				switch (source)
				{
				case GL_DEBUG_SOURCE_API_ARB:             sourceFmt = "API"; break;
				case GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB:   sourceFmt = "WINDOW_SYSTEM"; break;
				case GL_DEBUG_SOURCE_SHADER_COMPILER_ARB: sourceFmt = "SHADER_COMPILER"; break;
				case GL_DEBUG_SOURCE_THIRD_PARTY_ARB:     sourceFmt = "THIRD_PARTY"; break;
				case GL_DEBUG_SOURCE_APPLICATION_ARB:     sourceFmt = "APPLICATION"; break;
				case GL_DEBUG_SOURCE_OTHER_ARB:           sourceFmt = "OTHER"; break;
				}

				const char *typeFmt = "UNDEFINED";
				switch (type)
				{
				case GL_DEBUG_TYPE_ERROR_ARB:               typeFmt = "ERROR"; break;
				case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_ARB: typeFmt = "DEPRECATED_BEHAVIOR"; break;
				case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_ARB:  typeFmt = "UNDEFINED_BEHAVIOR"; break;
				case GL_DEBUG_TYPE_PORTABILITY_ARB:         typeFmt = "PORTABILITY"; break;
				case GL_DEBUG_TYPE_PERFORMANCE_ARB:         typeFmt = "PERFORMANCE"; break;
				case GL_DEBUG_TYPE_OTHER_ARB:               typeFmt = "OTHER"; break;
				}

				const char *severityFmt = "UNDEFINED";
				switch (severity)
				{
				case GL_DEBUG_SEVERITY_HIGH_ARB:   severityFmt = "HIGH";   break;
				case GL_DEBUG_SEVERITY_MEDIUM_ARB: severityFmt = "MEDIUM"; break;
				case GL_DEBUG_SEVERITY_LOW_ARB:    severityFmt = "LOW"; break;
				}

				//// ignore some...
				//if (id == 131076)
				//	return; // Usage warning: Generic vertex attribute array ? uses a pointer with a small value (0x0000000?). Is this intended to be used as an offset into a buffer object?
				if (id == 131185)
					return; // Buffer detailed info: Buffer object ? (bound to GL_ELEMENT_ARRAY_BUFFER_ARB, usage hint is GL_STATIC_DRAW) will use VIDEO memory as the source for buffer object operations.
				if (id == 131169)
					return; // Framebuffer detailed info: The driver allocated storage for renderbuffer N
				//if (id == 131154)
				//	return; // SCREENSHOTS HERE... Pixel-path performance warning: Pixel transfer is synchronized with 3D rendering.

				BOLTS_LERROR() << "[OGL] " << message << "[source=" << sourceFmt << " type=" << typeFmt << " id=" << id << "]";
				BASSERT_MSG(false, "OGL Error");
			}
		}

		// COMMANDS

		template<typename CMD>
		void BindCommand()
		{
			CMD::s_processFunction = [](void* backend, void* cmd)
			{
				CMD* typedCommand = static_cast<CMD*>(cmd);
				static_cast<BackendOpenGL35*>(backend)->ProcessResourceCommand<>(typedCommand);
			};
		}

		template<>
		void BackendOpenGL35::ProcessResourceCommand(command::CreateResource* cmd)
		{
			switch (cmd->handle.GetType()) {
			case RenderResourceType::IMAGE_BUFFER:
			{
				textures.Resize(cmd->handle);
				GL35Texture& newTex = textures.Get(cmd->handle);
				newTex.Reinit();
				break;
			}
			case RenderResourceType::SHADER_PROGRAM:
			{
				gpuPrograms.Resize(cmd->handle);
				GL35Program& newPrg = gpuPrograms.Get(cmd->handle);
				newPrg.Reinit();
				break;
			}
			case RenderResourceType::FRAMEBUFFER:
			{
				framebuffers.Resize(cmd->handle);
				//auto& newFB = framebuffers.Get(cmd->handle);
				break;
			}
			case RenderResourceType::GENERIC_BUFFER:
			{
				buffers.Resize(cmd->handle);
				break;
			}
			case RenderResourceType::PRIMITIVE_BUFFER:
			{
				primBuffers.Resize(cmd->handle);
				break;
			}
			default:
				BASSERT(false);
			}
		}

		template<>
		void BackendOpenGL35::ProcessResourceCommand(command::ReleaseResource* cmd)
		{
#define RELEASE_RESOURCE(resContainer)						\
			auto& oldRes = resContainer.Get(cmd->handle);	\
			oldRes.Unload();								\
			resContainer.ReturnHandle(cmd->handle);

			switch (cmd->handle.GetType()) {
			case RenderResourceType::IMAGE_BUFFER:
			{
				RELEASE_RESOURCE(textures);
				break;
			}
			case RenderResourceType::SHADER_PROGRAM:
			{
				RELEASE_RESOURCE(gpuPrograms);
				break;
			}
			case RenderResourceType::FRAMEBUFFER:
			{
				RELEASE_RESOURCE(framebuffers);
				break;
			}
			case RenderResourceType::GENERIC_BUFFER:
			{
				RELEASE_RESOURCE(buffers);
				break;
			}
			case RenderResourceType::PRIMITIVE_BUFFER:
			{
				RELEASE_RESOURCE(primBuffers);
				break;
			}
			default:
				BASSERT(false);
			}

#undef RELEASE_RESOURCE
		}

		// END COMMANDS

		BackendOpenGL35::BackendOpenGL35()
		{
		}

		BackendOpenGL35::~BackendOpenGL35()
		{
			delete[] boundTextures;
		}

		void BackendOpenGL35::Init()
		{
			GLenum err = glewInit();
			if (GLEW_OK != err) {
				/* Problem: glewInit failed, something is seriously wrong. */
				//TODO: Log init failure properly
				BOLTS_LERROR() << "GLEW Error: " << err;
				//printf( "GLEW Error: %s\n", glewGetErrorString( err ) );
			}
			
			glEnable(GL_CULL_FACE);
			glEnable(GL_DEPTH_TEST);
			glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
			//glEnable(GL_FRAMEBUFFER_SRGB);
			//glDisable( GL_CULL_FACE);
			//glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
			glPointSize(2.f);
			glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			GL_ERROR_CHECK();

			//debug stuff
			if (glDebugMessageControlARB)
			{
				glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
				glDebugMessageCallbackARB(glDebugBla, NULL);

				//glDebugMessageControlARB(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, 0, false);
				unsigned int severity[] = { GL_DEBUG_SEVERITY_HIGH_ARB, GL_DEBUG_SEVERITY_MEDIUM_ARB, GL_DEBUG_SEVERITY_LOW_ARB };
				//for (int i = 0; i < sizeof(severity) / sizeof(severity[0]); ++i)
				//{
				//	glDebugMessageControlARB(GL_DONT_CARE, GL_DONT_CARE, severity[i], 0, 0, true);
				//}

				unsigned int sources[] = { GL_DEBUG_SOURCE_API_ARB, GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB, GL_DEBUG_SOURCE_SHADER_COMPILER_ARB, GL_DEBUG_SOURCE_THIRD_PARTY_ARB, GL_DEBUG_SOURCE_APPLICATION_ARB, GL_DEBUG_SOURCE_OTHER_ARB };
				for (int j = 0; j < sizeof(sources) / sizeof(sources[0]); ++j)
				{
					for (int i = 0; i < sizeof(severity) / sizeof(severity[0]); ++i)
					{
						glDebugMessageControlARB(sources[j], GL_DEBUG_TYPE_ERROR_ARB, severity[i], 0, NULL, true);
						glDebugMessageControlARB(sources[j], GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_ARB, severity[i], 0, NULL, true);
						glDebugMessageControlARB(sources[j], GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_ARB, severity[i], 0, NULL, true);
						glDebugMessageControlARB(sources[j], GL_DEBUG_TYPE_PORTABILITY_ARB, severity[i], 0, NULL, true);
						glDebugMessageControlARB(sources[j], GL_DEBUG_TYPE_PERFORMANCE_ARB, severity[i], 0, NULL, true);
						glDebugMessageControlARB(sources[j], GL_DEBUG_TYPE_OTHER_ARB, severity[i], 0, NULL, true);
					}
				}
			}

			glBlendFunc(c_GLBlendFactor[BGBF_SRC_ALPHA], c_GLBlendFactor[BGBF_OM_SRC_ALPHA]);

			//https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glGet.xhtml
			// Tex units
			GLint maxtexunits = 80;
			glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &maxtexunits);
			boundTextures = new resHandle_t[maxtexunits];
			//
			memset(&shadowstate, 0, sizeof(shadowstate));
			memset(&builtinUniformBindings, 0, sizeof(builtinUniformBindings));
			//
			BindCommands();
		}

		// NOTE: Can be called from multiple threads
		resHandle_t BackendOpenGL35::ReserveResourceID(RenderResourceType type)
		{
			switch (type)
			{
			case RenderResourceType::IMAGE_BUFFER:
				return textures.GetNewHandle();
			case RenderResourceType::SHADER_PROGRAM:
				return gpuPrograms.GetNewHandle();
			case RenderResourceType::FRAMEBUFFER:
				return framebuffers.GetNewHandle();
			case RenderResourceType::GENERIC_BUFFER:
				return buffers.GetNewHandle();
			case RenderResourceType::PRIMITIVE_BUFFER:
				return primBuffers.GetNewHandle();
			default:
				BASSERT(false);
			}
			return resHandle_t::Make(type, 0);
		}

		void BackendOpenGL35::BindCommands()
		{
			// Register command handlers to command structs
			BindCommand<command::CreateResource>();
			BindCommand<command::ReleaseResource>();
			// Textures
			BindCommand<command::UpdateTextureData>();
			BindCommand<command::UpdateTextureFiltering>();
			BindCommand<command::GenerateTextureMips>();
			// Gpu programs
			BindCommand<command::CompileAndLoadProgram>();
			// Framebuffers
			BindCommand<command::UpdateRTBindings>();
			BindCommand<command::ResizeRT>();
			// Buffers
			BindCommand<command::ResizeBuffer>();
			BindCommand<command::UpdateSubBuffer>();
			// Prim Buffers
			BindCommand<command::UpdatePrimitiveBuffer>();
		}

		// END COMMANDS

		void BackendOpenGL35::ClearShadowstate()
		{
			memset(&shadowstate, 0, sizeof(shadowstate));
			memset(boundTextures, 0, sizeof(resHandle_t) * 80);
		}

		void BackendOpenGL35::CheckForErrors()
		{
			GL_ERROR_CHECK();
		}

		// Set render state
		void BackendOpenGL35::SetRenderState(const RenderState& rs)
		{
			GL_ERROR_CHECK();
			// Blend
			if (renderState.blendEq != rs.blendEq)
				if (rs.blendEq == BGBE_DISABLED) {
					glDisable(GL_BLEND);
				}
				else {
					glEnable(GL_BLEND);
					glBlendEquation(c_GLBlendFunc[rs.blendEq]);
				}
			// Blend Functions
			if (rs.blendEq != BGBE_DISABLED && (renderState.srcBlendFunc != rs.srcBlendFunc || renderState.dstBlendFunc != rs.dstBlendFunc))
				glBlendFunc(c_GLBlendFactor[rs.srcBlendFunc], c_GLBlendFactor[rs.dstBlendFunc]);
			// Cull Mode
			if (renderState.cullingMode != rs.cullingMode)
				if (rs.cullingMode == BGCM_NONE) {
					glDisable(GL_CULL_FACE);
				}
				else {
					glEnable(GL_CULL_FACE);
					glCullFace(c_GLCullFace[rs.cullingMode]);
				}
			// Depth Test
			if (renderState.isDepthTestEnabled != rs.isDepthTestEnabled)
				if (rs.isDepthTestEnabled)
					glEnable(GL_DEPTH_TEST);
				else
					glDisable(GL_DEPTH_TEST);
			// Depth Write
			if (renderState.isDepthWriteEnabled != rs.isDepthWriteEnabled)
				glDepthMask(rs.isDepthWriteEnabled);

			renderState = rs;
			GL_ERROR_CHECK();
		}

		void BackendOpenGL35::UpdateUniformData(const CBufferDescription& description, void* data, size_t)
		{
			GL_ERROR_CHECK();
			int texSlot = 0;
			// Set uniforms using old API
			for (int idx = 0; idx < description.numEntries; idx++) {
				const uint32_t offset = description.offsets[idx];
				const GLsizei arraySize = description.arraySizes[idx];
				const void* valueLocation = (char*)(data)+offset;
				switch (description.types[idx])
				{
				case SPT_MAT4:
					glUniformMatrix4fv(idx, arraySize, false, (const GLfloat*)valueLocation);
					break;
				case SPT_MAT34:
					glUniformMatrix3x4fv(idx, arraySize, false, (const GLfloat*)valueLocation);
					break;
				case SPT_MAT3:
					glUniformMatrix3fv(idx, arraySize, false, (const GLfloat*)valueLocation);
					break;
				case SPT_VEC4:
					glUniform4fv(idx, arraySize, (const GLfloat*)valueLocation);
					break;
				case SPT_VEC3:
					glUniform3fv(idx, arraySize, (const GLfloat*)valueLocation);
					break;
				case SPT_VEC2:
					glUniform2fv(idx, arraySize, (const GLfloat*)valueLocation);
					break;
				case SPT_FLOAT:
					glUniform1fv(idx, arraySize, (const GLfloat*)valueLocation);
					break;
				case SPT_INT32:
					static_assert(sizeof(GLint) == sizeof(int32_t), "these not matching is bad news");
					glUniform1iv(idx, arraySize, (const int32_t*)valueLocation);
					break;
				case SPT_TEX:// set texture unit indices
					texSlot++;
					glUniform1i(idx, GLint(texSlot));
					break;
				default: {
					BASSERT(false);
					BOLTS_LERROR() << "[GL35Backend] " "Error binding shader parameter. Unimplemented bind code for this SPT_: " << description.types[idx];
				}
				}
			}
			GL_ERROR_CHECK();
		}

		void BackendOpenGL35::BindTextureList(const resHandle_t* handles, TextureSamplerSettings* samplers, int arrayLen)
		{
			// Determine binding points & bind textures
			for (int texIdx = 0; texIdx < arrayLen; texIdx++) {
				const resHandle_t texHandle = handles[texIdx];
				const int texSlot = texIdx + 1;
				if (!texHandle.IsValid()) {
					//TODO: Should bind bad texture here, but this check belongs in the renderer
					BASSERT_MSG(false, "Missing texture bound to uniform");
					BOLTS_LERROR() << "[GL35Backend] " "Missing texture bound to shader for slot #" << texSlot;
					break;
				}
				auto& glTex = textures.Get(texHandle);
				const resHandle_t boundTex = boundTextures[texSlot];
				// Pick texture bind index
				if (boundTex != texHandle) {
					// Bind tex
					glTex.Bind(texSlot);
					boundTextures[texSlot] = texHandle; // Textures are bound off by one. This will surely never cause any bugs
					// Bind sampler
					if (!glTex.hasMips && samplers[texIdx].mipFilteringType != TextureFilterType::BTFT_NONE)
						samplers[texIdx].mipFilteringType = TextureFilterType::BTFT_NONE;
					//TODO: Tex sampler settings inside the texture are now ignored. Do we need them ?
					SetSamplerSettings(texSlot, samplers[texIdx]);
				}
			}
			GL_ERROR_CHECK();
		}

		void BackendOpenGL35::BindUniformBuffer(unsigned bindingPoint, resHandle_t bufferHandle, size_t offset, size_t size)
		{
			BASSERT(bindingPoint < countof(shadowstate.boundUniformBuffers));
			if (shadowstate.boundUniformBuffers[bindingPoint] != bufferHandle) {
				shadowstate.boundUniformBuffers[bindingPoint] = bufferHandle;
				auto& gpuBuffer = buffers.Get(bufferHandle);
				BASSERT(gpuBuffer.bufferGLType == GL_UNIFORM_BUFFER);
				gpuBuffer.Bind(*this, bindingPoint, offset, size);
				GL_ERROR_CHECK();
			}
		}

		void BackendOpenGL35::BindProgram(resHandle_t gpuProgramHandle)
		{
			if (shadowstate.boundProgram != gpuProgramHandle) {
				shadowstate.boundProgram = gpuProgramHandle;
				auto& program = gpuPrograms.Get(gpuProgramHandle);

				BASSERT(program.m_programID);
				if (program.m_programID != 0) {
					glUseProgram(program.m_programID);
				}
				GL_ERROR_CHECK();
			}
		}

		void BackendOpenGL35::BindPrimitives(resHandle_t primBufferHandle)
		{
			if (shadowstate.boundVAO != primBufferHandle) {
				GLenum indexType = 0;
				if (primBufferHandle.d != 0) {
					auto& vao = primBuffers.Get(primBufferHandle);
					BASSERT(vao.vaoID);
					glBindVertexArray(vao.vaoID);
					// also bind index buffer, even though we technically shouldn't have to #GLBUG
					if (vao.indexID) {
						glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vao.indexID);
					}
					indexType = vao.indexType;
				}
				shadowstate.boundVAO = primBufferHandle;
				shadowstate.boundIndexType = indexType;
			}
		}

		void BackendOpenGL35::Draw(GPUPrimitiveType primitiveType, unsigned primitiveCount, unsigned primitiveOffset, unsigned numInstances)
		{
			static array_t< GLenum, Bolts::Rendering::_BGPT_COUNT>	g_GLPrimitiveType = {
				GL_TRIANGLES,
				GL_TRIANGLE_STRIP,
				GL_TRIANGLE_FAN,
				GL_LINES,
				GL_LINE_STRIP,
				GL_POINTS
			};

			if (numInstances == 1)
				if (shadowstate.boundIndexType)
					glDrawElements(g_GLPrimitiveType[primitiveType], primitiveCount, shadowstate.boundIndexType, (void*)(uintptr_t)primitiveOffset);
				else
					glDrawArrays( g_GLPrimitiveType[primitiveType], primitiveOffset, primitiveCount);
			else
				if (shadowstate.boundIndexType)
					glDrawElementsInstanced(g_GLPrimitiveType[primitiveType], primitiveCount, shadowstate.boundIndexType, (void*)(uintptr_t)primitiveOffset, numInstances);
				else
					glDrawArraysInstanced(g_GLPrimitiveType[primitiveType], primitiveOffset, primitiveCount, numInstances);
		}

		void BackendOpenGL35::EnableRT(resHandle_t rtID)
		{
			GL_ERROR_CHECK();
			auto& rt = framebuffers.Get(rtID);
			rt.SetActive(*this);
			shadowstate.boundFB = rt.framebufferGLID;
			SetViewport({}, rt.resolution);
			GL_ERROR_CHECK();
		}

		void BackendOpenGL35::DisableRT()
		{
			GL_ERROR_CHECK();
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			SetViewport({ shadowstate.finalViewport.x, shadowstate.finalViewport.y }, { shadowstate.finalViewport.z, shadowstate.finalViewport.w });
			shadowstate.boundFB = 0;
			GL_ERROR_CHECK();
		}

		void BackendOpenGL35::SetViewport(Bolts::uvec2 start, Bolts::uvec2 size)
		{
			glViewport(start.x, start.y, size.x, size.y);
			currentViewport = { start.x, start.y, size.x, size.y };
			if (shadowstate.boundFB == 0)
				shadowstate.finalViewport = currentViewport;
		}

		void BackendOpenGL35::Clear(ClearFlags flags)
		{
			GL_ERROR_CHECK();
			GLbitfield mask = 0;
			mask |= flags & FLAG_COLOR ? GL_COLOR_BUFFER_BIT : 0;
			mask |= flags & FLAG_DEPTH ? GL_DEPTH_BUFFER_BIT : 0;
			mask |= flags & FLAG_STENCIL ? GL_STENCIL_BUFFER_BIT : 0;

			if ((flags & FLAG_DEPTH) && !renderState.isDepthWriteEnabled)
				glDepthMask(true);
			glClear(mask);
			if ((flags & FLAG_DEPTH) && !renderState.isDepthWriteEnabled)
				glDepthMask( false);
		}

		void BackendOpenGL35::SetClearColor(vec4 newColor)
		{
			glClearColor(newColor.r, newColor.g, newColor.b, newColor.a);
		}

		void BackendOpenGL35::SetDepthClearValue(float newVal)
		{
			glClearDepth(newVal);
		}
		void BackendOpenGL35::SetDepthBias(float slope, float scale)
		{
			if (slope > 0.f || scale > 0.f) {
				glEnable(GL_POLYGON_OFFSET_FILL);
				glPolygonOffset(slope, scale);
			}
			else {
				glDisable(GL_POLYGON_OFFSET_FILL);
			}
		}

		//https://www.khronos.org/opengl/wiki/Query_Object#Timer_queries
		static const int c_queryBufferSize = 32;
		static GLuint g_unusedQueryIds[c_queryBufferSize];
		static int g_numQueryIds = 0;

		queryId_t BackendOpenGL35::CreateQueryObject(QueryType qt)
		{
			static const GLenum c_glQueryType[] = {
				GL_TIMESTAMP
			};

			if (g_numQueryIds == 0) {
				glGenQueries(c_queryBufferSize, g_unusedQueryIds);
				g_numQueryIds = c_queryBufferSize;
			}
			g_numQueryIds--;
			GLuint id = g_unusedQueryIds[g_numQueryIds];
			glQueryCounter(id, c_glQueryType[qt]);
			return queryId_t(id);
		}

		bool BackendOpenGL35::IsQueryFinished(queryId_t qid)
		{
			int isDone = false;
			glGetQueryObjectiv(GLuint(qid), GL_QUERY_RESULT_AVAILABLE, &isDone);
			return isDone != 0;
		}

		queryResult_t BackendOpenGL35::GetQueryResult(queryId_t qid)
		{
			queryResult_t result = 0;
			GLuint id = GLuint(qid);
			glGetQueryObjectui64v(id, GL_QUERY_RESULT, &result); //result is nanoseconds for GL_TIMESTAMP
			
			if (g_numQueryIds == c_queryBufferSize)
			{
				auto halfBuffer = c_queryBufferSize / 2;
				glDeleteQueries(halfBuffer, g_unusedQueryIds + halfBuffer);
				g_numQueryIds -= halfBuffer;
			}
			g_unusedQueryIds[g_numQueryIds] = (GLuint)qid;
			g_numQueryIds++;

			return result;
		}
		

		void BackendOpenGL35::SetBuiltinUniformBufferSlot(hash32_t bufferNameHash, unsigned slot, unsigned bufferSize)
		{
			//static_assert(sizeof(builtinUniformSlots) * 8 <= countof(BackendOpenGL35::builtinUniformBindings), "");
			for (unsigned i = 0; i < countof(builtinUniformBindings); i++) {
				if (builtinUniformBindings[i].bufferName == 0) {
					builtinUniformBindings[i].bufferName = bufferNameHash;
					builtinUniformBindings[i].size = bufferSize;
					builtinUniformBindings[i].slot = slot;

					builtinUniformSlots |= 1 << slot; // mark slot as used, so it's not used somewhere else
					return;
				}
			}

			// code for erasing an entry
			//int idx = -1;
			//for (int i = 0; i < countof(builtinUniformBindings); i++) {
			//	if (builtinUniformBindings[i].bufferName == bufferNameHash) {
			//		idx = i;
			//		break;
			//	}
			//}
			//BASSERT(idx != -1);
			//for (int i = 0; i < countof(builtinUniformBindings); i++) {
			//	if (builtinUniformBindings[i].bufferName == 0) {
			//		std::swap(builtinUniformBindings[i], builtinUniformBindings[idx]);
			//		builtinUniformBindings[i].bufferName = 0;
			//		return;
			//	}
			//}
			BASSERT_MSG(false, "too many builtin bindings. increase size of builtinUniformBindings[]");
		}

		int BackendOpenGL35::GetBuiltinUniformBufferSlot(hash32_t bufferNameHash, unsigned bufferSize) const
		{
			for (unsigned i = 0; i < countof(builtinUniformBindings); i++) {
				if (builtinUniformBindings[i].bufferName == bufferNameHash) {
					if (builtinUniformBindings[i].size != 0)
						BASSERT(builtinUniformBindings[i].size == bufferSize);
					return (int)builtinUniformBindings[i].slot;
				}
			}
			return -1;
		}
};
};