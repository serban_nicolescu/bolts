#pragma once

#include <Rendering/RenderBackend.h>
#include <Rendering/GPUProgram.h>
#include <Rendering/MaterialNew.h>
#include <bolts_core.h>
#include <bolts_helpers.h>

namespace Bolts
{
	namespace Rendering 
	{
		class Material;
		class MatParameterHolder;
		class BackendOpenGL35;

		namespace detail {
			template<typename T>
			struct zeroOnMove
			{
				zeroOnMove() = default;
				zeroOnMove(T vv) :v(vv) {}
				zeroOnMove(zeroOnMove&& other) {
					v = other.v;
					other.v = 0;
				}
				zeroOnMove& operator=(zeroOnMove&& other)
				{
					v = other.v;
					other.v = 0;
					return *this;
				}
				operator T() { return v; }

				T v = 0;
			};
		};

		// OpenGL Types

		typedef unsigned int GLenum;
		typedef unsigned int GLbitfield;
		typedef unsigned int GLuint;
		typedef int GLint;
		typedef int GLsizei;
		typedef unsigned char GLboolean;
		typedef signed char GLbyte;
		typedef short GLshort;
		typedef unsigned char GLubyte;
		typedef unsigned short GLushort;
		typedef unsigned long GLulong;
		typedef float GLfloat;
		typedef float GLclampf;
		typedef double GLdouble;
		typedef double GLclampd;
		typedef void GLvoid;

		typedef detail::zeroOnMove<GLuint> glID_t;

		// Resources

		struct GL35Program {
			static const RenderResourceType c_resourceType = RenderResourceType::SHADER_PROGRAM;

			struct Shaders {
				GLuint shaderID[4]{};
			};

			struct AttribDesc {
				GLint			paramSize; //Number of elements( for vectors/matrices)
				hash32_t		paramName;
				uint16_t		paramIndex;
				GLenum			paramType;

				//Comparison operator defined for sorting containers
				bool operator< (const AttribDesc &other) {
					return paramName < other.paramName;
				}
				bool operator!= (const AttribDesc &other) {
					return paramName != other.paramName;
				}
				bool operator== (const AttribDesc &other) {
					return paramName == other.paramName;
				}

				bool IsSamplerParam() const;
			};

			struct UniformDesc : public AttribDesc {
			};

			typedef int						TextureUnitIdx_t;
			typedef vec_t< AttribDesc >		attributeList_t;
			typedef vec_t< UniformDesc >	uniformList_t;
			typedef vec_t< char >			texUnitsList_t;
			
			GL35Program() = default;
			GL35Program(GL35Program&& other) = default;
			//{
			//	m_attributes = std::move(other.m_attributes);
			//	m_uniforms = std::move(other.m_uniforms);
			//	m_cbuffer = std::move(other.m_cbuffer);
			//	m_programID = other.m_programID;
			//	m_numUniforms = other.m_numUniforms;
			//	m_numAttributes = other.m_numAttributes;
			//	other.m_programID = 0;
			//}
			GL35Program& operator=(GL35Program&& other) = default;
			//{
			//	m_attributes = std::move(other.m_attributes);
			//	m_uniforms = std::move(other.m_uniforms);
			//	m_cbuffer = std::move(other.m_cbuffer);
			//	m_programID = other.m_programID;
			//	m_numUniforms = other.m_numUniforms;
			//	m_numAttributes = other.m_numAttributes;
			//	other.m_programID = 0;
			//	return *this;
			//}

			~GL35Program() { Unload(); }

			void	Reinit();
			void	Unload();

			bool	Link(Shaders);//> Returns true on compilation success
			void	LoadProgramParameterInfo(BackendOpenGL35* backend);

			attributeList_t		m_attributes;

			CBufferDescription	m_uniforms;
			CBufferDescription	m_cbuffers[6];
			//vec_t<uint8_t>		m_uniformIndices;
			//texUnitsList_t		m_texUnits; // Holds the binding index for sampler-type uniforms. Used when binding textures

			glID_t				m_programID = 0;
			uint8_t				m_numAttributes;
		};

		struct GL35Texture
		{
			static const RenderResourceType c_resourceType = RenderResourceType::IMAGE_BUFFER;

			GL35Texture();
			GL35Texture(GL35Texture&&) = default;
			GL35Texture& operator=(GL35Texture&&) = default;
			~GL35Texture();

			void Reinit();
			void Unload();
			void Bind(GLint); //>Associate this texture to a specific texture unit. NOTE: 0 is reserved for Commit operations
			void Unbind();

			void Upload2DData(const void* data, uint64_t size, TextureDataFormat dataFormat);
			void Upload3DData(const void* data, uint64_t size, TextureDataFormat dataFormat);
			void UpdateFilterState();
			void GenerateMips();

			bool ReadPixels(TextureFormat readFormat, unsigned mipLevel, vec_t<char>& outBuffer) const;//>Reads texture data back from GPU and populates buffer using requested format

			TextureSamplerSettings	defaultSampler;
			uvec3					dimensions;
			glID_t					glID = 0;
			//
			TextureType				type = BTT_2D;
			TextureFormat			format = BTF_RGBA;
			bool					hasMips = false;
		};

		struct GL35Framebuffer
		{
			static const RenderResourceType c_resourceType = RenderResourceType::FRAMEBUFFER;

			GL35Framebuffer();
			GL35Framebuffer(GL35Framebuffer&&) = default;
			GL35Framebuffer& operator=(GL35Framebuffer&&) = default;
			~GL35Framebuffer();

			void Reinit(BackendOpenGL35&, const command::UpdateRTBindings&);
			void Unload();
			void Resize(BackendOpenGL35&,uvec2 newSize);
			void SetActive(BackendOpenGL35&);

			resHandle_t			colorTextures[6];
			resHandle_t			depthTexture;
			uvec2				resolution;
			bool				isFramebufferValid = false;
			glID_t				framebufferGLID = 0;
			glID_t				depthRenderbufferGLID = 0;
		};

		struct GL35Buffer
		{
			static const RenderResourceType c_resourceType = RenderResourceType::GENERIC_BUFFER;

			GL35Buffer() {}
			GL35Buffer(GL35Buffer&&) = default;
			GL35Buffer& operator=(GL35Buffer&&) = default;
			~GL35Buffer() { Unload(); }

			void Reinit(BackendOpenGL35&, const command::ResizeBuffer&);
			void UpdateSubBuffer(BackendOpenGL35&, const command::UpdateSubBuffer&);
			void Bind(BackendOpenGL35&, unsigned bindingIndex, size_t offset, size_t size);
			void Unload();

			GLenum				bufferGLType;
			glID_t				bufferID;
		};

		struct GL35PrimitiveBuffer
		{
			static const RenderResourceType c_resourceType = RenderResourceType::PRIMITIVE_BUFFER;

			GL35PrimitiveBuffer() {}
			GL35PrimitiveBuffer(GL35PrimitiveBuffer&&) = default;
			GL35PrimitiveBuffer& operator=(GL35PrimitiveBuffer&&) = default;
			~GL35PrimitiveBuffer() { Unload(); }

			void Reinit(BackendOpenGL35&, const command::UpdatePrimitiveBuffer&);
			void Unload();

			static const int c_maxVertBind = 2;
			typedef array_t<resHandle_t, c_maxVertBind>		vertexBufferArray_t;
			typedef array_t<VertexLayout, c_maxVertBind>	layoutArray_t;

			vertexBufferArray_t	vertexBuffers;
			//layoutArray_t		vertexLayouts;
			resHandle_t			indexBuffer;
			GLenum				indexType; //NOTE: a value of 0 means no index buffer is bound
			glID_t				indexID;
			glID_t				vaoID;
		};

		// END Resources

		class BackendOpenGL35 final: public RenderBackend
		{
		public:
			BackendOpenGL35();
			virtual ~BackendOpenGL35();

			// Callback hooks
			void Init() override;
			resHandle_t	ReserveResourceID(RenderResourceType) override; 

			// Resource mgmt
			template<typename CMD>
			void ProcessResourceCommand(CMD*);

			// Render API TEMP
			void UpdateUniformData(const CBufferDescription& description, void* data, size_t dataSize) override;
			void BindTextureList(const resHandle_t* handles, TextureSamplerSettings* samplers, int arrayLen) override;
			void BindUniformBuffer(unsigned bindingPoint, resHandle_t bufferHandle, size_t offset, size_t size) override;
			//void BindIndexBuffer(const IndexBuffer*) override;
			//void BindVertexBuffer(const VertexBuffer*) override;
			// Render API
			void BindProgram(resHandle_t gpuProgramHandle) override;
			void BindPrimitives(resHandle_t primBufferHandle) override;
			void Draw(GPUPrimitiveType primitiveType, unsigned primitiveCount, unsigned primitiveOffset, unsigned numInstances) override;
			void EnableRT(resHandle_t rtID) override;
			void DisableRT() override;
			void SetRenderState(const RenderState& rs) override;
			//
			void SetViewport(uvec2 start, uvec2 size) override;
			uvec2 GetViewportPosition() const { return { currentViewport.x, currentViewport.y }; }
			uvec2 GetViewportSize() const { return { currentViewport.z, currentViewport.w }; }
			void Clear(ClearFlags) override;
			void SetClearColor(vec4 newColor) override;
			void SetDepthClearValue(float newVal) override;
			void SetDepthBias(float slope, float scale) override;
			// Queries
			queryId_t		CreateQueryObject(QueryType) override;
			bool			IsQueryFinished(queryId_t) override;
			queryResult_t	GetQueryResult(queryId_t) override;
			// Resources
			void			SetBuiltinUniformBufferSlot(hash32_t bufferNameHash, unsigned slot, unsigned bufferSize = 0) override;
			int				GetBuiltinUniformBufferSlot(hash32_t bufferNameHash, unsigned bufferSize) const;
			uint32_t		GetBuiltinUniformUsedSlots() const { return builtinUniformSlots; }
			//
			void ClearShadowstate() override;
			// Private API
			void BindCommands();
			void CheckForErrors();
			//void SetParametersFromHolder(GL35Program& program, const MatParameterHolder& holder);
			void SetSamplerSettings(int texUnit, TextureSamplerSettings settings);
			

			detail::resourceContainer_t<GL35Texture>			textures;
			detail::resourceContainer_t<GL35Program>			gpuPrograms;
			detail::resourceContainer_t<GL35Framebuffer>		framebuffers;
			detail::resourceContainer_t<GL35Buffer>				buffers;
			detail::resourceContainer_t<GL35PrimitiveBuffer>	primBuffers;

			vec_t<TextureSamplerSettings>	texSamplers;
			vec_t<GLint>					texSamplerIDs;
			struct {
				resHandle_t						boundUniformBuffers[16];
				resHandle_t						boundProgram;
				resHandle_t						boundVAO;
				GLenum							boundIndexType;
				GLint							boundFB;
				uvec4							finalViewport; // the viewport last used to render directly to the screen
			} shadowstate; //NOTE: is zero-initialised
			resHandle_t*						boundTextures;
			RenderState							renderState;
			struct {
				unsigned slot;
				hash32_t bufferName;
				unsigned size;
			}									builtinUniformBindings[16];
			uint32_t							builtinUniformSlots = 0u;
			uvec4								currentViewport; // pos xy, size zw
		};

	};
};