#include "GL35Backend.h"

#include <bolts_assert.h>
#include <bolts_log.h>
#include <glm/gtc/type_ptr.hpp>
#include <GL/glew.h>



////////////////////////////////////////////////////////////////////

namespace Bolts {
	namespace Rendering {

		//Thank you Sean Eron Anderson: http://graphics.stanford.edu/~seander/bithacks.html
		// find the log base 2 of 32-bit v
		//TODO: Move to Utilities header
		inline int u32Log2(uint32_t v) {
			static const int MultiplyDeBruijnBitPosition[32] =
			{
				0, 9, 1, 10, 13, 21, 2, 29, 11, 14, 16, 18, 22, 25, 3, 30,
				8, 12, 20, 28, 15, 17, 24, 7, 19, 27, 23, 6, 26, 5, 4, 31
			};

			v |= v >> 1; // first round down to one less than a power of 2 
			v |= v >> 2;
			v |= v >> 4;
			v |= v >> 8;
			v |= v >> 16;

			return MultiplyDeBruijnBitPosition[(uint32_t)(v * 0x07C4ACDDU) >> 27];
		}

		//Bolts to GL mapping
		static const GLenum	g_glTexType[TextureType::_BTT_COUNT] = {
			GL_TEXTURE_1D, //BTT_1D
			GL_TEXTURE_2D, //BTT_2D
			GL_TEXTURE_3D, //BTT_3D
			GL_TEXTURE_CUBE_MAP //BTT_CUBE
		};

		static const GLenum  g_glTexFilter[int(TextureFilterType::_BTFT_COUNT) * int(TextureFilterType::_BTFT_COUNT)] = {
			GL_NEAREST,
			GL_NEAREST,
			GL_LINEAR,
			GL_NEAREST_MIPMAP_NEAREST,
			GL_NEAREST_MIPMAP_NEAREST,
			GL_LINEAR_MIPMAP_NEAREST,
			GL_NEAREST_MIPMAP_LINEAR,
			GL_NEAREST_MIPMAP_LINEAR,
			GL_LINEAR_MIPMAP_LINEAR
		};

		static const GLenum g_glTexWrap[unsigned(TextureWrapMode::_BTWM_COUNT)] = {
			GL_CLAMP_TO_EDGE, //BTWM_CLAMP_EDGE, 
			GL_REPEAT,// BTWM_REPEAT,
			GL_CLAMP_TO_BORDER,// BTWM_CLAMP_BORDER,
			GL_MIRRORED_REPEAT //BTWM_REPEAT_MIRROR
		};

		static const GLenum g_glTexCompareFunc[unsigned(TextureCompareFunc::_BTCF_COUNT)] = {
			GL_LESS,// BTCF_OFF = 0
			GL_LESS,// BTCF_LESS,
			GL_LEQUAL,// BTCF_LEQUAL, 
			GL_GREATER,// BTCF_GREATER, 
			GL_GEQUAL,// BTCF_GEQUAL, 
			GL_EQUAL,// BTCF_EQUAL, 
			GL_NOTEQUAL,// BTCF_NEQUAL, 
			GL_ALWAYS,// BTCF_ALWAYS
		};

		//Textures
		inline GLenum	GetTexMinFilter(TextureFilterType tft, TextureFilterType mft) {
			return g_glTexFilter[ int(tft) + (int(TextureFilterType::_BTFT_COUNT)*int(mft))];
		}

		inline GLenum	GetTexMagFilter(TextureFilterType tft) {
			return g_glTexFilter[ (int) tft];
		}
		inline GLenum	GetTexWrapMode(TextureWrapMode twm) {
			return g_glTexWrap[unsigned(twm)];
		}

		inline GLenum	GetTexCompareFunc(TextureCompareFunc tcf) {
			return g_glTexCompareFunc[unsigned(tcf)];
		}

		static const int c_btfCount = 18;
		static_assert(c_btfCount == _BTF_COUNT, "You need to update the arrays below with the right info for the new format");

		// Texture formats
		static const GLenum g_glInternalFormat[c_btfCount] = {
			GL_RGB,	//BTF_RGB
			GL_RGBA,//BTF_RGBA
			GL_RED,	//BTF_R	
			GL_RG,	//BTF_RG	
			GL_DEPTH_COMPONENT16,// BTF_DEPTH16
			GL_DEPTH_COMPONENT24,// BTF_DEPTH24
			GL_DEPTH_COMPONENT32,// BTF_DEPTH32
			GL_DEPTH_COMPONENT32F,// BTF_DEPTH32F
			GL_COMPRESSED_RGB_S3TC_DXT1_EXT,//BTF_DXT1_RGB
			GL_COMPRESSED_RGBA_S3TC_DXT3_EXT,//BTF_DXT3_RGBA
			GL_COMPRESSED_RGBA_S3TC_DXT5_EXT,//BTF_DXT5_RGBA
			GL_COMPRESSED_RED_RGTC1_EXT ,//BTF_BC4_R
			GL_COMPRESSED_RED_GREEN_RGTC2_EXT ,//BTF_BC5_TG
			GL_R16F,
			GL_RG16F,
			GL_RGB16F,
			GL_RGBA16F,//BTF_RGBA16F
			GL_R11F_G11F_B10F,//BTF_R11G11B10F
		};

		//https://www.khronos.org/opengl/wiki/Pixel_Transfer

		static const GLenum g_formatChannels[c_btfCount] = {
			GL_RGB,	//BTF_RGB
			GL_RGBA,//BTF_RGBA
			GL_RED,	//BTF_R	
			GL_RG,	//BTF_RG
			GL_DEPTH_COMPONENT,// BTF_DEPTH16
			GL_DEPTH_COMPONENT,// BTF_DEPTH24
			GL_DEPTH_COMPONENT,// BTF_DEPTH32
			GL_DEPTH_COMPONENT,// BTF_DEPTH32F
			GL_RGB,//BTF_DXT1_RGB
			GL_RGBA,//BTF_DXT3_RGBA
			GL_RGBA,//BTF_DXT5_RGBA
			GL_R,//BTF_BC4_R
			GL_RG,//BTF_BC5_TG
			GL_RED,//BTF_R16F
			GL_RG,//BTF_RG16F
			GL_RGB,//BTF_RGB16F
			GL_RGBA,//BTF_RGBA16F
			GL_RGB,//BTF_R11G11B10F
		};

		static const GLenum g_formatImplicitChannelType[c_btfCount] = {
			GL_UNSIGNED_BYTE,//BTF_RGB
			GL_UNSIGNED_BYTE,//BTF_RGBA
			GL_UNSIGNED_BYTE,//BTF_R
			GL_UNSIGNED_BYTE,//BTF_RG
			GL_FLOAT,// BTF_DEPTH16 //NOTE: Not sure if using floats for these is correct
			GL_FLOAT,// BTF_DEPTH24
			GL_FLOAT,// BTF_DEPTH32
			GL_FLOAT,// BTF_DEPTH32F
			GL_UNSIGNED_BYTE,//BTF_DXT1_RGB
			GL_UNSIGNED_BYTE,//BTF_DXT3_RGBA
			GL_UNSIGNED_BYTE,//BTF_DXT5_RGBA
			GL_UNSIGNED_BYTE,//BTF_BC4_R
			GL_UNSIGNED_BYTE,//BTF_BC5_TG
			GL_HALF_FLOAT,//BTF_R16F
			GL_HALF_FLOAT,//BTF_RG16F
			GL_HALF_FLOAT,//BTF_RGB16F
			GL_HALF_FLOAT,//BTF_RGBA16F
			GL_HALF_FLOAT,//BTF_R11G11B10F
		};

		//https://www.khronos.org/registry/OpenGL-Refpages/es2.0/xhtml/glPixelStorei.xml
		//https://stackoverflow.com/questions/25350803/non-power-of-two-textures-giving-a-this-application-has-requested-the-runtime
		// index this with width % 8
		static const int g_unpackAlignment[8] = { 8, 1, 2, 1, 4, 1, 2, 1 };

		static const int c_btdfCount = 6;
		static_assert(c_btdfCount == _BTDF_COUNT, "You need to update the arrays below with the right info for the new format");

		static const GLenum g_dataFormatType[c_btdfCount] = {
			0, //BTDF_IMPLICIT
			GL_FLOAT, //BTDF_FLOAT
			GL_HALF_FLOAT, //BTDF_HALF_FLOAT
			GL_UNSIGNED_BYTE, //BTDF_UBYTE
			GL_UNSIGNED_SHORT, //BTDF_USHORT
			GL_UNSIGNED_INT, //BTDF_UINT
		};

		inline bool IsSamplerParameter(GLenum paramType)
		{
			//GL_SAMPLER_2D
			return (paramType >= GL_SAMPLER_1D && paramType <= GL_SAMPLER_2D_SHADOW);
		}

		// Commands

		template<>
		void BackendOpenGL35::ProcessResourceCommand(command::UpdateTextureData* cmd)
		{
			GL35Texture& texture = textures.Get(cmd->handle);
			texture.type = cmd->type;
			texture.dimensions = cmd->dimensions;
			texture.format = cmd->format;
			texture.Bind(0);
			if (texture.type == BTT_3D) {
				texture.Upload3DData(cmd->dataPtr, cmd->dataSize, cmd->dataFormat);
			}
			else {
				texture.Upload2DData(cmd->dataPtr, cmd->dataSize, cmd->dataFormat);
			}
		}

		template<>
		void BackendOpenGL35::ProcessResourceCommand(command::UpdateTextureFiltering* cmd)
		{
			GL35Texture& texture = textures.Get(cmd->handle);
			texture.defaultSampler = cmd->newSettings;
			texture.UpdateFilterState();
		}
		
		template<>
		void BackendOpenGL35::ProcessResourceCommand(command::GenerateTextureMips* cmd)
		{
			GL35Texture& texture = textures.Get(cmd->handle);
			texture.Bind(0);
			texture.GenerateMips();
		}

		///////////////////////////////

		GL35Texture::GL35Texture()
		{

		}

		GL35Texture::~GL35Texture()
		{
			Unload();
		}

		void GL35Texture::Reinit()
		{
			if (glID == 0) {
				glGenTextures(1, &glID.v);
			}
		}

		void GL35Texture::Unload()
		{
			if (glID) {
				glDeleteTextures(1, &glID.v);
				glID = 0;
				Unbind();
			}
		}

		void GL35Texture::Bind(GLint tu)
		{
			BASSERT(tu >= 0);
			glActiveTexture(GL_TEXTURE0 + tu);
			glBindTexture(g_glTexType[type], glID);
		}

		void GL35Texture::Unbind()
		{
			//TODO: Do we need to actually glunbind ? is that a thing ?
		}

		bool GL35Texture::ReadPixels(TextureFormat readFormat, unsigned mipLevel, vec_t<char>& outBuffer) const
		{
			outBuffer.resize( GetMipSizeInBytes( format, BTDF_IMPLICIT, GetMipRes( dimensions, mipLevel)));
			glGetTexImage(g_glTexType[type], mipLevel, g_formatChannels[readFormat], g_formatImplicitChannelType[readFormat], outBuffer.data());
			GLenum err = glGetError();
			if (err != GL_NO_ERROR) {
				//TODO LOG: Texture read pixels error
				return false;
			}
			else {
				return true;
			}
		}

		void GL35Texture::Upload2DData(const void* data, uint64_t dataSize, TextureDataFormat dataFormat)
		{
			BASSERT(type == BTT_2D || type == BTT_CUBE);
			const bool isCube = type == BTT_CUBE;
			GLenum l_surfType = isCube ? GL_TEXTURE_CUBE_MAP_POSITIVE_X : GL_TEXTURE_2D;
			const unsigned numSurfaces = isCube ? 6 : 1;

			const GLenum l_glInternalFormat = g_glInternalFormat[format];
			const GLenum l_channels = g_formatChannels[format];
			const GLenum l_channelFormat = (dataFormat == BTDF_IMPLICIT) ? g_formatImplicitChannelType[format] : g_dataFormatType[dataFormat];
			const unsigned l_mip0Size = GetMipSizeInBytes(format, dataFormat, dimensions);
			BASSERT(l_mip0Size > 0);
			const bool hasMipData = data && ((l_mip0Size*numSurfaces) != dataSize);
			hasMips = hasMipData;
			const unsigned numMips = hasMipData ? GetMipCount(dimensions) : 1;

			const char* l_dataPtr = (char*)data;
			for (unsigned surf = 0; surf < numSurfaces; surf++) {
				for (unsigned mip = 0; mip < numMips; mip++) {
					const uvec3 mipDims = GetMipRes(dimensions, mip);
					const unsigned mipSize = GetMipSizeInBytes(format, dataFormat, mipDims);
					glPixelStorei(GL_UNPACK_ALIGNMENT, g_unpackAlignment[mipDims.x % 8]);

					if (IsCompressedFormat(format))
						glCompressedTexImage2D(l_surfType + surf, mip, l_glInternalFormat, mipDims.x, mipDims.y, 0,
							mipSize, l_dataPtr);
					else
						glTexImage2D(l_surfType + surf, mip, l_glInternalFormat, mipDims.x, mipDims.y, 0,
							l_channels, l_channelFormat, l_dataPtr);

					if (l_dataPtr != nullptr)
						l_dataPtr += mipSize;
				}
			}
		}

		void GL35Texture::Upload3DData(const void* data, uint64_t dataSize, TextureDataFormat dataFormat)
		{
			BASSERT(type == BTT_3D);
			BASSERT_MSG(!IsCompressedFormat(format), "Compressed 3D textures are unsupported");
			GLenum l_surfType = GL_TEXTURE_3D;

			const GLenum l_glInternalFormat = g_glInternalFormat[format];
			const GLenum l_channels = g_formatChannels[format];
			const GLenum l_channelFormat = (dataFormat == BTDF_IMPLICIT) ? g_formatImplicitChannelType[format] : g_dataFormatType[dataFormat];
			const unsigned l_mip0Size = GetMipSizeInBytes(format, dataFormat, dimensions);
			BASSERT(l_mip0Size > 0);
			const bool hasMipData = l_mip0Size == dataSize;
			hasMips = hasMipData;
			const unsigned numMips = hasMipData ? GetMipCount(dimensions) : 1;
			BASSERT(!IsCompressedFormat(format));

			const char* l_dataPtr = (char*)data;
			for (unsigned mip = 0; mip < numMips; mip++) {
				const uvec3 mipDims = GetMipRes(dimensions, mip);
				const unsigned mipSize = GetMipSizeInBytes(format, dataFormat, mipDims);
				glPixelStorei(GL_UNPACK_ALIGNMENT, g_unpackAlignment[mipDims.x % 8]);

				glTexImage3D(l_surfType, 0, l_glInternalFormat, mipDims.x, mipDims.y, mipDims.z, 0,
					l_channels, l_channelFormat, l_dataPtr);

				l_dataPtr += mipSize;
			}
		}

		void GL35Texture::UpdateFilterState()
		{
			Bind(0);
			const GLenum texType = g_glTexType[type];
			glTexParameteri(texType, GL_TEXTURE_MIN_FILTER,
				GetTexMinFilter(defaultSampler.filteringType, defaultSampler.mipFilteringType));
			glTexParameteri(texType, GL_TEXTURE_MAG_FILTER, GetTexMagFilter(defaultSampler.filteringType));
			glTexParameteri(texType, GL_TEXTURE_WRAP_S, GetTexWrapMode(defaultSampler.wrapMode));
			if (type >= BTT_2D) {
				glTexParameteri(texType, GL_TEXTURE_WRAP_T, GetTexWrapMode(defaultSampler.wrapMode));
			}
			if (type >= BTT_3D) {
				glTexParameteri(texType, GL_TEXTURE_WRAP_R, GetTexWrapMode(defaultSampler.wrapMode));
			}
			if (defaultSampler.texCompareMode == TextureCompareFunc::BTCF_OFF) {
				glTexParameteri(texType, GL_TEXTURE_COMPARE_MODE, GL_NONE);
			}
			else {
				glTexParameteri(texType, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
				glTexParameteri(texType, GL_TEXTURE_COMPARE_FUNC, GetTexCompareFunc(defaultSampler.texCompareMode));
			}
			//if ( m_DefaultSettings.m_WrapMode == TextureWrapMode::BTWM_CLAMP_BORDER){
			//	glTexParameterfv(texType, GL_TEXTURE_BORDER_COLOR, (const GLfloat*)&m_DefaultSettings.m_BorderColor);
			//}
		}

		void GL35Texture::GenerateMips()
		{
			//If there is only enough data for level0, then enable automatic mip generation
			glGenerateMipmap(g_glTexType[type]);
			hasMips = true;
		}

		// End GL35Texture

		//using Ptype = ShaderParamType;

		//static const array_t<GLenum, 10> c_ptype2glenum = {
		//	GL_FLOAT,//Ptype::PT_FLOAT, 
		//	GL_DOUBLE,//Ptype::PT_DOUBLE, 
		//	GL_INT,//Ptype::PT_INT32, 
		//	GL_FLOAT_VEC2,//Ptype::PT_VEC2, 
		//	GL_FLOAT_VEC3,//Ptype::PT_VEC3, 
		//	GL_FLOAT_VEC4,//Ptype::PT_VEC4, 
		//	GL_FLOAT_VEC4,//Ptype::PT_QUAT, 
		//	GL_FLOAT_MAT4,//Ptype::PT_MAT4, 
		//	GL_FLOAT_MAT3,//Ptype::PT_MAT3, 
		//	0 //Ptype::PT_TEX // This needs to not match, since it's a more complicated check
		//};
		//static_assert(c_ptype2glenum.size() == ShaderParamType::_SPT_COUNT, "You know what you have to do");

		//void BackendOpenGL35::SetParametersFromHolder(GL35Program& program, const MatParameterHolder& holder)
		//{
		//	driver.CheckForErrors(__LINE__);
		//	auto uniformDataPtr = program.m_uniforms.data();
		//	for(int uniformIdx = 0; uniformIdx < program.m_uniforms.size(); uniformIdx++) {
		//		const auto& uniformDesc = uniformDataPtr[uniformIdx];
		//		const stringHash_t paramName = uniformDesc.paramName;
		//		if (auto paramInfo = holder.TryGetParamInfo(paramName)) {
		//			//Check for type match
		//			if (c_ptype2glenum[paramInfo->m_type] != uniformDesc.paramType) {
		//				if (!IsSamplerParameter(uniformDesc.paramType) ||
		//					paramInfo->m_type != Ptype::PT_TEX ) {
		//					//TODO: Need to distinguish between different texture param types ( 2D, 3D, Cube, Array )
		//
		//					// Parameter type mismatch
		//					BASSERT_MSG(false, "Shader uniform type mismatch");
		//					BOLTS_LERROR() << "[RenderBackend] " << "Shader uniform type mismatch. Got " << paramInfo->m_type << ", wanted " << uniformDesc.paramType;
		//					continue;
		//				}
		//			}
		//			//Set value
		//			switch (paramInfo->m_type)
		//			{
		//			case MatParameterHolder::PT_MAT4:
		//				glUniformMatrix4fv(uniformDesc.paramIndex, 1, false, &holder.GetParamUnsafe<float>(paramInfo->m_index));
		//				break;
		//			case MatParameterHolder::PT_VEC3:
		//				glUniform3fv(uniformDesc.paramIndex, 1, &holder.GetParamUnsafe<float>(paramInfo->m_index));
		//				break;
		//			case MatParameterHolder::PT_FLOAT:
		//				glUniform1f(uniformDesc.paramIndex, holder.GetParamUnsafe<float>(paramInfo->m_index));
		//				break;
		//			case MatParameterHolder::PT_VEC4:
		//			case MatParameterHolder::PT_QUAT:
		//				glUniform4fv(uniformDesc.paramIndex, 1, &holder.GetParamUnsafe<float>(paramInfo->m_index));
		//				break;
		//			case MatParameterHolder::PT_VEC2:
		//				glUniform2fv(uniformDesc.paramIndex, 1, &holder.GetParamUnsafe<float>(paramInfo->m_index));
		//				break;
		//			case MatParameterHolder::PT_MAT3:
		//				glUniformMatrix3fv(uniformDesc.paramIndex, 1, false, &holder.GetParamUnsafe<float>(paramInfo->m_index));
		//				break;
		//			case MatParameterHolder::PT_INT32:
		//				glUniform1i(uniformDesc.paramIndex, GLint(holder.GetParamUnsafe<int32_t>(paramInfo->m_index)));
		//				break;
		//			case MatParameterHolder::PT_TEX:
		//			{
		//				//TODO: Need to distinguish between different texture param types ( 2D, 3D, Cube, Array )
		//				// Set texture unit and bind texture
		//				const char unitIdx = program.m_texUnits[uniformIdx];
		//				auto& texParam = holder.GetParamUnsafe<MatParameterHolder::TexParamInfo>(paramInfo->m_index);
		//				if (!texParam.handle.IsValid()) {
		//					BASSERT_MSG(false, "Missing texture bound to shader param");
		//					break;
		//				}
		//				// Set texture unit
		//				glUniform1i(uniformDesc.paramIndex, GLint(unitIdx));
		//				//
		//				auto& glTex = textures.Get(texParam.handle);
		//				// Bind sampler
		//				SetSamplerSettings(unitIdx, !texParam.useDefault ? texParam.sampler : glTex.defaultSampler);
		//				// Bind tex
		//				//TODO: Don't rebind tex if matching old unit
		//				glTex.Bind(unitIdx);
		//				break;
		//			}
		//			default:
		//				BASSERT_MSG(false, "Shader parameter type unimplemented");
		//			}
		//		}
		//	}
		//	driver.CheckForErrors(__LINE__);
		//}

		void BackendOpenGL35::SetSamplerSettings(int texUnit, TextureSamplerSettings settings)
		{
			// Look for another sampler with the same settings
			for (unsigned i = 0; i < texSamplers.size(); i++) {
				if (memcmp(&texSamplers[i], &settings, sizeof(settings)) == 0) {
					glBindSampler(texUnit, texSamplerIDs[i]);
					return;
				}
			}
			//
			GLuint samplerID = 0;
			glGenSamplers(1, &samplerID);
			glSamplerParameteri(samplerID, GL_TEXTURE_MIN_FILTER,
				GetTexMinFilter(settings.filteringType, settings.mipFilteringType));
			glSamplerParameteri(samplerID, GL_TEXTURE_MAG_FILTER, GetTexMagFilter(settings.filteringType));
			glSamplerParameteri(samplerID, GL_TEXTURE_WRAP_S, GetTexWrapMode(settings.wrapMode));
			glSamplerParameteri(samplerID, GL_TEXTURE_WRAP_T, GetTexWrapMode(settings.wrapMode));
			glSamplerParameteri(samplerID, GL_TEXTURE_WRAP_R, GetTexWrapMode(settings.wrapMode));
			if (settings.texCompareMode == TextureCompareFunc::BTCF_OFF) {
				glSamplerParameteri(samplerID, GL_TEXTURE_COMPARE_MODE, GL_NONE);
			}
			else {
				glSamplerParameteri(samplerID, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
				glSamplerParameteri(samplerID, GL_TEXTURE_COMPARE_FUNC, GetTexCompareFunc(settings.texCompareMode));
			}
			texSamplers.push_back(settings);
			texSamplerIDs.push_back(samplerID);
			glBindSampler(texUnit, samplerID);
		}

	};
};