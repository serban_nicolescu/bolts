#include "GL35Backend.h"
#include <GL/glew.h>

namespace Bolts {
	namespace Rendering {

		static const GLenum g_GLShaderType[_BGST_COUNT] = {
			GL_VERTEX_SHADER,
			GL_FRAGMENT_SHADER,
			GL_GEOMETRY_SHADER,
			GL_COMPUTE_SHADER
		};

		static const char* g_GLShaderTypeName[_BGST_COUNT] = {
			"Vertex",
			"Fragment",
			"Geometry",
			"Compute"
		};

		static const char g_ShaderParamAlign[_SPT_COUNT] = {
			sizeof(GLfloat),//SPT_FLOAT = 0, 
			sizeof(GLdouble),//SPT_DOUBLE, 
			sizeof(GLfloat),//SPT_INT32, 
			2 * sizeof(GLfloat),//SPT_VEC2, 
			4 * sizeof(GLfloat),//SPT_VEC3, 
			4 * sizeof(GLfloat),//SPT_VEC4, 
			4 * sizeof(GLfloat),//SPT_MAT4, 
			4 * sizeof(GLfloat),//SPT_MAT34,
			4 * sizeof(GLfloat),//SPT_MAT3, 
			sizeof(GLfloat),//SPT_TEX
		};

		static const char g_ShaderParamSize[_SPT_COUNT] = {
			sizeof(float),//SPT_FLOAT = 0, 
			sizeof(double),//SPT_DOUBLE, 
			sizeof(uint32_t),//SPT_INT32, 
			2 * sizeof(float),//SPT_VEC2, 
			3 * sizeof(float),//SPT_VEC3, 
			4 * sizeof(float),//SPT_VEC4, 
			16 * sizeof(float),//SPT_MAT4, 
			12 * sizeof(float),//SPT_MAT34,
			9 * sizeof(float),//SPT_MAT3, 
			sizeof(uint32_t),//SPT_TEX
		};

		static array_t< std::pair< GLuint, const GLchar *>, 12>	g_GPUBuiltinAttributeNames = {
			std::make_pair((GLuint)BGSS_POSITION, "a_posL"),
			std::make_pair((GLuint)BGSS_NORMALS, "a_normals"),
			std::make_pair((GLuint)BGSS_TANGENTS, "a_tangents"),
			std::make_pair((GLuint)BGSS_COLOR0, "a_color"),
			std::make_pair((GLuint)BGSS_COLOR0, "a_color0"),
			std::make_pair((GLuint)BGSS_COLOR1, "a_color1"),
			std::make_pair((GLuint)BGSS_TEXTURE01, "a_uv"),
			std::make_pair((GLuint)BGSS_TEXTURE01, "a_uv0"),
			std::make_pair((GLuint)BGSS_TEXTURE01, "a_uv01"),
			std::make_pair((GLuint)BGSS_TEXTURE23, "a_uv23"),
			std::make_pair((GLuint)BGSS_BONEI, "a_boneIdx"),
			std::make_pair((GLuint)BGSS_BONEW, "a_boneWeight"),
		};

		static inline ShaderParamType GLTypeToSPT(GLint paramType)
		{
			switch (paramType) {
				case GL_FLOAT: return SPT_FLOAT;
				case GL_DOUBLE: return SPT_DOUBLE;
				case GL_INT: return SPT_INT32;
				case GL_FLOAT_VEC2: return SPT_VEC2;
				case GL_FLOAT_VEC3: return SPT_VEC3;
				case GL_FLOAT_VEC4: return SPT_VEC4;
				case GL_FLOAT_MAT4: return SPT_MAT4;
				case GL_FLOAT_MAT3: return SPT_MAT3;
				case GL_FLOAT_MAT3x4: return SPT_MAT34;
			}
			return SPT_TEX;
		}

		static inline bool IsSamplerParameter(GLint paramType)
		{
			//GL_SAMPLER_2D
			return (paramType >= GL_SAMPLER_1D && paramType <= GL_SAMPLER_2D_SHADOW);
		}

		static GLuint CompileShaderCode(ShaderType shaderType, const char *sourceShaderCode, int sourceCodeSizeInBytes, const char* firstDefine, size_t defineSizeInBytes)
		{
			if (!sourceShaderCode)
				return 0;
			GLuint shaderID = glCreateShader(g_GLShaderType[shaderType]);
			if (shaderID == 0) {
				BOLTS_LERROR() << "Could not create shader of type: " << shaderType;
				return 0;
			}

			//TODO: static alloc
			static str_t finalShaderCodeBuffer;
			const char* finalShaderCode = sourceShaderCode;
			GLint codeSize = sourceCodeSizeInBytes;
			finalShaderCodeBuffer.clear();
			finalShaderCodeBuffer.reserve(200 * 1024);
			finalShaderCodeBuffer.append("#version 400 core\n");
			// Append defines
			if (firstDefine) {
				//Parse define string
				const char* dStart = firstDefine;
				while (dStart < firstDefine + defineSizeInBytes ) {
					const int defineLen = (int)strlen(dStart);
					finalShaderCodeBuffer.append("#define ");
					finalShaderCodeBuffer.append(dStart, defineLen);
					finalShaderCodeBuffer.append("\n");
					dStart += defineLen + 1;
				}
			}
			finalShaderCodeBuffer.append(sourceShaderCode, sourceCodeSizeInBytes);
			finalShaderCode = finalShaderCodeBuffer.c_str();
			codeSize = (int)finalShaderCodeBuffer.size();
			// Compile shader
			glShaderSource(shaderID, 1, &finalShaderCode, (GLint *)&codeSize);
			glCompileShader(shaderID);
			//
			GLint result;
			glGetShaderiv(shaderID, GL_COMPILE_STATUS, &result);
			if (result == GL_FALSE) {
				// Compilation error
				BOLTS_LDEBUG() << "Source was: ";
				BOLTS_LDEBUG() << finalShaderCode;
				BOLTS_LERROR() << "Error compiling shader of type: " << g_GLShaderTypeName[shaderType];
				const unsigned k_maxLogLength = 10000;
				char infoLog[k_maxLogLength];
				GLint logLength;
				glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &logLength);
				glGetShaderInfoLog(shaderID, k_maxLogLength, NULL, infoLog);
				BOLTS_LERROR() << "Log:" << infoLog;
				BASSERT(logLength < k_maxLogLength);
				if (logLength > k_maxLogLength) {
					BOLTS_LERROR() << "Not enough buffer space for entire error log.";
				}
				//
				glDeleteShader(shaderID);
				shaderID = 0;
				BASSERT(false);
			}
			//
			return shaderID;
		}

		template<>
		void BackendOpenGL35::ProcessResourceCommand(command::CompileAndLoadProgram* cmd)
		{
			GL35Program& program = gpuPrograms.Get(cmd->handle);
			GL35Program::Shaders shd;
			for (unsigned i = 0; i < GLM_COUNTOF(shd.shaderID); i++)
				shd.shaderID[i] = CompileShaderCode(cmd->shaderTypes[i], cmd->shaderCode[i], cmd->shaderCodeSize[i], cmd->defines, cmd->definesSize);
			BASSERT(cmd->owningProgram);
			if (program.Link(shd))
			{
				program.LoadProgramParameterInfo(this);
				// copy over uniform buffer descriptions
				for (unsigned i = 0; i < countof(program.m_cbuffers); i++) {
					if (program.m_cbuffers[i].bufferName)
						cmd->owningProgram->cbuffers[i].CopyFrom(program.m_cbuffers[i]);
				}
				// Copy over uniform buffer details
				CBufferDescription& dstDesc = cmd->owningProgram->cbuffers[GPUProgram::c_maxNumUniformBuffers - 1];
				CBufferDescription& srcDesc = program.m_uniforms;
				dstDesc.CopyFrom(srcDesc);
				cmd->owningProgram->OnLoadFinished();
			}
			else {
				// Set owner in an error state
				cmd->owningProgram->SetAssetState(assets::EAS_ERROR);
			}
		}

		bool GL35Program::Link(Shaders shaders)
		{
			//Clean up previous state
			Unload();
			//
			m_programID = glCreateProgram();
			BASSERT(m_programID != 0);
			if (m_programID == 0) {
				BOLTS_LERROR() << "Error linking shading program: not a valid program ID 0";
				return false;
			}
			// Link in shaders
			for(unsigned i = 0; i < GLM_COUNTOF(shaders.shaderID); i++)
				if(shaders.shaderID[i])
					glAttachShader(m_programID, shaders.shaderID[i]);
			// Set vertex attribute locations
			for (auto valuePair : g_GPUBuiltinAttributeNames) {
				glBindAttribLocation(m_programID, valuePair.first, valuePair.second);
			}
			// Link
			glLinkProgram(m_programID);
			// Check
			GLint linked;
			glGetProgramiv(m_programID, GL_LINK_STATUS, &linked);
			if (!linked) {
				glGetProgramiv(m_programID, GL_INFO_LOG_LENGTH, &linked);
				char *infoLog = new char[linked];
				glGetProgramInfoLog(m_programID, linked, NULL, infoLog);
				BOLTS_LERROR() << "Error linking shading program: " << infoLog;
				delete[] infoLog;
				return false;
			}
			// After linking you don't need shaders anymore
			for (unsigned i = 0; i < GLM_COUNTOF(shaders.shaderID); i++)
				if (shaders.shaderID[i])
					glDeleteShader(shaders.shaderID[i]);
			return true;
		}

		static void ParseBlockUniforms(GLuint pid, const GLuint uniformIndices[256], CBufferDescription& cbuffer)
		{
			//https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glGetActiveUniformsiv.xhtml
			GLint uniformParams[256];
			bool uniformErrors[256];
			memset(uniformErrors, 0, 256);
			const GLsizei numUniforms = cbuffer.numEntries;
			cbuffer.exactDataSize = 0;
			if (numUniforms == 0)
				return;
			// types
			glGetActiveUniformsiv(pid, numUniforms, uniformIndices, GL_UNIFORM_TYPE, uniformParams);
			for (int i = 0; i < numUniforms; i++) {
				ShaderParamType spt = GLTypeToSPT(uniformParams[i]);
				if (spt == SPT_TEX && !IsSamplerParameter(uniformParams[i])) {
					// Unrecognized uniform type
					uniformErrors[i] = true;
					spt = SPT_FLOAT;
				}
				cbuffer.types[i] = spt;
			}
			// names
			char nameBuff[256];
			for (int i = 0; i < numUniforms; i++) {
				GLsizei len;
				glGetActiveUniformName(pid, uniformIndices[i], sizeof(nameBuff), &len, nameBuff);
				cbuffer.names[i] = HashString(nameBuff, len);
				RegisterStringHash(cbuffer.names[i], nameBuff, len);
				if (uniformErrors[i]) {
					BOLTS_LERROR() << "[ShaderCompilation] " "Unimplemented uniform type: " << nameBuff;
					BASSERT(false);
				}
			}
			uint32_t bufferDataSize = 0; // exact size required to store data
			// array sizes
			glGetActiveUniformsiv(pid, numUniforms, uniformIndices, GL_UNIFORM_SIZE, uniformParams);
			for (int i = 0; i < numUniforms; i++)
				cbuffer.arraySizes[i] = (uint16_t)uniformParams[i];
			// offsets
			glGetActiveUniformsiv(pid, numUniforms, uniformIndices, GL_UNIFORM_OFFSET, uniformParams);
			if (uniformParams[0] != -1) {
				// uniform blocks
				for (int i = 0; i < numUniforms; i++) {
					cbuffer.offsets[i] = (uint16_t)uniformParams[i];
					const ShaderParamType spt = cbuffer.types[i];
					bufferDataSize = max(bufferDataSize, cbuffer.offsets[i] + (cbuffer.arraySizes[i] * g_ShaderParamSize[spt]));
				}
			}
			else {
				// old-style uniforms, need to work out offsets manually
				for (int i = 0; i < numUniforms; i++) {
					const ShaderParamType spt = cbuffer.types[i];
					bufferDataSize += alignAdjust(bufferDataSize, uniformParams[i] == 1 ? g_ShaderParamAlign[spt] : 4 * sizeof(GLfloat));
					cbuffer.offsets[i] = bufferDataSize;
					bufferDataSize += cbuffer.arraySizes[i] * g_ShaderParamSize[spt];
				}
			}
			BASSERT(cbuffer.exactDataSize == 0 || (cbuffer.exactDataSize == bufferDataSize));
			cbuffer.exactDataSize = bufferDataSize;
		}

		void GL35Program::LoadProgramParameterInfo(BackendOpenGL35* backend)
		{
			GLint value;
			glGetProgramiv(m_programID, GL_ACTIVE_ATTRIBUTES, &value);
			m_numAttributes = (uint8_t)value;
			m_attributes.clear();
			m_attributes.reserve(m_numAttributes);
			// attributes
			char nameHolder[128];
			{
				AttribDesc desc;
				for (int i = 0; i < m_numAttributes; ++i) {
					glGetActiveAttrib(m_programID, i, sizeof(nameHolder), NULL, &desc.paramSize, &desc.paramType, nameHolder);
					desc.paramName = Bolts::HashString(nameHolder);
					m_attributes.push_back(desc);
				}
			}
			std::sort(m_attributes.begin(), m_attributes.end());
			
			//TODO: Use this when you switch to GL45/DX11
			//https://www.khronos.org/opengl/wiki/Program_Introspection#Interface_query
			//https://www.khronos.org/registry/OpenGL/specs/gl/glspec45.core.pdf#page=159
			// cbuffers
			glGetProgramiv(m_programID, GL_ACTIVE_UNIFORM_BLOCKS, &value);
			const uint16_t numCbuffers = (uint16_t)value;
			BASSERT(numCbuffers < countof(m_cbuffers));
			GLuint uniformIndices[256];
			GLint uniformParams[256];
			auto uniformSlotMask = backend->GetBuiltinUniformUsedSlots();
			unsigned freeUniformSlot = 0;
			for (unsigned i = 0; i < numCbuffers; i++) {
				auto& cbuffer = m_cbuffers[i];
				// parse info
				GLint out = 0;
				glGetActiveUniformBlockiv(m_programID, i, GL_UNIFORM_BLOCK_DATA_SIZE, &out);
				cbuffer.bufferDataSize = out;
				out = 0;
				glGetActiveUniformBlockiv(m_programID, i, GL_UNIFORM_BLOCK_ACTIVE_UNIFORMS, &out);
				cbuffer.SetSize((uint16_t)out);
				GLsizei len;
				glGetActiveUniformBlockName(m_programID, i, sizeof(nameHolder), &len, nameHolder);
				cbuffer.bufferName = HashString(nameHolder);
				GLuint idx = glGetUniformBlockIndex(m_programID, nameHolder);
				glGetActiveUniformBlockiv(m_programID, i, GL_UNIFORM_BLOCK_ACTIVE_UNIFORM_INDICES, uniformParams);
				for (int j = 0; j < cbuffer.numEntries; j++)
					uniformIndices[j] = uniformParams[j];
				ParseBlockUniforms(m_programID, uniformIndices, m_cbuffers[i]);
				// set binding location
				int builtinBindingLocation = backend->GetBuiltinUniformBufferSlot(cbuffer.bufferName, cbuffer.exactDataSize);
				while ( ((1 << freeUniformSlot) & uniformSlotMask) && 
						freeUniformSlot < sizeof(uniformSlotMask)*8)
					freeUniformSlot++;
				if (builtinBindingLocation == -1)
					cbuffer.bindingLocation = (char)freeUniformSlot++;
				else
					cbuffer.bindingLocation = (char)builtinBindingLocation;
				glUniformBlockBinding(m_programID, idx, cbuffer.bindingLocation);
			}
			// load uniforms in non cbuffers
			/*
			// old code
			{
				UniformDesc desc;
				for (uint16_t i = 0; i < numAllUniforms; ++i) {
					glGetActiveUniform(m_programID, i, 128, NULL, &desc.paramSize, &desc.paramType, nameHolder);
					desc.paramIndex = i;
					desc.paramName = Bolts::HashString(nameHolder);
					m_uniforms.push_back(desc);
				}
			}
			std::sort(m_uniforms.begin(), m_uniforms.end());
			*/
			glGetProgramiv(m_programID, GL_ACTIVE_UNIFORMS, &value);
			uint16_t numAllUniforms = (uint16_t)value;
			BASSERT(numAllUniforms < 256);
			GLint numFreeUniforms = 0;
			{
				for (int i = 0; i < numAllUniforms; i++)
					uniformIndices[i] = i;
				glGetActiveUniformsiv(m_programID, numAllUniforms, uniformIndices, GL_UNIFORM_BLOCK_INDEX, uniformParams);
				for (int i = 0; i < numAllUniforms; i++) {
					if (uniformParams[i] == -1) {
						uniformIndices[numFreeUniforms] = i;
						numFreeUniforms++;
					}
				}
			}
			m_uniforms.SetSize( (uint16_t)numFreeUniforms);
			m_uniforms.exactDataSize = 0;
			ParseBlockUniforms(m_programID, uniformIndices, m_uniforms);
			m_uniforms.bufferDataSize = m_uniforms.exactDataSize;
		}

		/*
		void GL35Program::PrintParameterInfo()
		{
			BOLTS_LINFO() << " ------- Program Info LOG -------";
			BOLTS_LINFO() << " Active Attributes: " << (int)m_numAttributes;
			//BOLTS_LINFO() << " Active Uniforms: " << (int)numAllUniforms;
			BOLTS_LINFO() << " -- Attributes --";
			for (int i = 0; i < m_numAttributes; ++i) {
				BOLTS_LINFO() << " " << i << " - \"" << m_attributes[i].paramName << "\" Size: " << m_attributes[i].paramSize << " Type: " << m_attributes[i].paramType;
			}
			//BOLTS_LINFO() << " -- Uniforms --";
			//for (int i = 0; i < numAllUniforms; ++i) {
			//	BOLTS_LINFO() << " " << i << " - \"" << m_uniforms[i].paramName << "\" Size: " << m_uniforms[i].paramSize << " Type: " << m_uniforms[i].paramType;
			//}
		}
		*/

		void GL35Program::Reinit()
		{
			// No need to do anything
		}

		void GL35Program::Unload()
		{
			if (m_programID != 0) {
				glDeleteProgram(m_programID);
				m_programID = 0;
			}
		}
	};
};
