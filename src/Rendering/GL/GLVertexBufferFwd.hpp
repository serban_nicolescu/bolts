#pragma once
#include "Bolts/Core/Types.h"
namespace Bolts {
	namespace Rendering {

		class GLVertexBuffer;
		typedef intrusivePtr_t< GLVertexBuffer> GLVertexBufferPtr;
		typedef intrusivePtr_t< const GLVertexBuffer> GLVertexBufferCPtr;
	};
};
