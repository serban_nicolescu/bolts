#include "GL35Backend.h"
#include <GL/glew.h>

namespace Bolts {
	namespace Rendering {

		//https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glDrawBuffers.xhtml

		// Commands

		template<>
		void BackendOpenGL35::ProcessResourceCommand(command::UpdateRTBindings* cmd)
		{
			GL35Framebuffer& fb = framebuffers.Get(cmd->handle);
			fb.Reinit(*this, *cmd);
		}

		template<>
		void BackendOpenGL35::ProcessResourceCommand(command::ResizeRT* cmd)
		{
			GL35Framebuffer& fb = framebuffers.Get(cmd->handle);
			fb.Resize(*this, cmd->newSize);
		}
		///////////////////////////

		GL35Framebuffer::GL35Framebuffer() {}
		GL35Framebuffer::~GL35Framebuffer()
		{
			Unload();
		}

		void GL35Framebuffer::Reinit(BackendOpenGL35& backend, const command::UpdateRTBindings& bindInfo)
		{
			//TODO: Query GL_MAX_DRAW_BUFFERS to know if number of RTs is supported
			static_assert(sizeof(colorTextures) == sizeof(bindInfo.colorBindings), "These need to match");

			if (framebufferGLID == 0)
				glGenFramebuffers(1, &framebufferGLID.v);
			depthTexture = bindInfo.depthBinding;
			memcpy(colorTextures, bindInfo.colorBindings, sizeof(colorTextures));

			//Setup
			//Create the Depth render buffer
			if (bindInfo.wantDepth && !depthTexture.IsValid()) {
				if (depthRenderbufferGLID == 0) {
					glGenRenderbuffers(1, &depthRenderbufferGLID.v);
				}
			}
			else {
				if (depthRenderbufferGLID) {
					glDeleteRenderbuffers(1, &depthRenderbufferGLID.v);
					depthRenderbufferGLID = 0;
				}
			}
			GLint previousFB;
			glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &previousFB);//Store old FB to maintain state
			//Attach and test
			glBindFramebuffer(GL_FRAMEBUFFER, framebufferGLID);
			// SetupAttachments
			if (depthRenderbufferGLID)
				glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbufferGLID);
			else if (depthTexture.IsValid()) {
				auto& depthTex = backend.textures.Get(depthTexture);
				BASSERT(depthTex.glID);
				glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTex.glID, 0);
			}

			int i = 0;
			while(colorTextures[i].IsValid()) {
				auto& colorTex = backend.textures.Get(colorTextures[i]);
				BASSERT(colorTex.glID);
				if (colorTex.type == BTT_2D)
					glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, colorTex.glID, 0);
				else if (colorTex.type == BTT_CUBE)
					glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_CUBE_MAP_POSITIVE_X + bindInfo.boundTarget[i], colorTex.glID, 0);
				else
					BASSERT(false); // Expected a 2D or Cubemap texture
				i++;
			}
			// Alloc storage
			//TODO: Do we need the size before checking for validity ?
			Resize(backend, bindInfo.resolution);
			// ValidateFramebuffer
			isFramebufferValid = true;
			const GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
			if (status != GL_FRAMEBUFFER_COMPLETE) {
				const char* errorMsg = nullptr;
				switch (status) {
				case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
					errorMsg = "GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT";
					break;
				//case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
				//	break;
				case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
					errorMsg = "GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT";
					break;
				case GL_FRAMEBUFFER_UNSUPPORTED:
					errorMsg = "GL_FRAMEBUFFER_UNSUPPORTED";
					break;
				}
				BOLTS_LERROR() << "[Rendering] Invalid framebuffer. GL Error is: " << status << " | " << errorMsg;
				isFramebufferValid = false;
				BASSERT(false);
			}
			//
			glBindFramebuffer(GL_FRAMEBUFFER, GLuint(previousFB));//Restore previously set FB
		}

		void GL35Framebuffer::Unload()
		{
			if (framebufferGLID != 0) {
				glDeleteFramebuffers(1, &framebufferGLID.v);
				framebufferGLID = 0;
			}
			if (depthRenderbufferGLID != 0) {
				glDeleteRenderbuffers(1, &depthRenderbufferGLID.v);
				depthRenderbufferGLID = 0;
			}
		}

		void GL35Framebuffer::Resize(BackendOpenGL35& /*backend*/, uvec2 newSize)
		{
			//if (depthTexture.IsValid()) {
			//	auto& depthTex = backend.textures.Get(depthTexture);
			//	if (depthTex.defaultSampler.filteringType != TextureFilterType::BTFT_NEAREST) {
			//		depthTex.defaultSampler.filteringType = TextureFilterType::BTFT_NEAREST;
			//		depthTex.UpdateFilterState();
			//	}
			//	if (uvec2(depthTex.dimensions) != newSize) {
			//		// ?
			//		//tex->SetFiltering(TextureFilterType::BTFT_NEAREST);
			//		//tex->SetMipFiltering(TextureFilterType::BTFT_NEAREST);
			//		depthTex.dimensions = uvec3(newSize,1);
			//		depthTex.Bind(0);
			//		depthTex.Upload2DData(nullptr, 0);
			//	}
			//}
			//int i = 0;
			//while (colorTextures[i].IsValid()) {
			//	auto& colorTex = backend.textures.Get(colorTextures[i]);
			//	if (colorTex.defaultSampler.filteringType != TextureFilterType::BTFT_NEAREST) {
			//		colorTex.defaultSampler.filteringType = TextureFilterType::BTFT_NEAREST;
			//		colorTex.UpdateFilterState();
			//	}
			//	if (uvec2(colorTex.dimensions) != newSize) {
			//		// ?
			//		//tex->SetFiltering(TextureFilterType::BTFT_NEAREST);
			//		//tex->SetMipFiltering(TextureFilterType::BTFT_NEAREST);
			//		colorTex.dimensions = uvec3(newSize, 1);
			//		colorTex.Bind(0);
			//		colorTex.Upload2DData(nullptr, 0);
			//	}
			//	i++;
			//}

			// update depth RB size
			if (depthRenderbufferGLID) {
				if (newSize != resolution) {
					glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbufferGLID);
					//NOTE: We're using 32F depth here
					glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32F, resolution.x, resolution.y);
					glBindRenderbuffer(GL_RENDERBUFFER, 0);
				}
			}
			resolution = newSize;

			//TODO: Do we need to check for anything here ?

			//GLint previousFB;
			//glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &previousFB);//Store old FB to maintain state
			////Attach and test
			//glBindFramebuffer(GL_FRAMEBUFFER, framebufferGLID);
			//const GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
			//if (status != GL_FRAMEBUFFER_COMPLETE) {
			//	const char* errorMsg = nullptr;
			//	switch (status) {
			//		case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
			//			errorMsg = "GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS";
			//			break;
			//	}
			//	BOLTS_LERROR() << "[Rendering] Invalid framebuffer. GL Error is: " << status << " | " << errorMsg;
			//	isFramebufferValid = false;
			//}
			////
			//glBindFramebuffer(GL_FRAMEBUFFER, GLuint(previousFB));//Restore previously set FB
		}

		void GL35Framebuffer::SetActive(BackendOpenGL35&)
		{
			BASSERT(isFramebufferValid);
			int i = 0;
			while (colorTextures[i].IsValid()) {
				//TODO: Figure out why this is needed
				//auto& tex = backend.textures.Get(colorTextures[i]);
				//if (tex->_GetTextureUnit() != -1)
				//	glBindSampler(tex->_GetTextureUnit(), 0);
				i++;
			}
			const int numTex = i;
			//
			glBindFramebuffer(GL_FRAMEBUFFER, framebufferGLID);
			//Enable buffers to be writable for MRT
			const GLenum l_DrawBuffers[] = {
				GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2,
				GL_COLOR_ATTACHMENT3,GL_COLOR_ATTACHMENT4, GL_COLOR_ATTACHMENT5 };
			BASSERT_MSG(numTex <= 6,
				"Max number of color attachments is hardcoded to 6. Feel free to increase it if you need moar");
			glDrawBuffers(numTex < 7 ? numTex : 6, l_DrawBuffers);
		}

		//void GLRenderTarget::Clear()
		//{
		//	BASSERT(IsValid());
		//
		//	GLbitfield mask = (m_colorTextures.size() > 0 ? GL_COLOR_BUFFER_BIT : 0) |
		//		(HasDepthAttachment() ? GL_DEPTH_BUFFER_BIT : 0);
		//	glClear(mask);
		//}

		//bool GLRenderTarget::HasDepthAttachment() const
		//{
		//	return ((m_DepthRenderbufferGLID != 0) || m_depthTexture);
		//}

	};
};
