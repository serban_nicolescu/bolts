#pragma once
#include <bolts_core.h>

namespace Bolts{
	namespace Rendering{

		class GLRenderTarget;
		typedef intrusivePtr_t<GLRenderTarget> GLRenderTargetPtr;

	};
};
