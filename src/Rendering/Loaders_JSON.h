#ifndef Bolts_Loaders_JSON_h__
#define Bolts_Loaders_JSON_h__

#include <rapidjson/document.h>
#include <Rendering/GPUProgramFwd.h>
#include <Rendering/TextureFwd.h>

namespace Bolts {
	namespace Rendering {
		class GLDriver;

		bool LoadGPUProgram(GLDriver&, GPUProgramPtr, const rapidjson::Value&);

	};
};

#endif // Bolts_Loaders_JSON_h__
