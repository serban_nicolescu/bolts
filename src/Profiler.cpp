#include "Profiler.h"
#include "Rendering/RenderBackend.h"
#include "System/system.h"
#include "imgui.h"

namespace Bolts {

	static void cycleUp(uint32_t* v, uint32_t max) {
		*v = (*v + 1) % max;
	}

	static uint32_t cycleDiff(uint32_t a, uint32_t b, uint32_t max)
	{
		return ((max + a) - b) % max;
	}

	//static uint32_t cycleDown(uint32_t* v, uint32_t max) {
	//	*v = (*v - 1) % max;
	//	return *v;
	//}

	static Profiler g_Profiler;
	static Rendering::RenderBackend* g_RenderBackend;

	Profiler& Profiler::Get()
	{
		return g_Profiler;
	}

	void Profiler::Init(Rendering::RenderBackend* rdr)
	{
		BASSERT(rdr);
		g_RenderBackend = rdr;
		g_Profiler.cpuTimers = new Timer[c_TimerBuffer];
		g_Profiler.gpuTimers = new GPUTimer[c_GPUTimerBuffer];
		memset(g_Profiler.cpuTimers, 0, sizeof(*g_Profiler.cpuTimers) * c_TimerBuffer);
		memset(g_Profiler.gpuTimers, 0, sizeof(*g_Profiler.gpuTimers) * c_GPUTimerBuffer);
	}

	void Profiler::Shutdown()
	{
		delete[] g_Profiler.cpuTimers;
		g_Profiler.cpuTimers = nullptr;
		delete[] g_Profiler.gpuTimers;
		g_Profiler.gpuTimers = nullptr;
	}

	void Profiler::FrameStart()
	{
		BASSERT(g_RenderBackend);
		if ( frame == nullptr || 
			(++frame >= frames + c_NumCPUFrames) )
			frame = frames;

		frame->frameNumber = currentFrame;
		frame->timerStart = freeCpuTimer;
		frame->eventStart = freeCpuEvent;
		frame->startTime = System::GetElapsedMicroseconds();

		if (gpuFrame == nullptr ||
			(++gpuFrame >= gpuFrames + c_NumGPUFrames))
			gpuFrame = gpuFrames;
		BASSERT(gpuFrame->isComplete);
		gpuFrame->frameNumber = currentFrame;
		gpuFrame->timerStart = freeGpuTimer;
		gpuFrame->isComplete = false;
		gpuFrame->startQueryId = g_RenderBackend->CreateQueryObject(Rendering::BQT_TIMESTAMP);
		gpuFrame->startTimeCPU = frame->startTime;
	}

	void Profiler::FrameEnd()
	{
		frame->timerCount = cycleDiff(freeCpuTimer, frame->timerStart, c_TimerBuffer);
		frame->eventCount = cycleDiff(freeCpuEvent, frame->eventCount, c_TimerBuffer);
		frame->endTime = System::GetElapsedMicroseconds();

		gpuFrame->endQueryId = g_RenderBackend->CreateQueryObject(Rendering::BQT_TIMESTAMP);
		gpuFrame->timerCount = cycleDiff(freeGpuTimer, gpuFrame->timerStart, c_GPUTimerBuffer);

		auto otherGpuFrame = gpuFrames;
		while (otherGpuFrame < gpuFrames + c_NumGPUFrames) {
			if (!otherGpuFrame->isComplete &&
				g_RenderBackend->IsQueryFinished(otherGpuFrame->endQueryId)) 
			{
				otherGpuFrame->isComplete = true;
				otherGpuFrame->startTimeGPU = g_RenderBackend->GetQueryResult(otherGpuFrame->startQueryId);
				otherGpuFrame->endTimeGPU = g_RenderBackend->GetQueryResult(otherGpuFrame->endQueryId);
				//BASSERT(cycleDiff(gpuFrame->timerStart, otherGpuFrame->timerStart, c_GPUTimerBuffer) <= gpuFrame->timerCount);
				//BASSERT(cycleDiff(freeGpuTimer, otherGpuFrame->timerStart, c_GPUTimerBuffer) >= otherGpuFrame->timerCount);
				// if the final query is finished then we can read the others
				uint32_t numTimers = otherGpuFrame->timerCount;
				for (uint32_t i = 0; i < numTimers; i++) {
					auto idx = (otherGpuFrame->timerStart + i) % c_GPUTimerBuffer;
					GPUTimer* tmr = gpuTimers + idx;
					tmr->time = g_RenderBackend->GetQueryResult(tmr->startQueryId) - otherGpuFrame->startTimeGPU;
					tmr->endTime = g_RenderBackend->GetQueryResult(tmr->endQueryId) - otherGpuFrame->startTimeGPU;
					tmr->startQueryId = 0;
					tmr->endQueryId = 0;
				}
			}
			otherGpuFrame++;
		}
		currentFrame++;
	}

	inline uint32_t cscramble(uint32_t v)
	{
		v += 0x00009e37;
		v = v * (v + v);
		return v ^ (0x9e3779b9u + (v << 6) + (v >> 2));
	}

	int Profiler::PushCPUTimer(const char* name, uint32_t color)
	{
		Timer* tmr = cpuTimers + freeCpuTimer;
		//if (color == 0)
		//	color = cscramble( cycleDiff(freeCpuTimer, frame->timerStart, c_TimerBuffer) );
		tmr->color = color;
		strncpy(tmr->name, name, c_MaxTimerName);
		tmr->time = System::GetElapsedMicroseconds() - frame->startTime;
		tmr->endTime = 0;

		//TODO: This needs to be atomic
		cycleUp(&freeCpuTimer, c_TimerBuffer);
		BASSERT(freeCpuTimer != frame->timerStart);

		return (int)(tmr - cpuTimers); // return idx so we know what to pop
	}

	void Profiler::PopCPUTimer(int timerID)
	{
		BASSERT(timerID < c_TimerBuffer && timerID >= 0);
		Timer* tmr = cpuTimers + timerID;
		BASSERT(tmr->endTime == 0);
		tmr->endTime = System::GetElapsedMicroseconds() - frame->startTime;
	}

	int Profiler::PushGPUTimer(const char * name, uint32_t color)
	{
		GPUTimer* tmr = gpuTimers + freeGpuTimer;
		//if (color == 0)
		//	color = cscramble(cycleDiff(freeGpuTimer, gpuFrame->timerStart, c_GPUTimerBuffer));

		//TODO: if we find that we stomped over active timer we should release old timer queries
		tmr->color = color;
		strncpy(tmr->name, name, c_MaxTimerName);
		tmr->time = 0;
		tmr->endTime = 0;
		tmr->startQueryId = g_RenderBackend->CreateQueryObject(Rendering::BQT_TIMESTAMP);
		tmr->endQueryId = 0;
		//TODO: This needs to be atomic ?
		cycleUp(&freeGpuTimer, c_GPUTimerBuffer);
		BASSERT(freeGpuTimer != gpuFrame->timerStart);
		return (int)(tmr - gpuTimers); // return idx so we know what to pop
	}

	void Profiler::PopGPUTimer(int timerID)
	{
		BASSERT(timerID < c_GPUTimerBuffer && timerID >= 0);
		GPUTimer* tmr = gpuTimers + timerID;
		BASSERT(tmr->endQueryId == 0);
		tmr->endQueryId = g_RenderBackend->CreateQueryObject(Rendering::BQT_TIMESTAMP);
	}

	void Profiler::DrawCPUProfilingInfo()
	{
		auto profiledFrame = frame - 1;
		if (profiledFrame < frames)
			profiledFrame = frames + c_NumCPUFrames - 1;

		const int c_maxNesting = 8;
		uint64_t nestingEndTimes[c_maxNesting] = {};
		nestingEndTimes[0] = uint64_t(-1);
		uint32_t nestLevel = 1;

		float frameDurationMs = Bolts::System::MicroToMs(profiledFrame->endTime - profiledFrame->startTime);
		ImGui::Text("CPU - %.3f", frameDurationMs);
		ImGui::Separator();

		uint32_t numTimers = profiledFrame->timerCount;
		for (uint32_t i = 0; i < numTimers; i++) {
			auto idx = (profiledFrame->timerStart + i) % c_TimerBuffer;
			Timer* tmr = cpuTimers + idx;

			if (tmr->time < nestingEndTimes[nestLevel]) {
				cycleUp(&nestLevel, c_maxNesting);
				ImGui::Indent();
			}
			while (nestingEndTimes[nestLevel - 1] <= tmr->time) {
				ImGui::Unindent();
				nestLevel--;
			}
			nestingEndTimes[nestLevel] = tmr->endTime;

			uint32_t color = tmr->color;
			if (color == 0)
				color = HashString(tmr->name, countof(tmr->name));
			ImGui::PushStyleColor(ImGuiCol_Text, color | 0xFF303030);
			float durationInMs = Bolts::System::MicroToMs(tmr->endTime - tmr->time);
			ImGui::Text("%.3f - %s", durationInMs, tmr->name);
			ImGui::PopStyleColor();
		}
	
		// Find most recently completed GPU frame
		GPUFrameInfo* profiledGpuFrame = nullptr;
		uint32_t mostRecentGPUFrame = 0;
		{
			GPUFrameInfo* otherGpuFrame = gpuFrames;
			while (otherGpuFrame < gpuFrames + c_NumGPUFrames) {
				if (otherGpuFrame->isComplete && otherGpuFrame->frameNumber > mostRecentGPUFrame)
				{
					mostRecentGPUFrame = otherGpuFrame->frameNumber;
					profiledGpuFrame = otherGpuFrame;
				}
				otherGpuFrame++;
			}
		}
		//
		if (!profiledGpuFrame)
			return;

		auto gpuFrameDurationMicro = (profiledGpuFrame->endTimeGPU - profiledGpuFrame->startTimeGPU) / 1000;
		float gpuFrameFloat = gpuFrameDurationMicro / 1000.f;
		int frameDifference = profiledFrame->frameNumber - mostRecentGPUFrame;
		ImGui::Separator();
		ImGui::Text("GPU - %d frame(s) behind - %.3fms", frameDifference, gpuFrameFloat);
		ImGui::Separator();

		nestLevel = 1;
		nestingEndTimes[1] = 0;
		numTimers = profiledGpuFrame->timerCount;
		for (uint32_t i = 0; i < numTimers; i++) {
			auto idx = (profiledGpuFrame->timerStart + i) % c_GPUTimerBuffer;
			GPUTimer* tmr = gpuTimers + idx;

			if (tmr->time < nestingEndTimes[nestLevel]) {
				cycleUp(&nestLevel, c_maxNesting);
				ImGui::Indent();
			}
			while (nestingEndTimes[nestLevel - 1] <= tmr->time) {
				ImGui::Unindent();
				nestLevel--;
			}
			nestingEndTimes[nestLevel] = tmr->endTime;

			uint32_t color = tmr->color;
			if (color == 0)
				color = HashString(tmr->name, countof(tmr->name));
			ImGui::PushStyleColor(ImGuiCol_Text, color | 0xFF303030);
			auto durationNano = tmr->endTime - tmr->time;
			auto durationMicro = durationNano / 1000;
			float durationFloat = durationMicro == 0 ? durationNano / 1000.f : durationMicro / 1000.f;
			ImGui::Text("%.3f%s - %s", durationFloat, durationMicro == 0 ? "us": "ms", tmr->name);
			ImGui::PopStyleColor();
		}
	}

	//uint32_t Profiler::GetNumFrameTimers()
	//{
	//	return cycleDiff(freeCpuTimer, frame->timerStart, c_TimerBuffer);
	//}
	//
	//Profiler::Timer* Profiler::GetTimer(uint32_t frameTimerIdx)
	//{
	//	auto idx = (frame->timerStart + frameTimerIdx) % c_TimerBuffer;
	//	return cpuTimers + idx;
	//}
};