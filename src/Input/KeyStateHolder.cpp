#include "Input/IOEvents.h"
#include <bolts_assert.h>
#include <algorithm>

namespace Bolts {
	namespace Input {

		KeyStateHolder::KeyStateHolder() {
			memset(m_isKeyPressed, 0, sizeof(m_isKeyPressed));

			m_inputEventQueue.reserve(5);
			m_keyPressedArray.reserve(3);
			m_keyReleasedArray.reserve(3);
		}

		bool KeyStateHolder::KeyPressed(keyCode_t kc) const
		{
			return (std::find(m_keyPressedArray.begin(), m_keyPressedArray.end(), kc) != m_keyPressedArray.end());
		}
		bool KeyStateHolder::KeyReleased(keyCode_t kc) const
		{
			return (std::find(m_keyReleasedArray.begin(), m_keyReleasedArray.end(), kc) != m_keyReleasedArray.end());
		}

		void KeyStateHolder::EventsHandled()//Called at the end of a frame to mark one-time events such as KeyPressed as handled
		{
			m_keyPressedArray.clear();
			m_keyReleasedArray.clear();
			for (const keyEvent_t& ke : m_inputEventQueue) {
				switch (ke.state) {
				case KS_PRESSED: {
					if (!m_isKeyPressed[ke.keyCode])
						m_keyPressedArray.push_back(ke.keyCode);
					m_isKeyPressed[ke.keyCode] = true;
					break;
				}
				case KS_RELEASED: {
					if (m_isKeyPressed[ke.keyCode])
						m_keyReleasedArray.push_back(ke.keyCode);
					m_isKeyPressed[ke.keyCode] = false;
					break;
				}
				default://DAFUQ ?
					BASSERT(false);
				}
			}
			m_inputEventQueue.clear();
		}

	};
};