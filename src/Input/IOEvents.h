#pragma once

#include <bolts_core.h>

namespace Bolts {
	namespace Input {
		enum mouseButton_t { B_LEFT = 0, B_RIGHT, B_MIDDLE, B_4, B_5, _B_COUNT};
		enum mouseState_t { MS_PRESSED = 0, MS_RELEASED, MS_MOVED, MS_SCROLLED, MS_CONNECTED, MS_DISCONNECTED };
		enum keyState_t { KS_PRESSED = 0, KS_RELEASED};
		enum keyCode_t 
			{ KC_UP=0, KC_DOWN, KC_LEFT, KC_RIGHT, KC_SPACE, KC_ESC, KC_ENTER, KC_SHIFT, KC_CTRL, KC_ALT,KC_TAB,
				KC_A = 'A', KC_Z = 'Z',
				KC_PGUP = 0x5B, KC_PGDN, KC_BACKSPACE, KC_DELETE, KC_INSERT, KC_HOME, KC_END,
				KC_F1, KC_F2, KC_F3, KC_F4, KC_F5, KC_F6, KC_F7, KC_F8, KC_F9, KC_F10, KC_F11, KC_F12, _KC_COUNT};

		/*
		 *	NOTE: When the button state is > MS_MOVED, ignore the value of button
		 *	NOTE: When the button is MB_SCROLL , mouseX and Y contain the deltas for their respective axes
		 */
		struct mouseEvent_t {
			Bolts::vec2		coords;
			mouseState_t	state;
			mouseButton_t	button;
			uint8_t			mouseId;
		};

		class iMouseEventListener {
			public:
				virtual bool	OnMouseEvent( mouseEvent_t ) = 0;
		};

		struct keyEvent_t {
			keyState_t	state;
			keyCode_t	keyCode;
		};

		class iKeyEventListener {
		public:
			virtual bool	OnKeyEvent( keyEvent_t ) = 0;
		};

		class iCharEventListener {
		public:
			virtual bool	OnCharEvent(unsigned int uniChar) = 0;
		};

		//TODO HIGH: Double buffer the input event queue for thread safety and sanity
		class KeyStateHolder:public iKeyEventListener
		{
		private:
			bool				m_isKeyPressed[ _KC_COUNT];
			vec_t< keyCode_t>	m_keyPressedArray;//Holds keys pressed this frame
			vec_t< keyCode_t>	m_keyReleasedArray;//Holds keys released this frame
			vec_t< keyEvent_t>	m_inputEventQueue;//These are parsed when EventsHandled is called
		public:
			KeyStateHolder();
			
			bool	OnKeyEvent(keyEvent_t ke) { m_inputEventQueue.push_back(ke); return false; }

			bool	IsKeyDown( keyCode_t kc) const { return m_isKeyPressed[ kc]; }
			bool	KeyPressed( keyCode_t kc) const;
			bool	KeyReleased( keyCode_t kc) const;

			void	EventsHandled();//Called at the end of a frame to mark one-time events such as KeyPressed as handled
		};

		class MouseStateHolder:public iMouseEventListener
		{
		private:
			bool					m_isButtonPressed[ _B_COUNT];
			vec_t< mouseButton_t>	m_buttonPressedArray;//Holds keys pressed this frame
			vec_t< mouseButton_t>	m_buttonReleasedArray;//Holds keys released this frame
			vec_t< mouseEvent_t >	m_inputEventQueue;//These are parsed when EventsHandled is called
			mutable vec2			m_lastReportedPosition;//Used by GetMouseMoveAmount to determine amount of motion
			vec2					m_currentPosition;
			vec2					m_currentScroll;
		public:
			MouseStateHolder();

			bool	OnMouseEvent( mouseEvent_t me );

			bool	IsButtonDown( mouseButton_t bc) const { return m_isButtonPressed[bc]; }
			bool	HasButtonBeenPressed( mouseButton_t bc) const;
			bool	HasButtonBeenReleased( mouseButton_t bc) const;

			//! Returns the cumulative mouse motion relative to the last value returned by a call to this function ( initially relative to 0,0 )
			vec2	GetMouseMoveAmount() const { return m_currentPosition - m_lastReportedPosition; }
			vec2	GetMouseScrollAmount() const { return m_currentScroll; }
			//! Returns the current mouse position, relative to the window center ( between -1 .. 1 on each axis )
			vec2	GetMousePosition() const { return m_currentPosition; }
			
			void	EventsHandled();//Called at the end of a frame to mark one-time events such as KeyPressed as handled
		};

	};// namespace Input
};//namespace Bolts
