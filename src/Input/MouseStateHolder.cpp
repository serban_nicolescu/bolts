#include "Input/IOEvents.h"
#include <algorithm>

namespace Bolts {
	namespace Input {

		MouseStateHolder::MouseStateHolder() {
			memset(m_isButtonPressed, 0, sizeof(m_isButtonPressed));

			m_inputEventQueue.reserve(12);
			m_buttonPressedArray.reserve(_B_COUNT);
			m_buttonReleasedArray.reserve(_B_COUNT);
		}

		bool MouseStateHolder::OnMouseEvent(mouseEvent_t me)
		{
			if (me.state == MS_MOVED) {
				m_currentPosition = me.coords;
			}
			else if (me.state == MS_SCROLLED) {
				m_currentScroll += me.coords;
			}
			else {
				m_inputEventQueue.push_back(me);
			}
			return false;
		}

		bool MouseStateHolder::HasButtonBeenPressed(mouseButton_t bc) const
		{
			return (std::find(m_buttonPressedArray.begin(), m_buttonPressedArray.end(), bc) != m_buttonPressedArray.end());
		}
		bool MouseStateHolder::HasButtonBeenReleased(mouseButton_t bc) const
		{
			return (std::find(m_buttonReleasedArray.begin(), m_buttonReleasedArray.end(), bc) != m_buttonReleasedArray.end());
		}

		void MouseStateHolder::EventsHandled()//Called at the end of a frame to mark one-time events such as ButtonPressed as handled
		{
			m_buttonPressedArray.clear();
			m_buttonReleasedArray.clear();
			for (const mouseEvent_t& me : m_inputEventQueue) {
				switch (me.state) {
				case MS_PRESSED: {
					if (!m_isButtonPressed[me.button])
						m_buttonPressedArray.push_back(me.button);
					m_isButtonPressed[me.button] = true;
					break;
				}
				case MS_RELEASED: {
					if (m_isButtonPressed[me.button])
						m_buttonReleasedArray.push_back(me.button);
					m_isButtonPressed[me.button] = false;
					break;
				}
				}
			}
			m_inputEventQueue.clear();
			m_lastReportedPosition = m_currentPosition;
			m_currentScroll = {};
		}
	};
};