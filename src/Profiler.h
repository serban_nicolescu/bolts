#pragma once
#include <bolts_core.h>

namespace Bolts
{
	namespace Rendering {
		class RenderBackend;
	}

	class Profiler
	{
	public:
		static Profiler&	Get();

		static void			Init(Rendering::RenderBackend*);
		static void			Shutdown();

		static const int c_NumCPUFrames = 2;
		static const int c_NumGPUFrames = 6;
		static const int c_TimerBuffer = 64;
		static const int c_GPUTimerBuffer = 256;
		static const int c_MaxTimerName = 20;
		static const int c_EventDataBytes = 4 * sizeof(float);

		void FrameStart();
		void FrameEnd();

		int		PushCPUTimer(const char* name, uint32_t color);
		void	PopCPUTimer(int timerID);
		int		PushGPUTimer(const char* name, uint32_t color);
		void	PopGPUTimer(int timerID);
		//float	GetLastPoppedTimeInSec(); // returns the time in seconds of the last popped CPU event

		void DrawCPUProfilingInfo();
		void DrawGPUProfilingInfo();

		// Internal

		struct ScopedTimer {
			ScopedTimer(int t) :ti(t) {}
			~ScopedTimer() { Profiler::Get().PopCPUTimer(ti); }
			int ti;
		};

		struct ScopedGPUTimer {
			ScopedGPUTimer(int t) :ti(t) {}
			~ScopedGPUTimer() { Profiler::Get().PopGPUTimer(ti); }
			int ti;
		};

		struct EvtBase {
			char		name[c_MaxTimerName];
			uint64_t	time; // microseconds since frame start for timers, since game start for events
			uint32_t	color;
			uint32_t	_padding;
		};

		struct Timer :public EvtBase {
			uint64_t	endTime;
		};


		struct GPUTimer :public Timer {
			uint64_t	startQueryId;
			uint64_t	endQueryId;
		};

		//uint32_t		GetNumFrameTimers();
		//Timer*			GetTimer(uint32_t frameTimerIdx);
	private:
		uint32_t		currentFrame = 0;

		Timer*			cpuTimers;
		uint32_t		freeCpuTimer = 0;
		GPUTimer*		gpuTimers;
		uint32_t		freeGpuTimer = 0;

		struct Event : public EvtBase
		{
			uint32_t	evtType;
			char		evtData[c_EventDataBytes];
		};

		vec_t<Event>	cpuEvents;
		uint32_t		freeCpuEvent = 0;

		struct FrameInfo {
			uint64_t	startTime = 0;
			uint64_t	endTime = 0;
			uint32_t	frameNumber;
			uint32_t	timerStart = 0;
			uint32_t	timerCount = 0;
			uint32_t	eventStart;
			uint32_t	eventCount;
		};

		FrameInfo		frames[c_NumCPUFrames];
		FrameInfo*		frame;

		struct GPUFrameInfo {
			uint64_t	startTimeCPU = 0;
			uint64_t	startTimeGPU = 0;
			uint64_t	endTimeGPU = 0;
			uint64_t	startQueryId;
			uint64_t	endQueryId;
			uint32_t	frameNumber;
			uint32_t	timerStart = 0;
			uint32_t	timerCount = 0;
			bool		isComplete = true;
		};
		GPUFrameInfo	gpuFrames[c_NumGPUFrames];
		GPUFrameInfo*	gpuFrame;

	};
};

#define PROFILE_SCOPE(name) ::Bolts::Profiler::ScopedTimer __profileScopedTimer__{ ::Bolts::Profiler::Get().PushCPUTimer(name, 0)};
#define PROFILE_SCOPEGPU(name) ::Bolts::Profiler::ScopedGPUTimer __profileScopedGPUTimer__{ ::Bolts::Profiler::Get().PushGPUTimer(name, 0)};
