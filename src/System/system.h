#pragma once
#include <bolts_core.h>

namespace Bolts {
	namespace System {
		typedef uint64_t windowHandle_t;
		typedef str_t	path_t;

		namespace Window {
			enum styleFlags_t {
			    S_NONE		= 0,
			    S_RESIZEABLE = 1,
			    S_TITLE		= 2,
			    S_CLOSE		= 1 << 2,
			    S_MAXIMIZE	= 1 << 3,
			    S_MINIMIZE	= 1 << 4
			};
		};

		void		OnAppStart();

		path_t		OpenFileDialog(const char* hintPath = nullptr, const char* ext = nullptr, const char* extName = nullptr);
		path_t		SaveFileDialog(const char* hintPath = nullptr, const char* ext = nullptr, const char* extName = nullptr);
		const char* GetFilenameFromPath(const char* path, int pathSize);

		uint64_t	GetElapsedMicroseconds(); // Returns number of microseconds elapsed since OnAppStart was called
		float		GetElapsedMs(uint64_t us); // Returns the number of milliseconds that passed since the given microsecond(us) timestamp
		float		MicroToMs(uint64_t us);
		void		SleepMicroseconds(uint64_t us);
	};

	namespace Input {
		enum windowEventType_t { WE_RESIZED = 0, WE_CLOSED, WE_MINIMIZED, WE_LOSTFOCUS, WE_GOTFOCUS};

		//NOTE: If the event type is WE_RESIZED , value1 and value2 contain the width and the height, respectively
		struct windowEvent_t {
			System::windowHandle_t	windowHandle;
			int32_t					value1;
			int32_t					value2;
			windowEventType_t 		type;
		};

		class iWindowEventListener {
			public:
				virtual void	OnWindowEvent( windowEvent_t ) = 0;
		};

	};
};
