#pragma once

#include "bolts_core.h"
#include "System/system.h"
#include "Input/IOEvents.h"
#include "loki/Singleton.h"

//#include <loki\Pimpl.h>
#include <string>
#include <vector>

namespace Bolts {
	namespace System {

		class Win32SystemImpl;

		//TODO: Hiding cursor per window
		class Win32System: nonCopyable_t {
			public:
				static const windowHandle_t INVALID_WINDOW_HANDLE = 0;

				Win32System();
				~Win32System();

				static Win32System&			Get();

				void			InitApp();
				void			Update( windowHandle_t );

				windowHandle_t	NewWindow( const std::string &, uint32_t sw = 800, uint32_t sh = 600, uint8_t flags = Window::S_CLOSE );
				void			CloseWindow( windowHandle_t );
				bool			IsWindowOpen(windowHandle_t);
				ivec2			GetWindowSize( windowHandle_t ) const;
				void			SetWindowSize(windowHandle_t, ivec2);
				ivec2			GetWindowPos(windowHandle_t) const;
				void			SetWindowPos(windowHandle_t, ivec2);
				void			SetWindowTitle(windowHandle_t, const char* utf8Title);
				windowHandle_t	GetMainWindow() const; //Main window is the first window created
				//NOTE: Right now, it shows/hides the cursor for all the windows
				void			HideCursor( windowHandle_t );
				void			ShowCursor( windowHandle_t );
				uvec2			GetCursorCoords(windowHandle_t); //> returns mouse position in window coords, top-left

				vec_t<uvec2>	GetSupportedDisplayModes() const;
				bool			EnableFullscreen( windowHandle_t, uvec2 resolution ); //, bool vsync = false );
				void			DisableFullscreen();

				//******* Graphics *****
				//Return true if context initialized successfully
				bool			CreateGraphicsContext( windowHandle_t );
				void			DestroyGraphicsContext();

				//Returns false on failure. Fail reason details not implemented yet.
				bool			SwapWindowBuffers();

				void			RegisterWindowEventListener( Input::iWindowEventListener * );
				void			UnregisterWindowEventListener( Input::iWindowEventListener * );
				void			RegisterMouseEventListener( Input::iMouseEventListener * );
				void			UnregisterMouseEventListener( Input::iMouseEventListener * );
				void			RegisterKeyEventListener( Input::iKeyEventListener * );
				void			UnregisterKeyEventListener( Input::iKeyEventListener * );
				void			RegisterCharEventListener(Input::iCharEventListener *);
				void			UnregisterCharEventListener(Input::iCharEventListener *);
			private:
				std::unique_ptr<Win32SystemImpl>	m_impl;
		};
	};
};