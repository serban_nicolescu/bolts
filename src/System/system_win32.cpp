//#include "stdafx.h"
#include "System/system_win32.h"
#include "System/Win32/system_win32_impl.h"

/*
#include <glew/GL/glew.h>
#include <windows.h>
#include <GLFW/glfw.h>
*/
#include <iostream>

namespace Bolts {
	namespace System {

		typedef class Loki::SingletonHolder <
			Win32System,
			Loki::CreateUsingNew,
			Loki::DefaultLifetime
		> Win32SystemStn;

		Win32System& Win32System::Get()
		{
			return Win32SystemStn::Instance();
		}

		Win32System::Win32System() : m_impl( new Win32SystemImpl() )
		{

		}

		Win32System::~Win32System()
		{

		}

		void Win32System::InitApp()
		{
			m_impl->InitApp ();
		}

		windowHandle_t Win32System::NewWindow( const std::string &name, uint32_t screenWidth, uint32_t screenHeight, uint8_t flags/* = S_CLOSE */ )
		{
			return m_impl->NewWindow ( name, screenWidth, screenHeight, flags );
		}

		Bolts::System::windowHandle_t Win32System::GetMainWindow() const
		{
			return m_impl->GetMainWindow();
		}

		void Win32System::HideCursor( windowHandle_t handle )
		{
			m_impl->ShowCursor( handle, false );
		}

		void Win32System::ShowCursor( windowHandle_t handle )
		{
			m_impl->ShowCursor( handle, true );
		}

		uvec2 Win32System::GetCursorCoords(windowHandle_t handle)
		{
			return m_impl->GetCursorCoords(handle);
		}

		void Win32System::Update( windowHandle_t handle )
		{
			m_impl->Update ( handle );
		}

		void Win32System::RegisterWindowEventListener( Input::iWindowEventListener *listener )
		{
			m_impl->RegisterWindowEventListener( listener );
		}

		void Win32System::UnregisterWindowEventListener( Input::iWindowEventListener *listener )
		{
			m_impl->UnregisterWindowEventListener( listener );
		}

		void Win32System::CloseWindow( windowHandle_t handle )
		{
			m_impl->CloseWindow( handle );
		}

		bool Win32System::IsWindowOpen(windowHandle_t handle)
		{
			return m_impl->IsWindowOpen(handle);
		}

		void Win32System::RegisterMouseEventListener( Input::iMouseEventListener *listener )
		{
			m_impl->RegisterMouseEventListener( listener );
		}

		void Win32System::UnregisterMouseEventListener( Input::iMouseEventListener *listener )
		{
			m_impl->UnregisterMouseEventListener( listener );
		}

		void Win32System::RegisterKeyEventListener( Input::iKeyEventListener * listener)
		{
			m_impl->RegisterKeyEventListener( listener);
		}
		void Win32System::UnregisterKeyEventListener( Input::iKeyEventListener * listener)
		{
			m_impl->UnregisterKeyEventListener( listener);
		}
		void Win32System::RegisterCharEventListener(Input::iCharEventListener * listener)
		{
			m_impl->RegisterCharEventListener(listener);
		}
		void Win32System::UnregisterCharEventListener(Input::iCharEventListener * listener)
		{
			m_impl->UnregisterCharEventListener(listener);
		}
		ivec2 Win32System::GetWindowSize( windowHandle_t handle ) const
		{
			return m_impl->GetWindowSize( handle );
		}

		void Win32System::SetWindowSize(windowHandle_t handle, ivec2 size)
		{
			m_impl->SetWindowSize(handle, size);
		}

		ivec2 Win32System::GetWindowPos(windowHandle_t h) const
		{
			return m_impl->GetWindowPos(h);
		}

		void Win32System::SetWindowPos(windowHandle_t h, ivec2 pos)
		{
			m_impl->SetWindowPos(h, pos);
		}

		void Win32System::SetWindowTitle(windowHandle_t h, const char* utf8Title)
		{
			m_impl->SetWindowTitle(h, utf8Title);
		}

		vec_t<uvec2> Win32System::GetSupportedDisplayModes() const
		{
			return m_impl->GetSupportedDisplayModes();
		}

		bool Win32System::EnableFullscreen( windowHandle_t handle, uvec2 resolution ) //, bool vsync /*= false*/ )
		{

			return m_impl->EnableFullscreen( handle, resolution ); //, vsync );
		}

		void Win32System::DisableFullscreen()
		{
			m_impl->DisableFullscreen();
		}

		bool Win32System::CreateGraphicsContext( windowHandle_t handle )
		{
			return m_impl->CreateGraphicsContext( handle );
		}

		void Win32System::DestroyGraphicsContext()
		{
			m_impl->DestroyGraphicsContext();
		}

		bool Win32System::SwapWindowBuffers()
		{
			return m_impl->SwapWindowBuffers();
		}

	};
};
