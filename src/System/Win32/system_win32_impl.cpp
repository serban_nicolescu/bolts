#include "System/Win32/system_win32_impl.h"
#include "System/system.h"
#include <bolts_assert.h>

//Define the graphics context, if any
#ifdef BOLTS_OPENGL
	#include "System/Win32/system_win32_impl_opengl.h"
#endif

#include <commdlg.h>

#include <algorithm>
#include <iostream>

namespace {
	using namespace Bolts;

};

namespace Bolts {
	namespace System {
		// Timing functions
		//https://docs.microsoft.com/en-us/windows/desktop/SysInfo/acquiring-high-resolution-time-stamps

		static bool				s_appStarted = false;
		static LARGE_INTEGER	s_appStartTime;
		static LARGE_INTEGER	s_timeFreq;

		void OnAppStart()
		{
			s_appStarted = true;
			QueryPerformanceCounter(&s_appStartTime);
			QueryPerformanceFrequency(&s_timeFreq);
		}

		uint64_t GetElapsedMicroseconds()
		{
			BASSERT(s_appStarted);
			LARGE_INTEGER ctime;
			QueryPerformanceCounter(&ctime);
			ctime.QuadPart = ctime.QuadPart - s_appStartTime.QuadPart;
			ctime.QuadPart *= 1000000;
			ctime.QuadPart /= s_timeFreq.QuadPart; // time is now in microseconds
			return uint64_t(ctime.QuadPart);
		}

		float GetElapsedMs(uint64_t us)
		{
			return MicroToMs(GetElapsedMicroseconds() - us);
		}

		float MicroToMs(uint64_t us)
		{
			return (us / 1000.f);
		}

		void SleepMicroseconds(uint64_t us)
		{
			Sleep( (int) (us / 1000));
		}

		// File dialogs
		str_t OpenFileDialog(const char* hintPath, const char* ext, const char* extName)
		{
			OPENFILENAME ofn; // common dialog box structure
			TCHAR szFile[260];// buffer for file name
			HWND hwnd = 0;    // owner window

			// Initialize OPENFILENAME
			ZeroMemory(&ofn, sizeof(ofn));
			ofn.lStructSize = sizeof(ofn);
			ofn.hwndOwner = hwnd;
			ofn.lpstrFile = szFile;
			// Set lpstrFile[0] to '\0' so that GetOpenFileName does not 
			// use the contents of szFile to initialize itself.
			ofn.lpstrFile[0] = '\0';
			ofn.nMaxFile = sizeof(szFile);
			ofn.lpstrFilter = NULL;
			char filter[256];
			if (ext) {
				if (extName)
					sprintf_s(filter, sizeof(filter), "%s%c*.%s%c", extName, '\0', ext, '\0');
				else
					sprintf_s(filter, sizeof(filter), "*.%s%c", ext, '\0');
				ofn.lpstrFilter = filter;
			}
			ofn.nFilterIndex = 1;
			ofn.lpstrFileTitle = NULL;
			ofn.nMaxFileTitle = 0;
			ofn.lpstrInitialDir = hintPath;
			ofn.Flags = OFN_FILEMUSTEXIST | OFN_EXPLORER | OFN_NOCHANGEDIR;
			// Display the Open dialog box. 
			if (GetOpenFileName(&ofn) == TRUE)
				return ofn.lpstrFile;
			else return "";
		}

		str_t SaveFileDialog(const char* hintPath, const char* ext, const char* extName)
		{
			// Set lpstrFile[0] to '\0' so that GetOpenFileName does not 
			// use the contents of szFile to initialize itself.
			char szFile[260];       // buffer for file name
			if (hintPath)
				strncpy(szFile, hintPath, sizeof(szFile));
			else
				szFile[0] = '\0';
			// Initialize OPENFILENAME
			OPENFILENAME ofn;       // common dialog box structure
			ZeroMemory(&ofn, sizeof(ofn));
			ofn.lStructSize = sizeof(ofn);
			HWND hwnd = 0; // owner window
			ofn.hwndOwner = hwnd;
			ofn.lpstrFile = szFile;
			ofn.nMaxFile = sizeof(szFile);
			ofn.lpstrFilter = NULL;
			char filter[256];
			if (ext) {
				if (extName)
					sprintf_s(filter, sizeof(filter), "%s%c*.%s%c", extName, '\0', ext, '\0');
				else
					sprintf_s(filter, sizeof(filter), "*.%s%c", ext, '\0');
				ofn.lpstrFilter = filter;
			}
			ofn.nFilterIndex = 1;
			ofn.lpstrFileTitle = NULL;
			ofn.nMaxFileTitle = 0;
			ofn.lpstrInitialDir = hintPath;
			ofn.lpstrDefExt = ext;
			ofn.Flags = OFN_PATHMUSTEXIST | OFN_EXPLORER | OFN_OVERWRITEPROMPT | OFN_NOCHANGEDIR;
			// Display the Open dialog box. 
			if (GetSaveFileName(&ofn) == TRUE)
				return ofn.lpstrFile;
			else return "";
		}

		// Lockable Set

		template< class Key>
		void LockableSet<Key>::insert(typename container_t::value_type newItem)
		{
			m_data.insert(newItem);
		}

		template< class Key>
		void LockableSet<Key>::erase(typename container_t::key_type newItem)
		{
			if (!m_isLocked) {
				m_data.erase(newItem);
			}
			else {
				m_removeBuffer.insert(newItem);
			}
		}

		template< class Key>
		void LockableSet<Key>::Unlock()
		{
			m_isLocked = false;
			std::for_each(m_removeBuffer.begin(), m_removeBuffer.end(), [this](Key val) {
				m_data.erase(val);
			});
			m_removeBuffer.clear();
		}

		// Win32 System

		const char *Win32SystemImpl::CLASS_NAME = "Bolts_Window";

		Win32SystemImpl::Win32SystemImpl() : m_bIsInitialized(false), m_fullscreenWindow(NULL), m_windowHandles()
			, m_windowEventListeners(), m_mouseEventListeners(), m_keyEventListeners()
		{
			InitializeGraphicsContext();
		}

		Win32SystemImpl::~Win32SystemImpl()
		{
			if (m_fullscreenWindow != NULL) {
				ChangeDisplaySettings(NULL, 0);
			}

			if (m_bIsInitialized) {
				graphicsContext.DestroyContext();

				for (auto it = m_windowHandles.begin(), end = m_windowHandles.end(); it != end; it++) {
					DestroyWindow(*it);
				}

				UnregisterClassA(Win32SystemImpl::CLASS_NAME, GetModuleHandle(NULL));
			}
		}

		void Win32SystemImpl::InitApp()
		{
			RegisterWindowClass();
			m_bIsInitialized = true;
		}

		windowHandle_t Win32SystemImpl::NewWindow(const std::string &name, int32_t windowWidth, int32_t widowHeight, uint8_t flags)
		{
			if (!m_bIsInitialized) {
				//TODO LOG: Cannot create a window ! Win32System not initialized
				return INVALID_WINDOW_HANDLE;
			}

			// Compute position and size
			HDC screenDC = GetDC(NULL);
			int left = (GetDeviceCaps(screenDC, HORZRES) - windowWidth) / 2;
			int top = (GetDeviceCaps(screenDC, VERTRES) - widowHeight) / 2;
			ReleaseDC(NULL, screenDC);

			// Choose the window style according to the Style parameter
			DWORD win32Style = WS_VISIBLE;
			if (flags != Window::S_NONE) {
				if (flags & Window::S_TITLE) {
					win32Style |= WS_CAPTION;
				}
				if (flags & Window::S_RESIZEABLE) {
					win32Style |= WS_THICKFRAME | WS_MAXIMIZEBOX;
				}
				if (flags & Window::S_MINIMIZE) {
					win32Style |= WS_MINIMIZEBOX;
				}
				if (flags & Window::S_MAXIMIZE) {
					win32Style |= WS_MAXIMIZEBOX;
				}
				if (flags & Window::S_CLOSE) {
					win32Style |= WS_SYSMENU;
				}
			}

			// In windowed mode, adjust width and height so that window will have the requested client area
			RECT rectangle = { 0, 0, windowWidth, widowHeight };
			AdjustWindowRect(&rectangle, win32Style, false);
			windowWidth = rectangle.right - rectangle.left;
			widowHeight = rectangle.bottom - rectangle.top;

			HWND m_handle = CreateWindowA(Win32SystemImpl::CLASS_NAME, name.c_str(), win32Style, left, top, windowWidth, widowHeight, NULL, NULL, GetModuleHandle(NULL), this);
			m_windowHandles.insert(m_handle);

			return (windowHandle_t)m_handle;
		}

		Bolts::System::windowHandle_t Win32SystemImpl::GetMainWindow() const
		{
			return (m_windowHandles.size() > 0 ? (windowHandle_t)*m_windowHandles.begin() : 0);
		}

		void Win32SystemImpl::ShowCursor(windowHandle_t handle, bool bShow)
		{
			if (!HandleIsValid(handle)) {
				//TODO LOG: Invalid window handle provided
			}

			if (bShow) {
				SetCursor(LoadCursor(NULL, IDC_ARROW));
			}
			else {
				SetCursor(NULL);
			}
		}

		uvec2 Win32SystemImpl::GetCursorCoords(windowHandle_t handle)
		{
			POINT pos;
			HWND h = (HWND)handle;
			if ((::GetActiveWindow() == h) && ::GetCursorPos(&pos))
				if (::ScreenToClient(h, &pos))
					return { pos.x, pos.y };
			return {};
		}

		void Win32SystemImpl::Update(windowHandle_t handle)
		{
			if (!HandleIsValid(handle)) {
				//TODO LOG: Invalid window handle provided
				return;
			}

			ProcessMessages((HWND)handle);
			//glfwSwapBuffers();
		}

		bool Win32SystemImpl::HandleIsValid(windowHandle_t handle)
		{
			return (handle != Win32SystemImpl::INVALID_WINDOW_HANDLE);
		}

		void Win32SystemImpl::ProcessMessages(HWND windowsHandle)
		{
			MSG message;
			while (PeekMessage(&message, windowsHandle, 0, 0, PM_REMOVE)) {
				TranslateMessage(&message);
				DispatchMessage(&message);
			}
		}

		void Win32SystemImpl::RegisterWindowClass()
		{
			WNDCLASSA windowClass;
			windowClass.style = CS_OWNDC;
			windowClass.lpfnWndProc = &Win32SystemImpl::GlobalOnEventCallback;
			windowClass.cbClsExtra = 0;
			windowClass.cbWndExtra = 0;
			windowClass.hInstance = GetModuleHandle(NULL);
			windowClass.hIcon = NULL;
			windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
			windowClass.hbrBackground = 0;
			windowClass.lpszMenuName = NULL;
			windowClass.lpszClassName = Win32SystemImpl::CLASS_NAME;
			RegisterClassA(&windowClass);
		}

		LRESULT CALLBACK Win32SystemImpl::GlobalOnEventCallback(HWND handle, UINT message, WPARAM wParam, LPARAM lParam)
		{
			// Associate handle and Window instance when the creation message is received
			if (message == WM_CREATE) {
				// Get Win32SystemImpl instance (it was passed as the last argument of CreateWindow)
				LONG_PTR window = (LONG_PTR)reinterpret_cast<CREATESTRUCT *>(lParam)->lpCreateParams;

				// Set as the "user data" parameter of the window
				SetWindowLongPtr(handle, GWLP_USERDATA, window);

				if (Win32System::Get().CreateGraphicsContext( windowHandle_t(handle))) {
					BOLTS_LINFO() << "Successfully initialized the graphics context";
				}
				else {
					BOLTS_LFATAL() << "Error creating a graphics context";
				}
			}

			// Get the Win32SystemImpl instance corresponding to the window handle
			Win32SystemImpl* windowSystem = reinterpret_cast<Win32SystemImpl *>(GetWindowLongPtr(handle, GWLP_USERDATA));
			if (HandleIsValid((windowHandle_t)handle)) {
				// Forward the event to the appropriate function
				if (windowSystem)
					windowSystem->HandleSystemEvent(handle, message, wParam, lParam);
			} else {
				//TODO LOG: System asked to handle invalid handle
				BASSERT(false);
			}

			// We don't forward the WM_CLOSE message to prevent the OS from automatically destroying the window
			if (message == WM_CLOSE) {
				return 0;
			}

			//static const bool hasUnicode = hasUnicodeSupport();
			return DefWindowProc(handle, message, wParam, lParam);
		}


		Input::keyCode_t WindowsKCtoBoltsKC(WPARAM keyCode)
		{
			switch (keyCode) {
			case VK_UP:
				return Input::KC_UP;
			case VK_DOWN:
				return Input::KC_DOWN;
			case VK_LEFT:
				return Input::KC_LEFT;
			case VK_RIGHT:
				return Input::KC_RIGHT;
			case VK_ESCAPE:
				return Input::KC_ESC;
			case VK_RETURN:
				return Input::KC_ENTER;
			case VK_TAB:
				return Input::KC_TAB;
			case VK_PRIOR:
				return Input::KC_PGUP;
			case VK_NEXT:
				return Input::KC_PGDN;
			case VK_BACK:
				return Input::KC_BACKSPACE;
			case VK_DELETE:
				return Input::KC_DELETE;
			case VK_HOME:
				return Input::KC_HOME;
			case VK_INSERT:
				return Input::KC_INSERT;
			case VK_END:
				return Input::KC_END;
			case VK_LSHIFT:
			case VK_RSHIFT:
			case VK_SHIFT:
				return Input::KC_SHIFT;
			case VK_LCONTROL:
			case VK_RCONTROL:
			case VK_CONTROL:
				return Input::KC_CTRL;
			case VK_MENU:
				return Input::KC_ALT;
			case VK_SPACE:
				return Input::KC_SPACE;
			}

			if (keyCode >= 'A' && keyCode <= 'Z') {
				return Input::keyCode_t(keyCode);
			}

			if (keyCode >= VK_F1 && keyCode <= VK_F12) {
				return Input::keyCode_t(Input::KC_F1 + (keyCode - VK_F1));
			}

			return Input::KC_SPACE;
		}

		void Win32SystemImpl::HandleSystemEvent(HWND handle, UINT message, WPARAM wParam, LPARAM lParam)
		{
			windowHandle_t wHandle = reinterpret_cast<windowHandle_t>(handle);
			switch (message) {
				// Set cursor event
			case WM_SETCURSOR: {
				//https://stackoverflow.com/questions/4503506/cursor-style-doesnt-stay-updated
				//TODO: Handle cursor style here
				// The mouse has moved, if the cursor is in our window we must refresh the cursor
				//if (LOWORD(lParam) == HTCLIENT)
				//SetCursor(m_cursor);
				break;
			}

			// Close event
			case WM_CLOSE: {
				Input::windowEvent_t event;
				event.type = Input::WE_CLOSED;
				event.windowHandle = wHandle;
				PushWindowEvent(event);
				break;
			}

			case WM_MOVE: { // stopped moving
				//int xPos = (int)(short)LOWORD(lParam);   // horizontal position 
				//int yPos = (int)(short)HIWORD(lParam);   // vertical position 
			}
			
			case WM_SIZE: { // stopped resizing
				//if (wParam != SIZE_MAXIMIZED)
				//	break;
			}
			case WM_WINDOWPOSCHANGED:
			case WM_EXITSIZEMOVE: {
				uvec2 newSize = GetWindowSize((windowHandle_t)handle);
				Input::windowEvent_t event;
				event.type = Input::WE_RESIZED;
				event.value1 = (int32_t)newSize.x;
				event.value2 = (int32_t)newSize.y;
				event.windowHandle = wHandle;
				PushWindowEvent(event);

				break;
			}

			// Gain focus event
			case WM_SETFOCUS: {
				Input::windowEvent_t event;
				event.type = Input::WE_GOTFOCUS;
				event.windowHandle = wHandle;
				PushWindowEvent(event);

				break;
			}

			// Lost focus event
			case WM_KILLFOCUS: {
				Input::windowEvent_t event;
				event.type = Input::WE_LOSTFOCUS;
				event.windowHandle = wHandle;
				PushWindowEvent(event);

				break;
			}

			//Keyboard Button Pressed event
			case WM_KEYDOWN: {
				Input::keyEvent_t event;
				event.state = Input::KS_PRESSED;
				event.keyCode = WindowsKCtoBoltsKC(wParam);
				PushKeyEvent(event);
				break;
			}
			case WM_KEYUP: {
				Input::keyEvent_t event;
				event.state = Input::KS_RELEASED;
				event.keyCode = WindowsKCtoBoltsKC(wParam);
				PushKeyEvent(event);
				break;
			}

			// Mouse wheel event
			case WM_MOUSEWHEEL: {
				Input::mouseEvent_t event;
				event.state = Input::MS_SCROLLED;
				event.button = Input::B_LEFT;
				event.coords.x = 0.f;
				event.coords.y = float(GET_WHEEL_DELTA_WPARAM(wParam) / WHEEL_DELTA);
				PushMouseEvent(event);
				break;
			}

			// Mouse wheel event
			case WM_MOUSEHWHEEL: {
				Input::mouseEvent_t event;
				event.state = Input::MS_SCROLLED;
				event.button = Input::B_LEFT;
				event.coords.x = float(GET_WHEEL_DELTA_WPARAM(wParam) / WHEEL_DELTA);
				event.coords.y = 0.f;
				PushMouseEvent(event);
				break;
			}

			// Mouse left button down event
			case WM_LBUTTONDOWN: {
				Input::mouseEvent_t event;
				event.state = Input::MS_PRESSED;
				event.button = Input::B_LEFT;
				event.coords = GetMouseNormalizedCoords(wHandle, lParam);
				PushMouseEvent(event);
				break;
			}

			// Mouse left button up event
			case WM_LBUTTONUP: {
				Input::mouseEvent_t event;
				event.state = Input::MS_RELEASED;
				event.button = Input::B_LEFT;
				event.coords = GetMouseNormalizedCoords(wHandle, lParam);
				PushMouseEvent(event);
				break;
			}

			// Mouse right button down event
			case WM_RBUTTONDOWN: {
				Input::mouseEvent_t event;
				event.state = Input::MS_PRESSED;
				event.button = Input::B_RIGHT;
				event.coords = GetMouseNormalizedCoords(wHandle, lParam);
				PushMouseEvent(event);
				break;
			}

			// Mouse right button up event
			case WM_RBUTTONUP: {
				Input::mouseEvent_t event;
				event.state = Input::MS_RELEASED;
				event.button = Input::B_RIGHT;
				event.coords = GetMouseNormalizedCoords(wHandle, lParam);
				PushMouseEvent(event);
				break;
			}

			// Mouse wheel button down event
			case WM_MBUTTONDOWN: {
				Input::mouseEvent_t event;
				event.state = Input::MS_PRESSED;
				event.button = Input::B_MIDDLE;
				event.coords = GetMouseNormalizedCoords(wHandle, lParam);
				PushMouseEvent(event);
				break;
			}

			// Mouse wheel button up event
			case WM_MBUTTONUP: {
				Input::mouseEvent_t event;
				event.state = Input::MS_RELEASED;
				event.button = Input::B_MIDDLE;
				event.coords = GetMouseNormalizedCoords(wHandle, lParam);
				PushMouseEvent(event);
				break;
			}

			// Mouse X button down event
			case WM_XBUTTONDOWN: {
				Input::mouseEvent_t event;
				event.state = Input::MS_PRESSED;
				event.button = HIWORD(wParam) == XBUTTON1 ? Input::B_4 : Input::B_5;
				event.coords = GetMouseNormalizedCoords(wHandle, lParam);
				PushMouseEvent(event);
				break;
			}

			// Mouse X button up event
			case WM_XBUTTONUP: {
				Input::mouseEvent_t event;
				event.state = Input::MS_RELEASED;
				event.button = HIWORD(wParam) == XBUTTON1 ? Input::B_4 : Input::B_5;
				event.coords = GetMouseNormalizedCoords(wHandle, lParam);
				PushMouseEvent(event);
				break;
			}

			// Mouse move event
			case WM_MOUSEMOVE: {
				Input::mouseEvent_t event;
				event.state = Input::MS_MOVED;
				event.button = Input::B_LEFT;
				//static vec2 oldPos = vec2();
				event.coords = GetMouseNormalizedCoords(wHandle, lParam);// -oldPos;
				//oldPos = GetMouseNormalizedCoords(wHandle, lParam);
				PushMouseEvent(event);
			}
			// Character input
			case WM_CHAR:
			case WM_UNICHAR:
			{
				unsigned int codepoint = (unsigned)wParam;
				if (codepoint < 32 || (codepoint > 126 && codepoint < 160))
					return;
				PushCharEvent(codepoint);
			}
			}
		}

		void Win32SystemImpl::RegisterWindowEventListener(winEvtLnrPtr_t listener)
		{
			if (listener == NULL) {
				//TODO LOG: Null window listener pointer passed
				return;
			}
			m_windowEventListeners.insert(listener);
		}

		void Win32SystemImpl::UnregisterWindowEventListener(winEvtLnrPtr_t listener)
		{
			if (listener == NULL) {
				//TODO LOG: Null window listener pointer passed
				return;
			}
			m_windowEventListeners.erase(listener);
		}

		void Win32SystemImpl::PushWindowEvent(Input::windowEvent_t event)
		{
			auto lock = m_windowEventListeners.Lock();
			//Event handle guaranteed to be OK
			std::for_each(m_windowEventListeners.begin(), m_windowEventListeners.end(), [event](winEvtLnrPtr_t listener) {
				listener->OnWindowEvent(event);
			});
		}

		void Win32SystemImpl::RegisterMouseEventListener(mouseEvtLnrPtr_t listener)
		{
			if (listener == NULL) {
				//TODO LOG: Null window listener pointer passed
				return;
			}
			m_mouseEventListeners.insert(listener);
		}

		void Win32SystemImpl::UnregisterMouseEventListener(mouseEvtLnrPtr_t listener)
		{
			if (listener == NULL) {
				//TODO LOG: Null window listener pointer passed
				return;
			}
			m_mouseEventListeners.erase(listener);
		}

		void Win32SystemImpl::RegisterKeyEventListener(keyEvtLnrPtr_t listener)
		{
			if (listener == NULL) {
				//TODO LOG: Null window listener pointer passed
				return;
			}
			m_keyEventListeners.insert(listener);
		}

		void Win32SystemImpl::UnregisterKeyEventListener(keyEvtLnrPtr_t listener)
		{
			if (listener == NULL) {
				//TODO LOG: Null window listener pointer passed
				return;
			}
			m_keyEventListeners.erase(listener);
		}

		void Win32SystemImpl::RegisterCharEventListener(charEvtLnrPtr_t listener)
		{
			if (listener == NULL) {
				//TODO LOG: Null window listener pointer passed
				return;
			}
			m_charEventListeners.insert(listener);
		}

		void Win32SystemImpl::UnregisterCharEventListener(charEvtLnrPtr_t listener)
		{
			if (listener == NULL) {
				//TODO LOG: Null window listener pointer passed
				return;
			}
			m_charEventListeners.erase(listener);
		}


		void Win32SystemImpl::PushMouseEvent(Input::mouseEvent_t event)
		{
			auto lock = m_mouseEventListeners.Lock();
			for (auto listener : m_mouseEventListeners)
				if (listener->OnMouseEvent(event))
					break;
		}

		void Win32SystemImpl::PushKeyEvent(Input::keyEvent_t event)
		{
			auto lock = m_keyEventListeners.Lock();
			for (auto listener : m_keyEventListeners)
				if (listener->OnKeyEvent(event))
					break;
		}

		void Win32SystemImpl::PushCharEvent(unsigned int charCode)
		{
			auto lock = m_charEventListeners.Lock();
			for (auto listener : m_charEventListeners)
				if (listener->OnCharEvent(charCode))
					break;
		}

		void Win32SystemImpl::CloseWindow(windowHandle_t handle)
		{
			if (!HandleIsValid(handle)) {
				//TODO LOG: Invalid handle passed to CloseWindow
				return;
			}

			m_windowHandles.erase(std::find(m_windowHandles.begin(), m_windowHandles.end(), (HWND)handle));
			DestroyWindow((HWND)handle);
		}

		bool Win32SystemImpl::IsWindowOpen(windowHandle_t handle)
		{
			return IsWindow((HWND)handle);
		}

		uvec2 Win32SystemImpl::GetWindowSize( windowHandle_t handle ) const
		{
			RECT rect;
			::GetClientRect( ( HWND ) handle, &rect );
			return uvec2( (unsigned) ( rect.right - rect.left ), (unsigned) ( rect.bottom - rect.top ) );
		}

		void Win32SystemImpl::SetWindowSize(windowHandle_t handle, uvec2 dim)
		{
			//Windows size https://msdn.microsoft.com/en-us/library/windows/desktop/ms633545(v=vs.85).aspx
			UINT flags = SWP_NOMOVE | SWP_NOZORDER | SWP_NOCOPYBITS;

			RECT rect;
			rect.left = 0;
			rect.top = 0;
			rect.right = dim.x;
			rect.bottom = dim.y;
			::AdjustWindowRect(&rect, GetWindowLong((HWND)handle, GWL_STYLE), false);

			::SetWindowPos( (HWND) handle, 0, 0, 0, rect.right - rect.left, rect.bottom - rect.top, flags);
		}

		ivec2 Win32SystemImpl::GetWindowPos(windowHandle_t handle) const
		{
			RECT rect;
			::GetWindowRect((HWND)handle, &rect); // this includes the drop shadow size, for some reason ...
			return {rect.left, rect.top};
		}

		void Win32SystemImpl::SetWindowPos(windowHandle_t handle, ivec2 pos)
		{
			UINT flags = SWP_NOSIZE | SWP_NOZORDER | SWP_NOCOPYBITS;
	   		::SetWindowPos((HWND)handle, 0, pos.x, pos.y, 0,0, flags);
		}

		void Win32SystemImpl::SetWindowTitle(windowHandle_t h, const char * utf8Title)
		{
			SetWindowText((HWND)h, utf8Title);
		}

		vec_t<uvec2> Win32SystemImpl::GetSupportedDisplayModes() const
		{
			DEVMODE result;
			std::vector<uvec2> resolutions;

			EnumDisplaySettings( NULL, 0, &result );
			resolutions.push_back ( uvec2( (unsigned) result.dmPelsWidth, (unsigned) result.dmPelsHeight ) );
			int i = 1;
			while ( EnumDisplaySettings( NULL, i, &result ) ) {
				if ( result.dmBitsPerPel == 32 &&
				     ( result.dmPelsWidth != resolutions.back ().x &&
				       result.dmPelsHeight != resolutions.back ().y ) ) {
					resolutions.push_back ( uvec2( ( unsigned ) result.dmPelsWidth, (unsigned) result.dmPelsHeight ) );
				}
				i++;
			}
			return resolutions;
		}

		bool Win32SystemImpl::EnableFullscreen( windowHandle_t handle, uvec2 resolution ) //, bool vsync /*= false*/ )
		{
			if ( m_fullscreenWindow != NULL ) {
				//TODO LOG: Failed to change display mode to fullscreen.
				//			There is a window set to fullscreen already << Console::ENDL
				return false;
			}
			HWND wHandle = ( HWND ) handle;
			DEVMODE devMode;
			devMode.dmSize       = sizeof( devMode );
			devMode.dmPelsWidth  = resolution.x;
			devMode.dmPelsHeight = resolution.y;
			devMode.dmBitsPerPel = 32;
			devMode.dmFields     = DM_PELSWIDTH | DM_PELSHEIGHT | DM_BITSPERPEL;

			// Apply fullscreen mode
			if ( ChangeDisplaySettings( &devMode, CDS_FULLSCREEN ) != DISP_CHANGE_SUCCESSFUL ) {
				//TODO LOG: Failed to change display mode to fullscreen.
				//			Resolution: << resolution.width << " | " resolution.height << Console::ENDL
				return false;
			}

			// Remember the current fullscreen window
			//		And it's style, for restoring later
			m_fullscreenWindow = wHandle;
			WINDOWINFO winfo;
			winfo.cbSize = sizeof( WINDOWINFO );
			GetWindowInfo( wHandle, &winfo );
			m_fullscreenWindowOldStyle = winfo.dwStyle;
			m_fullscreenWindowOldSize = GetWindowSize ( handle );

			// Make the window flags compatible with fullscreen mode
			SetWindowLong( wHandle, GWL_STYLE, WS_POPUP | WS_CLIPCHILDREN | WS_CLIPSIBLINGS );
			SetWindowLong( wHandle, GWL_EXSTYLE, WS_EX_APPWINDOW );

			// Resize the window so that it fits the entire screen
			//		And update the changes to the style we made
			::SetWindowPos( wHandle, HWND_TOP, 0, 0, resolution.x, resolution.y, SWP_FRAMECHANGED );
			ShowWindow( wHandle, SW_SHOW );

			return true;
		}

		void Win32SystemImpl::DisableFullscreen()
		{
			if ( m_fullscreenWindow == NULL ) {
				return;
			}

			ChangeDisplaySettings( NULL, 0 );
			//Restore old window style
			SetWindowLong( m_fullscreenWindow, GWL_STYLE, m_fullscreenWindowOldStyle );
			//Calling this function updates the changes we made previously
			//		Note that the parameters that we passed are ignored, thanks to the flags we have set
			UINT flags = SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOZORDER;
			::SetWindowPos( m_fullscreenWindow, 0, 0, 0, m_fullscreenWindowOldSize.x, m_fullscreenWindowOldSize.y, flags );

			m_fullscreenWindow = NULL;
		}

		void Win32SystemImpl::InitializeGraphicsContext()
		{
			graphicsContext.Initialize();
		}

		bool Win32SystemImpl::CreateGraphicsContext( windowHandle_t handle )
		{
			BASSERT( m_bIsInitialized );

			return graphicsContext.CreateContext( ( HWND ) handle );
		}

		void Win32SystemImpl::DestroyGraphicsContext()
		{
			BASSERT( m_bIsInitialized );

			graphicsContext.DestroyContext ();
		}

		bool Win32SystemImpl::SwapWindowBuffers()
		{
			return graphicsContext.SwapWindowBuffers ();
		}

		vec2 Win32SystemImpl::GetCursorCoords(LPARAM lParam)
		{
			return { LOWORD(lParam) , HIWORD(lParam) };
		}

		vec2 Win32SystemImpl::GetMouseNormalizedCoords( windowHandle_t wHandle, LPARAM lParam )
		{
			uvec2	winSize = GetWindowSize ( wHandle);
			vec2 normCoords = GetCursorCoords(lParam);
			normCoords /= winSize;
			//normCoords.x = static_cast<float>(LOWORD(lParam)) / winSize.x;
			//normCoords.y = static_cast<float>( HIWORD( lParam ) ) / winSize.y;
			normCoords *= 2.f;
			normCoords -= 1.f;
			return normCoords;
		}
	};
};
