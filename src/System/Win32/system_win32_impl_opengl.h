#pragma once

#include <bolts_assert.h>
#include <bolts_log.h>

#include <GL/glew.h>
#include <GL/wglew.h>
#include <System/system_win32.h>
#include <microprofile.h>

//#define WIN32_LEAN_AND_MEAN
//#include <windows.h>

namespace Bolts {

	namespace {
		class ContextOpenGL {
			public:
				void	Initialize() {
					ResetHandles();
				}

				//NOTE: The thread which calls this function becomes the graphics context owning thread
				bool	CreateContext( HWND );
				void	DestroyContext();

				bool	SwapWindowBuffers ();
			private:
				void	ResetHandles();

				HWND	m_windowHandle;
				HDC		m_deviceContext;
				HGLRC 	m_renderContext;
		};

		static ContextOpenGL graphicsContext;

		//static void InitializeGLEW(HDC dc)
		//{
		//	//First, create the temporary GL Context
		//	// Create the render context (RC)
		//	HGLRC tempRC = wglCreateContext(dc);
		//	if (!tempRC) {
		//		//TODO LOG: Error creating default render context
		//		return;
		//	}
		//	// make it the current render context
		//	wglMakeCurrent(dc, tempRC);
		//
		//	//Initialize GLEW
		//	GLenum err = glewInit();
		//	BASSERT(err == GLEW_OK);
		//	if (GLEW_OK != err) {
		//		/* Problem: glewInit failed, something is seriously wrong. */
		//		BOLTS_LFATAL() << "Error initialising GLEW: " << (char*)glewGetErrorString(err);
		//	}
		//	//Clean up
		//	wglMakeCurrent(NULL, NULL);
		//	wglDeleteContext(tempRC);
		//}

		bool ContextOpenGL::CreateContext( HWND whandle )
		{
			// Display device name
			/*DISPLAY_DEVICE dd;
			dd.cb = sizeof(DISPLAY_DEVICE);
			for (int i = 0; EnumDisplayDevices(NULL, i, &dd, 0); i++)
			{
				if (!(dd.StateFlags & DISPLAY_DEVICE_ATTACHED_TO_DESKTOP))
					continue;
			}*/

			BASSERT( m_windowHandle == 0 );
			BASSERT( whandle != 0 );

			// get the device context (DC)
			m_deviceContext = GetDC( whandle );
			if ( !m_deviceContext ) {
				BOLTS_LFATAL() << "Error creating device context for the window !";
				return false;
			}
			// remember the window handle (HWND)
			m_windowHandle = whandle;

			//Choose Window FBO format
			int format;
			// set the pixel format for the DC
			PIXELFORMATDESCRIPTOR pfd;
			ZeroMemory( &pfd, sizeof( pfd ) );
			pfd.nSize = sizeof( pfd );
			pfd.nVersion = 1;
			pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL |
						  PFD_DOUBLEBUFFER;
			pfd.iPixelType = PFD_TYPE_RGBA;
			pfd.cColorBits = 32;
			pfd.cDepthBits = 24;
			pfd.cStencilBits = 8;
			pfd.iLayerType = PFD_MAIN_PLANE;
			format = ChoosePixelFormat( m_deviceContext, &pfd );
			//Set it
			SetPixelFormat( m_deviceContext, format, NULL);

			//InitializeGLEW( m_deviceContext);

			//Create render context
			if (WGL_ARB_create_context_profile && GLEW_VERSION_3_3){
				//TODO: Make debug context optional
				//TODO: Switch to Core profile once GUI rendering is done properly
				//Can explicitly request debug context and 3.3 Compat profile
				int attriblist[] = {
					WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
					WGL_CONTEXT_MINOR_VERSION_ARB, 3,
					WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB,
					WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_DEBUG_BIT_ARB,
					0 };

				m_renderContext = wglCreateContextAttribsARB(m_deviceContext, 0, attriblist);
			} else{
				// create the render context (RC)
				m_renderContext = wglCreateContext(m_deviceContext);
			}
			
			if ( !m_renderContext ) {
				//int errorCode = GetLastError();
				//TODO LOG: Error creating render context
				DestroyContext ();
				return false;
			}

			// make it the current render context
			wglMakeCurrent( m_deviceContext, m_renderContext );

			//Initialize GLEW
			GLenum err = glewInit();
			BASSERT(err == GLEW_OK);
			if (GLEW_OK != err) {
				/* Problem: glewInit failed, something is seriously wrong. */
				BOLTS_LFATAL() << "Error initialising GLEW: " << (char*)glewGetErrorString(err);
			}

			// disable vsync
			if (WGL_EXT_swap_control) {
				bool vSyncEnabled = false;
				wglSwapIntervalEXT(int(vSyncEnabled));
			}

#if MICROPROFILE_ENABLED
			MicroProfileGpuInitGL();
#endif

			return true;
		}

		void ContextOpenGL::DestroyContext()
		{
			if ( m_renderContext ) {
				wglMakeCurrent( NULL, NULL );
				wglDeleteContext( m_renderContext );
			}
			if ( m_windowHandle && m_deviceContext ) {
				ReleaseDC( m_windowHandle, m_deviceContext );
			}
			ResetHandles();
		}

		void ContextOpenGL::ResetHandles()
		{
			m_windowHandle = NULL;
			m_deviceContext = NULL;
			m_renderContext = NULL;
		}

		bool ContextOpenGL::SwapWindowBuffers()
		{
			//More info on Swap: http://msdn.microsoft.com/en-us/library/windows/desktop/dd369060(v=vs.85).aspx
			return (bool)SwapBuffers ( m_deviceContext );
		}

	};

};
