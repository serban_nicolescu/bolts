#pragma once

#include <bolts_core.h>
#include "System/system.h"
#include "Input/IOEvents.h"

#include <windows.h>

#include <string>
#include <vector>
#include <set>
#include <map>

namespace Bolts {
	namespace System {

		//TODO: Add unit tests
		template< class Key>
		class LockableSet
		{
		public:
			typedef LockableSet<Key> this_t;
			class ScopedLock
			{
			public:
				ScopedLock ( this_t* set){
					m_lockedSet = set;
				}
				~ScopedLock() { m_lockedSet->Unlock(); }
			private:
				this_t* m_lockedSet;
			};
			typedef ScopedLock lock_t;
			typedef std::set<Key> container_t;


			LockableSet():m_isLocked( false) {}

			inline lock_t	Lock(){ m_isLocked = true; return ScopedLock(this); }
			void			Unlock();

			void			insert( typename container_t::value_type newItem);
			void			erase( typename container_t::key_type newItem);
			inline typename container_t::iterator 
				begin() { return m_data.begin (); }
			inline typename container_t::iterator 
				end() { return m_data.end(); }

		private:
			bool			m_isLocked;
			container_t		m_data;
			container_t		m_removeBuffer;//Used to store items to be removed after unlocking
		};

		//TODO: Look into sharing a graphics context and rendering on other windows
		//TODO: Add Depth Bits and Stencil bits option
		//TODO: Add MSAA, Debug Output and sRGB Framebuffer options
		class Win32SystemImpl {
			public:
				typedef Input::iWindowEventListener	*winEvtLnrPtr_t;
				typedef Input::iMouseEventListener	*mouseEvtLnrPtr_t;
				typedef Input::iKeyEventListener	*keyEvtLnrPtr_t;
				typedef Input::iCharEventListener	*charEvtLnrPtr_t;

				static const windowHandle_t			INVALID_WINDOW_HANDLE = NULL;
				static const char					*CLASS_NAME;

				Win32SystemImpl();
				~Win32SystemImpl();

				void			InitApp();
				void			Update( windowHandle_t );

				windowHandle_t	NewWindow( const std::string &, int32_t sw, int32_t sh, uint8_t flags );
				void			CloseWindow( windowHandle_t handle );
				bool			IsWindowOpen(windowHandle_t handle);
				windowHandle_t	GetMainWindow() const;
				uvec2			GetWindowSize( windowHandle_t ) const;
				void			SetWindowSize(windowHandle_t, uvec2);
				ivec2			GetWindowPos(windowHandle_t) const;
				void			SetWindowPos(windowHandle_t, ivec2);
				void			SetWindowTitle(windowHandle_t, const char* utf8Title);
				void			ShowCursor( windowHandle_t, bool );
				uvec2			GetCursorCoords(windowHandle_t handle);

				vec_t<uvec2>	GetSupportedDisplayModes() const;
				bool			EnableFullscreen( windowHandle_t, uvec2 resolution ); //, bool vsync = false );
				void			DisableFullscreen();

				//******* Graphics *****
				//Return true if context initialized successfully
				bool			CreateGraphicsContext( windowHandle_t );
				void			DestroyGraphicsContext();

				bool			SwapWindowBuffers();

				//Event-related functions
				void			RegisterWindowEventListener( winEvtLnrPtr_t );
				void			UnregisterWindowEventListener( winEvtLnrPtr_t );
				void			RegisterMouseEventListener( mouseEvtLnrPtr_t );
				void			UnregisterMouseEventListener( mouseEvtLnrPtr_t );
				void			RegisterKeyEventListener( keyEvtLnrPtr_t);
				void			UnregisterKeyEventListener( keyEvtLnrPtr_t);
				void			RegisterCharEventListener(charEvtLnrPtr_t);
				void			UnregisterCharEventListener(charEvtLnrPtr_t);
			private:
				//Window related helpers
				static LRESULT CALLBACK		GlobalOnEventCallback( HWND handle, UINT message, WPARAM wParam, LPARAM lParam );
				static bool					HandleIsValid( windowHandle_t );

				//Graphics
				void			InitializeGraphicsContext();

				void			RegisterWindowClass();
				void			ProcessMessages( HWND );
				void			HandleSystemEvent( HWND handle, UINT message, WPARAM wParam, LPARAM lParam );

				vec2			GetCursorCoords(LPARAM lParam);
				vec2			GetMouseNormalizedCoords( windowHandle_t wHandle, LPARAM lParam );

				//Events
				void			PushWindowEvent( Input::windowEvent_t );
				void			PushMouseEvent( Input::mouseEvent_t );
				void			PushKeyEvent( Input::keyEvent_t );
				void			PushCharEvent(unsigned int charCode);

				bool					m_bIsInitialized;
				std::set< HWND >		m_windowHandles;
				HWND					m_fullscreenWindow;
				DWORD 					m_fullscreenWindowOldStyle;
				uvec2					m_fullscreenWindowOldSize;

				LockableSet<winEvtLnrPtr_t>		m_windowEventListeners;
				LockableSet<mouseEvtLnrPtr_t>	m_mouseEventListeners;
				LockableSet<keyEvtLnrPtr_t>		m_keyEventListeners;
				LockableSet<charEvtLnrPtr_t>	m_charEventListeners;
		};
	};
};