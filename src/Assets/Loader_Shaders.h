#pragma once
#include <Rendering/GPUProgramFwd.h>
#include <Assets/AssetManager.h>

namespace Bolts {

	namespace assets
	{
		class ShaderLoader:public IAssetLoader
		{
		public:
			typedef Rendering::GPUProgram AssetT;

			static assetID_t	RegisterShaderProgramData(const str_t& vsFilename, const str_t& fsFilename, const vec_t<str_t>& defines);
			static bool			GetShaderProgramPaths(assetID_t programID, str_t& vsFilename, str_t& fsFilename);
			static bool			GetShaderProgramData(assetID_t programID, str_t& vsFilename, str_t& fsFilename, vec_t<str_t>& defines);

			ShaderLoader(AssetManager&);
			~ShaderLoader();

			virtual void LoadFromSource(regHash_t) override;
			virtual void OnSourceChanged(IO::fileID_t, assetID_t) override;
			virtual void OnUnload(assetPtr_t&) override;
		private:
			void LoadProgramInternal(IO::fileID_t, AssetT*);
		};
	};
};
