#pragma once
#include <bolts_core.h>
#include <bolts_hash.h>
#include <bolts_intrusiveptr.h>

namespace Bolts {

	typedef hash32_t assetID_t;

	namespace assets {
		class AssetManager;

		enum BuiltinAssetType
		{
			EAT_ANIMATION_SET = 0,
			EAT_TEXTURE,
			EAT_MESH,
			EAT_MATERIAL,
			EAT_GPU_PROGRAM,
			EAT_LAST_BUILTIN
		};

		enum AssetState: int8_t
		{
			EAS_UNLOADED = 0, //> Asset exists but is not in the process of being loaded
			EAS_LOADING, //> Asset is in the process of being loaded ( will be read from disk, network, etc. )
			EAS_READY, //> Asset is ready to be used
			EAS_NOT_FOUND = -1, //> Asset ID is not recognized. Asset cannot currently be loaded
			EAS_ERROR = -2//> There was an error during the loading process. Asset is not loadable
		};

		struct assetMixin:public nonCopyable_t
		{
			friend class AssetManager;

			typedef bool drawFunc_t(assetMixin*);

			assetMixin(assetID_t aid) :m_assetID(aid) {};

			void onReferenceAdd() const {
				m_refCount++;
			}

			void onReferenceRelease() const {
				m_refCount--;
			}

			inline bool			IsValid() const { return (m_assetState > 0 && m_assetID.h != 0); } //> An invalid resource cannot be loaded ( it's either missing or in a bad state )
			inline void			SetAssetState(AssetState as) { m_assetState = as; }
			inline void			IncrementReloads() { m_reloadCount++; }

			inline const char*	GetAssetName() const { return m_assetID.s; }
			inline assetID_t	GetAssetID() const { return m_assetID.h; }
			inline AssetState	GetAssetState() const { return m_assetState; }
			inline uint8_t		GetReloadCount() const { return m_reloadCount; }
			inline uint32_t		GetRefCount() const { return m_refCount; }
		protected:
			regHash_t			m_assetID;
			mutable uint32_t	m_refCount = 0;
			AssetState			m_assetState = EAS_UNLOADED;
			uint8_t				m_reloadCount = 0;
			char				_padding[2];
		};

		//static_assert(sizeof(assetMixin) == 16,""); //32bit
		//static_assert(sizeof(assetMixin) == 24,""); //64bit

		typedef intrusivePtr_t<assetMixin> assetPtr_t;
	}

	using AssetManager = assets::AssetManager;
}