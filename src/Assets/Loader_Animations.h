#pragma once

#include <Assets/Assets.h>
#include <Assets/AssetManager.h>
#include <Rendering/AnimationSet.h>
#include <bolts_binaryserialization.h>

namespace Bolts
{
	namespace assets {
		class AnimLoader :public IAssetLoader
		{
		public:
			typedef Rendering::AnimationSet AssetT;

			AnimLoader(AssetManager&);
			~AnimLoader();

			virtual void LoadFromSource(regHash_t) override;
			virtual void OnSourceChanged(IO::fileID_t, assetID_t) override;
			virtual void OnUnload(assetPtr_t&) override;
		private:
			void LoadInternal(Blob::Reader, AssetT*);
		};
	};
};

