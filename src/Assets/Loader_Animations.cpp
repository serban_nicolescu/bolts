#include "Loader_Animations.h"
#include "AnimationFile.h"

#include <Assets/AssetManager.h>
#include <bolts_log.h>

#include <memory>

#define BOLTS_LOG_SCOPE "[AssetLoad] "

namespace Bolts {
	namespace assets {
		AnimLoader::AnimLoader(AssetManager& am) :IAssetLoader(am)
		{
			assetDrawFunc = [](assetMixin* assetPtr) -> bool {
				return static_cast<AssetT*>(assetPtr)->DrawProperties();
			};
		}

		AnimLoader::~AnimLoader() {}

		void AnimLoader::LoadFromSource(regHash_t sid)
		{
			auto newMesh = new AssetT(sid);
			//TODO: We can't unload animation file data. We should copy it into Animation assets
			//	Or steal it from Temp sources
			Blob::Reader fileData = IO::ReadAll(sid, IO::Forever);
			LoadInternal(fileData, newMesh);
			m_assetManager.AddAsset(sid, AssetT::c_assetType, newMesh);
			m_assetManager.RegisterAssetToSource(sid, AssetT::c_assetType, sid);
		}

		void AnimLoader::OnSourceChanged(IO::fileID_t sid, assetID_t assetID)
		{
			auto loadedAsset = m_assetManager.TryGetAsset<AssetT>(assetID);
			BASSERT(loadedAsset);
			if (!loadedAsset)
				return;
			// Reload data
			loadedAsset->IncrementReloads();
			Blob::Reader fileData = IO::ReadAll(sid, IO::Forever);
			LoadInternal(fileData, loadedAsset.get());
		}

		void AnimLoader::LoadInternal(Blob::Reader fileData, AssetT* anim)
		{
			if (fileData.IsEmpty()) {
				anim->SetAssetState(EAS_NOT_FOUND);
				return;
			}
			//
			anim->SetAssetState(EAS_READY);
			
			using namespace Bolts::Rendering;
			auto header = (const AnimHeader*)fileData.Head();
			BASSERT(header->versionNo == AnimHeader::c_latestVersion);
			if (header->versionNo != AnimHeader::c_latestVersion) {
				BOLTS_LERROR() << BOLTS_LOG_SCOPE << "Invalid animation file version. Expected " << AnimHeader::c_latestVersion << " got " << header->versionNo;
				return;
			}
			anim->SetData(header);
		}

		void AnimLoader::OnUnload(assetPtr_t& assetPtr)
		{
			auto pAsset = reinterpret_cast<AssetT*> (assetPtr.detach());
			//pAsset->Unload(*m_assetManager.GetCommandBuffer());
			delete pAsset;
		}

	};
};