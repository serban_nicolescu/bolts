#pragma once

#include <Assets/AssetManager.h>
#include <Rendering/TextureFwd.h>
#include <Rendering/RenderingCommon.h>

namespace Bolts {
	namespace assets
	{
		bool IsTexDDS(Blob::Reader);
		bool IsTexJPEG(Blob::Reader);
		bool IsTexTGA(Blob::Reader);

		bool LoadTexDDS(Blob::Reader, Rendering::RCB&, Bolts::Rendering::Texture* outTex);
		bool LoadTexJPEG(Blob::Reader, Rendering::RCB&, Bolts::Rendering::Texture* outTex);
		bool LoadTexTGA(Blob::Reader, Rendering::RCB&, Bolts::Rendering::Texture* outTex);

		class TextureLoader :public IAssetLoader
		{
		public:
			typedef Rendering::Texture AssetT;

			TextureLoader(AssetManager&);
			~TextureLoader();

			virtual void LoadFromSource(regHash_t) override;
			virtual void OnSourceChanged(IO::fileID_t, assetID_t) override;
			virtual void OnUnload(assetPtr_t&) override;
		private:
			void LoadTexture(IO::fileID_t, TextureLoader::AssetT*);
		};
	};
};