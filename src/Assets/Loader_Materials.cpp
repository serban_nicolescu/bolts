#include "Assets/Loader_Materials.h"
#include "Assets/Loader_Shaders.h"
#include "Assets/AssetManager.h"
#include <bolts_binaryserialization.h>
#include <bolts_binarymap_serialization.h>
#include <bolts_binarymap.h>
#include <bolts_helpers.h>
//#include <memory>
#include <algorithm>

#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>
#include <rapidjson/memorystream.h>
#include <rapidjson/encodedstream.h>

#include <Rendering/MaterialNew.h>
#include <Rendering/Texture.h>
#include <Rendering/GPUProgram.h>

#include <bolts_hash.h>
#include <bolts_log.h>
#include <bolts_assert.h>

#define BOLTS_LOG_SCOPE "[AssetLoad]"

using namespace Bolts;

namespace {
	using namespace rapidjson;

	//JSON Helpers
	bool ReadValue( const Document::ValueType& valJ, str_t& out ){
		if( !valJ.IsString( ) ) {
			return false;
		} else {
			out.assign( valJ.GetString( ), valJ.GetStringLength( ) );
		}
		return true;
	}

	bool ReadMember( const Document::ValueType& valJ, const char* memName, bool& out, bool	logError = true ){
		Value::ConstMemberIterator memIt = valJ.FindMember( memName );
		if( memIt == valJ.MemberEnd( ) || !memIt->value.IsBool()) {
			if( logError ) {
				BOLTS_LWARNING( ) << BOLTS_LOG_SCOPE << "Material JSON malformed.Material element missing \'" << memName << "\' or it is not a boolean.";
			}
			return false;
		} else {
			out = memIt->value.GetBool();
		}
		return true;
	}

	bool ReadMember( const Document::ValueType& valJ, const char* memName, str_t& out, bool	logError = true){
		out.clear();
		Value::ConstMemberIterator memIt = valJ.FindMember( memName );
		if( memIt == valJ.MemberEnd() ) {
			if( logError ) {
				BOLTS_LWARNING( ) << BOLTS_LOG_SCOPE << "Material JSON malformed.Material element missing \'" << memName << "\' or it is not a string.";
			}
			return false;
		}else{
			return ReadValue( memIt->value, out );
		}
	}

	bool ReadMember( const Document::ValueType& valJ, const char* memName, Value::ConstMemberIterator& out, bool	logError = true){
		Value::ConstMemberIterator memIt = valJ.FindMember( memName );
		if( memIt == valJ.MemberEnd( ) || !memIt->value.IsObject() ) {
			if( logError ) {
				BOLTS_LWARNING( ) << BOLTS_LOG_SCOPE << "Material JSON malformed.Material element missing \'" << memName << "\' or it is not an object.";
			}
			return false;
		} else {
			out = memIt;
		}
		return true;
	}

	bool ReadMember( Document::ValueType& valJ, const char* memName, Value::MemberIterator& out, bool	logError = true ){
		Value::MemberIterator memIt = valJ.FindMember( memName );
		if( memIt == valJ.MemberEnd( ) || !memIt->value.IsObject( ) ) {
			if( logError ) {
				BOLTS_LWARNING( ) << BOLTS_LOG_SCOPE << "Material JSON malformed.Material element missing \'" << memName << "\' or it is not an object.";
			}
			return false;
		} else {
			out = memIt;
		}
		return true;
	}

	bool ReadMemberArray( const Document::ValueType& valJ, const char* memName, Value::ConstMemberIterator& out, bool	logError = true ){
		Value::ConstMemberIterator memIt = valJ.FindMember( memName );
		if( memIt == valJ.MemberEnd( ) || !memIt->value.IsArray( ) ) {
			if( logError ) {
				BOLTS_LWARNING( ) << BOLTS_LOG_SCOPE << "Material JSON malformed.Material element missing \'" << memName << "\' or it is not an array.";
			}
			return false;
		} else {
			out = memIt;
		}
		return true;
	}

	//Material string to enum
	void ReadCullingMode( const Document::ValueType& valJ, Rendering::RenderState& rs, str_t& tempStorage)
	{
		if( ReadMember( valJ, "cull", tempStorage, false ) ) {
			if( tempStorage == "NONE" ) {
				rs.cullingMode = Rendering::BGCM_NONE;
				return;
			}
			if( tempStorage == "FRONT" ) {
				rs.cullingMode = Rendering::BGCM_FRONT;
				return;
			}
			if( tempStorage == "BACK" ) {
				rs.cullingMode = Rendering::BGCM_BACK;
				return;
			}
			BOLTS_LWARNING( ) << BOLTS_LOG_SCOPE << "Material JSON malformed. \'cull\' has undefined value: " << tempStorage.c_str( );
		}
	}

	void ReadDepthTest( const Document::ValueType& valJ, Rendering::RenderState& rs)
	{
		ReadMember(valJ, "depth_test", rs.isDepthTestEnabled, false);
	}

	void ReadDepthWrite( const Document::ValueType& valJ, Rendering::RenderState& rs)
	{
		ReadMember(valJ, "depth_write", rs.isDepthWriteEnabled, false);
	}

	void ReadBlending( const Document::ValueType& valJ, Rendering::RenderState& rs, str_t& tempStorage )
	{
		if( ReadMember( valJ, "blend_mode", tempStorage, false ) ) {
			if( tempStorage == "ADD" ) {
				rs.blendEq = Rendering::BGBE_ADD;
				return;
			}
			if( tempStorage == "SUBTRACT" ) {
				rs.blendEq = Rendering::BGBE_SUBTRACT;
				return;
			}
			if( tempStorage == "REVERSE_SUBTRACT" ) {
				rs.blendEq = Rendering::BGBE_REVERSE_SUBTRACT;
				return;
			}
			if (tempStorage == "MIN") {
				rs.blendEq = Rendering::BGBE_MIN;
				return;
			}
			if (tempStorage == "MAX") {
				rs.blendEq = Rendering::BGBE_MAX;
				return;
			}
			if( tempStorage == "OFF" ) {
				rs.blendEq = Rendering::BGBE_DISABLED;
				return;
			}
			BOLTS_LWARNING( ) << BOLTS_LOG_SCOPE << "Material JSON malformed. \'blend_mode\' has undefined value: " << tempStorage.c_str();
		}
	}

	void ReadBlendFuncs( const Document::ValueType& valJ, Rendering::RenderState& rs, str_t& tempStorage )
	{
		Rendering::BlendFunc	srcBlendF = Rendering::BGBF_SRC_ALPHA;
		Rendering::BlendFunc	dstBlendF = Rendering::BGBF_OM_SRC_ALPHA;
		if( ReadMember( valJ, "blend_src", tempStorage, false ) ) {
			if( tempStorage == "SRC_ALPHA" ) {
				srcBlendF = Rendering::BGBF_SRC_ALPHA;
			}else if( tempStorage == "ONE" ) {
				srcBlendF = Rendering::BGBF_ONE;
			}else if( tempStorage == "OM_SRC_ALPHA" ) {
				srcBlendF = Rendering::BGBF_OM_SRC_ALPHA;
			}else if( tempStorage == "ZERO" ) {
				srcBlendF = Rendering::BGBF_ZERO;
			} else {
				BOLTS_LWARNING( ) << BOLTS_LOG_SCOPE << "Material JSON malformed. \'blend_src\' has undefined value: " << tempStorage.c_str( );
				return;
			}
		}
		tempStorage.clear();
		if( ReadMember( valJ, "blend_dst", tempStorage, false ) ) {
			if( tempStorage == "SRC_ALPHA" ) {
				dstBlendF = Rendering::BGBF_SRC_ALPHA;
			} else if( tempStorage == "ONE" ) {
				dstBlendF = Rendering::BGBF_ONE;
			} else if( tempStorage == "OM_SRC_ALPHA" ) {
				dstBlendF = Rendering::BGBF_OM_SRC_ALPHA;
			} else if( tempStorage == "ZERO" ) {
				dstBlendF = Rendering::BGBF_ZERO;
			} else {
				BOLTS_LWARNING( ) << BOLTS_LOG_SCOPE << "Material JSON malformed. \'blend_dst\' has undefined value: " << tempStorage.c_str( );
				return;
			}
		}
		rs.dstBlendFunc = dstBlendF;
		rs.srcBlendFunc = srcBlendF;
		//mat.SetBlendFuncs( srcBlendF, dstBlendF );
	}

	bool ReadMaterialParameters( Rendering::MatParameterHolder& outParams, const rapidjson::Value& matObject, const char* matName)
	{
		//Load parameters
		struct DefaultParamHandler
		{
			vec4			m_vecHolder;
			unsigned			m_currentVecIdx;
			regHash_t	m_name;
			Rendering::MatParameterHolder& m_ph;

			DefaultParamHandler(Rendering::MatParameterHolder& ph, regHash_t paramName) :m_ph(ph), m_name(paramName) {
				m_currentVecIdx = unsigned(-1);
			}

			bool Null() { return false; }
			bool Bool(bool) { return false; }
			bool Int(int) { return false; }
			bool Uint(unsigned) { return false; }
			bool Int64(int64_t) { return false; }
			bool Uint64(uint64_t) { return false; }
			bool String(const char*, SizeType, bool) {
				return true; //String is name of texture resource, so accept it and let it be handled externally
			}
			//Vector
			bool StartArray() { m_currentVecIdx = 0; return true; }
			bool EndArray(SizeType elementCount) {
				if (m_currentVecIdx == 0) {
					BOLTS_LWARNING() << BOLTS_LOG_SCOPE << "Material JSON: Parameter ignored: " << m_name.s << " has empty Array value";
					return false;
				}
				switch (elementCount) {
				case 1:
					//TODO: Decide if this should be an error
					m_ph.Set(m_name, m_vecHolder.x);
					break;
				case 2:
					m_ph.Set(m_name, vec2(m_vecHolder.x, m_vecHolder.y));
					break;
				case 3:
					m_ph.Set(m_name, vec3(m_vecHolder.x, m_vecHolder.y, m_vecHolder.z));
					break;
				case 4:
					m_ph.Set(m_name, m_vecHolder);
					break;
				default:
					BOLTS_LWARNING() << BOLTS_LOG_SCOPE << "Material JSON: Parameter ignored: " << m_name.s << " has Array of 5+ elements";
					return false;
				}
				return true;
			}

			bool Double(double d) {
				if (m_currentVecIdx > 3) {
					//Not in an array. Just store the value
					//stringHash_t nameHash = HashString(m_name);
					m_ph.Set(m_name, (float)d);
				}
				else {
					m_vecHolder[m_currentVecIdx] = (float)d;
					m_currentVecIdx++;
				}
				return true;
			}

			bool StartObject() {
				return true; //Obj is texture with filter settings, so accept it and it wil be handled later
			}
			bool Key(const char* , SizeType , bool ) { return true; }
			bool EndObject(SizeType) { return true; }
		};

		Value::ConstMemberIterator paramsObjectIt;
		if (!ReadMember(matObject, "params", paramsObjectIt, false))
			return false;

		for (Value::ConstMemberIterator paramIt = paramsObjectIt->value.MemberBegin(); paramIt != paramsObjectIt->value.MemberEnd(); paramIt++)
		{
			regHash_t paramName{ paramIt->name.GetString() };
			// handle simple value parameters
			auto handler = DefaultParamHandler(outParams, paramName);
			if (!paramIt->value.Accept(handler)) {
				BOLTS_LWARNING() << BOLTS_LOG_SCOPE << "Material JSON [" << matName << "] Parameter ignored: " << paramName.s << " has invalid/unsupported value";
			}
			else {
				// handle texture params
				using namespace Rendering;

				const char* texName = nullptr;
				TextureSamplerSettings sampler;
				sampler.filteringType = TextureFilterType::BTFT_LINEAR;
				sampler.mipFilteringType = TextureFilterType::BTFT_LINEAR;
				sampler.wrapMode = TextureWrapMode::BTWM_REPEAT;
				
				if (paramIt->value.IsString()) {
					//If string then it's a texture resource name
					texName = paramIt->value.GetString();
				}
				else if (paramIt->value.IsObject()) {
					auto texNameParam = (paramIt->value).FindMember("name");
					if (texNameParam != paramIt->value.MemberEnd())
						texName = texNameParam->value.GetString();
					auto filterParam = paramIt->value.FindMember("filter");
					if (filterParam != paramIt->value.MemberEnd()) {
						const char* filterVal = filterParam->value.GetString();
						if (strcmp(filterVal, "linear") == 0) {
							sampler.filteringType = TextureFilterType::BTFT_LINEAR;
							sampler.mipFilteringType = TextureFilterType::BTFT_LINEAR;
						}
						else if (strcmp(filterVal, "point") == 0) {
							sampler.filteringType = TextureFilterType::BTFT_NEAREST;
							sampler.mipFilteringType = TextureFilterType::BTFT_NEAREST;
						}
						else if (strcmp(filterVal, "aniso") == 0) {
							BASSERT_MSG(false, "Anisotropic filtering isn't supported yet. Default to linear");
							sampler.filteringType = TextureFilterType::BTFT_ANISO;
							sampler.mipFilteringType = TextureFilterType::BTFT_ANISO;
						}
					}
					auto edgeParam = paramIt->value.FindMember("edge");
					if (edgeParam != paramIt->value.MemberEnd()) {
						const char* edgeVal = edgeParam->value.GetString();
						if (strcmp(edgeVal, "clamp") == 0)
							sampler.wrapMode = TextureWrapMode::BTWM_CLAMP_EDGE;
						else if (strcmp(edgeVal, "repeat") == 0)
							sampler.wrapMode = TextureWrapMode::BTWM_REPEAT;
						else if (strcmp(edgeVal, "repeat_mirror") == 0)
							sampler.wrapMode = TextureWrapMode::BTWM_REPEAT_MIRROR;
					}
				}
				else
					continue; // not a texture

				// load texture if one is specified in the material
				if (texName) {
					auto texAssetID = regHash_t(texName).h;
					outParams.SetTexToBeLoaded_Internal(paramName, texAssetID);
				}

				outParams.Set(paramName, sampler);
			}
		}
		
		return true;
	}

	//

	void LoadMaterialDependencies(AssetManager& am, Rendering::Material& oMaterial)
	{
		using namespace Rendering;
		BOLTS_LINFO() << BOLTS_LOG_SCOPE << "Loading Material: " << oMaterial.GetAssetName();

		// load shader program
		{
			auto progPtr = am.LoadAsset<Rendering::GPUProgram>(oMaterial.GetProgramID());
			BASSERT(progPtr && progPtr->IsValid());
			//TODO: should still keep progPtr, so material fixes itself when the program is fixed
			if (!progPtr->IsValid())
				progPtr = nullptr;
			oMaterial.SetProgram(progPtr);
		}
		// load textures
		auto& params = oMaterial.EditParams();
		for (const auto& info : params.GetInfo())
		{
			if (info.type == SPT_TEX)
			{
				//NOTE: Doing two linear searches here because of the API. Still better to use the Set() method when working with textures
				auto texAssetID = params.GetTexAssetID_Internal(info.dataOffset);
				if (texAssetID == 0)
					continue;
				auto pLoadedTexture = am.LoadAsset<Texture>(texAssetID);
				if (!pLoadedTexture) {
					BOLTS_LWARNING() << BOLTS_LOG_SCOPE << "Material JSON [" << GetHashString(oMaterial.GetAssetID())  << "] Could not find texture: \"" << GetHashString(texAssetID)
						<< "\" for parameter: \"" << GetHashString(info.key) << "\"";
					continue;
				}
				// set new texture
				params.Set(info.key, pLoadedTexture.get());
			}
		}
	}

	//Populates matInst with data from the matObj JSON object
	bool ParseMaterialInstanceJSON( AssetManager& am, const Document::ValueType& matObj, const str_t& matName, Rendering::Material& oMatInst)
	{
		//TODO: temp buffers, alloc
		str_t tempStr;
		tempStr.reserve(300);
		vec_t<str_t> defines;
		defines.reserve(64);
		str_t vsPath;
		str_t fsPath;

		// fetch base material
		if (ReadMember(matObj, "base", tempStr, false)) {
			Rendering::MaterialPtr base = am.TryGetAsset<Rendering::Material>( regHash_t{ tempStr });
			if (!base) {
				BOLTS_LERROR() << BOLTS_LOG_SCOPE << "Material [" << matName.c_str() << "]. Base material not found.";
				BASSERT(false);
				return false;
			}
			// copy over base material settings
			//TODO: this overwrites mat parameters on hot-reload.
			//	need to add a separate function for parameter copying
			oMatInst.CloneFrom(*base);
			oMatInst.SetBaseMaterialID(base->GetAssetID());
			// inherit defines
			assets::ShaderLoader::GetShaderProgramData(base->GetProgramID(), vsPath, fsPath, defines);
			// register to base material so you reload when base changes
			am.RegisterAssetToSource(base->GetAssetID(), assets::EAT_MATERIAL, oMatInst.GetAssetID());
		}

		//Load shaders
		{
			bool shadersChanged = false;
			Value::ConstMemberIterator definesIt;
			if (ReadMemberArray(matObj, "defines", definesIt, false)) {
				for (auto defineIt = definesIt->value.Begin(); defineIt != definesIt->value.End(); defineIt++) {
					if (!ReadValue(*defineIt, tempStr)) {
						BOLTS_LWARNING() << BOLTS_LOG_SCOPE << "Material [" << matName.c_str() << "]. One of the defines is not a string. Ignoring it.";
						continue;
					}
					// check for duplicates
					if (!contains(defines, tempStr))
						defines.push_back(tempStr);
					shadersChanged = true;
				}
			}
			//NOTE: This logic is a little more complicated because of hot-reload support
			bool hasProgramData = !vsPath.empty() && !fsPath.empty();
			Value::ConstMemberIterator shadersIt;
			if (ReadMember(matObj, "shaders", shadersIt, !hasProgramData)) {
				if (!ReadMember(shadersIt->value, "vs", vsPath) || !ReadMember(shadersIt->value, "fs", fsPath))
					return false;
				shadersChanged = true;
			}
			hasProgramData = !vsPath.empty() && !fsPath.empty();
			// register the shader program
			if (hasProgramData && shadersChanged) {
				auto programID = assets::ShaderLoader::RegisterShaderProgramData( vsPath, fsPath, defines);
				oMatInst.SetProgramID(programID);
			}
			if (oMatInst.GetProgramID() == 0) {
				BOLTS_LERROR() << BOLTS_LOG_SCOPE << "Material [" << matName.c_str() << "]. Invalid shader definition.";
				return false;
			}
		}
		//Load state data
		auto rs = oMatInst.GetRenderState();
		ReadCullingMode(matObj, rs, tempStr);
		ReadBlending(matObj, rs, tempStr);
		ReadBlendFuncs(matObj, rs, tempStr);
		ReadDepthTest(matObj, rs);
		ReadDepthWrite(matObj, rs);
		oMatInst.SetRenderState(rs);
		//Load parameters
		ReadMaterialParameters(oMatInst.EditParams(), matObj, matName.c_str());
		return true;
	}
};

static bool ParseMatJsonBlob(Blob::Reader src, IO::fileID_t sid, rapidjson::Document& docOut)
{
	using namespace rapidjson;
	BASSERT(!src.IsEmpty());
	if (src.IsEmpty()) {
		BOLTS_LERROR() << BOLTS_LOG_SCOPE << "Could not load materials from " << sid << ". Source not found";
		return false;
	}

	MemoryStream ms((char*)src.Head(), src.Remaining());
	EncodedInputStream< UTF8<>, MemoryStream> is(ms);
	docOut.ParseStream<kParseCommentsFlag | kParseTrailingCommasFlag>(is);

	if (docOut.HasParseError()) {
		BOLTS_LERROR() << BOLTS_LOG_SCOPE << "Material JSON malformed";
		BOLTS_LERROR() << BOLTS_LOG_SCOPE << "	Error code: " << docOut.GetParseError() << " at character: " << docOut.GetErrorOffset();
		return false;
	}
	return true;
}


namespace Bolts {
	namespace assets
	{
		static Rendering::Material* GetOrCreateMaterial(AssetManager& assetManager, regHash_t matName, IO::fileID_t jsonID)
		{
			using namespace Rendering;

			auto outMat = assetManager.TryGetAsset<Material>(matName);
			if (!outMat) {
				outMat = new Material(matName);
				assetManager.AddAsset(matName, Material::c_assetType, outMat);
				//Associate map material to source so we know what to update when it changes
				assetManager.RegisterAssetToSource(jsonID, Material::c_assetType, matName);
				outMat->SetAssetState( EAS_UNLOADED);
			}
			return outMat.get();
		}

		// Parse json and extract materials from it
		//		Adds Material assets as unloaded since we don't want to load textures & shaders implicitly
		static bool LoadMatpackJson(AssetManager& assetManager, Blob::Reader matSource, IO::fileID_t sid)
		{
			using namespace rapidjson;

			BOLTS_LINFO() << BOLTS_LOG_SCOPE << "Loading Matpack JSON: " << GetHashString(sid);

			Document matJ;
			bool success = ParseMatJsonBlob(matSource, sid, matJ);
			if (!success)
				return false;

			if (!matJ.IsArray()) {
				BOLTS_LERROR() << BOLTS_LOG_SCOPE << "Material JSON malformed.Root document isn't an array";
				return false;
			}
			// iterate through materials
			for (auto matIt = matJ.Begin(); matIt != matJ.End(); matIt++) {
				if (!matIt->IsObject()) {
					BOLTS_LERROR() << BOLTS_LOG_SCOPE << "Material JSON malformed.Root array has non-object element";
					return false;
				}
				//////////////////
				//TODO: Check for material name collisions somehow !
				str_t matName;
				if (!ReadMember(*matIt, "name", matName)) {
					continue;
				}
				//Parse JSON and extract data
				auto currentMat = GetOrCreateMaterial(assetManager, { matName }, sid);
				success = ParseMaterialInstanceJSON(assetManager, *matIt, matName, *currentMat);
				BASSERT(success);
				if (!success)
					currentMat->SetAssetState(EAS_ERROR);
				else if (currentMat->GetAssetState() != EAS_UNLOADED) // if already loaded, notify changes
					assetManager.NotifySourceChanged( currentMat->GetAssetID());
				
			}
			return true;
		}

		static bool LoadMatInstanceJSON(AssetManager& assetManager, Blob::Reader matSource, IO::fileID_t sid, Rendering::Material& outMat)
		{
			using namespace rapidjson;
			using namespace Rendering;

			outMat.SetAssetState(EAS_ERROR);
			const char* matName = GetHashString(outMat.GetAssetID());

			Document matJ;
			bool success = ParseMatJsonBlob(matSource, sid, matJ);
			if (!success)
				return false;

			if (!matJ.IsObject()) {
				BOLTS_LERROR() << BOLTS_LOG_SCOPE << "Material JSON [" << matName << "]. Root document isn't an object.";
				return false;
			}

			// load base material
			success = ParseMaterialInstanceJSON(assetManager, matJ, matName, outMat);
			if (success)
				outMat.SetAssetState(EAS_UNLOADED);
			return success;
		}

		//

		MaterialLoader::MaterialLoader(AssetManager& am) :IAssetLoader(am) {
			assetDrawFunc = [](assetMixin* assetPtr) -> bool {
				return static_cast<AssetT*>(assetPtr)->DrawProperties();
			};
		}
		MaterialLoader::~MaterialLoader() {}

		void MaterialLoader::LoadFromSource(regHash_t sid)
		{
			// there are 2 possible "things" that a sid can be: a material name or a matpack filename
			auto material = m_assetManager.TryGetAsset<Rendering::Material>(sid);
			// not an existing material
			if (!material) {
				// could be a matpack
				Blob::Reader matpackData = IO::ReadAll(sid, IO::Temp);
				if (!matpackData.IsEmpty()) {
					//NOTE: Register a fake asset to this source so we reload base materials when matpack changes
					m_assetManager.RegisterAssetToSource(sid, AssetT::c_assetType, sid);
					// matpack
					BASSERT_ONLY(bool result =) LoadMatpackJson( m_assetManager, matpackData, sid);
					BASSERT(result);
					return;
				}

				// not a matpack, not a loaded material, try loading an instance file
				const char* matName = sid.s ? sid.s : "bad";
				BASSERT(matName);
				BASSERT(sid.s);
				char filenameBuffer[128];
				const char* matFilename = filenameBuffer;
				BASSERT_ONLY(int l = )sprintf_s(filenameBuffer, sizeof(filenameBuffer), "%s%s", matName, ".material");
				BASSERT(l < sizeof(filenameBuffer));
				regHash_t instanceSourceId = regHash_t(matFilename);
				auto instanceData = IO::ReadAll(instanceSourceId, IO::Temp);
				if (!instanceData.IsEmpty()) {
					material = GetOrCreateMaterial(m_assetManager, { matName }, instanceSourceId);
					bool loadedOK = LoadMatInstanceJSON(m_assetManager, instanceData, instanceSourceId, *material);
					BASSERT(loadedOK);
					if (!loadedOK)
						material = nullptr;
				}
			}
			//
			if (material)
			{
				BASSERT(material->GetAssetState() == EAS_UNLOADED);
				LoadMaterialDependencies(m_assetManager, *material);
				material->SetAssetState(assets::EAS_READY);
			} 
			else 
				BOLTS_LERROR() << BOLTS_LOG_SCOPE << "Couldn't load material from: " << sid.s;
		}

		void MaterialLoader::OnSourceChanged(IO::fileID_t sid, assetID_t aid)
		{
			// there are 2 possible "things" that an sid can be: a matpack filename or a .material filename
			// there are 2 possible "things" that an aid can be: a matpack filename or a material instance ID

			// check for matpack changes
			if (aid == sid) 
			{
				// this is a matpack change, we need to reload everything
				auto matpackData = IO::ReadAll(sid, IO::Temp);
				BASSERT(!matpackData.IsEmpty());
				if (!matpackData.IsEmpty()) {
					BASSERT_ONLY(bool result = ) LoadMatpackJson(m_assetManager, matpackData, sid);
					BASSERT(result);
				}
				return;
			}
			
			// not a matpack, try loading an instance file
			auto material = m_assetManager.TryGetAsset<Rendering::Material>(aid);

			const char* matName = GetHashString(aid);
			BASSERT(matName);
			if (!matName)
				matName = "bad";
			char filenameBuffer[128];
			const char* matFilename = filenameBuffer;
			BASSERT_ONLY(int l = )sprintf_s(filenameBuffer, sizeof(filenameBuffer), "%s%s", matName, ".material");
			BASSERT(l < sizeof(filenameBuffer));
			if (sid == regHash_t(matFilename))
			{
				// material instance file
				auto instanceData = IO::ReadAll(sid, IO::Temp);
				BASSERT(!instanceData.IsEmpty());
				LoadMatInstanceJSON(m_assetManager, instanceData, sid, *material);
			}

			if (material && material->GetAssetState() != EAS_ERROR) {
				LoadMaterialDependencies(m_assetManager, *material);
				material->SetAssetState(assets::EAS_READY);
				material->IncrementReloads();
			}
			else {
				BOLTS_LERROR() << BOLTS_LOG_SCOPE << "Couldn't load material from: " << GetHashString(sid);
			}
		}

		void MaterialLoader::OnUnload(assetPtr_t& assetPtr)
		{
			auto pAsset = reinterpret_cast<AssetT*> (assetPtr.detach());
			pAsset->Unload();
			delete pAsset;
		}
	}
};