#pragma once

#include <Assets/Assets.h>
#include <Assets/AssetManager.h>
#include <Rendering/MeshFwd.h>
#include <bolts_binaryserialization.h>
#include <string>

namespace Bolts
{
	namespace assets {
		class MeshLoader :public IAssetLoader
		{
		public:
			typedef Rendering::Mesh AssetT;

			MeshLoader(AssetManager&);
			~MeshLoader();

			virtual void LoadFromSource(regHash_t) override;
			virtual void OnSourceChanged(IO::fileID_t, assetID_t) override;
			virtual void OnUnload(assetPtr_t&) override;
		private:
			void LoadMeshInternal(IO::fileID_t fileID, AssetT*);
		};
	};
};

