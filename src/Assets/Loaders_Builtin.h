#pragma once

#include <Assets/AssetManager.h>
#include <Assets/Loader_Animations.h>
#include <Assets/Loader_Materials.h>
#include <Assets/Loader_Meshes.h>
#include <Assets/Loader_Shaders.h>
#include <Assets/Loader_Textures.h>
#include <memory>

namespace Bolts
{
	void RegisterBuiltinAssets(assets::AssetManager& am)
	{
		using namespace assets;
		
		am.RegisterAssetType(std::make_unique<AnimLoader>(am), EAT_ANIMATION_SET);
		am.RegisterAssetType(std::make_unique<TextureLoader>(am), EAT_TEXTURE);
		am.RegisterAssetType(std::make_unique<MeshLoader>(am), EAT_MESH);
		am.RegisterAssetType(std::make_unique<ShaderLoader>(am), EAT_GPU_PROGRAM);
		am.RegisterAssetType(std::make_unique<MaterialLoader>(am), EAT_MATERIAL);
	}

};