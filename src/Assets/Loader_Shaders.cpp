#include "Assets/Loader_Shaders.h"
#include "Assets/AssetManager.h"
#include "Rendering/GPUProgram.h"
#include "Rendering/RenderBackend.h"
#include "IO/FileProviderImpl.h"
#include <bolts_log.h>
#include <bolts_hash.h>
#include <bolts_binarymap_serialization.h>

#define BOLTS_LOG_SCOPE "[ShadersLoad] "

namespace Bolts {
	namespace assets {


		assetID_t ShaderLoader::RegisterShaderProgramData(const str_t& vsFilename, const str_t& fsFilename, const vec_t<str_t>& defines)
		{
			BASSERT(!vsFilename.empty());
			BASSERT(!fsFilename.empty());
			// work out programID. Combined hash of the paths and the defines
			stringHash_t	progHash;
			progHash = HashString(vsFilename);
			progHash = HashString(progHash, fsFilename);
			for (auto& str : defines)
				progHash = HashString(progHash, str);
			// register GPU shaders/defines combo so loader can fetch it later
			if (!IO::GetMemorySource().HasFileData(progHash)) {
				Blob programData(1024);
				programData.Store(vsFilename);
				programData.Store(fsFilename);
				for (auto& define: defines)
					programData.Store(define);
				IO::GetMemorySource().AddSourceData(progHash, std::move(programData));
			}
			//TODO: add a debug check for hash collisions here
			return progHash;
		}

		bool ShaderLoader::GetShaderProgramPaths(assetID_t programID, str_t& vsFilename, str_t& fsFilename)
		{
			auto reader = IO::ReadAll(programID, IO::Forever);
			if (reader.IsEmpty())
				return false;
			BASSERT_ONLY(bool OK = )reader.Read(vsFilename);
			BASSERT(OK);
			BASSERT_ONLY(OK = )reader.Read(fsFilename);
			BASSERT(OK);
			return true;
		}

		bool ShaderLoader::GetShaderProgramData( assetID_t programID, str_t& vsFilename, str_t& fsFilename, vec_t<str_t>& defines)
		{
			auto reader = IO::ReadAll(programID, IO::Forever);
			if (reader.IsEmpty())
				return false;
			BASSERT_ONLY(bool OK = )reader.Read(vsFilename);
			BASSERT(OK);
			BASSERT_ONLY(OK = )reader.Read(fsFilename);
			BASSERT(OK);
			
			defines.clear();
			while (reader.Remaining() > 0) {
				defines.emplace_back();
				auto& define = defines.back();
				BASSERT_ONLY(OK = )reader.Read(define);
				BASSERT(OK);
			}
			return true;
		}

		ShaderLoader::ShaderLoader(AssetManager& am) :IAssetLoader(am) {
			assetDrawFunc = [](assetMixin* assetPtr) -> bool {
				return static_cast<AssetT*>(assetPtr)->DrawProperties();
			};
		}
		ShaderLoader::~ShaderLoader() {}

		void ShaderLoader::LoadFromSource(regHash_t sid)
		{
			auto newProgram = new AssetT(sid);
			LoadProgramInternal(sid, newProgram);
			m_assetManager.AddAsset(sid, AssetT::c_assetType, newProgram);
		}

		void ShaderLoader::LoadProgramInternal(IO::fileID_t sid , AssetT* outPrg)
		{
			auto programInfo = IO::ReadAll(sid, IO::Frame);
			BASSERT(!programInfo.IsEmpty());
			if (programInfo.IsEmpty()) {
				outPrg->SetAssetState(EAS_NOT_FOUND);
				return;
			}
			bool newLoad = false;
			if (outPrg->GetAssetState() == EAS_UNLOADED)
				newLoad = true;
			
			m_assetManager.RegisterAssetToSource(sid, AssetT::c_assetType, sid);
			outPrg->SetAssetState(EAS_LOADING);
			// Grab vs and fs sources
			const char *vsPath = {}, *firstDefine = {}, *fsPath = {};
			programInfo.Read(vsPath);
			programInfo.Read(fsPath);
			int definesLen = programInfo.Remaining();
			if (definesLen > 0)
				programInfo.Read(firstDefine);
			hash32_t vsNameHash = HashString(vsPath);
			hash32_t fsNameHash = HashString(fsPath);
			auto vsData = IO::ReadAll(vsNameHash, IO::Frame);
			auto fsData = IO::ReadAll(fsNameHash, IO::Frame);
			if (vsData.IsEmpty() || fsData.IsEmpty()) {
				BOLTS_LERROR() << BOLTS_LOG_SCOPE << "Could not open shader source files: " << vsPath << " | " << fsPath;
				outPrg->SetAssetState(EAS_NOT_FOUND);
				return;
			}
			if (newLoad) {
				m_assetManager.RegisterAssetToSource(vsNameHash, AssetT::c_assetType, sid);
				m_assetManager.RegisterAssetToSource(fsNameHash, AssetT::c_assetType, sid);
			}
			outPrg->QueueLoad(*m_assetManager.GetCommandBuffer(), vsData, fsData, firstDefine, definesLen);
		}

		void ShaderLoader::OnSourceChanged( IO::fileID_t, assetID_t assetID)
		{
			auto loadedAsset = m_assetManager.TryGetAsset<AssetT>(assetID);
			BASSERT(loadedAsset);
			if (!loadedAsset)
				return;
			// Reload data
			loadedAsset->IncrementReloads();
			if (loadedAsset->GetAssetState() != EAS_LOADING) {
				LoadProgramInternal(assetID, loadedAsset.get());
			}
		}

		void ShaderLoader::OnUnload(assetPtr_t& assetPtr)
		{
			auto pAsset = reinterpret_cast<AssetT*> (assetPtr.detach());
			pAsset->Unload(*m_assetManager.GetCommandBuffer());
			delete pAsset;
		}
	};
};