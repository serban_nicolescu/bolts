#pragma once
#include <bolts_core.h>
#include <bolts_binaryserialization.h>

namespace Bolts 
{
	struct AnimHeader;

	template<>
	struct is_pod< AnimHeader> : public std::true_type {};

	struct AnimClip
	{
		uint16_t	firstTrack;
		uint16_t	endTrack;
		uint32_t	nameHash;
		float		frameDuration; // in seconds. time between two keys
		uint32_t	numFrames;
		float		duration;// in seconds. (numFrames - 1) * frameDuration;
		char		clipName[32];
	};

	struct AnimTransform
	{
		vec3 pos;
		quat rot;
		vec3 scale;
	};

	struct AnimTrack
	{
		uint16_t	boneIdx;
		uint16_t	firstTransform; // index of first transform
	};

	struct AnimHeader
	{
		static const int c_latestVersion = 2;

		uint32_t versionNo = c_latestVersion;

		OffsetPtr< InplaceArray<hash32_t>>		boneNameHashes;
		OffsetPtr< InplaceArray<AnimClip>>		clips;
		OffsetPtr< InplaceArray<AnimTrack>>		tracks;
		OffsetPtr< InplaceArray<AnimTransform>>	transforms;
	};

};