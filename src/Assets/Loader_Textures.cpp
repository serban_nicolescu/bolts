#include "Assets/Loader_Textures.h"
#include "Rendering/RenderBackend.h"
#include "Rendering/Texture.h"
//#include "IO/FileIO.h"
#include <bolts_core.h>
#include <memory>
#include "jpeglib.h" //TODO: IS this still needed

#define STBI_HEADER_FILE_ONLY //SFML also loads stbimage
#include <Soil.h>

#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cstdint>

namespace Bolts {
	namespace assets
	{

		namespace
		{

#ifdef MACOS
			inline void swap_endian(void *val)
			{
				unsigned int *ival = (unsigned int *)val;

				*ival = ((*ival >> 24) & 0x000000ff) |
					((*ival >> 8) & 0x0000ff00) |
					((*ival << 8) & 0x00ff0000) |
					((*ival << 24) & 0xff000000);
			}
#else
			inline void swap_endian(void*)	{}
#endif

			///////////////////////////////////////////////////////////////////////////////
			// calculates size of DXTC texture in bytes
			inline unsigned int size_dxtc(unsigned int width, unsigned int height, unsigned int bpp)
			{
				return ((width + 3) / 4) * ((height + 3) / 4) * bpp;
			}

			inline unsigned clamp_size(unsigned size)
			{
				return (size <= 0 ? 1 : size);
			}

		}

		bool IsTexDDS(Blob::Reader data)
		{
			return (strncmp( (const char*)data.Head(), "DDS", 3) == 0);
		}

		bool IsTexJPEG(Blob::Reader data)
		{
			uint16_t header;
			data.Peek(header);
			return header == 0xD8FF;
		}

		bool LoadTexDDS(Blob::Reader data, Rendering::RCB& commands, Bolts::Rendering::Texture* outTex)
		{
			//See: https://msdn.microsoft.com/en-us/library/windows/desktop/bb943991(v=vs.85).aspx#File_Layout1
			BASSERT(IsTexDDS(data));
			// surface description flags
			const uint32_t DDSF_CAPS = 0x00000001;
			const uint32_t DDSF_HEIGHT = 0x00000002;
			const uint32_t DDSF_WIDTH = 0x00000004;
			const uint32_t DDSF_PITCH = 0x00000008;
			const uint32_t DDSF_PIXELFORMAT = 0x00001000;
			const uint32_t DDSF_MIPMAPCOUNT = 0x00020000;
			const uint32_t DDSF_LINEARSIZE = 0x00080000;
			const uint32_t DDSF_DEPTH = 0x00800000;

			// pixel format flags
			const uint32_t DDSF_ALPHAPIXELS = 0x00000001;
			const uint32_t DDSF_FOURCC = 0x00000004;
			const uint32_t DDSF_RGB = 0x00000040;
			const uint32_t DDSF_RGBA = 0x00000041;

			// dwCaps1 flags
			const uint32_t DDSF_COMPLEX = 0x00000008;
			const uint32_t DDSF_TEXTURE = 0x00001000;
			const uint32_t DDSF_MIPMAP = 0x00400000;

			// dwCaps2 flags
			const uint32_t DDSF_CUBEMAP = 0x00000200;
			const uint32_t DDSF_CUBEMAP_POSITIVEX = 0x00000400;
			const uint32_t DDSF_CUBEMAP_NEGATIVEX = 0x00000800;
			const uint32_t DDSF_CUBEMAP_POSITIVEY = 0x00001000;
			const uint32_t DDSF_CUBEMAP_NEGATIVEY = 0x00002000;
			const uint32_t DDSF_CUBEMAP_POSITIVEZ = 0x00004000;
			const uint32_t DDSF_CUBEMAP_NEGATIVEZ = 0x00008000;
			const uint32_t DDSF_CUBEMAP_ALL_FACES = 0x0000FC00;
			const uint32_t DDSF_VOLUME = 0x00200000;

			// compressed texture types
			const uint32_t FOURCC_DXT1 = 0x31545844; //(MAKEFOURCC('D','X','T','1'))
			const uint32_t FOURCC_DXT3 = 0x33545844; //(MAKEFOURCC('D','X','T','3'))
			const uint32_t FOURCC_DXT5 = 0x35545844; //(MAKEFOURCC('D','X','T','5'))
			const uint32_t FOURCC_DX10 = 0x30315844; //(MAKEFOURCC('D','X','1','0'))
			const uint32_t FOURCC_ATI2 = 0x32495441; //(MAKEFOURCC('A','T','I','2')) // BC5/ATI2/3DC/RGTC/LATC

			struct DDS_PIXELFORMAT
			{
				uint32_t dwSize;
				uint32_t dwFlags;
				uint32_t dwFourCC;
				uint32_t dwRGBBitCount;
				uint32_t dwRBitMask;
				uint32_t dwGBitMask;
				uint32_t dwBBitMask;
				uint32_t dwABitMask;
			};

			struct DDS_HEADER
			{
				uint32_t dwSize;
				uint32_t dwFlags;
				uint32_t dwHeight;
				uint32_t dwWidth;
				uint32_t dwPitchOrLinearSize;
				uint32_t dwDepth;
				uint32_t dwMipMapCount;
				uint32_t dwReserved1[11];
				DDS_PIXELFORMAT ddspf;
				uint32_t dwCaps1;
				uint32_t dwCaps2;
				uint32_t dwReserved2[3];
			};


			// miscFlag flags
			const uint32_t DDS_RESOURCE_MISC_TEXTURECUBE = 0x4;

			enum D3D10_RESOURCE_DIMENSION: uint32_t {
				D3D10_RESOURCE_DIMENSION_UNKNOWN = 0,
				D3D10_RESOURCE_DIMENSION_BUFFER = 1,
				D3D10_RESOURCE_DIMENSION_TEXTURE1D = 2,
				D3D10_RESOURCE_DIMENSION_TEXTURE2D = 3,
				D3D10_RESOURCE_DIMENSION_TEXTURE3D = 4
			};

			enum DXGI_FORMAT : uint32_t {
				DXGI_FORMAT_UNKNOWN = 0,
				DXGI_FORMAT_R32G32B32A32_TYPELESS = 1,
				DXGI_FORMAT_R32G32B32A32_FLOAT = 2,
				DXGI_FORMAT_R32G32B32A32_UINT = 3,
				DXGI_FORMAT_R32G32B32A32_SINT = 4,
				DXGI_FORMAT_R32G32B32_TYPELESS = 5,
				DXGI_FORMAT_R32G32B32_FLOAT = 6,
				DXGI_FORMAT_R32G32B32_UINT = 7,
				DXGI_FORMAT_R32G32B32_SINT = 8,
				DXGI_FORMAT_R16G16B16A16_TYPELESS = 9,
				DXGI_FORMAT_R16G16B16A16_FLOAT = 10,
				DXGI_FORMAT_R16G16B16A16_UNORM = 11,
				DXGI_FORMAT_R16G16B16A16_UINT = 12,
				DXGI_FORMAT_R16G16B16A16_SNORM = 13,
				DXGI_FORMAT_R16G16B16A16_SINT = 14,
				DXGI_FORMAT_R32G32_TYPELESS = 15,
				DXGI_FORMAT_R32G32_FLOAT = 16,
				DXGI_FORMAT_R32G32_UINT = 17,
				DXGI_FORMAT_R32G32_SINT = 18,
				DXGI_FORMAT_R32G8X24_TYPELESS = 19,
				DXGI_FORMAT_D32_FLOAT_S8X24_UINT = 20,
				DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS = 21,
				DXGI_FORMAT_X32_TYPELESS_G8X24_UINT = 22,
				DXGI_FORMAT_R10G10B10A2_TYPELESS = 23,
				DXGI_FORMAT_R10G10B10A2_UNORM = 24,
				DXGI_FORMAT_R10G10B10A2_UINT = 25,
				DXGI_FORMAT_R11G11B10_FLOAT = 26,
				DXGI_FORMAT_R8G8B8A8_TYPELESS = 27,
				DXGI_FORMAT_R8G8B8A8_UNORM = 28,
				DXGI_FORMAT_R8G8B8A8_UNORM_SRGB = 29,
				DXGI_FORMAT_R8G8B8A8_UINT = 30,
				DXGI_FORMAT_R8G8B8A8_SNORM = 31,
				DXGI_FORMAT_R8G8B8A8_SINT = 32,
				DXGI_FORMAT_R16G16_TYPELESS = 33,
				DXGI_FORMAT_R16G16_FLOAT = 34,
				DXGI_FORMAT_R16G16_UNORM = 35,
				DXGI_FORMAT_R16G16_UINT = 36,
				DXGI_FORMAT_R16G16_SNORM = 37,
				DXGI_FORMAT_R16G16_SINT = 38,
				DXGI_FORMAT_R32_TYPELESS = 39,
				DXGI_FORMAT_D32_FLOAT = 40,
				DXGI_FORMAT_R32_FLOAT = 41,
				DXGI_FORMAT_R32_UINT = 42,
				DXGI_FORMAT_R32_SINT = 43,
				DXGI_FORMAT_R24G8_TYPELESS = 44,
				DXGI_FORMAT_D24_UNORM_S8_UINT = 45,
				DXGI_FORMAT_R24_UNORM_X8_TYPELESS = 46,
				DXGI_FORMAT_X24_TYPELESS_G8_UINT = 47,
				DXGI_FORMAT_R8G8_TYPELESS = 48,
				DXGI_FORMAT_R8G8_UNORM = 49,
				DXGI_FORMAT_R8G8_UINT = 50,
				DXGI_FORMAT_R8G8_SNORM = 51,
				DXGI_FORMAT_R8G8_SINT = 52,
				DXGI_FORMAT_R16_TYPELESS = 53,
				DXGI_FORMAT_R16_FLOAT = 54,
				DXGI_FORMAT_D16_UNORM = 55,
				DXGI_FORMAT_R16_UNORM = 56,
				DXGI_FORMAT_R16_UINT = 57,
				DXGI_FORMAT_R16_SNORM = 58,
				DXGI_FORMAT_R16_SINT = 59,
				DXGI_FORMAT_R8_TYPELESS = 60,
				DXGI_FORMAT_R8_UNORM = 61,
				DXGI_FORMAT_R8_UINT = 62,
				DXGI_FORMAT_R8_SNORM = 63,
				DXGI_FORMAT_R8_SINT = 64,
				DXGI_FORMAT_A8_UNORM = 65,
				DXGI_FORMAT_R1_UNORM = 66,
				DXGI_FORMAT_R9G9B9E5_SHAREDEXP = 67,
				DXGI_FORMAT_R8G8_B8G8_UNORM = 68,
				DXGI_FORMAT_G8R8_G8B8_UNORM = 69,
				DXGI_FORMAT_BC1_TYPELESS = 70,
				DXGI_FORMAT_BC1_UNORM = 71,
				DXGI_FORMAT_BC1_UNORM_SRGB = 72,
				DXGI_FORMAT_BC2_TYPELESS = 73,
				DXGI_FORMAT_BC2_UNORM = 74,
				DXGI_FORMAT_BC2_UNORM_SRGB = 75,
				DXGI_FORMAT_BC3_TYPELESS = 76,
				DXGI_FORMAT_BC3_UNORM = 77,
				DXGI_FORMAT_BC3_UNORM_SRGB = 78,
				DXGI_FORMAT_BC4_TYPELESS = 79,
				DXGI_FORMAT_BC4_UNORM = 80,
				DXGI_FORMAT_BC4_SNORM = 81,
				DXGI_FORMAT_BC5_TYPELESS = 82,
				DXGI_FORMAT_BC5_UNORM = 83,
				DXGI_FORMAT_BC5_SNORM = 84,
				DXGI_FORMAT_B5G6R5_UNORM = 85,
				DXGI_FORMAT_B5G5R5A1_UNORM = 86,
				DXGI_FORMAT_B8G8R8A8_UNORM = 87,
				DXGI_FORMAT_B8G8R8X8_UNORM = 88,
				DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM = 89,
				DXGI_FORMAT_B8G8R8A8_TYPELESS = 90,
				DXGI_FORMAT_B8G8R8A8_UNORM_SRGB = 91,
				DXGI_FORMAT_B8G8R8X8_TYPELESS = 92,
				DXGI_FORMAT_B8G8R8X8_UNORM_SRGB = 93,
				DXGI_FORMAT_BC6H_TYPELESS = 94,
				DXGI_FORMAT_BC6H_UF16 = 95,
				DXGI_FORMAT_BC6H_SF16 = 96,
				DXGI_FORMAT_BC7_TYPELESS = 97,
				DXGI_FORMAT_BC7_UNORM = 98,
				DXGI_FORMAT_BC7_UNORM_SRGB = 99,
				DXGI_FORMAT_AYUV = 100,
				DXGI_FORMAT_Y410 = 101,
				DXGI_FORMAT_Y416 = 102,
				DXGI_FORMAT_NV12 = 103,
				DXGI_FORMAT_P010 = 104,
				DXGI_FORMAT_P016 = 105,
				DXGI_FORMAT_420_OPAQUE = 106,
				DXGI_FORMAT_YUY2 = 107,
				DXGI_FORMAT_Y210 = 108,
				DXGI_FORMAT_Y216 = 109,
				DXGI_FORMAT_NV11 = 110,
				DXGI_FORMAT_AI44 = 111,
				DXGI_FORMAT_IA44 = 112,
				DXGI_FORMAT_P8 = 113,
				DXGI_FORMAT_A8P8 = 114,
				DXGI_FORMAT_B4G4R4A4_UNORM = 115,
				DXGI_FORMAT_P208 = 130,
				DXGI_FORMAT_V208 = 131,
				DXGI_FORMAT_V408 = 132,
				DXGI_FORMAT_FORCE_UINT = 0xffffffff
			};

			struct DDS_HEADER_DXT10 {
				DXGI_FORMAT			dxgiFormat;
				D3D10_RESOURCE_DIMENSION	resourceDimension;
				unsigned int		miscFlag;
				unsigned int		arraySize;
				unsigned int		miscFlags2;
			};

			// open file
			using namespace Bolts::Rendering;

			const size_t fileSize = data.Remaining();
			//unsigned dataOffset = 4;// Skip file header
			uint32_t file;// Skip file header
			data.Read(file);

			// default to flat texture type (2D or rectangle)
			TextureType tt = BTT_2D;

			// read in DDS header
			DDS_HEADER ddsh;//= *reinterpret_cast<const DDS_HEADER*>(data.Data() + dataOffset);
			//dataOffset += sizeof(DDS_HEADER);
			data.Read(ddsh);
			swap_endian(&ddsh.dwSize);
			swap_endian(&ddsh.dwFlags);
			swap_endian(&ddsh.dwHeight);
			swap_endian(&ddsh.dwWidth);
			swap_endian(&ddsh.dwPitchOrLinearSize);
			swap_endian(&ddsh.dwMipMapCount);
			swap_endian(&ddsh.ddspf.dwSize);
			swap_endian(&ddsh.ddspf.dwFlags);
			swap_endian(&ddsh.ddspf.dwFourCC);
			swap_endian(&ddsh.ddspf.dwRGBBitCount);
			swap_endian(&ddsh.dwCaps1);
			swap_endian(&ddsh.dwCaps2);
			TextureFormat format;
			// read DDS header version
			if ((ddsh.ddspf.dwFlags & DDSF_FOURCC) &&
				(ddsh.ddspf.dwFourCC == FOURCC_DX10)) {

				DDS_HEADER_DXT10 ddsh10;// = *reinterpret_cast<const DDS_HEADER_DXT10*>(data.Head() + dataOffset);
				//dataOffset += sizeof(DDS_HEADER_DXT10);
				data.Read(ddsh10);
				swap_endian(&ddsh10.arraySize);
				swap_endian(&ddsh10.dxgiFormat);
				swap_endian(&ddsh10.miscFlag);
				swap_endian(&ddsh10.miscFlags2);
				swap_endian(&ddsh10.resourceDimension);
				BASSERT(ddsh10.arraySize == 1); //TODO: Add support for loading array textures


				switch (ddsh10.resourceDimension) {
				case D3D10_RESOURCE_DIMENSION_TEXTURE1D:
					tt = BTT_1D; break;
				case D3D10_RESOURCE_DIMENSION_TEXTURE2D:
					tt = BTT_2D; break;
				case D3D10_RESOURCE_DIMENSION_TEXTURE3D:
					tt = BTT_3D; break;
				default:
					BOLTS_LWARNING() << "[TexLoader] " << "Loading DDS texture with unknown dimension: " << ddsh10.resourceDimension;
					return false;
				}
				if (tt == BTT_2D && ddsh10.miscFlag == DDS_RESOURCE_MISC_TEXTURECUBE)
					tt = BTT_CUBE;
				// figure out what the image format is
				switch (ddsh10.dxgiFormat) {
				case DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM:
					format = BTF_RGBA;	break;
				case DXGI_FORMAT::DXGI_FORMAT_R16_FLOAT:
					format = BTF_R16F;	break;
				case DXGI_FORMAT::DXGI_FORMAT_R16G16_FLOAT:
					format = BTF_RG16F;	break;
				case DXGI_FORMAT::DXGI_FORMAT_R16G16B16A16_FLOAT:
					format = BTF_RGBA16F;	break;
				case DXGI_FORMAT::DXGI_FORMAT_BC1_UNORM:
					format = BTF_DXT1_RGB;	break;
				case DXGI_FORMAT::DXGI_FORMAT_BC3_UNORM:
					format = BTF_DXT3_RGBA;	break;
				case DXGI_FORMAT::DXGI_FORMAT_BC5_UNORM:
					format = BTF_DXT5_RGBA;	break;
				default:
					BOLTS_LWARNING() << "[TexLoader] " << "Unsupported texture format in DDS file: " << ddsh10.dxgiFormat;
					return false;
				}

			}
			else { 
				// Non-DX10 header
				// check if image is a cubemap
				if (ddsh.dwCaps2 & DDSF_CUBEMAP) {
					tt = BTT_CUBE;
				}
				// check if image is a volume texture
				if ((ddsh.dwCaps2 & DDSF_VOLUME) && (ddsh.dwDepth > 0)) {
					BOLTS_LWARNING() << "[TexLoader] " << "Loading 3D DDS textures in not currently implemented";
					return false;
				}
				// figure out what the image format is
				if (ddsh.ddspf.dwFlags & DDSF_FOURCC) {
					char fourcc[5];
					fourcc[4] = '\0';
					memcpy(fourcc, &ddsh.ddspf.dwFourCC, 4);
					switch (ddsh.ddspf.dwFourCC) {
					case FOURCC_DXT1:
						format = BTF_DXT1_RGB;
						break;
					case FOURCC_DXT3:
						format = BTF_DXT3_RGBA;
						break;
					case FOURCC_DXT5:
						format = BTF_DXT5_RGBA;
						break;
					//case FOURCC_ATI1:
					//	format = BTF_BC4_R;
					//	break;
					case FOURCC_ATI2:
						format = BTF_BC5_RG;
						break;
					default:
						BOLTS_LWARNING() << "[TexLoader] " << "Unsupported texture format in DDS file: " << fourcc;
						return false;
					}
				}
				else if (ddsh.ddspf.dwFlags == DDSF_RGBA && ddsh.ddspf.dwRGBBitCount == 32) {
					//ret.m_format = ETF_BGRA8;
					format = BTF_RGBA;

				}
				else if (ddsh.ddspf.dwFlags == DDSF_RGB && ddsh.ddspf.dwRGBBitCount == 32) {
					//ret.m_format = ETF_BGRA8;
					format = BTF_RGBA;
				}
				else if (ddsh.ddspf.dwFlags == DDSF_RGB && ddsh.ddspf.dwRGBBitCount == 24) {
					//ret.m_format = ETF_BGR8;
					format = BTF_RGB;
				}
				else if (ddsh.ddspf.dwRGBBitCount == 8) {
					format = BTF_R;
				}
				else {
					BOLTS_LWARNING() << "[TexLoader] " << "Unsupported texture format in DDS file";
					return false;
				}
			}

			const unsigned numMipmaps = ddsh.dwMipMapCount;
			bool withMips = (numMipmaps > 1);
			// store primary surface width/height/depth
			const unsigned width = ddsh.dwWidth;
			const unsigned height = ddsh.dwHeight;
			BASSERT(ddsh.dwDepth <= 1);
			const unsigned depth = clamp_size(ddsh.dwDepth);   // set to 1 if 0
			uvec3 dimensions = Bolts::uvec3(width, height, depth);

			// Load all surfaces for the image (6 surfaces for cubemaps, )
			unsigned texSurfaces = 1;
			if (tt == BTT_CUBE) {
				texSurfaces = 6; // Cubemap data is layed out like an array of 6 Tex2D textures
				//withMips = true;
			}

			auto texData = outTex->SetTexture(commands, tt, dimensions, format, BTDF_IMPLICIT, withMips);
			BASSERT_ONLY(unsigned remain = data.Remaining());
			BASSERT(remain == texData.dataSize);//Make sure we've worked out the size correctly
			memcpy(texData.data, data.Head(), texData.dataSize);
			if (!withMips)
				outTex->GenerateMipmaps(commands);
			return true;
		}

		bool LoadTexJPEG(Blob::Reader data, Rendering::RCB& commands, Bolts::Rendering::Texture* outTex)
		{
			BASSERT(IsTexJPEG(data));
			using namespace Bolts::Rendering;

			struct jpeg_decompress_struct cinfo;
			struct jpeg_error_mgr jerr;
			cinfo.err = jpeg_std_error(&jerr);
			jpeg_create_decompress(&cinfo);
			//NOTE: This is also a const cast. it doesn't actually change data, but still ...
			jpeg_mem_src(&cinfo, reinterpret_cast<unsigned char*>(data.Head()), data.Remaining());
			jpeg_read_header(&cinfo, true);
			cinfo.quantize_colors = false;
			//Change read settings here: Color mapping, rescaling
			jpeg_start_decompress(&cinfo);
			if (cinfo.output_components != 1 && cinfo.output_components != 3) {
				jpeg_destroy_decompress(&cinfo);
				BOLTS_LWARNING() << "[TexLoader] " << "Input JPEG image has " << cinfo.output_components << " components. Bolts only supports 1 and 3 component JPEGS";
				return false;
			}
			//
			{
				const unsigned scanlineSize = cinfo.output_width * cinfo.output_components;
				const unsigned fileOutTexSize = scanlineSize * cinfo.output_height;
				//First, create texture storage and set appropriate flags
				TextureFormat format = (cinfo.output_components == 1 ? BTF_R : BTF_RGB);
				uvec3 dimensions = Bolts::uvec3(cinfo.output_width, cinfo.output_height, 1);
				auto texData = outTex->SetTexture(commands, BTT_2D, dimensions, format, BTDF_IMPLICIT, false);
				BASSERT(fileOutTexSize == texData.dataSize);
				//Next decompress and read actual texture data
				unsigned char* scanlineOutPtr = (unsigned char*)texData.data;
				while (cinfo.output_scanline < cinfo.output_height) {
					auto numReadLines = jpeg_read_scanlines(&cinfo, &scanlineOutPtr, 1);
					scanlineOutPtr += numReadLines * scanlineSize;
				}
				//
				outTex->GenerateMipmaps(commands);
			}
			jpeg_destroy_decompress(&cinfo);
			return true;
		}

		bool LoadTexTGA(Blob::Reader data, Rendering::RCB& commands, Bolts::Rendering::Texture* outTex)
		{
			using namespace Bolts::Rendering;

			int width, height, channels;
			//TODO: So many copies. Ugh
			//	We could just read image headers here, but SOIL has no func to do this
			unsigned char* tempDataBuffer = SOIL_load_image_from_memory((unsigned char*)data.Head(), data.Remaining(), &width, &height, &channels, 0);
			if (!tempDataBuffer)
				return false;

			TextureFormat format;
			switch (channels) {
			case 1:
				format = BTF_R; break;
			case 3:
				format = BTF_RGB; break;
			case 4:
				format = BTF_RGBA; break;
			default:
				//Unsupported format
				SOIL_free_image_data(tempDataBuffer);
				return false;
			}
			uvec3 dimensions = Bolts::uvec3(width, height, 1);
			auto texData = outTex->SetTexture(commands, BTT_2D, dimensions, format, BTDF_IMPLICIT, false);
			memcpy(texData.data, tempDataBuffer, texData.dataSize);
			outTex->GenerateMipmaps(commands);
			SOIL_free_image_data(tempDataBuffer);
			return true;
		}

		void TextureLoader::LoadTexture(IO::fileID_t fileID, TextureLoader::AssetT* texture)
		{
			Blob::Reader fileData = IO::ReadAll(fileID, IO::Temp);
			if (fileData.IsEmpty()) {
				texture->SetAssetState(EAS_NOT_FOUND);
			}
			else {
				auto commandBufferPtr = m_assetManager.GetCommandBuffer();
				texture->Init(*commandBufferPtr);

				bool loadOK;
				if (IsTexDDS(fileData))
					loadOK = LoadTexDDS(fileData, *commandBufferPtr, texture);
				else if (IsTexJPEG(fileData))
					loadOK = LoadTexJPEG(fileData, *commandBufferPtr, texture);
				else
					loadOK = LoadTexTGA(fileData, *commandBufferPtr, texture);
				//
				if (loadOK)
					texture->SetAssetState(EAS_READY);
				else
					texture->SetAssetState(EAS_ERROR);
			}
		}

		TextureLoader::TextureLoader(AssetManager& am):IAssetLoader(am)
		{
			assetDrawFunc = [](assetMixin* assetPtr) -> bool {
				return static_cast<AssetT*>(assetPtr)->DrawProperties();
			};
		}

		TextureLoader::~TextureLoader() {}

		void TextureLoader::LoadFromSource(regHash_t sid)
		{
			Rendering::Texture* newTexture = new Rendering::Texture(sid);
			LoadTexture(sid, newTexture);
			m_assetManager.AddAsset(sid, AssetT::c_assetType, newTexture);
			m_assetManager.RegisterAssetToSource(sid, AssetT::c_assetType, sid);
		}

		void TextureLoader::OnSourceChanged(IO::fileID_t fileID, assetID_t assetID)
		{
			auto loadedAsset = m_assetManager.TryGetAsset<Rendering::Texture>(assetID);
			BASSERT(loadedAsset);
			if (!loadedAsset)
				return;
			// Reload data
			loadedAsset->IncrementReloads();
			LoadTexture(fileID, loadedAsset.get());
		}

		void TextureLoader::OnUnload(assetPtr_t& assetPtr)
		{
			auto pAsset = reinterpret_cast<AssetT*> (assetPtr.detach());
			pAsset->Unload(*m_assetManager.GetCommandBuffer());
			delete pAsset;
		}
	}
};