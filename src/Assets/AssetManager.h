#pragma once
#include "Assets/Assets.h"
#include "IO/FileProvider.h"
#include <bolts_core.h>
#include <bolts_intrusiveptr.h>

namespace Bolts {

	//The target is to have a single manager for all assets
	//	With asset types defined independently ( no single AssetType enum )
	// All assets are ref-counted, but not destroyed immediately

	// ? How to handle hot-reloading ( simple for tex & shader code )
	//	Could store reload function in expanded asset ref ptr, but would need to hold ref list in asset

	// ? How to handle load jobs, for offloading work to separate threads ( where possible )

	namespace Rendering {
		class ResourceCommandBuffer;
		typedef ResourceCommandBuffer RCB;
	};

	namespace assets {

		class AssetManager;

		// ? How to register loaders for assets ?
		//	More C-functions ? Prolly better to just use an IAssetLoader base class
		// Loader callbacks should be:
		//- on first call to load for given asset ( immediate )
		//- on first frame start after call to load ?
		//- they can queue load tasks
		class IAssetLoader
		{
		public:
			IAssetLoader(AssetManager& am):m_assetManager(am) {}
			~IAssetLoader() {}

			virtual void	LoadFromSource(regHash_t) = 0;
			virtual void	OnSourceChanged(IO::fileID_t, assetID_t) = 0;
			virtual void	OnUnload(assetPtr_t&) = 0;

			assetMixin::drawFunc_t* assetDrawFunc = nullptr;
		protected:
			AssetManager& m_assetManager;
		};

		//TODO: to speed up resource cleanup we can register to a manager list when refcount reaches 0
		//			to be cleared later when unloading things ( registering multiple times doesn't matter, final destroy will de-duplicate the list )
		class AssetManager
		{
		public:
			typedef hash_map_t< hash32_t, assetPtr_t> assetMap_t;
			friend class IAssetLoader;

			static const int c_maxAssetTypeId = 32;
			//
			AssetManager();
			~AssetManager();

			void RegisterAssetType(std::unique_ptr<IAssetLoader> loader,int assetType)
			{
				m_loaders[assetType] = std::move(loader);
			}

			void UnloadUnreferencedAssets();

			template<typename T>
			typename T::Ptr LoadAsset(regHash_t assetID) {
				T::Ptr out;
				out = TryGetAsset<T>(assetID);
				if (!out || (out->GetAssetState() == EAS_UNLOADED)) {
					// Load it
					IO::fileID_t sid = assetID;// Use asset ID as a source identifier
					LoadAssetsInternal(sid, T::c_assetType);
					out = TryGetAsset<T>(assetID);
				}
				return out;
			}

			assetPtr_t LoadAssetByType(regHash_t assetID, uint32_t assetType) {
				assetPtr_t out;
				out = TryGetAsset(assetID, assetType);
				if (!out) {
					// Load it
					IO::fileID_t sid = assetID;// Use asset ID as a source identifier
					LoadAssetsInternal(sid, assetType);
					out = TryGetAsset(assetID, assetType);
				}
				return out;
			}

			template<typename T>
			BOLTS_INLINE void LoadAssetFile(regHash_t sid)
			{
				LoadAssetsInternal(sid, T::c_assetType);
			}

			//Try to find loaded asset. Returns nullptr if nothing with that name is loaded
			template<typename T>
			typename T::Ptr TryGetAsset(hash32_t nameHash)
			{
				static_assert(T::c_assetType < c_maxAssetTypeId, "Increase AssetManager::c_maxAssetTypeId to add more asset types");
				return static_cast<T*>(TryGetAsset(nameHash, T::c_assetType).get());
			}

			//TODO: Add new asset
			//TODO: Add asset from Blob

			// Editor
			assetMixin::drawFunc_t*		GetPropertyDrawFunction(uint32_t assetType) { return m_loaders[assetType]->assetDrawFunc; }
			assetMap_t&					GetLoadedAssets(uint32_t assetType) { return m_assets[assetType]; }
			const char*					GetAssetName(assetID_t id) { return GetHashString(id); }
			// Reloading
			void RegisterAssetToSource(IO::fileID_t sid, int assetType, assetID_t aid);
			void NotifySourceChanged(IO::fileID_t); // public so memory assets have a way to report changes
			//INTERNAL
			void AddAsset(regHash_t nameHash, int assetType, assetPtr_t ptr)
			{
				BASSERT(m_assets[assetType].count(nameHash) == 0);
				std::pair<assetMap_t::iterator, bool> res = m_assets[assetType].insert({ nameHash, ptr });
				BASSERT(res.second);
			}

			Rendering::RCB* 		GetCommandBuffer() const { return m_commands; }
			void 					SetCurrentCommandBuffer(Rendering::RCB* cb) { m_commands = cb; }
		private:

			//NOTE: Calling this multiple times, currently, loads the same thing repeatedly
			BOLTS_INLINE 
			void LoadAssetsInternal(IO::fileID_t sid, int assetType) {
				m_loaders[assetType]->LoadFromSource(sid);
			}

			assetPtr_t TryGetAsset(hash32_t nameHash, uint32_t assetType)
			{
				BASSERT_MSG(assetType < c_maxAssetTypeId, "Increase AssetManager::c_maxAssetTypeId to add more asset types");
				auto assetIt = m_assets[assetType].find(nameHash);
				if (assetIt == m_assets[assetType].end()) {
					return nullptr;
				}
				else {
					return assetIt->second;
				}
			}

			// Asset Management
			assetMap_t						m_assets[c_maxAssetTypeId];
			std::unique_ptr<IAssetLoader>	m_loaders[c_maxAssetTypeId];
			Rendering::RCB* 				m_commands = nullptr;
			//
			// Files & sources
			//Blob					m_emptyBlob;
			//TODO: change the way reloads are handled
			//	Probably best to just use a callback function per asset type
			struct reloadData
			{
				int assetType;
				assetID_t assetID;
			};
			hash_map_t<IO::fileID_t, vec_t<reloadData>>	m_reloadData;
		};

	};//assets
};//Bolts