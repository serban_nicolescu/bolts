#pragma once

#include <Assets/Assets.h>
#include <Assets/AssetManager.h>
#include <bolts_core.h>
#include <Rendering/MaterialNewFwd.h>

namespace Bolts {

	namespace assets
	{

		class MaterialLoader:public IAssetLoader
		{
		public:
			typedef Rendering::Material AssetT;

			MaterialLoader(AssetManager&);
			~MaterialLoader();

			virtual void LoadFromSource(regHash_t) override;
			virtual void OnSourceChanged(IO::fileID_t, assetID_t) override;
			virtual void OnUnload(assetPtr_t&) override;
		};

	};
};
