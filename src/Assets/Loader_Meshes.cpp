#include "Loader_Meshes.h"
#include "MeshFile.h"

#include "Rendering/Mesh.h"
#include <Assets/AssetManager.h>
#include <bolts_log.h>

#include <memory>

#define BOLTS_LOG_SCOPE "[AssetLoad] "

namespace Bolts {
	namespace assets {
		MeshLoader::MeshLoader(AssetManager& am) :IAssetLoader(am)
		{
			assetDrawFunc = [](assetMixin* assetPtr) -> bool {
				return static_cast<AssetT*>(assetPtr)->DrawProperties();
			};
		}

		MeshLoader::~MeshLoader() {}

		void MeshLoader::LoadFromSource(regHash_t sid)
		{
			auto newMesh = new AssetT(sid);
			LoadMeshInternal(sid, newMesh);
			m_assetManager.AddAsset(sid, AssetT::c_assetType, newMesh);
			m_assetManager.RegisterAssetToSource(sid, AssetT::c_assetType, sid);
		}

		void MeshLoader::OnSourceChanged(IO::fileID_t sid, assetID_t assetID)
		{
			auto loadedAsset = m_assetManager.TryGetAsset<AssetT>(assetID);
			BASSERT(loadedAsset);
			if (!loadedAsset)
				return;
			// Reload data
			loadedAsset->IncrementReloads();
			LoadMeshInternal(sid, loadedAsset.get());
		}

		void MeshLoader::LoadMeshInternal(IO::fileID_t fileID, AssetT* mesh)
		{
			Blob::Reader fileData = IO::ReadAll(fileID, IO::Temp);
			if (fileData.IsEmpty()) {
				mesh->SetAssetState(EAS_NOT_FOUND);
				return;
			}
			// unload data
			auto& commands = *m_assetManager.GetCommandBuffer();
			mesh->Unload(commands);
			//
			mesh->SetAssetState(EAS_READY);
			
			using namespace Bolts::Rendering;
			auto header = (const MeshHeader*)fileData.Head();
			BASSERT(header->versionNo == MeshHeader::c_latestVersion);
			if (header->versionNo != MeshHeader::c_latestVersion) {
				BOLTS_LERROR() << BOLTS_LOG_SCOPE << "Invalid mesh version. Expected " << MeshHeader::c_latestVersion << " got " << header->versionNo;
				return;
			}

			// nodes
			mesh->aabb = header->sceneExtents;
			mesh->nodeData.Store(header->nodes->Data(), (unsigned)header->nodes->Size());
			mesh->nodes = (MeshNodes*)mesh->nodeData.Data();
			for (auto it = mesh->nodes->names->begin(); it != mesh->nodes->names->end(); ++it) {
				RegisterStringHash(mesh->nodes->nameHashes->Get(it.Index()), *it);
			}
			// mats
			auto& fileMats = *(header->materials);
			mesh->materials.resize(fileMats.names->Count());
			for (unsigned i = 0; i < fileMats.names->Count(); i++) {
				auto& mat = mesh->materials[i];
				mat.name = fileMats.names->Get(i);
				mat.nameHash = HashString(mat.name);
			}
			// mesh data
			for (unsigned i = 0; i < header->subMeshes.Count(); i++) {
				const MeshBufferInfo* mbinfo = header->subMeshes[i];
				//
				// Build vert buffer
				VertexLayout desc;
				desc.AddStream({ BGSS_POSITION, BGVT_FLOAT, 3 });
				desc.m_isSOA = false;
				const uint8_t c_posSize = 3 * sizeof(float);
				const uint8_t c_nrmSize = 3 * sizeof(float);
				if (mbinfo->hasNormals) {
					desc.AddStream({ BGSS_NORMALS, BGVT_FLOAT, 3 });
					if (mbinfo->hasTgts) {
						desc.AddStream({ BGSS_TANGENTS, BGVT_FLOAT, 3 });
					}
				}
				for (int k = 0; k < mbinfo->numColors; k++) {
					desc.AddStream({ StreamSemantic(BGSS_COLOR0 + k), BGVT_FLOAT, 4 });
				}
				for (int k = 0; k < mbinfo->numUVs; k += 2) {
					// Merge UVs pairs into vec4s
					uint8_t numChannels = ((k + 1) < mbinfo->numUVs ? 4 : 2);
					desc.AddStream({ StreamSemantic(BGSS_TEXTURE01 + (k / 2)), BGVT_FLOAT, numChannels});
				}
				if (mbinfo->hasBones) {
					desc.AddStream({ BGSS_BONEI, BGVT_I8, 4 });
					desc.AddStream({ BGSS_BONEW, BGVT_FLOAT, 4 });
				}
				//
				Rendering::VertexBufferPtr vbuffer = new Rendering::VertexBuffer();
				{
					dataBuffer_t dataOut = vbuffer->SetData(commands, BUH_STATIC_DRAW, desc, mbinfo->numVerts);
					BASSERT(dataOut.dataSize == mbinfo->vertices->DataSize());
					memcpy(dataOut.data, mbinfo->vertices->First(), dataOut.dataSize);
				}
				// Read index buffer
				Rendering::IndexBufferPtr ibuffer = new Rendering::IndexBuffer();
				{
					dataBuffer_t dataOut = ibuffer->SetData(commands, BUH_STATIC_DRAW, mbinfo->numIndices, mbinfo->getIndexBytes() == 2 ? BGVT_U16 : BGVT_U32);
					BASSERT(dataOut.dataSize == mbinfo->indices->DataSize());
					memcpy(dataOut.data, mbinfo->indices->First(), dataOut.dataSize);
				}
				//
				Rendering::PrimitiveBufferPtr pbuffer = new Rendering::PrimitiveBuffer();
				pbuffer->Build(commands, vbuffer, ibuffer, BGPT_TRIANGLES);

				Mesh::SubMesh subMesh;
				subMesh.name = regHash_t(mbinfo->name->First());
				subMesh.primitiveBuffer = pbuffer;
				subMesh.aabb = mbinfo->meshExtents;
				subMesh.matIdx = mbinfo->materialIdx;
				subMesh.nodeIdx = mbinfo->nodeIdx;
				if (i < mesh->subMeshes.size()) {
					mesh->subMeshes[i] = subMesh;
				}
				else {
					mesh->subMeshes.push_back(subMesh);
				}
			}
		}

		void MeshLoader::OnUnload(assetPtr_t& assetPtr)
		{
			auto pAsset = reinterpret_cast<AssetT*> (assetPtr.detach());
			pAsset->Unload(*m_assetManager.GetCommandBuffer());
			delete pAsset;
		}

	};
};