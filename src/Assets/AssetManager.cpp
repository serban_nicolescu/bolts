#include "Assets/AssetManager.h"
#include "IO/FileProviderImpl.h"

#define BOLTS_LOG_SCOPE	"[AssetManager] "

namespace Bolts {
	namespace assets {
		// Asset Manager

		AssetManager::AssetManager() {
			IO::RegisterSourceChangeCB(
				uint64_t(this),
				[this](IO::fileID_t sid) { NotifySourceChanged(sid); }
			);
		}

		AssetManager::~AssetManager() {
			IO::UnregisterSourceChangeCB(uint64_t(this));

			//manually destroy loaded assets 
			for (int i = 0; i < c_maxAssetTypeId; i++) {
				auto pLoader = m_loaders[i].get();
				for (auto& pair : m_assets[i]) {
					pLoader->OnUnload(pair.second);
				}
			}
		}

		void AssetManager::RegisterAssetToSource(IO::fileID_t sid, int assetType, assetID_t assetID)
		{
			vec_t<reloadData>& assetList = m_reloadData[sid];
			for (const auto& d : assetList)
				if (d.assetID == assetID && d.assetType == assetType)
					return;
			assetList.push_back({ assetType, assetID });
		}

		void AssetManager::NotifySourceChanged(IO::fileID_t sid)
		{
			auto data = m_reloadData.find(sid);
			if (data != m_reloadData.end()) {
				for (reloadData& rd : data->second)
					m_loaders[rd.assetType]->OnSourceChanged(sid, rd.assetID);
			}
		}
	};
};