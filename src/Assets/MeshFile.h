#pragma once
#include <bolts_core.h>
#include <bolts_binaryserialization.h>

namespace Bolts 
{
	struct MeshHeader;
	struct MeshNodes;
	struct MeshBufferInfo;

	template<>
	struct is_pod< MeshHeader> : public std::true_type {};
	template<>
	struct is_pod< MeshNodes> : public std::true_type {};
	template<>
	struct is_pod< MeshBufferInfo> : public std::true_type {};

	struct MeshNodes
	{
		OffsetPtr< InplaceArray<mat4>>		transforms;
		OffsetPtr< InplaceArray<uint32_t>>	parents;
		OffsetPtr< InplaceArray<hash32_t>>	nameHashes;
		OffsetPtr< InplaceOffsetArray<char>> names;

		const char* Data() const { return (const char*)&transforms; }
		size_t		Size() const { return (const char*)names->DataEnd() - Data(); }
	};

	struct MeshMaterials
	{
		OffsetPtr< InplaceOffsetArray<char>> names;
	};

	struct MeshHeader
	{
		static const int c_latestVersion = 12;

		uint32_t					versionNo = c_latestVersion;
		AABB						sceneExtents; // Scene extents, in world-space
		OffsetPtr<MeshNodes>		nodes;
		OffsetPtr<MeshMaterials>	materials;
		InplaceOffsetArray<MeshBufferInfo> subMeshes;
	};

	struct MeshBufferInfo
	{
		AABB meshExtents; // Mesh vertex extents, in model-space
		uint32_t numVerts = 0;
		uint32_t numIndices = 0; // Indices are 16bit if size is <65K, 32bit otherwise
		uint32_t nodeIdx = 0; // index into node list of owning node
		uint32_t materialIdx = 0; // index into materials list
		//uint32_t sizeOfVertexBuffer;
		unsigned char hasNormals = false;
		unsigned char hasTgts = false;
		unsigned char hasBones = false;
		unsigned char numColors;
		unsigned char numUVs;
		OffsetPtr< InplaceArray<char>> vertices;
		OffsetPtr< InplaceArray<char>> indices;
		OffsetPtr< InplaceArray<char>> name;

		uint8_t getIndexBytes() const { return (numIndices <= uint16_t(-1) ? 2 : 4); }
	};

};