#pragma once

#include <bolts_core.h>

namespace Bolts {
	namespace IO {

		enum FileFlags { EF_NONE = 0, EF_TEXT = 1, EF_APPEND = 2};

		size_t	readFile( const char* filepath, void* buffer, size_t bufferSize, FileFlags flags = EF_NONE);///> returns the number of characters read.
		size_t	fileSize( const char* filepath);
		bool	writeFile(const char* filepath, const void* buffer, size_t bufferSize, FileFlags flags = EF_NONE);
		const char* filenameFromPath(const char* filepath, int len);
		const char* extensionFromPath(const char* filepath, int len);
	};
};