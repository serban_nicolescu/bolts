#include "IO/FileProviderImpl.h"
#include <bolts_helpers.h>
#include <bolts_log.h>
#include <bolts_assert.h>
#include <bolts_hash.h>

#include <memory>
#include <thread>
#include <atomic>
#include <mutex>
#include <chrono>
#include <functional>

#include <windows.h>
#include <tchar.h> 
#include <stdio.h>
#include <strsafe.h>

#include "atlstr.h"

namespace Bolts {
	namespace IO
	{
		class FileWatcher;
		static void FileWatchLoop(FileWatcher* userdata);
		static HANDLE FileWatchThreadHandle = 0;

		class FileWatcher
		{
		public:
			struct filePath {
				fileID_t filenameHash;
				char fullpath[MAX_PATH];

				bool operator==(hash32_t h) const {
					return filenameHash == h;
				}
			};

			FileWatcher(const TCHAR* path)
			{
				m_watchedPath = path;
				m_watcher = std::thread(FileWatchLoop, this);
				FileWatchThreadHandle = (HANDLE)m_watcher.native_handle();
				m_newSources.reserve(20);
			}
			~FileWatcher()
			{
				m_isRunning = false;
				delete[] m_watchedPath;

				CancelSynchronousIo(FileWatchThreadHandle);//cancel the blocking synchronous ReadDirectoryChangesW
				m_watcher.join(); 
			}

			bool TryLock()
			{
				if (m_hasChanges) {
					m_queueMtx.lock();
					if (std::chrono::steady_clock::now() - m_lastChangedTime < std::chrono::milliseconds(200)) {
						m_queueMtx.unlock();
						return false;
					}

					return true;
				}
				return false;
			}

			void Unlock()
			{
				m_changedSources.clear();
				m_newSources.clear();
				m_deletedSources.clear();
				m_hasChanges = false;

				m_queueMtx.unlock();
			}
			
			void AddTo(vec_t<filePath>& vec, regHash_t filenameHash, const char* path, int pathlen)
			{
				BASSERT(path);
				BASSERT(pathlen < MAX_PATH);
				if (pathlen >= MAX_PATH)
					return;
				std::lock_guard<std::mutex> l(m_queueMtx);

				if (contains(vec, filenameHash.h))
					return;

				m_lastChangedTime = std::chrono::steady_clock::now();
				m_hasChanges = true;
				
				vec.push_back({});
				auto& newFile = vec.back();
				newFile.filenameHash = filenameHash;
				memcpy_s(newFile.fullpath, MAX_PATH, path, pathlen);
			}

			typedef std::chrono::steady_clock::time_point timePoint_t;
			std::thread			m_watcher;
			std::atomic<bool>	m_isRunning = true;

			std::mutex			m_queueMtx;
			timePoint_t			m_lastChangedTime;
			const TCHAR*		m_watchedPath;

			std::atomic<bool>	m_hasChanges = false;
			
			vec_t<filePath>		m_changedSources;
			vec_t<filePath>		m_newSources;
			vec_t<filePath>		m_deletedSources;
		};

		static void FileWatchLoop(FileWatcher* watcher)
		{
			HANDLE           hDir = NULL;
			hDir = CreateFile(watcher->m_watchedPath, FILE_LIST_DIRECTORY, FILE_SHARE_DELETE | FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS, NULL);
			if (hDir == INVALID_HANDLE_VALUE)
			{
				int err = GetLastError();
				BOLTS_LERROR() << "Error creating file watch thread. Error code: " << err;
				return;
			}

			while (watcher->m_isRunning) {
				TCHAR szPath[MAX_PATH];
				DWORD retLen = 0;
				if (ReadDirectoryChangesW(hDir, szPath, MAX_PATH - 20, true, FILE_NOTIFY_CHANGE_SIZE | FILE_NOTIFY_CHANGE_LAST_WRITE | FILE_NOTIFY_CHANGE_FILE_NAME, &retLen, NULL, NULL)) {
					if (!watcher->m_isRunning)
						return;
					// Get the filename that was changed
					char fpath[MAX_PATH];
					_FILE_NOTIFY_INFORMATION* fInfo = reinterpret_cast<_FILE_NOTIFY_INFORMATION*>(szPath);
					wcstombs(fpath, fInfo->FileName, MAX_PATH - 20);
					static_assert(sizeof(WCHAR) == 2,"");
					int pathLen = fInfo->FileNameLength / sizeof(WCHAR);
					fpath[pathLen] = 0;
					char* filename = fpath + pathLen;
					while (filename > fpath && (filename[-1] != '\\'))
						filename--;
					auto filenameHash = regHash_t(filename);
					//
					if (fInfo->Action == FILE_ACTION_MODIFIED)
						watcher->AddTo(watcher->m_changedSources, filenameHash, fpath, pathLen);
					if (fInfo->Action == FILE_ACTION_ADDED)
						watcher->AddTo(watcher->m_newSources, filenameHash, fpath, pathLen);
					if (fInfo->Action == FILE_ACTION_REMOVED)
						watcher->AddTo(watcher->m_deletedSources, filenameHash, fpath, pathLen);
					//TODO: handle renames
				}
				//NOTE: Sleeping here reduces duplicate change events
				std::this_thread::sleep_for(std::chrono::milliseconds(200));
			}
		}

		///////////////////

		template< class Callback>
		bool ListFilesIn(const TCHAR* wdirpath, Callback& cb)
		{
			WIN32_FIND_DATA ffd;
			LARGE_INTEGER filesize;
			TCHAR szDir[MAX_PATH];
			TCHAR fullPath[MAX_PATH];
			size_t length_of_arg;
			HANDLE hFind = INVALID_HANDLE_VALUE;
			DWORD dwError = 0;

			// Check that the input path plus 3 is not longer than MAX_PATH.
			// Three characters are for the "\*" plus NULL appended below.
			StringCchLength(wdirpath, MAX_PATH, &length_of_arg);
			// Prepare string for use with FindFile functions.  First, copy the
			// string to a buffer, then append '\*' to the directory name.
			StringCchCopy(szDir, MAX_PATH, wdirpath);
			StringCchCat(szDir, MAX_PATH, TEXT("\\*"));
			// Find the first file in the directory.
			hFind = FindFirstFile(szDir, &ffd);
			if (INVALID_HANDLE_VALUE == hFind) {
				return false;
			}

			// List all the files in the directory with some info about them.
			do {
				if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
					if (StrCmp(ffd.cFileName, ".") != 0 &&
						StrCmp(ffd.cFileName, "..") != 0) {

						//_tprintf( TEXT( "  %s   <DIR>\n" ), ffd.cFileName );

						StringCchCopy(fullPath, MAX_PATH, wdirpath);
						StringCchCat(fullPath, MAX_PATH, TEXT("\\"));
						StringCchCat(fullPath, MAX_PATH, ffd.cFileName);

						ListFilesIn(fullPath, cb);
					}
				}
				else {
					filesize.LowPart = ffd.nFileSizeLow;
					filesize.HighPart = ffd.nFileSizeHigh;
					//_tprintf( TEXT( "  %s   %ld bytes\n" ), ffd.cFileName, filesize.QuadPart );

					char fpath[MAX_PATH];
					//wcstombs(fpath, wdirpath, MAX_PATH);
					strcpy_s(fpath, MAX_PATH, wdirpath);
					fpath[length_of_arg] = '\\';
					//wcstombs(fpath + length_of_arg + 1, ffd.cFileName, MAX_PATH - length_of_arg - 1);
					strcpy_s(fpath + length_of_arg + 1, MAX_PATH - length_of_arg - 1, ffd.cFileName);

					cb(Bolts::HashString(fpath + length_of_arg + 1), fpath);
				}
			} while (FindNextFile(hFind, &ffd) != 0);

			dwError = GetLastError();
			if (dwError != ERROR_NO_MORE_FILES) {
				//TODO: Look up possible errors, and log them
			}

			FindClose(hFind);
			return true;
		}

		FileSystemSource::FileSystemSource()
		{
		}

		FileSystemSource::~FileSystemSource()
		{
			if (m_impl) {
				delete static_cast<FileWatcher*>(m_impl);
		}
		}

		void FileSystemSource::RegisterDir(const char* dirpath)
		{
			TCHAR* wdirpath = new TCHAR[MAX_PATH];
#ifdef UNICODE
			// Convert char* string to a wchar_t* string.
			int dirpathlen = strlen(dirpath) + 1;
			size_t convertedChars = 0;
			mbstowcs_s(&convertedChars, wdirpath, dirpathlen, dirpath, _TRUNCATE);
#else
			strcpy(wdirpath, dirpath);
#endif
			auto cb = [this](stringHash_t fname, const char* path) {
				AddFilepath(fname, path);
			};
			ListFilesIn(wdirpath, cb);

			BASSERT(!m_impl);
			m_impl = new FileWatcher(wdirpath);
		}

		void FileSystemSource::CheckForFileChanges()
		{
			m_changedFilesCache.clear();
			if (m_impl) {
				auto watcher = static_cast<FileWatcher*>(m_impl);
				if (watcher->TryLock()) {
					BOLTS_LINFO() << "[Filesystem] Change detected";

					//TODO: We could add another callback, for unloading assets
					// deleted files
					auto& deletedSources = watcher->m_deletedSources;
					for (auto& path : deletedSources) {
						m_paths.erase(path.filenameHash);
					}
					// added files
					auto& newSources = watcher->m_newSources;
					for (auto& path : newSources) {
						AddFilepath(path.filenameHash, path.fullpath);
					}
					// remember changed files
					auto& changedSources = watcher->m_changedSources;
					for (auto& path : changedSources)
						m_changedFilesCache.push_back(path.filenameHash);

					watcher->Unlock();
				}

			}
		}

	}
};