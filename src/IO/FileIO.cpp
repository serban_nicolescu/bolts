#include "IO/FileIO.h"
#include <bolts_log.h>
#include <bolts_assert.h>
#include <fstream>
#include <iostream>

size_t Bolts::IO::readFile(const char* filepath, void* buffer, size_t bufferSize, FileFlags flags)
{
	const char* mode = "rb";
	if (flags & EF_TEXT)
		mode = "r";
	FILE* fp = fopen(filepath, mode);
	if (!fp) {
		BOLTS_LERROR() << "Could not open file for reading: " << filepath;
		return false;
	}

	size_t readBytes = fread_s(buffer, bufferSize, 1, bufferSize, fp);
	BASSERT_ONLY(auto c = )fclose(fp);
	BASSERT(c == 0);
	return readBytes;
}

size_t Bolts::IO::fileSize( const char* filename)
{
	std::ifstream in( filename, std::ifstream::in | std::ifstream::binary );
	if ( in.fail() ) {
		BOLTS_LERROR() << "Could not open file for reading: " << filename;
		return 0;
	}
	in.seekg( 0, std::ifstream::end );
	return (size_t) in.tellg();
}

bool Bolts::IO::writeFile(const char * filepath,const void* buffer, size_t bufferSize, FileFlags flags)
{
	const char* mode = "wb";
	if (flags & EF_APPEND)
		mode = "ab";
	if (flags & EF_TEXT)
		if (flags & EF_APPEND)
			mode = "at";
		else
			mode = "wt";
	FILE* fp = fopen(filepath, mode);
	if (!fp) {
		BOLTS_LERROR() << "Could not open file for writing: " << filepath;
		return false;
	}
	fwrite(buffer, bufferSize, 1, fp);
	return fclose(fp) == 0; // 0 means OK
}

const char* Bolts::IO::filenameFromPath(const char* path, int len)
{
	if (!path)
		return nullptr;
	if (len == 0)
		len = (int)strlen(path);

	const char* r = strrchr(path, '\\');
	if (!r)
		r = strrchr(path, '/');

	if (!r)
		return path;
	if (r - path >= len)
		return nullptr;
	return r + 1;
}

const char* Bolts::IO::extensionFromPath(const char* path, int /*len*/)
{
	if (!path)
		return nullptr;

	const char* r = strrchr(path, '.');
	if (!r)
		return nullptr;
	else 
		return r + 1;
}
