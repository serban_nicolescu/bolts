#include "FileProvider.h"
#include "FileProviderImpl.h"
#include "FileIO.h"
#include "bolts_helpers.h"
#include <bolts_log.h>
#include <bolts_assert.h>
#include <bolts_hash.h>

namespace Bolts {
namespace IO {

	static Stats s_stats{};
	static Stats s_statsMax{};
	//NOTE: We use ptrs because we can't statically init Blobs
		//	Because they need foundation::Init() called
	static Blob* s_empty{};
	static Blob* s_temp{};

	//TODO: We need to allocate blobs individually so they don't move around in memory
	//	Use a stable vector or at least a pool allocator for the ptrs
	typedef std::unique_ptr<Blob> blobPtr_t;
	typedef vec_t< blobPtr_t> blobVec_t;
	typedef vec_t< fileID_t> idVec_t;

	static blobVec_t s_foreverBlobList;
	static idVec_t	 s_foreverIdList;
	static blobVec_t s_frameBlobList;
	static idVec_t	 s_frameIdList;

	void Init()
	{
		static Blob f_empty;
		s_empty = &f_empty;
		static Blob f_temp;
		s_temp = &f_temp;

		s_foreverIdList.reserve(500);
		s_foreverBlobList.reserve(500);
		s_frameBlobList.reserve(500);
		s_frameIdList.reserve(500);

		AddReader(&GetFSSource());
		AddReader(&GetMemorySource());
	}

	void FrameEnd()
	{
		//BOLTS_LDEBUG() << "[IO] Frame End";
		s_stats.numForeverBlobs = (unsigned)s_foreverBlobList.size();
		s_stats.numFrameBlobs = (unsigned)s_frameBlobList.size();
		for (auto& b : s_foreverBlobList)
			s_stats.foreverBlobMemory += b->Size();
		for (auto& b : s_frameBlobList)
			s_stats.frameBlobMemory += b->Size();
		s_frameBlobList.clear();
		s_frameIdList.clear();

#define	BLA_SET_MAX(prop) s_statsMax.##prop = Bolts::max(s_stats.##prop, s_statsMax.##prop);
		BLA_SET_MAX(numReads);
		BLA_SET_MAX(numDiskReads);
		BLA_SET_MAX(numFrameBlobs);
		BLA_SET_MAX(frameBlobMemory);
#undef BLA_SET_MAX
		s_stats = {};
	}

	const Stats& GetFrameStats() { return s_stats; }
	const Stats& GetFrameMaxStats() { return s_statsMax; }

	static Blob& KeepForever(fileID_t id)
	{
		s_foreverBlobList.emplace_back( new Blob());
		s_foreverIdList.push_back(id);
		return *(*s_foreverBlobList.rbegin());
	}

	static Blob& KeepFrame(fileID_t id)
	{
		s_frameBlobList.emplace_back( new Blob());
		s_frameIdList.push_back(id);
		return *(*s_frameBlobList.rbegin());
	}

	static vec_t< IReader*> s_readers{};

	void AddReader(IReader* reader)
	{
		s_readers.push_back(reader);
	}

	template<class Vec, class Val>
	int findIdx(const Vec& vec, const Val& value)
	{
		for (size_t i = 0; i < vec.size(); i++)
			if (vec[i] == value)
				return (int)i;
		return -1;
	}

	Blob::Reader ReadAll(fileID_t fileID, BlobLifetime lifetimeHint)
	{
		s_stats.numReads++;
		s_temp->Clear();

		//look for same fileID in Frame and Forever lists
		int i = findIdx(s_frameIdList, fileID);
		if (i != -1)
			return s_frameBlobList[i]->MakeReader();
		i = findIdx(s_foreverIdList, fileID);
		if (i != -1)
			return s_foreverBlobList[i]->MakeReader();

		Blob& optionalOut = *s_temp;
		const Blob* result = nullptr;
		for (auto pReader : s_readers) {
			if (auto pBlob = pReader->ReadAll(fileID, optionalOut)) {
				result = pBlob;
				break;
			}
		}
		//keep data if optional was used. Ugh
		if (result == &optionalOut) {
			if (lifetimeHint == Frame) {
				auto& newBlob = KeepFrame(fileID);
				newBlob = std::move(optionalOut);
				result = &newBlob;
			}
			if (lifetimeHint == Forever) {
				auto& newBlob = KeepForever(fileID);
				newBlob = std::move(optionalOut);
				result = &newBlob;
			}
		}
		if (!result)
			result = s_empty;

		return result->MakeReader();
	}

	//
	struct changeCbEntry {
		uint64_t			sourceName;
		sourceChangedCB_t	cb;
	};

	static vec_t< changeCbEntry> s_callbacksSourceChange;

	void RegisterSourceChangeCB(uint64_t sourceName, sourceChangedCB_t cb)
	{
		for (auto& entry : s_callbacksSourceChange)
			if (entry.sourceName == sourceName) {
				entry.cb = std::move(cb);
				return;
			}
		s_callbacksSourceChange.emplace_back(changeCbEntry{ sourceName, std::move(cb) });
	}

	void UnregisterSourceChangeCB(uint64_t sourceName)
	{
		unsigned i = 0;
		for (auto& entry : s_callbacksSourceChange) {
			if (entry.sourceName == sourceName)
				break;
			i++;
		}
		if (i != s_callbacksSourceChange.size())
		{
			s_callbacksSourceChange.erase(s_callbacksSourceChange.begin() + i);
		}
	}

	// Provider Impl

	// Filesystem
	static FileSystemSource s_fsSource;
	FileSystemSource& GetFSSource()
	{
		return s_fsSource;
	}

	void CheckForSourceChanges()
	{
		s_fsSource.CheckForFileChanges();

		auto& changedFiles = s_fsSource.GetChangedFiles();
		blobVec_t cachedFiles; // we only need this to keep stuff alive until we exit the function

		// remove cached file data, but keep it around so everyone has time to unload
		for (fileID_t file : changedFiles) {
			int i = findIdx(s_frameIdList, file);
			if (i != -1) {
				cachedFiles.emplace_back( std::move(s_frameBlobList[i]));
				s_frameBlobList.erase(s_frameBlobList.begin() + i);
				s_frameIdList.erase(s_frameIdList.begin() + i);
			}
			i = findIdx(s_foreverIdList, file);
			if (i != -1) {
				cachedFiles.emplace_back(std::move(s_foreverBlobList[i]));
				s_foreverBlobList.erase(s_foreverBlobList.begin() + i);
				s_foreverIdList.erase(s_foreverIdList.begin() + i);
			}
		}

		// tell everyone stuff changed
		for (fileID_t file : changedFiles)
			for (auto& entry : s_callbacksSourceChange)
				entry.cb(file);
	}

	const char* FileSystemSource::GetFileOpenPath(fileID_t fname)
	{
		auto it = m_paths.find(fname);
		if (it == m_paths.end()) {
			return nullptr;
		}
		return it->second.c_str();
	}

	fileID_t FileSystemSource::FindSourceID(hash32_t fileName, hash32_t ext)
	{
		const fileID_t* pSource = try_find(m_fileSources, fileName ^ ext);
		if (pSource)
			return *pSource;
		else
			return 0;
	}

	void FileSystemSource::AddFilepath(fileID_t fname, const char* path)
	{
		if (m_paths.find(fname) != m_paths.end()) {
			BOLTS_LERROR() << "Duplicate filename found in filesystem!" << "Original string is :" << m_paths[fname] << " . Duplicate path is :" << path;
			BASSERT("Duplicate filename found in filesystem !");
		}
		m_paths[fname] = path;

		const char* filename = IO::filenameFromPath(path, 0);
		BASSERT(filename);
		const char* ext = IO::extensionFromPath(filename, 0);
		if (!ext)
			return;
		hash32_t extHash = HashString(ext);
		hash32_t filenameExtCombo = HashString(filename) ^ extHash;
		BASSERT(try_find(m_fileSources, filenameExtCombo) == nullptr);
		m_fileSources[filenameExtCombo] = fname;
	}

	const Blob* FileSystemSource::ReadAll(fileID_t fileID, Blob& out)
	{
		//TODO: Very important. We don't cache file reads at all
		//	Should at least check the Frame data list to see if this ID was used before
		auto it = m_paths.find(fileID);
		if (it != m_paths.end()) { // Missing file is not an error, other providers might have it
			if (!ReadFile(it->second.c_str(), out)) {
				BOLTS_LERROR() << "[Filesystem] " << "Error opening file: " << it->second.c_str();
				BASSERT_MSG(false, "Could not open file");
			}
			else {
				return &out;
			}
		}
		return nullptr;
	}

	bool FileSystemSource::ReadFile(const char* path, Blob& out)
	{
		s_stats.numDiskReads++;

		unsigned fileSize = (unsigned)Bolts::IO::fileSize(path);
		if (fileSize) {
			out.SetCapacity(fileSize);
			BASSERT_ONLY(auto readBytes = ) Bolts::IO::readFile(path, out.WillStore(fileSize), (size_t)fileSize);
			BASSERT(readBytes == fileSize);
			return true;
		}
		out.Clear();
		return false;
	}

	// Memory Source

	static MemorySource s_memSource;
	MemorySource& GetMemorySource()
	{
		return s_memSource;
	}

	bool MemorySource::HasFileData(fileID_t fileID) const
	{
		return m_sourceCache.count(fileID) != 0;
	}

	void MemorySource::AddSourceData(fileID_t newID, Blob&& data)
	{
		BASSERT_MSG(m_sourceCache.count(newID) == 0, "Hash collision when registering asset source data, or same source registered twice");
		sourcePtr_t newEntry{ new Blob(std::move(data)) };
		m_sourceCache.insert_or_assign(newID, std::move(newEntry));
	}

	const Blob* MemorySource::ReadAll(fileID_t fileID, Blob& /*outOptional*/)
	{
		auto it = m_sourceCache.find(fileID);
		if (it != m_sourceCache.end()) {
			return it->second.get();
		}
		return nullptr; // Nothing was found
	}

};
};