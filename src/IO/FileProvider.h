#pragma once 
#include <bolts_core.h>
#include <bolts_binaryserialization.h>
#include <functional>

namespace Bolts
{
	namespace IO {
		typedef hash32_t	fileID_t; // unique identifier for a disk/network/memory file

		void		Init();
		void		FrameEnd(); // free memory used for reading and update stats

		//inline sourceID_t	MakeSourceID(const char* sourceName, size_t sourceNameLen) { return Bolts::HashString(sourceName, sourceNameLen); }
		//inline sourceID_t	MakeSourceID(const char* sourceName) { return Bolts::HashString(sourceName); }
		//inline sourceID_t	MakeSourceID(const std::string& sourceName) { return Bolts::HashString(sourceName); }

		//TODO: Pass memory allocator and drop this Lifetime thing
		enum BlobLifetime {
			Temp = 1, // keep alive until next Read() call
			Frame = 2, // keep alive until next FrameStart
			Forever = 4 // keep alive forever
			//TODO: We need a refcounted version for medium-sized asset files
		};

		Blob::Reader	ReadAll(fileID_t, BlobLifetime);

		// Async API WIP
		/*
		enum AsyncResult {
			DONE = 0,
			WAIT = 1,
			NOT_FOUND = 2
		};
		virtual bool TryGetAsync(sourceID_t sourceID, AsyncResult* result); //> Returns true when data is available and can be Get()
		*/

		//

		typedef std::function<void(fileID_t)> sourceChangedCB_t;
		void	RegisterSourceChangeCB(uint64_t sourceName, sourceChangedCB_t cb);
		void	UnregisterSourceChangeCB(uint64_t sourceName);
		void	CheckForSourceChanges();

		class FileSystemSource;
		class MemorySource;

		FileSystemSource&	GetFSSource();
		MemorySource&		GetMemorySource();

		struct Stats {
			uint64_t foreverBlobMemory = 0;
			uint64_t frameBlobMemory = 0;
			unsigned numForeverBlobs = 0;
			unsigned numFrameBlobs = 0;
			unsigned numReads = 0;
			unsigned numDiskReads = 0;
		};

		const Stats& GetFrameStats();
		const Stats& GetFrameMaxStats();
	};
};