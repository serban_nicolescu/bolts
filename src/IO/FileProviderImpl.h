#pragma once 
#include "FileProvider.h"

namespace Bolts
{
	namespace IO {
		/// Interface for classes providing access to file data
		class IReader
		{
		public:
			//> NOTE: The "out" parameter is optional. Only there to reduce memory allocations
			//	Return nullptr if you don't know this file
			virtual const Blob* ReadAll(fileID_t fileID, Blob& outOptional) = 0;
		};
		void AddReader(IReader*);

		//
		class FileSystemSource :public IReader
		{
		public:
			FileSystemSource();
			~FileSystemSource();

			const Blob* ReadAll(fileID_t fileID, Blob& out) override;
			//
			void		RegisterDir(const char* dirpath);
			fileID_t	FindSourceID(hash32_t fileName, hash32_t ext); ///> returns a sourceID, based on separate file name and ext hashes. useful for assets
			const char*	GetFileOpenPath(fileID_t filename); ///> Fetch path to given file name hash. Use this to open files based on their name hash

			typedef vec_t<fileID_t> fileList_t;
			void				CheckForFileChanges();
			const fileList_t&	GetChangedFiles() { return m_changedFilesCache; }
		private:
			typedef std::unique_ptr<Blob> blobPtr_t;

			void		AddFilepath(fileID_t fname, const char* path);
			bool		ReadFile(const char* path, Blob& outOptional);

			Blob								m_tempReturnBlob{};
			hash_map_t< fileID_t, str_t>		m_paths;
			hash_map_t< hash32_t, fileID_t>		m_fileSources;
			//hash_map_t< hash32_t, vec_t<sourceID_t>>	m_extFiles;
			fileList_t							m_changedFilesCache;
			void*								m_impl = nullptr;
		};

		/// Holds data forever in memory
		class MemorySource : public IReader
		{
		public:
			const Blob* ReadAll(fileID_t fileID, Blob& outOptional) override;//> ID-based fetching. Requires source names being registered. See AddSourceData above

			bool	HasFileData(fileID_t fileID) const;
			void	AddSourceData(fileID_t fileID, Blob&& data); //> Adds an in-memory source with the given name
		private:
			typedef std::unique_ptr<Blob> sourcePtr_t;
			typedef hash_map_t< fileID_t, sourcePtr_t>	dataMap_t;

			dataMap_t		m_sourceCache; //> Holds registred data
		};
	};
}