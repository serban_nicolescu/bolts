#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <set>
#include <stdio.h>
#include <fstream>
#include <assert.h>
#include <cstdarg>
#include "MurmurHash/MurmurHash3.h"

using namespace std;

const char* readFileAsText(const char *filename)
{
	std::ifstream in(filename, std::ifstream::in | std::ifstream::binary);
	if (in.fail()) {
		return nullptr;
	}
	in.seekg(0, std::ifstream::end);
	auto size = (size_t)in.tellg();
	in.seekg(0, std::ifstream::beg);
	char* data = new char[size+1];
	in.read(data, size);
	data[size] = EOF;
	return data;
}

void throwError(const char* location, const char* message, ...)
{
	static char msgBuff[1024];
	va_list argp;
	va_start(argp, message);
	vsprintf_s(msgBuff, message, argp);
	va_end(argp);
	cout << "Error: " << msgBuff << ": " << location;
	exit(1);
}

const char* skipWhite(const char* s)
{
	while (isspace(*s) != 0 && *s != EOF) s++;
	return s;
}

const char* seekChar(const char* s, char c)
{
	while (*s != c && *s != EOF) s++;
	return *s == c ? s : nullptr;
}

const char* skipChars(const char* s)
{
	while (isspace(*s) == 0 && *s != EOF) s++;
	return s;
}

const char* skipLine(const char* s)
{
	auto eol = seekChar(s, '\n');
	if (!eol)
		return nullptr;
	return eol + 1;
}

const char* skipLetters(const char* s)
{
	while (isalpha(*s++) != 0);
	return s - 1;
}

const char* skipNumber(const char* s)
{
	while (isdigit(*s++) != 0);
	return s - 1;
}

struct subs {
	bool is(const char* s)
	{
		return (strlen(s) == (end - start)) 
			&& (strncmp(s, start, end - start) == 0);
	}

	bool startsWith(const char* s)
	{
		return strncmp(s, start, strlen(s)) == 0;
	}

	int toInt()
	{
		return atoi(start);
	}

	string toString() const { return string(start, end); }

	bool valid() {
		return end != start;
	}

	void skip(const char** s)
	{
		//assert(*s == start);
		*s = end;
	}

	const char* start;
	const char* end;
};

subs peekWord(const char* s)
{
	s = skipWhite(s);
	subs r;
	r.start = s;
	r.end = skipLetters(s);
	return r;
}

subs peekNumber(const char* s)
{
	s = skipWhite(s);
	subs r;
	r.start = s;
	r.end = skipNumber(s);
	return r;
}

subs peekChars(const char* s)
{
	s = skipWhite(s);
	subs r;
	r.start = s;
	r.end = skipChars(s);
	return r;
}

bool maybeChar(const char** s, char c)
{
	*s = skipWhite(*s);
	if (**s == c) {
		*s = *s + 1;//skip it
		return true;
	}
	return false;
}

void expectChar(const char** s, char c)
{
	if (!maybeChar(s, c))
		throwError(*s, "expected a %c here", c);
}

struct op
{
	const char* tryParse(const char* s)
	{
		subs op = peekChars(s);
		if (op.is("==")) {
			t = EQ;
		}
		else if (op.is("!=")) {
			t = NEQ;
		}
		else if (op.is("||")) {
			t = OR;
		}
		else if (op.is("&&")) {
			t = AND;
		}
		else
			return nullptr;
		return op.end;
	}

	enum {
		NONE,
		EQ,
		NEQ,
		OR,
		AND
	} t = NONE;
};

struct operand {
	operand() : c(0) {}
	operand(const operand& other) { t = other.t; negated = other.negated; memcpy(&def, &other.def, sizeof(subs)); }

	const char* tryParse(const char* s)
	{
		if (maybeChar(&s, '!'))
			negated = true;

		subs word = peekWord(s);
		subs number = peekNumber(s);
		if (!word.valid() && !number.valid())
			throwError(s, "invalid operand");

		if (number.valid()) {
			t = CONST;
			c = number.toInt();
		}
		else if (word.is("true")) {
			t = CONST;
			c = 1;
			word.skip(&s);
		}
		else if (word.is("false")) {
			t = CONST;
			c = 0;
			word.skip(&s);
		}
		else if (word.is("defined")) {
			word.skip(&s);
			expectChar(&s, '(');
			word = peekWord(s);
			t = DEFINED;
			def = word;
			word.skip(&s);
			expectChar(&s, ')');
		}
		//TODO: parse actual value defines here
		return s;
	}

	enum {
		CONST, // number or true or false
		DEFINED, // defined(DEF)
		//DEFINE // value of DEF 
	} t = CONST;
	union {
		int c;
		subs def;
	};
	bool negated = false;
};

struct expr {
	const char* tryParse(const char* s)
	{
		s = left.tryParse(s);
		auto p = opr.tryParse(s);
		if (p)
		{
			s = right.tryParse(p);
		}
		return s;
	}

	operand left;
	op		opr;
	operand right;
};

struct statement {
	statement():ex() {}
	statement(const statement& other) {
		memcpy(this, &other, sizeof(statement));
	}

	const char* tryParse(const char* s)
	{
		subs word = peekChars(s);
		word.skip(&s);
		if (word.is("#define")) {
			t = DEFINE;
			name = peekWord(s);
			name.skip(&s);
		}
		else if (word.is("#undef")) {
			t = UNDEFINE;
			name = peekWord(s);
			name.skip(&s);
		}
		else if (word.is("#ifdef") || word.is("#ifndef")) {
			t = IF;
			ex.opr.t = op::NONE;
			ex.left.t = operand::DEFINED;
			ex.left.def = peekWord(s);
			ex.left.def.skip(&s);
			ex.left.negated = word.is("#ifndef");
			s = skipLine(s);
		}
		else if (word.is("#if")) {
			t = IF;
			s = ex.tryParse(s);
			s = skipLine(s);
		}
		else if (word.is("#else")) {
			t = ELSE;
			s = skipLine(s);
		}
		else if (word.is("#endif")) {
			t = ENDIF;
			s = skipLine(s);
		}
		else
			return nullptr;

		return s;
	}

	enum {
		IF,
		ELSE,
		ENDIF,
		DEFINE,
		UNDEFINE
	} t;
	union {
		subs name;
		expr ex;
	};
};

struct token
{
	token() {}
	token(statement st) :s(st), t(STATEMENT) {}
	token(subs ss) :cb(ss), t(CODE_BLOCK) {}
	token(const token& other) {
		memcpy(this, &other, sizeof(token));
	}


	enum {
		STATEMENT,
		CODE_BLOCK
	} t;
	union {
		statement	s;
		subs		cb; //code block
	};
};

struct Tokens {
	Tokens() {
		currentCB.start = currentCB.end = nullptr;
	}

	void parse(const char* s)
	{
		while (*s != EOF)
		{
			subs word = peekChars(s);
			if (*word.start == EOF) {
				submitCB();
				return;
			}
			if (word.startsWith("//")) {
				word.skip(&s);
				// line comments
				s = skipLine(s);
				//TODO: keep comments
			}
			else if (word.startsWith("/*")) {
				// block comments
				const char* commentBlockStart = s;
				word.skip(&s);
				int commentStack = 1;
				do {
					s = seekChar(s, '*');
					if (!s)
						throwError(commentBlockStart, "No closing found for comment block start.");

					if (s[-1] == '/') {
						commentStack++;
						s++;
					}
					else if (s[1] == '/') {
						commentStack--;
						s += 2;
					}
				} while (commentStack > 0);
				subs comment;
				comment.start = commentBlockStart;
				comment.end = s;
				//TODO: keep comments
			}
			else {
				statement stmt;
				auto newS = stmt.tryParse(s);
				if (newS) {
					submitCB();
					tokens.emplace_back(stmt);
					s = newS;
				}
				else {
					if (currentCB.start == nullptr)
						currentCB.start = s;
					s = skipLine(s);
					currentCB.end = s;
				}
			}
		}
		submitCB();
	}

	void submitCB() {
		if (currentCB.start != nullptr) {
			tokens.emplace_back(currentCB);
			currentCB.start = nullptr;
		}
	}

	subs currentCB;
	vector<token> tokens;
};

typedef map<string, int> defMap_t;

int opVal(const operand& opr, const defMap_t& defs)
{
	switch (opr.t) {
	case operand::CONST:
		return opr.negated ? !opr.c : opr.c;
	case operand::DEFINED:
		return defs.count(opr.def.toString()) == (opr.negated ? 0 : 1);
	//case operand::DEFINE:
	//	return defs.count(opr.def.toString()) == (opr.negated ? 0 : 1);
	}
	assert(false);
	return 0;
}

bool eval(const expr& e, const defMap_t& defs)
{
	switch (e.opr.t)
	{
	case op::NONE:
		return opVal(e.left, defs) > 0;
	case op::EQ:
		return opVal(e.left, defs) == opVal(e.right, defs);
	case op::NEQ:
		return opVal(e.left, defs) != opVal(e.right, defs);
	case op::OR:
		return opVal(e.left, defs) || opVal(e.right, defs);
	case op::AND:
		return opVal(e.left, defs) && opVal(e.right, defs);
	}
	assert(false);
	return false;
}

size_t preprocess(const Tokens& tok, const vector<string>& defs, char* out)
{
	defMap_t defineValues;
	for (auto& s : defs)
		defineValues.insert({ s, 1 });

	auto os = out;
	vector<bool> skipStack;
	skipStack.push_back(false);

	int currentDepth = 0;
	int skipDepth = -1;
	bool skip = false;
	//TODO: validate that ifs are closed properly
	for (const auto& t : tok.tokens) {
		if (t.t == token::CODE_BLOCK) {
			if (skip)
				continue;
			size_t tocopy = t.cb.end - t.cb.start;
			memcpy(out, t.cb.start, tocopy);
			out += tocopy;
		}
		else {
			const statement& s = t.s;
			switch (s.t) {
			case statement::DEFINE:
				if (skip)
					continue;
				defineValues.insert({s.name.toString(), 1});
				break;
			case statement::UNDEFINE:
				if (skip)
					continue;
				defineValues.erase(s.name.toString());
				break;
			case statement::IF: {
				currentDepth++;
				if (skip)
					continue;
				bool isTrue = eval(s.ex, defineValues);
				if (!isTrue) {
					skip = true;
					skipDepth = currentDepth;
				}
				break;
			}
			case statement::ELSE: {
				if (skip && currentDepth != skipDepth)
					continue;

				skip = !skip;
				if (skip)
					skipDepth = currentDepth;
				break;
			}
			case statement::ENDIF: {
				currentDepth--;
				if (skip) {
					if (currentDepth >= skipDepth)
						continue;
					skipDepth = -1;
					skip = false;
				}
				break;
			}
			}
		}
	}
	return out - os;
}

void getUsedDefines(const Tokens& toks, set<string>& defs)
{
	for (const auto& tok : toks.tokens) {
		if (tok.t == token::STATEMENT && tok.s.t == statement::IF) 
		{
			const auto& expression = tok.s.ex;
			if (expression.left.t ==  operand::DEFINED)
				defs.insert(expression.left.def.toString());
			if (expression.right.t == operand::DEFINED)
				defs.insert(expression.right.def.toString());
		}
	}
}

// hashing

struct hash_t {
	uint64_t h1;
	uint64_t h2;
};


//TODO: Add support for parenteses in expressions
//TODO: Add support for value defines ( and replacing them in code )
int main(int argc, char** argv)
{
	if (argc < 2)
		return 1;

	const char* src = readFileAsText(argv[1]);
	if (!src) {
		cout << "Error reading input file\n";
		return 1; // error reading input
	}


	Tokens tokenizer;
	tokenizer.parse(src);

	set<string> defs;
	getUsedDefines(tokenizer, defs);
	//build ast
	size_t outMaxSize = strlen(src);
	char* out = new char[outMaxSize];
	size_t outSize = preprocess(tokenizer, { "SHADOWS" }, out);
	out[outSize] = 0;
	// write out

	char blablabla[500];
	sprintf_s(blablabla, "%s.out", argv[1]);
	std::ofstream outfile(blablabla, std::ofstream::out | std::ofstream::binary);
	if (outfile.fail()) {
		return 2;
	}
	outfile.write(out, outSize);
	outfile.close();
	int a = 0;

}