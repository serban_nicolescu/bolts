
//Constants and settings
var cGameListenPort = 13;

//External Modules
//var mNet = require('net');
var mJayson = require('jayson');

/*
function connectToGame(){
  if ( gameTCP !== null)
    return;

  gameTCP = net.connect({port: tcpGamePort}, function() { //'connect' listener
    console.log('client connected');
    var str = 'world!\r\n';
    str = str + str + str +str +str +str +str +str +str;
    gameTCP.write(str);
  });
  gameTCP.on('data', function(data) {
    console.log( 'REPLY: ' + data.toString());
    gameTCP.end();
  });
  gameTCP.on('end', function() {
    console.log('client disconnected');
    gameTCP = null;
  });
}*/

function OnRPCClientDisconnected(error)
{
   console.log( "[CRS] Game Disconnected: " + error);
   //mJayson.server().tcp().listen( portNo);
}

function OnNewRPCClientConnected( socket)
{
  console.log( "[CRS] Game Connected");
  socket.setKeepAlive(true);
  socket.on( 'timeout', OnRPCClientDisconnected);
  socket.on( 'error', OnRPCClientDisconnected)
}

// ***************  External functions ******************************//

//Server
var RPCServer = null;

function EnableRPCServer( portNo)
{
    RPCServer = mJayson.server();
    var TCPServer = RPCServer.tcp();
    TCPServer.on( 'connection', OnNewRPCClientConnected);
    RPCServer.on( 'response', function(req, resp)
    {
      console.log( '[CRS] Response sent [' + req.id + '] '+ JSON.stringify(resp, null, 4));
    });
    RPCServer.on( 'request', function( request)
    {
      console.log( '[CRS] Request Received: ' + JSON.stringify(request, null, 4));
    });
    TCPServer.listen( portNo);
}

function AddMethod( name, method)
{
    if (RPCServer === null) 
        throw "Controller RPC Server ERROR: Please call StartServer( listenPort) first";
    RPCServer.method(name, method);
    console.log( "[CRS] Added method: " + name);
}

//Game-Side functions
//var tcpRPCclient = null;

//NOTE: callback = function(err, error, response)
function CallGameFunction( functionName, paramArray, callback)
{
  console.log("[CTRL] Calling Game: " + functionName);
  var tcpRPCclient = mJayson.client.tcp({
    port: cGameListenPort,
    hostname: 'localhost'
  });
  // invoke function
  //tcpRPCclient.request('PrintFloat', [23.54],null,callback);
  tcpRPCclient.request( functionName, paramArray, null,callback);
}

exports.StartServer = EnableRPCServer;
exports.AddMethod = AddMethod;
exports.CallGameFunction = CallGameFunction;