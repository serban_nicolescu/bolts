
Controller
	Easy-to-use API
		Small footprint
		Fast-to-compile
	Is a TCP Server for the game
		Can only have one game connected at a time
	Is a HTTP Server for the views
		Can have multiple web "views" that inspect and change the data
		
Game
	Can only have one controller connected at a time
	On New Connection sends JSON config ( See CCJSON below)
		Contains registered vars with names and values
	And registers RPC functions
		Implicit VarUpdate functions
		User-Registered functions
		
	Can only tweak: bool, int, unsigned int, float, vec3, string
	Game registers a tweaker name, initial value and callback function (called on controller Update)
		With optional different display name
	Stores these.
	
	When a new connection is made, it sends the JSON
	When a new tweaker is added, it sends the JSON

Messages
	From Controller to Game
		Tweaker Updates
		Function calls
	From Game to Controller
		CCJSON ( On Connect)
		Tweaker updates
		Triggers updates

TODO:
	Game
		Serialize tweakers to JSON
		Generate CCJSON
		Send CCJSON
		Send New Tweakers
		Send Tweaker updates
	Controller
		Listen for CCJSON
		Listen for Tweaker changes
		Listen for Tweaker updates
	
CCJSON - Controller Config JSON
	{
		tweaks:[
			{tweak} - 
			name - displayed name
			varName - variable name ( is a stringified string Hash)
			varType - number representing clientside variable type
			category - "namespace", strings separated by dots ( vehicles.sensitivity, players.p1.maxSpeed, etc. )
			value - bool, int, unsigned, float, vec3, string TODO
				vec3 is array of doubles
			minValue
			maxValue
			autorefresh - send to client automatically when value changes, or on demand ( display a button)
			],
		triggers:[
			{trigger} - Just a button used to call a user-defined function
			name - Displayed name
			category - Same as tweak.category
			functionName - Game-side function name
			]
	}
			