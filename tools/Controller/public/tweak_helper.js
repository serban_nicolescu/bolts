window.onload = function() {
    var controllerIp = $('#controllerIp').val();
    var controllerPort = $('#controllerPort').val();
    var socket = io.connect('http://' + controllerIp + ':' + controllerPort);
    var tweakers = document.getElementById("tweakers");
 
    var TWEAKER_DIV_BASE_NAME = 'tweaker-';
    var INPUT_ELEM_BASE_NAME = 'tweaker-input-';

    //Called when the user changes the value of one of the tweaks
    function OnTweakValueChanged( name, value)
    {
        console.log( "Tweak[ " + name + " ] - " + value);

        socket.emit( 'onTweakValueChanged', { 'name': name, 'value': value});
    }

    function GetTweakUIElement( varName)
    {
        var tweakerID = '#' + INPUT_ELEM_BASE_NAME + varName;
        return $(tweakerID);
    }
    function GetTweakContainer( varName)
    {
        var tweakerID = '#' + TWEAKER_DIV_BASE_NAME + varName;
        return $(tweakerID);
    }
    function GetTweakNameFromContainer( tweakContainer)
    {
        return tweakContainer.attr('id').slice( TWEAKER_DIV_BASE_NAME.length);
    }

    //On slider value changed
    $(tweakers).on("slide",".float-tweak", 
        function( event, ui ) {
            //Display the value
            var sliderParent = $(event.target).parent();
            sliderParent.find("input").val( ui.value);
        });
    $(tweakers).on("slidestop",".float-tweak", 
        function( event, ui ) {
            var varName = $(event.target).attr('id').slice( INPUT_ELEM_BASE_NAME.length);
            //Notify the server of the change
            //var varName = sliderParent.children().text();//The the label has the variable name as a first child
            OnTweakValueChanged( varName, ui.value);
        });

    $(tweakers).on("click",".bool-tweak",
         function( event) {
            var varName = $(event.target).attr('id').slice( INPUT_ELEM_BASE_NAME.length);
            var currentValue = $(this).prop("checked");
            OnTweakValueChanged( varName, currentValue);
            //$(this).val(currentValue);
            //TODO: Send value to Controller
        });

    function InitTweakerUI()
    {
        $(tweakers).children().each(
            function(){
                InitialiseTweaker( $(this));
            }
        );

        $(".bool-tweak").button();
    }

    //Initialises the client-side JS elements for a specific variable( i.e. enables sliders, etc.)
    function InitialiseTweaker( tweakContainer){
        var varInitialValue = tweakContainer.find("input").val();
        tweakContainer.find(".float-tweak").slider({
                range: "min", //temp
                value: varInitialValue,
                min: 0.0, //temp
                max: 100.0 // temp
        });
    }

    function OnNewVariableAdded( varData){
        //Add new var content ( html markup )
        tweakers.innerHTML += varData.content;
        //Initialise client-side elements
        InitialiseTweaker( varData.name);
    }

    function UpdateSlider( varData){
        var sliderID = TWEAKER_DIV_BASE_NAME + varData.name + '_sld';
        $('#' + sliderID).slider( 'value', varData.value);
    }

    //socket.on('newTweaker', addNewTweaker);
    //socket.on('updateVariable', OnVarChanged);

    //Temporary tweaker add
    //addNewTweaker( { name:"Test", value:65 });
    InitTweakerUI();
}