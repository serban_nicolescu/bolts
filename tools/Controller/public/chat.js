window.onload = function() {
 
    var messages = [];
    var socket = io.connect('http://localhost:3700');
    var field = document.getElementById("field");
    var sendButton = document.getElementById("send");
    var content = document.getElementById("content");
    var tweakers = document.getElementById("tweakers");
    var name = document.getElementById("name");
 
    var TWEAKER_DIV_BASE_NAME = 'tweaker-';

    function UpdateServersideVariable( name, value)
    {
        socket.emit( 'updateVariableValue', { 'name': name, 'value': value});
    }

    function GetVarTweakerElement( varName)
    {
        var tweakerID = '#' + TWEAKER_DIV_BASE_NAME + varName;
        return $(tweakerID);
    }

    //On slider value changed
    $(tweakers).on("slide",".range-slider", 
        function( event, ui ) {
            //Display the value
            var sliderParent = $(event.target).parent();
            sliderParent.find("input").val( ui.value);
        });
    $(tweakers).on("slidestop",".range-slider", 
        function( event, ui ) {
            var varName = $(event.target).attr('id');
            varName = varName.slice( TWEAKER_DIV_BASE_NAME.length);
            //Notify the server of the change
            //var varName = sliderParent.children().text();//The the label has the variable name as a first child
            UpdateServersideVariable( varName, ui.value);
        });

    function TweakerSetup()
    {
        $(tweakers).children().each(
            function(){
                var varName = $(this).children().text();
                InitialiseTweaker( varName);
            }
        );
    }

    //Initialises the client-side JS elements for a specific variable( i.e. enables sliders, etc.)
    function InitialiseTweaker( varName){
        var tweakerElement = GetVarTweakerElement( varName);
        var varInitialValue = tweakerElement.parent().find("input").val();
        //TODO: Select tweaker type based on var type or param or elem class
        tweakerElement.slider({
                range: "min", //temp
                value: varInitialValue,
                min: 1, //temp
                max: 700 // temp
        });
    }

    function OnNewVariableAdded( varData){
        //Add new var content ( html markup )
        tweakers.innerHTML += varData.content;
        //Initialise client-side elements
        InitialiseTweaker( varData.name);
    }

    function OnVarChanged( varData){
        //Dispatch to the handler
        //TODO: Select tweaker type based on var type or param
        UpdateSlider( varData);
    }

    function UpdateSlider( varData){
        var sliderID = TWEAKER_DIV_BASE_NAME + varData.name;
        $('#' + sliderID).slider( 'value', varData.value);
    }

    addNewTweaker = function (data) {
        console.log("New Tweaker: ", data);
        if(data) {
            var newVar = data;
            var newTweakerId = TWEAKER_DIV_BASE_NAME + newVar.name;
            var newContent = '<div id="' + newTweakerId + '">';
            //First add the new HTML elements to the DOM
            newContent += '<p> \
                    <label>Value: \
                        <input type="text" readonly style="border:0; color:#f6931f; font-weight:bold;"> \
                        <div class="range-slider"></div> \
                    </label> \
                </p>';

            newContent += '</div>';
            tweakers.innerHTML += newContent;
            //Next add jQuery functionality to them
            $('#' + newTweakerId + ' .range-slider').slider({
                range: "min",
                value: newVar.value,
                min: 1,
                max: 700
            });
            $( '#' + newTweakerId + ' input').val( newVar.value);
        } else {
            console.log("There is a problem:", data);
        }
    };

    socket.on('message', function (data) {
        if(data.message) {
            messages.push(data);
            var html = '';
            for(var i=0; i<messages.length; i++) {
                html += '<b>' + (messages[i].username ? messages[i].username : 'Server') + ': </b>';
                html += messages[i].message + '<br />';
            }
            content.innerHTML = html;
        } else {
            console.log("There is a problem:", data);
        }
    });

    socket.on('newTweaker', addNewTweaker);
    socket.on('updateVariable', OnVarChanged);
 
    sendButton.onclick = function() {
        if(name.value == "") {
            alert("Please type your name!");
        } else {
            var text = field.value;
            socket.emit('send', { message: text, username: name.value });
        }
    };

    //Temporary tweaker add
    //addNewTweaker( { name:"Test", value:65 });
    TweakerSetup();
}