var controllerTCP = null; // The TCP connection to BController

/*
controllerTCP.CallGameFunction( "PrintFloat", [ 12.34], function(err, error, response){
  if (err) throw err;
  if ( error)
    console.log("[CRG] ERROR calling function: ", error);    
  console.log("Called game func");
});
*/

//********** Const values ***************//

//Names of the BController RPC functions to update tweaker values
//    There is a function for each type
var C_TWEAK_UPDATE_FUNC_NAMES = {
  0:"OnBoolChanged",
  1:"OnFloatChanged"
};

//***************************************//

var l_ccjson = { tweaks:{
    "hcbool1":{
      "name":'Hardcoded Bool 1',
      "varName":'hcbool1',
      "varType":0,
      "value":1
    },
    "hcfloat1":{
      "name":'Hardcoded Float 1',
      "varName":'hcfloat1',
      "varType":1,
      "value":54.8
    }
    } };


function RPCUpdateTweakValue(err, error, wasOK)
{
  //if (err) throw err; //For some reason rethrowing doesn't work
  if ( err || error){
    console.log("[CTRL] ERROR updating Game tweak value: ");    
    console.log( error);
    return;
  }

  if (wasOK)
    console.log( "[CTRL] Updated Game tweak value");
  else
    console.log( "[CTRL] ERROR updating Game tweak value");
}

function RPCSetCCJSON(err, error, newccjson)
{
  //if (err) throw err; //For some reason rethrowing doesn't work
  if ( err || error){
    console.log("[CTRL] ERROR getting CCJSON from game");    
    return;
  }

  console.log( "[CTRL] Game CCJSON: ");
  ccjson = eval( '(' + newccjson + ')');

  ccjsonTweaks = {};
  for (var i = ccjson.tweaks.length - 1; i >= 0; i--) {
    var tweak = ccjson.tweaks[i];
    ccjsonTweaks[ tweak.name] = tweak;
  };
  ccjson.tweaks = ccjsonTweaks;
  console.log( ccjson);

  l_ccjson = ccjson;
  //ccjson.tweaks.forEach( AddTweak);
}

function RPCRegisterNewTweak( tweakData, replyCallback)
{
  console.log( "[CTRL] Game registered new tweak");
  AddTweak( eval( '(' + tweakData + ')') );
}

function AddTweak( tweakData)
{
  l_ccjson.tweaks.push( tweakData);
  //TODO: Send tweak data to webclients
}

function OnWebUpdateTweakValue( data)
{
  //Send new value straight to the game
  console.log('[CTRL] ~ ' + data.name + ' = ' + data.value);
  var tweakData = l_ccjson.tweaks[ data.name];
  if ( tweakData === undefined){
    console.log('[CTRL] ERROR: Cannot find tweak data for: ' + data.name);
    console.log( l_ccjson.tweaks);
    return; 
  }
  tweakData.value = data.value;

  var rpcFuncName = C_TWEAK_UPDATE_FUNC_NAMES[ tweakData.varType];
  var params = [ tweakData.name, tweakData.varType, tweakData.value];
  console.log( params);
  controllerTCP.CallGameFunction( rpcFuncName, params, RPCUpdateTweakValue);
}

function RPCOnGameUpdateTweakValue( varName, varValue)
{
  //Send new value straight to the game
  console.log('[CTRL] ~ ' + varName+ ' = ' + varValue);
  var tweakData = l_ccjson.tweaks[ varName];
  if ( tweakData === undefined){
    console.log('[CTRL] ERROR: Cannot find tweak data for: ' + varName);
    console.log( l_ccjson.tweaks);
    return; 
  }
  tweakData.value = varValue;
  //TODO: Send new value to WebClient
}

function InitController( cTCP)
{
  controllerTCP = cTCP;
  //Final
  controllerTCP.AddMethod( "RegisterNewTweak", RPCRegisterNewTweak);
  controllerTCP.AddMethod( "SetCCJSON", RPCSetCCJSON);
  controllerTCP.AddMethod( "UpdateTweakValue", RPCOnGameUpdateTweakValue);

  controllerTCP.CallGameFunction( "GetCCJson", [], RPCSetCCJSON);
}

//Called when a new web client is opened
function OnNewConnection( iosocket)
{
  iosocket.on('onTweakValueChanged', OnWebUpdateTweakValue); 
}

function GetCCJSON()
{ return l_ccjson; }

exports.Init = InitController;
exports.OnNewConnection = OnNewConnection;
exports.GetCCJSON = GetCCJSON;
