var express = require("express");
var app = express();
var port = 3700;
var jade = require('jade');
//Setup Express
app.set('views', __dirname + '/tpl');
app.set('view engine', "jade");
app.engine('jade', jade.__express);

//Include client-side js code
app.use(express.static(__dirname + '/public'));

//Constants and settings
var cGameListenPort = 13;
var cTCPServerPort = 15;

var controllerTCP = require("./controllerTCP.js");

controllerTCP.StartServer( cTCPServerPort);

function ConsolePrint(err, error, response)
{
  console.log('[CTRL] GameReplied: ' + response);
}

var controller = require("./controller.js");
controller.Init( controllerTCP);

var io = require('socket.io').listen(app.listen(port));
var messages = [{ message: 'welcome to the chat' }];

//Web-Client socket setup
io.sockets.on('connection', function (socket) {
    controller.OnNewConnection( socket);
    //io.sockets.emit('message', data); //Use this to talk to the webclient
});

app.get("/", function(req, res){
    console.log("[Global] New WebClient Connected");
    //res.l_variables =  [{ data: { name:"test", value:42}}]; //controller.GetRegisteredVars();
    res.render("main", { l_variables: { 
        "controllerIp":"192.168.0.12",
        "controllerPort":port,
        "ccjson":controller.GetCCJSON()} });
    //console.log( controller.GetCCJSON());
});

console.log("[Global] WebSock listening on port " + port);