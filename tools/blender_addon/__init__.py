# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

import bpy

from bpy.props import StringProperty, BoolProperty, FloatProperty, EnumProperty
from bpy.types import Operator, AddonPreferences
from bpy_extras.io_utils import ExportHelper

bl_info = {
	"name": "Bolts Collada Exporter",
	"author": "Cerbu",
	"blender": (2, 5, 8),
	"support": "OFFICIAL",
	"category": "Import-Export"
}


class ExportBolts(Operator, ExportHelper):
	bl_idname = "export.bolts_split"
	bl_label = "Export selected nodes to DAE"

	directory = bpy.props.StringProperty(subtype='DIR_PATH')

	def execute(self,context):
		import os.path
		from collections import deque
		
		outDir =  bpy.path.abspath(self.directory) #os.path.dirname(bpy.data.filepath)
		print("Outputing to: " + outDir)
		exportedList = []
		selectedList = []
		for node in bpy.context.selected_objects:
			if (node.select and (not node.parent.select)):
				exportedList.append(node)
			if (node.select):
				selectedList.append(node)
		
		for node in selectedList:
			node.select = False
		for node in exportedList:
			node.select = True
			outPath = os.path.join(outDir, node.name + ".dae")
			
			# select children so they get exported
			allChildren = []
			childStack = deque()
			childStack.extend(node.children)
			while childStack:
				childNode = childStack.popleft()
				childNode.select = True
				childStack.extend(childNode.children)
				allChildren.append(childNode)
			
			# export
			bpy.ops.export_scene.dae(filepath=outPath,
				use_export_selected=True,
				use_triangles=True )
			
			# restore
			for childNode in allChildren:
				childNode.select = False
			
			print(outPath)
			node.select = False
		for node in selectedList:
			node.select = True
		return {'FINISHED'}

	def invoke(self, context, event):
		context.window_manager.fileselect_add(self)
		return {'RUNNING_MODAL'}

def add_object_button(self,context):
	self.layout.operator(ExportBolts.bl_idname, icon='PLUGIN')

def register():
	bpy.utils.register_module(__name__)
	bpy.types.INFO_MT_file_export.append(add_object_button)

def unregister():
	bpy.utils.unregister_module(__name__)
	bpy.types.INFO_MT_file_export.remove(add_object_button)

if __name__ == "__main__":
	register()
