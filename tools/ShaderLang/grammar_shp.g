
//Grammar for lark-parse
//NOTE: Could try using manual ambiguity handling in the lexer to remove the need for explicit %code blocks

program: (statement | comment | unconditional_statement )* 

mul: "*" name [mul]*
test: test2+
?test2: code_block | comment

block : (statement | comment )*

code_block : "%code" LINE+ "%endcode"

?statement :"%define" name_list -> define_statement
		|	"%undef" name_list -> undefine_statement
		|	"%if" condition block [els block] endif
		|	"%pass" name name "%vertex" name "%pixel" name "%endpass" -> pass_block

?unconditional_statement : "%context" name -> context
		|	"%static_feature" feature -> static_feature
		|	"%dynamic_feature" feature -> dynamic_feature
		|	"%exclude" feature name_list -> exclude
		|	"%include" includefile -> include
		|	code_block

?name: NAME
COMMENT : "//" /[^\n]/*
comment : COMMENT
els:"%else"
endif:"%endif"
LINE.2:/[^\n]/+
includefile : ESCAPED_STRING
define : name
feature : name
name_list : name ("," name)*
feature_list : feature ("," feature)*

//?feature_type : "toggle"
//			| "enum" name_list
// Conditionals
NEGATION : "!"
LOGICAL_COMPARISON : ("!="|"==") 
LOGICAL_OPERAND : ("||"|"&&")
//NOTE: Technically, only features should be comparable to enums, but this is the only way to make the parser happy
//expression : [NEGATION] (define | define LOGICAL_COMPARISON name )
expression : [NEGATION] define
condition : expression (LOGICAL_OPERAND expression)*

%import common.CNAME -> NAME
%import common.ESCAPED_STRING
%import common.WS
%ignore WS