from lark import Lark,Transformer,Tree
from lark.tree import Visitor
from collections import namedtuple,OrderedDict
import sys
import io
import copy
import gc

#TODO: How to store all these permutations ?
		# Runtime -> Store tables for all static features
		# Material -> Keep a switch table for dynamic features, and assign each feature a bit in an unsigned char
		#	Keep separate tables for each render-context/render-list combo
		#	Remove dynamic features aggressively ( add context %excludes )
		# Build-time -> brute-force, and store a table for each static_feature permutation
		#	Use %excludes to remove configs early, then check %defines if they've been used before to skip unnecessary recompilation
		# After compilation, CRC all shader programs and do a final collapsing across the entire project
		#TODO: How to store permutations and codegen results ?
		#TODO: Output generated code

		#TODO: Preprocess using all feature permutations
		#TODO: Need to pre-process tree twice
		#	First to get accepted contexts for given config
		#	And second to trim unused permutations based on context
		#SOLUTION:
		#	Instead of two-pass, do a pre-process for every context variant possible ( based on list gathered in initial parse )
		#	When post-process context doesn't match desired one, discard permutation
		#	Ex. Preprocess with config + context = MAIN_VIEW. If during preprocess no %context MAIN_VIEW node is found, discard permutation
		#TODO: Need to explicitly specify matching context when defining passes. Passes from inactive context are ignored
		#	Discard permutation if no passes have been defined

class ValidationError(Exception):
	def __init__(self, message):
		# Call the base class constructor with the parameters it needs
		super(ValidationError, self).__init__(message)
		# Now for your custom code...
		#self.errors = errors

class SHPGatherIncludes(Visitor):
	def __init__(self):
		self.includes = set()

	def include(self,t):
		self.includes.add( t.children[0].children[0].value[1:-1])

class SHPReplaceIncludes(Transformer):
	def __init__(self, generator):
		self.generator = generator

	def include(self,t):
		include_filename = t[0].children[0].value[1:-1]
		return self.generator.fetch_include(include_filename)

class SHPGatherFeatures(Visitor):
	def __init__(self):
		self.features = {}
		self.contexts = set()
		self.pass_names = set()
		self.code = ""

	def make_feature(self, type,t):
		feature = {}
		feature_tree = t.children[0]
		feature["name"] = self.get_name(feature_tree)
		feature["type"] = type
		# Excludes are added later
		feature["excludes"] = set()
		return feature["name"],feature
	# Helpers
	def get_name(self, t):
		return t.children[0].value
	def get_name_list(self, list_tree):
		r = []
		for child in list_tree.children:
			r.append(child.value)
		return r
	def handle_feature_def(self, type, t):
		feature_name, feature = self.make_feature(type, t)
		if feature_name in self.features:
			raise ValidationError("Duplicate feature name found: \"%s\"" % feature_name)
		self.features[feature_name] = feature
	# AST Nodes
	def code_block(self,t):
		for token in t.children:
			self.code += token.value
			self.code += '\n'
	def static_feature(self, t):
		self.handle_feature_def("static", t)
	def dynamic_feature(self, t):
		self.handle_feature_def("dynamic", t)
	def context(self, t):
		self.contexts.add( t.children[0].value)
	def pass_block(self, t):
		t = t.children
		self.pass_names.add( t[1].value)
	def exclude(self, t):
		feature_name = self.get_name(t.children[0])
		excludes = self.get_name_list(t.children[1])
		feature = self.features.get(feature_name, None)
		if feature:
			self.features[feature_name]["excludes"].update(excludes)
		else:
			raise ValidationError("Unknown feature name in exclude statement: \"%s\"" % (feature_name,))

#TODO: Make another visitor to validate if all referenced values and features exist

RenderPassData = namedtuple("RenderPassData", ["context","name","vertex","fragment"])

class SHPGenerate(Visitor):
	def __init__(self, feature_list, contexts):
		# Move static features first
		self.all_features = OrderedDict()
		self.all_contexts = contexts
		self.warnings = []
		self.feature_indices = {}
		self.excludes = {} # Maps a define to the list of defines that would exclude it
		i = 0
		for feature_name,feature in sorted( feature_list.items(), key=lambda item: 0 if item[1]["type"] == "static" else 1 ):
			self.all_features[feature_name] = feature
			self.feature_indices[feature_name] = i
			# Collapse excludes
			excludes = feature["excludes"]
			if excludes:
				for excluded in excludes:
					excluder_list = self.excludes.get( excluded, set())
					if excluded not in feature_list:
						self.warn("Unrecognized feature name \"%s\" found in an exclude list for \"%s\"" % (excluded, feature_name))
					excluder_list.add(feature_name)
					self.excludes[excluded] = excluder_list
			i +=1
		#
		if len(feature_list) >= 16:
			error = "More than 16 features defined. Only a maximum of 16 features are supported for a single Shader Group.\nFeatures are "
			for f in feature_list.keys():
				flist += "%s " % f
			raise ValidationError(error)
		# Generation stats
		self.static_eval = 0
		self.active_eval = 0
		self.unique_perm = 0
		self.invalid_perm = 0
		# Preprocessing state
		self.active_context = None
		self.defines = set()
		self.reset_for_visit()
		# Generation helpers
		self.feature_names = list(self.feature_indices.keys())
		self.build_exclude_map()
		# Generation Results
		self.program_indices = [] # Maps a feature mask to an index into the unique_programs map, or None if config is invalid
		self.unique_programs = []

	def build_exclude_map(self):
		self.feature_exclude_masks = [0] * len(self.feature_indices)
		for feature,idx in self.feature_indices.items():
			excludes = self.all_features[feature]["excludes"]
			for excluded in excludes:
				if excluded in self.feature_indices:
					excl_idx = self.feature_indices[excluded]
					self.feature_exclude_masks[excl_idx] = self.feature_exclude_masks[excl_idx] | (1 << idx)

	def reset_for_geneneration(self):
		self.program_indices = []
		num_permutations = 1 << len(self.all_features)
		for context in self.all_contexts:
			self.program_indices.append( [None] * num_permutations)
		self.unique_programs = []

	def reset_for_visit(self):
		self.condition_eval = []
		self.block_is_active = [True]
		self.passes = []

	def warn(self, message):
		self.warnings.append( message)
	# Helpers
	def get_name(self, t):
		return t.children[0].value
	def get_name_list(self, list_tree):
		r = []
		for child in list_tree.children:
			r.append(child.value)
		return r
	# Block handling
	def is_block_active(self):
		return self.block_is_active[-1];
	def start_block(self, active, condition):
		self.block_is_active.append(active)
		self.condition_eval.append(condition)
	def end_block(self):
		self.block_is_active.pop()
		self.condition_eval.pop()
	# State handling
	def add_define(self, define):
		# Check if not excluded
		if not self.excludes.get( define, set()).isdisjoint( self.defines):
			# Some of the excluding defines are enabled, so we can't enable this define
			return False
		self.defines.add(define)
	def add_feature(self, feature):
		# Check if not excluded
		if not self.excludes.get( feature, set()).isdisjoint( self.defines):
			# Some of the excluding defines are enabled, so we can't enable this define
			return False
		self.defines.add(feature)
		# Remove defines that are excluded by this one
		for define in self.all_features[feature]["excludes"]:
			self.remove_feature_or_define(define)
		return True
	def remove_feature_or_define(self, name):
		self.defines.discard(name)
	def eval_expression(self, t):
		#REMOVED enum support
		#if len(t) >= 3:
		#	feature_tree, op, val_tree = t[idx],t[idx+1],t[idx+2]
		#	feature_name = self.get_name(feature_tree)
		#	val = val_tree.value
		#	return (is_negated != (op.value is "==")) != ( feature_name in self.feature_vals and self.feature_vals[feature_name] == val)
		idx = 0
		is_negated = False
		if not type(t[0]) is Tree:
			is_negated = True
			idx = 1
		define_name = self.get_name(t[idx])
		return is_negated != ( define_name in self.defines)
		
	# AST Nodes
	def define_statement(self, t):
		if not self.is_block_active():
			return
		define_name = self.get_name(t.children[0])
		self.add_define(define_name)
		return
	def undefine_statement(self, t):
		if not self.is_block_active():
			return
		defines = self.get_name_list(t.children[0])
		for define_name in defines:
			self.remove_feature_or_define(define_name)
		return
	def pass_block(self, t):
		if not self.is_block_active():
			return
		t = t.children
		ctx_name = t[0].value
		if ctx_name != self.active_context:
			return
		pass_name = t[1].value
		vert_name = t[2].value
		frag_name = t[3].value
		self.passes.append(  RenderPassData(ctx_name,pass_name, vert_name,frag_name) )
	def condition(self,t):
		cond_eval = False
		if self.is_block_active():
			compare_and = True
			cond_eval = True
			for child in t.children:
				if not type(child) is Tree:
					compare_and = child.value is '&&'
				else:
					cond_eval = cond_eval and self.eval_expression(child.children) if compare_and else cond_eval or self.eval_expression(child.children)
		self.start_block( cond_eval, cond_eval)
	def els(self,t):
		cond_eval = False
		if self.is_block_active():
			# Negate condition
			cond_eval = not self.condition_eval[-1]
		self.end_block()
		self.start_block( cond_eval, cond_eval)
	def endif(self,t):
		self.end_block()
	#
	#def prepare(self, features, context):
	#	self.reset()
	#	self.active_context = context
	#	self.add_define(context)
	#	for name, val in features.items():
	#		self.add_feature(name, val)

	# 
	def get_feature_mask( self, features):
		i = 0
		for feature in features:
			i += 1 << self.feature_indices[feature]
		return i

	def eval_tree(self, parse_tree, context, enabled_feature_mask):
		self.active_context = context
		# Prepare defines
		self.defines.clear()
		self.defines.add(context)
		for i in range(0, len(self.feature_indices) ):
			if ( enabled_feature_mask & (1 << i)):
				self.defines.add( self.feature_names[i])
		#
		self.reset_for_visit()
		self.visit(parse_tree)
		return tuple(self.passes)

	# Process all feature and context permutations
	def generate_files(self,ast):
		self.reset_for_geneneration()
		remaining_features = list(self.all_features.keys())
		requested_features = []
		self.gen_recursive( ast, requested_features, remaining_features, {}, {}, 0)

	def get_valid_features(self,accepted_mask, current_depth):
		if current_depth > 6:
			return None
		# Work out excludes for given accepted features
		num_features = len(self.feature_indices)
		candidates_mask = 0
		feature_mask = 0
		for i in range(0,num_features):
			feature_mask = 1 << i
			if ( (accepted_mask & feature_mask) and (self.feature_exclude_masks[i] & accepted_mask) == 0):
				# Feature is accepted based on currently active features
				candidates_mask |= feature_mask
		if candidates_mask == accepted_mask:
			# Candidates are unchanged after another iteration. Keep list as valid
			return candidates_mask
		else:
			return self.get_valid_features(candidates_mask,current_depth+1)

	def gen_recursive(self,ast, requested_features, remaining, static_cache, eval_cache, depth):
		# Check if current defines haven't already been tried
		self.static_eval += 1
		request_mask = self.get_feature_mask(requested_features)
		accepted_mask = self.get_valid_features(request_mask, 0) 
		if accepted_mask is None: # Feature setup is invalid
			context_index = 0
			for context in self.contexts:
				self.program_indices[context_index][request_mask] = None
				self.invalid_perm += 1
				context_index += 1
		# Check if this set of valid features has already been evaluated
		if not accepted_mask in static_cache:
			static_cache[accepted_mask] = request_mask # Point all similar feature setups to results for this one
			context_index = 0
			for context in self.all_contexts:
				self.active_eval += 1
				eval_result = (self.eval_tree(ast, context,accepted_mask), frozenset( self.defines))
				if len( eval_result[0]) == 0: # Feature setup is invalid
					program_idx = None
					self.invalid_perm += 1
				else:
					if not eval_result in eval_cache:
						self.unique_perm += 1
						self.unique_programs.append( eval_result)
						program_idx = len(self.unique_programs) - 1
						eval_cache[eval_result] = program_idx
					else:
						program_idx = eval_cache[eval_result]
				# Set [shader program set] idx for current permutation
				self.program_indices[context_index][request_mask] = program_idx
				#
				context_index += 1
		else: # Found similar feature setup already processed. Copy results from there
			similar_mask = static_cache[accepted_mask]
			context_index = 0
			for context in self.all_contexts:
				self.program_indices[context_index][request_mask] = self.program_indices[context_index][similar_mask]
				context_index += 1
		# Process remaining permutations
		if len(remaining) > 0:
			i = 0
			for feature in remaining:
				#for x in range(0,depth):
				#	print(end='\t')
				#print(feature)
				requested_features.append(feature)
				self.gen_recursive(ast,requested_features,remaining[i+1:],static_cache, eval_cache, depth+1)
				requested_features.pop()
				i+=1

class ShaderGenerator:
	def __init__(self, grammar_content, shp_content):
		self.parser = Lark(grammar_content,start='program', parser='lalr', lexer="contextual")
		self.source = self.parser.parse(shp_content)
		self.includes_missing = self.scan_for_includes( self.source)
		self.includes = OrderedDict()

	def add_source(self, filename, content):
		del self.includes_missing[ self.includes_missing.index(filename) ] #Doing it like this throws error when filename isn't in list
		parse_tree = self.parser.parse(content)
		includes = self.scan_for_includes( parse_tree)
		for include_string in includes:
			if include_string not in self.includes:
				self.includes_missing.append( include_string)
		self.includes[filename] = parse_tree

	def can_generate(self):
		return len(self.includes_missing) == 0

	def scan_for_includes( self, parse_tree ):
		includes = SHPGatherIncludes()
		includes.visit(parse_tree)
		return list(includes.includes)

	def fetch_include(self, include_filename):
		if not include_filename in self.includes:
			raise Exception("Requested include file not found in map: %s" % include_filename)
		included_tree = self.includes[include_filename]
		del self.includes[include_filename] # Don't include same file multiple times
		replacer = SHPReplaceIncludes(self)
		return replacer.transform(included_tree)

	def generate_permutations(self):
		# Replace includes
		replacer = SHPReplaceIncludes(self)
		self.source = replacer.transform( self.source)
		#
		gatherer = SHPGatherFeatures()
		gatherer.visit(self.source)
		#print_exclusion_table(gatherer.features)
		generator = SHPGenerate( gatherer.features, gatherer.contexts)
		generator.generate_files( self.source)

		print("Features:", len(gatherer.features), " | Contexts", len(gatherer.contexts))
		print("Results"," | Static Tests:", generator.static_eval," | Active Tests:", generator.active_eval, " | Unique Permutations", generator.unique_perm, " | Invalid Permutations", generator.invalid_perm)

		return { "features":generator.all_features, "contexts":gatherer.contexts, "pass_names": gatherer.pass_names, "programs":generator.unique_programs, "mapping":generator.program_indices, "code":gatherer.code}
