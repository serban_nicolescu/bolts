// ShgJsonToBinary.cpp : Defines the entry point for the console application.
//

#include <string>
#include <vector>
#include <cstdio>
#include <cstdint>
#include <type_traits>
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

class NonCopyable
{
	NonCopyable(const NonCopyable&) = delete;
	NonCopyable(NonCopyable&&) = delete;
	NonCopyable& operator=(const NonCopyable&) = delete;
	NonCopyable& operator=(NonCopyable&&) = delete;
public:
	NonCopyable() {}
};

template<class T>
struct OffsetPtr : NonCopyable
{
	uint32_t offset;

	T* operator->() { return get(); }
	const T* operator->() const { return get(); }

	const T* get() const { return reinterpret_cast<const T*>(reinterpret_cast<const char*>(this) + offset); }
	T* get() { return reinterpret_cast<T*>(reinterpret_cast<char*>(this) + offset); }
	const T& operator[](std::ptrdiff_t index) const { return *(get() + index); }
	T& operator[](std::ptrdiff_t index) { return *(get() + index); }
};

// Array for fixed-sized elements. Data is assumed to be placed right after container
template<class T>
struct InplaceArray :NonCopyable
{
	uint32_t size;

	uint32_t SizeInBytes() const { return sizeof(*this) + (size * sizeof(T)); }
	T* Begin() { return reinterpret_cast<T*>(&size + 1); }
	const T* Begin() const { return reinterpret_cast<const T*>(&size + 1); }
	T operator[](std::ptrdiff_t index) const { return *(Begin() + index); }
};

// Array for variable-sized elements. Data is assumed to be placed right after container
template<class T>
struct InplaceOffsetVector :NonCopyable
{
	InplaceArray<uint32_t> offsets; // Byte offsets from this to data element

	uint32_t Size() const { return offsets.size; } // Return the number of elements
	uint32_t SizeOf(unsigned index) const { return offsets[index + 1] - offsets[index]; } // Returns the size in bytes of given element
	uint32_t SizeInBytes() const { return offsets[offsets.size]; } // Size in bytes of this vector and its contents
	const T* Get(std::ptrdiff_t index) const { return reinterpret_cast<const T*>(reinterpret_cast<const char*>(this) + offsets[index]); }
	const T* operator[](std::ptrdiff_t index) const { return Get(index); }
};

// Helpers

template<typename T, typename U>
int find_index_of(const std::vector<T>& vec, const U& val)
{
	auto it = std::find(vec.begin(), vec.end(), val);
	if (it == vec.end())
		return -1;
	else return it - vec.begin();
}

template<typename T>
uint32_t size_est(T in)
{
	return sizeof(T);
}

template<typename U>
std::enable_if_t< std::is_pod<U>::value, uint32_t> size_est(const std::vector<U>& in)
{
	return sizeof(uint32_t) + (in.size() * sizeof(U));
}

template<typename U>
std::enable_if_t< !std::is_pod<U>::value, uint32_t> size_est(const std::vector<U>& in)
{
	uint32_t size = sizeof(uint32_t);
	size += (in.size() + 1) * sizeof(uint32_t);
	for (auto const& elem : in)
		size += elem.sizeEst();
	return size;
}

uint32_t size_est(const std::string& in)
{
	return in.length() + 1;
}

uint32_t size_est(const std::vector<std::string>& in)
{
	const uint32_t numElements = in.size();
	uint32_t currentOffset = sizeof(numElements) + (numElements + 1) * sizeof(uint32_t);
	for (auto const& str : in) {
		currentOffset += str.length() + 1;
	}
	return currentOffset;
}

template<class T>
uint32_t write_to_file(T in, FILE* f)
{
	return fwrite(&in, 1, sizeof(T), f);
}

// Write a vector of variable length objects as an IndirectOffsetVector<T>
template<class T>
std::enable_if_t< !std::is_pod<T>::value, uint32_t> write_to_file(const std::vector<T>& in, FILE* f)
{
	uint32_t numElements = in.size();
	std::vector<uint32_t> offsets;
	offsets.resize(numElements + 1);
	int idx = 0;
	uint32_t currentOffset = sizeof(numElements) + (numElements + 1) * sizeof(uint32_t);
	offsets[idx] = currentOffset;
	for (auto const& elem : in) {
		idx++;
		currentOffset += elem.sizeEst();
		offsets[idx] = currentOffset;
	}
	uint32_t written = write_to_file(offsets, f);
	for (auto const& elem : in) {
		written += elem.writeToFile(f);
	}
	return written;
}

template<class T>
std::enable_if_t< std::is_pod<T>::value, uint32_t> write_to_file(const std::vector<T>& in, FILE* f)
{
	const uint32_t numElements = in.size();
	uint32_t written = fwrite(&numElements, 1, sizeof(numElements), f);
	written += fwrite(in.data(), 1, sizeof(T) * numElements, f);
	return written;
}

// Write a vector of strings as an IndirectOffsetVector<char>
uint32_t write_to_file(const std::vector<std::string>& in, FILE* f)
{
	const uint32_t numElements = in.size();
	std::vector<uint32_t> offsets;
	offsets.resize(numElements + 1);
	int idx = 0;
	uint32_t currentOffset = sizeof(numElements) + (numElements + 1) * sizeof(uint32_t);
	offsets[idx] = currentOffset;
	for (auto const& str : in) {
		idx++;
		currentOffset += str.length() + 1;
		offsets[idx] = currentOffset;
	}
	uint32_t written = 0;
	written += fwrite(&numElements, 1, sizeof(uint32_t), f);
	written += fwrite(offsets.data(), 1, sizeof(uint32_t)*(numElements + 1), f);
	for (auto const& str : in) {
		written += fwrite(str.c_str(), 1, str.length() + 1, f);
	}
	return written;
}


uint32_t write_to_file(const std::string& in, FILE* f)
{
	return fwrite(in.c_str(), 1, in.length() + 1, f);
}


// Public classes

struct PassInfo {
	int8_t contextIndex;
	uint8_t passIndex;
	uint16_t programIndex;
};

struct ProgramSet
{
	PassInfo passes[4];
};

struct ProgramMap
{
	OffsetPtr< InplaceArray<uint16_t>> programIndicesUsed;
	OffsetPtr< InplaceArray<ProgramSet>> programSets;
};

struct ProgramCompileInfo
{
	OffsetPtr< char> vertexEntry;
	OffsetPtr< char> fragmentEntry;
	OffsetPtr< InplaceOffsetVector <char>> defines;
};


struct ShaderGroupFile {
	OffsetPtr< InplaceOffsetVector<char>> staticFeatures;
	OffsetPtr< InplaceOffsetVector<char>> dynamicFeatures;
	OffsetPtr< InplaceOffsetVector<char>> contexts;
	OffsetPtr< InplaceOffsetVector<char>> passes;
	OffsetPtr< char > code;
	OffsetPtr< InplaceOffsetVector<ProgramCompileInfo>> uniquePrograms;
	OffsetPtr< InplaceOffsetVector<ProgramMap>> staticMap;
};

//

int main(int argc, char* argv[])
{
	using namespace std;

	const char* inputFilename = static_cast<const char*>(argv[1]);
	const char* outputFilename = static_cast<const char*>(argv[2]);

	vector<char> inputData;

	{
		FILE* f = nullptr;
		fopen_s(&f, inputFilename, "rb");
		if (f)
		{
			fseek(f, 0, SEEK_END);
			long fsize = ftell(f);
			fseek(f, 0, SEEK_SET);
			inputData.resize(fsize + 1);
			fread(inputData.data(), 1, fsize, f);
			inputData[fsize] = 0;
			fclose(f);
		}
	}
	//
	struct Program
	{
		uint32_t sizeEst() const {
			uint32_t currentOffset = sizeof(ProgramCompileInfo);
			currentOffset += size_est(vertex_entry);
			currentOffset += size_est(fragment_entry);
			currentOffset += size_est(defines);
			return currentOffset;
		}

		uint32_t writeToFile(FILE* f) const{
			ProgramCompileInfo pm;
			uint32_t currentOffset = sizeof(ProgramCompileInfo);
			pm.vertexEntry.offset = currentOffset - offsetof(ProgramCompileInfo, vertexEntry);
			currentOffset += size_est(vertex_entry);
			pm.fragmentEntry.offset = currentOffset - offsetof(ProgramCompileInfo, fragmentEntry);
			currentOffset += size_est(fragment_entry);
			pm.defines.offset = currentOffset - offsetof(ProgramCompileInfo, defines);
			currentOffset += size_est(defines);

			uint32_t written = fwrite(&pm, 1, sizeof(ProgramCompileInfo), f);
			written += write_to_file(vertex_entry, f);
			written += write_to_file(fragment_entry, f);
			written += write_to_file(defines, f);
			return written;
		}


		bool operator==(const Program& other) const { return vertex_entry == other.vertex_entry && fragment_entry == other.fragment_entry && defines == other.defines; }

		string vertex_entry;
		string fragment_entry;
		vector<string> defines;
	};

	struct ProgramMapData
	{
		uint32_t sizeEst() const {
			uint32_t currentOffset = sizeof(ProgramMap);
			currentOffset += size_est(usedProgramIndices);
			currentOffset += size_est(sets);
			return currentOffset;
		}

		uint32_t writeToFile(FILE* f) const {
			ProgramMap pm;
			uint32_t currentOffset = sizeof(ProgramMap);
			pm.programIndicesUsed.offset = currentOffset - offsetof(ProgramMap, programIndicesUsed);
			currentOffset += size_est(usedProgramIndices);
			pm.programSets.offset = currentOffset - offsetof(ProgramMap, programSets);
			currentOffset += size_est(sets);

			uint32_t written = fwrite(&pm, 1, sizeof(ProgramMap), f);
			written += write_to_file(usedProgramIndices, f);
			written += write_to_file(sets, f);
			return written;
		}

		vector<uint16_t> usedProgramIndices;
		vector<ProgramSet> sets;
	};

	struct PerContextData {
		vector<PassInfo> pass;
	};

	vector<Program> uniquePrograms;
	vector<string> contexts;
	vector<string> passes;
	vector<string> allFeatures;
	vector<string> staticFeatures;
	vector<string> dynamicFeatures;
	vector<PerContextData> contextData;
	contextData.emplace_back();//Index 0 is bad

	//Read JSON
	using namespace rapidjson;
	Document d;
	d.Parse<ParseFlag::kParseDefaultFlags>(inputData.data());
	Value& contextsJson = d["contexts"];
	for (Value::ConstValueIterator itr = contextsJson.Begin(); itr != contextsJson.End(); ++itr)
		contexts.push_back(itr->GetString());
	Value& passesJson = d["pass_names"];
	for (Value::ConstValueIterator itr = passesJson.Begin(); itr != passesJson.End(); ++itr)
		passes.push_back(itr->GetString());
	// Features
	Value& featuresJson = d["features"];
	auto it = featuresJson.MemberBegin();
	for (; it != featuresJson.MemberEnd(); it++)
	{
		const char* name = it->name.GetString();
		allFeatures.push_back(name);
		if (strcmp("static", it->value["type"].GetString()) == 0) {
			staticFeatures.push_back(name);
		}
		else {
			dynamicFeatures.push_back(name);
		}
	}
	static_assert(std::is_pod< uint16_t>::value,"Bla");
	// Programs
	Value& programsJson = d["programs"];
	contextData.reserve(programsJson.Size());
	Program temp;
	for (Value::ConstValueIterator program = programsJson.Begin(); program != programsJson.End(); ++program) {
		contextData.emplace_back();
		PerContextData& currentEntry = contextData.back();
		temp.defines.clear();

		const Value& definesArray = (*program)[1u];
		temp.defines.reserve(definesArray.Size());
		for (Value::ConstValueIterator define = definesArray.Begin(); define != definesArray.End(); ++define)
			temp.defines.push_back(define->GetString());

		const Value& passesJson = (*program)[0u];
		for (Value::ConstValueIterator pass = passesJson.Begin(); pass != passesJson.End(); ++pass) {
			const Value& stuff = *pass;
			currentEntry.pass.emplace_back();
			auto& currentPass = currentEntry.pass.back();
			currentPass.contextIndex = find_index_of(contexts, stuff[0u].GetString());
			currentPass.passIndex = find_index_of(passes, stuff[1u].GetString());
			temp.vertex_entry = stuff[2u].GetString();
			temp.fragment_entry = stuff[3u].GetString();
			int idx = find_index_of(uniquePrograms, temp);
			if (idx == -1) {
				uniquePrograms.push_back(temp);
				idx = uniquePrograms.size() - 1;
			}
			currentPass.programIndex = idx;
		}
	}
	//
	Value& mappingJson = d["mapping"];
	vector<vector<uint16_t>> mappings;
	mappings.resize(contexts.size());
	int idx = 0;
	for (Value::ConstValueIterator contextsJson = mappingJson.Begin(); contextsJson != mappingJson.End(); ++contextsJson) {
		auto& indices = mappings[idx];
		indices.reserve(contextsJson->Size());
		for (Value::ConstValueIterator contextDataIndex = contextsJson->Begin(); contextDataIndex != contextsJson->End(); ++contextDataIndex) {
			if (contextDataIndex->IsNull())
				indices.push_back(0);
			else
				indices.push_back(contextDataIndex->GetInt() + 1);
		}
		idx++;
	}
	Value& codeJson = d["code"];
	// Put everything into the final binary format
	const unsigned numDynamicSets = 1 << (staticFeatures.size());// One dynamic map for every static permutation
	const unsigned numProgramSets = 1 << (dynamicFeatures.size());
	const unsigned numContexts = contexts.size();
	bool usedPrograms[1 << 16];
	vector<ProgramMapData> dynamicMaps;
	dynamicMaps.resize(numDynamicSets);
	for (int progMap = 0; progMap < numDynamicSets; progMap++) {
		ProgramMapData& currentDynamic = dynamicMaps[progMap];
		memset(usedPrograms, 0, 1 << 16);
		currentDynamic.sets.resize(numProgramSets);
		for (int progSet = 0; progSet < numProgramSets; progSet++) {
			ProgramSet& currentSet = currentDynamic.sets[progSet];
			int mappingOffset = progMap * numProgramSets + progSet;
			int numPasses = 0;
			for (int ctx = 0; ctx < numContexts; ctx++) {
				int dataIdx = mappings[ctx][mappingOffset];
				if (dataIdx == 0)
					continue;
				PerContextData& ctxData = contextData[dataIdx];
				for (auto data : ctxData.pass) {
					currentSet.passes[numPasses] = data;
					numPasses++;
					if (!usedPrograms[data.programIndex]) {
						currentDynamic.usedProgramIndices.push_back(data.programIndex);
						usedPrograms[data.programIndex] = true;
					}
				}
			}
			// Mark remaining passes as invalid
			for (int i = numPasses; i < 4; i++)
				currentSet.passes[i].contextIndex = -1;
		}
	}

	// Write it out
	{
		ShaderGroupFile fileMap;
		uint32_t offset = sizeof(ShaderGroupFile);
		fileMap.staticFeatures.offset = offset - offsetof(ShaderGroupFile, staticFeatures);
		offset += size_est(staticFeatures);
		fileMap.dynamicFeatures.offset = offset - offsetof(ShaderGroupFile, dynamicFeatures);
		offset += size_est(dynamicFeatures);
		fileMap.contexts.offset = offset - offsetof(ShaderGroupFile, contexts);
		offset += size_est(contexts);
		fileMap.passes.offset = offset - offsetof(ShaderGroupFile, passes);
		offset += size_est(passes);
		fileMap.code.offset = offset - offsetof(ShaderGroupFile, code);
		offset += codeJson.GetStringLength() + 1;
		fileMap.uniquePrograms.offset = offset - offsetof(ShaderGroupFile, uniquePrograms);
		offset += size_est(uniquePrograms);
		fileMap.staticMap.offset = offset - offsetof(ShaderGroupFile, staticMap);
		//
		FILE* f = nullptr;
		fopen_s(&f, outputFilename, "wb");
		uint32_t size;
		if (f)
		{
			fwrite(&fileMap, 1, sizeof(ShaderGroupFile), f);
			write_to_file(staticFeatures, f);
			write_to_file(dynamicFeatures, f);
			write_to_file(contexts, f);
			write_to_file(passes, f);
			fwrite(codeJson.GetString(), 1, codeJson.GetStringLength()+1, f);
			size = offset;
			size += write_to_file(uniquePrograms, f);
			//Write static map offset table
			size += write_to_file(dynamicMaps, f);
			fclose(f);
		}
		
		/*
		fopen_s(&f, outputFilename, "rb");
		vector<char> data;
		data.resize(size);
		fread(data.data(), 1, size, f);
		ShaderGroupFile& test = *reinterpret_cast<ShaderGroupFile*>(data.data());
		test.code.get();
		test.uniquePrograms->Size(); test.uniquePrograms->SizeInBytes(); test.uniquePrograms->SizeOf(0); test.uniquePrograms->Get(0);
		test.uniquePrograms->Get(0)->fragmentEntry.get();
		test.uniquePrograms->Get(0)->defines.get()->Get(0);
		test.staticFeatures->Size(); test.staticFeatures->SizeInBytes(); test.staticFeatures->SizeOf(0); test.staticFeatures->Get(0);
		test.staticMap->Size(); test.staticMap->SizeInBytes(); test.staticMap->SizeOf(0); test.staticMap->Get(0)->programIndicesUsed->operator[](0);
		fclose(f);
		*/
	
	}


	//



	return 0;
}

