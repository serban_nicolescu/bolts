from parse_shp import ShaderGenerator
import os
import sys
import io
import copy
import argparse
import json

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	
	parser.add_argument("--src", help="path to source .shp file")
	parser.add_argument("-i", "--include", action="append")
	parser.add_argument("-g", "--grammar", default="./grammar_shp.g")
	parser.add_argument("-s", "--save")
	parser.add_argument("--pretty", action="store_true")
	args = parser.parse_args()

	with open(args.grammar) as f:
		grammar = f.read()
	with open(args.src) as f:
		source = f.read()

	generator = ShaderGenerator(grammar, source)

	found_one = True
	while (not generator.can_generate() and found_one):
		found_one = False
		for include_filename in generator.includes_missing:
			for path in args.include:
				_,filename = os.path.split(path)
				if filename != include_filename:
					continue
				with open(path) as f:
					generator.add_source(filename, f.read())
					found_one = True

	if (not generator.can_generate()):
		raise Exception("Could not find all include files for source. Missing file(s): %s" % ','.join(generator.includes_missing))
	#
	results = generator.generate_permutations()
	class ResultEncoder(json.JSONEncoder):
		def default(self, obj):
			if isinstance(obj, frozenset) or isinstance(obj, set):
				return list(obj)
			# Let the base class default method raise the TypeError
			return json.JSONEncoder.default(self, obj)
	if args.save:
		with open(args.save, 'w') as f:
			if not args.pretty:
				json.dump( results, f, cls=ResultEncoder)
			else:
				json.dump( results, f, cls=ResultEncoder, indent=4)
