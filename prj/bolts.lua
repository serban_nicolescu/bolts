-- A project defines one build target
local lProjectName = "Bolts"

_deps[lProjectName] = function (header_only)
	header_only = header_only or false
	includedirs 
	{ 
		"../src/",
		bCfg.depPath .. "rapidjson/include/",
		bCfg.depPath .. "glew/include/",
		bCfg.depPath .. "Loki/include/",
		bCfg.depPath .. "rapidxml/",
		bCfg.depPath .. "IconFontCppHeaders/",
	}
	
	defines { "BOLTS_OPENGL" }
	
	_deps["BoltsCore"](header_only)
	_deps["libjpg"](header_only)
	_deps["ImGui"](header_only)
	_deps["soil"](header_only)
	_deps["microprofile"](header_only)
	
	if not header_only then	
		links {
			lProjectName,
			"opengl32", 
			"glu32", 
			"glew32",
		}
		
		libdirs 
		{
			bCfg.depPath .. "glew/lib/Release/Win32/", 
		}
	end
end

project "Bolts"
	location ( bCfg.prjPath )
	kind "StaticLib"
	language "C++"
	files 
	{ 
		"../src/**.h", 
		"../src/**.cpp" 
	}
	
	includedirs
	{
		"../src/",
		bCfg.depPath .. "rapidjson/include/",
		bCfg.depPath .. "glew/include/",
		bCfg.depPath .. "Loki/include/",
		bCfg.depPath .. "rapidxml/",
		bCfg.depPath .. "IconFontCppHeaders/",
	}
	
	flags { "FatalWarnings" }
	warnings "Extra"
	
	targetdir ( "../libs/" .. _ACTION .. "/%{prj.name}/%{cfg.shortname}" )
	
	configuration { "vs*"}
		defines { "_WIN32_WINNT=0x0600" }
	configuration {}
	
	--Disable some warnings
	buildoptions 
	{ 
		"/wd\"4127\"", -- VS: Conditional expression is constant ( from logging)
		"/wd\"4512\"", -- VS: Assignment operator could not be generated
	}
	
	--Defined by default for now
	defines { "BOLTS_OPENGL", "GLM_FORCE_INLINE" }
	
	_deps["BoltsCore"]()
	_deps["libjpg"]()
	_deps["ImGui"]()
	_deps["soil"]()
	_deps["microprofile"]()

	
	