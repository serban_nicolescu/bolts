-- A solution contains projects, and defines the available configurations


-- Global config
include "common.lua"

-------------------------------------------------
--
-------------------------------------------------

solution "Shooty"
	location ( bCfg.prjPath )
	
	configurations { "Debug", "Release" }
	
	debugdir ( "../resources" )
	targetdir( bCfg.libOutPath )
	objdir 	( bCfg.objPath )
	
	filter { "action:vs*" }
		system "Windows"
		platforms { "x32", "x64" }
		characterset "MBCS"
	filter {}

	flags { "MultiProcessorCompile", "NoMinimalRebuild" }
	
	configuration "Debug"
		defines { "DEBUG" }
		optimize "Off"
		inlining "Explicit"
		symbols "On"
		targetsuffix "-d"
	configuration "Release"
		defines { "NDEBUG" }
		optimize "Speed"
	configuration {}
	
	filter { "system:Windows" }
		defines { "WIN32", "_CRT_SECURE_NO_WARNINGS", "_SCL_SECURE_NO_WARNINGS", "WIN32_LEAN_AND_MEAN", "NOMINMAX" }
		systemversion "latest"
	filter{}
	
	include "microprofile.lua"
	include "libjpg.lua"
	include "Soil.lua"
	-- GUI Stuff
	include "imgui.lua"
	include "imgui_nodes.lua"
	--
	include "bolts_core.lua"
	include "bolts.lua"
	include "shooty.lua"
	-- External Tool Stuff
	-- include "nanomsg.lua"
	