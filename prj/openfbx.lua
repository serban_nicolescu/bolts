local lProjectName = "OpenFBX"

_deps[lProjectName] = function (header_only)
	header_only = header_only or false
	
	includedirs 
	{ 
		bCfg.depPath .. lProjectName .. "/src/",
	}
	
	if not header_only then	
		links {
			lProjectName,
		}
	end
end

project (lProjectName)
	location ( bCfg.prjPath )
	kind "StaticLib"
	language "C++"
	
	files 
	{ 
		bCfg.depPath .. lProjectName .. "/src/**.h",
		bCfg.depPath .. lProjectName .. "/src/**.cpp",
		bCfg.depPath .. lProjectName .. "/src/**.c",
	}
	includedirs 
	{ 
		bCfg.depPath .. lProjectName .. "/src/"
	}	 

	defines
	{ 
		
	}