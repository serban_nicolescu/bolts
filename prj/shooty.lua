-- A project defines one build target
project "Shooty"
	location ( bCfg.prjPath )
	kind "ConsoleApp"
	language "C++"
	
	files 
	{ 
		"../Shooty/**.h", 
		"../Shooty/**.cpp",
	}
	includedirs 
	{
		"../Shooty",
		"ShgJsonToBinary/ShaderGroupBinary.h",
		bCfg.depPath .. "lightmapper",
	}
	
	-- VS-specific
	configuration { "vs*"}
		defines { "_WIN32_WINNT=0x0501" }
		--Disable some warnings
		buildoptions 
		{ 
			"/wd\"4127\"", -- VS: Conditional expression is constant ( from logging)
			"/wd\"4512\"", -- VS: assignment operator could not be generated
		}
	configuration {}
	-- Configs
	--libdirs (bCfg.libOutPath)
	configuration "Debug*"
		targetdir ( bCfg.outPath .. "Debug/" )
	configuration "Release*"
		targetdir ( bCfg.outPath .. "Release/" )
	configuration {}
	
	pchsource "../Shooty/stdafx.cpp"
	pchheader "stdafx.h"
	
	--flags { "FatalWarnings" }
	defines { "NN_NO_EXPORTS", "GLM_FORCE_INLINE"}
	
	_deps["ImGui"]()
	_deps["ImGuiNodes"]()
	_deps["microprofile"]()
	_deps["Bolts"]()
	_deps["boost"]()
	
	