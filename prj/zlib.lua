local lProjectName = "zlib"

_deps[lProjectName] = function (header_only)
	header_only = header_only or false
	
	includedirs 
	{ 
		bCfg.depPath ..  "zlib/",
	}
	
	if not header_only then	
		links {
			lProjectName,
		}
	end
end

project (lProjectName)
	location ( bCfg.prjPath )
	kind "StaticLib"
	language "C"
	
	files 
	{ 
		bCfg.depPath .. "zlib/adler32.c", 
		bCfg.depPath .. "zlib/compress.c", 
		bCfg.depPath .. "zlib/crc32.c", 
		bCfg.depPath .. "zlib/deflate.c", 
		bCfg.depPath .. "zlib/gzclose.c", 
		bCfg.depPath .. "zlib/gzlib.c", 
		bCfg.depPath .. "zlib/gzread.c", 
		bCfg.depPath .. "zlib/gzwrite.c", 
		bCfg.depPath .. "zlib/inflate.c", 
		bCfg.depPath .. "zlib/infback.c", 
		bCfg.depPath .. "zlib/inftrees.c", 
		bCfg.depPath .. "zlib/inffast.c", 
		bCfg.depPath .. "zlib/trees.c", 
		bCfg.depPath .. "zlib/uncompr.c", 
		bCfg.depPath .. "zlib/zutil.c", 
		bCfg.depPath .. "zlib/crc32.h", 
		bCfg.depPath .. "zlib/deflate.h", 
		bCfg.depPath .. "zlib/gzguts.h", 
		bCfg.depPath .. "zlib/inffast.h", 
		bCfg.depPath .. "zlib/inffixed.h", 
		bCfg.depPath .. "zlib/inflate.h", 
		bCfg.depPath .. "zlib/inftrees.h", 
		bCfg.depPath .. "zlib/trees.h", 
		bCfg.depPath .. "zlib/zutil.h", 
	}
	includedirs 
	{ 
		bCfg.depPath .. "zlib/",
	}
	
	--forceincludes { "windows.h" }