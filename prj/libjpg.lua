local lProjectName = "libjpg"

_deps[lProjectName] = function (header_only)
	header_only = header_only or false
	includedirs 
	{ 
		bCfg.depPath .. "libjpg/",
	}
	if not header_only then	
		links {
			lProjectName
		}
	end
end

project(lProjectName)
	location ( bCfg.prjPath )
	kind "StaticLib"
	language "C"
	
	files 
	{ 
		bCfg.depPath .. "libjpg/**.c", 
		bCfg.depPath .. "libjpg/**.h",
	}
	includedirs 
	{ 
		bCfg.depPath .. "libjpg/",
	}
