local lProjectName = "Box2D"

_deps[lProjectName] = function (header_only)
	header_only = header_only or false
	
	includedirs 
	{ 
		bCfg.depPath .. lProjectName
	}
	
	if not header_only then	
		links { lProjectName }
	end
end

project (lProjectName)
	kind "StaticLib"
	language "C++"
	
	files 
	{ 
		bCfg.depPath .. lProjectName .. "/Box2D/**.h",
		bCfg.depPath .. lProjectName .. "/Box2D/**.hpp",
		bCfg.depPath .. lProjectName .. "/Box2D/**.cpp",
	}
	
	includedirs 
	{ 
		bCfg.depPath .. lProjectName
	}
	
	