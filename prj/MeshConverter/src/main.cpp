#include "StdAfx.h"
#include <Assets/AssetManager.h>
#include <Assets/MeshFile.h>
#include <Assets/AnimationFile.h>
#include <IO/FileIO.h>
#include <string>
#include <cstdio>
#include <iterator>
#include <deque>

#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags

#include <ofbx.h>

#define RAPIDJSON_ASSERT(x)
//#include <rapidjson/reader.h>
#include <rapidjson/document.h>
#include "rapidjson/filewritestream.h"
#include <rapidjson/prettywriter.h>

//#include "thekla_atlas.h" // unique UV generator

#define BOLTS_LOG_SCOPE "[AssetLoad]"

using namespace Bolts;

const char* filenameFromPath(const char* path, int len)
{
	if (!path)
		return nullptr;
	if (len == 0)
		len = (int)strlen(path);

	const char* r = strrchr(path, '\\');
	if (!r)
		r = strrchr(path, '/');

	if (!r)
		return path;
	if (r - path >= len)
		return nullptr;
	return r + 1;
}

struct
{
	void Load(rapidjson::Document& d) {
		centerMeshes		= d["centerMeshes"].GetBool();
		generateUVs			= d["generateUVs"].GetBool();
		generateNormals		= d["generateNormals"].GetBool();
		generateTangents	= d["generateTangents"].GetBool();
		flipUVs				= d["flipUVs"].GetBool();
		forceExportAnimations = d["forceExportAnimations"].GetBool();
		mat.exp				= d["exportMaterials"].GetBool();
		mat.matpack			= d["exportMatpack"].GetBool();
	}

	bool centerMeshes = true;
	bool generateUVs = false;
	bool generateNormals = false;
	bool generateTangents = false;
	bool flipUVs = false;
	bool forceExportAnimations = false;
	struct
	{
		bool exp = false;
		bool matpack = false;
	} mat;

} g_options;

BOLTS_INLINE vec2 toVec2(const ofbx::Vec2& fbxVec) { return { (float)fbxVec.x,(float)fbxVec.y }; }
BOLTS_INLINE vec3 toVec3(const ofbx::Vec3& fbxVec) { return { (float)fbxVec.x,(float)fbxVec.y,(float)fbxVec.z }; }
BOLTS_INLINE vec4 toVec4(const ofbx::Vec4& fbxVec) { return { (float)fbxVec.x,(float)fbxVec.y,(float)fbxVec.z, (float)fbxVec.w }; }
BOLTS_INLINE vec3 ToVec3(const aiVector3D& v) { return { v.x, v.y, v.z }; }
BOLTS_INLINE quat ToQuat(const aiQuaternion& q) { return { q.x, q.y, q.z, q.w }; }

void WriteMeshes(Blob &out, Blob::StorePtr<MeshHeader> header, const ofbx::IScene* scene)
{
	// Meshes first
	char subMeshName[256];
	header->subMeshes.Reserve(out, scene->getMeshCount());
	uint32_t matIdx = 0;
	for (int i = scene->getMeshCount(); i != 0; i--) {
		auto pMesh = scene->getMesh(i - 1);
		auto pGeom = pMesh->getGeometry();
		//TODO:map submeshes to individual buffers (based on num of materials)
		BASSERT(pMesh->getMaterialCount() == 1);
		//
		auto mbinfo = out.StoreNew<MeshBufferInfo>();
		header->subMeshes.PushBack(mbinfo.get());
		mbinfo->materialIdx = matIdx++;
		mbinfo->nodeIdx = 0; //NOTE: This gets filled in by the node scanning code
		mbinfo->numVerts = pGeom->getVertexCount();
		mbinfo->numIndices = pGeom->getIndexCount();// aiMsh->mNumFaces * 3;
		// Next is buffer data info
		mbinfo->hasNormals = pGeom->getNormals() != nullptr;
		mbinfo->hasTgts = pGeom->getTangents() != nullptr;
		//TODO: mbinfo->hasBones = aiMsh->HasBones();
		mbinfo->numColors = pGeom->getColors() != nullptr ? 1 : 0;
		mbinfo->numUVs = pGeom->getUVCount();
		if (mbinfo->numUVs > 4) {
			mbinfo->numUVs = 4;
			BOLTS_LWARNING() << BOLTS_LOG_SCOPE << "Mesh has more than 4 UV channels, but only 4 are supported. Skipping the rest";
		}
		//
		const uint8_t c_posSize = 3 * sizeof(float);
		const uint8_t c_nrmSize = 3 * sizeof(float);
		uint8_t vertexSize = c_posSize; // Position
		vertexSize += 2 * sizeof(float) * mbinfo->numUVs;
		vertexSize += 4 * sizeof(float) * mbinfo->numColors;
		vertexSize += mbinfo->hasNormals ? 3 * sizeof(float) : 0;
		vertexSize += mbinfo->hasTgts ? 3 * sizeof(float) : 0;
		//TODO: Write bone data
		//vertexSize += mbinfo->hasBones > 0 ? 4 * (sizeof(float) + 1) : 0;
		const uint32_t sizeOfVertexBuffer = mbinfo->numVerts*vertexSize;
		//
		const char* meshName = nullptr;
		if (pMesh->name[0] != '\0') {
			meshName = pMesh->name;
		}
		else {
			sprintf(subMeshName, "Mesh%d", i);
			meshName = subMeshName;
		}
		// write out name first
		auto nameArray = out.StoreNewArray<char>(strlen(meshName) + 1);
		mbinfo->name = nameArray.get();
		memcpy(nameArray->First(), meshName, nameArray->count);
		// followed by the interleaved vertex data
		{
			mbinfo->vertices = out.StoreNewArray<char>(sizeOfVertexBuffer).get();
			char* vertDataOut = mbinfo->vertices->First();
			// Pos
			AABB meshAABB;
			meshAABB.min = vec3{ std::numeric_limits<float>::infinity() };
			meshAABB.max = vec3{ -std::numeric_limits<float>::infinity() };
			for (uint32_t i = 0; i < mbinfo->numVerts; ++i) {
				
				vec3 vpos = toVec3(pGeom->getVertices()[i]);
				meshAABB.include(vpos);
				*(vec3*)(vertDataOut + i * vertexSize) = vpos;
			}
			uint8_t offset = c_posSize;

			//if (g_options.centerMeshes) {
			//	// Center mesh in local space
			//	for (uint32_t i = 0; i < mbinfo->numVerts; ++i) {
			//		vec3 vpos = (vec3&)aiMsh->mVertices[i];
			//		*(vec3*)(vertDataOut + i * vertexSize) = vpos - sceneCenter;
			//	}
			//
			//	mbinfo->bboxExtents = glm::max(glm::abs(min - sceneCenter), glm::abs(max - sceneCenter)) / 2.f;
			//}
			//else 
			{
				mbinfo->meshExtents = meshAABB;
			}
			//Normals
			if (mbinfo->hasNormals) {
				for (uint32_t i = 0; i < mbinfo->numVerts; ++i) {
					*(vec3*)(vertDataOut + i * vertexSize + offset) = toVec3(pGeom->getNormals()[i]);
				}
				offset += 3 * sizeof(float);
				if (mbinfo->hasTgts) {
					for (uint32_t i = 0; i < mbinfo->numVerts; ++i) {
						*(vec3*)(vertDataOut + i * vertexSize + offset) = toVec3(pGeom->getTangents()[i]);
					}
					offset += 3 * sizeof(float);
				}
			}
			// UV & COLORS
			BASSERT(mbinfo->numColors == 1); //OpenFBX doesn't support multiple color channels
			for (uint8_t k = 0; k < mbinfo->numColors; k++) {
				for (uint32_t i = 0; i < mbinfo->numVerts; ++i) {
					*(vec4*)(vertDataOut + i * vertexSize + offset) = toVec4(pGeom->getColors()[i]);
				}
				offset += 4 * sizeof(float);
			}
			for (uint8_t k = 0; k < mbinfo->numUVs; k++) {
				for (uint32_t i = 0; i < mbinfo->numVerts; ++i) {
					*(vec2*)(vertDataOut + i * vertexSize + offset) = toVec2(pGeom->getUVs(k)[i]);
				}
				offset += 2 * sizeof(float);
			}
		}
		// Indices
		const uint8_t c_idxSize = mbinfo->getIndexBytes();
		const uint32_t sizeOfIndexBuffer = mbinfo->numIndices * c_idxSize;

		mbinfo->indices = out.StoreNewArray<char>(sizeOfIndexBuffer).get();
		void* idxDataOut = (void*)mbinfo->indices->First();
		int idxCount = mbinfo->numIndices;
		for (unsigned i = 0; i < idxCount; i++) {
			if (c_idxSize == 2) {
				((uint16_t*)(idxDataOut))[i] = pGeom->getFaceIndices()[i];
			}
			else {
				((uint32_t*)(idxDataOut))[i] = pGeom->getFaceIndices()[i];
			}
		}

		// Generate UV1 for lightmapping
		//Thekla::Atlas_Options atlasOptions;
		//Thekla::atlas_set_default_options(&atlasOptions);
		//Thekla::Atlas_Error err;
		//Thekla::Atlas_Input_Mesh imesh;
		//auto outputMesh = Thekla::atlas_generate(, &atlasOptions, &err);
	}
}

void WriteNodes(Blob &out, Blob::StorePtr<MeshHeader> header, const ofbx::IScene* scene)
{
	using namespace ofbx;

	const ofbx::Object* const* pObjArray = scene->getAllObjects();
	auto pObjArrayEnd = pObjArray + scene->getAllObjectCount();
	while (pObjArray != pObjArrayEnd) {
		auto pObj = pObjArray[0];
		auto type = pObj->getType();

		if (type == Object::Type::NULL_NODE ||
			type == Object::Type::ROOT ||
			type == Object::Type::MESH) 
		{
			//pObj[0]->
		}


		pObjArray++;
	}

	header->nodes = out.StoreNew<MeshNodes>().get();
	uint32_t nodeCount = 1;
	auto transforms = out.StoreNewArray<mat4>(nodeCount);
	header->nodes->transforms = transforms.get();
	auto parents = out.StoreNewArray<uint32_t>(nodeCount);
	header->nodes->parents = parents.get();
	auto nameHashes = out.StoreNewArray<hash32_t>(nodeCount);
	header->nodes->nameHashes = nameHashes.get();
	auto names = out.StoreNew< InplaceOffsetArray<char> >();
	header->nodes->names = names.get();
	names->Reserve(out, nodeCount);
	// work out scene extents
	AABB sceneAABB;
	vec3 obbOut[8];
	sceneAABB.min = vec3{ std::numeric_limits<float>::infinity() };
	sceneAABB.max = vec3{ -std::numeric_limits<float>::infinity() };
	//TODO: get AABB
	//
	uint32_t nodeI = 0;
	parents->Set(0) = 0;
	//transforms->First() //TODO:
	char* pName = "not_impl";
	names->PushBack((char*)out.Store(pName, strlen(pName)));
	nameHashes->Set(0) = HashString(pName);
	/*
	while (nodes.size() > 0) {
		aiNode* node = nodes.front();
		// node details
		auto transformDst = transforms->First() + nodeI;
		memcpy(transformDst, &node->mTransformation.Transpose(), sizeof(mat4));
		nameHashes->Set(nodeI) = HashString(node->mName.C_Str(), node->mName.length);
		names->PushBack((char*)out.Store(node->mName.C_Str()));
		// set mesh node indices
		for (unsigned i = 0; i < node->mNumMeshes; i++) {
			MeshBufferInfo* mbinfo = header->subMeshes[node->mMeshes[i]];
			mbinfo->nodeIdx = nodeI;
			GetOBBVerts(obbOut, *transformDst, mbinfo->meshExtents);
			for (int vIdx = 0; vIdx < 8; vIdx++)
				sceneAABB.include(obbOut[vIdx]);
		}
		//
		auto qsize = nodes.size() - 1;
		for (unsigned i = 0; i < node->mNumChildren; i++) {
			// Store parent idx for child nodes
			parents->Set(nodeI + qsize + i + 1) = nodeI;
			nodes.push_back(node->mChildren[i]);
		}
		nodes.pop_front();
		nodeI++;
	}
	*/
	names->SetEnd(out.DataEnd());
	//
	header->sceneExtents = sceneAABB;
}

bool WriteMaterialFile(const ofbx::Material* pMat)
{
	std::string filename = pMat->name;
	filename.append(".material");
	auto outFile = fopen(filename.c_str(), "wb");
	if (!outFile) {
		BOLTS_LERROR() << BOLTS_LOG_SCOPE " Could't open output file for writing: " << filename.c_str();
		return false;
	}

	using namespace rapidjson;
	Document d;
	auto& a = d.GetAllocator();
	d.SetObject();
	d.AddMember("base", "Phong#shadowed", a);
	/*
	Value defines(kArrayType);
	//	"defines": ["SHADOWS", "ALBEDO_MAP", "NORMAL_MAP", "ALPHA_TEST"],
	defines.PushBack("SHADOWS", a);
	Value params(kObjectType);
	for (int i = 0; i < mat->mNumProperties; i++) {
		auto& prop = *mat->mProperties[i];
		auto& propName = prop.mKey;

		BOLTS_LINFO() << BOLTS_LOG_SCOPE " Mat " << filename.C_Str() << " | Prop " << propName.C_Str() << " | Type " << prop.mType << " | Bytes " << prop.mDataLength;
		if (prop.mType == aiPTI_Float) {
			if (prop.mDataLength == 4)
				BOLTS_LINFO() << BOLTS_LOG_SCOPE " Float " << *((float*)prop.mData);
			if (prop.mDataLength == 12)
				BOLTS_LINFO() << BOLTS_LOG_SCOPE " Vec3 " << *((vec3*)prop.mData);
		}
		if (prop.mType == aiPTI_String) {
			// The string is stored as 32 but length prefix followed by zero-terminated UTF8 data
			BOLTS_LINFO() << BOLTS_LOG_SCOPE " String " << (prop.mData + 4);
		}
		if (prop.mType == aiPTI_Buffer && prop.mDataLength == 4) {
			char chars[5];
			chars[4] = '\0';
			uint32_t val;
			memcpy(chars, prop.mData, 4);
			memcpy(&val, prop.mData, 4);
			// The string is stored as 32 but length prefix followed by zero-terminated UTF8 data
			BOLTS_LINFO() << BOLTS_LOG_SCOPE " Buffer Chars " << chars << " Int " << val;
		}

		for (auto& m : c_propMap)
			if (m.type == prop.mType && strcmp(m.inProp, prop.mKey.C_Str()) == 0)
			{
				Value v;
				switch (m.type) {
				case aiPTI_Float: {
					if (m.valCount == 1) {
						v.SetFloat(*((float*)prop.mData));
					}
					else {
						v.SetArray();
						for (int vi = 0; vi < (prop.mDataLength / 4); vi++) {
							v.PushBack(*((float*)(prop.mData + (vi * 4))), a);
						}
					}
					break;
				}
				case aiPTI_String: {
					auto len = *(uint32_t*)(prop.mData + 4);
					const char* texPath = prop.mData + 4;
					const char* texFilename = filenameFromPath(texPath, len);
					BASSERT(texFilename);
					v.SetString(texFilename, a);
					break;
				}
				}
				if (!v.Empty() && m.outProp) {
					params.AddMember(Value(m.outProp, a), v, a);
				}
				if (m.addDefine)
					defines.PushBack(Value(m.addDefine, a), a);
				BOLTS_LINFO() << BOLTS_LOG_SCOPE " Saved";
			}
	}
	d.AddMember("defines", defines, a);
	d.AddMember("params", params, a);
	*/

	// write file
	char writeBuffer[65536];
	FileWriteStream os(outFile, writeBuffer, sizeof(writeBuffer));
	PrettyWriter<FileWriteStream> writer(os);
	d.Accept(writer);
	fclose(outFile);
	return true;
}

void WriteMaterials(Blob &out, Blob::StorePtr<MeshHeader> header, const ofbx::IScene* scene)
{
	header->materials = out.StoreNew<MeshMaterials>().get();
	auto names = out.StoreNew< InplaceOffsetArray<char> >();
	header->materials->names = names.get();
	// count materials
	int numMaterials = 0;
	for (int i = scene->getMeshCount(); i != 0; i--) {
		auto pMesh = scene->getMesh(i - 1);
		numMaterials += pMesh->getMaterialCount();
	}
	names->Reserve(out, numMaterials);
	//
	for (int i = scene->getMeshCount(); i != 0; i--) {
		auto pMesh = scene->getMesh(i - 1);
		const ofbx::Material* pMat = pMesh->getMaterial(0);
		if (g_options.mat.exp)
			WriteMaterialFile(pMat);
		names->PushBack((char*)out.Store(pMat->name));
	}
	names->SetEnd(out.DataEnd());
}

void WriteMeshes(Blob &out, Blob::StorePtr<MeshHeader> header, const aiScene* scene)
{
	// Meshes first
	char subMeshName[256];
	header->subMeshes.Reserve(out, scene->mNumMeshes);
	for (int i = scene->mNumMeshes; i != 0; i--) {
		const aiMesh* aiMsh = scene->mMeshes[i - 1];
		//
		auto mbinfo = out.StoreNew<MeshBufferInfo>();
		header->subMeshes.PushBack(mbinfo.get());
		mbinfo->materialIdx = aiMsh->mMaterialIndex;
		mbinfo->numVerts = aiMsh->mNumVertices;
		mbinfo->numIndices = aiMsh->mNumFaces * 3;
		// Next is buffer data info
		mbinfo->hasNormals = aiMsh->HasNormals();
		mbinfo->hasTgts = aiMsh->HasTangentsAndBitangents();
		//TODO: mbinfo->hasBones = aiMsh->HasBones();
		mbinfo->numColors = aiMsh->GetNumColorChannels();
		if (mbinfo->numColors > 2) {
			mbinfo->numColors = 2;
			BOLTS_LWARNING() << BOLTS_LOG_SCOPE << "Mesh has more than 2 color channels, but only 2 are supported. Skipping the rest";
		}
		mbinfo->numUVs = aiMsh->GetNumUVChannels();
		if (mbinfo->numUVs > 4) {
			mbinfo->numUVs = 4;
			BOLTS_LWARNING() << BOLTS_LOG_SCOPE << "Mesh has more than 4 UV channels, but only 4 are supported. Skipping the rest";
		}
		//
		const uint8_t c_posSize = 3 * sizeof(float);
		const uint8_t c_nrmSize = 3 * sizeof(float);
		uint8_t vertexSize = c_posSize; // Position
		vertexSize += 2 * sizeof(float) * aiMsh->GetNumUVChannels();
		vertexSize += 4 * sizeof(float) * aiMsh->GetNumColorChannels();
		vertexSize += mbinfo->hasNormals ? 3 * sizeof(float) : 0;
		vertexSize += mbinfo->hasTgts ? 3 * sizeof(float) : 0;
		//TODO: Write bone data
		//vertexSize += mbinfo->hasBones > 0 ? 4 * (sizeof(float) + 1) : 0;
		const uint32_t sizeOfVertexBuffer = mbinfo->numVerts*vertexSize;
		//
		const char* meshName = nullptr;
		if (aiMsh->mName.length != 0) {
			meshName = aiMsh->mName.C_Str();
		}
		else {
			sprintf(subMeshName, "Mesh%d", i);
			meshName = subMeshName;
		}
		// write out name first
		auto nameArray = out.StoreNewArray<char>(strlen(meshName) + 1);
		mbinfo->name = nameArray.get();
		memcpy(nameArray->First(), meshName, nameArray->count);
		// followed by the interleaved vertex data
		{
			mbinfo->vertices = out.StoreNewArray<char>(sizeOfVertexBuffer).get();
			char* vertDataOut = mbinfo->vertices->First();
			// Pos
			AABB meshAABB;
			meshAABB.min = vec3{ std::numeric_limits<float>::infinity() };
			meshAABB.max = vec3{ -std::numeric_limits<float>::infinity() };
			for (uint32_t i = 0; i < mbinfo->numVerts; ++i) {
				vec3 vpos = (vec3&)aiMsh->mVertices[i];
				meshAABB.include(vpos);
				*(vec3*)(vertDataOut + i * vertexSize) = vpos;
			}
			uint8_t offset = c_posSize;

			//if (g_options.centerMeshes) {
			//	// Center mesh in local space
			//	for (uint32_t i = 0; i < mbinfo->numVerts; ++i) {
			//		vec3 vpos = (vec3&)aiMsh->mVertices[i];
			//		*(vec3*)(vertDataOut + i * vertexSize) = vpos - sceneCenter;
			//	}
			//
			//	mbinfo->bboxExtents = glm::max(glm::abs(min - sceneCenter), glm::abs(max - sceneCenter)) / 2.f;
			//}
			//else 
			{
				mbinfo->meshExtents = meshAABB;
			}
			//Normals
			if (mbinfo->hasNormals) {
				for (uint32_t i = 0; i < mbinfo->numVerts; ++i) {
					*(aiVector3D*)(vertDataOut + i * vertexSize + offset) = aiMsh->mNormals[i];
				}
				offset += 3 * sizeof(float);
				if (mbinfo->hasTgts) {
					for (uint32_t i = 0; i < mbinfo->numVerts; ++i) {
						*(aiVector3D*)(vertDataOut + i * vertexSize + offset) = aiMsh->mTangents[i];
					}
					offset += 3 * sizeof(float);
				}
			}
			// UV & COLORS
			for (uint8_t k = 0; k < mbinfo->numColors; k++) {
				for (uint32_t i = 0; i < mbinfo->numVerts; ++i) {
					*(aiColor4D*)(vertDataOut + i * vertexSize + offset) = aiMsh->mColors[k][i];
				}
				offset += 4 * sizeof(float);
			}
			for (uint8_t k = 0; k < mbinfo->numUVs; k++) {
				for (uint32_t i = 0; i < mbinfo->numVerts; ++i) {
					*(aiVector2D*)(vertDataOut + i * vertexSize + offset) = *reinterpret_cast<aiVector2D*>(aiMsh->mTextureCoords[k] + i);
				}
				offset += 2 * sizeof(float);
			}
			// Bones
			for (int i = 0; i < aiMsh->mNumBones; i++) {
				//aiMsh->mBones
				//const aiBone* bone = aiMsh->mBones[i];
				////bone->mWeights->
			}
		}
		// Indices
		const uint8_t c_idxSize = mbinfo->getIndexBytes();
		const uint32_t sizeOfIndexBuffer = mbinfo->numIndices * c_idxSize;

		mbinfo->indices = out.StoreNewArray<char>(sizeOfIndexBuffer).get();
		char* idxDataOut = mbinfo->indices->First();
		for (unsigned i = 0; i < aiMsh->mNumFaces; i++) {
			BASSERT_MSG(aiMsh->mFaces[i].mNumIndices == 3, "Only loading of triangle faces supported currently");
			const uint8_t c_faceSize = c_idxSize * 3;
			if (c_idxSize == 2) {
				uint16_t idx[3];
				idx[0] = aiMsh->mFaces[i].mIndices[0];
				idx[1] = aiMsh->mFaces[i].mIndices[1];
				idx[2] = aiMsh->mFaces[i].mIndices[2];
				memcpy(idxDataOut + i * c_faceSize, idx, c_faceSize);
			}
			else {
				memcpy(idxDataOut + i * c_faceSize, aiMsh->mFaces[i].mIndices, c_faceSize);
			}
		}

		// Generate UV1 for lightmapping
		//Thekla::Atlas_Options atlasOptions;
		//Thekla::atlas_set_default_options(&atlasOptions);
		//Thekla::Atlas_Error err;
		//Thekla::Atlas_Input_Mesh imesh;
		//auto outputMesh = Thekla::atlas_generate(, &atlasOptions, &err);
	}
}

void WriteNodes(Blob &out, Blob::StorePtr<MeshHeader> header, const aiScene * scene)
{
	header->nodes = out.StoreNew<MeshNodes>().get();
	uint32_t nodeCount = 0;
	std::deque<aiNode*> nodes{ scene->mRootNode };
	while (nodes.size() > 0) {
		aiNode* node = nodes.front();
		for (unsigned i = 0; i < node->mNumChildren; i++) {
			nodes.push_back(node->mChildren[i]);
		}
		nodes.pop_front();
		nodeCount++;
	}

	auto transforms = out.StoreNewArray<mat4>(nodeCount);
	header->nodes->transforms = transforms.get();
	auto parents = out.StoreNewArray<uint32_t>(nodeCount);
	header->nodes->parents = parents.get();
	auto nameHashes = out.StoreNewArray<hash32_t>(nodeCount);
	header->nodes->nameHashes = nameHashes.get();
	auto names = out.StoreNew< InplaceOffsetArray<char> >();
	header->nodes->names = names.get();
	names->Reserve(out, nodeCount);
	// work out scene extents
	AABB sceneAABB;
	vec3 obbOut[8];
	sceneAABB.min = vec3{ std::numeric_limits<float>::infinity() };
	sceneAABB.max = vec3{ -std::numeric_limits<float>::infinity() };
	//
	uint32_t nodeI = 0;
	nodes.push_back(scene->mRootNode);
	parents->Set(0) = 0;
	while (nodes.size() > 0) {
		aiNode* node = nodes.front();
		// node details
		auto transformDst = transforms->First() + nodeI;
		memcpy(transformDst, &node->mTransformation.Transpose(), sizeof(mat4));
		nameHashes->Set(nodeI) = HashString(node->mName.C_Str(), node->mName.length);
		names->PushBack((char*)out.Store(node->mName.C_Str()));
		// set mesh node indices
		for (unsigned i = 0; i < node->mNumMeshes; i++) {
			MeshBufferInfo* mbinfo = header->subMeshes[node->mMeshes[i]];
			mbinfo->nodeIdx = nodeI;
			GetOBBVerts(obbOut, *transformDst, mbinfo->meshExtents);
			for (int vIdx = 0; vIdx < 8; vIdx++)
				sceneAABB.include(obbOut[vIdx]);
		}
		//
		auto qsize = nodes.size() - 1;
		for (unsigned i = 0; i < node->mNumChildren; i++) {
			// Store parent idx for child nodes
			parents->Set(nodeI + qsize + i + 1) = nodeI;
			nodes.push_back(node->mChildren[i]);
		}
		nodes.pop_front();
		nodeI++;
	}
	names->SetEnd(out.DataEnd());
	//
	header->sceneExtents = sceneAABB;
}

struct sPropMap {
	const char*			inProp;
	const char*			outProp;
	aiPropertyTypeInfo	type;
	char				valCount; // number of values for this prop
	const char*			addDefine;
};

static sPropMap c_propMap[] = {
	{ "$clr.diffuse" , "u_color", aiPTI_Float, 3, nullptr},
	{ "$raw.DiffuseColor|file" , "u_albedoMap", aiPTI_String, 1, "ALBEDO_MAP"},
	{ "$raw.NormalMap|file" , "u_normalMap", aiPTI_String, 1, "NORMAL_MAP"},
	{ "$raw.NormalMap|file" , nullptr, aiPTI_String, 1, "NORMAL_MAP_RG"},
};

bool WriteMaterialFile(aiMaterial* mat)
{
	auto filename = mat->GetName();
	filename.Append(".material");
	auto outFile = fopen(filename.C_Str(), "wb");
	if (!outFile) {
		BOLTS_LERROR() << BOLTS_LOG_SCOPE " Could't open output file for writing: " << filename.C_Str();
		return false;
	}

	using namespace rapidjson;
	Document d;
	auto& a = d.GetAllocator();
	d.SetObject();
	d.AddMember("base", "Phong#shadowed", a);
	Value defines(kArrayType);
	//	"defines": ["SHADOWS", "ALBEDO_MAP", "NORMAL_MAP", "ALPHA_TEST"],
	defines.PushBack("SHADOWS",a);
	Value params(kObjectType);
	for (int i = 0; i < mat->mNumProperties; i++) {
		auto& prop = *mat->mProperties[i];
		auto& propName = prop.mKey;

		BOLTS_LINFO() << BOLTS_LOG_SCOPE " Mat " << filename.C_Str() <<  " | Prop " << propName.C_Str() << " | Type " << prop.mType << " | Bytes " << prop.mDataLength;
		if (prop.mType == aiPTI_Float) {
			if (prop.mDataLength == 4)
				BOLTS_LINFO() << BOLTS_LOG_SCOPE " Float " << *((float*)prop.mData);
			if (prop.mDataLength == 12)
				BOLTS_LINFO() << BOLTS_LOG_SCOPE " Vec3 " << *((vec3*)prop.mData);
		}
		if (prop.mType == aiPTI_String) {
			// The string is stored as 32 but length prefix followed by zero-terminated UTF8 data
			BOLTS_LINFO() << BOLTS_LOG_SCOPE " String " << (prop.mData + 4);
		}
		if (prop.mType == aiPTI_Buffer && prop.mDataLength == 4) {
			char chars[5];
			chars[4] = '\0';
			uint32_t val;
			memcpy(chars, prop.mData, 4);
			memcpy(&val, prop.mData, 4);
			// The string is stored as 32 but length prefix followed by zero-terminated UTF8 data
			BOLTS_LINFO() << BOLTS_LOG_SCOPE " Buffer Chars " << chars << " Int " << val;
		}

		for (auto& m : c_propMap)
			if (m.type == prop.mType && strcmp(m.inProp, prop.mKey.C_Str()) == 0) 
			{
				Value v;
				switch (m.type) {
					case aiPTI_Float: {
						if (m.valCount == 1) {
							v.SetFloat(*((float*)prop.mData));
						}
						else {
							v.SetArray();
							for (int vi = 0; vi < (prop.mDataLength / 4); vi++) {
								v.PushBack(*((float*)(prop.mData + (vi*4))), a);
							}
						}
						break;
					}
					case aiPTI_String: {
						auto len = *(uint32_t*)(prop.mData + 4);
						const char* texPath = prop.mData + 4;
						const char* texFilename = filenameFromPath(texPath, len);
						BASSERT(texFilename);
						v.SetString(texFilename, a);
						break;
					}
				}
				if (!v.Empty() && m.outProp) {
					params.AddMember(Value(m.outProp, a), v, a);
				}
				if (m.addDefine)
					defines.PushBack(Value(m.addDefine,a), a);
				BOLTS_LINFO() << BOLTS_LOG_SCOPE " Saved";
			}
	}
	d.AddMember("defines", defines, a);
	d.AddMember("params", params, a);

	// write file
	char writeBuffer[65536];
	FileWriteStream os(outFile, writeBuffer, sizeof(writeBuffer));
	PrettyWriter<FileWriteStream> writer(os);
	d.Accept(writer);
	fclose(outFile);
	return true;
}

void WriteMaterials(Blob &out, Blob::StorePtr<MeshHeader> header, const aiScene * scene)
{
	BASSERT(scene->HasMaterials());
	header->materials = out.StoreNew<MeshMaterials>().get();
	auto names = out.StoreNew< InplaceOffsetArray<char> >();
	header->materials->names = names.get();
	names->Reserve(out, scene->mNumMaterials);
	//
	for (int i = 0; i < scene->mNumMaterials; i++) {
		aiMaterial* mat = scene->mMaterials[i];
		if (g_options.mat.exp)
			WriteMaterialFile(mat);
		aiString name = mat->GetName();
		names->PushBack((char*)out.Store(name.C_Str()));
	}
	names->SetEnd(out.DataEnd());
}

void WriteAnimation(Blob &out, Blob::StorePtr<AnimHeader> header, const aiScene* scene)
{
	// check that all clips are for the same skeleton
	bool everythingOK = true;
	for (int i = 0; i < scene->mNumAnimations; i++) {
		const aiAnimation* aiAnim = scene->mAnimations[i];
		if (i == 0) {
			header->boneNameHashes = out.StoreNewArray<hash32_t>( aiAnim->mNumChannels).get();
			for (int boneIdx = 0; boneIdx < aiAnim->mNumChannels; boneIdx++) {
				const char* nodeName = aiAnim->mChannels[boneIdx]->mNodeName.C_Str();
				//TODO: This API is so bad !
				header->boneNameHashes->Set(boneIdx) = HashString(nodeName); 
			}
		}
		else {
			everythingOK &= (header->boneNameHashes->count == aiAnim->mNumChannels);
		}
	}
	BASSERT(everythingOK);
	if (!everythingOK) {
		BOLTS_LERROR() << BOLTS_LOG_SCOPE " Not all animation clips match the same skeleton";
		return;
	}
	header->clips = out.StoreNewArray<AnimClip>(scene->mNumAnimations).get();
	// count total number of tracks & frames
	int numTotalTracks = 0;
	int numTotalKeys = 0;
	for (int i = 0; i < scene->mNumAnimations; i++) {
		const aiAnimation* aiAnim = scene->mAnimations[i];
		auto& animClip = header->clips->Set(i);
		//
		unsigned trackCount = 0;
		for (int boneIdx = 0; boneIdx < aiAnim->mNumChannels; boneIdx++) {
			unsigned trackKeyCount = aiAnim->mChannels[boneIdx]->mNumPositionKeys;
			trackKeyCount = max(aiAnim->mChannels[boneIdx]->mNumRotationKeys, trackKeyCount);
			trackKeyCount = max(aiAnim->mChannels[boneIdx]->mNumScalingKeys, trackKeyCount);
			numTotalKeys += trackKeyCount;
			if (trackKeyCount > 0)
				trackCount++;
		}
		BASSERT(trackCount != 0);
		//
		auto& clipName = aiAnim->mName;
		animClip.nameHash = HashString(clipName.C_Str());
		memcpy(animClip.clipName, clipName.C_Str(), min(clipName.length + 1, (unsigned)countof(animClip.clipName) - 1));
		animClip.clipName[countof(animClip.clipName) - 1] = '\0';
		//
		BASSERT(aiAnim->mTicksPerSecond != 0.f);
		animClip.frameDuration = 1.0 / aiAnim->mTicksPerSecond;
		animClip.numFrames = (unsigned)aiAnim->mDuration + 1;
		animClip.duration = aiAnim->mDuration * animClip.frameDuration;
		animClip.firstTrack = numTotalTracks;
		numTotalTracks += trackCount;
		animClip.endTrack = numTotalTracks;
	}
	//
	header->tracks = out.StoreNewArray<AnimTrack>(numTotalTracks).get();
	header->transforms = out.StoreNewArray<AnimTransform>(numTotalKeys).get();
	// storage alloc done
	const int numBones = header->boneNameHashes->count;
	const auto& bones = *header->boneNameHashes;
	auto tracksOut = header->tracks.get()->First();
	auto transformsOut = header->transforms.get()->First();
	unsigned currentTrack = 0;
	unsigned currentTrf = 0;
	for (unsigned animIdx = 0; animIdx < scene->mNumAnimations; animIdx++) {
		const aiAnimation* aiAnim = scene->mAnimations[animIdx];
		const auto& animClip = header->clips->Get(animIdx);
		BASSERT(currentTrack == animClip.firstTrack);
		for (int boneIdx = 0; boneIdx < aiAnim->mNumChannels; boneIdx++) {
			const auto aiTrack = aiAnim->mChannels[boneIdx];
			if (aiTrack->mNumPositionKeys <= 0)
				continue;
			auto trackOut = tracksOut[currentTrack++];
			trackOut.firstTransform = currentTrf;
			currentTrf += animClip.numFrames;
			// find bone
			trackOut.boneIdx = -1;
			hash32_t boneHash = HashString(aiTrack->mNodeName.C_Str());
			for (int i = 0; i < numBones; i++)
				if (bones[i] == boneHash)
					trackOut.boneIdx = i;
			BASSERT(trackOut.boneIdx != -1);
			// key data
			const auto aiPositions = aiTrack->mPositionKeys;
			const auto aiRotations = aiTrack->mRotationKeys;
			const auto aiScales = aiTrack->mScalingKeys;
			const int maxPos = aiTrack->mNumPositionKeys;
			const int maxRot = aiTrack->mNumRotationKeys;
			const int maxScale = aiTrack->mNumScalingKeys;
			int posIdx = 0;
			int rotIdx = 0;
			int scaleIdx = 0;
			unsigned keyCount = animClip.numFrames;
			double currentTime = 0.0; // current time, in frames ( weird, I know )
;			for (int keyIdx = 0; keyIdx < keyCount; keyIdx++) {
				auto& transformOut = transformsOut[trackOut.firstTransform + keyIdx];
				
				struct bla {
					static float getAlpha(double a, double b, double t) {
						return (float) ((t - a) / (b - a));
					}
				};

				while (currentTime >= aiPositions[posIdx].mTime && posIdx < maxPos)
					posIdx++;
				if (posIdx >= maxPos)
					transformOut.pos = ToVec3(aiPositions[posIdx - 1].mValue);
				else {
					float alpha = bla::getAlpha(aiPositions[posIdx - 1].mTime, aiPositions[posIdx].mTime, currentTime);
					transformOut.pos = glm::mix(ToVec3(aiPositions[posIdx-1].mValue), ToVec3(aiPositions[posIdx].mValue), alpha);
				}

				while (currentTime >= aiRotations[rotIdx].mTime && rotIdx < maxRot)
					rotIdx++;
				if (rotIdx >= maxRot)
					transformOut.rot = ToQuat(aiRotations[rotIdx - 1].mValue);
				else {
					float alpha = bla::getAlpha(aiRotations[rotIdx - 1].mTime, aiRotations[rotIdx].mTime, currentTime);
					transformOut.rot = glm::lerp(ToQuat(aiRotations[rotIdx - 1].mValue), ToQuat(aiRotations[rotIdx].mValue), alpha);
				}

				while (currentTime >= aiScales[scaleIdx].mTime && scaleIdx < maxScale)
					scaleIdx++;
				if (scaleIdx >= maxScale)
					transformOut.scale = ToVec3(aiScales[scaleIdx - 1].mValue);
				else {
					float alpha = bla::getAlpha(aiScales[scaleIdx - 1].mTime, aiScales[scaleIdx].mTime, currentTime);
					transformOut.scale = glm::mix(ToVec3(aiScales[scaleIdx - 1].mValue), ToVec3(aiScales[scaleIdx].mValue), alpha);
				}

				currentTime += 1.0;
			}
		}
		BASSERT(currentTrack == animClip.endTrack);
	}
}

int main(int argc, char* argv[])
{
	// bolts init stuff
#ifdef DEBUG
	Core::assert::AddAssertHandler(Core::assert::BreakpointAH);
#endif
	Core::assert::AddAssertHandler(Core::assert::CoutAH);
	//serialization::Init();

	//
	std::string inPath;
	std::string outPath;
	std::string outAnimPath;
	std::string optPath;

	if (argc == 1)	{
		printf("Convert mesh in FBX, OBJ or Colllada formats to Bolts .mesh files. Usage: MeshConverter.exe <path-to-input-mesh> <path-to-output-mesh> [-o <path-to-options-json>]");
		return 1;
	}
	inPath = argv[1];
	BASSERT(!inPath.empty());
	if (argc >= 3)
		outPath = argv[2];
	// parse optional params
	for (int i = 1; i < argc; i++) {
		if (strcmp("-o", argv[i]) == 0) {
			optPath = argv[++i];
		}
	}
	// try parse options
	if (optPath.empty())
		optPath = inPath + ".json";
	auto f = fopen(optPath.c_str(), "rb");
	if (!f) {
		BOLTS_LINFO() << BOLTS_LOG_SCOPE " Can't open options file: " << optPath;
	}
	else
	{
		rapidjson::Document d;
		fseek(f, 0, SEEK_END);
		long fsize = ftell(f);
		fseek(f, 0, SEEK_SET);  //same as rewind(f);
		char *json = new char[fsize + 1];
		fread(json, fsize, 1, f);
		json[fsize] = 0;
		d.Parse(json);

		g_options.Load(d);

		fclose(f);
	}
	// test input file exists
	auto fhandle = fopen(inPath.c_str(), "rb");
	if (fhandle)
		fclose(fhandle);
	else{
		printf("Couldn't open input file");
		return 2;
	}

	//Test open FBX
	if (strstr(inPath.c_str(), ".fbx") ||
		strstr(inPath.c_str(), ".FBX"))
	{
		//TODO: mmap file ?
		auto fileSize = Bolts::IO::fileSize(inPath.c_str());
		unsigned char* fileData = new unsigned char[fileSize];
		Bolts::IO::readFile(inPath.c_str(), fileData, fileSize, Bolts::IO::EF_NONE);

		//TODO: this does another copy, just make it read the data you give it
		auto pFbxScene = ofbx::load(fileData, (int)fileSize, (size_t)ofbx::LoadFlags::TRIANGULATE);
		delete[] fileData;

		// if the import failed, report it
		if (!pFbxScene)
		{
			BOLTS_LERROR() << BOLTS_LOG_SCOPE" Error loading: " << inPath;
			BOLTS_LERROR() << BOLTS_LOG_SCOPE"	" << ofbx::getError();
			return 3;
		}
		BOLTS_LINFO() << BOLTS_LOG_SCOPE " Exporting from: " << inPath;

		bool exportMeshes = pFbxScene->getMeshCount() > 0;
		BASSERT(exportMeshes);
		// default out path
		if (outPath.empty()) {
			outPath = inPath;
			int extStart = outPath.rfind('.');
			int extLen = outPath.length() - extStart;
			outPath.replace(extStart, extLen, exportMeshes ? ".mesh" : ".anim");
		}

		auto outFile = fopen(outPath.c_str(), "wb");
		if (!outFile) {
			BOLTS_LERROR() << BOLTS_LOG_SCOPE " Could't open output file for writing: " << outPath;
			return 4;
		}

		Blob out;
		if (exportMeshes && !g_options.forceExportAnimations) {
			BOLTS_LINFO() << BOLTS_LOG_SCOPE " Exporting meshes";
			auto header = out.StoreNew<MeshHeader>();
			WriteMeshes(out, header, pFbxScene); // Meshes have to be first
			WriteNodes(out, header, pFbxScene);
			WriteMaterials(out, header, pFbxScene);
		}
		fwrite(out.Data(), out.Size(), 1, outFile);
		fclose(outFile);

		pFbxScene->destroy();
	}
	else {

		// import file using Assimp
		Assimp::Importer importer;
		unsigned importFlags = aiProcess_JoinIdenticalVertices | aiProcess_RemoveComponent | aiProcess_Triangulate;
		if (g_options.generateNormals)
			importFlags |= aiProcess_GenNormals;
		if (g_options.generateTangents)
			importFlags |= aiProcess_CalcTangentSpace;
		if (g_options.flipUVs)
			importFlags |= aiProcess_FlipUVs;
		importer.SetPropertyInteger(AI_CONFIG_PP_RVC_FLAGS, aiComponent_LIGHTS | aiComponent_CAMERAS); // Ignore these things in the export
		importer.SetPropertyInteger(AI_CONFIG_IMPORT_NO_SKELETON_MESHES, 1);
		importer.SetPropertyInteger(AI_CONFIG_IMPORT_COLLADA_USE_COLLADA_NAMES, 1);
		//importer.SetPropertyBool(AI_CONFIG_IMPORT_FBX_PRESERVE_PIVOTS, false);
		const aiScene* scene = importer.ReadFile(inPath.c_str(), importFlags);

		// if the import failed, report it
		if (!scene)
		{
			BOLTS_LERROR() << BOLTS_LOG_SCOPE" Error loading: " << inPath;
			BOLTS_LERROR() << BOLTS_LOG_SCOPE"	" << importer.GetErrorString();
			return 3;
		}
		BOLTS_LINFO() << BOLTS_LOG_SCOPE " Exporting from: " << inPath;

		bool exportMeshes = scene->HasMeshes();
		// default out path
		if (outPath.empty()) {
			outPath = inPath;
			int extStart = outPath.rfind('.');
			int extLen = outPath.length() - extStart;
			outPath.replace(extStart, extLen, exportMeshes ? ".mesh" : ".anim");
		}

		auto outFile = fopen(outPath.c_str(), "wb");
		if (!outFile) {
			BOLTS_LERROR() << BOLTS_LOG_SCOPE " Could't open output file for writing: " << outPath;
			return 4;
		}

		Blob out;
		if (exportMeshes && !g_options.forceExportAnimations) {
			BOLTS_LINFO() << BOLTS_LOG_SCOPE " Exporting meshes";
			auto header = out.StoreNew<MeshHeader>();
			WriteMeshes(out, header, scene); // Meshes have to be first
			WriteNodes(out, header, scene);
			WriteMaterials(out, header, scene);
		}
		else {
			BOLTS_LINFO() << BOLTS_LOG_SCOPE " Exporting animations";
			auto header = out.StoreNew<AnimHeader>();
			WriteAnimation(out, header, scene);
		}
		fwrite(out.Data(), out.Size(), 1, outFile);
		fclose(outFile);
	}
}