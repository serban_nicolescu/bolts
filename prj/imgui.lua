local lProjectName = "ImGui"

_deps[lProjectName] = function (header_only)
	header_only = header_only or false
	
	includedirs 
	{ 
		bCfg.depPath .. "ImGui/",
	}
	
	if not header_only then	
		links {
			lProjectName,
		}
	end
end

project (lProjectName)
	location ( bCfg.prjPath )
	kind "StaticLib"
	language "C++"
	
	files 
	{ 
		bCfg.depPath .. "ImGui/*.cpp", 
		bCfg.depPath .. "ImGui/*.h",
	}
	includedirs 
	{ 
		bCfg.depPath .. "ImGui/",
	}
	
	--forceincludes { "windows.h" }