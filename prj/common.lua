_deps = {}

bCfg = {
	depPath = "../dependencies/",
	outPath = "../bin/" .. _ACTION .. "/",
	prjPath = _ACTION,
	objPath = "../obj/" .. _ACTION,
}
bCfg.libPath = bCfg.depPath .. "libs/" .. _ACTION .. "/"
bCfg.libOutPath = bCfg.depPath .. "libs/" .. _ACTION .. "/%{prj.name}/%{cfg.shortname}"
bCfg.boostPath = bCfg.depPath .. "boost_1_69_0/"
-- Utility defines
bUtils = {
	action = _ACTION or ""
}
function bUtils.GetOutputName( baseName )
	return baseName .. '_%{cfg.shortname}'
end

function bUtils.GetOutDirName( baseName )
	return baseName .. '_%{cfg.shortname}'
end

function bUtils.IsVSAction()
	return _ACTION and string.find( _ACTION, "vs")
end

function bUtils.GetActionOutPath() --Returns the path where the projects for the current action would be generated
	--NOTE: Should match values in the project generation batch files
	if (bUtils.IsVSAction()) then
		return _ACTION
	else
		return ""
	end
end

_deps["rapidjson"] = function ()
	includedirs 
	{ 
		bCfg.depPath .. "rapidjson/include/"
	}
end

_deps["boost"] = function ()
	includedirs
	{
		bCfg.boostPath,
	}
	
	libdirs
	{
		bCfg.boostPath .. "/lib/",
		bCfg.depPath .. "/libs/boost/",
	}
end

-- disable Visual Studio Just My Code Debugging ( big perf cost on Debug )
require "vstudio"
premake.override(premake.vstudio.vc2010.elements, "clCompile", function(oldfn, cfg)
  local calls = oldfn(cfg)
  table.insert(calls, function(cfg)
    premake.vstudio.vc2010.element("SupportJustMyCode", nil, "false")
  end)
  return calls
end)