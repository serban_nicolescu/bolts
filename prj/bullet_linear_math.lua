local lProjectName = "BulletLinearMath"

_deps[lProjectName] = function (header_only)
	header_only = header_only or false
	includedirs 
	{ 
		bCfg.depPath .. "bullet/src/",
	}
	if not header_only then	
		links {
			lProjectName,
		}
	end
end

project (lProjectName)
	location ( bCfg.prjPath )
	kind "StaticLib"
	language "C++"
	
	-- Only include a minimal subset to eliminate dependencies
	files 
	{ 
		bCfg.depPath .. "bullet/src/LinearMath/**.h", 
		bCfg.depPath .. "bullet/src/LinearMath/**.cpp",
	}
	
	includedirs 
	{ 
		bCfg.depPath .. "bullet/src/",
	}
	
	dofile "bullet.lua"