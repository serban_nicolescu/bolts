local lProjectName = "BoltsCore"

_deps[lProjectName] = function (header_only)
	header_only = header_only or false
	
	includedirs 
	{ 
		bCfg.depPath .. lProjectName .. "/include/",
		bCfg.depPath .. "BitSquid/Foundation/",
		bCfg.depPath .. "glm/",
	}
	
	if not header_only then	
		links {
			lProjectName,
		}
	end
end

external "BoltsCore"
	location ( bCfg.depPath .. "%{prj.name}/prj/" .. _ACTION )
	kind "StaticLib"
	language "C++"
	uuid "5f4a4b6e-7904-4450-85e0-60f601f927af"
	