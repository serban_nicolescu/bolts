local lProjectName = "SDL2"

_deps[lProjectName] = function (header_only)
	header_only = header_only or false
	
	includedirs 
	{ 
		bCfg.depPath .. lProjectName .. "/include/",
	}
	
	if not header_only then
		
		links 
		{ 
			"opengl32", 
			"glu32", 
			"SDL2",
			"SDL2main"
		}
		libdirs
		{
			bCfg.depPath .. "SDL2/lib/x86/",
		}
	end
end

project (lProjectName)
	location ( bCfg.prjPath )
	kind "StaticLib"
	language "C++"
	
	-- Only include a minimal subset to eliminate dependencies
	print(bCfg.depPath .. lProjectName .. "/include/**.h")
	files 
	{ 
		bCfg.depPath .. lProjectName .. "/include/**.h",
		bCfg.depPath .. lProjectName .. "/include/**.hpp",
	}
	
	includedirs 
	{ 
		bCfg.depPath .. lProjectName .. "/include/",
	}

	defines { }