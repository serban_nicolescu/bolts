local lProjectName = "microprofile"
_deps[lProjectName] = function (header_only)
	header_only = header_only or false
	
	includedirs 
	{ 
		bCfg.depPath .. "microprofile/",
	}
	
	defines { "MICROPROFILE_GPU_TIMERS_GL" }
	
	if not header_only then	
		links {
			lProjectName,
			"Ws2_32.lib"
		}
	end
end

project(lProjectName)
	location ( bCfg.prjPath )
	kind "StaticLib"
	language "C++"
	
	files 
	{ 
		bCfg.depPath .. "microprofile/*.cpp", 
		bCfg.depPath .. "microprofile/*.h" 
	}
	includedirs 
	{ 
		bCfg.depPath .. "microprofile/*.h",
		bCfg.depPath .. "glew/include/",
	}
	defines { "MICROPROFILE_USE_CONFIG", "MICROPROFILE_GPU_TIMERS_GL" }