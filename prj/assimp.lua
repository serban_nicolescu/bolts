_deps["Assimp"] = function (header_only)
	header_only = header_only or false
	
	includedirs 
	{ 
		bCfg.depPath .. "assimp-5.0.0/include/",
	}
	
	if not header_only then
		_deps["zlib"]()
	
		links {
			"Assimp",
		}
	end
end

project "Assimp"
	kind "StaticLib"
	language "C++"
	
	defines{ 
		"ASSIMP_BUILD_NO_C4D_IMPORTER", 
		"ASSIMP_BUILD_NO_STEP_IMPORTER", 
		"ASSIMP_BUILD_NO_IFC_IMPORTER",
		"ASSIMP_BUILD_NO_ASSBIN_EXPORTER",
		"ASSIMP_BUILD_NO_3MF_IMPORTER",
		"ASSIMP_BUILD_NO_3MF_IMPORTER",
		"ASSIMP_BUILD_NO_OGRE_IMPORTER",
		"ASSIMP_BUILD_NO_OWN_ZLIB",
		"OPENDDL_STATIC_LIBARY"
	}
	
	files 
	{ 
		bCfg.depPath .. "assimp-5.0.0/include/assimp/**.h", 
		bCfg.depPath .. "assimp-5.0.0/code/**.cpp", 
		bCfg.depPath .. "assimp-5.0.0/code/**.h",
		--bCfg.depPath .. "assimp-5.0.0/contrib/**.cpp", 
		--bCfg.depPath .. "assimp-5.0.0/contrib/**.h", 
		--bCfg.depPath .. "assimp-5.0.0/contrib/**.c",
		
		bCfg.depPath .. "assimp-5.0.0/contrib/openddlparser/**.cpp", 
		bCfg.depPath .. "assimp-5.0.0/contrib/openddlparser/**.h", 
		bCfg.depPath .. "assimp-5.0.0/contrib/openddlparser/**.c",
		bCfg.depPath .. "assimp-5.0.0/contrib/stb_image/**.cpp", 
		bCfg.depPath .. "assimp-5.0.0/contrib/stb_image/**.h", 
		bCfg.depPath .. "assimp-5.0.0/contrib/stb_image/**.c",
		bCfg.depPath .. "assimp-5.0.0/contrib/utf8cpp/**.cpp", 
		bCfg.depPath .. "assimp-5.0.0/contrib/utf8cpp/**.h", 
		bCfg.depPath .. "assimp-5.0.0/contrib/utf8cpp/**.c",
		bCfg.depPath .. "assimp-5.0.0/contrib/unzip/**.cpp", 
		bCfg.depPath .. "assimp-5.0.0/contrib/unzip/**.h", 
		bCfg.depPath .. "assimp-5.0.0/contrib/unzip/**.c",
		bCfg.depPath .. "assimp-5.0.0/contrib/irrXML/**.cpp", 
		bCfg.depPath .. "assimp-5.0.0/contrib/irrXML/**.h", 
		bCfg.depPath .. "assimp-5.0.0/contrib/irrXML/**.c",
		-- bCfg.depPath .. "assimp/contrib/clipper/**.cpp", 
		-- bCfg.depPath .. "assimp/contrib/clipper/**.h", 
		-- bCfg.depPath .. "assimp/contrib/ConvertUTF/**.h",
		-- bCfg.depPath .. "assimp/contrib/ConvertUTF/**.c",
		-- bCfg.depPath .. "assimp/contrib/zlib/**.h",
		-- bCfg.depPath .. "assimp/contrib/zlib/**.c",
		-- bCfg.depPath .. "assimp/contrib/unzip/**.c",
		-- bCfg.depPath .. "assimp/contrib/unzip/**.h",
		-- bCfg.depPath .. "assimp/contrib/irrXML/**.h",
		-- bCfg.depPath .. "assimp/contrib/irrXML/**.cpp",
		-- bCfg.depPath .. "assimp/contrib/poly2tri/**.h",
		-- bCfg.depPath .. "assimp/contrib/poly2tri/**.cc",
		-- bCfg.depPath .. "assimp/contrib/poly2tri/**.cc",
	}
	includedirs 
	{ 
		bCfg.depPath .. "assimp-5.0.0/contrib/irrXML/",
		bCfg.depPath .. "assimp-5.0.0/contrib/unzip/",
		--bCfg.depPath .. "assimp-5.0.0/contrib/zlib/",
		bCfg.depPath .. "assimp-5.0.0/contrib/openddlparser/include",
		bCfg.depPath .. "assimp-5.0.0/contrib/rapidjson/include",
		bCfg.depPath .. "assimp-5.0.0/",
		bCfg.depPath .. "assimp-5.0.0/include/",
		bCfg.depPath .. "assimp-5.0.0/code/",
		bCfg.boostPath	
	}
	
	-- Temporarily disabled
	--pchsource ( bCfg.depPath .. "assimp/code/AssimpPCH.cpp")
	--pchheader ( "AssimpPCH.h")
	
	-- Configs
	configuration "Release*"
		floatingpoint "Fast"
		omitframepointer "On"
	configuration { "Release*", "windows" }
		vectorextensions "SSE2"
		buildoptions { 
			"/GS-",-- No buffer sec. checks
			"/Oi", -- Enable intrinsic functions
			"/Ob2" -- Inline any suitable
		}
	configuration {}
	
	_deps["zlib"](true);