_deps["Assimp"] = function (header_only)
	header_only = header_only or false
	
	includedirs 
	{ 
		bCfg.depPath .. "assimp-5.0.0/include/",
	}
	
	if not header_only then
		links {
			"Assimp",
			"IrrXML"
		}
	end
end

external "Assimp"
	location ( bCfg.depPath .. "assimp-5.0.0/code")
	kind "StaticLib"
	language "C++"
	uuid "449FCCFC-11B4-3CD8-9BC2-03029B4C58F1"
	
external "IrrXML"
	location ( bCfg.depPath .. "assimp-5.0.0/contrib/irrXML")
	kind "StaticLib"
	language "C++"
	uuid "F36EF524-B368-3F71-9074-85DA0ABDB524"
	
	
	
	
	