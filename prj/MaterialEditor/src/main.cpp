#include <System/system_win32.h>
#include <bolts_binaryserialization.h>
#include <bolts_hash.h>
#include <Rendering/GL/GL35Backend.h>
#include <Profiler.h>
#include <IO/FileProviderImpl.h>
#include <IO/FileIO.h>

#include <chrono>
#include <fstream>
#include <iostream>

#include <Assets/AssetManager.h>
#include <Assets/Loaders_Builtin.h>
#include <Rendering/GPUProgram.h>
#include <Rendering/MaterialNew.h>
#include <Rendering/Texture.h>
#include <Rendering/Mesh.h>
#include <Helpers/MeshBuilder.h>
#include <Render/DebugDraw.h>

#include <Assets/Loader_Shaders.h>
#include <Assets/Loader_Materials.h>

#include <Helpers/imgui_impl_bolts.h>
#include <imgui.h>
#include "ImGuizmo/ImGuizmo.h"
#include "GUI/Properties.h"
#include <IconsKenney.h>

#include <rapidxml.hpp>
#include <rapidxml_print.hpp>

#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/prettywriter.h>


using namespace std;
using namespace Bolts;
using namespace Bolts::System;

static DebugDrawer* g_debugDrawer = nullptr;
namespace Bolts
{
	DebugDrawer* DebugDraw() { return g_debugDrawer; }
};

class App :
	public Input::iWindowEventListener,
	public Input::iMouseEventListener,
	public Input::iKeyEventListener,
	public Input::iCharEventListener
{
public:
	App() {
		m_windowSize = { 600,600 };

		Win32System::Get().InitApp();
		m_mainWindow = Win32System::Get().NewWindow("BoltsMatEdit", m_windowSize.x, m_windowSize.y, System::Window::S_TITLE | System::Window::S_RESIZEABLE | System::Window::S_MAXIMIZE | System::Window::S_CLOSE);

		Win32System::Get().RegisterWindowEventListener(this);
		Win32System::Get().RegisterMouseEventListener(this);
		Win32System::Get().RegisterKeyEventListener(this);
		Win32System::Get().RegisterCharEventListener(this);

		m_renderBackend = new Rendering::BackendOpenGL35();
		m_renderBackend->Init();
		m_renderer.InitRenderer(m_renderBackend);
		m_assetManager.SetCurrentCommandBuffer(m_renderBackend->CreateCommandBuffer());
		RegisterBuiltinAssets(m_assetManager);// register asset loaders
		m_renderer.InitBuiltinResources(m_assetManager);
		g_debugDrawer = m_renderer.GetDebugDrawer();
		// init UI
		ImGui_Init((void*)m_mainWindow, m_renderer.GetCommandBuffer(), "matEditorWindows.ini");

		InitEditor();

		Profiler::Init(m_renderBackend);
	}

	~App() {
		delete m_renderBackend;

		Win32System::Get().UnregisterWindowEventListener(this);
		Win32System::Get().UnregisterMouseEventListener(this);
		Win32System::Get().UnregisterKeyEventListener(this);

		Profiler::Shutdown();
	}

	bool AppUpdate()
	{
		Profiler::Get().FrameStart();

		static chrono::system_clock::time_point lastFrame = chrono::system_clock::now();

		chrono::duration<double> dtSec = chrono::system_clock::now() - lastFrame;
		lastFrame = chrono::system_clock::now();
		m_deltaSecs = (float)(dtSec.count());

		Win32System::Get().Update(m_mainWindow);

		bool running = !m_bExiting;
		if (running)
		{
			//snprintf(title, sizeof(title), "Shooty - FPS: %.2f - MS: %.2f", 1.0f / m_deltaSecs, m_deltaSecs * 1000.f);
			//Win32System::Get().SetWindowTitle(m_mainWindow, title);

			Bolts::IO::CheckForSourceChanges();

			auto size = Win32System::Get().GetWindowSize(m_mainWindow);
			ImGui_NewFrame(m_deltaSecs, m_mainWindow, size.x, size.y, size.x, size.y);
			ImGuizmo::BeginFrame();

			m_renderer.StartNewFrame();

			running = Update(m_deltaSecs);

			m_renderer.TweakSettings();

			m_renderer.GetBackend()->ExecuteResourceCommands(m_assetManager.GetCommandBuffer());
			m_renderer.Render(m_assetManager, m_windowSize, {});

			{
				ImGui::Render();
				ImGui_RenderDrawData(*m_renderBackend);
			}
			m_renderer.EndFrame();

			{
				Win32System::Get().SwapWindowBuffers();
				m_renderBackend->SetClearColor(vec4(0.7f, 0.9f, 0.9f, 1.0f));
				m_renderBackend->Clear();
			}
		}

		if (!running) {
			m_badTexture.reset();

			ImGui_Shutdown(m_renderer.GetCommandBuffer());
			m_renderBackend->ExecuteResourceCommands(m_assetManager.GetCommandBuffer());
			m_renderer.FlushResourceCommands();

			SaveSettings();
		}

		Profiler::Get().FrameEnd();
		Bolts::IO::FrameEnd();
		return running;
	}

	// input events

	void OnWindowEvent(Input::windowEvent_t event) override {
		switch (event.type) {
		case Input::WE_CLOSED:
			Win32System::Get().CloseWindow(event.windowHandle);
			m_bExiting = true;
			break;
		case Input::WE_RESIZED:
			m_windowSize = { (unsigned)event.value1, (unsigned)event.value2 };
			break;
		}
	}

	bool OnMouseEvent(Input::mouseEvent_t event) override {
		bool stop = ImGui_MouseEvent(event);
		if (stop)
			return true;
		m_mouseState.OnMouseEvent(event);
		return false;
	}

	bool OnKeyEvent(Input::keyEvent_t event) override {
		bool stop = ImGui_KeyEvent(event);
		if (stop)
			return true;
		m_keyState.OnKeyEvent(event);
		return false;
	}

	bool OnCharEvent(unsigned int c) {
		return ImGui_CharCallback(c);
	}

	// editor
	struct MatInstanceTab
	{
		char							m_matName[64] = "\0";
		Rendering::MaterialPtr			m_material;
		str_t							m_vs;
		str_t							m_fs;
		vec_t<str_t>					m_defines;
		bool							m_hasFocus;
		bool							m_hasChanges;
	};

	void OnMaterialChanged(MatInstanceTab& tab)
	{
		//TODO: save old contents
			
		if (tab.m_matName[0] == '/0') {
			tab.m_material = nullptr;
			return;
		}
		tab.m_material = m_assetManager.LoadAsset<Rendering::Material>( Bolts::detail::StringWrapper(tab.m_matName));
		if (tab.m_material) {
			assets::ShaderLoader::GetShaderProgramData(tab.m_material->GetProgramID(), tab.m_vs, tab.m_fs, tab.m_defines);
		}
		tab.m_hasChanges = false;
	}

	void ChangePreviewMesh(const char* newMesh)
	{
		m_previewMesh = m_assetManager.LoadAsset<Rendering::Mesh>(regHash_t(newMesh));
		auto& renderable = m_renderer.GetStaticMesh(m_previewRenderable);
		renderable.m_mesh = m_previewMesh;
		if (m_previewMesh && m_previewMesh->IsValid()) {
			float dist = 3.f;
			vec3 ext = m_previewMesh->aabb.ext();
			float maxExt = std::max(std::max(ext.x, ext.y), ext.z);
			m_previewCamera.SetPosition(m_previewMesh->aabb.max * dist);
			m_previewCamera.SetTarget(m_previewMesh->aabb.center());
			m_previewCamera.SetZFar(maxExt * dist * 2.8f);
		}
	}

	void RenderMaterialProps(MatInstanceTab& tab)
	{
		if (ImGui::InputText("Material", tab.m_matName, sizeof(tab.m_matName), ImGuiInputTextFlags_EnterReturnsTrue))
		{
			OnMaterialChanged(tab);
		}

		ImGui::SameLine();
		if (ImGui::Button(ICON_KI_SAVE)) {
			SaveTab(tab);
		}

		if (!tab.m_material || !tab.m_material->IsValid())
		{
			ImGui::Text("Invalid material asset");
			if (!tab.m_material)
				ImGui::Text("	Cannot find material with that name");
			else if (!tab.m_material->GetProgram())
				ImGui::Text("	Missing GPU Program");
			else if (!tab.m_material->GetProgram()->IsValid())
				ImGui::Text("	Invalid GPU Program");
			else
				ImGui::Text("	Unknown error");
			return;
		}
		ImGui::Separator();
		ImGui::TextDisabled("Base Material");
		{
			ImGui::SameLine();
			regHash_t baseMatName = tab.m_material->GetBaseMaterialID();
			if (ImGuiEdit("Base Material", &baseMatName)) {
				tab.m_material->SetBaseMaterialID(baseMatName.h);
			}
		}

		ImGui::TextDisabled("Shaders");
		ImGui::SameLine();
		ImGui::Text("%s | %s", tab.m_vs.c_str(), tab.m_fs.c_str());
		ImGui::TextDisabled("Defines");
		for (auto& def : tab.m_defines) {
			ImGui::SameLine();
			ImGui::Text("%s", def.c_str());
		}
		ImGui::SameLine();
		if (ImGui::SmallButton("+###editdefs"))
			ImGui::OpenPopup("Edit Defines");

		if (ImGui::BeginPopup("Edit Defines", ImGuiWindowFlags_AlwaysAutoResize)) {
			for (auto& def : tab.m_defines) {
				ImGui::PushID(def.c_str());
				if (ImGui::Button("X")) {
					try_remove(tab.m_defines, def);
					ImGui::PopID();
					break;
				}
				ImGui::SameLine();
				ImGui::Text("%s", def.c_str());
				ImGui::PopID();
			}
			static bool addDef = false;
			if (addDef) {
				char defineStr[64];
				defineStr[0] = '\0';
				if (ImGui::InputText("Define", defineStr, sizeof(defineStr), ImGuiInputTextFlags_EnterReturnsTrue)) {
					if (!Bolts::contains(tab.m_defines, defineStr)) {
						tab.m_defines.emplace_back(defineStr);
						addDef = false;
					}
				}
			}
			if (ImGui::SmallButton("+###newdefine")) {
				addDef = true;
			}
			ImGui::EndPopup();
		}

		ImGui::Separator();
		auto gpuProgram = tab.m_material->GetProgram();
		auto& programUniforms = gpuProgram->cbuffers[Rendering::GPUProgram::c_maxNumUniformBuffers - 1];
		auto& matParams = tab.m_material->EditParams();
		bool anyChanges = false;
		ImGui::Columns(2);

		for (int i = 0; i < programUniforms.numEntries; i++) {

			auto pParamInfo = matParams.TryGetInfo(programUniforms.names[i]);
			Rendering::ShaderParamType uniformType = programUniforms.types[i];
			hash32_t uniformNameHash = programUniforms.names[i];
			const char* uniformName = GetHashString(uniformNameHash);
			if (contains(m_hiddenProps, uniformName))
				continue;
			BASSERT(uniformName);

			ImGui::PushID(i);
			if (pParamInfo)
				ImGui::Text(uniformName);
			else {
				ImGui::TextDisabled(uniformName);
				if (ImGui::IsItemClicked(1))
					if (!contains(m_hiddenProps, uniformName))
						m_hiddenProps.emplace_back(uniformName);
			}

			ImGui::SameLine();
			ImGui::TextDisabled( Rendering::c_SPTNames[uniformType] );
			if (pParamInfo && uniformType != pParamInfo->type) {
				ImGui::SameLine();
				ImGui::Text(" !");
			}
			ImGui::NextColumn();

			if (!pParamInfo) {
				if (ImGui::Button("+")) {
					// Add param for uniform
					anyChanges = true;
					using namespace Rendering;
					switch (uniformType)
					{
					case SPT_FLOAT:
						matParams.Set( uniformNameHash, 0.f);
						break;
					case SPT_VEC2:
						matParams.Set(uniformNameHash, vec2());
						break;
					case SPT_VEC3:
						matParams.Set(uniformNameHash, vec3());
						break;
					case SPT_VEC4:
						matParams.Set(uniformNameHash, vec4());
						break;
					case SPT_TEX:
						matParams.Set(uniformNameHash, m_badTexture);
						break;
					}
				}
			}
			else {
				using namespace Rendering;
				char noLabel[8];
				snprintf(noLabel, countof(noLabel), "###%d", i);
				switch (uniformType)
				{
				case SPT_FLOAT:
					anyChanges |= ImGuiEdit(noLabel, (float*)matParams.EditDataUnsafe(pParamInfo->dataOffset), 0.f, 1.f);
					break;
				case SPT_VEC2:
					anyChanges |= ImGuiEdit(noLabel, (vec2*)matParams.EditDataUnsafe(pParamInfo->dataOffset));
					break;
				case SPT_VEC3:
					anyChanges |= ImGuiEdit(noLabel, (vec3*)matParams.EditDataUnsafe(pParamInfo->dataOffset));
					break;
				case SPT_VEC4:
					anyChanges &= ImGuiEdit(noLabel, (vec4*)matParams.EditDataUnsafe(pParamInfo->dataOffset));
					break;
				case SPT_TEX:
					auto texData = *(const MatParameterHolder::TextureParam*)matParams.GetDataUnsafe(pParamInfo->dataOffset);
					regHash_t texName = texData.tex ? regHash_t(texData.tex->GetAssetID()) : regHash_t{};
					if (ImGuiEdit(noLabel, &texName)) {
						auto newTexPtr = m_assetManager.LoadAsset<Rendering::Texture>(texName);
						if (newTexPtr->GetAssetState() == assets::EAS_READY) {
							matParams.Set(uniformNameHash, newTexPtr);
							anyChanges = true;
						}
						else {
							BOLTS_LWARNING() << "Error loading texture " << texName.s;
							if (newTexPtr->GetAssetState() == assets::EAS_NOT_FOUND)
								BOLTS_LWARNING() << "	Asset not found";
							if (newTexPtr->GetAssetState() == assets::EAS_ERROR)
								BOLTS_LWARNING() << "	Error loading";
						}
					}
					if (texData.tex)
					{
						ImGui::SameLine();
						Rendering::TextureSamplerSettings sampler = texData.sampler;
						if (texData.useDefault)
							sampler = texData.tex->GetSampler();
						static const char* c_samplerNames[] = {
							"NONE",
							"NEAREST",
							"LINEAR",
							"WAT?",
							"ANISO",
						};
						ImGui::Text("%s | %s", c_samplerNames[(int)sampler.filteringType], c_samplerNames[(int)sampler.mipFilteringType]);

						ImGuiImage(texData.handle.d, 256, 256);
						const uvec3 res = texData.tex->GetDimensions();
						if (ImGui::IsMouseDown(2) && ImGui::IsItemHovered()) {
							ImGui::BeginTooltip();
							ImGuiImage(texData.handle.d, res.x, res.y);
							ImGui::EndTooltip();
						}
					}

					break;
				}
			}
			ImGui::NextColumn();

			ImGui::PopID();
		}
		tab.m_hasChanges = anyChanges;
	}

	void SaveTab(MatInstanceTab& tab)
	{
		if (!tab.m_material)
			return;

		str_t matFilename = m_saveFolder + tab.m_matName;
		auto path = Bolts::System::SaveFileDialog(matFilename.c_str(), "material", "Material");
		if (path.empty())
			return;

		rapidjson::Document doc;
		auto& rootObj = doc.SetObject();
		auto& alloc = doc.GetAllocator();

		const char* baseMatName = m_assetManager.GetAssetName(tab.m_material->GetBaseMaterialID());
		rapidjson::Value bmv{ baseMatName, alloc };
		rootObj.AddMember("base", bmv, alloc);
		if (tab.m_defines.size() > 0) {
			rapidjson::Value definesObj;
			definesObj.SetArray();
			for (auto& define : tab.m_defines)
				definesObj.PushBack(rapidjson::StringRef(define.c_str()), alloc);
			rootObj.AddMember("defines", definesObj, alloc);
		}
		rapidjson::Value paramsObj;
		paramsObj.SetObject();
		auto& matParams = tab.m_material->GetParams();
		for (auto& paramInfo : matParams.GetInfo()) {
			const char* paramName = GetHashString(paramInfo.key);
			BASSERT(paramName);
			rapidjson::Value::StringRefType  paramNameValue(paramName);
			const void* paramValueData = matParams.GetDataUnsafe(paramInfo.dataOffset);
			rapidjson::Value paramValue;
			switch (paramInfo.type) {
			case Rendering::ShaderParamType::SPT_FLOAT:
				paramsObj.AddMember(paramNameValue, *(float*)paramValueData, alloc);
				break;
			case Rendering::ShaderParamType::SPT_VEC2:
				paramValue.SetArray();
				paramValue.PushBack(((vec2*)paramValueData)->x, alloc);
				paramValue.PushBack(((vec2*)paramValueData)->y, alloc);
				paramsObj.AddMember(paramNameValue, paramValue, alloc);
				break;
			case Rendering::ShaderParamType::SPT_VEC3:
				paramValue.SetArray();
				paramValue.PushBack(((vec3*)paramValueData)->x, alloc);
				paramValue.PushBack(((vec3*)paramValueData)->y, alloc);
				paramValue.PushBack(((vec3*)paramValueData)->z, alloc);
				paramsObj.AddMember(paramNameValue, paramValue, alloc);
				break;
			case Rendering::ShaderParamType::SPT_VEC4:
				paramValue.SetArray();
				paramValue.PushBack(((vec4*)paramValueData)->x, alloc);
				paramValue.PushBack(((vec4*)paramValueData)->y, alloc);
				paramValue.PushBack(((vec4*)paramValueData)->z, alloc);
				paramValue.PushBack(((vec4*)paramValueData)->w, alloc);
				paramsObj.AddMember(paramNameValue, paramValue, alloc);
				break;
			case Rendering::ShaderParamType::SPT_TEX: {
				auto texParam = (Rendering::MatParameterHolder::TextureParam*)paramValueData;
				BASSERT(texParam->texAssetID != 0);
				auto texName = m_assetManager.GetAssetName(texParam->texAssetID);
				BASSERT(texName);
				//TODO: handle sampler serialisation
				paramValue.SetString(texName, alloc);
				paramsObj.AddMember(paramNameValue, paramValue, alloc);
			} break;
			default:
				BASSERT(false);
				BOLTS_LERROR() << "Error: Serialising not implemented for parameter " << paramName << ", of type " << Rendering::c_SPTNames[paramInfo.type];
			}
		}
		rootObj.AddMember("params", paramsObj, alloc);

		rapidjson::StringBuffer buffer;
		rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buffer);
		writer.SetIndent('\t', 1);
		writer.SetMaxDecimalPlaces(5);
		writer.SetFormatOptions(rapidjson::kFormatSingleLineArray);
		doc.Accept(writer);

		//
		BASSERT_ONLY(bool result = ) Bolts::IO::writeFile(path.c_str(), buffer.GetString(), buffer.GetSize(), Bolts::IO::EF_TEXT);
		BASSERT(result);
	}
	
	void OpenTab(const char* matName, int matNameLen) 
	{
		BASSERT(matName);
		for (const auto& tab : m_openTabs) {
			if (strcmp(tab.m_matName, matName) == 0)
				return;
		}
		m_openTabs.emplace_back();
		auto& tab = m_openTabs.back();
		BASSERT(matNameLen < countof(tab.m_matName));
		strcpy_s(tab.m_matName, matNameLen + 1, matName);
		tab.m_matName[matNameLen] = '\0';
		OnMaterialChanged(tab);
	}

	void InitEditor()
	{
		// matpacks
		m_matpacks = {
			"standard.matpack",
			"blinn_phong.matpack",
			"post_processing.matpack",
			"matcaps.matpack",
			"tycoon.matpack",
			"widgets.matpack"
		};
		for (auto& packName : m_matpacks) {
			m_assetManager.LoadAssetFile<Rendering::Material>( HashString(packName));
		}
		{
			using namespace Rendering;
			regHash_t texName("BadTex");
			m_badTexture = new Texture(texName);
			m_badTexture->Init(*m_assetManager.GetCommandBuffer());
			auto texData = m_badTexture->SetTexture(*m_assetManager.GetCommandBuffer(), BTT_2D, { 1,1,1 }, BTF_RGBA, BTDF_IMPLICIT, false);
			BASSERT(texData.dataSize == 4);
			*((uint32_t*)texData.data) = 0xF0F000F0;
		}

		// meshes
		{
			auto pMesh = m_assetManager.LoadAsset<Bolts::Rendering::Mesh>("UnitSphere");
			pMesh->subMeshes.push_back({
				Bolts::MeshBuilder::SphereIndexed(m_renderer.GetCommandBuffer(), 1.f, 32, 32),
				"Sphere"
				});
			pMesh->aabb.min = vec3(-0.5f);
			pMesh->aabb.max = vec3(0.5f);
			pMesh->materials.push_back({});
			pMesh->SetAssetState(assets::EAS_READY);
		}

		// scene setup
		m_previewCamera.onReferenceAdd();
		m_previewCamera.SetUpVector({ 0.f, 0.f, 1.f });
		m_previewCamera.SetPosition({ 5.f, 5.f, 5.f });
		m_previewCamera.SetTarget({});
		m_previewCamera.SetZNear(0.1f);
		m_previewCamera.SetZFar(50.f);
		m_renderer.UpdateCamera(&m_previewCamera);

		// preview obj
		m_previewVO = m_renderer.AddVisibilityObject();
		mat4 m;
		m_renderer.SetVOTransformAndRadius(m_previewVO, m, 10.f);
		m_renderer.SetVOLists(m_previewVO, Rendering::voListMask_t(Rendering::MAIN_VIEW | Rendering::SHADOW_VIEW));
		auto& renderable = m_renderer.AddStaticMesh();
		m_previewRenderable = renderable.GetRenderableID();
		m_renderer.AttachRenderable(m_previewVO, m_previewRenderable);

		// tabs
		LoadSettings();
		if (m_openTabs.empty())
			m_openTabs.emplace_back();
		if (m_previewMeshName.empty())
			m_previewMeshName = "UnitSphere";
		ChangePreviewMesh(m_previewMeshName.c_str());
	}

	bool Update(float dt)
	{
		m_renderer.StartNewFrame();
		auto renderBackend = m_renderer.GetBackend();

		//TODO: This shows up upside down
		m_renderer.SetDirectionalLight({ 0.f, 0.f, 1.f }, { 1.f, 1.f, 1.f });
		m_renderer.EditShadowSettings().shadowDistance = m_previewCamera.GetZFar();
		 
		// mouse look
		if (m_previewMesh)
			m_previewCamera.SetTarget(m_previewMesh->aabb.center());
		if (m_mouseState.IsButtonDown(Bolts::Input::B_RIGHT)) {
			float lookSpeed = 2.f; 
			m_previewCamera.OrbitRight(-lookSpeed * m_mouseState.GetMouseMoveAmount().x);
			m_previewCamera.OrbitUp(-lookSpeed * m_mouseState.GetMouseMoveAmount().y);
		}
		 
		if (m_mouseState.GetMouseScrollAmount().y != 0.f)
			m_previewCamera.MoveForward(m_mouseState.GetMouseScrollAmount().y * ( m_keyState.IsKeyDown(Input::KC_SHIFT) ? 0.1f : 0.01f) * glm::length(m_previewCamera.GetPosition()));

		// toolbar 
		ImGuiWindowFlags window_flags = 0;  
		window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoDocking;
		window_flags |= ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar;
		window_flags |= ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoBringToFrontOnFocus;
		window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

		ImGui::SetNextWindowPos(ImVec2(15.f, 15.f));
		ImGui::SetNextWindowBgAlpha(0.2f);
		ImGui::Begin("ViewportWidgets", nullptr, window_flags);
		ImGui::AlignTextToFramePadding();
		
		ImGui::Text(ICON_KI_PLUS " New");
		bool createNewMaterial = ImGui::IsItemClicked();
		ImGui::SameLine();
		ImGui::Text(ICON_KI_SAVE " Open");
		bool openNewMaterial = ImGui::IsItemClicked();
		ImGui::SameLine();
		ImGui::Text("|");
		ImGui::SameLine();
		ImGui::Text("Cam %.3f %.3f %.3f", m_previewCamera.GetPosition().x, m_previewCamera.GetPosition().y, m_previewCamera.GetPosition().z);

		float toolbarMax = ImGui::GetWindowContentRegionMax().y + ImGui::GetWindowPos().y;
		float toolbarX = ImGui::GetWindowPos().x; 
		ImGui::End();
	
		// new mat
		static char matName[64];
		static char baseMatName[64];
		if (createNewMaterial)
			ImGui::OpenPopup("Create New Material");
		if (ImGui::BeginPopupModal("Create New Material", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {
			ImGui::InputText("Name", matName, sizeof(matName));
			if (matName[0] != '\0') {
				auto& loadedMats = m_assetManager.GetLoadedAssets(assets::EAT_MATERIAL);

				ImGui::InputText("Base", baseMatName, sizeof(baseMatName));
				if (baseMatName[0] != '\0') {
					auto baseMat = m_assetManager.LoadAsset<Rendering::Material>(HashString(baseMatName));
					if (baseMat && baseMat->IsValid()) {
						if (ImGui::Button("Create")) 
						{
							auto newMat = new Rendering::Material(HashString(matName));
							newMat->CloneFrom(*baseMat);
							newMat->SetAssetState(assets::EAS_READY);
							m_assetManager.AddAsset(newMat->GetAssetID(), assets::EAT_MATERIAL, newMat);

							m_openTabs.emplace_back();
							strcpy(m_openTabs.back().m_matName, matName);
							OnMaterialChanged(m_openTabs.back());

							ImGui::CloseCurrentPopup();
						}
					}
				}
			}
			if(ImGui::Button("Cancel"))
				ImGui::CloseCurrentPopup();
			ImGui::EndPopup();
		}
		if (openNewMaterial) {
			path_t materialPath = OpenFileDialog(nullptr, "material", "Material Instance");
			if (const char* matFilenameCstr = IO::filenameFromPath(materialPath.c_str(), materialPath.length())) {
				str_t matName;
				matName.assign(matFilenameCstr, IO::extensionFromPath(matFilenameCstr, strlen(matFilenameCstr)) - matFilenameCstr - 1);
				OpenTab(matName.c_str(), matName.size());
			}
		}

		// other settings
		ImGui::SetNextWindowPos(ImVec2(toolbarX, toolbarMax + 20.f));
		if (ImGui::Begin("Settings"))
		{
			ImGui::Text("Preview Mesh: %s", m_previewMeshName.c_str());
			if (ImGui::IsItemClicked()) {
				path_t modelPath = OpenFileDialog(nullptr, "mesh", "Mesh");
				if (const char* modelName = IO::filenameFromPath(modelPath.c_str(), modelPath.length())) {
					ChangePreviewMesh(modelName);
					if (m_previewMesh && m_previewMesh->IsValid())
						m_previewMeshName.assign(modelName);
				}
			}
			ImGui::SameLine();
			if (ImGui::SmallButton(ICON_KI_SAVE)) {
				ImGui::SetClipboardText( m_previewMeshName.c_str() );
			}
			ImGui::Separator();
			ImGui::Text("Matpacks");
			ImGui::Indent();
			for (auto& matpak: m_matpacks)
				ImGui::Text( matpak.c_str());
			ImGui::Unindent();
		} 
		ImGui::End();

		// tabs
		int tabIdx = 0;
		for (int i = 0; i < m_openTabs.size(); i++) {
			auto& tab = m_openTabs[i];
			char winName[64];
			snprintf(winName, countof(winName), "%s%s###%d", tab.m_matName, tab.m_hasChanges ? " *" : "", tabIdx);
			winName[63] = '\0';
			bool isOpen = true;
			if (ImGui::Begin( winName, &isOpen))
			{
				tab.m_hasFocus = ImGui::IsWindowFocused();
				RenderMaterialProps( tab);
			}
			ImGui::End();

			if (!isOpen) {
				m_openTabs.erase(m_openTabs.begin() + i);
				i--;
				continue;
			}

			// update preview mat
			if (tab.m_hasFocus && tab.m_material) {
				auto& renderable = m_renderer.GetStaticMesh(m_previewRenderable);
				renderable.m_materials.resize( renderable.m_mesh->materials.size() );

				for (auto& matPtr : renderable.m_materials)
					matPtr = tab.m_material;
			}
			tabIdx++;
		}

		m_keyState.EventsHandled();
		m_mouseState.EventsHandled();

		m_renderer.UpdateCamera(&m_previewCamera);
		return true;
	}

	// settings
	typedef rapidxml::xml_document<char>	xmlDoc_t;
	typedef rapidxml::xml_node<char>		xmlElem_t;

	void LoadSettings()
	{
		using namespace Bolts;
		using namespace rapidxml;
		// load XML
		const char* filepath = "matEditorOptions.xml";
		size_t fsize = Bolts::IO::fileSize(filepath);
		if (fsize == 0) {
			return;
		}
		str_t content;
		content.resize(fsize + 1);
		auto readSize = Bolts::IO::readFile(filepath, &content[0], fsize + 1, Bolts::IO::EF_TEXT);
		content[fsize] = '\0';
		xmlDoc_t doc;
		doc.parse<0>(&content[0]);
		//
		auto rootNode = doc.first_node();
		BASSERT(rootNode);
		auto tabsNode = rootNode->first_node("tabs");
		BASSERT(tabsNode);
		auto tabNode = tabsNode->first_node();
		while (tabNode) {
			auto nameAtt = tabNode->first_attribute("name");
			BASSERT(nameAtt);
			OpenTab(nameAtt->value(), nameAtt->value_size());
			tabNode = tabNode->next_sibling();
		}
		//
		auto hiddenPropsNode = rootNode->first_node("hidden_props");
		BASSERT(tabsNode);
		auto pNode = hiddenPropsNode->first_node();
		while (pNode) {
			m_hiddenProps.emplace_back(pNode->value() , pNode->value_size());
			pNode = pNode->next_sibling();
		}
		//
		auto node = rootNode->first_node("preview_mesh");
		if (node) {
			auto attrib = node->first_attribute();
			m_previewMeshName.assign(attrib->value(), attrib->value_size());
		}
	}

	void SaveSettings()
	{
		using namespace rapidxml;
		xmlDoc_t doc;
		auto rootNode = doc.allocate_node(node_element, "settings", nullptr);
		doc.append_node(rootNode);
		
		auto tabsNode = doc.allocate_node(node_element, "tabs", nullptr);
		rootNode->append_node(tabsNode);
		for (auto& tab : m_openTabs) {
			if (!tab.m_matName || strlen(tab.m_matName) == 0)
				continue;
			auto tabNode = doc.allocate_node(node_element, "tab", nullptr);
			auto nameAtt = doc.allocate_attribute("name", doc.allocate_string( tab.m_matName ));
			tabNode->append_attribute(nameAtt);
			tabsNode->append_node(tabNode);
		}

		auto hiddenPropsNode = doc.allocate_node(node_element, "hidden_props", nullptr);
		rootNode->append_node(hiddenPropsNode);
		for (auto& propname : m_hiddenProps) {
			auto pNode = doc.allocate_node(node_element, "p", propname.c_str());
			hiddenPropsNode->append_node(pNode);
		}

		{
			auto node = doc.allocate_node(node_element, "preview_mesh", nullptr);
			node->append_attribute(doc.allocate_attribute("name", doc.allocate_string(m_previewMeshName.c_str())));
			rootNode->append_node(node);
		}

		//
		str_t s;
		::rapidxml::print(std::back_inserter(s), doc, 0);
		BASSERT_ONLY(bool result = ) Bolts::IO::writeFile("matEditorOptions.xml", s.data(), s.size(), Bolts::IO::EF_TEXT);
		BASSERT(result);
	}

private:
	System::windowHandle_t			m_mainWindow = {};
	bool							m_bExiting = false;
	float							m_deltaSecs = 0.f;
	Input::KeyStateHolder			m_keyState;
	Input::MouseStateHolder			m_mouseState;

	Rendering::RenderBackend*		m_renderBackend = nullptr;
	Rendering::Renderer				m_renderer;

	assets::AssetManager			m_assetManager;
	Rendering::TexturePtr			m_badTexture;

	Rendering::voHandle_t			m_previewVO;
	Rendering::renderableHandle_t	m_previewRenderable;
	Rendering::MeshPtr				m_previewMesh;
	str_t							m_previewMeshName;
	Camera							m_previewCamera;

	uvec2							m_windowSize;
	vec_t<str_t>					m_matpacks;
	vec_t<str_t>					m_hiddenProps;
	vec_t< MatInstanceTab>			m_openTabs;
	str_t							m_saveFolder;
};

int main()//(int argc, char* argv[])
{
	// Setup assertion handlers
	Core::assert::AddAssertHandler(Core::assert::CoutAH);
	Core::assert::AddAssertHandler(Core::assert::BreakpointAH);

	Bolts::IO::Init();
	Bolts::System::OnAppStart();

	// Register data folder ( CWD )
	IO::GetFSSource().RegisterDir("./");

	App app;
	while (app.AppUpdate());

	return 0;
}
