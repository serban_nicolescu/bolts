-- A solution contains projects, and defines the available configurations

-- Global config
include "common.lua"

-------------------------------------------------
--
-------------------------------------------------

solution "MeshConverter"
	location ( _ACTION )
	
	configurations { "Debug", "Release" }
	
	debugdir ( "../resources" )
	targetdir( bCfg.libOutPath )
	objdir 	( bCfg.objPath )
	
	
	filter { "action:vs*" }
		system "Windows"
		platforms { "x32", "x64" }
	filter {}

	flags { "MultiProcessorCompile", "NoMinimalRebuild" }
	
	configuration "Debug"
		defines { "DEBUG" }
		optimize "Off"
		symbols "On"
		targetsuffix "-d"
	configuration "Release"
		defines { "NDEBUG" }
		optimize "Speed"
	configuration {}
	
	filter { "system:Windows" }
		defines { "WIN32", "_CRT_SECURE_NO_WARNINGS", "_SCL_SECURE_NO_WARNINGS", "WIN32_LEAN_AND_MEAN", "NOMINMAX" }
		characterset "MBCS"
		systemversion "latest"
	filter{}
	
	-- Bolts
	include "microprofile.lua"
	include "libjpg.lua"
	include "Soil.lua"
	include "IMGUI.lua"
	include "bolts_core.lua"
	include "bolts.lua"	
	-- Tool
	include "zlib.lua"
	include "assimp.lua"
	include "openfbx.lua"
	
project "MeshConverter"
	location ( bCfg.prjPath )
	kind "ConsoleApp"
	language "C++"
	
	files 
	{ 
		"MeshConverter/src/**.h", 
		"MeshConverter/src/**.cpp",
	}
	includedirs 
	{
		"MeshConverter/src",
		"../Shooty",
		"../Shooty/StdAfx.h",
		bCfg.depPath .. "rapidjson/include/",
	}
	
	-- VS-specific
	configuration { "vs*"}
		defines { "_WIN32_WINNT=0x0501" }
		--flags { "Unicode" }
	configuration {}
	-- Configs
	--libdirs (bCfg.libOutPath)
	configuration "Debug*"
		targetdir ( bCfg.outPath .. "Debug/" )
	configuration "Release*"
		targetdir ( bCfg.outPath .. "Release/" )
	configuration {}
	
	--flags { "FatalWarnings" }
	defines { "MICROPROFILE_ENABLED=0" }
	
	--Disable some warnings
	buildoptions 
	{ 
		"/wd\"4127\"", -- VS: Conditional expression is constant ( from logging)
		"/wd\"4512\"", -- VS: assignment operator could not be generated
	}
	
	--Other projects or precompiled libs
	_deps["Assimp"](false)
	_deps["Bolts"](false)
	_deps["OpenFBX"](false)
	
	