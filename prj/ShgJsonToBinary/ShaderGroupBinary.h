#pragma once

#include <Bolts/Core/Types.h>
#include <Bolts/Core/BinarySerialization.h>

namespace Bolts {

	namespace ShaderGroup {

		struct PassInfo {
			int8_t contextIndex;
			uint8_t passIndex;
			uint16_t programIndex;
		};

		struct ProgramSet
		{
			PassInfo passes[4];
		};

		struct ProgramMap
		{
			OffsetPtr< InplaceArray<uint16_t>> programIndicesUsed;
			OffsetPtr< InplaceArray<ProgramSet>> programSets;
		};

		struct ProgramCompileInfo
		{
			OffsetPtr< char> vertexEntry;
			OffsetPtr< char> fragmentEntry;
			OffsetPtr< InplaceOffsetArray <char>> defines;
		};


		struct ShaderGroupFile {
			OffsetPtr< InplaceOffsetArray<char>> staticFeatures;
			OffsetPtr< InplaceOffsetArray<char>> dynamicFeatures;
			OffsetPtr< InplaceOffsetArray<char>> contexts;
			OffsetPtr< InplaceOffsetArray<char>> passes;
			OffsetPtr< char > code;
			OffsetPtr< InplaceOffsetArray<ProgramCompileInfo>> uniquePrograms;
			OffsetPtr< InplaceOffsetArray<ProgramMap>> staticMap;
		};
	}
};