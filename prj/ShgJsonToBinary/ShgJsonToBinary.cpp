// ShgJsonToBinary.cpp : Defines the entry point for the console application.
//

#include "ShaderGroupBinary.h"
#include <string>
#include <vector>
#include <cstdio>
#include <type_traits>
#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>

#include <Bolts/Core/Types.h>
#include <Bolts/Core/BinarySerialization.h>

using namespace Bolts;
using namespace Bolts::ShaderGroup;

// Helpers

template<typename T, typename U>
int find_index_of(const std::vector<T>& vec, const U& val)
{
	auto it = std::find(vec.begin(), vec.end(), val);
	if (it == vec.end())
		return -1;
	else return it - vec.begin();
}
//

int main(int argc, char* argv[])
{
	using namespace std;

	serialization::Init();

	const char* inputFilename = static_cast<const char*>(argv[1]);
	const char* outputFilename = static_cast<const char*>(argv[2]);

	vector<char> inputData;

	{
		FILE* f = nullptr;
		fopen_s(&f, inputFilename, "rb");
		if (f)
		{
			fseek(f, 0, SEEK_END);
			long fsize = ftell(f);
			fseek(f, 0, SEEK_SET);
			inputData.resize(fsize + 1);
			fread(inputData.data(), 1, fsize, f);
			inputData[fsize] = 0;
			fclose(f);
		}
	}
	//
	struct Program
	{
		uint32_t  SerializedSizeEst(::Bolts::Blob& b) const {
			return sizeof(ProgramCompileInfo) + b.SizeOf(vertex_entry) + b.SizeOf(fragment_entry) + b.SizeOf(defines);
		}

		void Serialize(::Bolts::Blob& b) const {
			ProgramCompileInfo* pm = (ProgramCompileInfo*) b.WillStore(sizeof(ProgramCompileInfo));
			pm->vertexEntry = b.Store(vertex_entry);
			pm->fragmentEntry = b.Store(fragment_entry);
			pm->defines = b.Store(defines);
		}

		bool operator==(const Program& other) const { return vertex_entry == other.vertex_entry && fragment_entry == other.fragment_entry && defines == other.defines; }

		string vertex_entry;
		string fragment_entry;
		vector<string> defines;
	};

	struct ProgramMapData
	{
		uint32_t  SerializedSizeEst(::Bolts::Blob& b) const {
			return sizeof(ProgramMap) + b.SizeOf(usedProgramIndices) + b.SizeOf(sets);
		}

		void Serialize(::Bolts::Blob& b) const {
			ProgramMap* pm = (ProgramMap*)b.WillStore(sizeof(ProgramMap));
			pm->programIndicesUsed = b.Store(usedProgramIndices);
			pm->programSets = b.Store(sets);
		}

		vector<uint16_t> usedProgramIndices;
		vector<ProgramSet> sets;
	};

	struct PerContextData {
		vector<PassInfo> pass;
	};

	vector<Program> uniquePrograms;
	vector<string> contexts;
	vector<string> passes;
	vector<string> allFeatures;
	vector<string> staticFeatures;
	vector<string> dynamicFeatures;
	vector<PerContextData> contextData;
	contextData.emplace_back();//Index 0 is bad

	//Read JSON
	using namespace rapidjson;
	Document d;
	d.Parse<ParseFlag::kParseDefaultFlags>(inputData.data());
	Value& contextsJson = d["contexts"];
	for (Value::ConstValueIterator itr = contextsJson.Begin(); itr != contextsJson.End(); ++itr)
		contexts.push_back(itr->GetString());
	Value& passesJson = d["pass_names"];
	for (Value::ConstValueIterator itr = passesJson.Begin(); itr != passesJson.End(); ++itr)
		passes.push_back(itr->GetString());
	// Features
	Value& featuresJson = d["features"];
	auto it = featuresJson.MemberBegin();
	for (; it != featuresJson.MemberEnd(); it++)
	{
		const char* name = it->name.GetString();
		allFeatures.push_back(name);
		if (strcmp("static", it->value["type"].GetString()) == 0) {
			staticFeatures.push_back(name);
		}
		else {
			dynamicFeatures.push_back(name);
		}
	}
	static_assert(std::is_pod< uint16_t>::value,"Bla");
	// Programs
	Value& programsJson = d["programs"];
	contextData.reserve(programsJson.Size());
	Program temp;
	for (Value::ConstValueIterator program = programsJson.Begin(); program != programsJson.End(); ++program) {
		contextData.emplace_back();
		PerContextData& currentEntry = contextData.back();
		temp.defines.clear();

		const Value& definesArray = (*program)[1u];
		temp.defines.reserve(definesArray.Size());
		for (Value::ConstValueIterator define = definesArray.Begin(); define != definesArray.End(); ++define)
			temp.defines.push_back(define->GetString());

		const Value& passesJson = (*program)[0u];
		for (Value::ConstValueIterator pass = passesJson.Begin(); pass != passesJson.End(); ++pass) {
			const Value& stuff = *pass;
			currentEntry.pass.emplace_back();
			auto& currentPass = currentEntry.pass.back();
			currentPass.contextIndex = find_index_of(contexts, stuff[0u].GetString());
			currentPass.passIndex = find_index_of(passes, stuff[1u].GetString());
			temp.vertex_entry = stuff[2u].GetString();
			temp.fragment_entry = stuff[3u].GetString();
			int idx = find_index_of(uniquePrograms, temp);
			if (idx == -1) {
				uniquePrograms.push_back(temp);
				idx = uniquePrograms.size() - 1;
			}
			currentPass.programIndex = idx;
		}
	}
	//
	Value& mappingJson = d["mapping"];
	vector<vector<uint16_t>> mappings;
	mappings.resize(contexts.size());
	int idx = 0;
	for (Value::ConstValueIterator contextsJson = mappingJson.Begin(); contextsJson != mappingJson.End(); ++contextsJson) {
		auto& indices = mappings[idx];
		indices.reserve(contextsJson->Size());
		for (Value::ConstValueIterator contextDataIndex = contextsJson->Begin(); contextDataIndex != contextsJson->End(); ++contextDataIndex) {
			if (contextDataIndex->IsNull())
				indices.push_back(0);
			else
				indices.push_back(contextDataIndex->GetInt() + 1);
		}
		idx++;
	}
	Value& codeJson = d["code"];
	// Put everything into the final binary format
	const unsigned numDynamicSets = 1 << (staticFeatures.size());// One dynamic map for every static permutation
	const unsigned numProgramSets = 1 << (dynamicFeatures.size());
	const unsigned numContexts = contexts.size();
	bool usedPrograms[1 << 16];
	vector<ProgramMapData> dynamicMaps;
	dynamicMaps.resize(numDynamicSets);
	for (int progMap = 0; progMap < numDynamicSets; progMap++) {
		ProgramMapData& currentDynamic = dynamicMaps[progMap];
		memset(usedPrograms, 0, 1 << 16);
		currentDynamic.sets.resize(numProgramSets);
		for (int progSet = 0; progSet < numProgramSets; progSet++) {
			ProgramSet& currentSet = currentDynamic.sets[progSet];
			int mappingOffset = progMap * numProgramSets + progSet;
			int numPasses = 0;
			for (int ctx = 0; ctx < numContexts; ctx++) {
				int dataIdx = mappings[ctx][mappingOffset];
				if (dataIdx == 0)
					continue;
				PerContextData& ctxData = contextData[dataIdx];
				for (auto data : ctxData.pass) {
					currentSet.passes[numPasses] = data;
					numPasses++;
					if (!usedPrograms[data.programIndex]) {
						currentDynamic.usedProgramIndices.push_back(data.programIndex);
						usedPrograms[data.programIndex] = true;
					}
				}
			}
			// Mark remaining passes as invalid
			for (int i = numPasses; i < 4; i++)
				currentSet.passes[i].contextIndex = -1;
		}
	}

	// Write it out
	{
		Blob b;
		b.SetCapacity(10 * (1 << 20)); // Reserve 10MB of mem

		ShaderGroupFile* fileMap = static_cast<ShaderGroupFile*>(b.WillStore(sizeof(ShaderGroupFile)));
		fileMap->staticFeatures = b.Store(staticFeatures);
		fileMap->dynamicFeatures = b.Store(dynamicFeatures);
		fileMap->contexts = b.Store(contexts);
		fileMap->passes = b.Store(passes);
		void* codeBuffer = b.WillStore(codeJson.GetStringLength() + 1);
		memcpy(codeBuffer, codeJson.GetString(), codeJson.GetStringLength()+1);
		fileMap->code = codeBuffer;
		fileMap->uniquePrograms = b.Store(uniquePrograms);
		fileMap->staticMap = b.Store(dynamicMaps);
		//
		FILE* f = nullptr;
		fopen_s(&f, outputFilename, "wb");
		if (f)
		{
			fwrite(b.Data(), 1, b.Size(), f);
			fclose(f);
		}
		/*
		fopen_s(&f, outputFilename, "rb");
		vector<char> data;
		data.resize(b.Size());
		fread(data.data(), 1, b.Size(), f);
		ShaderGroupFile& test = *reinterpret_cast<ShaderGroupFile*>(data.data());
		test.code.get();
		test.uniquePrograms->Size(); test.uniquePrograms->SizeInBytes(); test.uniquePrograms->SizeOf(0); test.uniquePrograms->Get(0);
		test.uniquePrograms->Get(0)->fragmentEntry.get();
		test.uniquePrograms->Get(0)->defines.get()->Get(0);
		test.staticFeatures->Size(); test.staticFeatures->SizeInBytes(); test.staticFeatures->SizeOf(0); test.staticFeatures->Get(0);
		test.staticMap->Size(); test.staticMap->SizeInBytes(); test.staticMap->SizeOf(0); test.staticMap->Get(0)->programIndicesUsed->operator[](0);
		fclose(f);
		*/
	}


	//



	return 0;
}

