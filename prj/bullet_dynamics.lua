local lProjectName = "BulletDynamics"
project (lProjectName)
	location ( bCfg.prjPath )
	kind "StaticLib"
	language "C++"
	
	-- Only include a minimal subset to eliminate dependencies
	files 
	{ 
		bCfg.depPath .. "bullet/src/" .. lProjectName .. "/**.h", 
		bCfg.depPath .. "bullet/src/" .. lProjectName .. "/**.cpp",
	}
	
	includedirs 
	{ 
		bCfg.depPath .. "bullet/src/",
	}
	
	dofile "bullet.lua"