-- A solution contains projects, and defines the available configurations

-- Global config
_deps = {}

bCfg = {
	depPath = "../dependencies/",
	outPath = "../bin/" .. _ACTION .. "/",
	prjPath = "prj/" .. _ACTION,
	objPath = "obj/" .. _ACTION,
}
bCfg.libPath = bCfg.depPath .. "libs/" .. _ACTION .. "/"
bCfg.libOutPath = bCfg.depPath .. "libs/" .. _ACTION .. "/%{prj.name}/%{cfg.shortname}"
bCfg.boostPath = bCfg.depPath .. "boost_1_64_0/"
-- Utility defines
bUtils = {
	action = _ACTION or ""
}
function bUtils.GetOutputName( baseName )
	return baseName .. '_%{cfg.shortname}'
end

function bUtils.GetOutDirName( baseName )
	return baseName .. '_%{cfg.shortname}'
end

function bUtils.IsVSAction()
	return _ACTION and string.find( _ACTION, "vs")
end

function bUtils.GetActionOutPath() --Returns the path where the projects for the current action would be generated
	--NOTE: Should match values in the project generation batch files
	if (bUtils.IsVSAction()) then
		return _ACTION
	else
		return ""
	end
end

--newoption {
--	trigger 	= "no-pch",
--	description = "NOT IMPLEMENTED !!! Disable force including pch files to the project"
--}

-------------------------------------------------
--
-------------------------------------------------

solution "ShgJson2Shg"
	location ( _ACTION )
	
	configurations { "Debug", "Release" }
	
	debugdir ( "ShgJsonToBinary/test/" )
	targetdir( bCfg.libOutPath )
	objdir 	( bCfg.objPath )
	
	filter { "action:vs*" }
		system "Windows"
		platforms { "x32", "x64" }
	filter {}

	flags { "MultiProcessorCompile", "NoMinimalRebuild" }
	
	configuration "Debug"
		defines { "DEBUG" }
		optimize "Off"
		flags { "Symbols" }
		targetsuffix "-d"
	configuration "Release"
		defines { "NDEBUG" }
		optimize "Speed"
	configuration {}
	
	filter { "system:Windows" }
		defines { "WIN32", "_CRT_SECURE_NO_WARNINGS", "_SCL_SECURE_NO_WARNINGS", "WIN32_LEAN_AND_MEAN", "NOMINMAX" }
	filter{}
	--
	include "bolts_core.lua"
	include "dependencies.lua"
	
project "ShgJson2Shg"
	--location ( "ShgJsonToBinary/prj/" .. _ACTION )
	kind "ConsoleApp"
	language "C++"
	
	files 
	{ 
		"ShgJsonToBinary/**.h", 
		"ShgJsonToBinary/**.cpp",
	}
	includedirs 
	{
		"ShgJsonToBinary",
		bCfg.boostPath,
	}
	
	-- VS-specific
	configuration { "vs*"}
		defines { "_WIN32_WINNT=0x0501" }
	configuration { "vs2013" }
		flags { "Unicode" }
	configuration {}
	-- Configs
	--libdirs (bCfg.libOutPath)
	configuration "Debug*"
		targetdir ( bCfg.outPath .. "Debug/" )
	configuration "Release*"
		targetdir ( bCfg.outPath .. "Release/" )
	configuration {}
	
	--flags { "FatalWarnings" }
	
	--Disable some warnings
	buildoptions 
	{ 
		"/wd\"4127\"", -- VS: Conditional expression is constant ( from logging)
		"/wd\"4512\"", -- VS: assignment operator could not be generated
	}
	
	--Other projects or precompiled libs
	links 
	{
	}
	
	libdirs
	{
		bCfg.boostPath .. "/stage/lib/",
	}
	-- Windows-only for now
	libdirs 
	{
		bCfg.depPath .. "/libs/boost/",
	}	
	
	_deps["BoltsCore"]()
	_deps["rapidjson"]()