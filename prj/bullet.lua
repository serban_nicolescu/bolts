	floatingpoint "Fast"
	-- Temporarily disable until you figure out the side-effects
	-- flags { "NoRTTI", "StaticRuntime"} 
	
	configuration "Debug"
		flags { "NoEditAndContinue" }
	configuration {"Windows"}
		defines { "_CRT_SECURE_NO_DEPRECATE"}
	configuration{}

	filter { "action:vs*" }
		flags { "NoExceptions"}
		defines { "_HAS_EXCEPTIONS=0" }
	filter {}

	removedefines "DEBUG"
