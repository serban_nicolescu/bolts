local lProjectName = "soil"

_deps[lProjectName] = function (header_only)
	header_only = header_only or false
	includedirs 
	{ 
		bCfg.depPath .. "soil/src/",
	}
	if not header_only then	
		links {
			lProjectName
		}
	end
end

project (lProjectName)
	location ( bCfg.prjPath )
	kind "StaticLib"
	language "C++"
	
	uuid "e9d7f9cf-102d-4dc9-a86c-f839a99b433c"
	
	files 
	{ 
		bCfg.depPath .. "soil/src/**.h", 
		bCfg.depPath .. "soil/src/**.c" 
	}
	
	includedirs
	{
		bCfg.depPath .. "soil/src/",
	}
	
	excludes
	{
		bCfg.depPath .. "soil/src/**.cpp",
		bCfg.depPath .. "soil/src/**original**"
	}
	