local lProjectName = "nanomsg"
project (lProjectName)
	location ( bCfg.prjPath )
	kind "StaticLib"
	language "C"
	
	files 
	{ 
		bCfg.depPath .. lProjectName .. "-0.4/src/**.h", 
		bCfg.depPath .. lProjectName .. "-0.4/src/**.c", 

	}
	includedirs 
	{ 
		bCfg.depPath .. lProjectName .. "-0.4/src/"
	}	 

	defines
	{ 
		"NN_NO_EXPORTS",--Create static lib
		-- "NN_EXPORTS", -- Create dynamic lib
		"NN_USE_LITERAL_IFADDR",
		"NN_HAVE_WINDOWS",
	}