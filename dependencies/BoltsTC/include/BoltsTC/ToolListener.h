#ifndef __TOOL_LISTENER_H__
#define __TOOL_LISTENER_H__

#include "Bolts/Core/BinarySerialization.h"
#include <boost/container/flat_map.hpp>

struct CommandHeader;

//IMPORTANT: All socket operations should happen on the same thread
class ToolListener
{
public:
	typedef std::function<void( ToolListener& tl, uint32_t commandCode, serialization::Blob&& payload )> commandHandler_t;

	ToolListener( );
	~ToolListener();

	void StartListening();//>To be used on game-side
	void Connect();//> To be used tool-side
	void Update();

	void QueueCommand( uint32_t commandCode, serialization::Blob&& payload );//> Send command to tool with given payload. Zero-copy sending
	//TODO: Add way to unregister command handlers
	//TODO: Add way to push data to sendingBuffer without allocation

	void RegisterCommandHandler( uint32_t commandCode, commandHandler_t);//> Register function to call when a commmand with specified code is received
private:
	typedef std::vector<char>	blob_t;
	typedef boost::container::flat_map< uint32_t, commandHandler_t>		handlerMap_t;
	struct QueuedCommand
	{
		QueuedCommand( uint32_t cc, serialization::Blob&& pl) :
			commandCode(cc),
			payload(std::move(pl))
		{}
		QueuedCommand(QueuedCommand&& other) :
			commandCode(other.commandCode),
			payload(std::move(other.payload))
			{}

		QueuedCommand& operator=(QueuedCommand&& other)
		{
			commandCode = other.commandCode;
			payload = std::move(other.payload);
			return *this;
		}

		uint32_t				commandCode;
		serialization::Blob		payload;
	private:
		BOOST_MOVABLE_BUT_NOT_COPYABLE(QueuedCommand);
	};

	void Init( bool isHost);

	void ReadMessage();
	void SendQueuedMessage();

	void OnCommandReceived( CommandHeader&, void* parameters );

	void CleanUpSocket();


	blob_t		m_receivingBuffer;//> Temporary storage for recently received command
	blob_t		m_sendingBuffer;//> Holds payloads for commands about to be sent. Reset after a call to Update()
	std::vector<QueuedCommand> m_queuedCommands;
	handlerMap_t m_handlerMap;
	int			m_socket;
	void*		m_cheaderBuffer;
	unsigned	m_nextMessageTag;
};

#endif //__TOOL_LISTENER_H__
