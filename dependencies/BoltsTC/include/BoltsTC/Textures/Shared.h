#ifndef __TEXTURES_SHARED_H__
#define __TEXTURES_SHARED_H__

#include <vector>

extern uint32_t	c_cnTextureDataReq;//>Sent by tool. Requests data about a particular texture
extern uint32_t	c_cnTextureListReq;//>Sent by tool. Requests data about what textures are loaded in the client
extern uint32_t	c_cnTextureData;//>Sent by client. Contains all the required information about a texture
extern uint32_t	c_cnTextureList;//>Sent by client. Reply to a TextureListReq

namespace serialization
{
	class Blob;
};

enum GPUTextureFormat
{
	GTF_RGB8,
	GTF_RGBA8,
	GTF_R8,
	GTF_DEPTH16,
	GTF_DEPTH24,
	GTF_DEPTH32,
	GTF_DXT1_RGB,
	GTF_DXT3_RGBA,
	GTF_DXT5_RGBA,
	_GTF_COUNT
};

struct TextureDataReq
{
	uint32_t texID;
	uint8_t  texMip;

	bool	SerializeTo( serialization::Blob& ) const;
	bool	SerializeFrom( serialization::Blob& );
};

struct TextureImageData
{
	uint16_t			width;
	uint16_t			height;
	uint16_t			depth;
	std::vector<char>	data;
	GPUTextureFormat	format;

	bool	SerializeTo( serialization::Blob& ) const;
	bool	SerializeFrom( serialization::Blob& );
};

struct TextureListData
{
	struct TexInfo
	{
		uint32_t	id;
		std::string name;
	};
	std::vector< TexInfo >		infos;

	bool	SerializeTo( serialization::Blob& ) const;
	bool	SerializeFrom( serialization::Blob& );
};

#endif //__TEXTURES_SHARED_H__