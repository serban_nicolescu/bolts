#ifndef __TEXTURE_MGR_TOOL_H__
#define __TEXTURE_MGR_TOOL_H__

#include <Bolts/Core/Hash.h>
#include "BoltsTC/ToolListener.h"
#include "BoltsTC/Textures/Shared.h"

namespace Bolts
{
	class TextureManager;
}

namespace textures
{
	void RegisterHandlers( ToolListener& tl );//> Registers functions that handle tool requests for texture-related stuff
	//
	void SendTextureData( ToolListener& tl, Bolts::TextureManager* tm, Bolts::stringHash_t texID, uint8_t mipLevel);
	void SendTextureList( ToolListener& tl, Bolts::TextureManager* tm);
}

#endif //__TEXTURE_MGR_TOOL_H__