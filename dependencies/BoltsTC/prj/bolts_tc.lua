-- A solution contains projects, and defines the available configurations
-- Global config
bCfg = {
	depPath = "../../",
	prjPath = _ACTION .. "/%{prj.name}",
	objPath = "obj/" .. _ACTION,
}
bCfg.libPath = bCfg.depPath .. "libs/" .. _ACTION .. "/"
bCfg.libOutPath = bCfg.depPath .. "libs/" .. _ACTION .. "/%{prj.name}/%{cfg.shortname}"
bCfg.boostPath = bCfg.depPath .. "boost_1_56_0/"
-- Utility defines
bUtils = {
	action = _ACTION or ""
}
function bUtils.GetOutputName( baseName )
	return baseName .. '_%{cfg.shortname}'
end

function bUtils.GetOutDirName( baseName )
	return baseName .. '_%{cfg.shortname}'
end

-------------------------------------------------
--
-------------------------------------------------

solution "BoltsTC"
	location ( _ACTION )
	
	configurations { "Debug", "Release" }
	
	targetdir( bCfg.libOutPath )
	objdir 	( bCfg.objPath )
	
	filter { "action:vs*" }
		system "Windows"
		platforms { "x32", "x64" }
	filter {}

	flags { "MultiProcessorCompile", "NoMinimalRebuild" }
	
	configuration "Debug"
		defines { "DEBUG" }
		optimize "Off"
		flags { "Symbols" }
		targetsuffix "-d"
	configuration "Release"
		defines { "NDEBUG" }
		optimize "Speed"
	configuration {}
	
	filter { "system:Windows" }
		defines { "WIN32", "_CRT_SECURE_NO_WARNINGS", "_SCL_SECURE_NO_WARNINGS", "WIN32_LEAN_AND_MEAN", "NOMINMAX" }
	filter{}
	
project "BoltsTC"
	kind "StaticLib"
	language "C++"
	
	uuid "a2898490-86ed-11e4-b4a9-0800200c9a66"
	
	files 
	{ 
		"../include/**.h", 
		"../src/**.h",
		"../src/**.cpp" 
	}
	
	defines { "BOLTS_OPENGL" }
	
	includedirs
	{
		"../include/",
		"../src/",
		bCfg.boostPath,
		bCfg.depPath .. "../src/", -- Bolts
		bCfg.depPath .. "BoltsCore/include/",
		bCfg.depPath .. "glm/",
		bCfg.depPath .. "nanomsg-0.4/src/",
		bCfg.depPath .. "BitSquid/Foundation/",
	}
	
	pchheader "stdafx.h"
	pchsource "../src/stdafx.cpp"
	
	flags { "ExtraWarnings", "FatalWarnings" }
		
	--Disable some warnings
	buildoptions 
	{ 
		"/wd\"4127\"", -- VS: Conditional expression is constant ( from logging)
		"/wd\"4512\"", -- VS: Assignment operator could not be generated
	}