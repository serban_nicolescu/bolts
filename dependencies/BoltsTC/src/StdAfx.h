#pragma once

#include <vector>
#include <string>
#include <Bolts/Core/Assert.h>
#include <Bolts/Core/Types.h>
#include <Bolts/Core/MathTypes.h>
#include <Bolts/Core/SmartPointers.h>
#include <Bolts/Core/Log.h>
#include <Bolts/Core/Hash.h>