#include "StdAfx.h"
#include "BoltsTC/Textures/Client.h"
#include "BoltsTC/Textures/Shared.h"
#include "Bolts/Core/BinarySerialization.h"

#include <Rendering/Managers/TextureManager.h>
#include <Rendering/Texture.h>

namespace textures
{
	static_assert( Bolts::Rendering::_BTF_COUNT == _GTF_COUNT, "Tool texture formats don't match Bolts values" );

	void RegisterHandlers( ToolListener& tl, Bolts::TextureManager* tm )
	{
		tl.RegisterCommandHandler( c_cnTextureDataReq, [tm] ( ToolListener& tl, uint32_t, serialization::Blob&& payload ) {
			TextureDataReq reqData;
			reqData.SerializeFrom( payload );
			SendTextureData(tl, tm, Bolts::stringHash_t(reqData.texID), reqData.texMip);
		} );

		tl.RegisterCommandHandler( c_cnTextureListReq, [tm] ( ToolListener& tl, uint32_t, serialization::Blob&&) {
			SendTextureList( tl , tm);
		} );
	}

	void SendTextureData( ToolListener& tl, Bolts::TextureManager* tm, Bolts::stringHash_t texID, uint8_t mipLevel )
	{
		BASSERT( tm );

		if ( tm->HasTexture( texID )) {
			auto texPtr = tm->GetTexture( texID );
			serialization::Blob commandParameters;
			TextureImageData reqData;
			//Stats
			reqData.format = (GPUTextureFormat) texPtr->GetFormat();
			reqData.width = (uint16_t) texPtr->GetDimensions().x;
			reqData.height = (uint16_t)texPtr->GetDimensions().y;
			reqData.depth = (uint16_t) texPtr->GetDimensions().z;
			//Data
			unsigned texDataSize = texPtr->GetReadStorageSize( Bolts::Rendering::BTF_RGBA, 0);
			reqData.data.resize( texDataSize );
			texPtr->ReadPixels( Bolts::Rendering::BTF_RGBA, mipLevel, reqData.data);
			//Send it
			reqData.SerializeTo( commandParameters );
			tl.QueueCommand( c_cnTextureData, std::move( commandParameters ) );
		}
	}

	void SendTextureList( ToolListener&, Bolts::TextureManager* tm )
	{
		tm;
		BASSERT( tm );
		//
		//serialization::Blob commandParameters;
		//TextureListData	reqData;
		//TextureListData::TexInfo tinfo;
		//const auto& texList = tm->GetNamedTexturesList();
		//for ( auto const& texData : texList ) {
		//	tinfo.id = texData.first;
		//	tinfo.name = texData.second;
		//	reqData.infos.push_back(tinfo);
		//}
		//reqData.SerializeTo( commandParameters );
		//tl.QueueCommand( c_cnTextureList, std::move( commandParameters ) );
	}
}