#include "StdAfx.h"
#include "BoltsTC/Textures/Client.h"
#include "Bolts/Core/BinarySerialization.h"
#include <Rendering/Managers/TextureManager.h>
#include <Rendering/Texture.h>


namespace tools
{
	void operator<<( serialization::Blob& storage, Bolts::uvec3 val )
	{
		storage.Store( val.x );
		storage.Store( val.y );
		storage.Store( val.z );
	}

	void SendTextureData( ToolListener* tl, Bolts::TextureManager* tm, Bolts::stringHash_t texID )
	{
		BASSERT( tl );
		BASSERT( tm );

		if ( tm->HasTexture( texID )) {
			auto texPtr = tm->GetTexture( texID );
			serialization::Blob commandParameters;

			//Stats
			commandParameters.Store( (uint32_t) texPtr->GetFormat() );
			commandParameters << texPtr->GetDimensions();
			//Data
			unsigned texDataSize = texPtr->GetReadStorageSize( Bolts::Rendering::BTF_RGBA, 0);
			commandParameters.Store( texDataSize );
			serialization::blob_t dataBuffer( texDataSize );//TODO: Fix useless copy here
			texPtr->ReadPixels( Bolts::Rendering::BTF_RGBA, 0, dataBuffer );
			commandParameters.Store( dataBuffer.data(), dataBuffer.size() );

			//Send it
			tl->QueueCommand( Bolts::HashMM("GetTextureData"), std::move( commandParameters ) );
		}
	}

	void SendTextureList( ToolListener* tl, Bolts::TextureManager* tm )
	{
		tl; tm;
		BASSERT( tl );
		BASSERT( tm );

		//serialization::Blob commandParameters;
		//const auto& texList = tm->GetNamedTexturesList();
		//commandParameters.Store( (uint32_t) texList.size() );
		//for ( auto texData : texList ) {
		//	commandParameters.Store( texData.first );
		//	commandParameters.Store( texData.second );
		//}
		//tl->QueueCommand( Bolts::HashCString32( "ListTextures" ), std::move( commandParameters ) );

	}
}