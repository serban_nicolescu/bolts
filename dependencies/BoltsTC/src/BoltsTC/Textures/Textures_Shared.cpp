#include "StdAfx.h"
#include "BoltsTC/Textures/Shared.h"
#include "Bolts/Core/BinarySerialization.h"


bool	TextureDataReq::SerializeTo( serialization::Blob& blob) const
{
	blob.Store( texID );
	blob.Store( texMip );
	return true;
}

bool	TextureDataReq::SerializeFrom( serialization::Blob& blob)
{
	blob.Load( texID );
	blob.Load( texMip );
	return true;
}

bool	TextureImageData::SerializeTo( serialization::Blob& blob) const
{
	blob.Store( width );
	blob.Store( height );
	blob.Store( depth );
	blob.Store( data.data(), data.size() );
	blob.Store( format );
	return true;
}

bool	TextureImageData::SerializeFrom( serialization::Blob& blob)
{
	blob.Load( width );
	blob.Load( height );
	blob.Load( depth );
	blob.Load( data );
	blob.Load( format );

	return true;
}