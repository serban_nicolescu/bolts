#include "StdAfx.h"
#include "BoltsTC/ToolListener.h"
#include <nn.h>
#include <pair.h>

namespace {
	const char* c_toolListenerLT = "[ToolListener] ";
};

//Used for sending both requests and replies
struct CommandHeader
{
	uint32_t commandCode;//> The command requested
	uint32_t commandTag; //> The identifier to use when sending back the reply. UNUSED
	uint32_t payloadSize;//> Size in bytes of parameter buffer
};


ToolListener::ToolListener():
	m_cheaderBuffer(nullptr), m_nextMessageTag(0)
{

}

ToolListener::~ToolListener()
{
	if ( m_cheaderBuffer ) {
		nn_freemsg( m_cheaderBuffer );
	}

	CleanUpSocket();
}

void ToolListener::StartListening()
{
	Init( true);
}

void ToolListener::Connect()
{
	Init( false );
}

void ToolListener::Init( bool isHost)
{
	BASSERT( m_cheaderBuffer == nullptr );

	m_socket = nn_socket( AF_SP, NN_PAIR );

	int rc = 0;
	if (isHost)
		rc = nn_bind( m_socket, "ipc:///tmp/test.ipc" );
	else
		rc = nn_connect( m_socket, "ipc:///tmp/test.ipc" );

	if ( rc == 0 ) {
		//If binding failed
		CleanUpSocket();
		return;
	}

	m_cheaderBuffer = nn_allocmsg( sizeof( CommandHeader ), 0 );
	m_receivingBuffer.resize( 1024 );
}

void ToolListener::CleanUpSocket()
{
	if ( m_socket >= 0 )
		nn_shutdown( m_socket, 0 );
}

void ToolListener::QueueCommand( uint32_t commandCode, serialization::Blob&& payload )
{	
	m_queuedCommands.push_back( QueuedCommand { commandCode, std::move( payload ) } );
}

void ToolListener::RegisterCommandHandler( uint32_t commandCode, commandHandler_t ch)
{
	BASSERT( m_handlerMap.count( commandCode ) == 0 );

	m_handlerMap[commandCode] = ch;
}

void ToolListener::Update()
{
	if ( m_socket >= 0 ) {
		ReadMessage();
		SendQueuedMessage();
	}
}

void ToolListener::ReadMessage()
{
	BASSERT( m_socket >= 0 );

	//Listen for messages
	struct nn_msghdr hdr;
	struct nn_iovec iov[2];
	CommandHeader chdr;
	const char c_minBytesToRead = sizeof( chdr );
	iov[0].iov_base = &chdr;
	iov[0].iov_len	= c_minBytesToRead;
	iov[1].iov_base = m_receivingBuffer.data();
	iov[1].iov_len	= m_receivingBuffer.size();
	memset( &hdr, 0, sizeof( hdr ) );
	hdr.msg_iov = iov;
	hdr.msg_iovlen = 2;

	BASSERT( errno == 0 );//Find out if we are hiding errno errors ( this system is so bad ! )
	int rv = nn_recvmsg( m_socket, &hdr, NN_DONTWAIT );
	if ( rv > 0 )
	{
		const unsigned bytesRead = rv;
		if ( bytesRead < c_minBytesToRead ) {
			BOLTS_LERROR() << c_toolListenerLT << "Read incomplete message from socket.Command header is malformed. Size was: " << bytesRead << ".Minimum header size is: " << c_minBytesToRead;
		} else {
			unsigned expectedSize = sizeof( CommandHeader ) + chdr.payloadSize;
			if ( bytesRead <  expectedSize ) {
				BOLTS_LERROR() << c_toolListenerLT << "Read incomplete message from socket. Size was: " << bytesRead << ".Expected:" << expectedSize;
			} else {
				//Got valid command
				BOLTS_LINFO() << c_toolListenerLT << "Got command [" << chdr.commandCode << " | " << chdr.commandTag << "] Payload size: " << chdr.payloadSize;
				OnCommandReceived( chdr, m_receivingBuffer.data() );
			}
		}
	} else {
		if ( errno != EAGAIN ) {//If EAGAIN it means nothing is available to read, so we ignore it
			BOLTS_LERROR() << c_toolListenerLT << "Error received when reading from socket: " << errno;
		}
		errno = 0;
	}
}

void ToolListener::OnCommandReceived( CommandHeader& chdr, void* params)
{
	if ( m_handlerMap.count( chdr.commandCode ) != 0 ) {
		serialization::Blob data; //TODO: Useless copy. Change to ReadBlob
		data.Store( params, chdr.payloadSize );
		m_handlerMap[chdr.commandCode]( *this, chdr.commandCode, std::move( data ) );
	} else {
		BOLTS_LWARNING() << c_toolListenerLT << "Received unknown command code from tool: " << chdr.commandCode;
	}
}

void ToolListener::SendQueuedMessage()
{
	if ( m_queuedCommands.size() == 0 )
		return;

	auto& command = m_queuedCommands.back();

	struct nn_msghdr hdr;
	struct nn_iovec iov[2];
	CommandHeader chdr;
	chdr.commandCode = command.commandCode;
	chdr.commandTag = m_nextMessageTag++;
	chdr.payloadSize = command.payload.Size();
	const char c_bytesToSend = sizeof( chdr );
	iov[0].iov_base = &chdr;
	iov[0].iov_len = c_bytesToSend;
	iov[1].iov_base = command.payload.Data();
	iov[1].iov_len = chdr.payloadSize;
	memset( &hdr, 0, sizeof( hdr ) );
	hdr.msg_iov = iov;
	hdr.msg_iovlen = 2;

	if ( nn_sendmsg( m_socket, &hdr, 0 ) == 0 ) {
		BOLTS_LERROR() << c_toolListenerLT << "Error sending command [" << chdr.commandCode << "|" << chdr.commandTag << "]";
	}
	// Delete payload storage
	m_queuedCommands.pop_back();
}
