#include "bolts_alloc.h"
#include "bolts_assert.h"
#include <memory>

namespace Bolts {

	////////

	struct Mov
	{
		Mov() = default;
		Mov(Mov&&) = default;
		Mov& operator=(Mov&&) = default;

		int a, b, c;
	};

	template<typename T>
	struct Cont
	{
		Cont() = default;
		Cont(Cont&&) = default;
		Cont& operator=(Cont&&) = default;

		vec_t<T> bla;
	};

	struct Test
	{
		Cont<Mov> bla1;
		Cont<int> bla2;
	};

	void blatest()
	{
		Test t;
	}

	/////////

	FixedAllocator::FixedAllocator(size_t size)
	{
		m_start = m_current = (char*)malloc(size);
		m_end = m_start + size;
	}

	FixedAllocator::FixedAllocator(void * start, void * end)
	{
		m_start = m_current = (char*)start;
		m_end = (char*)end;
	}

	void * FixedAllocator::Alloc(size_t size, unsigned alignment)
	{
		// Align ptr
		m_current = m_current + alignAdjust((uintptr_t)m_current, alignment);
		void* userPtr = m_current;
		m_current += size;

		if (m_current >= m_end)
		{
			BASSERT_MSG(false,"Fixed allocator is out of memory");
			// out of memory
			return nullptr;
		}
		return userPtr;
	}

	void FixedAllocator::Reset(void)
	{
		m_current = m_start;
	}

	void* StupidLeakingAllocator::Alloc(size_t size, size_t)
	{
		m_allocations.push_back(malloc(size));
		return m_allocations.back();
	}

	void StupidLeakingAllocator::Reset()
	{
		for (void* mem : m_allocations) {
			free(mem);
		}
		m_allocations.clear();
	}

}
