#include "bolts_log.h"
#include <iostream>
#include <cstdlib> //For std::exit()

#ifdef WIN32
#include <windows.h>

namespace {
	const int g_levelColor[ Bolts::Log::_ELL_COUNT] = {
		FOREGROUND_RED | FOREGROUND_INTENSITY,
		FOREGROUND_RED,
		FOREGROUND_GREEN | FOREGROUND_BLUE,
		FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY,
		FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED,
	};

};
#endif

namespace {
	const char* g_levelName[ Bolts::Log::_ELL_COUNT] = {
		"F",
		"E",
		"W",
		"I",
		"D"
	};
};

namespace Bolts{

	unsigned Log::s_logLevel = Bolts::Log::ELL_DEBUG;
	unsigned Log::s_logChannels = unsigned(-1);


};

namespace Bolts{

	Log::Feed& Log::Feed::operator<<( float val )
	{
		std::cout << val;
		return *this;
	}

	Log::Feed& Log::Feed::operator<<( int val )
	{
		std::cout << val;
		return *this;
	}

	Log::Feed& Log::Feed::operator<<( unsigned val )
	{
		std::cout << val;
		return *this;
	}

	Log::Feed& Log::Feed::operator<<(unsigned long long val)
	{
		std::cout << val;
		return *this;
	}

	Log::Feed& Log::Feed::operator<<( const char* val )
	{
		if (val)
			std::cout << val;
		return *this;
	}
	Log::Feed& Log::Feed::operator<<( str_t val )
	{
		std::cout << val;
		return *this;
	}

	Log::Feed& Log::Feed::operator<<(vec2 val)
	{
		std::cout << val.x << " " << val.y;
		return *this;
	}

	Log::Feed& Log::Feed::operator<<(vec3 val)
	{
		std::cout << val.x << " " << val.y << " " << val.z;
		return *this;
	}

	Log::Feed& Log::Feed::operator<<(vec4 val)
	{
		std::cout << val.x << " " << val.y << " " << val.z << " " << val.w;
		return *this;
	}

	Log::Feed::~Feed()
	{
		std::cout << '\n';

#ifdef WIN32
		HANDLE hConsole = GetStdHandle( STD_OUTPUT_HANDLE );
		SetConsoleTextAttribute( hConsole, ( WORD ) FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);
#endif
		if ( m_logLevel <= ELL_WARNING) {
			std::cout << m_fileName << ':' << m_lineNo << '\n';
		}
	}

	Log::Feed::Feed(unsigned ll, unsigned lineNo, const char* fileName) :
		m_logLevel(ll), m_lineNo(lineNo), m_fileName(fileName)
	{
		//TODO: Add timestamp to log messages
#ifdef WIN32
		HANDLE hConsole = GetStdHandle( STD_OUTPUT_HANDLE );
		SetConsoleTextAttribute( hConsole, ( WORD ) g_levelColor[ll]);
#endif
		std::cout << '[' << g_levelName[ll] << "]";
	}

};