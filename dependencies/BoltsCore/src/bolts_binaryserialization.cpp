#include "bolts_binaryserialization.h"
#include <memory>
#include "array.h"
#include "memory.h"
#pragma warning(disable : 4456)
#include "memory_impl.hpp"
#pragma warning(default: 4456)

namespace Bolts
{
	Blob::Blob():
		m_storage( foundation::memory_globals::default_allocator())
	{
		//foundation::array::grow( m_storage, 30 );
	}

	Blob::Blob( unsigned capacity ):
		m_storage( foundation::memory_globals::default_allocator() )
	{
		foundation::array::set_capacity( m_storage, capacity);
	}

	Blob::Blob(Blob&& other):m_storage( std::move(other.m_storage))
	{
	}

	Blob::~Blob() {}

	Blob& Blob::operator=(Blob&& other)
	{
		m_storage = std::move(other.m_storage);
		return *this;
	}

	void Blob::MoveInto(Blob& other)
	{
		other.m_storage = std::move(m_storage);
	}

	void Blob::SetCapacity( unsigned newSize)
	{
		foundation::array::set_capacity( m_storage, (uint32_t)newSize );
	}

	void Blob::Fit( unsigned additionalNumBytes )//> Ensure there is enough storage space for given amount of bytes
	{
		using namespace foundation;
		const unsigned requiredStorage = array::size( m_storage ) + additionalNumBytes;
		array::resize( m_storage, requiredStorage );		
	}

	void* Blob::StoreInternal( const void* buffer, unsigned dataSize )
	{
		void* start = WillStore( dataSize );
		memcpy(start, buffer, dataSize );
		return start;
	}

	void* Blob::WillStore( unsigned extraDataSize )
	{
		const unsigned currentIdx = m_storage._size;
		Fit( extraDataSize );
		return &m_storage[currentIdx];
	}
};

//https://stackoverflow.com/questions/42895755/how-to-name-sections-groups-for-three-c-objects-when-using-init-seg
struct InitFoundation
{
	InitFoundation() {
		foundation::memory_globals::init();
	}
	~InitFoundation() {
		//TODO: IMPORTANT! This reports we still have allocated memory, and since it doesn't crash even after this call it means that memory never gets freed
		foundation::memory_globals::shutdown();
	}
};
static InitFoundation g_boltsBla;
