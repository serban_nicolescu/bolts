#include "bolts_hash.h"
#include "bolts_assert.h"
#include "MurmurHash/MurmurHash3.h"
#include <cstring>
#include <thread>
#include <mutex>
#include <atomic>

namespace {

	inline Bolts::hash32_t	HashCS32(uint32_t seed, const char* targetCStr, size_t strLen)
	{
		Bolts::hash32_t	outHash;
		MurmurHash3_x86_32( targetCStr, (int)strLen, seed, &outHash );
		return outHash;
	}
}

namespace Bolts{


	hash32_t HashString(const std::string& targetStr)
	{
		return HashCS32(detail::c_hashSeed, targetStr.c_str(), targetStr.length());
	}

	hash32_t HashString(uint32_t start, const std::string& targetStr)
	{
		return HashCS32(start, targetStr.c_str(), targetStr.length());
	}

	hash32_t	HashString(const char* targetCStr)
	{
		return HashCS32(detail::c_hashSeed, targetCStr, strlen(targetCStr));
	}

	hash32_t	HashString(uint32_t start, const char* targetCStr)
	{
		return HashCS32(start, targetCStr, strlen(targetCStr));
	}

	hash32_t	HashString(const char* targetCStr, size_t targetLen)
	{
		return HashCS32(detail::c_hashSeed, targetCStr, targetLen);
	}


	// Global strings table

	//string storage
	static const int c_strBufferAllocSize = 1024 << 6; // 64KB string pages
	static char* g_stringBuffer = nullptr;
	static char* g_stringStorageHead = nullptr;
	static std::atomic_flag g_stringBufferLocked = ATOMIC_FLAG_INIT;
	static std::atomic<int> g_numStrings = 0;

	static const char* StoreString(const char* s, size_t slen)
	{
		g_numStrings++;
		// copy & store string in global pool
		if (!g_stringBuffer || (g_stringStorageHead - g_stringBuffer + slen + 1) >= c_strBufferAllocSize) {
			BASSERT_MSG(!g_stringBuffer, "We shouldn't need to resize this buffer. This code is here to prevent crashes. Memory will leak.");
			//NOTE: When we hit >64KB of string we start leaking 64KB pages on shutdown. Who cares ?
			g_stringBuffer = g_stringStorageHead = new char[c_strBufferAllocSize];
		}
		// copy string
		char* newString = (char*)memcpy(g_stringStorageHead, s, slen + 1);
		g_stringStorageHead = newString + slen + 1;
		return newString;
	}

	//https://en.wikipedia.org/wiki/Cuckoo_hashing
	static constexpr int c_hashBits = 16;
	static_assert(c_hashBits * 2 <= sizeof(hash32_t) * 8, "There must be enough sets of bits available in our string hashes.");
	static constexpr hash32_t c_hashTableSize = 1 << c_hashBits;
	static constexpr hash32_t c_hashMask = c_hashTableSize - 1;
	
	//1.5 MB of static storage is being used here ...
	static const char* g_strings1[c_hashTableSize] = {};
	static hash32_t	g_hashes1[c_hashTableSize] = {};
	static const char* g_strings2[c_hashTableSize] = {};
	static hash32_t	g_hashes2[c_hashTableSize] = {};
	//static const char* g_strings3[c_hashTableSize] = {};
	//static hash32_t	g_hashes3[c_hashTableSize] = {};

	static inline hash32_t H1(hash32_t h) { return h & c_hashMask; }
	static inline hash32_t H2(hash32_t h) { return (h >> c_hashBits) & c_hashMask; }
	//static inline hash32_t H3(hash32_t h) { return (h >> (c_hashBits*2)) & c_hashMask; }

	const char* RegisterStringHash(hash32_t hash, const char* cstr, size_t slen)
	{
		if (!cstr)
			return nullptr;
		if (slen == 0)
			slen = strlen(cstr);
		// check for stored strings
		const char* oldStr = GetHashString(hash);
		if (oldStr)
			return oldStr;

		//NOTE: Intel suggests using PAUSE on spin-loops like this one
		//https://software.intel.com/en-us/node/524249
		// Lock
		while (!g_stringBufferLocked.test_and_set()) { ; }

		const char* strCpy = StoreString(cstr, slen);
		hash32_t newHash = hash;
		const char* newStr = strCpy;
		int maxLoop = 10;
		while (maxLoop--) {
			hash32_t key = H1(newHash);
			if (g_hashes1[key] == 0) {
				g_hashes1[key] = newHash;
				g_strings1[key] = newStr;
				return strCpy;
			}
			std::swap(g_hashes1[key], newHash);
			std::swap(g_strings1[key], newStr);
			key = H2(newHash);
			if (g_hashes2[key] == 0) {
				g_hashes2[key] = newHash;
				g_strings2[key] = newStr;
				return strCpy;
			}
			std::swap(g_hashes2[key], newHash);
			std::swap(g_strings2[key], newStr);
			//key = H3(newHash);
			//if (g_hashes3[key] == 0) {
			//	g_hashes3[key] = newHash;
			//	g_strings3[key] = newStr;
			//	return strCpy;
			//}
			//std::swap(g_hashes3[key], newHash);
			//std::swap(g_strings3[key], newStr);
		}
		BASSERT_MSG(false, "Too many collisions when storing string hash mapping");
		// Unlock
		g_stringBufferLocked.clear();
		return strCpy;
	}

	const char *	GetHashString(hash32_t hash)
	{
		//TODO: We should definitely also lock here ...

		// check for stored strings
		const uint32_t key1 = H1(hash);
		const uint32_t key2 = H2(hash);
		//const uint32_t key3 = H3(hash);
		if (g_hashes1[key1] == hash) {
			return g_strings1[key1];
		}
		else if (g_hashes2[key2] == hash) {
			return g_strings2[key2];
		}
		//else if (g_hashes3[key3] == hash) {
		//	return g_strings3[key3];
		//}
		else
			return nullptr;
	}

	void ClearStrings()
	{
		memset(g_hashes1, 0, sizeof(g_hashes1));
		memset(g_hashes2, 0, sizeof(g_hashes2));
		//memset(g_hashes3, 0, sizeof(g_hashes3));
		memset(g_strings1, 0, sizeof(g_strings1));
		memset(g_strings2, 0, sizeof(g_strings2));
		//memset(g_strings3, 0, sizeof(g_strings3));
		g_stringStorageHead = g_stringBuffer = nullptr;
	}
};

const char* HS(Bolts::hash32_t h) { return Bolts::GetHashString(h); }
