#include "bolts_binarymap.h"
#include "bolts_assert.h"
#include <algorithm>

namespace {
	const Bolts::BinaryMapBuilder::numKeys_t k_keyGrowthRate = 10;//> Increment to use when growing the keys buffer
	const Bolts::BinaryMapBuilder::dataLength_t k_dataGrowthRate = 128;//> Minimum increment to use when growing data buffer
}

using namespace Bolts;

void BinaryMap::Release()
{
	delete[] m_keyStorage;
	delete[] m_dataStorage;
	m_keyStorage = nullptr;
	m_dataStorage = nullptr;
}

BinaryMapBuilder::BinaryMapBuilder()
{
	ResetStorage();
}

BinaryMapBuilder::~BinaryMapBuilder()
{
	delete[] m_keyStorage;
	delete[] m_dataStorage;
}

bool BinaryMapBuilder::ResizeKeys(numKeys_t newKeyCapacity)
{
	if (newKeyCapacity == m_keyCapacity)
		return true;
	//First, get more room
	const unsigned keySize = sizeof(key_t);
	const size_t newSize = BinaryMapView::GetKeyStorageRequired(newKeyCapacity);
	char* newKeyStorage = new char[newSize];
	BASSERT(newKeyStorage);
	//Work out some values
	const size_t newKeysSize = sizeof(numKeys_t) + newKeyCapacity * keySize; //Get size of key-only storage
	offset_t*const newOffsetPtr = (offset_t*)(newKeyStorage + newKeysSize); //Update offset storage position
	if (m_keyStorage) { //Check if it isn't the first allocation
		//Copy key data to new buffer
		const unsigned keyCount = std::min( GetNumKeys(), newKeyCapacity);
		const size_t oldKeysSize = sizeof(numKeys_t) + keyCount * keySize; //Get size of old key data
		const size_t oldOffsetsSize = keyCount * sizeof(offset_t); //Get size of old offset data
		memcpy(newKeyStorage, m_keyStorage, oldKeysSize);
		memcpy(newOffsetPtr, m_offsetStorage, oldOffsetsSize);
		//Free old buffer
		delete[] m_keyStorage;
	}
	else {
		//Set initial key amount to 0
		*(numKeys_t*)(newKeyStorage) = 0;
	}
	//Update buffer ptr
	m_keyStorage = (key_t*)newKeyStorage;
	m_offsetStorage = newOffsetPtr;
	m_keyCapacity = newKeyCapacity;
	return true;
}

bool BinaryMapBuilder::ResizeData(dataLength_t newDataCapacity)
{
	newDataCapacity += sizeof(dataLength_t); // Also make sure to keep space for size value
	if (newDataCapacity == m_dataCapacity)
		return true;
	//First, get more room
	char* newDataStorage = new char[newDataCapacity];
	if (!newDataStorage) {
		return false;
	}
	//Copy old data
	if (m_dataStorage) { //Check if it isn't the first allocation
		memcpy(newDataStorage, m_dataStorage, GetDataSize());
		//Free old buffer
		delete[] m_dataStorage;
	}
	else {
		*(dataLength_t*)(newDataStorage) = sizeof(dataLength_t); //Set initial data size to point past buffer size indicator
	}
	//Update buffer ptr
	m_dataStorage = newDataStorage;
	m_dataCapacity = newDataCapacity;
	return true;
}

bool BinaryMapBuilder::StoreValue(key_t storeKey, const void* data, size_t size, unsigned alignment)
{
	BASSERT_MSG(size <= std::numeric_limits<offset_t>::max(), "Attempting to store binary struct member that is larger than maximum member size (currently 65KB )");
	//Check for key/offset space
	if (m_freeKeyIdx > m_keyCapacity) {
		if (!ResizeKeys(m_keyCapacity + k_keyGrowthRate)) {
			return false;
		}
	}
	//Work out size required
	const offset_t startAlignAdjust = (offset_t)alignAdjust(GetDataSize(), alignment);
	const offset_t sizeIncrease = (offset_t)size + startAlignAdjust;
	//Check for data space
	if (GetDataSize() + sizeIncrease > m_dataCapacity) {
		if (!ResizeData(m_dataCapacity + std::max(k_dataGrowthRate, (dataLength_t)sizeIncrease))) {
			return false;
		}
	}
	//Update keys and offsets
	m_keyStorage[m_freeKeyIdx] = storeKey;
	m_offsetStorage[m_freeKeyIdx - 1] = GetDataSize() + startAlignAdjust;
	*reinterpret_cast<hash32_t*>(m_keyStorage) = m_freeKeyIdx; //Update key count
	m_freeKeyIdx++;

	//Store data
	memcpy(m_dataStorage + GetDataSize() + startAlignAdjust, data, size);
	SetDataSize(GetDataSize() + sizeIncrease);
	return true;
}

void* Bolts::BinaryMapBuilder::ReserveArray(key_t storeKey, offset_t elemCount, offset_t elemSize, unsigned elemAlign)
{
	BASSERT_MSG(elemCount * size_t(elemSize) <= std::numeric_limits<offset_t>::max(), "Attempting to store binary struct member that is larger than maximum member size");
	//Check for key/offset space
	if (m_freeKeyIdx > m_keyCapacity) {
		if (!ResizeKeys(m_keyCapacity + k_keyGrowthRate)) {
			return nullptr;
		}
	}
	//Work out size required
	const offset_t startAlignAdjust = (offset_t)alignAdjust(GetDataSize() + sizeof(offset_t), alignof(offset_t));
	const offset_t dataAlignAdjust = (offset_t)alignAdjust(GetDataSize() + sizeof(offset_t) + startAlignAdjust, elemAlign);
	const offset_t arraySize = elemCount * elemSize;
	const offset_t sizeIncrease = sizeof(offset_t) + startAlignAdjust + dataAlignAdjust + arraySize;
	//Check for data space
	if (GetDataSize() + sizeIncrease > m_dataCapacity) {
		if (!ResizeData(m_dataCapacity + std::max(k_dataGrowthRate, (dataLength_t)sizeIncrease))) {
			return nullptr;
		}
	}
	//Update keys and offsets
	m_keyStorage[m_freeKeyIdx] = storeKey;
	m_offsetStorage[m_freeKeyIdx - 1] = GetDataSize() + startAlignAdjust;
	*reinterpret_cast<hash32_t*>(m_keyStorage) = m_freeKeyIdx; //Update key count
	m_freeKeyIdx++;

	//Store size and then aligned data buffer
	char* currentDataStart = m_dataStorage + GetDataSize() + startAlignAdjust;
	memcpy(currentDataStart, &elemCount, sizeof(offset_t));
	currentDataStart += dataAlignAdjust + sizeof(offset_t);
	SetDataSize(GetDataSize() + sizeIncrease);
	return currentDataStart;
}

bool Bolts::BinaryMapBuilder::StoreArray(key_t storeKey, const void * data, offset_t elemCount, offset_t elemSize, unsigned elemAlign)
{
	void* dataLoc = ReserveArray(storeKey, elemCount, elemSize, elemAlign);
	memcpy(dataLoc, data, elemCount * elemSize);
	return true;
}

bool BinaryMapBuilder::StoreBM(hash32_t hash, const BinaryMapView& bm)
{
	//TODO: Align BM-type values. Data should be aligned to 8, since it can hold doubles
	auto storeKey = BinaryMapView::MakeKey<BinaryMapView>(hash);
	const auto size = bm.GetSizeInBytes();
	BASSERT_MSG(size <= std::numeric_limits<offset_t>::max(), "Attempting to store binary struct member that is larger than maximum member size (currently 65KB )");
	//Check for key/offset space
	if (m_freeKeyIdx > m_keyCapacity) {
		if (!ResizeKeys(m_keyCapacity + k_keyGrowthRate)) {
			return false;
		}
	}
	//Check for data space
	if (GetDataSize() + size > m_dataCapacity) {
		if (!ResizeData(m_dataCapacity + std::max(k_dataGrowthRate, (dataLength_t)size))) {
			return false;
		}
	}
	//Update keys and offsets
	m_keyStorage[m_freeKeyIdx] = storeKey;
	m_offsetStorage[m_freeKeyIdx - 1] = GetDataSize();
	*reinterpret_cast<hash32_t*>(m_keyStorage) = m_freeKeyIdx; //Update key count
	m_freeKeyIdx++;

	//Store key and then data buffer. Size of key buffer is first elem in it ( keysStorage_t ) 
	auto keysSize = bm.GetKeysSize();
	memcpy(m_dataStorage + GetDataSize(), bm.GetKeyBuffer(), keysSize);
	memcpy(m_dataStorage + GetDataSize() + keysSize, bm.GetDataBuffer(), bm.GetDataSize());
	SetDataSize(GetDataSize() + (offset_t)size);
	return true;
}

void BinaryMapBuilder::ResetStorage() //> Removes ownership of internal buffers and resets all variables. Object will behave as if newly constructed
{
	m_keyStorage = nullptr;
	m_offsetStorage = nullptr;
	m_keyCapacity = 0;
	m_freeKeyIdx = 1;//When building data, skip the first element since it holds the size of the buffer

	m_dataStorage = nullptr;
	m_dataCapacity = 0;//> In bytes
}

void BinaryMapBuilder::Clear()
{
	m_freeKeyIdx = 1;
}

BinaryMap BinaryMapBuilder::Build()
{
	ResizeKeys(GetNumKeys());//Make sure key buffer fits exactly, otherwise the BM will not index the data properly
							 //Pass ownership of data
	auto ks = m_keyStorage;
	auto ds = m_dataStorage;
	ResetStorage();
	//Build the binary map and return it
	return BinaryMap(ks, ds);
}

BinaryMapView BinaryMapBuilder::GetTempMap()
{
	ResizeKeys(GetNumKeys());//Make sure key buffer fits exactly, otherwise the BM will not index the data properly
							 //Build the binary map and return it
	return BinaryMapView((char*)m_keyStorage, (char*)m_dataStorage);
}