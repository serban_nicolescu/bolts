#include "bolts_binarymap_serialization.h"
#include "bolts_assert.h"

//> Stores a copy of the BinaryMap into the given Blob
Bolts::Blob& operator<< (Bolts::Blob& blob, const Bolts::BinaryMapView& map)
{
	blob.Store(map.GetKeyBuffer(), map.GetKeysSize());
	blob.Store(map.GetDataBuffer(), map.GetDataSize());
	return blob;
}

//> Maps the Blob to a BinaryMap
void operator>> (const Bolts::Blob& blob, Bolts::BinaryMapView& map)
{
	Bolts::Blob::Reader reader = blob.MakeReader();
	Bolts::BinaryMap::numKeys_t numKeys;
	reader.Peek(numKeys); // Read without advancing idx
	unsigned keyStorageSize = Bolts::BinaryMap::GetKeyStorageRequired(numKeys);
	void* keyBuffer;
	BASSERT(reader.Read(keyBuffer, keyStorageSize));
	Bolts::BinaryMap::numKeys_t dataStorageSize;
	reader.Peek(dataStorageSize);
	void* dataBuffer;
	BASSERT(reader.Read(dataBuffer, dataStorageSize));

	map = Bolts::BinaryMapView(keyBuffer, dataBuffer);
}
