#include "bolts_assert.h"
#include <vector>
#include <iostream>
#include <sstream>

//Windows
#include <intrin.h> //Needed for __debugbreak()
#include <windows.h> // MessageBox

namespace
{
	std::vector< Bolts::Core::assert::assertFunc_t>	s_assertHandlers = std::vector< Bolts::Core::assert::assertFunc_t>();
};

namespace Bolts {
	namespace Core
	{
		void AssertFailed(const char* file, unsigned line, const char* cond, const char* msg, bool* keepAssertActive)
		{
			for (auto handler : s_assertHandlers){
				if (!handler(file, line, cond, msg))
					*keepAssertActive = false;
			}
		}

		namespace assert
		{
			void AddAssertHandler(assertFunc_t newHandler)
			{
				s_assertHandlers.push_back(newHandler);
			}

			bool CoutAH(const char* file, unsigned line, const char* cond, const char* msg)
			{
				std::cout << "[" << file << ":" << line << "] Assertion hit: (" << cond << ").";
				if ( msg)
					std::cout << "Msg: " << msg;
				std::cout << '\n';
				return true;
			}

			bool BreakpointAH(const char* file, unsigned line, const char* cond, const char* msg)
			{
				std::stringstream header;
				header << "[" << file << ":" << line << "] Assertion hit";
				std::stringstream message;
				message << "Condition: " << cond << '\n';
				if (msg)
					message << "Msg: " << msg;
				message << '\n';

				int msgboxID = MessageBoxA(
					NULL,
					message.str().c_str(),
					header.str().c_str(),
					MB_ICONERROR | MB_ABORTRETRYIGNORE | MB_DEFBUTTON2 | MB_TASKMODAL
					);

				switch (msgboxID)
				{
				case IDABORT:
					return true;
				case IDRETRY:
					__debugbreak();
					return true;
				case IDIGNORE:
					return false;
				}
				return true;
			}
		}
	};
};