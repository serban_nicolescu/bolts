#pragma once
#include <bolts_types.h>
#include <bolts_assert.h>

namespace Bolts
{
	/*
	struct Test :public IndirectionVectorSOA<Test>
	{
		template<typename... Args>
		void Emplace(Args&&... args) {
			m_data.emplace_back(std::forward<Args>(args)...);
		}

		void PopBack()
		{
			m_data.pop_back();
		}
		uint32_t Size() const
		{
			return m_data.size();
		}
		void Swap(uint32_t idx1, uint32_t idx2)
		{
			std::swap(m_data[idx1], m_data[idx2]);
		}

		int Get(id_t id)
		{
			return m_data[GetIdx(id)];
		}

		Bolts::vec_t<int> m_data;
	};
	*/

	template<class Data>
	class IndirectionVectorSOA
	{
	public:
		typedef uint32_t id_t;

		template<typename... Args>
		id_t AddActive(Args&&... args)
		{
			id_t newId;
			if (m_freeIdList.empty()) {
				m_idToData.push_back(Derived()->Size());
				newId = (unsigned)(m_idToData.size() - 1);
			}
			else {
				newId = m_freeIdList.back();
				m_freeIdList.pop_back();
			}
			Derived()->Emplace( std::forward<Args>(args)...);
			//m_data.emplace_back(std::forward<Args>(args)...);
			m_dataToId.push_back(newId);
			m_idToData[newId] = Derived()->Size() - 1;

			Activate(newId);
			return newId;
		}

		void Remove(id_t id)
		{
			BASSERT(IsValid(id));
			// Removing moves the last active item into this id's slot and shortens the data vector
			auto deadIdx = m_idToData[id];
			// If there are any active items
			if (m_sizeActive) {
				// If removed item is active swap last active item with the last inactive one
				if (deadIdx < m_sizeActive){
					auto lastActiveIdx = m_sizeActive - 1;
					if (m_sizeActive < Derived()->Size())
					{
						SwapLast(lastActiveIdx);
					}
					m_sizeActive--;
				}
				// Now swap the last item with the one about to be removed
				SwapLast(deadIdx);
			}
			// Remove the last data element
			Derived()->PopBack();
			//m_data.pop_back();
			m_dataToId.pop_back();
			m_idToData[id] = uint32_t(-1); // Give id an invalid slot
			// Keep id for next add
			m_freeIdList.push_back(id);
		}

		void Deactivate(id_t id)
		{
			BASSERT(IsValid(id));
			// Removing moves the last active item into this id's slot and shortens the data vector
			auto currentIdx = m_idToData[id];
			if (currentIdx >= m_sizeActive)
				return;
			// If removed item is not the last active item
			if (currentIdx + 1 < m_sizeActive) {
				auto lastActiveIdx = m_sizeActive - 1;
				SwapItems(currentIdx, lastActiveIdx);
			}
			m_sizeActive--;
		}

		void Activate(id_t id)
		{
			BASSERT(IsValid(id));
			// Removing moves the last active item into this id's slot and shortens the data vector
			auto currentIdx = m_idToData[id];
			if (currentIdx < m_sizeActive)
				return;
			// If activated item is not the first inactive item
			if (currentIdx > m_sizeActive) {
				//auto lastInactiveIdx = m_sizeActive;
				SwapItems(currentIdx, m_sizeActive);
			}
			m_sizeActive++;
		}

		bool IsActive(id_t id) const {
			return (IsValid(id) && (m_idToData[id] < m_sizeActive));
		}

		bool IsValid(id_t id) const {
			return (id < m_idToData.size());
		}

		uint32_t SizeActive() const {
			return m_sizeActive;
		}

		uint32_t GetIdx(id_t id) const
		{
			BASSERT(IsValid(id));
			return m_idToData[id];
		}
	private:

		Data* Derived() { return static_cast<Data*>(this); }

		void SwapItems(uint32_t idx1, uint32_t idx2)
		{
			//std::swap(m_data[idx1], m_data[idx2]);
			Derived()->Swap(idx1, idx2);
			id_t id1 = m_dataToId[idx1];
			id_t id2 = m_dataToId[idx2];
			m_dataToId[idx2] = id1;
			m_dataToId[idx1] = id2;
			m_idToData[id1] = idx2;
			m_idToData[id2] = idx1;
		}

		void SwapLast(uint32_t idx1)
		{
			SwapItems(idx1, Derived()->Size() - 1);
		}

		uint32_t		m_sizeActive = 0;
		vec_t<id_t>		m_dataToId;
		vec_t<uint32_t> m_idToData;
		vec_t<id_t>		m_freeIdList;
	};
};