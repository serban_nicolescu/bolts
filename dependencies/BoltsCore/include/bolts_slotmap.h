#pragma once
#include <bolts_types.h>
#include <bolts_assert.h>
#include <collection_types.h>
#include <array.h>
#include <memory.h>

namespace Bolts
{
	template<class T, bool CallConstructor>
	class SlotMap
	{
	public:
		SlotMap() :m_data(foundation::memory_globals::default_allocator()) {}
		~SlotMap() {
			if (CallConstructor)
			{
				if (m_firstFree == badHandle)
					BASSERT(Capacity() == 0);
				else {
					unsigned numFree = 0;
					while (m_firstFree != badHandle) {
						numFree++;
						m_firstFree = *reinterpret_cast<id_t*>(&m_data[m_firstFree]);
					}
					BASSERT(Capacity() == numFree);
				}
				
			}
		}
		SlotMap(const SlotMap&) = default;
		SlotMap(SlotMap&& other) = default;
		SlotMap& operator=(const SlotMap&) = default;
		SlotMap& operator=(SlotMap&& other) = default;

		typedef uint32_t id_t;
		static_assert(sizeof(T) > sizeof(id_t), "The free list is stored inplace, so elements need to be large enough");
		typedef efficient_return<T> get_t;
		typedef efficient_return<const T> get_const_t;
		
		inline uint32_t Capacity() const {
			return foundation::array::size(m_data);
		}

		inline void Reserve(size_t newCapacity)
		{
			foundation::array::reserve(m_data, newCapacity);
		}

		inline get_t Get(id_t id)
		{
			BASSERT(IsValid(id));
			return m_data[id];
		}

		inline get_const_t Get(id_t id) const
		{
			BASSERT(IsValid(id));
			return m_data[id];
		}

		template<typename... Args>
		id_t Add(Args&&... args)
		{
			id_t freeID;
			if (m_firstFree != badHandle) {
				freeID = m_firstFree;
				m_firstFree = *reinterpret_cast<id_t*>(&m_data[m_firstFree]);
			}
			else {
				unsigned oldSize = foundation::array::size(m_data);
				foundation::array::resize(m_data, oldSize + 1);
				freeID = id_t(oldSize);
			}

			if (CallConstructor)
				new (&m_data[freeID]) T(std::forward<Args>(args)...);

			return freeID;
		}

		void Remove(id_t id)
		{
			BASSERT(IsValid(id));

			if (CallConstructor)
				m_data[id].~T();

			if (id != foundation::array::size(m_data) - 1) {
				*reinterpret_cast<id_t*>(&m_data[id]) = m_firstFree;
				m_firstFree = id;
			}
			else
				foundation::array::pop_back(m_data);
		}

		void Clear()
		{
			static_assert(!CallConstructor, "Can't clear when we want dtors be called");
			foundation::array::resize(m_data, 0);
			m_firstFree = badHandle;
		}

	private:
		bool IsValid(id_t id) const {
			return (id < Capacity());
		}

		foundation::Array<T>	m_data;
		id_t					m_firstFree = badHandle;
	};
};