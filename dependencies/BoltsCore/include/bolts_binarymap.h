#pragma once
#include <bolts_types.h>
#include <bolts_hash.h>

namespace Bolts {

	// Non-owning view over binary map data
	class BinaryMapView
	{
	public:
		friend class BinaryMapBuilder;
		template<typename DataT>
		friend class BinaryMapArrayView;

		typedef uint32_t key_t;
		typedef uint32_t numKeys_t;
		static_assert(sizeof(numKeys_t) == sizeof(key_t), "numKeys_t is expected to be the same sizeof as key_t");
		typedef uint32_t dataOffset_t;

		BinaryMapView() {}
		BinaryMapView(const BinaryMapView& other) {
			m_keyStorage = other.m_keyStorage;
			m_dataStorage = other.m_dataStorage;
		}
		BinaryMapView(void* keyStorage, void* dataStorage)
		{
			m_keyStorage = static_cast<key_t*>(keyStorage);
			m_dataStorage = static_cast<char*>(dataStorage);
		}

		BinaryMapView& operator=(const BinaryMapView& other) {
			m_keyStorage = other.m_keyStorage;
			m_dataStorage = other.m_dataStorage;
			return *this;
		}

		bool IsValid() const
		{
			return (m_keyStorage && m_dataStorage);
		}

		void Clear()
		{
			m_dataStorage = nullptr;
			m_keyStorage = nullptr;
		}

		void Set(stringHash_t key, bool in) { SetTmpl(key, in); }
		void Set(stringHash_t key, float in) { SetTmpl(key, in); }
		void Set(stringHash_t key, double in) { SetTmpl(key, in); }
		void Set(stringHash_t key, int8_t in) { SetTmpl(key, in); }
		void Set(stringHash_t key, uint8_t in) { SetTmpl(key, in); }
		void Set(stringHash_t key, int16_t in) { SetTmpl(key, in); }
		void Set(stringHash_t key, uint16_t in) { SetTmpl(key, in); }
		void Set(stringHash_t key, int32_t in) { SetTmpl(key, in); }
		void Set(stringHash_t key, uint32_t in) { SetTmpl(key, in); }
		void Set(stringHash_t key, ivec2 in) { SetTmpl(key, in); }
		void Set(stringHash_t key, ivec3 in) { SetTmpl(key, in); }
		void Set(stringHash_t key, ivec4 in) { SetTmpl(key, in); }
		void Set(stringHash_t key, uvec2 in) { SetTmpl(key, in); }
		void Set(stringHash_t key, uvec3 in) { SetTmpl(key, in); }
		void Set(stringHash_t key, uvec4 in) { SetTmpl(key, in); }
		void Set(stringHash_t key, vec2 in) { SetTmpl(key, in); }
		void Set(stringHash_t key, vec3 in) { SetTmpl(key, in); }
		void Set(stringHash_t key, vec4 in) { SetTmpl(key, in); }
		void Set(stringHash_t key, quat in) { SetTmpl(key, in); }
		void Set(stringHash_t key, const mat3& in) { SetTmpl(key, in); }
		void Set(stringHash_t key, const mat4& in) { SetTmpl(key, in); }

		bool	Get(stringHash_t key, bool& out)		const { return GetValue(key, out); }
		bool	Get(stringHash_t key, float& out)		const { return GetValue(key, out); }
		bool	Get(stringHash_t key, double& out)		const { return GetValue(key, out); }
		bool	Get(stringHash_t key, int8_t& out)		const { return GetValue(key, out); }
		bool	Get(stringHash_t key, uint8_t& out)		const { return GetValue(key, out); }
		bool	Get(stringHash_t key, int16_t& out)		const { return GetValue(key, out); }
		bool	Get(stringHash_t key, uint16_t& out)	const { return GetValue(key, out); }
		bool	Get(stringHash_t key, int32_t& out)		const { return GetValue(key, out); }
		bool	Get(stringHash_t key, uint32_t& out)	const { return GetValue(key, out); }
		bool	Get(stringHash_t key, ivec2& out)		const { return GetValue(key, out); }
		bool	Get(stringHash_t key, ivec3& out)		const { return GetValue(key, out); }
		bool	Get(stringHash_t key, ivec4& out)		const { return GetValue(key, out); }
		bool	Get(stringHash_t key, uvec2& out)		const { return GetValue(key, out); }
		bool	Get(stringHash_t key, uvec3& out)		const { return GetValue(key, out); }
		bool	Get(stringHash_t key, uvec4& out)		const { return GetValue(key, out); }
		bool	Get(stringHash_t key, vec2& out)		const { return GetValue(key, out); }
		bool	Get(stringHash_t key, vec3& out)		const { return GetValue(key, out); }
		bool	Get(stringHash_t key, vec4& out)		const { return GetValue(key, out); }
		bool	Get(stringHash_t key, quat& out)		const { return GetValue(key, out); }
		bool	Get(stringHash_t key, mat3& out)		const { return GetValue(key, out); }
		bool	Get(stringHash_t key, mat4& out)		const { return GetValue(key, out); }
		//> NOTE: You can also use PeekString to get a null-terminated char*
		bool	Get(stringHash_t key, str_t& out)	const
		{
			auto storeKey = MakeKey<char*>(key);
			void* dataPtr = GetDataPtr(storeKey);
			if (!dataPtr)
				return false;
			out = static_cast<const char*>(dataPtr);
			return true;
		}
		bool	Get(stringHash_t key, const char*& out)	const
		{
			auto storeKey = MakeKey<char*>(key);
			void* dataPtr = GetDataPtr(storeKey);
			if (!dataPtr)
				return false;
			out = static_cast<const char*>(dataPtr);
			return true;
		}

		bool	Get(stringHash_t key, BinaryMapView& out)	const
		{
			auto storeKey = MakeKey<BinaryMapView>(key);
			void* dataPtr = GetDataPtr(storeKey);
			if (!dataPtr)
				return false;
			const size_t keyDataBytes = GetKeyStorageRequired(*static_cast<numKeys_t*>(dataPtr));
			out.m_keyStorage = static_cast<key_t*>(dataPtr);
			out.m_dataStorage = static_cast<char*>(dataPtr) + keyDataBytes;
			return true;
		}

		const char* PeekString(stringHash_t key) const //> Returns ptr to a null-terminated string. Zero-copy. NOTE: Returns nullptr if not found
		{
			return (char*)GetDataPtr(MakeKey<char*>(key));
		}

		template<typename T, typename = std::enable_if_t<pass_by_value<T>()> >
		BinaryMapArrayView<T> GetArray(stringHash_t hash)
		{
			key_t key = MakeArrayKey<T>(hash);
			dataOffset_t* data = static_cast<dataOffset_t*>(GetDataPtr(key));
			if (!data)
				return BinaryMapArrayView<T>();
			// Work out aligned data start
			const dataOffset_t arraySize = *data;
			return BinaryMapArrayView<T>(key, alignPtr<T>(data + 1), arraySize);
		}

		template<typename T, typename = std::enable_if_t<pass_by_value<T>()> >
		BinaryMapArrayView<const T> GetArray(stringHash_t hash) const
		{
			key_t key = MakeArrayKey<T>(hash);
			dataOffset_t* data = static_cast<dataOffset_t*>(GetDataPtr(key));
			if (!data)
				return BinaryMapArrayView<const T>();
			// Work out aligned data start
			const dataOffset_t arraySize = *data;
			return BinaryMapArrayView<const T>(key, alignPtr<T>(data + 1), arraySize);
		}

		// Number of items in map
		numKeys_t GetSize() const
		{
			return (m_keyStorage ? *m_keyStorage : 0);
		}

		// Size in bytes of key and offset data
		dataOffset_t GetKeysSize() const {
			return sizeof(numKeys_t) + GetSize()*(sizeof(key_t) + sizeof(dataOffset_t));//First 4 bytes in keyStorage is number of elements
		}

		// Return the size in bytes of the data buffer
		dataOffset_t GetDataSize() const {
			return (m_dataStorage ? *(dataOffset_t*)m_dataStorage : 0);//First element in dataStorage is total number of bytes of buffer
		}

		// Returns total size in bytes of both data and key mapping
		dataOffset_t GetSizeInBytes() const {
			return GetKeysSize() + GetDataSize();
		}

		// Internal buffer peeking. Change these at your own peril !
		const key_t*	GetKeyBuffer()const { return m_keyStorage; }
		const void*		GetDataBuffer()const { return m_dataStorage; }

		class BinaryMapIt
		{
		public:
			BinaryMapIt() = default;
			BinaryMapIt(const BinaryMapIt&) = default;
			BinaryMapIt(key_t*	keyStorage, char* dataStorage, numKeys_t current, numKeys_t last) :
				m_keyStorage(keyStorage), m_dataStorage(dataStorage), m_current(current), m_count(last) {}

			BinaryMapIt& operator=(const BinaryMapIt&) = default;

			bool operator!=(const BinaryMapIt& other)
			{
				return (other.m_current != m_current) || (other.m_dataStorage != m_dataStorage);
			}

			BinaryMapIt operator*() {
				return *this;
			}

			BinaryMapIt operator+(size_t offset) {
				BinaryMapIt next = *this;
				next.m_current += (numKeys_t)offset;
				return next;
			}

			BinaryMapIt& operator++() {
				m_current++;
				return *this;
			}

			BinaryMapIt operator[](stringHash_t hash) {
				BinaryMapView tempBm;
				return Get(tempBm) ? tempBm[hash] : BinaryMapIt();
			}

			bool IsValid() const
			{
				return m_current <= m_count;
			}

			inline bool KeyIs(stringHash_t hash) const
			{
				return KeyMatchHash(m_keyStorage[m_current], hash);
			}

			template< typename T>
			inline bool TypeIs() const
			{
				return KeyMatchType<T>(m_keyStorage[m_current]);
			}

			template< typename T>
			inline bool TypeIs(const T&) const
			{
				return KeyMatchType<T>(m_keyStorage[m_current]);
			}

			bool	Get(bool& out)		const { return GetValue(out); }
			bool	Get(float& out)		const { return GetValue(out); }
			bool	Get(double& out)	const { return GetValue(out); }
			bool	Get(int8_t& out)	const { return GetValue(out); }
			bool	Get(uint8_t& out)	const { return GetValue(out); }
			bool	Get(int16_t& out)	const { return GetValue(out); }
			bool	Get(uint16_t& out)	const { return GetValue(out); }
			bool	Get(int32_t& out)	const { return GetValue(out); }
			bool	Get(uint32_t& out)	const { return GetValue(out); }
			bool	Get(vec2& out)		const { return GetValue(out); }
			bool	Get(vec3& out)		const { return GetValue(out); }
			bool	Get(vec4& out)		const { return GetValue(out); }
			bool	Get(quat& out)		const { return GetValue(out); }
			bool	Get(mat3& out)		const { return GetValue(out); }
			bool	Get(mat4& out)		const { return GetValue(out); }
			//> NOTE: You can also use PeekString to get a null-terminated char*
			bool	Get(std::string& out)	const
			{
				if (!IsValid() || !TypeIs<char*>())
					return false;
				char* dataStartPtr = m_dataStorage + GetDataOffset(m_count, m_keyStorage + m_current);
				out = dataStartPtr;
				return true;
			}
			bool	Get(const char*& out)	const
			{
				if (!IsValid() || !TypeIs<char*>())
					return false;
				char* dataStartPtr = m_dataStorage + GetDataOffset(m_count, m_keyStorage + m_current);
				out = dataStartPtr;
				return true;
			}
			bool	Get(BinaryMapView& out)	const
			{
				if (!IsValid() || !TypeIs<BinaryMapView>())
					return false;
				void* dataStartPtr = m_dataStorage + GetDataOffset(m_count, m_keyStorage + m_current);
				const size_t keyDataBytes = GetKeyStorageRequired(*static_cast<numKeys_t*>(dataStartPtr));
				out.m_keyStorage = static_cast<key_t*>(dataStartPtr);
				out.m_dataStorage = static_cast<char*>(dataStartPtr) + keyDataBytes;
				return true;
			}

			template<typename T, typename = std::enable_if_t<pass_by_value<T>()> >
			BinaryMapArrayView<T> AsArray()
			{
				key_t key = m_keyStorage[m_current];
				if (!IsValid() || !KeyMatchArrayType<T>(key))
					return BinaryMapArrayView<T>();
				dataOffset_t* dataPtr = reinterpret_cast<dataOffset_t*>(m_dataStorage + GetDataOffset(m_count, m_keyStorage + m_current));
				// Work out aligned data start
				const dataOffset_t arraySize = *dataPtr;
				return BinaryMapArrayView<T>(key, alignPtr<T>(dataPtr + 1), arraySize);
			}

		private:
			//Getters
			template< typename T>
			inline bool GetValue(T& val) const
			{
				if (!IsValid() || !TypeIs<T>())
					return false;
				void* dataPtr = m_dataStorage + GetDataOffset(m_count, m_keyStorage + m_current);
				val = *static_cast<T*>(dataPtr);
				return true;
			}

			template< typename T>
			inline T GetValueCopy(T default) const
			{
				if (!IsValid() || !TypeIs<T>())
					return default;
				void* dataPtr = m_dataStorage + GetDataOffset(m_count, m_keyStorage + m_current);
				return *static_cast<T*>(dataPtr);
			}

			key_t*	m_keyStorage = nullptr;
			char*	m_dataStorage = nullptr;
			numKeys_t m_current = 1; //> NOTE: 1-based index into map data
			numKeys_t m_count = 0;
		};

		BinaryMapIt begin()
		{
			return { m_keyStorage, m_dataStorage, 1, GetSize() };
		}

		BinaryMapIt end()
		{
			return { m_keyStorage, m_dataStorage, GetSize() + 1, GetSize() };
		}
		
		BinaryMapIt operator[](stringHash_t hash)
		{
			if (!m_keyStorage || !m_dataStorage)
				return BinaryMapIt();
			//Look for key in array
			numKeys_t	numKeys = GetSize();
			key_t* keyLoc = m_keyStorage + 1;
			key_t* lastKey = m_keyStorage + 1 + numKeys;
			while ((keyLoc <= lastKey) && (!KeyMatchHash(*keyLoc, hash))) {
				keyLoc++;
			}
			return BinaryMapIt{ m_keyStorage, m_dataStorage, numKeys_t(keyLoc - m_keyStorage), numKeys };
		}

		BinaryMapIt Find(const char* keyString)
		{
			return operator[](HashString(keyString));
		}

		static unsigned GetKeyStorageRequired(numKeys_t numKeys) { return sizeof(numKeys_t) + numKeys * (sizeof(key_t) + sizeof(dataOffset_t)); }

	protected:
		static const unsigned c_keyTypeBits = 8;
		static const key_t c_keyHashMask = key_t(-1) << c_keyTypeBits;
		static const key_t c_keyHashArrayMask = key_t(-1) << (c_keyTypeBits - 1);
		static const key_t c_keyTypeMask = ~c_keyHashMask;
		static const key_t c_keyTypeArrayMask = 1 << (c_keyTypeBits - 1); // last key type bit is 1 for arrays

		enum ParamType : uint32_t
		{
			PT_FLOAT, PT_DOUBLE, 
			PT_BOOL, 
			PT_INT8, PT_INT16, PT_INT32, PT_INT64, PT_UINT8, PT_UINT16, PT_UINT32, PT_UINT64,
			PT_IVEC2, PT_IVEC3, PT_IVEC4,
			PT_UVEC2, PT_UVEC3, PT_UVEC4,
			PT_VEC2, PT_VEC3, PT_VEC4, 
			PT_QUAT, 
			PT_MAT4, PT_MAT3, 
			PT_STRING, 
			PT_BINARY_MAP
		};

		template< ParamType EnumValue>
		struct EnumValueAssoc
		{
			enum : uint32_t { value = EnumValue };
		};

		template< typename T>
		struct EnumForType {};//Default undefined for unsupported types
		template<>	struct EnumForType< double> : public EnumValueAssoc< PT_DOUBLE> {};
		template<>	struct EnumForType< float> : public EnumValueAssoc< PT_FLOAT> {};
		template<>	struct EnumForType< bool> : public EnumValueAssoc< PT_BOOL> {};
		template<>	struct EnumForType< int8_t> : public EnumValueAssoc< PT_INT8> {};
		template<>	struct EnumForType< uint8_t> : public EnumValueAssoc< PT_UINT8> {};
		template<>	struct EnumForType< int16_t> : public EnumValueAssoc< PT_INT16> {};
		template<>	struct EnumForType< uint16_t> : public EnumValueAssoc< PT_UINT16> {};
		template<>	struct EnumForType< int32_t> : public EnumValueAssoc< PT_INT32> {};
		template<>	struct EnumForType< uint32_t> : public EnumValueAssoc< PT_UINT32> {};
		template<>	struct EnumForType< int64_t> : public EnumValueAssoc< PT_INT64> {};
		template<>	struct EnumForType< uint64_t> : public EnumValueAssoc< PT_UINT64> {};
		template<>	struct EnumForType< ivec2> : public EnumValueAssoc< PT_IVEC2> {};
		template<>	struct EnumForType< ivec3> : public EnumValueAssoc< PT_IVEC3> {};
		template<>	struct EnumForType< ivec4> : public EnumValueAssoc< PT_IVEC4> {};
		template<>	struct EnumForType< uvec2> : public EnumValueAssoc< PT_UVEC2> {};
		template<>	struct EnumForType< uvec3> : public EnumValueAssoc< PT_UVEC3> {};
		template<>	struct EnumForType< uvec4> : public EnumValueAssoc< PT_UVEC4> {};
		template<>	struct EnumForType< vec2> : public EnumValueAssoc< PT_VEC2> {};
		template<>	struct EnumForType< vec3> : public EnumValueAssoc< PT_VEC3> {};
		template<>	struct EnumForType< vec4> : public EnumValueAssoc< PT_VEC4> {};
		template<>	struct EnumForType< quat> : public EnumValueAssoc< PT_QUAT> {};
		template<>	struct EnumForType< mat3> : public EnumValueAssoc< PT_MAT3> {};
		template<>	struct EnumForType< mat4> : public EnumValueAssoc< PT_MAT4> {};
		template<>	struct EnumForType< char*> : public EnumValueAssoc< PT_STRING> {};
		template<>	struct EnumForType< BinaryMapView> : public EnumValueAssoc< PT_BINARY_MAP> {};

		//

		template< typename T>
		static inline key_t MakeKey(stringHash_t hash)
		{
			return key_t((hash & c_keyHashMask) | EnumForType<T>::value);
		}

		template< typename T>
		static inline key_t MakeArrayKey(stringHash_t hash)
		{
			return key_t((hash & c_keyHashMask) | EnumForType<T>::value | c_keyTypeArrayMask);
		}

		template< typename T>
		static inline bool KeyMatch(key_t key, stringHash_t hash)
		{
			return MakeKey<T>(hash) == key;
		}

		template< typename T>
		static inline bool KeyMatchArray(key_t key, stringHash_t hash)
		{
			return MakeArrayKey<T>(hash) == key;
		}

		// Match keys of elements to hash
		static inline bool KeyMatchHash(key_t key, stringHash_t hash)
		{
			return key_t(hash & c_keyHashMask) == (key & c_keyHashMask);
		}

		// Match keys of non-array elements to hash
		static inline bool KeyMatchHashNoAray(key_t key, stringHash_t hash)
		{
			return key_t(hash & c_keyHashMask) == (key & c_keyHashArrayMask);
		}

		// Match keys of elements to non-array type
		template< typename T>
		static inline bool KeyMatchType(key_t key)
		{
			return EnumForType<T>::value == (key & c_keyTypeMask);
		}

		// Match keys of elements to array type
		template< typename T>
		static inline bool KeyMatchArrayType(key_t key)
		{
			return (EnumForType<T>::value | c_keyTypeArrayMask) == (key & c_keyTypeMask);
		}

		//Setters
		template< typename T>
		inline void SetTmpl(key_t key, T in)
		{
			auto storeKey = MakeKey<T>(key);
			SetValue(storeKey, &in, sizeof(in));
		}

		inline void SetTmpl(key_t key, const mat3& in)
		{
			auto storeKey = MakeKey<mat3>(key);
			SetValue(storeKey, &in, sizeof(in));
		}

		inline void SetTmpl(key_t key, const mat4& in)
		{
			auto storeKey = MakeKey<mat4>(key);
			SetValue(storeKey, &in, sizeof(in));
		}

		inline void SetTmpl(key_t key, const std::string& in)
		{
			auto storeKey = MakeKey<char*>(key);
			SetValue(storeKey, in.c_str(), in.length() + 1);//Copy string along with null-terminator
		}

		inline void SetTmpl(key_t key, const char* in, size_t length)
		{
			auto storeKey = MakeKey<char*>(key);
			SetValue(storeKey, in, length + 1);//Expects null-terminator to be included in buffer
		}

		inline void SetValue(key_t storeKey, const void* data, size_t size)
		{
			void* dataLoc = GetDataPtr(storeKey);
			if (dataLoc) {
				memcpy(dataLoc, data, size);
			}
		}

		//Getters
		template< typename T>
		inline bool GetValue(key_t key, T& val) const
		{
			auto storeKey = MakeKey<T>(key);
			void* dataPtr = GetDataPtr(storeKey);
			if (dataPtr) {
				val = *static_cast<T*>(dataPtr);
			}
			return dataPtr != nullptr;
		}

		template< typename T>
		inline T GetValueCopy(key_t key, T default) const
		{
			auto storeKey = MakeStoreKey<T>(key);
			void* dataPtr = GetDataPtr(storeKey);
			if (dataPtr) {
				return *static_cast<T*>(dataPtr);
			}
			return default;
		}

		void* GetDataPtr(key_t fullKey) const
		{
			if (!m_keyStorage || !m_dataStorage)
				return nullptr;
			//TODO PERF LOW: Could be optimised if key array were sorted
			//Look for key in array
			size_t	numKeys = GetSize();
			key_t* keyLoc = m_keyStorage + 1;
			key_t* lastKey = m_keyStorage + 1 + numKeys;
			while ((keyLoc <= lastKey) && (*keyLoc != fullKey)) {
				keyLoc++;
			}
			//Return data ptr or nullptr if not found
			return (keyLoc > lastKey ? nullptr : m_dataStorage + GetDataOffset(numKeys, keyLoc));
		}

		static inline dataOffset_t	GetDataOffset(size_t numKeys, key_t* keyPtr)
		{
			return *(dataOffset_t*)(keyPtr + numKeys);
		}

		inline dataOffset_t*	GetFirstOffsetPtr(numKeys_t numKeys) const {
			return (dataOffset_t*)(m_keyStorage + 1 + numKeys);
		}

		//Key data should look like
		// [numKeys_t * 1][key_t * numKeys][offset_t * numKeys]
		// First 4 bytes are numKeys
		key_t*	m_keyStorage = nullptr;
		//Data is expected to have a dataLength_t at the start 
		char*	m_dataStorage = nullptr;
	};


	template<typename T>
	class ArrayIt
	{
	public:
		ArrayIt() = default;
		ArrayIt(const ArrayIt&) = default;
		ArrayIt(T* dataStart, size_t count) :
			m_dataStart(dataStart), m_dataEnd(dataStart + count), m_current(dataStart) {}

		ArrayIt& operator=(const ArrayIt&) = default;

		bool operator!=(const ArrayIt& other)
		{
			return (other.m_current != m_current) || (other.m_dataStart != m_dataStart);
		}

		ArrayIt& operator++() {
			m_current++;
			return *this;
		}

		ArrayIt& operator+=(size_t offset) {
			m_current += offset;
			return *this;
		}

		ArrayIt operator+(size_t offset) {
			ArrayIt newIt(*this);
			newIt += offset;
			return newIt;
		}

		T	operator*() const
		{
			return IsValid() ? *m_current : T{};
		}

		bool IsValid() const
		{
			return (m_current && m_current < m_dataEnd);
		}

		uint32_t Index() const
		{
			return m_current - m_dataStart;
		}

	private:
		T*	m_dataStart = nullptr;
		T*	m_dataEnd = nullptr;
		T*	m_current = nullptr;
	};

	//TODO: Add support for arrays of strings and BinaryMaps
	template<typename DataT>
	class BinaryMapArrayView
	{
	public:
		BinaryMapArrayView() = default;
		BinaryMapArrayView(const BinaryMapArrayView&) = default;
		BinaryMapArrayView(BinaryMapView::key_t key, DataT* dataStorage, size_t count) :
			m_key(key), m_dataBegin(dataStorage), m_count(count) {}

		BinaryMapArrayView& operator=(const BinaryMapArrayView&) = default;

		// Iterator support
		ArrayIt<DataT> begin()
		{
			return ArrayIt<DataT>(m_dataBegin, m_count);
		}

		ArrayIt<DataT> end()
		{
			return (begin() + m_count);
		}

		DataT operator[](std::size_t idx) {
			if (idx < 0 || idx >= m_count)
				return *ArrayIt<DataT>();

			return *(begin() + idx);
		}
		// API
		bool IsValid() const
		{
			return m_dataBegin != nullptr;
		}

		uint32_t GetSize() const
		{
			return m_count;
		}

		inline bool KeyIs(stringHash_t hash) const
		{
			return BinaryMapView::KeyMatchHash(m_key, hash);
		}

		template< typename T>
		inline bool TypeIs() const
		{
			return false;
		}

		template<>
		inline bool TypeIs<DataT>() const
		{
			return true;
		}

		template< typename T>
		inline bool TypeIs(T&) const
		{
			return false;
		}
		
		template<>
		inline bool TypeIs<DataT>(DataT&) const
		{
			return false;
		}
	private:
		BinaryMapView::key_t	m_key = 0;
		DataT*					m_dataBegin = nullptr;
		uint32_t				m_count = 0;
	};

	class BinaryMap :public BinaryMapView
	{
	public:
		BinaryMap() = default;
		BinaryMap(void* keyStorage, void* dataStorage) :BinaryMapView(keyStorage, dataStorage) {}
		BinaryMap(BinaryMap&& other) {
			m_keyStorage = other.m_keyStorage;
			m_dataStorage = other.m_dataStorage;
			other.m_keyStorage = nullptr;
			other.m_dataStorage = nullptr;
		}
		BinaryMap(const BinaryMap&) = delete;

		BinaryMap& operator=(BinaryMap&& other) {
			m_keyStorage = other.m_keyStorage;
			m_dataStorage = other.m_dataStorage;
			other.m_keyStorage = nullptr;
			other.m_dataStorage = nullptr;
			return *this;
		}
		BinaryMap& operator=(const BinaryMap& other) = delete;

		~BinaryMap() {
			Release();
		}

		//inline const BinaryMap operator* () const {
		//	return BinaryMap(m_keyStorage, m_dataStorage);
		//}
		//
		//inline BinaryMap operator*() {
		//	return BinaryMap(m_keyStorage, m_dataStorage);
		//}

		void Release();
	};

	//Class used to build dynamic maps that hold generic binary data
	//Used to make a BinaryMap
	class BinaryMapBuilder
	{
	public:
		typedef BinaryMapView::key_t		key_t;
		typedef BinaryMapView::numKeys_t	numKeys_t;
		typedef BinaryMapView::dataOffset_t	dataLength_t;
		typedef BinaryMapView::dataOffset_t	offset_t;

		BinaryMapBuilder();
		~BinaryMapBuilder();

		bool ResizeKeys(numKeys_t newKeyAmount); //>Change key capacity. If called before first Store it will not copy anything
		bool ResizeData(dataLength_t newDataAmount);//>Change data capacity. If called before first Store it will not copy anything. NOTE: It will also add room for storing the buffer size at the start of the buffer.
		void Clear(); //> Removes all keys and values but keeps storage

		BinaryMap Build();//> Returns a BinaryMap owning the built data and resets the Builder's storage
		BinaryMapView GetTempMap();//> Returns a temporary BinaryMap containing the built data. WARNING: Returned map is only valid until next Store(),Fit() or Resize() call

		void Store(stringHash_t hash, float in) { StoreTmpl(hash, in); }
		void Store(stringHash_t hash, double in) { StoreTmpl(hash, in); }
		void Store(stringHash_t hash, bool in) { StoreTmpl(hash, in); }
		void Store(stringHash_t hash, int8_t in) { StoreTmpl(hash, in); }
		void Store(stringHash_t hash, uint8_t in) { StoreTmpl(hash, in); }
		void Store(stringHash_t hash, int16_t in) { StoreTmpl(hash, in); }
		void Store(stringHash_t hash, uint16_t in) { StoreTmpl(hash, in); }
		void Store(stringHash_t hash, int32_t in) { StoreTmpl(hash, in); }
		void Store(stringHash_t hash, uint32_t in) { StoreTmpl(hash, in); }
		void Store(stringHash_t hash, int64_t in) { StoreTmpl(hash, in); }
		void Store(stringHash_t hash, uint64_t in) { StoreTmpl(hash, in); }
		void Store(stringHash_t hash, ivec2 in) { StoreTmpl(hash, in); }
		void Store(stringHash_t hash, ivec3 in) { StoreTmpl(hash, in); }
		void Store(stringHash_t hash, ivec4 in) { StoreTmpl(hash, in); }
		void Store(stringHash_t hash, uvec2 in) { StoreTmpl(hash, in); }
		void Store(stringHash_t hash, uvec3 in) { StoreTmpl(hash, in); }
		void Store(stringHash_t hash, uvec4 in) { StoreTmpl(hash, in); }
		void Store(stringHash_t hash, vec2 in) { StoreTmpl(hash, in); }
		void Store(stringHash_t hash, vec3 in) { StoreTmpl(hash, in); }
		void Store(stringHash_t hash, vec4 in) { StoreTmpl(hash, in); }
		void Store(stringHash_t hash, quat in) { StoreTmpl(hash, in); }
		void Store(stringHash_t hash, const mat3& in) { StoreTmpl(hash, in); }
		void Store(stringHash_t hash, const mat4& in) { StoreTmpl(hash, in); }
		void Store(stringHash_t hash, const str_t& in) { StoreCStr(hash, in.c_str(), in.length()); }
		//NOTE: Expects null-terminated string. Will copy length+1
		void Store(stringHash_t hash, const char* in) { StoreCStr(hash, in, strlen(in)); }
		void Store(stringHash_t hash, const BinaryMapView& in) { StoreBM(hash, in); }

		template<typename T, typename = std::enable_if_t<pass_by_value<T>()> >
		void Store(stringHash_t hash, const vec_t<T>& in)
		{
			auto storeKey = BinaryMapView::MakeArrayKey<T>(hash);
			StoreArray(storeKey, in.data(), in.size(), sizeof(T), alignof(T));
		}

		template<typename T>
		void* ReserveArray(stringHash_t hash, size_t numElements)
		{
			auto storeKey = BinaryMapView::MakeArrayKey<T>(hash);
			return ReserveArray(storeKey, numElements, sizeof(T), alignof(T));
		}
	private:

		template< typename T, typename = std::enable_if_t< pass_by_value<T>()> >
		inline void StoreTmpl(hash32_t hash, T in)
		{
			auto storeKey = BinaryMapView::MakeKey<T>(hash);
			StoreValue(storeKey, &in, sizeof(in), alignof(T));
		}

		template< typename T, typename = std::enable_if_t< !pass_by_value<T>()> >
		inline void StoreTmpl(hash32_t hash, const T& in)
		{
			auto storeKey = BinaryMapView::MakeKey<T>(hash);
			StoreValue(storeKey, &in, sizeof(in), alignof(T));
		}

		inline void StoreCStr(hash32_t hash, const char* in, size_t length)
		{
			auto storeKey = BinaryMapView::MakeKey<char*>(hash);
			StoreValue(storeKey, in, length + 1, alignof(char));//Expects null-terminator to be included in buffer
		}

		bool	StoreValue(key_t storeKey, const void* data, size_t size, unsigned alignment);
		void*	ReserveArray(key_t storeKey, offset_t elemCount, offset_t elemSize, unsigned elemAlign); //> Store array info and make room. Returns ptr to array data, size of elemCount*elemSize
		bool	StoreArray(key_t storeKey, const void * data, offset_t elemCount, offset_t elemSize, unsigned elemAlign);
		bool	StoreBM(hash32_t hash, const BinaryMapView& value);

		void ResetStorage(); //> Removes ownership of internal buffers and resets all variables. Object will behave as if newly constructed

		//Length utility functions
		inline offset_t	GetDataSize() const {
			return (m_dataStorage ? *(dataLength_t*)(m_dataStorage) : 0);
		}

		void SetDataSize(offset_t newSize) {
			if (m_dataStorage) {
				*(dataLength_t*)(m_dataStorage) = newSize;
			}
		}
		numKeys_t GetNumKeys() const {
			return (m_keyStorage ? *(numKeys_t*)(m_keyStorage) : 0);
		}

		void SetNumKeys(numKeys_t newSize) {
			if (m_keyStorage) {
				*(numKeys_t*)(m_keyStorage) = newSize;
			}
		}

		key_t*		m_keyStorage;
		offset_t*	m_offsetStorage;//>Points to first offset, matching the first key
		numKeys_t	m_keyCapacity; //> In number of keys
		unsigned	m_freeKeyIdx;

		char*			m_dataStorage;
		dataLength_t	m_dataCapacity;//> In bytes
	};

};
