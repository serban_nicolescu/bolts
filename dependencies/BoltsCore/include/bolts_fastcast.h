#pragma once
//Thank you to Sree Kotay, 2006
// Implementation found here: http://stereopsis.com/sree/fpu2006.html

//Warning: Code only works for little endian
#include <stdint.h>

namespace Bolts{
	namespace Helpers{

	namespace detail{
		// ===================================================================================================================
		//  Constants
		// ====================================================================================================================
		const double _xs_doublemagic             = double (6755399441055744.0);//2^52 * 1.5,  uses limited precision to floor
		const double _xs_doublemagicdelta        = (1.5e-8);                   //almost .5f = .5f + 1e^(number of exp bit)
		const double _xs_doublemagicroundeps     = (.5f-_xs_doublemagicdelta); //almost .5f = .5f - 1e^(number of exp bit)
	};

	inline int32_t CRoundToInt(double val)
	{
		val = val + detail::_xs_doublemagic;
		return  ((int32_t*)&val)[0];
	}
	inline int32_t ToInt(double val)
	{
		 return (val<0) ?   CRoundToInt(val-detail::_xs_doublemagicroundeps) : CRoundToInt(val+detail::_xs_doublemagicroundeps);
	}
	inline int32_t FloorToInt(double val)
	{
		return CRoundToInt (val - detail::_xs_doublemagicroundeps);
	}
	inline int32_t CeilToInt(double val)
	{
		return CRoundToInt (val + detail::_xs_doublemagicroundeps);
	}
	inline int32_t RoundToInt(double val)
	{
		 return CRoundToInt (val + detail::_xs_doublemagicdelta);
	}

	};
};