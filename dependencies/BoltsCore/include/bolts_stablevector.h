
#include <bolts_types.h>
#include <bolts_assert.h>
#include <memory>

namespace Bolts {

	/*
		Simple vector-like class that holds elements in contiguous "pages"
		Useful properties:
			Elements are stable in memory even after addition and removal of other elements
			Iterators are never invalidated
			Push-back cost of new elements does not depend on current number of elements
		Caveats:
			Memory is never freed, except when the entire stucture gets destroyed
			Iterating efficiently is a bit of a bitch ( see GetPage )
	*/
	template< class ValT, uint16_t PageSize>
	class StableVector
	{
	public:
		struct It
		{
			uint16_t	pageIdx;
			uint16_t	idx;
		};

		static const unsigned k_pageArraySize = PageSize;
		struct PageHeader
		{
			uint16_t	GetSize() inline const { return end - head; }
			ValT*		Get(uint16_t idx) const {
				if (idx < GetSize())
					return head + idx;
				else
					return nullptr;
			}

			std::unique_ptr<ValT[k_pageArraySize]>	head;
			ValT*					end;
		};

		const static It	k_invalidIt = { -1U, -1U };

		const PageHeader GetPage(It it)
		{
			return (it.pageIdx < m_pages.size() ? m_pages[it.pageIdx] : PageHeader());
		}

		ValT* TryGet(It it)
		{
			PageHeader page = GetPage(it);
			return page.Get(it.idx);
		}

		bool IsValid(It it) inline
		{
			PageHeader page = GetPage(it);
			return (it.idx < page.GetSize());
		}

		It	Add()
		{
			if (m_freeSlots.empty())
			{
				PageHeader page = m_pages.back();
				if (page.GetSize() < k_pageArraySize)
				{
					*page.end = ValT();
					page.end++;
				}
				else {
					AllocPages(1);
					PageHeader page = m_pages.back();
					*page.end = ValT();
					page.end++;
				}
			}
			else {
				It freeIt = m_freeSlots.back();
				m_freeSlots.pop_back();
				*TryGet(freeIt) = ValT();
				return freeIt;
			}
		}

		void Remove(It it)
		{
			//TODO: We need to at least clear empty pages
			BASSERT(IsValid(it) && (std::find(m_freeSlots.begin(), m_freeSlots.end(), it) == m_freeSlots.end()));
			m_freeSlots.push_back(it);
		}

	private:

		void AllocPages(uint16_t count)
		{
			auto oldSize = m_pages.size();
			m_pages.reserve(m_pages.size() + count);
			for (unsigned i = count; i > 0; --i)
			{
				PageHeader page;
				page.head = new ValT[k_pageArraySize];
				page.end = page.head.get();
				m_pages.push_back(page);
			}
		}

		vec_t< PageHeader>	m_pages;
		vec_t< It >			m_freeSlots;

	};

};