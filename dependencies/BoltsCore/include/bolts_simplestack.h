#pragma once

#include <bolts_assert.h>
#include <bolts_types.h>

namespace Bolts{



/*
namespace detail{
	namespace SimpleStack{
		template< typename T>
		struct ptr_is_safe
		{
		};
	};
};

//A simple stack implemented using a std::vector.
//Treats input elements as POD-types (e.g uses memcpy for copying)
//Minimum element size is 4bytes
//Supports element addition
//	But no removal
//TODO: Add function for minimizing memory use
//			Or alloc fixed amount
//TODO: Switch to BitSquid foundation::array internally and be done with it
//TODO SAFETY: Add check for has_trivial_copy for the types
struct SimpleStack
{
	typedef char		elem_t;
	typedef uint16_t	index_t;
	typedef uint16_t	allocSize_t;

	SimpleStack() = default;
	SimpleStack(SimpleStack&& other) = default;
	SimpleStack(const SimpleStack& other) = default;
	SimpleStack& operator=(const SimpleStack& other) = default;
	SimpleStack& operator=(SimpleStack&& other) = default;

	static_assert( sizeof(elem_t) == 1,"");

	void	Clear(){
		m_buffer.clear ();
	}

	template< typename T>
	index_t	Insert( typename boost::call_traits< T >::param_type newElem, std::enable_if_t< !std::is_pointer<T>(), T>* temp = nullptr)
	{
		temp;
		//BOOST_STATIC_ASSERT( sizeof(T) >= sizeof(elem_t));
		const allocSize_t elemNum = sizeof(T);
		index_t idx = Alloc( elemNum);
		Set< typename boost::call_traits< T >::param_type >( idx, newElem);

		return idx;
	}

	template< typename T>
	index_t	Insert( typename detail::SimpleStack::ptr_is_safe<T>::ptr_type newElem)
	{
		typedef typename detail::SimpleStack::ptr_is_safe<T>::ptr_type insertElem_t;
		//BOOST_STATIC_ASSERT( sizeof(insertElem_t) >= sizeof(elem_t));
		const allocSize_t elemNum = sizeof(insertElem_t);// / sizeof( elem_t);
		index_t idx = Alloc( elemNum);
		Set< T >( idx, newElem);
		return idx;
	}

	template< typename T>
	typename boost::call_traits<T>::const_reference Get ( index_t position) const
	{
		BASSERT( IsIndexValid<T>(position));
		return reinterpret_cast< typename boost::call_traits<T>::const_reference> (m_buffer[position]);
	}

	template< typename T>
	typename boost::call_traits<T>::reference Get(index_t position)
	{
		BASSERT(IsIndexValid<T>(position));
		return reinterpret_cast< typename boost::call_traits<T>::reference> (m_buffer[position]);
	}

	template< typename T>
	typename boost::call_traits<T>::reference Edit(index_t position)
	{
		BASSERT(IsIndexValid<T>(position));
		return reinterpret_cast< typename boost::call_traits<T>::reference> (m_buffer[position]);
	}

	template< typename T>
	void	Set ( index_t position, typename boost::call_traits< std::enable_if_t< !std::is_pointer<T>(), T> >::param_type newValue)
	{
		BASSERT( IsIndexValid<T>(position));
		memcpy( static_cast<void*> (&m_buffer[position]), static_cast<const void*> (&newValue), sizeof(T));
	}
	template< typename T>
	void	Set ( index_t position, typename detail::SimpleStack::ptr_is_safe<T>::ptr_type newValue) //Selectively enabling ptrs to certain types
	{
		typedef typename detail::SimpleStack::ptr_is_safe<T>::ptr_type ptr_type;
		BASSERT( IsIndexValid<ptr_type>(position));
		memcpy( static_cast<void*> (&m_buffer[position]), &newValue, sizeof(ptr_type));
	}

	template< typename T>
	bool	IsIndexValid( index_t position) const
	{
		return ((position + (sizeof(T) / sizeof( elem_t)) - 1) < m_buffer.size());
	}

	index_t	Alloc( allocSize_t numElems){
		index_t currentIdx = static_cast<index_t>(m_buffer.size());
		m_buffer.resize ( m_buffer.size() + numElems);
		return currentIdx;
	}

	vec_t< elem_t>	m_buffer;
};

*/

};
