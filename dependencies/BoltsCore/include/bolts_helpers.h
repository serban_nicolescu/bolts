#pragma once

namespace Bolts {

	template<class Cont, class Val>
	bool contains(const Cont& container, const Val& value)
	{
		return std::find(container.begin(), container.end(), value) != container.end();
	}

	template<class Val>
	inline Val pop_get(vec_t<Val>& vector)
	{
		Val temp = std::move(vector.back());
		vector.pop_back();
		return std::move(temp);
	}

	template<class Cont, class Val>
	bool try_remove(Cont& container, const Val& value)
	{
		auto it = std::find(container.begin(), container.end(), value);
		if (it == container.end())
			return false;

		container.erase(it);
		return true;
	}

	//template< class Key, class Val>
	//Val* try_find( const map_t<Key, Val* >& map, Key key )
	//{
	//	auto it = map.find( key );
	//	return it == map.end() ? nullptr : it->second;
	//}

	template< class Key, class Val>
	Val* try_find(const hash_map_t<Key, Val* >& map, Key key)
	{
		auto it = map.find(key);
		return it == map.end() ? nullptr : it->second;
	}

	//template<class Key, class Val>
	//const Val* try_find(const map_t<Key, Val >& map, Key key)
	//{
	//	auto it = map.find(key);
	//	return it == map.end() ? nullptr : &it->second;
	//}

	template<class Key, class Val>
	const Val* try_find(const hash_map_t<Key, Val >& map, Key key)
	{
		auto it = map.find(key);
		return it == map.end() ? nullptr : &it->second;
	}

	template<class Cont, class Key, class Val>
	void map_replace(Cont& container, Key key, Val&& value)
	{
		auto it = container.find(key);
		if (it == container.end())
			container.emplace(key, std::move(value));
		else
			it->second = std::move(value);
	}

	//blob stack
	template<class V, BOLTS_TYPE_IS_POD(V)>
	int	stack_add(blob_t& blob, V val)
	{
		int location = blob.size();
		blob.resize(location + sizeof(V));
		//*reinterpret_cast<V*>(blob.data() + location) = val; //NOTE: don't assign, use memcopy so compiler knows it's unaligned
		memcpy(blob.data() + location, &val, sizeof(V));
		return location;
	}

	BOLTS_INLINE uint8_t* stack_addsize(blob_t& blob, int size)
	{
		int location = blob.size();
		blob.resize(location + size);
		return blob.data() + location;
	}

};

