#pragma once

#include <bolts_types.h>

namespace Bolts
{
	//TODO: https://blog.molecular-matters.com/2013/01/29/memory-allocation-strategies-a-growing-stack-like-lifo-allocator/
	//API: https://blog.molecular-matters.com/2011/07/07/memory-system-part-2/
	//https://msdn.microsoft.com/en-us/library/windows/desktop/aa366887(v=vs.85).aspx
	//PageSize: https://msdn.microsoft.com/en-us/library/windows/desktop/ms724958(v=vs.85).aspx

	//https://blog.molecular-matters.com/2012/08/14/memory-allocation-strategies-a-linear-allocator/

	class IAllocator {
	public:
		virtual ~IAllocator();

		virtual void* Alloc(size_t size, size_t alignment) = 0;
		virtual void Free(void* ptr) = 0;
	};

	class Mallocator
	{
	public:

		void* Alloc(size_t size, size_t /*alignment*/)
		{
			return malloc(size);
		}

		inline void Free(void* ptr) {
			free(ptr);
		}
	};

	//TODO: This doesn't destroy the mem it owns, so the explicit ctor leaks
	class FixedAllocator
	{
	public:
		explicit FixedAllocator(size_t size);
		FixedAllocator(void* start, void* end);

		void* Alloc(size_t size, unsigned alignment);

		inline void Free(void*) {}

		void Reset(void);

	//private:
		char* m_start;
		char* m_end;
		char* m_current;
	};

	class StupidLeakingAllocator
	{
	public:
		void* Alloc(size_t size, size_t alignment);

		inline void Free(void*) {}

		void Reset(void);

		//private:
		vec_t<void*> m_allocations;
	};

	template<class T>
	class MakeGeneric : public IAllocator, T
	{
	public:
		virtual ~MakeGeneric() {}

		void* Alloc(size_t size, size_t alignment) override
		{
			T::Alloc(size, alignment);
		}

		void Free(void* ptr) override {
			T::Free(ptr);
		}
	};

};

#define BOLTS_NEW(allocPtr, T) new ((allocPtr)->Alloc(sizeof(T), alignof(T))) T
#define BOLTS_DELETE(allocPtr, T, p) { if (p) {(p)->~T(); (allocPtr)->Free(p);} }