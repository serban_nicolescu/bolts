//Intentionally left outside include guard
#ifndef BOLTS_LOG_MAX_LEVEL
#define BOLTS_LOG_MAX_LEVEL ::Bolts::Log::ELL_DEBUG
#endif

#pragma once

#include <bolts_types.h>

namespace Bolts
{
	class Log
	{
		static unsigned s_logLevel;
		static unsigned s_logChannels;//TODO: Add support for logging channels
	public:
		enum Level{
			ELL_FATAL = 0,
			ELL_ERROR,
			ELL_WARNING,
			ELL_INFO,
			ELL_DEBUG,
			_ELL_COUNT
		};

		static unsigned ReportingLevel() { return s_logLevel; }
		static void		SetReportingLevel( unsigned newLevel) { s_logLevel = newLevel; }

		class Feed{

		unsigned m_lineNo;
		const char* m_fileName;
		unsigned m_logLevel;

		public:
			Feed( unsigned ll, unsigned lineNo, const char* fileName);
			~Feed();

			Feed& operator<< ( float val);
			Feed& operator<< ( int val);
			Feed& operator<< ( unsigned val);
			Feed& operator<< ( unsigned long long val);
			Feed& operator<< ( const char* val);
			Feed& operator<< ( str_t val );
			Feed& operator<< ( vec2 val);
			Feed& operator<< ( vec3 val);
			Feed& operator<< ( vec4 val);
		};
	};

};

#define BOLTS_LOG(level) \
	if (level > BOLTS_LOG_MAX_LEVEL) ;\
	else if (level > ::Bolts::Log::ReportingLevel()) ; \
	else ::Bolts::Log::Feed(level, __LINE__, __FILE__) \

#define BOLTS_LFATAL()		BOLTS_LOG( ::Bolts::Log::ELL_FATAL)
#define BOLTS_LERROR()		BOLTS_LOG( ::Bolts::Log::ELL_ERROR)
#define BOLTS_LWARNING()	BOLTS_LOG( ::Bolts::Log::ELL_WARNING)
#define BOLTS_LINFO()		BOLTS_LOG( ::Bolts::Log::ELL_INFO)
#define BOLTS_LDEBUG()		BOLTS_LOG( ::Bolts::Log::ELL_DEBUG)
