#pragma once

namespace Bolts {
	namespace Core {
		namespace assert {
			typedef bool( *assertFunc_t )( const char* file, unsigned line, const char* cond, const char* msg );

			void AddAssertHandler( assertFunc_t );

			bool CoutAH( const char* file, unsigned line, const char* cond, const char* msg );
			bool BreakpointAH( const char* file, unsigned line, const char* cond, const char* msg ); //> Uses platform-specific function to add a breakpoint when an assert is hit
		};

		void AssertFailed(const char* file, unsigned line, const char* cond, const char* msg, bool* keepAssertEnabled); //> Will call all registered handlers with the given parameters
	};
};

#define BASSERT_MSG( condition, msg)	\
		do { static bool s_assertEnabled = true;   \
			if (!(condition) && s_assertEnabled){ \
				::Bolts::Core::AssertFailed(__FILE__, __LINE__, #condition, msg, &s_assertEnabled); \
			} } while(0)

#define BASSERT( condition )	\
		do { static bool s_assertEnabled = true;   \
			if (!(condition) && s_assertEnabled){  \
				::Bolts::Core::AssertFailed(__FILE__, __LINE__, #condition, nullptr,&s_assertEnabled); \
			} } while(0)

#define BASSERT_ONLY( code ) code