#pragma once

#include <bolts_binarymap.h>
#include <bolts_binaryserialization.h>

//> Stores a copy of the BinaryMap into the given Blob
Bolts::Blob& operator<< (Bolts::Blob& blob, const Bolts::BinaryMapView& map);
//> Creates a BinaryMap from the given Blob. NOTE: BinaryMap memory still owned by the Blob
void operator>> (const Bolts::Blob& blob, Bolts::BinaryMapView& map);
