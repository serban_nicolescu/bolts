#pragma once

#include <bolts_types.h>
#include <bolts_assert.h>
#include <collection_types.h>
#include <array.h>
#include <vector>

namespace Bolts
{

	namespace serialization
	{
		/*template<typename T>
		constexpr bool is_specialised_impl(long long) {
			return false;
		}

		template<typename T>
		constexpr decltype(std::declval<T>().Serialize(std::declval<::Bolts::Blob>()), bool()) is_specialised_impl(int) {
			return true;
		}

		template<typename T>
		constexpr bool is_specialised() {
			return is_specialised_impl<T>(0);
		}*/


		/*
				template<typename T, bool>
				struct is_specialised
				{
					static const bool value = false;
				};

				template<typename T, decltype(T::Serialize)* = 0>
				struct is_specialised<T,true>
				{
					static const bool value = true;
				};*/

	};

	// Array for fixed-sized elements. Data is assumed to be placed right after container
	//NOTE: A stored vec_t<T> in a Blob maps the data to an InplaceArray<T>
	template<class T>
	struct InplaceArray :nonCopyable_t
	{
		typedef efficient_return<T> get_t;
		typedef efficient_return<const T> get_const_t;

		// iterator interface
		class it
		{
			friend struct InplaceArray;
			BOLTS_INLINE it(InplaceArray* owner, uint32_t start) :container(owner), index(start) {}

			InplaceArray* container;
			uint32_t index = 0;
		public:
			BOLTS_INLINE it(const it& other) = default;
			BOLTS_INLINE it& operator=(const it& other) = default;

			BOLTS_INLINE it & operator++() { index++; return *this; }
			BOLTS_INLINE get_t operator*() { return container->operator[](index); }
			BOLTS_INLINE bool operator==(it other) const { return other.container == container && other.index == index; }
			BOLTS_INLINE bool operator!=(it other) const { return other.container != container || other.index != index; }
			BOLTS_INLINE operator bool() const { return index >= 0 && container->count > index; }
		};
		BOLTS_INLINE it begin() { return it(this, 0); }
		BOLTS_INLINE it end() { return it(this, count); }
		// api
		BOLTS_INLINE get_t			operator[](std::ptrdiff_t index) { return Get(index); }
		BOLTS_INLINE get_const_t	operator[](std::ptrdiff_t index) const { return Get(index); }
		BOLTS_INLINE T*				First() { return reinterpret_cast<T*>(&count + 1); }
		BOLTS_INLINE const T*		First() const { return reinterpret_cast<const T*>(&count + 1); }
		BOLTS_INLINE uint32_t		DataSize() const { return (count * sizeof(T)); }
		BOLTS_INLINE uint32_t		SizeInBytes() const { return sizeof(*this) + (count * sizeof(T)); }
		BOLTS_INLINE get_t			Get(std::ptrdiff_t index) { return *(First() + index); }
		BOLTS_INLINE get_const_t	Get(std::ptrdiff_t index) const { return *(First() + index); }
		BOLTS_INLINE T&				Set(std::ptrdiff_t index) { return *(First() + index); }
		//T*				Reserve(Blob& owner, uint32_t cnt )
		//{
		//	BASSERT(owner.IsOwnerOf(this));
		//	BASSERT(count == 0);
		//	// Check that this array is the last placed thing in the blob, since it assumes its data comes after itself
		//	BASSERT((void*)(owner.DataEnd() - sizeof(*this)) == (void*)this);
		//	count = cnt;
		//	return static_cast<T*> (owner.WillStore(sizeof(T) * count));
		//}
		// members
		uint32_t count;
	};

	//Simple class to hold unstructured binary data to be serialised to disk or sent over the wire
	//TODO HIGH: Rethink the Peeker/Reader API. It's a little stupid. The Peeker::Read(void*&,size) API is unexpected
	//TODO High: Test this
	//TODO High: Test the helpers as well
	class Blob
	{
	public:
		Blob();
		Blob(unsigned capacity);
		Blob(Blob&& other);//> NOTE: other is no longer usable after being moved
		~Blob();
		Blob& operator=(Blob&& other);
		//
		void MoveInto(Blob& other);
		void SetCapacity(unsigned);
		BOLTS_INLINE	void					Clear() { foundation::array::clear(m_storage); }
		BOLTS_INLINE	unsigned				Size() const { return m_storage._size; }//> Returns the total size of the currently stored data
		BOLTS_INLINE	unsigned char*			Data() { return reinterpret_cast<unsigned char*>(m_storage._data); }//> Returns the pointer to the internal buffer holding the serialised data
		BOLTS_INLINE	const unsigned char*	Data() const { return reinterpret_cast<unsigned char*>(m_storage._data); }
		BOLTS_INLINE	const unsigned char*	DataEnd() const { return reinterpret_cast<unsigned char*>(m_storage._data + m_storage._size); }
		BOLTS_INLINE	bool					IsEmpty() const { return Size() == 0; }
		BOLTS_INLINE	bool					IsOwnerOf(void* ptr) const {
			return ((void*)ptr >= m_storage._data) && ((char*)ptr < m_storage._data + m_storage._size);
		}

		class Reader //> Tiny helper class that provides read-only access to the data a Blob
		{
		public:
			Reader(const Blob& blob) :m_blob(blob), m_readOffset(0) {}

			BOLTS_INLINE void* Head() //> Pointer to the next element to be Read
			{
				return (char*)m_blob.Data() + m_readOffset;
			}

			BOLTS_INLINE void Reset() //> Returns the read index to the beginning of the buffer
			{
				m_readOffset = 0;
			}

			BOLTS_INLINE bool IsEmpty() const { return m_blob.Size() == 0; }

			BOLTS_INLINE unsigned Remaining() const {
				return (unsigned)m_blob.Size() - m_readOffset;
			}

			template< typename T, typename std::enable_if_t< Bolts::is_pod<T>::value >* = nullptr>
			BOLTS_INLINE bool Peek(T& val) //> Reads without advancing the read index
			{
				return CopyInternal(&val, sizeof(T));
			}

			BOLTS_INLINE bool Peek(void*& outData, unsigned size)
			{
				if (m_blob.Size() < (m_readOffset + size))
					return false;
				outData = Head();
				return true;
			}

			template< typename T, typename std::enable_if_t< Bolts::is_pod<T>::value >* = nullptr>
			BOLTS_INLINE bool Read(T& val)
			{
				if (Peek(val)) {
					m_readOffset += sizeof(T);
					return true;
				}
				return false;
			}

			inline bool Read(str_t& val)
			{
				char* head = (char*)Head();
				unsigned len = 0;
				unsigned end = Remaining();
				while ( len < end && 
						head[len] != '\0')
					len++;
				BASSERT(len < end);
				if (len >= end)
					return false;

				val.assign(head, len);
				m_readOffset += len + 1;
				return true;
			}

			inline bool Read(const char*& val) //>NOTE: Doesn't copy string data. Just points to stored null-terminated string
			{
				char* head = (char*)Head();
				unsigned len = 0;
				unsigned end = Remaining();
				while (len < end &&
					head[len] != '\0')
					len++;
				BASSERT(len < end);
				if (len >= end)
					return false;

				val = head;
				m_readOffset += len + 1;
				return true;
			}

			BOLTS_INLINE bool Read(void*& outData, unsigned size)
			{
				if (Peek(outData, size)) {
					m_readOffset += size;
					return true;
				}
				return false;
			}
		private:

			BOLTS_INLINE bool CopyInternal(void* dst, unsigned size)
			{
				if (m_blob.Size() < (m_readOffset + size))
					return false;
				//NOTE: don't assign via reinterpret_cast, use memcopy so compiler knows it's unaligned
				memcpy(dst, (char*)m_blob.Data() + m_readOffset, size);
				return true;
			}

			const Blob&	m_blob;
			unsigned	m_readOffset; //> Current offset that we're reading from
		};

		Reader	MakeReader() const { return Reader(*this); }

		// Single Element Specialisation
		template<typename T, std::enable_if_t< ::Bolts::is_pod<T>::value, void*> = 0>
		void* Store(T val) { return StoreInternal(val); }
		template<typename T>
		void* Store(T* val) { static_assert(false, "Don't store ptrs directly"); return nullptr; }

		/*template<typename T, std::enable_if_t< ::Bolts::serialization::is_specialised<T>(), void*> = 0>
		void* Store(const T& elem) {
			const uint32_t startOffset = Size();
			elem.Serialize(*this);
			return &m_storage[startOffset];
		}*/

		// String Specialisation
		void* Store(const str_t& val) { return StoreInternal(val.c_str(), (unsigned)val.length() + 1); }
		void* Store(const char* cstr) { return  StoreInternal(cstr, (unsigned)strlen(cstr) + 1); }
		void* Store(const char* cstr, uint32_t length) { return StoreInternal(cstr, length + 1); }
		// Vector Specialisation
		template< typename T, std::enable_if_t< ::Bolts::is_pod<T>::value, void*> = 0>
		void Store(const vec_t<T>& valVec)
		{
			void* data = WillStoreArray(valVec.size(), sizeof(T) * valVec.size());
			memcpy(data, valVec.data(), sizeof(T) * valVec.size());
			//return data;
		}
		template< typename T, std::enable_if_t< !::Bolts::is_pod<T>::value, void*> = 0>
		void Store(const vec_t<T>& elemVec)
		{
			// For non-POD type we assume they have varying size, so store in an InplaceOffsetArray
			auto offsetVec = StoreNewArray<uint32_t>(elemVec.size() + 1);
			int idx = 0;
			for (const auto& elem : elemVec) {
				char* valPtr = (char*)Store(elem);
				offsetVec->Set(idx) = valPtr - (char*)offsetVec.get(); // set offsets for individual elements
				idx++;
			}
			// set end offset
			offsetVec->Set(elemVec.size()) = DataEnd() - (const unsigned char*)offsetVec.get();
		}
		// Generic storage - Pointers returned are only valid until next Store operation
		void* Store(const void* data, unsigned size)
		{
			return StoreInternal(data, size);
		}
		void*  WillStore(unsigned extraDataSize);//>Makes room for extraDataSize bytes and returns pointer to buffer
		void*  WillStoreArray(uint32_t elementCount, unsigned dataSizeInBytes) //>Stores a uint32 "count", followed by a "dataSizeInBytes" sized buffer. Returns ptr to buffer
		{
			StoreInternal<uint32_t>(elementCount);
			return WillStore(dataSizeInBytes);
		}

		// Serialized size estimates
		// Single objects
		template< typename T, std::enable_if_t< ::Bolts::is_pod<T>::value, void*> = 0>
		uint32_t SizeOf(T /*val*/) { return sizeof(T); }
		// Vectors
		template< typename T, std::enable_if_t< ::Bolts::is_pod<T>::value, void*> = 0>
		uint32_t SizeOf(const vec_t<T>& valArray) {
			return sizeof(uint32_t) + (sizeof(T)*valArray.size());
		}
		template< typename T, std::enable_if_t< !::Bolts::is_pod<T>::value, void*> = 0>
		uint32_t SizeOf(const vec_t<T>& valArray) {
			uint32_t size = sizeof(uint32_t);
			size += (valArray.size() + 1) * sizeof(uint32_t);
			for (auto const& elem : valArray)
				size += SizeOf(elem);
			return size;
		}
		// Strings
		uint32_t SizeOf(const char* str) { return (unsigned)strlen(str) + 1; }
		uint32_t SizeOf(const char* /*str*/, uint32_t length) { return length + 1; }
		uint32_t SizeOf(const str_t& str) { return (unsigned)str.length() + 1; }
		//
		template<typename T>
		struct StorePtr
		{
			T* operator->() {
				return reinterpret_cast<T*>(&blob.m_storage[offset]);
			}

			T* get() { return operator->(); }

			Blob& blob;
			uint32_t offset;
		};

		template<typename T>
		StorePtr<T> StoreNew() {
			void* newPtr = WillStore(sizeof(T));
			new (newPtr) T(); // Call ctor
			return { *this, (uint32_t) ((char*)newPtr - m_storage._data) };
		}

		template<typename T>
		StorePtr<InplaceArray<T>> StoreNewArray(uint32_t count) {
			auto arrayPtr = StoreNew<InplaceArray<T>>();
			arrayPtr->count = count;
			WillStore(sizeof(T) * count);
			return arrayPtr;
		}

		template<typename T>
		StorePtr<T> ToRelative(void* ptr) {
			return { *this, (char*)ptr - m_storage._data };
		}
	private:
		Blob(const Blob& blob) = delete;
		Blob& operator=(const Blob& other) = delete;

		void* StoreInternal(const void* buffer, unsigned dataSize);

		template< typename T>
		void* StoreInternal(T val)
		{
			return StoreInternal(&val, sizeof(T));
		}

		void	Fit(unsigned numAdditionalBytes);//> Increase storage space with given amount of additional bytes

		foundation::Array<char>		m_storage;
	};

	// Inplace Array for variable-sized elements.
	//NOTE: A stored vec_t<str_t> in a Blob maps the data to an InplaceOffsetArray<char>
	template<class T>
	struct InplaceOffsetArray :nonCopyable_t
	{
		typedef T*			get_t;
		typedef const T*	get_const_t;

		// iterator interface
		class it
		{
			friend struct InplaceOffsetArray;
			BOLTS_INLINE it(InplaceOffsetArray* owner, uint32_t start) :container(owner), index(start) {}

			InplaceOffsetArray* container;
			uint32_t index = 0;
		public:
			BOLTS_INLINE it(const it& other) = default;
			BOLTS_INLINE it& operator=(const it& other) = default;

			BOLTS_INLINE operator bool() const { return index >= 0 && container->Count() > index; }

			BOLTS_INLINE it& operator++() { index++; return *this; }
			BOLTS_INLINE get_const_t operator*() const { return container->Get(index); }
			BOLTS_INLINE bool operator==(it other) const { return other.container == container && other.index == index; }
			BOLTS_INLINE bool operator!=(it other) const { return other.container != container || other.index != index; }

			BOLTS_INLINE uint32_t Index() const { return index; }
		};
		BOLTS_INLINE it begin() { return it(this,0); }
		BOLTS_INLINE it end() { return it(this, Count()); }
		// api
		BOLTS_INLINE get_t			operator[](std::ptrdiff_t index) { return Get(index); }
		BOLTS_INLINE get_const_t	operator[](std::ptrdiff_t index) const { return Get(index); }
		BOLTS_INLINE uint32_t		Count() const { return offsets.count - 1; } // Return the number of elements
		BOLTS_INLINE const void*	DataEnd() const { return Get(Count()); }
		BOLTS_INLINE uint32_t		SizeOf(unsigned index) const { return offsets[index + 1] - offsets[index]; } // Returns the size in bytes of given element
		BOLTS_INLINE uint32_t		SizeInBytes() const { return offsets[Count()]; } // Size in bytes of this vector and its contents
		BOLTS_INLINE get_t			Get(std::ptrdiff_t index) { return reinterpret_cast<T*>(reinterpret_cast<char*>(this) + offsets[index]); }
		BOLTS_INLINE get_const_t	Get(std::ptrdiff_t index) const { return reinterpret_cast<const T*>(reinterpret_cast<const char*>(this) + offsets[index]); }
		// storage API
		void Reserve(Blob& storage, uint32_t count) {
			BASSERT(offsets.count == 0);
			BASSERT(storage.IsOwnerOf(this));
			// Check that this array is the last placed thing in the blob, since it assumes its data comes after itself
			BASSERT((void*)(storage.DataEnd() - sizeof(*this)) == (void*)this);
			offsets.count = count + 1;
			void* data = storage.WillStore(sizeof(uint32_t) * offsets.count);
			memset(data, 0, sizeof(uint32_t) * (count+1));
		}
		void PushBack(const void* data) {
			// find free slot
			uint32_t freeIdx = 0;
			for(;freeIdx < Count(); freeIdx++)
				if (offsets[freeIdx] == 0)
					break;
			BASSERT(freeIdx != Count());
			if (freeIdx == Count())
				return;

			offsets.Set(freeIdx) = uint32_t((char*)data - (char*)this);
		}
		void SetEnd(const void* end) {
			BASSERT(end > Get(Count() - 1));
			offsets.Set(Count()) = uint32_t((char*)end - (char*)this);
		}

		// members
		InplaceArray<uint32_t> offsets; // Byte offsets from this to data element
	};

	template<typename T, typename std::enable_if_t< std::is_pod<T>::value && (sizeof(T) <= 16) >* = nullptr>
	Blob& operator<< (Blob& blob, T value)
	{
		blob.Store(value);
		return blob;
	}

	template<typename T, typename std::enable_if_t< (sizeof(T) > 16) >* = nullptr>
	Blob& operator<< (Blob& blob, T&& value)
	{
		blob.Store(FWD(value));
		return blob;
	}
}