#pragma once
#include <bolts_types.h>
#include <string>

namespace Bolts
{
	namespace detail 
	{
		struct StringWrapper
		{
			StringWrapper(const char* str) : s(str),len(strlen(str)) {};
			StringWrapper(const Bolts::str_t& str) : s(str.c_str()), len(str.size()) {};
			const char* s;
			size_t len;
		};

	#if defined(_MSC_VER)
		#define BIG_CONSTANT(x) (x)

		#define ROTL32(x,y)	_rotl(x,y)
		#define ROTL64(x,y)	_rotl64(x,y)
	#else
		#define BIG_CONSTANT(x) (x##LLU)

			BOLTS_INLINE uint32_t rotl32(uint32_t x, int8_t r)
			{
				return (x << r) | (x >> (32 - r));
			}

			BOLTS_INLINE uint64_t rotl64(uint64_t x, int8_t r)
			{
				return (x << r) | (x >> (64 - r));
			}

		#define	ROTL32(x,y)	rotl32(x,y)
		#define ROTL64(x,y)	rotl64(x,y)

	#endif


		const uint32_t c_hashSeed = 0;
		const uint32_t c_hashC1 = 0xcc9e2d51;
		const uint32_t c_hashC2 = 0x1b873593;

		BOLTS_INLINE uint32_t fmix32(uint32_t h)
		{
			h ^= h >> 16;
			h *= 0x85ebca6b;
			h ^= h >> 13;
			h *= 0xc2b2ae35;
			h ^= h >> 16;

			return h;
		}
		BOLTS_INLINE uint64_t fmix64(uint64_t k)
		{
			k ^= k >> 33;
			k *= BIG_CONSTANT(0xff51afd7ed558ccd);
			k ^= k >> 33;
			k *= BIG_CONSTANT(0xc4ceb9fe1a85ec53);
			k ^= k >> 33;

			return k;
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////

		template <unsigned int N, unsigned int I>
		struct MurmurHash32
		{
			BOLTS_INLINE static hash32_t HashIt(const char(&str)[N], uint32_t h)
			{
				const int nblocks = (N - 1) / 4;
				const uint32_t *blocks = (const uint32_t *)(str + nblocks * 4);
				uint32_t k1 = blocks[-int(I / 4)];
				k1 *= c_hashC1;
				k1 = ROTL32(k1, 15);
				k1 *= c_hashC2;

				uint32_t h1 = h;
				h1 ^= k1;
				h1 = ROTL32(h1, 13);
				h1 = h1 * 5 + 0xe6546b64;

				return MurmurHash32<N, I - 4>::HashIt(str, h1);
			}

		};

		template <unsigned int N>
		struct MurmurHash32 < N, 3 >
		{
			BOLTS_INLINE static hash32_t HashIt(const char(&str)[N], uint32_t h)
			{
				const int nblocks = (N - 1) / 4;
				const uint8_t *tail = (const uint8_t *)(str + nblocks * 4);
				uint32_t k1 = 0;
				k1 ^= tail[2] << 16;
				k1 ^= tail[1] << 8;
				k1 ^= tail[0];
				k1 *= c_hashC1;
				k1 = ROTL32(k1, 15);
				k1 *= c_hashC2;

				return h ^ k1;
			}
		};

		template <unsigned int N>
		struct MurmurHash32 < N, 2 >
		{
			BOLTS_INLINE static hash32_t HashIt(const char(&str)[N], uint32_t h)
			{
				const int nblocks = (N - 1) / 4;
				const uint8_t *tail = (const uint8_t *)(str + nblocks * 4);
				uint32_t k1 = 0;
				k1 ^= tail[1] << 8;
				k1 ^= tail[0];
				k1 *= c_hashC1;
				k1 = ROTL32(k1, 15);
				k1 *= c_hashC2;

				return h ^ k1;
			}
		};

		template <unsigned int N>
		struct MurmurHash32 < N, 1 >
		{
			BOLTS_INLINE static hash32_t HashIt(const char(&str)[N], uint32_t h)
			{
				const int nblocks = (N - 1) / 4;
				const uint8_t *tail = (const uint8_t *)(str + nblocks * 4);
				uint32_t k1 = 0;
				k1 ^= tail[0];
				k1 *= c_hashC1;
				k1 = ROTL32(k1, 15);
				k1 *= c_hashC2;

				return h ^ k1;
			}
		};

		template <unsigned int N>
		struct MurmurHash32 < N, 0 >
		{
			BOLTS_INLINE static hash32_t HashIt(const char(&)[N], uint32_t h)
			{
				return h;
			}
		};

	};
	
	// This version hashes string literals at compile-time ( NOTE: But it doesn't generate compile-time constants :( )
	template<unsigned int N>
	BOLTS_INLINE hash32_t HashMM(const char(&str)[N]) {
		return detail::fmix32(detail::MurmurHash32<N, N - 1>::HashIt(str, detail::c_hashSeed) ^ (N - 1));
	}

	hash32_t	HashString(const std::string& targetStr);
	hash32_t	HashString(uint32_t seed, const std::string& targetStr);
	hash32_t	HashString(const char* targetCStr);
	hash32_t	HashString(uint32_t seed, const char* targetCStr);
	hash32_t	HashString(const char* targetCStr, size_t targetLen);

	//> Helper for storing const strings along with their hashes
	struct hashCstr_t
	{
		template<unsigned int N>
		BOLTS_INLINE hashCstr_t(const char(&str)[N])
		{
			h = HashMM(str);
			s = str;
		}

		BOLTS_INLINE operator hash32_t() const { return h; }

		hash32_t		h = 0;
		const char*		s = nullptr;
	};

	//> Helper for auto-hashing strings for hash parameters.
	struct stringHash_t
	{
	public:
		stringHash_t() : m_hash(0) {};

		template <unsigned int N>
		stringHash_t(const char(&str)[N])
			: m_hash(HashMM(str))
		{
		}

		stringHash_t(hash32_t val) : m_hash(val) {}
		stringHash_t(detail::StringWrapper str) {
			m_hash = HashString(str.s, str.len);
		}

		stringHash_t& operator= (hash32_t val){
			m_hash = val;
			return *this;
		}

		operator hash32_t() const { return m_hash; }

	//private: //NOTE: Making this public turns stringHash_t into a POD
		hash32_t m_hash;
	};
	
	const char *	GetHashString(hash32_t hash); //> Retrieve stored string for given hash
	const char *	RegisterStringHash(hash32_t hash, const char* cstr, size_t strlen = 0); //> NOTE: Copies cstr. Returns ptr to copy
	void			ClearStrings(); //> Clears all registered hash strings. Only used for testing

	//> Helper for auto-registering strings and storing them, along with their hashes
	struct regHash_t
	{
		regHash_t() = default;
		regHash_t(const regHash_t&) = default;
		regHash_t& operator=(const regHash_t&) = default;
		
		template<unsigned int N>
		BOLTS_INLINE regHash_t(const char(&str)[N])
		{
			h = HashMM(str);
			s = RegisterStringHash(h, str, N);
		}

		BOLTS_INLINE regHash_t(detail::StringWrapper str) 
		{
			h = HashString(str.s, str.len);
			s = RegisterStringHash(h, str.s, str.len);
		}

		BOLTS_INLINE regHash_t(hash32_t hs)
		{
			h = hs;
			s = GetHashString(h);
		}

		BOLTS_INLINE regHash_t(stringHash_t sh)
		{
			h = sh;
			s = GetHashString(h);
		}

		BOLTS_INLINE operator hash32_t() const { return h; }

		const char*		s = nullptr;
		hash32_t		h = 0;
	};
};

//> Helper debug vis function. For quickly casting hashes, since we can't natvis typedefs
const char* HS(Bolts::hash32_t h);