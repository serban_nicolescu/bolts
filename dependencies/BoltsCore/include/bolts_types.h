#pragma once
#include <cstdint>
#include <vector>
#include <array>
#include <unordered_map>
#include <string>

// On MSVC, disable "warning C4201: nonstandard extension used : nameless struct/union" warning (level 4).
#ifdef _MSC_VER
	#pragma warning(push)
	#pragma warning(disable:4201)
#endif
	#include <glm/glm.hpp>
	#include <glm/gtc/quaternion.hpp>
	#include <glm/gtx/norm.hpp>
#ifdef _MSC_VER
	#pragma warning(pop)
#endif


/*
#ifdef _MSC_VER
#pragma warning( push, 3)
#endif

//#include <boost/intrusive_ptr.hpp>

#ifdef _MSC_VER
#pragma warning( pop)
#endif
*/

#if defined(_MSC_VER)
	#define BOLTS_INLINE __forceinline
#else
	// Tested on GCC ARM 4.8.6 and Clang x86-64 3.9.0 on https://gcc.godbolt.org/# with -02
		// GCC -Og also collapses the hash to a single value at compile-time
	#define BOLTS_INLINE __attribute__((always_inline))
#endif

template <typename T, std::size_t N>
constexpr std::size_t countof(T const (&)[N]) noexcept
{
	return N;
}

// static_cast to rvalue reference
#define MOV(...) static_cast<std::remove_reference_t<decltype(__VA_ARGS__)>&&>(__VA_ARGS__)
// static_cast to identity
// The extra && aren't necessary as discussed above, but make it more robust in case it's used with a non-reference.
#define FWD(...) static_cast<decltype(__VA_ARGS__)&&>(__VA_ARGS__)

namespace Bolts {

	typedef uint32_t hash32_t;

	BOLTS_INLINE hash32_t hashCombine(hash32_t h1, hash32_t h2) {
		return h1 ^ (h2 + 0x9e3779b9 + (h1 << 6) + (h1 >> 2));
	}

	static constexpr uint32_t badHandle = uint32_t(-1);

	// containers

	template<typename T>
	using vec_t = std::vector<T>;

	using blob_t = vec_t<uint8_t>; //> Binary Blob buffer


	template<typename T, int S>
	using array_t = std::array<T,S>;

	template<typename K, typename V>
	using hash_map_t = std::unordered_map<K, V>;

	using str_t = std::string;
	
	template <uint32_t a, uint32_t b, uint32_t c, uint32_t d>
	struct FourCC
	{
		static const uint32_t v = (((((d << 8) | c) << 8) | b) << 8) | a;
	};

	struct nonCopyable_t
	{
		nonCopyable_t() {}
	private:
		nonCopyable_t(const nonCopyable_t&) = delete;
		nonCopyable_t(nonCopyable_t&&) = delete;
		nonCopyable_t& operator=(const nonCopyable_t&) = delete;
		nonCopyable_t& operator=(nonCopyable_t&&) = delete;
	};

	template<class T>
	struct OffsetPtr : nonCopyable_t
	{
		uint32_t offset = 0;

		BOLTS_INLINE void operator=(const T* pointee) { offset = (unsigned)(reinterpret_cast<const char*>(pointee) - (const char*)this); }
		BOLTS_INLINE operator bool() const { return offset >= 4; }

		BOLTS_INLINE T* operator->() { return get(); }
		BOLTS_INLINE const T* operator->() const { return get(); }
		BOLTS_INLINE T& operator*() { return *get(); }
		BOLTS_INLINE const T& operator*() const { return *get(); }

		BOLTS_INLINE const T* get() const { return reinterpret_cast<const T*>(reinterpret_cast<const char*>(this) + offset); }
		BOLTS_INLINE T* get() { return reinterpret_cast<T*>(reinterpret_cast<char*>(this) + offset); }
		BOLTS_INLINE const T& operator[](std::ptrdiff_t index) const { return *(get() + index); }
		BOLTS_INLINE T& operator[](std::ptrdiff_t index) { return *(get() + index); }
	};

	//

	// Returns positive adjustment required to align val to a multiple of alignment
	BOLTS_INLINE unsigned alignAdjust(uintptr_t val, unsigned alignment) {
		return unsigned(((val + (alignment - 1)) & (0u-alignment)) - val);
	}

	// Returns first suitable location for given type, starting from given ptr
	template<typename T>
	T* alignPtr(void* data) {
		return reinterpret_cast<T*>(static_cast<char*>(data) + alignAdjust(uintptr_t(data), alignof(T)));
	}

	inline int countSetBits(uint32_t v) {
		//TODO: popcnt ?
		int count = 0;
		for (; v != 0; v >>= 1) {
			count += (v & 1);
		}
		return count;
	}

	// Helper for determining the base type of a class member
	template<class T, class U>
	T* GetMemberType(T U::* /*memPtr*/) { return nullptr; }

	// POD trait
	// Bolts::is_pod allows overriding default behavior ( generally used for simple POD-likes with default-ctors )
	template<class T>
	struct is_pod : public std::is_pod < T > {};
	#define BOLTS_TYPE_IS_POD(T) typename std::enable_if_t< Bolts::is_pod<T>::value >* = nullptr

	// Math Stuff

	const float c_PI = 3.14159265358979323846264338327950288f;
	const float c_Sqrt2 = 1.41421356237309504880168872420969808f;

	typedef glm::vec2 vec2;
	typedef glm::vec3 vec3;
	typedef glm::vec4 vec4;
	typedef glm::ivec2 ivec2;
	typedef glm::ivec3 ivec3;
	typedef glm::ivec4 ivec4;
	typedef glm::uvec2 uvec2;
	typedef glm::uvec3 uvec3;
	typedef glm::uvec4 uvec4;
	typedef glm::tvec3<unsigned char> byte3;
	typedef glm::tvec4<unsigned char> byte4;	
	typedef glm::quat quat;
	typedef glm::mat4 mat4;
	typedef glm::mat3x4 mat34; //TODO: Is it 3x4 or 4x3 ?
	typedef glm::mat3 mat3;

	const vec2 vec2_zero{ 0.f, 0.f};
	const vec3 vec3_zero{ 0.f, 0.f, 0.f };
	const vec4 vec4_zero{ 0.f, 0.f, 0.f, 0.f };
	const vec2 vec2_one{ 1.f, 1.f };
	const vec3 vec3_one{ 1.f, 1.f, 1.f};
	const vec4 vec4_one{ 1.f, 1.f, 1.f, 1.f };

	template<>
	struct is_pod< Bolts::vec2> : public std::true_type {};
	template<>
	struct is_pod< Bolts::vec3> : public std::true_type {};
	template<>
	struct is_pod< Bolts::vec4> : public std::true_type {};
	template<>
	struct is_pod< Bolts::ivec2> : public std::true_type {};
	template<>
	struct is_pod< Bolts::ivec3> : public std::true_type {};
	template<>
	struct is_pod< Bolts::ivec4> : public std::true_type {};
	template<>
	struct is_pod< Bolts::uvec2> : public std::true_type {};
	template<>
	struct is_pod< Bolts::uvec3> : public std::true_type {};
	template<>
	struct is_pod< Bolts::uvec4> : public std::true_type {};
	template<>
	struct is_pod< Bolts::byte3> : public std::true_type {};
	template<>
	struct is_pod< Bolts::byte4> : public std::true_type {};
	template<>
	struct is_pod< Bolts::quat> : public std::true_type {};
	template<>
	struct is_pod< Bolts::mat3> : public std::true_type {};
	template<>
	struct is_pod< Bolts::mat4> : public std::true_type {};
	template<>
	struct is_pod< Bolts::mat34> : public std::true_type {};

	
	// Noise-based rand

	//NOTE: Source: GDC 2017: Noise-Based RNG
	// could also use std::hash<uint32_t>, but lose the seed
	BOLTS_INLINE uint32_t RandomSquirrel3(uint32_t position, uint32_t seed)
	{
		// these need to be large primes, with many random bits set
		const unsigned BIT_NOISE1 = 0xB5297A4D;
		const unsigned BIT_NOISE2 = 0x68E31DA4;
		const unsigned BIT_NOISE3 = 0x1B56c4e9;

		uint32_t mangled = position;
		mangled *= BIT_NOISE1;
		mangled += seed;
		mangled ^= (mangled >> 8);
		mangled += BIT_NOISE2;
		mangled ^= (mangled << 8);
		mangled *= BIT_NOISE3;
		mangled ^= (mangled >> 8);
		return mangled;
	}

	// Returns 32 random bits based on given seed & position
	BOLTS_INLINE uint32_t RandU32_S(uint32_t position, uint32_t seed) { return RandomSquirrel3(position, seed); }
	// Returns a random float in [0..1] based on given seed & position
	BOLTS_INLINE float RandUnitFloat_S(uint32_t position, uint32_t seed) { return float(RandomSquirrel3(position, seed) & 0x00FFFFFF) / float(0x00FFFFFF); }
	// Returns a random float in [-1..1] based on given seed & position
	BOLTS_INLINE float RandUnitNegFloat_S(uint32_t position, uint32_t seed) { return RandUnitFloat_S(position, seed) * 2.f - 1.f; }

	extern uint32_t g_randomSeed;
	extern uint32_t g_randomGlobalPos;
	// global variants, not thread-safe
	BOLTS_INLINE uint32_t RandU32_G() { return RandomSquirrel3(g_randomGlobalPos++, g_randomSeed); }
	BOLTS_INLINE float RandUnitFloat_G() { return RandUnitFloat_S(g_randomGlobalPos++, g_randomSeed); }
	BOLTS_INLINE float RandUnitNegFloat_G() { return RandUnitNegFloat_S(g_randomGlobalPos++, g_randomSeed); }

	BOLTS_INLINE vec2 RandUnitVec2_G() {
		vec2 r{ RandUnitNegFloat_G(), RandUnitNegFloat_G()};
		if (r == vec2_zero)
			return vec2(1.f, 0.f);
		return glm::normalize(r);
	}
	//

	struct Segment {
		vec3 p1;
		vec3 p2;
	};

	struct Ray {
		vec3 o;
		vec3 invN;
	};

	struct AABB {
		vec3 min;
		vec3 max;

		BOLTS_INLINE vec3 center() const { return (max + min) * 0.5f; }
		BOLTS_INLINE vec3 ext() const { return (max - min) * 0.5f; }
		BOLTS_INLINE void include(const vec3& p) {
			min = glm::min(p, min);
			max = glm::max(p, max);
		}
	};

	BOLTS_INLINE Ray MakeRay(Segment s) {
		vec3 invN = 1.f / glm::normalize(s.p2 - s.p1);
		return { s.p1, invN };
	}

	template<typename N>
	BOLTS_INLINE N min(N a, N b) { return a < b ? a : b; };

	template<typename N>
	BOLTS_INLINE N max(N a, N b) { return a > b ? a : b; };

	//https://tavianator.com/fast-branchless-raybounding-box-intersections/
	//https://tavianator.com/fast-branchless-raybounding-box-intersections-part-2-nans/
	inline bool RayAABB(Ray ray, AABB box) {
		double t1 = (box.min[0] - ray.o[0])*ray.invN[0];
		double t2 = (box.max[0] - ray.o[0])*ray.invN[0];

		double tmin = min(t1, t2);
		double tmax = max(t1, t2);

		for (int i = 1; i < 3; ++i) {
			t1 = (box.min[i] - ray.o[i])*ray.invN[i];
			t2 = (box.max[i] - ray.o[i])*ray.invN[i];

			tmin = max(tmin, min(min(t1, t2), tmax));
			tmax = min(tmax, max(max(t1, t2), tmin));
		}

		return tmax > max(tmin, 0.0);
	}

	inline void GetAABBVerts(vec3 out[8], const AABB& aabb) {
		out[0] = vec3{ aabb.max.x, aabb.max.y, aabb.max.z };
		out[1] = vec3{ aabb.min.x, aabb.max.y, aabb.max.z };
		out[2] = vec3{ aabb.max.x, aabb.min.y, aabb.max.z };
		out[3] = vec3{ aabb.min.x, aabb.min.y, aabb.max.z };
		out[4] = vec3{ aabb.max.x, aabb.max.y, aabb.min.z };
		out[5] = vec3{ aabb.min.x, aabb.max.y, aabb.min.z };
		out[6] = vec3{ aabb.max.x, aabb.min.y, aabb.min.z };
		out[7] = vec3{ aabb.min.x, aabb.min.y, aabb.min.z };
	}

	inline void GetOBBVerts(vec3 out[8], const mat4& tr, const AABB& aabb) {
		vec3 center = vec3(tr[3]);
		mat3 t = mat3(tr);
		out[0] = center + t * vec3{ aabb.max.x, aabb.max.y, aabb.max.z };
		out[1] = center + t * vec3{ aabb.min.x, aabb.max.y, aabb.max.z };
		out[2] = center + t * vec3{ aabb.max.x, aabb.min.y, aabb.max.z };
		out[3] = center + t * vec3{ aabb.min.x, aabb.min.y, aabb.max.z };
		out[4] = center + t * vec3{ aabb.max.x, aabb.max.y, aabb.min.z };
		out[5] = center + t * vec3{ aabb.min.x, aabb.max.y, aabb.min.z };
		out[6] = center + t * vec3{ aabb.max.x, aabb.min.y, aabb.min.z };
		out[7] = center + t * vec3{ aabb.min.x, aabb.min.y, aabb.min.z };
	}

	inline void GetWorldSpaceAABB(AABB& out, const vec3& obbExt, const mat4& mat)
	{
		vec3 extMax = {
			glm::dot(abs(vec3(mat[0])), obbExt),
			glm::dot(abs(vec3(mat[1])), obbExt),
			glm::dot(abs(vec3(mat[2])), obbExt),
		};
		vec3 pos{ mat[3] };
		out.min = pos - vec3(extMax);
		out.max = pos + vec3(extMax);
	};

	inline void GetPointsAABB(AABB& out, const vec3* points, int numPoints)
	{
		if (numPoints > 0) {
			out.min = out.max = points[0];
			for (int i = 1; i < numPoints; i++)
				out.include(points[i]);
		}
	}

	// Thanks to Paul Bourke for line-line test code. http://paulbourke.net/geometry/pointlineplane/
	/*
	Calculate the line segment PaPb that is the shortest route between
	two lines P1P2 and P3P4. Calculate also the values of mua and mub where
	Pa = P1 + mua (P2 - P1)
	Pb = P3 + mub (P4 - P3)
	Return FALSE if no solution exists.
	*/
	inline bool LineLineIntersect(Segment s1, Segment s2, Segment *so, double *mua, double *mub)
	{
		const float k_eps = 0.001f;
		vec3 p13, p43, p21;
		double d1343, d4321, d1321, d4343, d2121;
		double numer, denom;

		p13.x = s1.p1.x - s2.p1.x;
		p13.y = s1.p1.y - s2.p1.y;
		p13.z = s1.p1.z - s2.p1.z;
		p43.x = s2.p2.x - s2.p1.x;
		p43.y = s2.p2.y - s2.p1.y;
		p43.z = s2.p2.z - s2.p1.z;
		if (abs(p43.x) < k_eps && abs(p43.y) < k_eps && abs(p43.z) < k_eps)
			return false;
		p21.x = s1.p2.x - s1.p1.x;
		p21.y = s1.p2.y - s1.p1.y;
		p21.z = s1.p2.z - s1.p1.z;
		if (abs(p21.x) < k_eps && abs(p21.y) < k_eps && abs(p21.z) < k_eps)
			return false;

		d1343 = p13.x * p43.x + p13.y * p43.y + p13.z * p43.z;
		d4321 = p43.x * p21.x + p43.y * p21.y + p43.z * p21.z;
		d1321 = p13.x * p21.x + p13.y * p21.y + p13.z * p21.z;
		d4343 = p43.x * p43.x + p43.y * p43.y + p43.z * p43.z;
		d2121 = p21.x * p21.x + p21.y * p21.y + p21.z * p21.z;

		denom = d2121 * d4343 - d4321 * d4321;
		if (abs(denom) < k_eps)
			return false;
		numer = d1343 * d4321 - d1321 * d4343;

		*mua = numer / denom;
		*mub = (d1343 + d4321 * (*mua)) / d4343;

		so->p1 = {
			s1.p1.x + *mua * p21.x,
			s1.p1.y + *mua * p21.y,
			s1.p1.z + *mua * p21.z 
		};
		so->p2 = {
			s2.p1.x + *mub * p43.x,
			s2.p1.y + *mub * p43.y,
			s2.p1.z + *mub * p43.z
		};
		return true;
	}

	BOLTS_INLINE float	degToRad(float degs) { return degs * (c_PI / 180.f); }
	BOLTS_INLINE float	radToDeg(float rads) { return rads * (180.f / c_PI); }

	// END Math
	//

	// Helper to determine how given type should be passed as a parameter
	template<typename T>
	constexpr bool pass_by_value() {
		return ::Bolts::is_pod<T>::value && (sizeof(T) <= 16);
	}

	template<class T, bool isSmallPOD>
	struct efficient_return_t			{ typedef T& type; };
	template<class T>
	struct efficient_return_t<T, true>	{ typedef T type; };
	template<class T>
	using efficient_return = typename efficient_return_t<T, pass_by_value<T>()>::type;

	template<class T, bool isSmallPOD>
	struct efficient_param_t { typedef const T& type; };
	template<class T>
	struct efficient_param_t<T, true> { typedef T type; };
	template<class T>
	using efficient_param = typename efficient_param_t<T, pass_by_value<T>()>::type;
};