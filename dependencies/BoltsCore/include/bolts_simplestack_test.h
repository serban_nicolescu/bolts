#include <Bolts/Core/SimpleStack.h>

void	DoStackTest()
{
	//BEGIN SimpleStack test
	float val1 = 0.42f;
	//uint32_t val1 = 1234;
	const float val2 = 0.6234f;
	Bolts::vec3 val3( 0.4f, 1.f, 3.f);

	Bolts::SimpleStack::index_t lastIdx, ind1;
	Bolts::SimpleStack test_stack;

	lastIdx = test_stack.Insert<float>( val1);
	BASSERT( val1 == test_stack.Get<float>( lastIdx));
	ind1 = lastIdx;
	lastIdx = test_stack.Insert<float>( val2);
	BASSERT( val2 == test_stack.Get<float>( lastIdx));

	lastIdx = test_stack.Insert<Bolts::vec3>( val3);
	BASSERT( val3 == test_stack.Get<Bolts::vec3>( lastIdx));

	BASSERT( val1 == test_stack.Get<float>( ind1));
}