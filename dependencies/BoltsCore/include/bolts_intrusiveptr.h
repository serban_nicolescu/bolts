#pragma once

#include <bolts_core.h>
#include <bolts_assert.h>

namespace Bolts {

	template< typename Base, typename I>
	class RefCountedType {
	public:
		BOLTS_INLINE RefCountedType() = default;
		BOLTS_INLINE RefCountedType(RefCountedType&& other) { m_references = 0; }
		BOLTS_INLINE RefCountedType(const RefCountedType& other) { m_references = 0; }

		BOLTS_INLINE void onReferenceAdd() const {
			++m_references;
		};

		BOLTS_INLINE void onReferenceRelease() const {
			m_references--;
			if (m_references == 0)
			{
				delete BaseCast();
			}
		}

	protected:
		BOLTS_INLINE Base* BaseCast() const { return (Base*)this; }

		mutable I m_references = 0;
	};

	template<typename T>
	using RefCountT = RefCountedType< T, uint32_t >;

	template<typename T>
	inline void intrusive_ptr_add_ref(const T *expr)
	{
		expr->onReferenceAdd();
	};

	template<typename T>
	inline void intrusive_ptr_release(const T *expr)
	{
		expr->onReferenceRelease();
	};

	//
	//  intrusivePtr_t
	//
	//  A smart pointer that uses intrusive reference counting.
	//
	//  Relies on unqualified calls to
	//  
	//      void intrusive_ptr_add_ref(T * p);
	//      void intrusive_ptr_release(T * p);
	//
	//          (p != 0)
	//
	//  The object is responsible for destroying itself.
	//

	template<class T> class intrusivePtr_t
	{
	private:

		typedef intrusivePtr_t this_type;

	public:

		typedef T element_type;

		constexpr intrusivePtr_t() noexcept : px(0)
		{
		}

		intrusivePtr_t(T * p, bool add_ref = true) : px(p)
		{
			if (px != 0 && add_ref) intrusive_ptr_add_ref(px);
		}

		template<class U>
		intrusivePtr_t(intrusivePtr_t<U> const & rhs) : px(rhs.get())
		{
			if (px != 0) intrusive_ptr_add_ref(px);
		}

		intrusivePtr_t(intrusivePtr_t const & rhs) : px(rhs.px)
		{
			if (px != 0) intrusive_ptr_add_ref(px);
		}

		~intrusivePtr_t()
		{
			if (px != 0) intrusive_ptr_release(px);
		}

		template<class U> intrusivePtr_t & operator=(intrusivePtr_t<U> const & rhs)
		{
			this_type(rhs).swap(*this);
			return *this;
		}

		// Move support
		intrusivePtr_t(intrusivePtr_t && rhs) noexcept : px(rhs.px)
		{
			rhs.px = 0;
		}

		intrusivePtr_t & operator=(intrusivePtr_t && rhs) noexcept
		{
			this_type(static_cast<intrusivePtr_t &&>(rhs)).swap(*this);
			return *this;
		}

		template<class U> friend class intrusivePtr_t;

		template<class U>
		intrusivePtr_t(intrusivePtr_t<U> && rhs) : px(rhs.px)
		{
			rhs.px = 0;
		}

		template<class U>
		intrusivePtr_t & operator=(intrusivePtr_t<U> && rhs) noexcept
		{
			this_type(static_cast<intrusivePtr_t<U> &&>(rhs)).swap(*this);
			return *this;
		}

		intrusivePtr_t & operator=(intrusivePtr_t const & rhs)
		{
			this_type(rhs).swap(*this);
			return *this;
		}

		intrusivePtr_t & operator=(T * rhs)
		{
			this_type(rhs).swap(*this);
			return *this;
		}

		void reset()
		{
			this_type().swap(*this);
		}

		void reset(T * rhs)
		{
			this_type(rhs).swap(*this);
		}

		void reset(T * rhs, bool add_ref)
		{
			this_type(rhs, add_ref).swap(*this);
		}

		BOLTS_INLINE T * get() const noexcept
		{
			return px;
		}

		T * detach() noexcept
		{
			T * ret = px;
			px = 0;
			return ret;
		}

		BOLTS_INLINE T & operator*() const noexcept
		{
			BASSERT(px != 0);
			return *px;
		}

		BOLTS_INLINE T* operator->() const
		{
			BASSERT(px != 0);
			return px;
		}

		BOLTS_INLINE explicit operator bool() const noexcept
		{
			return px != 0;
		}

		void swap(intrusivePtr_t & rhs) noexcept
		{
			T * tmp = px;
			px = rhs.px;
			rhs.px = tmp;
		}

	private:

		T * px;
	};

	template<class T, class U> inline bool operator==(intrusivePtr_t<T> const & a, intrusivePtr_t<U> const & b) noexcept
	{
		return a.get() == b.get();
	}

	template<class T, class U> inline bool operator!=(intrusivePtr_t<T> const & a, intrusivePtr_t<U> const & b) noexcept
	{
		return a.get() != b.get();
	}

	template<class T, class U> inline bool operator==(intrusivePtr_t<T> const & a, U * b) noexcept
	{
		return a.get() == b;
	}

	template<class T, class U> inline bool operator!=(intrusivePtr_t<T> const & a, U * b) noexcept
	{
		return a.get() != b;
	}

	template<class T, class U> inline bool operator==(T * a, intrusivePtr_t<U> const & b) noexcept
	{
		return a == b.get();
	}

	template<class T, class U> inline bool operator!=(T * a, intrusivePtr_t<U> const & b) noexcept
	{
		return a != b.get();
	}

	template<class T> inline bool operator==(intrusivePtr_t<T> const & p, nullptr_t) noexcept
	{
		return p.get() == 0;
	}

	template<class T> inline bool operator==(nullptr_t, intrusivePtr_t<T> const & p) noexcept
	{
		return p.get() == 0;
	}

	template<class T> inline bool operator!=(intrusivePtr_t<T> const & p, nullptr_t) noexcept
	{
		return p.get() != 0;
	}

	template<class T> inline bool operator!=(nullptr_t, intrusivePtr_t<T> const & p) noexcept
	{
		return p.get() != 0;
	}

}