-- A solution contains projects, and defines the available configurations
-- Global config
bCfg = {
	depPath = "../../",
	prjPath = _ACTION .. "/%{prj.name}",
	objPath = "obj/" .. _ACTION,
	outPath = "../bin/" .. _ACTION,
}
bCfg.libPath = bCfg.depPath .. "libs/" .. _ACTION .. "/"
bCfg.libOutPath = bCfg.depPath .. "libs/" .. _ACTION .. "/%{prj.name}/%{cfg.shortname}"
-- Utility defines
bUtils = {
	action = _ACTION or ""
}
function bUtils.GetOutputName( baseName )
	return baseName .. '_%{cfg.shortname}'
end

function bUtils.GetOutDirName( baseName )
	return baseName .. '_%{cfg.shortname}'
end

-------------------------------------------------
--
-------------------------------------------------

solution "BoltsCore"
	location ( _ACTION )
	
	configurations { "Debug", "Release" }
	
	targetdir( bCfg.libOutPath )
	objdir 	( bCfg.objPath )
	
	filter { "action:vs*" }
		system "Windows"
		platforms { "x32", "x64" }
		characterset "MBCS"
		systemversion "latest"
	filter {}

	flags { "MultiProcessorCompile", "NoMinimalRebuild" }
	
	configuration "Debug"
		defines { "DEBUG" }
		optimize "Off"
		symbols 'On'
		targetsuffix "-d"
	configuration "Release"
		defines { "NDEBUG" }
		optimize "Speed"
	configuration {}
	
	filter { "system:Windows" }
		defines { "WIN32", "_CRT_SECURE_NO_WARNINGS", "_SCL_SECURE_NO_WARNINGS", "WIN32_LEAN_AND_MEAN", "NOMINMAX" }
	filter{}
	
project "BoltsCore"
	kind "StaticLib"
	language "C++"
	
	uuid "5f4a4b6e-7904-4450-85e0-60f601f927af"
	
	files 
	{ 
		"../include/**.h", 
		"../src/**.cpp",
		"../BoltsCore.natvis",
		"../glm/util/glm.natvis"
	}
	
	includedirs
	{
		"../include/",
		bCfg.depPath .. "glm",
		bCfg.depPath .. "BitSquid/Foundation/",
	}
	
	flags { "FatalWarnings" }
	warnings "Extra"
		
	--Disable some warnings
	buildoptions 
	{ 
		"/wd\"4127\"", -- VS: Conditional expression is constant ( from logging)
		"/wd\"4512\"", -- VS: Assignment operator could not be generated
	}

project "CoreTests"
	location ( _ACTION )
	kind "ConsoleApp"
	language "C++"
	
	files 
	{ 
		"../tests/**.h", 
		"../tests/**.cpp",
	}
	includedirs 
	{
		"../src",
		"../include",
		"../tests",
		bCfg.depPath .. "glm",
		bCfg.depPath .. "Catch/include",
		bCfg.depPath .. "BitSquid/Foundation",
	}
	
	-- VS-specific
	configuration { "vs*"}
		defines { "_WIN32_WINNT=0x0501" }
	configuration {}
	-- Configs
	--libdirs (bCfg.libOutPath)
	configuration "Debug*"
		targetdir ( bCfg.outPath .. "Debug/" )
	configuration "Release*"
		targetdir ( bCfg.outPath .. "Release/" )
	configuration {}
	
	--Disable some warnings
	buildoptions 
	{ 
		"/wd\"4127\"", -- VS: Conditional expression is constant ( from logging)
		"/wd\"4512\"", -- VS: assignment operator could not be generated
	}
	
	--Other projects or precompiled libs
	links 
	{ 
		"BoltsCore"
	}