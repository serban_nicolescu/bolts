#include <catch.hpp>
#include <bolts_hash.h>
#include <string>

namespace {
	const char*	c_testScope = "[string_table]";
}

using namespace Bolts;

TEST_CASE( "Table works", c_testScope ) {
	ClearStrings();
	hash32_t h1 = stringHash_t("Bla");
	RegisterStringHash(h1, "Bla", strlen("Bla"));
	const char* str = GetHashString(h1);
	REQUIRE( str );
	REQUIRE( strcmp(str, "Bla") == 0);
}

TEST_CASE( "Table copies strings", c_testScope ) {
	ClearStrings();
	hash32_t h1 = stringHash_t("LongAssString");
	char bla[50] = {};
	strcpy(bla, "LongAssString");
	const char* regStr = RegisterStringHash(h1, bla, strlen("LongAssString"));
	const char* str = GetHashString(h1);
	REQUIRE(str == regStr);
	REQUIRE(str != bla);
	REQUIRE(strcmp(str, bla) == 0);
	const char* str2 = GetHashString(h1);
	REQUIRE(str2 != bla);
	REQUIRE(strcmp(str2, bla) == 0);
	REQUIRE(str == str2);
}

TEST_CASE("Table works out string length", c_testScope) {
	ClearStrings();
	hash32_t h1 = stringHash_t("LongAssString");
	char bla[50] = {};
	strcpy(bla, "LongAssString");
	RegisterStringHash(h1, bla);
	const char* str = GetHashString(h1);
	REQUIRE(str != bla);
	REQUIRE(strcmp(str, bla) == 0);
}

TEST_CASE("More strings", c_testScope) {
	ClearStrings();
	hash32_t h1 = stringHash_t("Bla");
	hash32_t h2 = stringHash_t("Bla2");
	RegisterStringHash(h1, "Bla");
	RegisterStringHash(h2, "Bla2");
	const char* str1 = GetHashString(h1);
	const char* str2 = GetHashString(h2);
	REQUIRE(str1);
	REQUIRE(str2);
	REQUIRE(strcmp(str1, "Bla") == 0);
	REQUIRE(strcmp(str2, "Bla2") == 0);
}

TEST_CASE("Hash collisions", c_testScope) {
	ClearStrings();
	hash32_t h1 = 1;
	hash32_t h2 = 1 + (1 << 8);
	hash32_t h3 = 1 + (1 << 8) + (1 << 16);
	hash32_t h4 = 1 + (1 << 8) + (1 << 16) + (1 << 24);
	RegisterStringHash(h1, "Bla");
	RegisterStringHash(h2, "Bla2");
	RegisterStringHash(h3, "Bla3");
	RegisterStringHash(h4, "Bla4");
	const char* str1 = GetHashString(h1);
	const char* str2 = GetHashString(h2);
	const char* str3 = GetHashString(h3);
	const char* str4 = GetHashString(h4);
	REQUIRE(str1);
	REQUIRE(str2);
	REQUIRE(str3);
	REQUIRE(str4);
	REQUIRE(strcmp(str1, "Bla") == 0);
	REQUIRE(strcmp(str2, "Bla2") == 0);
	REQUIRE(strcmp(str3, "Bla3") == 0);
	REQUIRE(strcmp(str4, "Bla4") == 0);
}

TEST_CASE("Even more strings", c_testScope) {
	ClearStrings();

	char bla[50];

	int num = 0;
	while (num++ < 50) {
		sprintf(bla, "Rocky%d", num);
		hash32_t h = stringHash_t((const char*)bla);
		RegisterStringHash(h, bla);
		const char* str = GetHashString(h);
		REQUIRE(str);
		REQUIRE(strcmp(str, bla) == 0);
	}
	num = 0;
	while (num++ < 50) {
		sprintf(bla, "Rocky%d", num);
		hash32_t h = stringHash_t((const char*)bla);
		const char* str = GetHashString(h);
		REQUIRE(str);
		REQUIRE(strcmp(str, bla) == 0);
	}
}

TEST_CASE("Unregistered hashes", c_testScope) {
	ClearStrings();
	hash32_t h1 = 1;
	hash32_t h2 = 1 + (1 << 8);
	hash32_t h3 = 1 + (1 << 8) + (1 << 16);
	hash32_t h4 = 1 + (1 << 8) + (1 << 16) + (1 << 24);
	RegisterStringHash(h1, "Bla");
	RegisterStringHash(h2, "Bla2");
	const char* str1 = GetHashString(h1);
	const char* str2 = GetHashString(h2);
	const char* str3 = GetHashString(h3);
	const char* str4 = GetHashString(h4);
	REQUIRE(str1);
	REQUIRE(str2);
	REQUIRE(!str3);
	REQUIRE(!str4);
	REQUIRE(strcmp(str1, "Bla") == 0);
	REQUIRE(strcmp(str2, "Bla2") == 0);
}

TEST_CASE("Test auto-registrer const hash", c_testScope) {
	ClearStrings();
	regHash_t r{ "LongAssStrings" };
	hash32_t h1 = r;
	const char* str = GetHashString(h1);
	REQUIRE(str);
	REQUIRE(str == r.s);
	REQUIRE(str != "LongAssStrings");
	REQUIRE(strcmp(str, "LongAssStrings") == 0);
}

TEST_CASE("Test auto-registrer runtime hash", c_testScope) {
	ClearStrings();
	const char* string = "Yosemite Sam";
	regHash_t r{ string };
	hash32_t h1 = r;
	const char* str = GetHashString(h1);
	REQUIRE(str);
	REQUIRE(str == r.s);
	REQUIRE(str != string);
	REQUIRE(strcmp(str, string) == 0);
}

TEST_CASE("Test auto-registrer runtime string", c_testScope) {
	ClearStrings();
	Bolts::str_t string = "Yosemite Sam";
	regHash_t r{ string };
	hash32_t h1 = r;
	const char* str = GetHashString(h1);
	REQUIRE(str);
	REQUIRE(str == r.s);
	REQUIRE(string == str);
}
TEST_CASE("Test auto-registrer implicit ctor", c_testScope) {
	ClearStrings();
	const char* string = "Yosemite Sam";
	auto bla = [](regHash_t t)
	{
		hash32_t h1 = t;
		const char* str = GetHashString(h1);
		REQUIRE(str);
		REQUIRE(str == t.s);
	};
	bla({ string }); //NOTE: Need the parenthesis to hint we want an implicit conversion
}