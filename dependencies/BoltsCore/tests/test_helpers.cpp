#include "bolts_types.h"
#include <catch.hpp>

namespace {
	const char*	c_testScope = "[helpers]";
};


template<typename T>
typename std::enable_if< Bolts::pass_by_value<T>(), bool>::type type_is_pass_by_value()
{
	return true;
}

template<typename T>
typename std::enable_if< !Bolts::pass_by_value<T>(), bool>::type type_is_not_pass_by_value()
{
	return true;
}

using namespace Bolts;

TEST_CASE("Templates", c_testScope) {
	REQUIRE(type_is_pass_by_value<bool>());
	REQUIRE(type_is_pass_by_value<uint8_t>());
	REQUIRE(type_is_pass_by_value<vec2>());
	REQUIRE(type_is_pass_by_value<vec3>());
	REQUIRE(type_is_pass_by_value<vec4>());
	REQUIRE(type_is_pass_by_value<quat>());
	REQUIRE(type_is_not_pass_by_value<mat3>());
	REQUIRE(type_is_not_pass_by_value<mat4>());
};