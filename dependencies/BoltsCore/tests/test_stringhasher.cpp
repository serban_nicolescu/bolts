#include <catch.hpp>
#include <bolts_hash.h>
#include <string>

namespace {
	const char*	c_testScope = "[string_hash]";
}

using namespace Bolts;

TEST_CASE( "Hash is consistent", c_testScope ) {
	const char caStr[] = "Bla1";
	const hash32_t hashVal = 0xbb962be6;
	REQUIRE( HashString( caStr ) == hashVal );
	const std::string str = caStr;
	REQUIRE( HashString( str ) == hashVal );
	const char* cStr = caStr;
	REQUIRE( Bolts::HashString( cStr ) == hashVal );
}

TEST_CASE( "Hash 2", c_testScope ) {
	const char caStr[] = "Bla1";
	const hash32_t hashVal = 0xbb962be6;
	REQUIRE( HashString( caStr ) == hashVal );
	const hash32_t hashCalc = HashString( caStr );
	REQUIRE( hashCalc == hashVal );
	REQUIRE( HashString( caStr ) == hashCalc );
}

TEST_CASE( "Hashes are different", c_testScope ) {
	const char caStr1[] = "Bla1";
	const char caStr1_again[] = "Bla1";
	const char caStr2[] = "Bla2";
	const char caStr3[] = "Bla3";
	const hash32_t hashVal1 = HashString( caStr1 );
	const hash32_t hashVal2 = HashString( caStr2 );
	const hash32_t hashVal3 = HashString( caStr3 );

	REQUIRE_FALSE( hashVal1 == hashVal2 );
	REQUIRE_FALSE( hashVal1 == hashVal3 );
	REQUIRE_FALSE( hashVal2 == hashVal3 );
	REQUIRE( HashString( caStr1_again ) == hashVal1 );
}

TEST_CASE( "Hash const str is consistent", c_testScope ) {
	const char caStr1[] = "Bla1";
	const char caStr1_again[] = "Bla1";
	const hash32_t hashVal1 = HashString( caStr1 );

	REQUIRE( HashString( caStr1_again ) == hashVal1 );
}

TEST_CASE("Template hash is consistent", c_testScope) {

	// Inline-ing test
	hash32_t bb = HashMM("BillyBobThornton");
	//printf("%d", bb);


	REQUIRE(HashMM("Bla1") == HashString("Bla1"));
	REQUIRE(HashMM("Bla") == HashString("Bla"));
	REQUIRE(HashMM("Bl") == HashString("Bl"));
	REQUIRE(HashMM("B") == HashString("B"));
	REQUIRE(HashMM("BillyBobThornton") == HashString("BillyBobThornton"));
	REQUIRE(HashMM("AllThe ships at c") == HashString("AllThe ships at c"));
}

TEST_CASE("stringHash type", c_testScope) {
	stringHash_t t = "Tony Danza";
	stringHash_t t2{ "Tony Danza" };

	REQUIRE(HashMM("Bla1") == stringHash_t("Bla1"));
	REQUIRE(stringHash_t("Bla") == HashString("Bla"));
}