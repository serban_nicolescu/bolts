#include <catch.hpp>
#include <bolts_slotmap.h>

namespace {
	const char*	c_testScope = "[slot_map]";
}

using namespace Bolts;

TEST_CASE("[slot_map]" "Basic 1", c_testScope) {
	SlotMap<uint64_t, true> intCtr;
	uint32_t id0 = intCtr.Add();
	uint32_t id1 = intCtr.Add(42);
	uint32_t id2 = intCtr.Add(333);
	REQUIRE(id0 == 0);
	REQUIRE(id1 == 1);
	REQUIRE(id2 == 2);
	REQUIRE(intCtr.Capacity() == 3);
	REQUIRE(intCtr.Get(id0) == 0);
	REQUIRE(intCtr.Get(id1) == 42);
	REQUIRE(intCtr.Get(id2) == 333);

	SECTION("Remove 1") {
		intCtr.Remove(id0);
		REQUIRE(intCtr.Capacity() == 3);
	}

	SECTION("Remove 2") {
		intCtr.Remove(id1);
		REQUIRE(intCtr.Capacity() == 3);
	}

	SECTION("Remove 3") {
		intCtr.Remove(id2);
		// Removing the last item reduces array size
		REQUIRE(intCtr.Capacity() == 2);
	}

	SECTION("Remove 4") {
		intCtr.Remove(id2);
		intCtr.Remove(id1);
		REQUIRE(intCtr.Capacity() == 1);
	}

	SECTION("Remove All in order") {
		intCtr.Remove(id2);
		intCtr.Remove(id1);
		intCtr.Remove(id0);
		REQUIRE(intCtr.Capacity() == 0);
	}

	SECTION("Remove All shuffled") {
		intCtr.Remove(id1);
		intCtr.Remove(id0);
		intCtr.Remove(id2);
		REQUIRE(intCtr.Capacity() == 2);
	}

	SECTION("Remove & Add") {
		intCtr.Remove(id1);
		uint32_t idNew = intCtr.Add(82);
		REQUIRE(idNew == 1);
		REQUIRE(intCtr.Get(id1) == 82);
		REQUIRE(intCtr.Capacity() == 3);
	}

	SECTION("Remove & Add 2") {
		intCtr.Remove(id1);
		uint32_t idNew0 = intCtr.Add(82);
		uint32_t idNew1 = intCtr.Add(123);
		REQUIRE(intCtr.Capacity() == 4);
		REQUIRE(idNew0 == 1);
		REQUIRE(idNew1 == 3);
		REQUIRE(intCtr.Get(idNew1) == 123);
	}

	SECTION("Remove All & Add") {
		intCtr.Remove(id0);
		intCtr.Remove(id1);
		intCtr.Remove(id2);
		uint32_t idNew0 = intCtr.Add(82);
		uint32_t idNew1 = intCtr.Add(123);
		uint32_t idNew2 = intCtr.Add();
		REQUIRE(intCtr.Get(idNew0) == 82);
		REQUIRE(intCtr.Get(idNew1) == 123);
		REQUIRE(intCtr.Get(idNew2) == 0);
		REQUIRE(intCtr.Capacity() == 3);
	}

}