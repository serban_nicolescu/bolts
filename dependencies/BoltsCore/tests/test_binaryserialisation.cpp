#include "bolts_binarymap.h"
#include "bolts_hash.h"
#include "bolts_binaryserialization.h"
#include <catch.hpp>

namespace {
	const char*	c_testScope = "[binary_serialisation]";
};

using namespace Bolts;

//struct TestSpecialised
//{
//	bool operator==(const TestSpecialised& other) {
//		return (someInt == other.someInt && someString == other.someString);
//	}
//
//	void Serialize(Bolts::Blob& b) const {
//		b.Store(someInt);
//		//b.Store(someString);
//	}
//
//	int someInt = 30;
//	//str_t someString;
//};


TEST_CASE("Blob", c_testScope) {
	Blob b;

	SECTION("PODs") {
		b.Store(vec3{});
		b.Store((uint32_t)0);
		b.Store(true);
		REQUIRE(b.Size() == sizeof(vec3) + sizeof(uint32_t) + sizeof(bool));
	};

	SECTION("Strings Write") {
		const char* s[] = {
			"Bla", "Bli", "Blu"
		};
		b.Store(s[0]);
		b.Store(s[1], strlen(s[1]));
		b.Store(str_t(s[2]));
		REQUIRE(b.Size() == (strlen(s[0]) + 1) * 3);
		Blob::Reader r = b.MakeReader();
		str_t out;
		for (int i = 0; i < countof(s); i++) {
			r.Read(out);
			REQUIRE(out == s[i]);
			out.clear();
		}
		REQUIRE(r.Remaining() == 0);
	};

	SECTION("POD Vector") {
		std::vector<uint32_t> uintv;
		uintv.push_back(42);
		uintv.push_back(4242);
		uintv.push_back(424242);
		b.Store(uintv);
		InplaceArray<uint32_t>* inplacev = (InplaceArray<uint32_t>*)b.Data();
		int i = 0;
		REQUIRE(inplacev->count == uintv.size());
		for (uint32_t val : *inplacev) {
			REQUIRE(uintv[i] == val);
			REQUIRE(uintv[i] == inplacev->Get(i));
			i++;
		}
	};

	/*SECTION("Non-POD Type") {
		TestSpecialised t;
		static_assert(!Bolts::serialization::is_specialised<int>(), "Specialised serialisation trait doesn't work");
		static_assert(Bolts::serialization::is_specialised<TestSpecialised>(), "Specialised serialisation trait doesn't work");
		t.someInt = 25;
		b.Store(t);
	};*/

	SECTION("Vector of strings") {
		std::vector<std::string> stdv;
		stdv.push_back("Some");
		stdv.push_back("times");
		stdv.push_back("it");
		stdv.push_back("works.");
		b.Store(stdv);
		InplaceOffsetArray<char>* inplacev = (InplaceOffsetArray<char>*)b.Data();
		int i = 0;
		REQUIRE(inplacev->Count() == stdv.size());
		for (auto val : *inplacev) {
			REQUIRE(stdv[i] == val);
			REQUIRE(stdv[i] == inplacev->Get(i));
			i++;
		}
	};

	/*SECTION("Vector of specialised things") {
		std::vector<TestSpecialised> stdv;
		stdv.push_back({ 42, "John" });
		b.Store(stdv);
		InplaceOffsetArray<TestSpecialised>* inplacev = (InplaceOffsetArray<TestSpecialised>*)b.Data();
		int i = 0;
		REQUIRE(inplacev->Count() == stdv.size());
		for (auto val : *inplacev) {
			REQUIRE(stdv[i] == *val);
			REQUIRE(stdv[i] == *inplacev->Get(i));
			i++;
		}
	}*/

	//TODO: Test Blob::StorePtr
	//TODO: Test storing & reading of InplaceArray
	//TODO: Test storing & reading of InplaceOffsetArray
}
