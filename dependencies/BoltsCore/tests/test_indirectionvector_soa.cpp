#include <catch.hpp>
#include <bolts_indirectionvectorSOA.h>

namespace {
	const char*	c_testScope = "[indirection_vector_soa]";
}

using namespace Bolts;

struct Test :public IndirectionVectorSOA<Test>
{
	template<typename... Args>
	void Emplace(Args&&... args) {
		m_data.emplace_back(std::forward<Args>(args)...);
	}

	void PopBack()
	{
		m_data.pop_back();
	}
	uint32_t Size() const
	{
		return m_data.size();
	}
	void Swap(uint32_t idx1, uint32_t idx2)
	{
		std::swap(m_data[idx1], m_data[idx2]);
	}

	int Get(id_t id)
	{
		return m_data[GetIdx(id)];
	}

	Bolts::vec_t<int> m_data;
};

TEST_CASE("[indirection_vector_soa]" "Bla", c_testScope) {
	Test vec;
	uint32_t id1 = vec.AddActive(1);
	uint32_t id2 = vec.AddActive(2);
	uint32_t id3 = vec.AddActive();
	REQUIRE(id1 == 0);
	REQUIRE(vec.Size() == 3);
	REQUIRE(vec.SizeActive() == 3);
	REQUIRE(vec.IsActive(id1));
	REQUIRE(vec.IsActive(id2));

	REQUIRE(vec.Get(id1) == 1);
	REQUIRE(vec.Get(id2) == 2);
	REQUIRE(vec.Get(id3) == 0);

	SECTION("Activate active item") {
		vec.Activate(id1);
		REQUIRE(vec.SizeActive() == 3);
		REQUIRE(vec.IsActive(id1));
		REQUIRE(vec.IsActive(id2));
	}
	SECTION("Deactivate") {
		vec.Deactivate(id1);
		REQUIRE_FALSE(vec.IsActive(id1));
		REQUIRE(vec.IsActive(id2));
		REQUIRE(vec.Size() == 3);
		REQUIRE(vec.SizeActive() == 2);
		REQUIRE(vec.Get(id1) == 1);
		REQUIRE(vec.Get(id2) == 2);
		REQUIRE(vec.Get(id3) == 0);
	}
	SECTION("Deactivate Add") {
		vec.Deactivate(id1);
		auto id4 = vec.AddActive(4);
		REQUIRE_FALSE(vec.IsActive(id1));
		REQUIRE(vec.IsActive(id2));
		REQUIRE(vec.Size() == 4);
		REQUIRE(vec.SizeActive() == 3);
		REQUIRE(vec.Get(id1) == 1);
		REQUIRE(vec.Get(id2) == 2);
		REQUIRE(vec.Get(id3) == 0);
		REQUIRE(vec.Get(id4) == 4);
	}
	SECTION("Activate") {
		vec.Activate(id1);
		REQUIRE(vec.SizeActive() == 3);
		REQUIRE(vec.IsActive(id1));
		REQUIRE(vec.IsActive(id2));
	}
	SECTION("Double Deactivate") {
		vec.Deactivate(id2);
		vec.Deactivate(id2);
		REQUIRE_FALSE(vec.IsActive(id2));
		REQUIRE(vec.IsActive(id1));
		REQUIRE(vec.IsActive(id3));
		REQUIRE(vec.Size() == 3);
		REQUIRE(vec.SizeActive() == 2);
		REQUIRE(vec.Get(id1) == 1);
		REQUIRE(vec.Get(id2) == 2);
		REQUIRE(vec.Get(id3) == 0);
	}
	SECTION("Double Activate") {
		vec.Activate(id1);
		vec.Activate(id1);
		REQUIRE(vec.SizeActive() == 3);
		REQUIRE(vec.IsActive(id1));
		REQUIRE(vec.IsActive(id2));
	}
	SECTION("Activate Deactivate Activate") {
		vec.Deactivate(id1);
		vec.Deactivate(id2);
		vec.Activate(id1);
		REQUIRE(vec.SizeActive() == 2);
		REQUIRE(vec.IsActive(id1));
		REQUIRE_FALSE(vec.IsActive(id2));
		REQUIRE(vec.IsActive(id3));
	}
	SECTION("Remove") {
		vec.Remove(id2);
		REQUIRE(vec.IsActive(id1));
		REQUIRE(vec.IsActive(id3));
		REQUIRE(vec.Size() == 2);
		REQUIRE(vec.SizeActive() == 2);
		REQUIRE(vec.Get(id1) == 1);
		REQUIRE(vec.Get(id3) == 0);
	}
	SECTION("Remove Inactive") {
		vec.Remove(id2);
		vec.Deactivate(id1);
		REQUIRE_FALSE(vec.IsActive(id1));
		vec.Remove(id1);
		REQUIRE(vec.IsActive(id3));
		REQUIRE(vec.Size() == 1);
		REQUIRE(vec.SizeActive() == 1);
		REQUIRE(vec.Get(id3) == 0);
	}
	SECTION("Remove Add") {
		vec.Remove(id2);
		uint32_t id4 = vec.AddActive(4);
		REQUIRE(vec.IsActive(id1));
		REQUIRE(vec.IsActive(id3));
		REQUIRE(vec.Size() == 3);
		REQUIRE(vec.SizeActive() == 3);
		REQUIRE(vec.Get(id1) == 1);
		REQUIRE(vec.Get(id3) == 0);
		REQUIRE(vec.Get(id4) == 4);
	}
}