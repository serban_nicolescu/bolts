#include "bolts_binarymap.h"
#include "bolts_hash.h"
#include <catch.hpp>

namespace {
	const char*	c_testScope = "[binary_map]";
};

using namespace Bolts;

TEST_CASE("BinaryBuilder_Basic1", c_testScope) {
	//Setup
	const char key1[] = "TestVar1";
	const char key2[] = "TestVar2";
	//Build data
	BinaryMapBuilder bb;
	const uint16_t v1 = 42;
	const uint32_t v2 = 65;
	bb.Store(key1, v1);
	bb.Store(key2, v2);
	//Test map conversion
	BinaryMap bm = bb.Build(); // Grab built binary map
	BinaryMapView bmv = bm;
	//
	REQUIRE(bm.GetSize() == 2);
	REQUIRE(bmv.GetSize() == 2);
	//Getters
	//	Validate data
	uint16_t outVar1;
	REQUIRE(bm.Get(key1, outVar1));
	REQUIRE(outVar1 == 42);
	uint32_t outVar2;
	REQUIRE(bm.Get(key2, outVar2));
	REQUIRE(outVar2 == 65);
	//	Check invalid gets
	REQUIRE_FALSE(bm.Get("WrongKey", outVar1));
	REQUIRE(outVar1 == 42);//Make sure output variable is unchanged
	uint8_t wrongTypeVar = 200;
	REQUIRE_FALSE(bm.Get(key1, wrongTypeVar));//Check for type safety
	REQUIRE(wrongTypeVar == 200);//Make sure output variable is unchanged
}

TEST_CASE("BinaryBuilder_AllTypes", c_testScope) {
	//Build data
	BinaryMapBuilder bb;
	const bool b = true;
	const uint8_t u8 = 200;
	const int8_t i8 = -120;
	const uint16_t u16 = 10000;
	const int16_t i16 = -1;
	const uint32_t u32 = 2345213;
	const int32_t i32 = -2000000;
	const float fl = 42.f;
	const double  dbl = 42.0;
	const vec2 v2{ 1.f, 2.f};
	const vec3 v3{ 1.f, 2.f, 3.f };
	const vec4 v4{ 1.f, 2.f, 3.f, 4.f };
	const quat q{ 2.f, 1.f, 0.f, 0.f };
	const mat3 m3;
	const mat4 m4;
	const std::string s = "Bla bla bla, I'm a std::string";
	const char* const cs = "Bla bla bla, I'm a c-string";
	bb.Store("ThisABool", b);
	bb.Store("UInt8", u8);
	bb.Store("Int8", i8);
	bb.Store("UInt16", u16);
	bb.Store("Int16", i16);
	bb.Store("UInt32", u32);
	bb.Store("Int32", i32);
	bb.Store("FloatNr", fl);
	bb.Store("DoubleNr", dbl);
	bb.Store("Vector2", v2);
	bb.Store("Long ass key name for vector 3", v3);
	bb.Store("Bla", v4);
	bb.Store("QUat", q);
	bb.Store("Mat3", m3);
	bb.Store("Matrix4", m4);
	bb.Store("String1", s);
	bb.Store("String2", cs);
	// Grab built binary map
	BinaryMap bm = bb.Build();
	// Read all types
	bool outb = false;
	uint8_t outu8 = 12;
	int8_t outi8 = 13;
	uint16_t outu16 = 0;
	int16_t outi16 = 8;
	uint32_t outu32;
	int32_t outi32;
	float outfl;
	double  outdbl;
	vec2 outv2{};
	vec3 outv3{};
	vec4 outv4{};
	quat outq{};
	mat3 outm3;
	outm3[2][2] = 42.f;
	mat4 outm4;
	outm4[3][3] = 420.f;
	std::string outs = "dsdewdqwd";
	const char* outcs = nullptr;
	REQUIRE(bm.Get("ThisABool",	outb));
	REQUIRE(bm.Get("UInt8",		outu8));
	REQUIRE(bm.Get("Int8",		outi8));
	REQUIRE(bm.Get("UInt16",	outu16));
	REQUIRE(bm.Get("Int16",		outi16));
	REQUIRE(bm.Get("UInt32",	outu32));
	REQUIRE(bm.Get("Int32",		outi32));
	REQUIRE(bm.Get("FloatNr",	outfl));
	REQUIRE(bm.Get("DoubleNr",	outdbl));
	REQUIRE(bm.Get("Vector2",	outv2));
	REQUIRE(bm.Get("Long ass key name for vector 3", outv3));
	REQUIRE(bm.Get("Bla",		outv4));
	REQUIRE(bm.Get("QUat",		outq));
	REQUIRE(bm.Get("Mat3",		outm3));
	REQUIRE(bm.Get("Matrix4",	outm4));
	REQUIRE(bm.Get("String1",	outs));
	REQUIRE(bm.Get("String2",	outcs));
	REQUIRE(outb == b);
	REQUIRE(outu8 == u8);
	REQUIRE(outi8 == i8);
	REQUIRE(outu16 == u16);
	REQUIRE(outi16 == i16);
	REQUIRE(outu32 == u32);
	REQUIRE(outi32 == i32);
	REQUIRE(outfl == fl);
	REQUIRE(outdbl == dbl);
	REQUIRE(outv2 == v2);
	REQUIRE(outv3 == v3);
	REQUIRE(outv4 == v4);
	REQUIRE(outq == q);
	REQUIRE(outm3 == m3);
	REQUIRE(outm4 == m4);
	REQUIRE(outs == s);
	REQUIRE( strcmp(outcs, cs) == 0);
	// Same type, different key
	const char* outCStr;
	std::string outStr;
	REQUIRE(bm.Get("String1", outStr));
	REQUIRE(outStr == s);
	REQUIRE(bm.Get("String2", outStr));
	REQUIRE(outStr == cs);
	REQUIRE(bm.Get("String1", outCStr));
	// Wrong key
	REQUIRE_FALSE(bm.Get("WrongKey", outb));
	REQUIRE(outb == b);
	// Wrong type
	REQUIRE_FALSE(bm.Get("ThisABool", outdbl));
	REQUIRE(outdbl == dbl);
}

TEST_CASE("BinaryBuilder_AllTypesWith[]", c_testScope) {
	//Build data
	BinaryMapBuilder bb;
	const bool b = true;
	const uint8_t u8 = 200;
	const int8_t i8 = -120;
	const uint16_t u16 = 10000;
	const int16_t i16 = -1;
	const uint32_t u32 = 2345213;
	const int32_t i32 = -2000000;
	const float fl = 42.f;
	const double  dbl = 42.0;
	const vec2 v2{ 1.f, 2.f };
	const vec3 v3{ 1.f, 2.f, 3.f };
	const vec4 v4{ 1.f, 2.f, 3.f, 4.f };
	const quat q{ 2.f, 1.f, 0.f, 0.f };
	const mat3 m3;
	const mat4 m4;
	const std::string s = "Bla bla bla, I'm a std::string";
	const char* const cs = "Bla bla bla, I'm a c-string";
	bb.Store("ThisABool", b);
	bb.Store("UInt8", u8);
	bb.Store("Int8", i8);
	bb.Store("UInt16", u16);
	bb.Store("Int16", i16);
	bb.Store("UInt32", u32);
	bb.Store("Int32", i32);
	bb.Store("FloatNr", fl);
	bb.Store("DoubleNr", dbl);
	bb.Store("Vector2", v2);
	bb.Store("Long ass key name for vector 3", v3);
	bb.Store("Bla", v4);
	bb.Store("QUat", q);
	bb.Store("Mat3", m3);
	bb.Store("Matrix4", m4);
	bb.Store("String1", s);
	bb.Store("String2", cs);
	// Grab built binary map
	BinaryMap bm = bb.Build();
	// Read all types
	bool outb = false;
	uint8_t outu8 = 12;
	int8_t outi8 = 13;
	uint16_t outu16 = 0;
	int16_t outi16 = 8;
	uint32_t outu32;
	int32_t outi32;
	float outfl;
	double  outdbl;
	vec2 outv2{};
	vec3 outv3{};
	vec4 outv4{};
	quat outq{};
	mat3 outm3;
	outm3[2][2] = 42.f;
	mat4 outm4;
	outm4[3][3] = 420.f;
	std::string outs = "dsdewdqwd";
	const char* outcs = nullptr;

	auto bmit = bm["ThisABool"];
	bm.Find("bla");

	REQUIRE(bm["ThisABool"].Get(outb));
	REQUIRE(bm["UInt8"].Get(outu8));
	REQUIRE(bm["Int8"].Get(outi8));
	REQUIRE(bm["UInt16"].Get(outu16));
	REQUIRE(bm["Int16"].Get(outi16));
	REQUIRE(bm["UInt32"].Get(outu32));
	REQUIRE(bm["Int32"].Get(outi32));
	REQUIRE(bm["FloatNr"].Get(outfl));
	REQUIRE(bm["DoubleNr"].Get(outdbl));
	REQUIRE(bm["Vector2"].Get(outv2));
	REQUIRE(bm["Long ass key name for vector 3"].Get(outv3));
	REQUIRE(bm["Bla"].Get(outv4));
	REQUIRE(bm["QUat"].Get(outq));
	REQUIRE(bm["Mat3"].Get(outm3));
	REQUIRE(bm["Matrix4"].Get(outm4));
	REQUIRE(bm["String1"].Get(outs));
	REQUIRE(bm["String2"].Get(outcs));
	REQUIRE(outb == b);
	REQUIRE(outu8 == u8);
	REQUIRE(outi8 == i8);
	REQUIRE(outu16 == u16);
	REQUIRE(outi16 == i16);
	REQUIRE(outu32 == u32);
	REQUIRE(outi32 == i32);
	REQUIRE(outfl == fl);
	REQUIRE(outdbl == dbl);
	REQUIRE(outv2 == v2);
	REQUIRE(outv3 == v3);
	REQUIRE(outv4 == v4);
	REQUIRE(outq == q);
	REQUIRE(outm3 == m3);
	REQUIRE(outm4 == m4);
	REQUIRE(outs == s);
	REQUIRE(strcmp(outcs, cs) == 0);
	// Same type, different key
	const char* outCStr;
	std::string outStr;
	REQUIRE(bm["String1"].Get(outStr));
	REQUIRE(outStr == s);
	REQUIRE(bm["String2"].Get(outStr));
	REQUIRE(outStr == cs);
	REQUIRE(bm["String1"].Get(outCStr));
	// Wrong key
	REQUIRE_FALSE(bm["WrongKey"].Get(outb));
	REQUIRE(outb == b);
	// Wrong type
	REQUIRE_FALSE(bm["ThisABool"].Get(outdbl));
	REQUIRE(outdbl == dbl);
}

TEST_CASE("BinaryBuilder_NestedBM", c_testScope) {
	//Setup
	const char key1[] = "TestVar1";
	const char key2[] = "TestVar2";
	const char key3[] = "NestedMap";
	//Build data
	BinaryMapBuilder bb;
	const uint16_t v1 = 42;
	const uint16_t v2 = 65;
	bb.Store(key1, v1);
	bb.Store(key2, v2);
	BinaryMap bm1 = bb.Build();
	bb.Store(key3, bm1);
	BinaryMap bmparent = bb.Build();
	//Getters
	//	Validate data
	BinaryMapView bmchild;
	REQUIRE_FALSE(bmchild.IsValid());
	uint16_t outVar1;
	REQUIRE_FALSE(bmchild[key1].Get(outVar1));
	REQUIRE(bmparent.Get(key3, bmchild));
	REQUIRE(bmchild.Get(key1, outVar1));
	REQUIRE(outVar1 == v1);
	uint16_t outVar2;
	REQUIRE(bmchild.Get(key2, outVar2));
	REQUIRE(outVar2 == v2);
	//	Check invalid gets
	uint8_t wrongTypeVar = 0;
	REQUIRE_FALSE(bmparent.Get(key3, outVar1));
	REQUIRE_FALSE(bmparent.Get(key1, bmchild));
	REQUIRE_FALSE(bmparent.Get(key1, outVar1));
	// Check [] operator
	REQUIRE( bmparent[key3][key1].Get(outVar1));
	REQUIRE(outVar1 == v1);
	REQUIRE( bmparent[key3][key2].Get(outVar2));
	REQUIRE(outVar2 == v2);
	outVar2 = v1 + v2;
	REQUIRE_FALSE(bmparent["NotAMap"]["FalseKEy"]["Bla"].Get(outVar2));
	REQUIRE(outVar2 == (v1 + v2));
}

TEST_CASE("Array", c_testScope) {
	SECTION("Simple") {
		const Bolts::vec_t<uint8_t> arrb = { 2,3,51,5,62,4 };
		BinaryMapBuilder bb;
		bb.Store("Some byte array", arrb);
		BinaryMap bm = bb.Build();
		//
		auto bmarr1 = bm.GetArray<uint8_t>("Some byte array");
		REQUIRE(bmarr1.IsValid());
		REQUIRE(bmarr1.TypeIs<uint8_t>());
		REQUIRE_FALSE(bmarr1.TypeIs<double>());
		REQUIRE(bmarr1.KeyIs("Some byte array"));
		REQUIRE(bmarr1.GetSize() == arrb.size());
		REQUIRE(bmarr1[0] == arrb[0]);
		REQUIRE(bmarr1[arrb.size() - 1] == arrb[arrb.size() - 1]);
		unsigned i = 0;
		for (uint8_t v : bmarr1)
		{
			REQUIRE(v == arrb[i]);
			i++;
		}
		i = 0;
		auto wrongIt = bmarr1.begin();
		for (auto it = bmarr1.begin(), end = bmarr1.end(); it != end; ++it)
		{
			REQUIRE(i == it.Index());
			REQUIRE(*it == arrb[it.Index()]);
			REQUIRE(it.IsValid());
			i++;
			wrongIt = it;
		}
		++wrongIt;
		REQUIRE_FALSE(wrongIt.IsValid());

	};
	SECTION("Null-object") {
		BinaryMapArrayView<float> arr;
		REQUIRE_FALSE(arr.IsValid());
		REQUIRE(arr.TypeIs<float>());
		REQUIRE(arr[42] == float());
		auto arrIt = arr.begin();
		REQUIRE_FALSE(arrIt.IsValid());
		REQUIRE(*arrIt == float());
		++arrIt;
		REQUIRE_FALSE(arrIt.IsValid());
		arrIt += 54;
		REQUIRE_FALSE(arrIt.IsValid());
	};
	SECTION("Complex") {
		const Bolts::vec_t<uint8_t> arrb = { 2,3,51,5,62,4 };
		const Bolts::vec_t<double> arrd = { 5.0, 42.0, 75.0};
		const uint16_t u16 = 414;
		const bool b = false;
		BinaryMapBuilder bb;
		bb.Store("anotherbool", b);
		bb.Store("Some byte array", arrb);
		bb.Store("2 bytes", u16);
		bb.Store("This is a double array", arrd);
		bb.Store("abool", b);
		BinaryMap bm = bb.Build();
		//
		auto outArrb = bm.GetArray<uint8_t>("Some byte array");
		REQUIRE(outArrb.IsValid());
		auto outArrd = bm.GetArray<double>("This is a double array");
		REQUIRE(outArrd.IsValid());
		REQUIRE(outArrb.GetSize() == arrb.size());
		REQUIRE(outArrd.GetSize() == arrd.size());
		unsigned i = 0;
		for (auto v : outArrb)
		{
			REQUIRE(v == arrb[i]);
			i++;
		}
		i = 0;
		for (auto v : outArrd)
		{
			REQUIRE(v == arrd[i]);
			i++;
		}
		//
		outArrb = bm["Some byte array"].AsArray<uint8_t>();
		REQUIRE(outArrb.IsValid());
	};
}

TEST_CASE("BinaryBuilder_Iterators", c_testScope) {
	//Build data
	BinaryMapBuilder bb;
	const bool b = true;
	const uint8_t u8 = 280;
	const int8_t i8 = -120;
	const uint16_t u16 = 10000;
	const int16_t i16 = -1;
	const uint32_t u32 = 2345213;
	const int32_t i32 = -2000000;
	const float fl = 42.f;
	const double  dbl = 42.0;
	const vec2 v2{ 1.f, 2.f };
	const vec3 v3{ 1.f, 2.f, 3.f };
	const vec4 v4{ 1.f, 2.f, 3.f, 4.f };
	const quat q{ 2.f, 1.f, 0.f, 0.f };
	const mat3 m3;
	const mat4 m4;
	const std::string s = "Bla bla bla, I'm a std::string";
	const char* const cs = "Bla bla bla, I'm a c-string";
	bb.Store("ThisABool", b);
	bb.Store("UInt8", u8);
	bb.Store("Int8", i8);
	bb.Store("UInt16", u16);
	bb.Store("Int16", i16);
	bb.Store("UInt32", u32);
	bb.Store("Int32", i32);
	bb.Store("FloatNr", fl);
	bb.Store("DoubleNr", dbl);
	bb.Store("Vector2", v2);
	bb.Store("Long ass key name for vector 3", v3);
	bb.Store("Bla", v4);
	bb.Store("QUat", q);
	bb.Store("Mat3", m3);
	bb.Store("Matrix4", m4);
	bb.Store("String1", s);
	bb.Store("String2", cs);
	// Test BM iterators
	BinaryMap bm = bb.Build();
	REQUIRE(bm.begin().KeyIs("ThisABool"));
	REQUIRE(bm.begin().TypeIs<bool>());
	REQUIRE(bm.begin().TypeIs(b));
	for (auto it : bm)
	{
		REQUIRE(it.IsValid());
	}
	//TODO: Test array iterators
}

TEST_CASE("BinaryBuilder_New", c_testScope) {
	/*
	for ( range_declaration : range_expression ) loop_statement
	<=>
	{
		auto && __range = range_expression ;
		for (auto __begin = __range.begin(), __end = __range.end(); __begin != __end; ++__begin){
			range_declaration = *__begin;
			loop_statement
		}
	}
	*/

	//BinaryMap bm, bm_out;
	//bm.Get<uint32_t>("Bla");
	// uint32_t r = bm.Get("Bla", uint32_t(0));
	//bm.Get<BinaryMap>("Bla");
	//bm.Get("Bla",&bm_out) -> bool;
	//bm["Bla"] -> BinaryMapIt
	//bm[0]-> BinaryMapArrayIt
	//bm.begin() -> BinaryMapIt

	//BinaryMapIt bv;
	// Getters
	//bv.Get<uint32>(0);
	//int i = 8;
	//bv.Get(i) -> bool;
	//bv(i) -> bool
	//if (bm["NumChicken"](i))
	//	print("There are %d chicken",i);
	//
	//BinaryMapArrayIt ba;
	//ba[0] -> BinaryArrayValIt
}