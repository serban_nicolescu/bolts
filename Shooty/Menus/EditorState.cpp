#include "StdAfx.h"
#include "EditorState.h"
#include "Assets/AssetManager.h"
#include "Render/DebugDraw.h"
#include <Rendering/Mesh.h>
#include <System/system.h>
#include <Profiler.h>

#include <Helpers/MeshBuilder.h>
#include <Rendering/MaterialNew.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>

#include "Components/MeshComponent.h"
#include "Components/DirectionalLightComponent.h"

#include "Serialization/PropertiesJSON.h"
#include "Serialization/PropertiesXML.h"
#include "Components/ComponentProperties.h"

#include <imgui.h>
#include "ImGuizmo/ImGuizmo.h"
#include <IconsKenney.h>
//Scenes
#include "Scenes/GameplaySceneNewComp.h"
#include "Scenes/DualContouringScene.h"
#include "Scenes/ShaderToyScene.h"
#include "Scenes/LightmapScene.h"
#include "Scenes/TycoonScene.h"

#include "microprofile.h"

static Bolts::DebugDrawer* g_debugDrawer = nullptr;

namespace Bolts
{
	DebugDrawer* DebugDraw() { return g_debugDrawer; }
};

using namespace Bolts;

bool EditorState::Settings::Load()
{
	propVec_t props;
	if (editor::LoadXMLFile("editor_options.xml", props))
	{
		BOLTS_LINFO() << "[Editor]" << "Loaded options.";
		Bolts::BinaryMapBuilder builder;
		PropVecToBinary(props, builder);
		auto bm = builder.GetTempMap();
		bm.Get("WindowSize", windowSize);
		bm.Get("WindowPos", windowPos);
		bm.Get("Fullscreen", fullscreen);

		bm.Get("Camera Position", cameraPos);
		bm.Get("Camera Target Position", cameraTgtPos);

		bm.Get("CurrentScene", currentOpenScene);
		bm.Get("LastSavedObj", lastSaveObj);
		bm.Get("LastSavedPrefab", lastSavePrefab);
		bm.Get("LastOpenedObj", lastOpenObj);
		bm.Get("LastOpenedPrefab", lastOpenPrefab);

		return true;
	}
	return false;
}

bool EditorState::Settings::Save()
{
	propVec_t props;
	props.emplace_back(MakeDefaultProp<PropUVec2>("WindowSize", windowSize));
	props.emplace_back(MakeDefaultProp<PropUVec2>("WindowPos", windowPos));
	props.emplace_back(MakeDefaultProp<PropBool>("Fullscreen", fullscreen));

	props.emplace_back(MakeDefaultProp<PropVec3>("Camera Position", cameraPos));
	props.emplace_back(MakeDefaultProp<PropVec3>("Camera Target Position", cameraTgtPos));

	props.emplace_back(MakeDefaultProp<PropString>("CurrentScene", currentOpenScene));
	props.emplace_back(MakeDefaultProp<PropString>("LastSavedObj", lastSaveObj));
	props.emplace_back(MakeDefaultProp<PropString>("LastSavedPrefab", lastSavePrefab));
	props.emplace_back(MakeDefaultProp<PropString>("LastOpenedObj", lastOpenObj));
	props.emplace_back(MakeDefaultProp<PropString>("LastOpenedPrefab", lastOpenPrefab));
	if (editor::SaveXMLFile("editor_options.xml", props)) {
		BOLTS_LINFO() << "[Editor]" << "Saved options.";
		return true;
	}
	return false;
}

struct TransformGizmo
{
	void RenderToolbar()
	{
		if (ImGui::RadioButton("None", currentGizmoOperation == -1))
			currentGizmoOperation = ImGuizmo::OPERATION(-1);
		ImGui::SameLine();
		if (ImGui::RadioButton("Translate", currentGizmoOperation == ImGuizmo::TRANSLATE))
			currentGizmoOperation = ImGuizmo::TRANSLATE;
		ImGui::SameLine();
		if (ImGui::RadioButton("Rotate", currentGizmoOperation == ImGuizmo::ROTATE))
			currentGizmoOperation = ImGuizmo::ROTATE;
		ImGui::SameLine();
		if (ImGui::RadioButton("Scale", currentGizmoOperation == ImGuizmo::SCALE))
			currentGizmoOperation = ImGuizmo::SCALE;

		if (currentGizmoOperation != ImGuizmo::SCALE)
		{
			if (ImGui::RadioButton("Local", currentGizmoMode == ImGuizmo::LOCAL))
				currentGizmoMode = ImGuizmo::LOCAL;
			ImGui::SameLine();
			if (ImGui::RadioButton("World", currentGizmoMode == ImGuizmo::WORLD))
				currentGizmoMode = ImGuizmo::WORLD;
		}

		ImGui::Checkbox("", &useSnap);
		ImGui::SameLine();
		snapAmt = {};
		switch (currentGizmoOperation)
		{
		case ImGuizmo::TRANSLATE:
			snapAmt = vec3{ 1.f };
			ImGui::InputFloat3("Snap", &snapAmt[0]);
			break;
		case ImGuizmo::ROTATE:
			snapAmt.x = 5.f;
			ImGui::InputFloat("Angle Snap", &snapAmt.x);
			break;
		case ImGuizmo::SCALE:
			snapAmt.x = 0.1f;
			ImGui::InputFloat("Scale Snap", &snapAmt.x);
			break;
		}
	}

	void RenderApplyGizmo(const Bolts::Camera* activeCam, GOTransform& tComp)
	{
		if (currentGizmoOperation >= 0) {
			ImGuizmo::SetDrawlist();

			auto viewM = activeCam->GetViewMatrix();
			auto projM = activeCam->GetProjectionMatrix();
			auto localM = tComp.GetAbsTransform();
			ImGuizmo::SetRect(0, 0, ImGui::GetIO().DisplaySize.x, ImGui::GetIO().DisplaySize.y);
			ImGuizmo::Manipulate(&viewM[0][0], &projM[0][0], currentGizmoOperation, currentGizmoMode, &localM[0][0], nullptr, useSnap ? &snapAmt.x : nullptr);

			Bolts::vec3 matrixTranslation, matrixRotation, matrixScale;
			ImGuizmo::DecomposeMatrixToComponents(&localM[0][0], &matrixTranslation[0], &matrixRotation[0], &matrixScale[0]);
			tComp.SetPosition(matrixTranslation);
			tComp.SetRotation(Bolts::quat(glm::radians(matrixRotation)));
			tComp.SetScale(matrixScale);
		}
	}

	ImGuizmo::OPERATION		currentGizmoOperation = ImGuizmo::OPERATION(-1);
	ImGuizmo::MODE			currentGizmoMode = ImGuizmo::WORLD;
	Bolts::vec3				snapAmt;
	bool					useSnap = false;
};

static TransformGizmo selectionGizmo;

struct CamControl
{
	float	moveSpeed = 20.f;
	float	lookSpeed = 0.8f;
	float	snapSpeed = 1.f;

	Bolts::goRef_t selectedObject;
};

void CameraFocusObject(Camera* cam, goRef_t go)
{
	if (go) {
		auto meshCmp = go->TryGetComponent<CMgrMesh>();
		if (meshCmp) {
			auto& goT = go->GetTransform();
			//NOTE: VOs use this code to cull:
			//vec3 stupid = glm::max(glm::abs(m_mesh->aabb.min), glm::abs(m_mesh->aabb.max));
			//const float voSphereRadius = glm::length(stupid * goTransform.GetScale());

			AABB& meshBox = meshCmp->GetMesh()->aabb;
			auto bboxExt = meshBox.ext() * 1.5f * goT.GetScale();
			auto targetPos = goT.GetPosition() + meshBox.center();
			float maxSide = bboxExt.x;
			if (maxSide < bboxExt.y) maxSide = bboxExt.y;
			if (maxSide < bboxExt.z) maxSide = bboxExt.z;
			maxSide = std::max(maxSide, 4.f);
			maxSide *= Bolts::c_Sqrt2;
			const float distToFit = maxSide / tan(cam->GetFov() / 2);
			cam->SetPosition(targetPos - (distToFit * cam->GetForward()));
			cam->SetTarget(targetPos);
		}
	}
}

void ControlCamera(Camera* cam, float dt, const CamControl& settings, const Input::KeyStateHolder& keyState, const Input::MouseStateHolder& mouseState)
{
	const float moveSpeed = keyState.IsKeyDown(Input::KC_SHIFT) ? settings.moveSpeed * 10.f: settings.moveSpeed;
	const float lookSpeed = Bolts::c_PI * settings.lookSpeed;
	using namespace Input;

	// Mouse look
	if (mouseState.IsButtonDown(Bolts::Input::B_RIGHT)) {
		cam->LookRight(lookSpeed * mouseState.GetMouseMoveAmount().x);
		cam->LookUp(lookSpeed * mouseState.GetMouseMoveAmount().y);
	}
	// Movement
	{
		if (keyState.IsKeyDown(keyCode_t('W')))
			cam->MoveForward(dt * moveSpeed);
		if (keyState.IsKeyDown(keyCode_t('S')))
			cam->MoveForward(-dt * moveSpeed);
		if (keyState.IsKeyDown(keyCode_t('A')))
			cam->MoveSideways(dt * moveSpeed);
		if (keyState.IsKeyDown(keyCode_t('D')))
			cam->MoveSideways(-dt * moveSpeed);
		//if (keyState.IsKeyDown(keyCode_t('Q')))
		//	cam->TiltRight(-deltaT * 1.1f);
		//if (keyState.IsKeyDown(keyCode_t('E')))
		//	cam->TiltRight(deltaT * 1.1f);

		if (mouseState.GetMouseScrollAmount().y != 0.f)
			cam->MoveForward( moveSpeed * mouseState.GetMouseScrollAmount().y * 0.1f);
	}

	// Object zoom
	static bool stickyFollow = false;
	bool zDown = keyState.KeyReleased(keyCode_t('Z'));
	if (zDown && keyState.IsKeyDown(keyCode_t('S')))
		stickyFollow = !stickyFollow;
	if (zDown || stickyFollow) {
		auto go = settings.selectedObject;
		CameraFocusObject(cam, go);
	}
}

EditorState::EditorState(Bolts::System::windowHandle_t menuWindow, Bolts::Rendering::Renderer& renderer, Bolts::assets::AssetManager& rs) :
	m_windowHandle(menuWindow), m_renderer(renderer), m_assetManager(rs), m_assetsWindow(rs)
{
	m_debugDrawer = m_renderer.GetDebugDrawer();
}

EditorState::~EditorState()
{
	for (auto& scene : m_scenes)
		scene->OnDestroy();
}

void EditorState::OnRegister()
{
	//auto windowSize = Bolts::System::Win32System::Get().GetWindowSize(m_windowHandle);

	g_debugDrawer = m_debugDrawer;
	OnResume();

	m_GOS.Init();
	RegisterBuiltinComponentManagers(m_GOS, m_assetManager, m_renderer);
	m_scenes.emplace_back(new TycoonScene(m_renderer, m_assetManager, m_GOS));
	m_sceneNames.emplace_back("Tycoon");
	m_scenes.back()->OnRegister();
	m_scenes.emplace_back(new SceneLightmap(m_renderer, m_assetManager, m_GOS));
	m_sceneNames.emplace_back("Lightmap");
	m_scenes.back()->OnRegister();
	m_scenes.emplace_back(new SceneShaderToy(m_renderer, m_assetManager, m_GOS));
	m_sceneNames.emplace_back("Shader Toy");
	m_scenes.back()->OnRegister();
	m_scenes.emplace_back(new GameplaySceneNewComp(m_renderer, m_assetManager, m_GOS));
	m_sceneNames.emplace_back("Gameplay");
	m_scenes.back()->OnRegister();
	m_scenes.emplace_back(new SceneDualContouring(m_renderer, m_assetManager, m_GOS));
	m_sceneNames.emplace_back("DualContouring");
	m_scenes.back()->OnRegister();
	//
	SetActiveScene(0);

	// Editor
	m_editorCamera.onReferenceAdd();
	m_editorSmoothCamera.onReferenceAdd();

	m_editorCamera.SetZFar(300.f);
	InitEditorResources();
	// Load settings
	if (m_settings.Load()) {
		m_editorCamera.SetPosition(m_settings.cameraPos);
		m_editorCamera.SetTarget(m_settings.cameraTgtPos);

		// load last open scene
		if (m_settings.currentOpenScene.empty() ||
			!OpenScene(m_settings.currentOpenScene))
			SpawnDefaultObjects();
	}
	m_editorSmoothCamera.CopyFrom(&m_editorCamera);

	if (!m_settings.fullscreen) {
		System::Win32System::Get().SetWindowSize(m_windowHandle, m_settings.windowSize);
		System::Win32System::Get().SetWindowPos(m_windowHandle, m_settings.windowPos);
	}
	else
		System::Win32System::Get().EnableFullscreen(m_windowHandle, m_settings.windowSize);
	//
	static auto s_assetOpenCB = [this](uint32_t assetType, hash32_t assetID) {
		auto assetPtr = m_assetManager.LoadAssetByType(assetID, assetType);
		m_assetDetailsWindow.SetAsset(assetPtr.get(), m_assetManager.GetPropertyDrawFunction(assetType));
		m_focusAssetWindow = true;
	};
	Bolts::SetAssetOpenCallback([](uint32_t a, hash32_t b) {s_assetOpenCB(a, b); });

	// process Init resources
	//PROFILE_SCOPE("Execute Resource Commands");
	m_renderer.GetBackend()->ExecuteResourceCommands(m_assetManager.GetCommandBuffer());
}

bool EditorState::OnUpdate(double dtSec)
{
	MICROPROFILE_SCOPEI("Editor", "OnUpdate", MP_YELLOW);
	PROFILE_SCOPE("Editor Update");

	float dt = (float)(dtSec);
	m_frameCount++;

	const float aspectRatio = (float)m_windowSize.x / (float)m_windowSize.y;
	GetActiveCamera()->SetAspectRatio(aspectRatio);

	// ImGui dockspace (needs to be first)
	if (false)
	{
		ImGuiWindowFlags flags = ImGuiWindowFlags_NoDocking;
		ImGuiViewport* viewport = ImGui::GetMainViewport();
		ImGui::SetNextWindowPos(viewport->Pos);
		ImGui::SetNextWindowSize(viewport->Size);
		ImGui::SetNextWindowViewport(viewport->ID);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
		flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
		flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus | ImGuiWindowFlags_NoFocusOnAppearing;
		//flags |= ImGuiWindowFlags_NoInputs;

		ImGui::SetNextWindowBgAlpha(0.f);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
		ImGui::Begin("MainWindow", nullptr, flags);
		ImGui::PopStyleVar();

		ImGuiID dockspace_id = ImGui::GetID("MainWindow");
		ImGui::DockSpace(dockspace_id, {}, ImGuiDockNodeFlags_KeepAliveOnly | ImGuiDockNodeFlags_NoSplit);

		ImGui::End();
		ImGui::PopStyleVar(); // padding
	}

	//g_debugDrawer->Ring({ 0.f, 10.f, 0.f }, 10.f, 1.f, {1.0, 0.0,0.0,1.0});

	uvec2 viewportPosition, viewportSize = m_windowSize;
	// Game virtual window test
	if (false) {
		ImGuiWindowFlags window_flags = 0;
		window_flags |= ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoScrollbar;
		window_flags |= ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoBringToFrontOnFocus;

		ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0, 0, 0, 0));
		ImGui::Begin("GameView", nullptr, window_flags);
		viewportPosition = { ImGui::GetWindowPos().x, viewportSize.y - ImGui::GetWindowPos().y - ImGui::GetWindowSize().y };
		viewportSize = { ImGui::GetWindowSize().x, ImGui::GetWindowSize().y };
		ImGui::End();
		ImGui::PopStyleColor();
	}

	// GAMEPLAY
	m_renderer.StartNewFrame();
	//
	{
		PROFILE_SCOPE("Scene");
		m_scene->OnUpdate(dtSec);
		m_activeSpace = m_scene->GetActiveSpace();
	}
	{
		PROFILE_SCOPE("GOS");
		m_GOS.Update(dt, m_frameCount);
	}
	// RENDER
	{
		//TODO: Store all cmd buffers somewhere where the renderer can check they've been submitted
		{
			PROFILE_SCOPE("Execute Resource Commands");
			m_renderer.GetBackend()->ExecuteResourceCommands(m_assetManager.GetCommandBuffer());
		}

		PROFILE_SCOPE("Main Render");
		m_renderer.UpdateCamera(GetActiveCamera());
		m_renderer.Render(m_assetManager, viewportSize, viewportPosition);
		m_renderer.EndFrame();

	}
	// END GAMEPLAY

	// TOOLS
	if (m_showEditorMenu)
		m_renderer.TweakSettings();

	// Camera control
	using namespace Bolts::Input;
	CamControl camControlSettings;
	camControlSettings.selectedObject = GetSelectedObject();
	ControlCamera( &m_editorCamera, dt, camControlSettings, m_keyState, m_mouseState);
	m_editorSmoothCamera.BlendFrom(&m_editorCamera, 0.2f);
	//
	{
		if (m_keyState.IsKeyDown(keyCode_t('D')) && m_keyState.IsKeyDown(KC_CTRL))
			SetSelectedObject(nullptr);
	}
	
	if (m_showEditorMenu)
	{
		//UI
		//ImGui::ShowStyleEditor();
		if (ImGui::Begin("Camera Settings", nullptr, ImGuiWindowFlags_AlwaysAutoResize)) {
			auto camPtr = GetActiveCamera();
			float near = camPtr->GetZNear();
			float far = camPtr->GetZFar();
			ImGui::Checkbox("Use Editor Camera", &m_usingEditorCamera);
			ImGui::SliderFloat("Near", &near, 0.f, 10.f);
			ImGui::SliderFloat("Far", &far, 1.f, 200.f);
			camPtr->SetZNear(near);
			camPtr->SetZFar(far);
		}
		ImGui::End();

		if (ImGui::Begin("Editor"))
		{
			if (ImGui::BeginMenu("Scenes"))
			{
				for (unsigned i = 0; i < m_scenes.size(); i++) {
					if (ImGui::MenuItem(m_sceneNames[i].c_str()))
						SetActiveScene(i);
				}
				ImGui::EndMenu();
			}
		}
		ImGui::End();

		//if (false) {
		//	// Editor Workspace
		//	ImGuiWindowFlags window_flags = 0;
		//	window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse;
		//	window_flags |= ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoBringToFrontOnFocus;
		//	window_flags |= ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollbar;
		//	window_flags |= m_showEditorMenu ? ImGuiWindowFlags_MenuBar : 0;
		//	ImGui::SetNextWindowPos(ImVec2(0.f, 0.f));
		//	ImGui::SetNextWindowSize(ImVec2(m_windowSize.x, m_windowSize.y));
		//
		//	ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0, 0, 0, 0));
		//	ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
		//	bool bla = true;
		//	ImGui::Begin("Editor", &bla, window_flags);
		//	ImGui::End(); // Editor
		//	ImGui::PopStyleVar();
		//	ImGui::PopStyleColor();
		//
		//	//auto dockId = ImGui::GetID("EditorWorkspace");
		//	//ImGui::DockSpace(dockId, ImVec2( wsize.x / 4.f, wsize.y ));
		//}

		// Gizmos virtual window
		if (true) {
			ImGuiWindowFlags window_flags = 0;
			window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoDocking;
			window_flags |= ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar;
			window_flags |= ImGuiWindowFlags_NoInputs | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoBringToFrontOnFocus;

			ImGui::SetNextWindowSize({ (float)m_windowSize.x, (float)m_windowSize.y });
			ImGui::SetNextWindowPos(ImVec2(0.f, 0.f));
			ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0, 0, 0, 0));
			ImGui::Begin("Gizmos", nullptr, window_flags);
			ImGui::End();
			ImGui::PopStyleColor();
		}

		// Viewport floating toolbar test
		if (true) {
			ImGuiWindowFlags window_flags = 0;
			window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoDocking;
			window_flags |= ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar;
			window_flags |= ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoBringToFrontOnFocus;
			
			ImGui::SetNextWindowPos(ImVec2(m_windowSize.x *0.5f, 20.f));
			ImGui::SetNextWindowBgAlpha(0.2f);
			ImGui::Begin("ViewportWidgets", nullptr, window_flags);
				ImGui::AlignTextToFramePadding();

				if (!m_playMode) 
					ImGui::Text(ICON_KI_FORWARD " Editor");
				else
					ImGui::Text(ICON_KI_PAUSE " Game");
				if (ImGui::IsItemClicked()) {
					m_playMode = !m_playMode;
				}
				ImGui::SameLine();

				ImGui::Text(ICON_KI_SEARCH_PLUS);
				if (ImGui::IsItemClicked())
					CameraFocusObject( &m_editorCamera, GetSelectedObject());

				ImGui::SameLine();
				ImGui::Checkbox("Editor Camera", &m_usingEditorCamera);
			ImGui::End();
		}

		// Level object list
		SceneTreeUI();

		// Property Window
		ObjectPropertiesUI();

		// Assets
		AssetsUI();

		m_renderingWindow.Update();

		// Profiling
		if (ImGui::Begin("Profiling")) {
			Bolts::Profiler::Get().DrawCPUProfilingInfo();
		}
		ImGui::End();

		//TODO: This should only be enabled in "Play" mode
		// Update Properties from component values
		if (GetSelectedObject() && !ImGui::IsAnyItemFocused()) {
			goRef_t goref = GetSelectedObject();
			ResyncObjectProperties(goref);
		}
	}

	if (!m_playMode)
	{
		// Widgets
		if (m_keyState.IsKeyDown(keyCode_t('E')))
			selectionGizmo.currentGizmoOperation = ImGuizmo::TRANSLATE;
		if (m_keyState.IsKeyDown(keyCode_t('R')))
			selectionGizmo.currentGizmoOperation = ImGuizmo::ROTATE;
		if (m_keyState.IsKeyDown(keyCode_t('T')))
			selectionGizmo.currentGizmoOperation = ImGuizmo::SCALE;
		if (m_keyState.IsKeyDown(keyCode_t('Q')))
			selectionGizmo.currentGizmoOperation = ImGuizmo::OPERATION(-1);
		selectionGizmo.useSnap = ImGui::IsKeyDown(KC_CTRL);

		if (ImGui::Begin("Toolbox", nullptr, 0)) {
			ImGui::BeginChild("ToolboxContent", ImVec2(0, -ImGui::GetFrameHeightWithSpacing()), false);
			if (GetSelectedObject()) {
				selectionGizmo.RenderToolbar();
			}
			ImGui::EndChild();
			ImGui::Separator();
			//Satusbar content here
			auto cam = GetActiveCamera();
			ImGui::Text("Camera: %.3f %.3f %.3f", cam->GetPosition().x, cam->GetPosition().y, cam->GetPosition().z);
		}
		ImGui::End();

		// Transform gizmo
		if (GetSelectedObject()) {
			ImGui::Begin("Gizmos");
			selectionGizmo.RenderApplyGizmo(GetActiveCamera(), GetSelectedObject()->EditTransform());
			ImGui::End();
		}

		// Draw	selection box
		const Bolts::vec4 c_selColor{ 0.f, 1.f, 0.f,0.15f };
		auto go = GetSelectedObject();
		AABB box;
		GetObjectLocalAABB(box, go);
		if (box.ext() != vec3_zero) {
			// tight mesh box bounds
			vec3 obbPoints[8];
			GetOBBVerts(obbPoints, go->GetTransform().GetAbsTransform(), box);
			m_debugDrawer->OBB(obbPoints, c_selColor, DebugDrawStyle(BDDS_TRANSP | BDDS_WIREFRAME));
			// wireframe of world-space AABB
			AABB aabbWorld;
			GetPointsAABB(aabbWorld, obbPoints, ::countof(obbPoints));
			vec3 aabbPoints[8];
			GetAABBVerts(aabbPoints, aabbWorld);
			m_debugDrawer->OBB(aabbPoints, { 1.f, 0.f, 0.f, 0.3f }, DebugDrawStyle(BDDS_WIREFRAME));
		}

		// Editor keyboard shortcuts
		if (m_keyState.IsKeyDown(KC_CTRL) && m_keyState.KeyReleased(keyCode_t('S'))) {
			SaveScene();
		}
	}
	//
	m_keyState.EventsHandled(); 
	m_mouseState.EventsHandled();
	//
	return m_isRunning;
}

void EditorState::SceneTreeUI()
{
	if (!m_activeSpace || m_activeSpace->GetActiveObjects().empty())
		return;
	if (ImGui::Begin("Game Objects", nullptr, 0)) {

		if (ImGui::Button(ICON_KI_SAVE " Save Scene")) {
			SaveScene();
		}

		ImGui::SameLine();
		if (ImGui::Button("Load Scene")) {
			auto filepath = Bolts::System::OpenFileDialog(m_settings.currentOpenScene.c_str(), "bobj", "Bolts Object");
			if (filepath.size() > 0) {
				m_settings.currentOpenScene = filepath;
				OpenScene(filepath);
			}
		}

		if (ImGui::Button("Spawn Object"))
		{
			auto filepath = Bolts::System::OpenFileDialog(m_settings.lastOpenObj.c_str(), "bobj", "Bolts Object");
			if (filepath.size() > 0) {
				if (SpawnObjectFromFile(filepath))
					m_settings.lastOpenObj = filepath;
			}
		}

		ImGui::SameLine();
		if (ImGui::Button("Spawn Prefab"))
		{
			auto filepath = Bolts::System::OpenFileDialog(m_settings.lastOpenPrefab.c_str(), "bobj", "Bolts Object");
			if (filepath.size() > 0) {
				if (SpawnSceneFromFile(filepath))
					m_settings.lastOpenPrefab = filepath;
			}
		}

		ImGui::SameLine();
		if (ImGui::Button("Save As Prefab")) {
			auto filepath = Bolts::System::SaveFileDialog(m_settings.lastSavePrefab.c_str(), "bobj", "Bolts Object");
			if (filepath.size() > 0) {
				m_settings.lastSavePrefab = filepath;
				SaveSceneToFile(filepath);
			}
		}
		//
		unsigned objIdx = 0;
		char objLabel[256];
		static vec_t<goRef_t> objStack;
		objStack.clear();

		objStack.push_back(m_activeSpace->GetRoot());

		while (!objStack.empty()) {
			auto go = objStack.back();
			objStack.pop_back();
			if (!go) {
				ImGui::TreePop();
				continue;
			}
			// object data
			auto& children = go->GetChildren();
			const bool noChildren = children.empty();
			sprintf(objLabel, "Obj%d", objIdx);
			const bool objSelected = m_selectedObj == go;

			const int nodeFlags =
				ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | ImGuiTreeNodeFlags_DefaultOpen |
				(objSelected ? ImGuiTreeNodeFlags_Selected : 0) |
				(noChildren ? ImGuiTreeNodeFlags_Leaf : 0);
			// object node
			if (ImGui::TreeNodeEx(objLabel, nodeFlags))
			{
				// drag & drop
				if (ImGui::BeginDragDropSource())
				{
					ImGui::SetDragDropPayload("game_object", &objIdx, sizeof(objIdx));
					ImGui::Text(objLabel);
					ImGui::EndDragDropSource();
				}
				if (ImGui::BeginDragDropTarget())
				{
					if (const ImGuiPayload* pl = ImGui::AcceptDragDropPayload("game_object")) {
						unsigned droppedObjIdx = *(unsigned*)pl->Data;
						auto& newChild = m_activeSpace->GetActiveObjects()[droppedObjIdx];
						go->AddChild(*newChild);
					}
					ImGui::EndDragDropTarget();
				}
				// selection
				if (ImGui::IsItemClicked()) {
					SetSelectedObject(go);
				}
				// push a null so we treepop at the right moment
				objStack.push_back(nullptr);
				// continue with children, in order
				objStack.insert(objStack.end(), children.rbegin(), children.rend());
			}
			objIdx++;
		}
	}
	ImGui::End();
}

void EditorState::ObjectPropertiesUI()
{
	if (!GetSelectedObject())
		return;
	componentTag_t componentToAdd = badHandle;
	componentTag_t componentToRemove = badHandle;
	if (ImGui::Begin("Properties", nullptr, 0)) {
		goRef_t goref = GetSelectedObject();
		Bolts::BinaryMapBuilder builder;
		//TODO: HOWTO Align to right
		if (ImGui::Button("SAVE")) {
			auto filepath = Bolts::System::SaveFileDialog(m_settings.lastSaveObj.c_str(), "bobj", "Bolts Object");
			if (filepath.size()) {
				m_settings.lastSaveObj = filepath;
				SaveObjectToFile(filepath, goref);
			}
		}
		ImGui::SameLine();
		if (ImGui::Button("DELETE") || m_keyState.KeyReleased(Input::KC_DELETE)) {
			m_GOS.RemoveGO(m_activeSpace, goref);
		}
		// Add component menu
		ImGui::SameLine();
		if (ImGui::Button("+"))
			ImGui::OpenPopup("AddComponent");
		//ImGui::BeginPopupModal
		if (ImGui::BeginPopup("AddComponent", ImGuiWindowFlags_AlwaysAutoResize)) {
			ImGui::Text("Change components");
			ImGui::Separator();
			for (componentTag_t cTag : m_GOS.GetComponentModels().GetTags()) {
				// skip components we already have
				if (cTag == GetObjectTag())
					continue;
				ImGui::PushID(cTag);
				bool isAdded = m_selectedObj->TryGetComponentRef(cTag).IsValid();
				if (!isAdded && ImGui::Button("+"))
					componentToAdd = cTag;
				if (isAdded && ImGui::Button("-"))
					componentToRemove = cTag;
				ImGui::SameLine();
				const char* compName = m_GOS.FindComponentManagerForTag(cTag)->GetComponentName({});
				ImGui::Text(compName);
				ImGui::PopID();
			}
			//ImGui::CloseCurrentPopup();
			ImGui::EndPopup();
		}
		// Component Properties
		// Transform
		if (ImGui::CollapsingHeader("Transform")) {
			// Props
			propVec_t* selectedProps = m_GOS.EditObjectProperties(goref).EditProps(GetObjectTag());
			bool wasChanged = false;
			for (auto& prop : *selectedProps) {
				wasChanged |= prop->Draw();
			}
			if (wasChanged) {
				PropVecToBinary(*selectedProps, builder);
				goref->Reload(builder.GetTempMap());
				builder.Clear();
			}
		}
		// Components
		auto& comps = goref->GetComponents();
		for (auto compPair : comps) {
			auto compRef = compPair.ref;
			// Label
			if (ImGui::CollapsingHeader(compRef.TryGetMgr()->GetComponentName(compRef), ImGuiTreeNodeFlags_DefaultOpen)) {
				// Props
				propVec_t* selectedProps = m_GOS.EditObjectProperties(goref).EditProps(compPair.tag);
				BASSERT(selectedProps);
				bool wasChanged = false;
				for (auto& prop : *selectedProps) {
					wasChanged = wasChanged | prop->Draw();
				}
				if (wasChanged) {
					PropVecToBinary(*selectedProps, builder);
					compRef.TryGetMgr()->ReloadComponent(compRef, builder.GetTempMap());
					builder.Clear();
				}
			}
		}
	}
	ImGui::End();
	//
	if (componentToAdd != badHandle) {
		m_GOS.AddComponent(m_activeSpace, m_selectedObj, componentToAdd);
		m_GOS.Start(m_activeSpace);
	}
	if (componentToRemove != badHandle) {
		m_GOS.RemoveComponent(m_activeSpace, m_selectedObj, componentToRemove);
		m_GOS.Start(m_activeSpace);
	}
}

void EditorState::AssetsUI()
{
	if (m_focusAssetWindow) {
		ImGui::SetNextWindowFocus();
		m_focusAssetWindow = false;
	}
	if (ImGui::Begin("Asset")) {
		m_assetDetailsWindow.DrawContent();
	}
	ImGui::End();
	if (ImGui::Begin("Assets")) {
		m_assetsWindow.DrawContent();
	}
	ImGui::End();
}

void EditorState::SetActiveScene(int idx)
{
	if (idx == m_activeScene)
		return;

	m_selectedObj = nullptr;
	m_selectedCompIdx = -1;

	if (m_scene)
		m_scene->OnPause();
	m_scene = m_scenes[idx].get();
	m_scene->OnResume();
	m_activeScene = idx;
	m_activeSpace = m_scene->GetActiveSpace();
}

void EditorState::OnResume()
{
	using namespace Bolts;

	System::Win32System::Get().RegisterKeyEventListener(this);
	System::Win32System::Get().RegisterMouseEventListener(this);
	System::Win32System::Get().RegisterWindowEventListener(this);
	m_isPaused = false;
}


void EditorState::OnPause()
{
	using namespace Bolts;

	System::Win32System::Get().UnregisterKeyEventListener(this);
	System::Win32System::Get().UnregisterMouseEventListener(this);
	System::Win32System::Get().UnregisterWindowEventListener(this);
	m_isPaused = true;
}

void EditorState::OnDestroy()
{
	OnPause();
	if (System::Win32System::Get().IsWindowOpen(m_windowHandle)) {
		m_settings.windowSize = System::Win32System::Get().GetWindowSize(m_windowHandle);
		m_settings.windowPos = System::Win32System::Get().GetWindowPos(m_windowHandle);
	}
	m_settings.cameraPos = m_editorCamera.GetPosition();
	m_settings.cameraTgtPos = m_editorCamera.GetTarget();
	m_settings.Save();

	if (m_boundingBoxGeom) m_boundingBoxGeom->Release(*m_assetManager.GetCommandBuffer());
	if (m_arrowGeom) m_arrowGeom->Release(*m_assetManager.GetCommandBuffer());
}

//////////////

using namespace Bolts::Input;

bool EditorState::OnKeyEvent(keyEvent_t event)
{
	using namespace Bolts;

	switch (event.state) {
	case Input::KS_PRESSED:
	{
		switch (event.keyCode) {
		case Input::KC_F1:
			m_showEditorMenu = !m_showEditorMenu;
			break;
		case Input::KC_F5:
			m_playMode = !m_playMode;
			break;
		case Input::KC_ESC:
			m_isRunning = false;
			break;
		}
		break;
	}
	case Input::KS_RELEASED:
	{
		switch (event.keyCode) {
		case Input::KC_F1:
			break;
		}
		break;
	}

	}

	if (m_playMode)
		m_scene->OnKeyEvent(event);
	else
		m_keyState.OnKeyEvent(event);
	return false;
}

bool EditorState::OnMouseEvent(Bolts::Input::mouseEvent_t event)
{
	if (ImGui::IsAnyItemHovered())
		return false;

	if (m_playMode) {
		m_scene->OnMouseEvent(event);
		return false;
	}
	else
		m_mouseState.OnMouseEvent(event);

	if (event.state == MS_RELEASED && event.button == B_LEFT)
	{
		// select object under mouse
		auto cam = GetActiveCamera();
		Ray r = Bolts::MakeRay(cam->ProjectScreenCoords( m_mouseState.GetMousePosition()));
		goRef_t hits[64];
		int numHits = GetRayHitObjects(r, hits, 64);
		if (numHits != 0) {
			if (numHits != m_clickIntersections.size()) {
				SetSelectedObject(hits[0]);
				m_clickIntersections.clear();
				for (int i = 0; i < numHits; i++)
					m_clickIntersections.push_back(hits[i]);
			}
			else {
				int selectionIdx = -1;
				for (int i = 0; i < numHits; i++)
					if (hits[i] == m_selectedObj)
						selectionIdx = i + 1;
				if (selectionIdx == -1 || selectionIdx == numHits)
					selectionIdx = 0;

				SetSelectedObject(hits[selectionIdx]);
			}
		}
	}

	return false;
}

void EditorState::OnWindowEvent(Bolts::Input::windowEvent_t event)
{
	if (event.type == WE_RESIZED)
	{
		m_windowSize = { (unsigned)event.value1, (unsigned)event.value2 };
		m_settings.windowSize = m_windowSize;
	}
}

////////////////////////

Bolts::Camera* EditorState::GetActiveCamera()
{
	//TODO: This should be grabbing the camera used for rendering, from the renderer
	return m_usingEditorCamera && !m_playMode ? &m_editorSmoothCamera : m_scene->GetActiveCamera().get();
}

void EditorState::InitEditorResources()
{
	Rendering::RCB& commands = *m_assetManager.GetCommandBuffer();

	m_boundingBoxGeom = Bolts::MeshBuilder::CubeIndexed(commands, Bolts::vec3(1.f));
	m_arrowGeom = Bolts::MeshBuilder::CylinderIndexed(commands, { 1.f, 0.f, 0.f }, { 0.f, 1.f, 0.f }, 1.f, 10.f, 32, 5);

	m_widgetMat = m_assetManager.LoadAsset<Bolts::Rendering::Material>("WidgetSolid");
	m_boundingBoxMat = m_widgetMat;
}

goRef_t	EditorState::GetSelectedObject()
{
	return m_selectedObj;
}

void EditorState::SetSelectedObject(goRef_t ref)
{
	m_selectedObj = ref;
}

void EditorState::GetObjectLocalAABB(Bolts::AABB& out, goRef_t ref)
{
	if (ref) {
		auto meshCmp = ref->TryGetComponent<CMgrMesh>();
		if (meshCmp && meshCmp->GetMesh()) {
			auto& meshAABB = meshCmp->GetMesh()->aabb;
			out.min = meshAABB.min;
			out.max = meshAABB.max;
		}
	}
}

int EditorState::GetRayHitObjects(Bolts::Ray r, goRef_t* out, int outSize)
{
	int hits = 0;
	for (auto& objPtr : m_activeSpace->GetActiveObjects()) {
		AABB aabb;
		GetObjectLocalAABB(aabb, *objPtr);
		vec3 ext = aabb.ext();
		if (ext == vec3_zero)
			continue;
		// convert local AABB to world and realign
		vec3 obbPoints[8];
		GetOBBVerts(obbPoints, objPtr->GetTransform().GetAbsTransform(), aabb);
		AABB aabbWorld;
		GetPointsAABB(aabbWorld, obbPoints, ::countof(obbPoints));
		if (Bolts::RayAABB(r, aabbWorld)) {
			out[hits++] = *objPtr;
			if (hits == outSize)
				break;
		}
	}
	return hits;
}

void EditorState::ResyncObjectProperties(goRef_t goRef)
{
	// Transform & game object
	BASSERT(m_GOS.EditObjectProperties(goRef).EditProps(GetObjectTag()));
	auto& transformProps = *m_GOS.EditObjectProperties(goRef).EditProps(GetObjectTag());
	auto bindings = m_GOS.GetPropBindings(GetObjectTag());
	bindings->RefreshProperties(transformProps, &goRef->GetTransform());
	// Components
	for (auto& compPair : goRef->GetComponents()) {
		componentTag_t compTag = compPair.tag;
		auto& compProps = *m_GOS.EditObjectProperties(goRef).EditProps(compTag);
		auto bindings = m_GOS.GetPropBindings(compTag);
		auto compRef = compPair.ref;
		bindings->RefreshProperties(compProps, compRef.GetUnsafePtr());
	}
}

void EditorState::SpawnDefaultObjects()
{
	//Spawn objects here
	GOPrototype meshObj;
	meshObj.m_components.push_back(MeshComponent::Tag);

	goRef_t go;
	// Spawn some objects
	go = m_GOS.SpawnGO(m_activeSpace, meshObj);
	//auto meshPtr = m_MeshManager.GetMesh( Bolts::HashCString32( "mycitadel_stripped.dae" ) );
	go->TryGetComponent<CMgrMesh>()->SetMesh("mitsuba.mesh");
	go->TryGetComponent<CMgrMesh>()->SetMaterial(0, "Phong#shadowed");
	//m_GameObjects.back()->EditTransform().SetPosition({ 0.f,100.f,0.f });
	//m_GameObjects.back()->EditTransform().SetScale( vec3(100.f));

	go = m_GOS.SpawnGO(m_activeSpace, meshObj);
	go->TryGetComponent<CMgrMesh>()->SetMesh("house_obj.mesh");
	go->TryGetComponent<CMgrMesh>()->SetMaterial(0, "TestHouse");
	go->EditTransform().SetScale(vec3(0.01f));
	go->EditTransform().SetPosition({ 30.f,0.f,0.f });

	go = m_GOS.SpawnGO(m_activeSpace, meshObj);
	go->TryGetComponent<CMgrMesh>()->SetMesh("HarmonicsBall");
	go->TryGetComponent<CMgrMesh>()->SetMaterial(0, "ShowCubemap");
	go->EditTransform().SetPosition({ 0.f,30.f,0.f });
	
	go = m_GOS.SpawnGO(m_activeSpace, meshObj);
	go->TryGetComponent<CMgrMesh>()->SetMesh("sky.mesh");
	go->TryGetComponent<CMgrMesh>()->SetMaterial(0, "PreethamSky");
	
	GOPrototype directionalLightObj;
	directionalLightObj.m_components.push_back(DirectionalLightComponent::Tag);
	go = m_GOS.SpawnGO(m_activeSpace, directionalLightObj);
	go->EditTransform().SetRotation({ 0.920825f, 0.341906f, 0.175837f, 0.065289f });
	go->TryGetComponent<CMgrLight>()->SetLightColor({ 0.3f,0.3f,0.3f });

	m_GOS.Start(m_activeSpace);
}

void EditorState::SaveScene()
{
	BOLTS_LINFO() << "[Editor] " << "Saving scene to: " << m_settings.currentOpenScene;
	if (m_settings.currentOpenScene.empty())
		m_settings.currentOpenScene = Bolts::System::SaveFileDialog(m_settings.currentOpenScene.c_str(), "bobj", "Bolts Object");
	if (m_settings.currentOpenScene.size() > 0) {
		SaveSceneToFile(m_settings.currentOpenScene);
	}
	BOLTS_LINFO() << "[Editor] " << "Save successful.";
}

bool EditorState::OpenScene(const str_t & filepath)
{
	//TODO: Unload all
	return SpawnSceneFromFile(filepath);
}

bool EditorState::SpawnObjectFromFile(const str_t& filepath)
{
	MICROPROFILE_SCOPEI("Gameplay", "LoadObjectFromXML", MP_AQUAMARINE3);
	BASSERT(m_activeSpace);
	BASSERT(filepath.size());
	editor::xmlDoc_t& doc = editor::GetEmptyXMLDoc();
	editor::LoadXMLFile(filepath.c_str(), doc);
	auto newGO = editor::LoadObjectFromXML(m_GOS, m_activeSpace, doc);
	if (!newGO)
		return false;
	if (auto selectedGO = GetSelectedObject())
		selectedGO->AddChild(newGO);
	m_GOS.Start(m_activeSpace);
	return true;
}

bool EditorState::SpawnSceneFromFile(const str_t& filepath)
{
	MICROPROFILE_SCOPEI("Gameplay", "LoadSceneFromXML", MP_AQUAMARINE3);
	BASSERT(m_activeSpace);
	BASSERT(filepath.size());
	editor::xmlDoc_t& doc = editor::GetEmptyXMLDoc();
	if (!editor::LoadXMLFile(filepath.c_str(), doc) || !editor::LoadSceneFromXML(m_GOS, m_activeSpace, doc))
		return false;
	m_GOS.Start(m_activeSpace);
	return true;
}

void EditorState::SaveObjectToFile(const str_t & filepath, goRef_t obj)
{
	MICROPROFILE_SCOPEI("Gameplay", "SaveObjToFile", MP_AQUAMARINE4);
	BASSERT(filepath.size());
	ResyncObjectProperties(obj);
	editor::xmlDoc_t& doc = editor::GetEmptyXMLDoc();
	editor::SaveObjectToXML(doc, m_GOS, obj);
	BASSERT_ONLY(bool savedOK = ) editor::SaveXMLFile(filepath.c_str(), doc);
	BASSERT(savedOK);
}

void EditorState::SaveSceneToFile(const str_t& filepath)
{
	MICROPROFILE_SCOPEI("Gameplay", "SaveSceneToFile", MP_AQUAMARINE4);
	BASSERT(m_activeSpace);
	BASSERT(filepath.size());
	// Resync props so code-driven values get saved
	for (auto& goPtr : m_activeSpace->GetActiveObjects())
		ResyncObjectProperties(*goPtr);
	//
	editor::xmlDoc_t& doc = editor::GetEmptyXMLDoc();
	editor::SaveSceneToXML(doc, m_GOS, m_activeSpace);
	BASSERT_ONLY(bool savedOK = ) editor::SaveXMLFile(filepath.c_str(), doc);
	BASSERT(savedOK);
}

/*
void EditorState::UpdateWidgets(double dt)
{
	using namespace Bolts;
	dt;
	auto activeCam = GetActiveCamera();
	auto ray = activeCam->ProjectScreenCoords(m_mouseState.GetMousePosition());
	m_hoveredAxis = -1;
	if (auto go = GetSelectedObject()) {
		const vec3 wstart = go->GetTransform().GetPosition();
		//Work out scale to keep it same size on screen
		const float distToCam = glm::length(activeCam->GetPosition() - wstart);
		const float widgetScale = distToCam * tan(activeCam->GetFov() / 2) * 0.05f * 10.f; // 10.f is the size of the widget mesh
		const float k_lineThickness = 0.1f * widgetScale;
		//
		vec3 wendX = vec3(widgetScale, 0.f, 0.f) * go->GetTransform().GetRotation();
		vec3 wendY = vec3(0.f, widgetScale, 0.f) * go->GetTransform().GetRotation();
		vec3 wendZ = vec3(0.f, 0.f, widgetScale) * go->GetTransform().GetRotation();
		double ta{}, tb{};
		vec3 p1, p2;
		if (LineLineIntersect(ray.start, ray.end, wstart, wendX, &p1, &p2, &ta, &tb)
			&& (glm::length(p1 - p2) < k_lineThickness) && tb > 0.f && tb < 1.f && ta > 0.f)
		{
			m_hoveredAxis = 0;
		}
		else if (LineLineIntersect(ray.start, ray.end, wstart, wendY, &p1, &p2, &ta, &tb)
			&& (glm::length(p1 - p2) < k_lineThickness) && tb > 0.f && tb < 1.f && ta > 0.f)
		{
			m_hoveredAxis = 1;
		}
		else if (LineLineIntersect(ray.start, ray.end, wstart, wendZ, &p1, &p2, &ta, &tb)
			&& (glm::length(p1 - p2) < k_lineThickness) && tb > 0.f && tb < 1.f && ta > 0.f)
		{
			m_hoveredAxis = 2;
		}

	}
}
*/

void EditorState::RenderBBox(const Bolts::mat4& worldT, Bolts::vec3 color)
{
	using namespace Bolts;
	if (m_boundingBoxMat && m_boundingBoxMat) {
		auto activeCam = GetActiveCamera();

		auto& rendererBuiltins = m_renderer.GetBuiltins();
		rendererBuiltins.Set("u_w", worldT);
		rendererBuiltins.Set("u_v", activeCam->GetViewMatrix());
		rendererBuiltins.Set("u_p", activeCam->GetProjectionMatrix());
		m_boundingBoxMat->EditParams().Set("u_color", color);

		m_renderer.EnableMaterial(*m_boundingBoxMat);
		m_renderer.Draw(*m_boundingBoxGeom);
	}
}