#pragma once

#include <bolts_core.h>
#include <Input/IOEvents.h>
#include <Helpers/Camera.h>
#include <memory>
#include <vector>

namespace Bolts
{
	class Space;
};

class IGameScene
{
public:
	virtual ~IGameScene() {};

	virtual bool	OnUpdate(double dt) = 0;//Return false to pop state
	//
	virtual void	OnRegister() = 0;
	virtual void	OnResume() = 0;
	virtual void	OnPause() = 0;
	virtual void	OnDestroy() = 0;
	//
	virtual void OnKeyEvent(Bolts::Input::keyEvent_t ev) = 0;
	virtual void OnMouseEvent(Bolts::Input::mouseEvent_t ev) = 0;
	//
	virtual Bolts::CameraPtr GetActiveCamera() = 0;
	virtual Bolts::Space* GetActiveSpace() = 0;
};

