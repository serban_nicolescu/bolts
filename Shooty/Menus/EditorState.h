#pragma once
#include "Menus/GameScene.h"
#include "Components/Space.h"
#include "Components/GOS.h"
#include "Render/Renderer.h"
#include <System/system_win32.h>
#include <Helpers/Camera.h>
#include <Input/IOEvents.h>
#include "Editor/AssetDetailsWindow.h"
#include "Editor/AssetsWindow.h"
#include "Editor/RenderingWindow.h"

using namespace Bolts;

namespace Bolts {
	class DebugDrawer;
}

class EditorState : public Input::iKeyEventListener, Input::iMouseEventListener, Input::iWindowEventListener
{
public:
	EditorState(System::windowHandle_t menuWindow, Rendering::Renderer& renderer, AssetManager& rs);
	~EditorState();

	bool OnUpdate(double dt);

	void OnRegister();
	void OnResume();
	void OnPause();
	void OnDestroy();

	bool OnKeyEvent(Input::keyEvent_t ev) override;
	bool OnMouseEvent(Input::mouseEvent_t ev) override;
	void OnWindowEvent(Input::windowEvent_t ev) override;
private:
	bool		m_isPaused = false;
	bool		m_isRunning = true;

	unsigned	m_frameCount = 0;

	System::windowHandle_t	m_windowHandle;
	Rendering::Renderer&	m_renderer;
	AssetManager&			m_assetManager;
	Input::KeyStateHolder	m_keyState;
	Input::MouseStateHolder	m_mouseState;
	GOS						m_GOS;
	Space*					m_activeSpace = {};
	DebugDrawer*			m_debugDrawer{};

	IGameScene*				m_scene = nullptr;

	// Editor
	Camera*			GetActiveCamera();
	void			InitEditorResources();
	goRef_t			GetSelectedObject();
	void			SetSelectedObject(goRef_t);

	void			SpawnDefaultObjects();
	bool			OpenScene(const str_t& filepath); // Unloads everything and loads a different scene
	void			SaveScene();

	void			SceneTreeUI();
	void			ObjectPropertiesUI();
	void			AssetsUI();
	// Viewport
	Bolts::vec3		GetObjectLocalExtents(goRef_t);
	void			GetObjectLocalAABB(Bolts::AABB& out, goRef_t ref);
	int				GetRayHitObjects(Bolts::Ray r, goRef_t* out, int outSize);

	//	Saving and serialisation
	void			ResyncObjectProperties(goRef_t); //> Sets object prop values from object state

	bool			SpawnObjectFromFile(const str_t& filepath);
	bool			SpawnSceneFromFile(const str_t& filepath);
	void			SaveObjectToFile(const str_t& filepath, goRef_t obj);
	void			SaveSceneToFile(const str_t& filepath);
	// 
	void			SetActiveScene(int);
	//
	void			RenderBBox(const mat4& worldT, vec3 color);

	// settings
	struct Settings
	{
		bool Load();
		bool Save();

		uvec2	windowSize = {600,600};
		uvec2	windowPos = { 0,0 };
		bool	fullscreen = false;

		vec3	cameraPos;
		vec3	cameraTgtPos;

		str_t	currentOpenScene;
		str_t	lastSaveObj;
		str_t	lastSavePrefab;
		str_t	lastOpenObj;
		str_t	lastOpenPrefab;

	};
	Settings	m_settings;
	uvec2		m_windowSize;
	// Scene View
	Rendering::PrimitiveBufferPtr	m_boundingBoxGeom;
	Rendering::PrimitiveBufferPtr	m_arrowGeom;
	Rendering::MaterialPtr			m_boundingBoxMat;
	Rendering::MaterialPtr			m_widgetMat;
	bool			m_playMode = false;
	bool			m_usingEditorCamera = true;
	Camera			m_editorCamera;
	Camera			m_editorSmoothCamera;
	//
	vec_t<goRef_t>	m_clickIntersections;
	goRef_t			m_selectedObj;
	int				m_selectedCompIdx = -1;
	// Windows
	Editor::AssetDetailsWindow	m_assetDetailsWindow;
	Editor::AssetsWindow		m_assetsWindow;
	bool						m_focusAssetWindow = false;
	RenderingWindow				m_renderingWindow;

	// Scene Mgmt
	typedef vec_t< std::unique_ptr<IGameScene>> sceneVec_t;
	int				m_activeScene = -1;
	sceneVec_t		m_scenes;
	vec_t< str_t>	m_sceneNames;
	// Menu stuff
	bool	m_showEditorMenu = true;
};