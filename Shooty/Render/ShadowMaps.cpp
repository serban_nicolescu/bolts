#include "StdAfx.h"
#include "DebugDraw.h"
#include "Renderer.h"
#include "ShadowMaps.h"
#include "Assets/AssetManager.h"

#include "Rendering/GPUProgram.h"
#include "Rendering/Texture.h"
#include "Rendering/MaterialNew.h"

#include "imgui.h"

namespace Bolts {
	namespace Rendering {

		ShadowMapsFeature::ShadowMapsFeature()
		{
			shadowCam.SetAspectRatio(1.f);
			shadowCam.SetCameraOrtho();
		}

		//TODO: Cleanup resources
		ShadowMapsFeature::~ShadowMapsFeature() = default;

		void ShadowMapsFeature::InitBuiltinResources(Bolts::assets::AssetManager& am)
		{
			const uvec3 texDim = uvec3(settings.resolution, settings.resolution, 1);
			auto& rcb = *am.GetCommandBuffer();
			{
				//shadowMapTexture = am.GetCommandBuffer()->CreateResource(RenderResourceType::IMAGE_BUFFER);
				//auto updateTexCommand = am.GetCommandBuffer()->AddCommand<command::UpdateTextureData>(shadowMapTexture);
				//updateTexCommand->type = BTT_2D;
				//updateTexCommand->format = settings.format;
				//updateTexCommand->dimensions = texDim;
				//
				//auto updFilterCmd = am.GetCommandBuffer()->AddCommand<command::UpdateTextureFiltering>(shadowMapTexture);
				//updFilterCmd->newSettings.filteringType = TextureFilterType::BTFT_NEAREST;

				TextureSamplerSettings newSettings{};
				newSettings.filteringType = TextureFilterType::BTFT_NEAREST;
				regHash_t texName{ "DirectionalShadowDepth" };
				shadowMapTexture = new Texture(texName);
				shadowMapTexture->Init(rcb);
				shadowMapTexture->SetRTTexture(rcb, BTT_2D, texDim, settings.format);
				shadowMapTexture->SetSampler(rcb, newSettings);
				BASSERT(shadowMapTexture->GetAssetState() == assets::EAS_READY);
			}
			{
				TextureSamplerSettings newSettings{};
				newSettings.filteringType = TextureFilterType::BTFT_NEAREST;
				regHash_t texName{ "DirectionalShadowColor" };
				uselessShadowColorTexture = new Texture(texName);
				uselessShadowColorTexture->Init(rcb);
				uselessShadowColorTexture->SetRTTexture(rcb, BTT_2D, texDim, BTF_RGB);
				uselessShadowColorTexture->SetSampler(rcb, newSettings);
				BASSERT(shadowMapTexture->GetAssetState() == assets::EAS_READY);

				//uselessShadowColorTexture = am.GetCommandBuffer()->CreateResource(RenderResourceType::IMAGE_BUFFER);
				//auto updateTexCommand = am.GetCommandBuffer()->AddCommand<command::UpdateTextureData>(uselessShadowColorTexture);
				//updateTexCommand->type = BTT_2D;
				//updateTexCommand->format = BTF_RGB;
				//updateTexCommand->dimensions = texDim;
				//
				//auto updFilterCmd = am.GetCommandBuffer()->AddCommand<command::UpdateTextureFiltering>(uselessShadowColorTexture);
				//updFilterCmd->newSettings.filteringType = TextureFilterType::BTFT_NEAREST;
			}

			shadowFramebuffer = am.GetCommandBuffer()->CreateResource(RenderResourceType::FRAMEBUFFER);
			auto updateRTCommand = am.GetCommandBuffer()->AddCommand<command::UpdateRTBindings>(shadowFramebuffer);
			updateRTCommand->resolution = uvec2(texDim);
			updateRTCommand->depthBinding = shadowMapTexture->GetHandle();
			updateRTCommand->colorBindings[0] = uselessShadowColorTexture->GetHandle();

			shadowCasterSolid = am.LoadAsset<Rendering::Material>("ShadowCaster");
		}

		void ShadowMapsFeature::TweakSettings()
		{
			ImGui::PushID("Shadow Settings");
			ImGui::Text("Shadow Settings");
			{
				int res = settings.resolution;
				ImGui::RadioButton("256", &settings.resolution, 256); ImGui::SameLine();
				ImGui::RadioButton("512", &settings.resolution, 512); ImGui::SameLine();
				ImGui::RadioButton("1024", &settings.resolution, 1024); ImGui::SameLine();
				ImGui::RadioButton("2048", &settings.resolution, 2048); ImGui::SameLine();
				ImGui::RadioButton("4096", &settings.resolution, 4096);
				if (res != settings.resolution)
					settings.resChanged = true;
				ImGui::SameLine(); ImGui::Text("Resolution");
			}
			{
				int fmt = settings.format;
				ImGui::RadioButton("16", &fmt, BTF_DEPTH16); ImGui::SameLine();
				ImGui::RadioButton("24", &fmt, BTF_DEPTH24); ImGui::SameLine();
				ImGui::RadioButton("32",&fmt, BTF_DEPTH32); ImGui::SameLine();
				ImGui::RadioButton("32F",&fmt, BTF_DEPTH32F);
				if (fmt != settings.format) {
					settings.format = (TextureFormat)fmt;
					settings.resChanged = true;
				}
				ImGui::SameLine(); ImGui::Text("Format");
			}

			ImGui::SliderFloat("World Extents", &settings.shadowDistance, 5.f, 400.f);
			ImGui::SliderFloat("Depth Bias", &settings.depthBias, 0.0f, 0.005f,"%.4f");
			ImGui::SliderFloat("Normal Bias", &settings.slopeBias, 0.0f, 1.0f);
			ImGui::Checkbox("Use automatic frustum matching", &settings.useAutoShadowFrustum);
			ImGui::Checkbox("Use tight frustum fit", &settings.useTightFrustumFit);

			ImGui::Checkbox("Freeze shadow frustum", &freezeShadowFrustum);
			ImGui::Checkbox("Preview shadow frustum", &displayShadowFrustumPreview);
			if(displayShadowFrustumPreview)
				ImGui::Image( (ImTextureID)(uintptr_t)shadowMapTexture->GetHandle().d, { 256,256 });

			ImGui::PopID();
		}

		void ShadowMapsFeature::PrepareDirectionalShadowView(Renderer& rd, detail::FPackage& fpack, detail::VPackage& vpack)
		{
			// Change RT resolution
			if (settings.resChanged) {
				auto& commands = rd.GetCommandBuffer();
				
				shadowMapTexture->SetRTTexture(commands, BTT_2D, uvec3(settings.resolution, settings.resolution, 1), settings.format);
				//auto depthResizeCmd = commands.AddCommand<command::UpdateTextureData>(shadowMapTexture);
				//depthResizeCmd->type = BTT_2D;
				//depthResizeCmd->format = ;
				//depthResizeCmd->dimensions = ;

				uselessShadowColorTexture->SetRTTexture(commands, BTT_2D, uvec3(settings.resolution, settings.resolution, 1), BTF_RGB);
				//auto colorResizeCmd = commands.AddCommand<command::UpdateTextureData>(uselessShadowColorTexture);
				//colorResizeCmd->type = BTT_2D;
				//colorResizeCmd->format = BTF_RGB;
				//colorResizeCmd->dimensions = uvec3(settings.resolution, settings.resolution, 1);

				auto rtResizeCmd = commands.AddCommand<command::ResizeRT>(shadowFramebuffer);
				rtResizeCmd->newSize = { settings.resolution, settings.resolution };

				settings.resChanged = false;
			}
			// Place shadow virtual camera
			if (!freezeShadowFrustum) {
				if (!settings.useAutoShadowFrustum)
				{
					shadowCam.SetPosition(glm::normalize(fpack.LightDirection) * (settings.shadowDistance / 4.f)); //Move arbitrarily toward the light
					shadowCam.SetUpVector({ 0.f, 0.f, 1.f });
					shadowCam.SetOrthoData(-settings.shadowDistance,settings.shadowDistance, -settings.shadowDistance, settings.shadowDistance);
					shadowCam.SetZNear(-settings.shadowDistance);
					shadowCam.SetZFar(settings.shadowDistance);
				}
				else {
					vec2 texelWorldSize;
					float zFar;
					vec3 lightCamPos;
					auto mainViewCamera = fpack.MainView.ActiveCamera;
					// Fit camera to main view frustum
					if (settings.useTightFrustumFit) { // Tight view frustum fit, flicker under motion
						//TODO: Use scene info to better fit
						lightCamPos = mainViewCamera->GetPosition() + (mainViewCamera->GetForward() * (mainViewCamera->GetZNear() + mainViewCamera->GetZFar()) / 2.f);
						shadowCam.SetPosition(lightCamPos); //Camera position in frustum middle
						shadowCam.SetTarget(lightCamPos - fpack.LightDirection);

						vec3 viewFrusumCorners[8];
						mainViewCamera->ExtractFrustumWSCorners(viewFrusumCorners);
						//DebugDraw()->Point( shadowCam.GetPosition(),1.f,{ 1.f,0.f,0.f,1.f });
						const mat4& shadowView = shadowCam.GetViewMatrix();
						vec3 min = vec3(shadowView * vec4(viewFrusumCorners[0], 1.f));
						vec3 max = vec3(shadowView * vec4(viewFrusumCorners[0], 1.f));
						for (int i = 1; i < 8; i++) {
							vec3 r = vec3(shadowView * vec4(viewFrusumCorners[i], 1.f));
							min = glm::min(r, min);
							max = glm::max(r, max);
						}
						vec2 viewSize = vec2(max.x - min.x, max.y - min.y);
						texelWorldSize = viewSize / float(settings.resolution);
						// Move camera "back" so it fits everything in Z
						lightCamPos += -shadowCam.GetForward() * min.z;
						zFar = max.z - min.z;
						// Set XY projection extents
						shadowCam.SetOrthoData( min.x, max.x, min.y, max.y);
					}
					else {
						// surround view frustum into a sphere, use AABB that holds that sphere.
						const vec4 sphere = mainViewCamera->TightEnclosingSphere(settings.shadowDistance);
						const float radius = sphere.w;
						const float viewSize = 2.f * radius;
						lightCamPos = vec3(sphere) + (fpack.LightDirection * radius); // Place just outside sphere
						texelWorldSize = vec2( viewSize / float(settings.resolution));
						zFar = viewSize;
						//TODO: We can get a slightly better fit by using the light-space bounds of the view frustum
						shadowCam.SetOrthoData(-radius, radius, -radius, radius);
					}
					// Remove translation flicker by moving shadow cam in shadow-texel increments
					{
						shadowCam.SetPosition({}); //Temp camera 
						shadowCam.SetTarget(-fpack.LightDirection);
						shadowCam.SetUpVector({ 0.f, 0.f, 1.f });
						if ((abs(glm::dot(shadowCam.GetUp(), fpack.LightDirection)) - 1.f) < FLT_EPSILON)
							shadowCam.SetUpVector({ 1.f, 0.f, 0.f});
						lightCamPos = shadowCam.SnapToViewSpaceGrid(lightCamPos, texelWorldSize);
					}
					shadowCam.SetPosition(lightCamPos); //Camera position on view frustum edge
					shadowCam.SetTarget(lightCamPos - fpack.LightDirection);
					shadowCam.SetZNear(0.1f);
					shadowCam.SetZFar(zFar); // Contain all of the view frustum
				}
			}
			//
			if (displayShadowFrustumPreview) {
				DebugDraw()->Point(shadowCam.GetPosition(), 1.f, { 1.f,0.f,0.f,1.f });
				vec3 viewFrusumCorners[8];
				shadowCam.ExtractFrustumWSCorners(viewFrusumCorners);
				for (int i = 0; i < 7; i++)
					DebugDraw()->Line(viewFrusumCorners[i], viewFrusumCorners[i + 1], { 1.f,0.f,0.f,1.f }
				);
			}
			//
			vpack.SetCameraData(&shadowCam);
			vpack.VOMask = SHADOW_VIEW;
			fpack.ShadowDrawSolidsMaterial = shadowCasterSolid.get();
		}

		void ShadowMapsFeature::OnSubmit(Renderer& rd)
		{
			auto backend = rd.GetBackend();
			backend->SetDepthBias(settings.slopeBias, settings.depthBias);
			// Prepare render targets
			backend->EnableRT(shadowFramebuffer);
			backend->SetViewport({}, { settings.resolution, settings.resolution });
			backend->Clear(FLAG_DEPTH);
		}

		void ShadowMapsFeature::OnPostSubmit(Renderer& rd, detail::FPackage& fpack)
		{
			auto backend = rd.GetBackend();
			backend->DisableRT();
			backend->SetDepthBias(0.f, 0.f);
			//
			auto& rendererBuiltins = rd.GetBuiltins();
			TextureSamplerSettings shadowParams;
			shadowParams.texCompareMode = TextureCompareFunc::BTCF_LEQUAL;
			shadowParams.filteringType = TextureFilterType::BTFT_LINEAR;
			shadowParams.mipFilteringType = TextureFilterType::BTFT_NONE;
			rendererBuiltins.Set("u_shadowMap", shadowMapTexture, shadowParams);
			rendererBuiltins.Set("u_depthShadowBias", 0.f);//NOTE:Using SetDepthBias instead settings.depthBias);
			vec4 shadowSize;
			shadowSize.x = shadowSize.y = (float)settings.resolution;
			shadowSize.z = shadowSize.w = 1.f / shadowSize.x;
			rendererBuiltins.Set("u_directionalShadowSize", shadowSize);
			
			//Bias when rendering to fit from clip space to texture space
			const glm::mat4 biasMatrix(
				0.5, 0.0, 0.0, 0.0,
				0.0, 0.5, 0.0, 0.0,
				0.0, 0.0, 0.5, 0.0,
				0.5, 0.5, 0.5, 1.0
			);
			fpack.DirShadowMatrix = biasMatrix * shadowCam.GetProjectionMatrix() * shadowCam.GetViewMatrix();
		}
	};
};