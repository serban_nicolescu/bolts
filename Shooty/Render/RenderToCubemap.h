#pragma once

#include "Helpers/Camera.h"
//#include "Rendering/RenderTargetFwd.h"
#include "Rendering/RenderingCommon.h"
#include "Rendering/MaterialNewFwd.h"
#include "Rendering/TextureFwd.h"
#include <bolts_core.h>

namespace Bolts
{
	namespace assets {
		class AssetManager;
	}

	namespace Rendering 
	{
		namespace detail {
			struct FPackage;
			struct VPackage;
		}

		class Renderer;
		class ResourceCommandBuffer;

		struct CubemapSettings
		{
			int resolution = 256;
			bool resChanged = false;
			bool generateMips = false;
			bool alwaysRender = false;
			float nearPlane = 0.1f;
			float farPlane = 100.0f;
			//
			vec3 position;
		};

		struct RenderToCubemapFeature
		{
			RenderToCubemapFeature();
			~RenderToCubemapFeature();

			void TweakSettings();

			void InitBuiltinResources(assets::AssetManager&);
			void Resize(ResourceCommandBuffer&);
			void PrepareView(Renderer&, detail::FPackage&, detail::VPackage&, int faceIdx);
			void OnSubmit(Renderer&, int faceIdx);
			void OnPostSubmit(Renderer&, int faceIdx);
			void OnMainSubmit(Renderer&, detail::FPackage&);

			CubemapSettings settings;
			Camera renderCam;
			TexturePtr cubemapTex;
			resHandle_t depthTex;
			resHandle_t	faceFB[6];
			bool hasMips = false;
		};
	}
}