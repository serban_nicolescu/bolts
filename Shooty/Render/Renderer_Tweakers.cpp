#include "StdAfx.h"
#include "Renderer.h"
#include <bolts_assert.h>

#include "imgui.h"


namespace Bolts
{
	namespace Rendering
	{

		void Renderer::TweakSettings()
		{
			if (ImGui::Begin("Rendering Settings", nullptr)) {
				ImGui::Separator();
				m_shadows.TweakSettings();
				ImGui::Separator();
				m_renderToCubemap.TweakSettings();
				ImGui::Separator();
				m_postProcessing.TweakSettings();
			}
			ImGui::End();
		}
	};
};

