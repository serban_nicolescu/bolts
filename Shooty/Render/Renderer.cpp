#include "StdAfx.h"
#include "Renderer.h"
#include "Rendering/Mesh.h"
#include "Rendering/MaterialNew.h"
#include "DebugDraw.h"
#include <bolts_assert.h>
#include <bolts_helpers.h>
#include <glm/gtc/matrix_transform.hpp>

#include <microprofile.h>
#include "Profiler.h"

namespace Bolts
{

	namespace Rendering
	{
		namespace {
			// builtin uniforms

			static const unsigned c_uniformSlotGlobals = 0;
			static const unsigned c_uniformSlotLights = 1;

			struct uniformGlobals
			{
				static const stringHash_t name;
				mat4 viewM;
				mat4 projM;
				vec4 worldCamera; // camera used for rendering current pass
				vec4 mainCamera; // main scene camera
				vec4 testColor;
			};
			const stringHash_t uniformGlobals::name{ "BoltsGlobals" };

			struct uniformLights
			{
				static const stringHash_t name;
				mat4 directionalShadowM;
				vec4 directionalLightDir;
				vec4 directionalLightColor;
				vec4 dynamicLightPosR[8]; // .w is radius 
				vec4 dynamicLightColor[8];
				int32_t dynamicLightCount;
			};
			const stringHash_t uniformLights::name{ "BoltsLights" };

			// materials
			//> Transfer params from holder into data, based on desc
			static int ExtractMaterialParameters(const CBufferDescription& desc, const MatParameterHolder& holder, CBufferData& dataOut, resHandle_t* texDataOut, TextureSamplerSettings* texSamplersOut)
			{
				dataOut.Resize(desc);
				int nextTexIdx = 0;
				int texIdx = 0;
				for (int idx = 0; idx < desc.numEntries; idx++)
				{
					const void* src = nullptr;
					auto info = holder.TryGetInfo(desc.names[idx]);
					if (info && desc.types[idx] == info->type && desc.arraySizes[idx] == 1) {
						if (info->type == SPT_TEX) 
						{
							texIdx = nextTexIdx;
							auto& texParam = *(const MatParameterHolder::TextureParam*) holder.GetDataUnsafe(info->dataOffset);
							if (texParam.handle.IsValid())
								texDataOut[texIdx] = texParam.handle;
							if (!texParam.useDefault)
								texSamplersOut[texIdx] = texParam.sampler;
							src = &texIdx;
						}
						else
						{
							src = holder.GetDataUnsafe(info->dataOffset);
						}
						void* dst = dataOut.Store(desc, idx);
						BASSERT(dst);
						memcpy(dst, src, detail::c_typeSize[info->type]);
					}
					if (desc.types[idx] == SPT_TEX)
						nextTexIdx++;
				}
				return nextTexIdx;
			}
		};

		Renderer::Renderer() = default;
		Renderer::~Renderer() = default;

		void Renderer::InitRenderer(RenderBackend* b)
		{
			m_backend = b;
			m_mainFrameCommandBuffer = m_backend->CreateCommandBuffer();

			// uniform buffers
			{
				m_globalUniformsBuffer = m_mainFrameCommandBuffer->CreateResource(GENERIC_BUFFER);
				auto cmd = m_mainFrameCommandBuffer->AddCommand<command::ResizeBuffer>(m_globalUniformsBuffer);
				cmd->dataPtr = nullptr;
				cmd->dataSize = sizeof(uniformGlobals);
				cmd->type = BUFFER_CONSTANT;
				cmd->usage = BUH_STREAM_DRAW;

				b->SetBuiltinUniformBufferSlot(uniformGlobals::name, c_uniformSlotGlobals, cmd->dataSize);
			}

			{
				m_lightUniformsBuffer = m_mainFrameCommandBuffer->CreateResource(GENERIC_BUFFER);
				auto cmd = m_mainFrameCommandBuffer->AddCommand<command::ResizeBuffer>(m_lightUniformsBuffer);
				cmd->dataPtr = nullptr;
				cmd->dataSize = sizeof(uniformLights);
				cmd->type = BUFFER_CONSTANT;
				cmd->usage = BUH_STREAM_DRAW;

				b->SetBuiltinUniformBufferSlot(uniformLights::name, c_uniformSlotLights, cmd->dataSize);
			}

			// textures
			{
				m_badTexHandle = m_mainFrameCommandBuffer->CreateResource(IMAGE_BUFFER);
				auto texDataCmd = m_mainFrameCommandBuffer->AddCommand<command::UpdateTextureData>(m_badTexHandle);
				texDataCmd->dimensions = { 1,1,1 };
				texDataCmd->format = BTF_RGB;
				texDataCmd->type = BTT_2D;
				size_t byteSize = GetMipSizeInBytes(texDataCmd->format, BTDF_IMPLICIT, texDataCmd->dimensions);
				void* data = m_mainFrameCommandBuffer->AllocScratchMemory(m_badTexHandle, byteSize);
				const byte3 badColor = byte3(255, 0, 127);
				BASSERT(byteSize == sizeof(badColor));
				memcpy(data, &badColor, sizeof(badColor));
				texDataCmd->dataPtr = data;
				texDataCmd->dataSize = byteSize;
			}

			//m_renderables.BindRenderableType<StaticMeshRenderable>();
			
		}

		void Renderer::StartNewFrame()
		{
			m_frameStats = detail::FrameStats();
			m_frameNum++;
		}

		void Renderer::FlushResourceCommands()
		{
			m_backend->ExecuteResourceCommands(m_mainFrameCommandBuffer);
		}

		void Renderer::SubmitVisibleRenderables(detail::VPackage& ViewPackage, detail::FPackage& FramePackage, detail::RenderContext &ctx)
		{
			PROFILE_SCOPE("SubmitVisibleRenderables");

			// bind per-view constants
			//m_builtinShaderParams.SetParam("u_v", ViewPackage.ViewMatrix);
			//m_builtinShaderParams.SetParam("u_p", ViewPackage.ProjMatrix);
			//m_builtinShaderParams.SetParam("u_cameraW", vec3(ViewPackage.CameraWorldPos));

			uniformGlobals viewGlobals;
			viewGlobals.viewM = ViewPackage.ViewMatrix;
			viewGlobals.projM = ViewPackage.ProjMatrix;
			viewGlobals.worldCamera = vec4( ViewPackage.CameraWorldPos, 0.f);
			viewGlobals.mainCamera = vec4( ViewPackage.CameraWorldPos, 0.f);
			viewGlobals.testColor = { 1.f, 0.f, 1.f, 0.f };

			{
				auto updateGlobalsCmd = GetCommandBuffer().AddCommand<command::ResizeBuffer>(m_globalUniformsBuffer);
				updateGlobalsCmd->type = BUFFER_CONSTANT;
				updateGlobalsCmd->dataSize = sizeof(viewGlobals);
				updateGlobalsCmd->dataPtr = &viewGlobals; //NOTE: This is safe, as long as we flush commands in current scope
			}

			// bind per-frame constants
			if (!ctx.drawingShadows)
			{
				// Lighting settings
				uniformLights lightUniforms;
				lightUniforms.directionalShadowM = FramePackage.DirShadowMatrix;
				lightUniforms.directionalLightDir = vec4( glm::normalize(FramePackage.LightDirection), 0.0);
				lightUniforms.directionalLightColor = vec4(FramePackage.LightIntensity, 0.0);

				auto& visibleLights = ViewPackage.VisibleRenderables[detail::LIGHT];
				lightUniforms.dynamicLightCount = (int)std::min(visibleLights.size(), glm::countof(lightUniforms.dynamicLightPosR));
				for (int i = 0; i < lightUniforms.dynamicLightCount; i++) {
					auto& light = GetLight(visibleLights[i]);
					
					lightUniforms.dynamicLightPosR[i] = light.m_worldTransform[3];
					lightUniforms.dynamicLightPosR[i].w = light.m_radius;
					lightUniforms.dynamicLightColor[i] = vec4{ light.m_color, 1.f };
				}
				// upload data
				auto updateLightsCmd = GetCommandBuffer().AddCommand<command::ResizeBuffer>(m_lightUniformsBuffer);
				updateLightsCmd->type = BUFFER_CONSTANT;
				updateLightsCmd->dataSize = sizeof(lightUniforms);
				updateLightsCmd->dataPtr = &lightUniforms; //NOTE: This is safe, as long as we flush commands in current scope
			}
			//
			FlushResourceCommands();

			// bind implicit uniform buffers
			m_backend->BindUniformBuffer( c_uniformSlotGlobals, m_globalUniformsBuffer, 0, 0);
			m_backend->BindUniformBuffer( c_uniformSlotLights, m_lightUniformsBuffer, 0, 0);

			if (ctx.drawingShadows) 
			{
				BASSERT(FramePackage.ShadowDrawSolidsMaterial); 
				if (FramePackage.ShadowDrawSolidsMaterial)
					EnableMaterial( *FramePackage.ShadowDrawSolidsMaterial);
			}

			// submit renderables
			auto& staticMeshes = ViewPackage.VisibleRenderables[detail::STATIC_MESH];
			if (staticMeshes.size() > 0) {
				StaticMeshRenderable::SubmitMany(staticMeshes,*this, FramePackage, ViewPackage, ctx);
			}
			// submit custom passes
			for (auto customPassFunc : m_customRenderLists)
				(*customPassFunc)( *this, FramePackage, ViewPackage, ctx);

			m_frameStats.numSubmits += ViewPackage.NumVisibleRenderables;
		}

		void Renderer::Render(assets::AssetManager&, uvec2 viewportSize, uvec2 viewportOffset)
		{
			MICROPROFILE_SCOPEI("Rendering", "Render", MP_ORANGE1);
			MICROPROFILE_SCOPEGPUI("Render", MP_ORANGE1);
			m_windowSize = viewportSize;

			detail::FPackage& fpackage = GetRenderFramePackage();
			fpackage.Clear();
			fpackage.FrameNumber = m_frameNum;

			detail::VPackage& mainView = fpackage.MainView;
			float aspectRatio = (float)viewportSize.x / viewportSize.y;
			m_mainCamera.SetAspectRatio(aspectRatio);
			mainView.SetCameraData(&m_mainCamera);
			mainView.VOMask = MAIN_VIEW;

			detail::RenderContext mainCtx;
	   
			// Directional Shadows
			{
				MICROPROFILE_SCOPEI("Rendering", "Shadows Directional", MP_ORANGE3);
				MICROPROFILE_SCOPEGPUI("DirectionalShadows", MP_ORANGE3);
				PROFILE_SCOPE("DirectionalShadows");
				PROFILE_SCOPEGPU("DirectionalShadows");

				detail::VPackage& shadowsView = fpackage.ShadowsView;
				m_shadows.PrepareDirectionalShadowView(*this, fpackage, shadowsView);
				//TODO: Cull shadowcasters better
				CullVOS(shadowsView);
				//
				m_shadows.OnSubmit(*this);

				detail::RenderContext ctx;
				ctx.drawingShadows = true;
				SubmitVisibleRenderables(shadowsView, fpackage, ctx);

				m_shadows.OnPostSubmit(*this, fpackage);
			}

			// Render to cubemap
			if (m_renderToCubemap.settings.alwaysRender)
			{
				MICROPROFILE_SCOPEI("Rendering", "Render to cubemap", MP_ORANGE3);
				MICROPROFILE_SCOPEGPUI("RenderToCubemap", MP_ORANGE3);
				PROFILE_SCOPE("RenderToCubemap");
				PROFILE_SCOPEGPU("RenderToCubemap");

				detail::VPackage& view = fpackage.RenderToCubeView;
				detail::RenderContext reflectionCtx;
				reflectionCtx.drawingReflections = true;
				for (int i = 0; i < 6; i++) {
					view.Clear();
					m_renderToCubemap.PrepareView(*this, fpackage, view, i);
					FlushResourceCommands();
					CullVOS(view);
					//
					m_renderToCubemap.OnSubmit(*this, i);

					SubmitVisibleRenderables(view, fpackage, reflectionCtx);

					m_renderToCubemap.OnPostSubmit(*this, i);
				}
			}

			// Main View
			{
				MICROPROFILE_SCOPEI("Rendering", "Main scene render", MP_ORANGE2);
				MICROPROFILE_SCOPEGPUI("RenderScene", MP_ORANGE2);
				PROFILE_SCOPE("MainScene");
				PROFILE_SCOPEGPU("MainScene");

				CullVOS(mainView);
				m_frameStats.numCulled += m_vos.SizeActive() - mainView.NumVisibleRenderables;
				m_frameStats.numVisible += mainView.NumVisibleRenderables;

				m_renderToCubemap.OnMainSubmit(*this, fpackage);
				m_debugDrawer->SubmitGizmos(GetCommandBuffer());//TODO: Don't resubmit if this is called multiple times per frame

				m_backend->SetViewport(viewportOffset, viewportSize);
				m_postProcessing.OnSubmit(*this, fpackage, mainView);

				// Submit all visibile renderables
				SubmitVisibleRenderables(mainView, fpackage, mainCtx);
				// Debug Gizmos
				m_debugDrawer->DrawGizmos(*this, mainView.ViewMatrix, mainView.ProjMatrix);
			}

			{
				PROFILE_SCOPE("Final PostProcessing");
				PROFILE_SCOPEGPU("Final PostProcessing");
				m_postProcessing.OnPostSubmit(*this, fpackage);
			}

			// Call render hacks
			{
				PROFILE_SCOPE("RenderHacks");
				PROFILE_SCOPEGPU("RenderHacks");
				for (renderHack_t* hack : m_renderHacks)
				{
					(*hack)(*this);
				}
			}
		}

		void Renderer::RenderCustomView(const mat4& viewMat, const mat4& projMat, vec3 cameraPosition)
		{
			MICROPROFILE_SCOPEI("Rendering", "Custom render view", MP_ORANGE2);
			MICROPROFILE_SCOPEGPUI("Render", MP_ORANGE2);
			PROFILE_SCOPE("CustomView");
			PROFILE_SCOPEGPU("CustomView");

			detail::FPackage& fpackage = GetRenderFramePackage();
			detail::VPackage& view = fpackage.CustomView;
			detail::RenderContext customCtx;
			
			view.Clear();
			view.CameraWorldPos = cameraPosition;
			view.ViewMatrix = viewMat;
			view.ProjMatrix = projMat;

			CullVOS(view);
			SubmitVisibleRenderables(view, fpackage, customCtx);
		}

		void Renderer::EndFrame()
		{
			m_backend->ClearShadowstate();
			m_debugDrawer->ClearGizmos();
		}

		void Renderer::EnableMaterial(const Material& mat)
		{
			GPUProgram* shaderProgram = mat.GetProgram();
			BASSERT(shaderProgram);
			m_backend->SetRenderState(mat.GetRenderState());
			m_backend->BindProgram(shaderProgram->shaderHandle);
			// update textures and uniforms
			const CBufferDescription& materialUniformsDesc = shaderProgram->cbuffers[ GPUProgram::c_maxNumUniformBuffers - 1];
			// Clear tex list & samplers
			memset(m_scratchTexListData, 0, sizeof(m_scratchTexListData));
			memset(m_scratchTexSamplerData, 0, sizeof(m_scratchTexSamplerData));
			int numTextures = 0; // count required samplers
			numTextures = ExtractMaterialParameters(materialUniformsDesc, mat.GetParams(), m_scratchCbufferData, m_scratchTexListData, m_scratchTexSamplerData);
			ExtractMaterialParameters(materialUniformsDesc, m_builtinShaderParams, m_scratchCbufferData, m_scratchTexListData, m_scratchTexSamplerData);
			for (int i = 0; i < numTextures; i++)
				if (!m_scratchTexListData[i].IsValid())
					m_scratchTexListData[i] = m_badTexHandle;
			BASSERT(numTextures < c_numMaxTexBindings);
			m_backend->BindTextureList(m_scratchTexListData, m_scratchTexSamplerData, numTextures);
			m_backend->UpdateUniformData(materialUniformsDesc, m_scratchCbufferData.data, m_scratchCbufferData.dataSize);
		}

		void Renderer::QuickSubmitBuiltinUniforms(const Material& mat)
		{
			GPUProgram* shaderProgram = mat.GetProgram();
			BASSERT(shaderProgram);
			const CBufferDescription& materialUniformsDesc = shaderProgram->cbuffers[GPUProgram::c_maxNumUniformBuffers - 1];
			ExtractMaterialParameters(materialUniformsDesc, m_builtinShaderParams, m_scratchCbufferData, m_scratchTexListData, m_scratchTexSamplerData);
			m_backend->UpdateUniformData(materialUniformsDesc, m_scratchCbufferData.data, m_scratchCbufferData.dataSize);
		}
		
		static BOLTS_INLINE int ApproxPrimitiveCount(GPUPrimitiveType prim, uint32_t numElements)
		{
			switch (prim) {
			case BGPT_TRIANGLES:
				return numElements / 3;
			case BGPT_TRIANGLE_STRIP:
			case BGPT_TRIANGLE_FAN:
				return numElements - 2;
			case BGPT_POINTS:
				return numElements;
			case BGPT_LINES:
				return numElements / 2;
			case BGPT_LINE_STRIP:
				return numElements - 1;
			}
			return 0;
		}

		void Renderer::Draw(const PrimitiveBuffer& pbuffer, uint32_t numElements)
		{
			BASSERT_MSG(pbuffer.IsValid(), "Rendering: Attempting to render from an invalid primitive buffer");
			m_frameStats.numBatches++;
			m_frameStats.numDraws++;
			m_frameStats.numPrimitives += pbuffer.m_numPrimitives;
			m_backend->BindPrimitives(pbuffer.m_pbufferHandle);
			m_backend->Draw(pbuffer.m_primitiveType, numElements ? numElements : pbuffer.m_numElements, 0, 1);
		}

		void Renderer::Draw(GPUPrimitiveType primType, uint32_t numElements)
		{
			BASSERT(numElements > 0);
			m_frameStats.numBatches++;
			m_frameStats.numDraws++;
			m_frameStats.numPrimitives += ApproxPrimitiveCount(primType, numElements);
			m_backend->BindPrimitives({});
			m_backend->Draw(primType, numElements, 0, 1);
		}

		void Renderer::DrawInstanced(const PrimitiveBuffer& pbuffer, uint32_t numElements, int numInstances)
		{
			BASSERT_MSG(pbuffer.IsValid(), "Rendering: Attempting to render from an invalid primitive buffer");
			m_frameStats.numBatches++;
			m_frameStats.numDraws += numInstances;
			m_frameStats.numPrimitives += pbuffer.m_numPrimitives * numInstances;
			m_backend->BindPrimitives(pbuffer.m_pbufferHandle);
			m_backend->Draw(pbuffer.m_primitiveType, numElements ? numElements : pbuffer.m_numElements, 0, numInstances);
		}

		void Renderer::DrawInstanced(GPUPrimitiveType primType, uint32_t numElements, int numInstances)
		{
			BASSERT(numElements > 0);
			m_frameStats.numBatches++;
			m_frameStats.numDraws += numInstances;
			m_frameStats.numPrimitives += ApproxPrimitiveCount(primType, numElements) * numInstances;
			m_backend->BindPrimitives({});
			m_backend->Draw(primType, numElements, 0, numInstances);
		}

		void Renderer::AddCustomRenderlist(customRenderlist_t* renderFunc)
		{
			BASSERT(renderFunc);
			if (!contains(m_customRenderLists, renderFunc)) {
				m_customRenderLists.push_back(renderFunc);
			}
		}

		void Renderer::RemoveCustomRenderlist(customRenderlist_t* renderFunc)
		{
			try_remove(m_customRenderLists, renderFunc);
		}

		void Renderer::AddRenderHack(renderHack_t* h)
		{
			BASSERT(h);
			if (!contains(m_renderHacks, h)) {
				m_renderHacks.push_back(h);
			}
		}

		void Renderer::RemoveRenderHack(renderHack_t* h)
		{
			try_remove(m_renderHacks, h);
		}

		//
		void Renderer::SetDirectionalLight(vec3 direction, vec3 intensity)
		{
			auto& frame = GetRenderFramePackage();
			frame.LightIntensity = intensity;
			frame.LightDirection = direction;
		}

		void Renderer::UpdateCamera(const Camera* cam)
		{
			m_mainCamera.CopyFrom(cam);
		}

		mat4 GetDirectionalLightTransform(vec3 lightDir, mat4 model)
		{
			using namespace Bolts;
			Camera tempCam;
			tempCam.SetZNear(10.f);
			tempCam.SetZFar(100000.f);
			tempCam.SetPosition(lightDir * 1000.f); //Move arbitrarily toward the light
			const mat4	viewWorld = tempCam.GetViewMatrix() * model;
			const float viewSize = 6000.f;
			const mat4	wvp = glm::ortho(-viewSize, viewSize, -viewSize, viewSize, 10.f, 3000.f)  * viewWorld;
			//const mat4	wvp = tempCam.GetProjectionMatrix () * viewWorld;
			return wvp;
		}
};
};
