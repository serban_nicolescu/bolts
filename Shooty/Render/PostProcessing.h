#pragma once

//#include "Helpers/Camera.h"
//#include "Rendering/RenderTargetFwd.h"
#include "Rendering/RenderingCommon.h"
#include "Rendering/MaterialNewFwd.h"
#include "Rendering/TextureFwd.h"
#include <bolts_core.h>

namespace Bolts
{
	namespace assets {
		class AssetManager;
	}

	namespace Rendering 
	{
		namespace detail {
			struct FPackage;
			struct VPackage;
		}

		class Renderer;
		class ResourceCommandBuffer;
		class PrimitiveBuffer;

		struct RenderTarget
		{
			TexturePtr	texColor;
			TexturePtr	texDepth;
			resHandle_t	rtHandle;
			float sizeRatio = 1.f; //resolution ratio to main framebuffer
			vec4 resolution;
		};

		struct RenderPass
		{
			void Init(assets::AssetManager&	assetManager, const char* matName, uvec2 outRes, TextureFormat outFmt, const char* outName);
			void Init(assets::AssetManager&	assetManager, const char* matName, uvec2 outRes, resHandle_t outTex);
			void Render(Renderer& renderer, bool clear);

			MaterialPtr	mat;
			TexturePtr	outTex;
			resHandle_t	texHandle;
			resHandle_t	FB;
		};

		struct PostSettings
		{
			bool enableSSAO = true;
			float ssaoRadiusMeters = 1.f; //world-space meters
			float ssaoRadiusClamp = 0.25f; // screen-space max distance

			bool enableBloom = true;
			float bloomThreshold = 0.9f;

			bool enableTAA = false;
			float taaAlpha = 0.1f;
		};

		struct PostProcessing
		{
			PostProcessing();
			~PostProcessing();

			void InitBuiltinResources(assets::AssetManager&);
			void ResizeRT(Renderer& rd);

			void TweakSettings();
			void OnSubmit(Renderer&, detail::FPackage&, detail::VPackage&); // before main scene render
			void OnPostSubmit(Renderer&, detail::FPackage&); // after main scene render

			PostSettings settings;

			uvec2	maxRTResolution = { 0,0 };
			uvec2	mainSceneResolutionPx;
			vec4	mainResolution;
			RenderTarget mainSceneRT[2]; // RT for main scene rendering
			RenderTarget* mainScene{};
			RenderTarget* mainScenePrevious{};
			MaterialPtr	matFinalBlit; // apply final post processing the scene and blits it into the backbuffer
			RenderPass	ssao; // generate SSAO texture

			struct {
				RenderTarget rtSceneJittered;
				MaterialPtr	matAccumulate;
			} taa;

			struct {
				MaterialPtr	matThreshold;
				MaterialPtr	matBlurH;
				MaterialPtr	matBlurV;
				RenderTarget rt[2];
				TexturePtr	texOut; // texture where final result is stored. points to one of the rt outputs above
			} bloom;

			//Debug
			bool enabled = false;
			bool showAOOnly = false;
		};
	}
}