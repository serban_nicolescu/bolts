#pragma once
#include "Helpers/Camera.h"
#include "Rendering/MeshFwd.h"
#include "Rendering/GPUProgram.h"
#include "ShadowMaps.h"
#include "RenderToCubemap.h"
#include "PostProcessing.h"
#include <Rendering/Common/MatParameterHolder.h>
#include <bolts_core.h>
#include <bolts_slotmap.h>
#include <bolts_binaryserialization.h>
#include <bolts_indirectionvectorSOA.h>

namespace Bolts
{
	class DebugDrawer;

	namespace Rendering
	{
		typedef uint32_t voHandle_t;
		typedef uint32_t renderableHandle_t;
		class StaticMeshRenderable;
		class PrimitiveBuffer;

		enum voListMask_t : uint8_t
		{
			NO_VIEW				= 1 << 0,
			MAIN_VIEW			= 1 << 1,
			SHADOW_VIEW			= 1 << 2,
			REFLECTIONS_VIEW	= 1 << 3,
		};

		namespace detail
		{
			enum RenderableType {
				STATIC_MESH = 0,
				LIGHT,
				RT_COUNT
			};

			enum RenderList {
				SOLIDS = 0,
				TRANSPARENTS,
				DEPTH_ONLY, // prepass
				RL_COUNT
			};

			class RenderableBase
			{
			public:
				friend class Renderer;
				
				void							SetWorldTransform(const mat4&);
				BOLTS_INLINE const mat4&		GetWorldTransform() const { return m_worldTransform; }
				BOLTS_INLINE renderableHandle_t	GetRenderableID() const { return m_id; }
			protected:
				renderableHandle_t m_id;
				mat4 m_worldTransform;
			};

			struct VisibilityTestResult
			{
				uint32_t vIdx; // This is the index into the VisibilitySet list, not a voID
				float camDistSq;
			};
			typedef vec_t<VisibilityTestResult> visResults_t;

			struct VPackage // View Package
			{
				void Clear();
				void SetCameraData(const Camera* cam);

				mat4						ViewMatrix;
				mat4						ProjMatrix;
				vec_t<uint32_t>				RenderLists[8]; //NOTE: Have indices into VPackage lists
				Camera const*				ActiveCamera;
				vec3						CameraWorldPos;
				voListMask_t				VOMask; // mask for selecting VO sets

				//vec_t<uint32_t>			RenderableIndices; // Corresponding indices into FPackage, for retrieving per-frame data
				vec_t<renderableHandle_t>	VisibleRenderables[RT_COUNT];
				uint32_t					NumVisibleRenderables = 0;
				visResults_t				VisibilityResultCache; //only here so we don't reallocate every frame. Ideally we'd only have one per-thread
			};

			struct FPackage // Frame Package
			{
				void Clear();

				VPackage			MainView;
				vec3				LightDirection;
				vec3				LightIntensity;
				unsigned			FrameNumber;
				//
				VPackage			CustomView;
				VPackage			RenderToCubeView;
				// Shadow rendering
				VPackage			ShadowsView;
				mat4				DirShadowMatrix;
				Material*			ShadowDrawSolidsMaterial {};
			};

			struct RenderContext
			{
				bool drawingShadows = false;
				bool drawingReflections = false;
			};

			class VisibilitySet:public IndirectionVectorSOA<VisibilitySet>
			{
			public:
				template<typename... Args>
				void Emplace(Args&&... args) {
					visibilitySpheres.emplace_back();
					voAttachments.emplace_back();
					voListMasks.emplace_back();
					voDebugString.emplace_back();
				}

				void PopBack()
				{
					visibilitySpheres.pop_back();
					voAttachments.pop_back();
					voListMasks.pop_back();
					voDebugString.pop_back();
				}

				unsigned Size() const
				{
					return (unsigned)visibilitySpheres.size();
				}

				void Swap(uint32_t idx1, uint32_t idx2)
				{
					std::swap(visibilitySpheres[idx1], visibilitySpheres[idx2]);
					std::swap(voAttachments[idx1], voAttachments[idx2]);//TODO: Attach a VO to multiple renderables ?
					std::swap(voListMasks[idx1], voListMasks[idx2]);
					std::swap(voDebugString[idx1], voDebugString[idx2]);
				}

				vec_t<vec4>					visibilitySpheres;
				vec_t<renderableHandle_t>	voAttachments;
				vec_t<voListMask_t>			voListMasks; // bitmask of vo lists this VO is registered to
				vec_t<regHash_t>			voDebugString;
			};

			typedef uint32_t voIdx_t;// index into visibilityObjects array

			struct FrameStats {
				int numDraws = 0; // approx, number of draw "commands"
				int numBatches = 0; // number of actual draw function calls, including instanced and indirect
				int numVisible = 0;
				int numCulled = 0;
				int numSubmits = 0;
				int numPrimitives = 0; // number of triangles, points or lines
			};
		};

		class StaticMeshRenderable :public detail::RenderableBase
		{
		public:
			static const detail::RenderableType c_type = detail::RenderableType::STATIC_MESH;

			static void OnVisibleInFrame(void* renderables, int count, renderableHandle_t id, Renderer&, const detail::FPackage&);
			static void OnVisibleInView(void* renderables, int count, renderableHandle_t id, Renderer&, const detail::FPackage&, const detail::VPackage&, uint32_t viewIndex);
			static void SubmitMany(const vec_t<renderableHandle_t>& toSubmit, Renderer& renderer, const detail::FPackage& fpack, const detail::VPackage& vpack, const detail::RenderContext& ctx);

			StaticMeshRenderable();
			~StaticMeshRenderable();
			void		OnVisibleInFrame(Renderer&, const detail::FPackage&, uint32_t frIdx); //NOTE: The frIdx is this renderable's index into the frame package
			void		OnVisibleInView(Renderer&, const detail::FPackage&, const detail::VPackage&, uint32_t vrIdx);//NOTE: The vrIdx is this renderable's index into the view package
			void		Submit(Renderer&, const detail::FPackage&, const detail::VPackage&, const detail::RenderContext&);

			MeshPtr				m_mesh;
			vec_t<MaterialPtr>	m_materials;
		};

		class LightRenderable :public detail::RenderableBase
		{
		public:
			static const detail::RenderableType c_type = detail::RenderableType::LIGHT;
			
			vec3		m_color = vec3_one;
			float		m_radius = 1.f;
		};

		class Renderer
		{
		public:
			Renderer();
			~Renderer();

			void InitRenderer(RenderBackend*);
			RenderBackend* GetBackend() { return m_backend; }
			ResourceCommandBuffer& GetCommandBuffer() { return *m_mainFrameCommandBuffer; }
			void FlushResourceCommands();

			//TEMP - Used for setting implicit shader parameters ( like m_WVP, m_LighPos0, etc.)
			MatParameterHolder&	GetBuiltins() { return m_builtinShaderParams; }

			//
			void InitBuiltinResources(assets::AssetManager&);
			void TweakSettings();
			// VO Management
			voHandle_t AddVisibilityObject();
			void RemoveVisibilityObject(voHandle_t);
			void RegisterVisibilityObject(voHandle_t);
			void UnregisterVisibilityObject(voHandle_t);
			void SetVOData(voHandle_t, const mat4& world,float radius);
			void SetVOTransformAndRadius(voHandle_t, const mat4& world, float radius);
			void SetVODebugString(voHandle_t, regHash_t h);
			//void SetVOTransform(voHandle_t, const mat4& world);
			//void SetVORadius(voHandle_t, float radius);
			void SetVOLists(voHandle_t, voListMask_t);
			// Renderable Management
			void RemoveRenderable(renderableHandle_t);//NOTE: This doesn't detach it from the VO. you need to do that yourself or we crash ( if we're lucky )
			void AttachRenderable(voHandle_t, renderableHandle_t);
			void DetachRenderable(voHandle_t, renderableHandle_t);
			StaticMeshRenderable&	AddStaticMesh();
			StaticMeshRenderable&	GetStaticMesh(renderableHandle_t);
			LightRenderable&		AddLight();
			LightRenderable&		GetLight(renderableHandle_t);
			detail::RenderableBase& GetRenderableAny(renderableHandle_t);
			// Frames
			void StartNewFrame();
			void CullVOS(detail::VPackage & mainView);
			void SubmitVisibleRenderables(detail::VPackage & mainView, detail::FPackage & fpackage, detail::RenderContext &ctx);
			void Render(assets::AssetManager&, uvec2 viewportSize, uvec2 viewportOffset = {});
			void RenderCustomView(const mat4& viewMat, const mat4& projMat, vec3 cameraPosition);
			void EndFrame();
			// Rendering
			void EnableMaterial(const Material& mat);
			void QuickSubmitBuiltinUniforms(const Material& mat); // update GPU uniform data using the builtins list. Much faster than EnableMaterial
			void Draw(const PrimitiveBuffer&, uint32_t numPrimitives = 0);
			void Draw(GPUPrimitiveType primType, uint32_t numPrimitives); // draw without a prim buffer attached
			void DrawInstanced(const PrimitiveBuffer&, uint32_t numPrimitives, int numInstances);
			void DrawInstanced(GPUPrimitiveType primType, uint32_t numPrimitives, int numInstances);
			//
			typedef std::function<void(Renderer&, const detail::FPackage&, const detail::VPackage&, const detail::RenderContext&)> customRenderlist_t;
			void	AddCustomRenderlist(customRenderlist_t*);
			void	RemoveCustomRenderlist(customRenderlist_t*);
			//
			typedef std::function<void(Renderer&)> renderHack_t;
			void	AddRenderHack(renderHack_t*);
			void	RemoveRenderHack(renderHack_t*);
			// Settings
			uvec2 GetWindowSize() { return m_windowSize; }
			void SetDirectionalLight(vec3 direction, vec3 intensity);
			void UpdateCamera(const Camera* cam);
			ShadowMapSettings& EditShadowSettings() { return m_shadows.settings; }
			// Debug
			const detail::FrameStats&	GetFrameStats() { return m_frameStats;}
			DebugDrawer*				GetDebugDrawer() { return m_debugDrawer; }

		private:
			static const int	c_numMaxTexBindings = 20;

			CBufferData			m_scratchCbufferData;
			resHandle_t			m_scratchTexListData[c_numMaxTexBindings];
			TextureSamplerSettings m_scratchTexSamplerData[c_numMaxTexBindings];
			resHandle_t			m_badTexHandle;
			MaterialPtr			m_badMat;
			MatParameterHolder	m_builtinShaderParams;
			resHandle_t			m_globalUniformsBuffer;
			resHandle_t			m_lightUniformsBuffer;

			vec_t< customRenderlist_t*> m_customRenderLists; // List of functions to call during main frame rendering.
			vec_t< renderHack_t*> m_renderHacks; // List of functions to call after frame rendering is done.

			detail::FPackage&	GetRenderFramePackage() { return m_frame; }
			
			RenderBackend*					m_backend = nullptr;
			ResourceCommandBuffer*			m_mainFrameCommandBuffer = nullptr;
			DebugDrawer*					m_debugDrawer = nullptr;
			// Renderables
			detail::VisibilitySet					m_vos;//Holds all created and registered VisibilityObjects for main view
			SlotMap<StaticMeshRenderable, true>		m_staticMeshes;
			SlotMap<LightRenderable, true>			m_lights;
			// Frame
			uvec2				m_windowSize;
			unsigned			m_frameNum = 0;
			detail::FPackage	m_frame;
			detail::FrameStats	m_frameStats;
			//
			Camera				m_mainCamera;
			ShadowMapsFeature	m_shadows;
			RenderToCubemapFeature m_renderToCubemap;
			PostProcessing		m_postProcessing;
		};
};
};