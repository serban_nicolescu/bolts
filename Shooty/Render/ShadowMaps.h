#pragma once

#include "Helpers/Camera.h"
//#include "Rendering/RenderTargetFwd.h"
#include "Rendering/RenderingCommon.h"
#include "Rendering/MaterialNewFwd.h"
#include "Rendering/TextureFwd.h"
#include <bolts_core.h>

namespace Bolts
{
	namespace assets {
		class AssetManager;
	}

	namespace Rendering 
	{
		namespace detail {
			struct FPackage;
			struct VPackage;
		}

		class Renderer;
		class ResourceCommandBuffer;

		//https://msdn.microsoft.com/en-us/library/windows/desktop/ee416324%28v=vs.85%29.aspx
		//http://ogldev.atspace.co.uk/www/tutorial49/tutorial49.html
		// add receiver normal bias
		// see the witness blog for shadow filtering
		struct ShadowMapsFeature
		{
			ShadowMapsFeature();
			~ShadowMapsFeature();

			void InitBuiltinResources(Bolts::assets::AssetManager&);

			void TweakSettings();
			void PrepareDirectionalShadowView(Renderer&, detail::FPackage&, detail::VPackage&);
			void OnSubmit(Renderer&);
			void OnPostSubmit(Renderer&, detail::FPackage&);

			ShadowMapSettings settings;
			Camera shadowCam;
			mat4 shadowBPVMatrix; // Bias * Proj * View matrix

			TextureWeakPtr shadowMapTexture;
			TextureWeakPtr uselessShadowColorTexture;
			resHandle_t	shadowFramebuffer;

			MaterialPtr shadowCasterSolid;
			//Debug
			bool freezeShadowFrustum = false;
			bool displayShadowFrustumPreview = false;
		};
	}
}