#include "StdAfx.h"
#include "DebugDraw.h"
#include "Rendering/Mesh.h"
#include "Rendering/MaterialNew.h"
#include <microprofile.h>

namespace Bolts
{
	DebugDrawer::DebugDrawer() {}
	DebugDrawer::~DebugDrawer() {}

	void DebugDrawer::Init(Rendering::RCB& commands, Rendering::MaterialPtr colorMat, Rendering::MaterialPtr transpMat)
	{
		Rendering::VertexLayout vlayout;
		vlayout.AddStream({ Rendering::BGSS_POSITION, Rendering::BGVT_FLOAT, 3 });
		vlayout.AddStream({ Rendering::BGSS_COLOR0, Rendering::BGVT_FLOAT, 4 });

		m_colorMat = colorMat;
		m_transpMat = transpMat;
		{ //Tri vertices desc
			m_triVertices = new Rendering::VertexBuffer;
			m_triVertices->SetData(commands, Rendering::BUH_DYNAMIC_DRAW, vlayout, 0);
			m_triIndices = new Rendering::IndexBuffer;
			m_triIndices->SetData(commands, Rendering::BUH_DYNAMIC_DRAW, 0, Rendering::BGVT_U16);

			m_triPrimitives = new Rendering::PrimitiveBuffer;
			m_triPrimitives->Build(commands, m_triVertices, m_triIndices, Rendering::BGPT_TRIANGLES);
		}
		{ //Line vertices desc
			m_lineVertices = new Rendering::VertexBuffer;
			m_lineVertices->SetData(commands, Rendering::BUH_DYNAMIC_DRAW, vlayout, 0);

			m_linePrimitives = new Rendering::PrimitiveBuffer;
			m_linePrimitives->Build(commands, m_lineVertices, nullptr, Rendering::BGPT_LINES);
		}
		{ //Point vertices desc
			m_pointVertices = new Rendering::VertexBuffer;
			m_pointVertices->SetData(commands, Rendering::BUH_DYNAMIC_DRAW, vlayout, 0);

			m_pointPrimitives = new Rendering::PrimitiveBuffer;
			m_pointPrimitives->Build(commands, m_pointVertices, nullptr, Rendering::BGPT_POINTS);
		}

		//
		const float c_circleVerts = 64.f;
		const float c_circleStepSize = c_PI * 2.f / c_circleVerts;
		m_cacheCircleVerts.reserve( int(c_circleVerts));
		for (float i = 0.f; i < c_circleVerts + 1; i++)
		{
			vec2 p;
			p.x = cosf(i * c_circleStepSize);
			p.y = sinf(i * c_circleStepSize);
			m_cacheCircleVerts.push_back(p);
		}
	}

	void DebugDrawer::Line(vec3 start, vec3 end, vec4 color)
	{
		m_lines.push_back({ start, color });
		m_lines.push_back({ end, color });
	}

	void DebugDrawer::Point(vec3 pos, float /*scale*/, vec4 color)
	{
		m_points.push_back({ pos, color});
	}

	void DebugDrawer::OBB(vec3 extents, const mat4& transform, vec4 color, DebugDrawStyle dds)
	{
		AABB box;
		box.min = -extents;
		box.max = extents;
		vec3 points[8];
		GetOBBVerts(points, transform, box);
		OBB(points, color, dds);
	}

	// const AABB& aabb, const mat4& transform
	void DebugDrawer::OBB(const vec3 points[8], vec4 color, DebugDrawStyle dds)
	{
		//vec3 points[8];
		//GetOBBVerts(points, transform, aabb);
		//points[0] = transform * vec4(+extents.x,+extents.y,+extents.z, 1.f);
		//points[1] = transform * vec4(-extents.x,+extents.y,+extents.z, 1.f);
		//points[2] = transform * vec4(+extents.x,-extents.y,+extents.z, 1.f);
		//points[3] = transform * vec4(-extents.x,-extents.y,+extents.z, 1.f);
		//points[4] = transform * vec4(+extents.x,+extents.y,-extents.z, 1.f);
		//points[5] = transform * vec4(-extents.x,+extents.y,-extents.z, 1.f);
		//points[6] = transform * vec4(+extents.x,-extents.y,-extents.z, 1.f);
		//points[7] = transform * vec4(-extents.x,-extents.y,-extents.z, 1.f);
		if (dds & DebugDrawStyle::BDDS_WIREFRAME) {
			m_lines.push_back({ points[0], color });
			m_lines.push_back({ points[1], color });
			m_lines.push_back({ points[0], color });
			m_lines.push_back({ points[2], color });
			m_lines.push_back({ points[0], color });
			m_lines.push_back({ points[4], color });
			m_lines.push_back({ points[7], color });
			m_lines.push_back({ points[3], color });
			m_lines.push_back({ points[7], color });
			m_lines.push_back({ points[5], color });
			m_lines.push_back({ points[7], color });
			m_lines.push_back({ points[6], color });
			//
			m_lines.push_back({ points[3], color });
			m_lines.push_back({ points[1], color });
			m_lines.push_back({ points[3], color });
			m_lines.push_back({ points[2], color });
			m_lines.push_back({ points[4], color });
			m_lines.push_back({ points[6], color });
			m_lines.push_back({ points[4], color });
			m_lines.push_back({ points[5], color });
			m_lines.push_back({ points[5], color });
			m_lines.push_back({ points[1], color });
			m_lines.push_back({ points[6], color });
			m_lines.push_back({ points[2], color });
		}
		if (dds & DebugDrawStyle::BDDS_TRANSP){
			BASSERT(m_tris.size() < uint16_t(-1));
			uint16_t base = (uint16_t)m_tris.size();
			m_triIds.push_back(base + 0); m_triIds.push_back(base + 3); m_triIds.push_back(base + 2); // +Z
			m_triIds.push_back(base + 0); m_triIds.push_back(base + 1); m_triIds.push_back(base + 3); 
			m_triIds.push_back(base + 0); m_triIds.push_back(base + 4); m_triIds.push_back(base + 5); // +Y
			m_triIds.push_back(base + 0); m_triIds.push_back(base + 5); m_triIds.push_back(base + 1); 
			m_triIds.push_back(base + 0); m_triIds.push_back(base + 6); m_triIds.push_back(base + 4); // +X
			m_triIds.push_back(base + 0); m_triIds.push_back(base + 2); m_triIds.push_back(base + 6);
			m_triIds.push_back(base + 7); m_triIds.push_back(base + 4); m_triIds.push_back(base + 6); // -Z
			m_triIds.push_back(base + 7); m_triIds.push_back(base + 5); m_triIds.push_back(base + 4);
			m_triIds.push_back(base + 7); m_triIds.push_back(base + 6); m_triIds.push_back(base + 2); // -Y
			m_triIds.push_back(base + 7); m_triIds.push_back(base + 2); m_triIds.push_back(base + 3);
			m_triIds.push_back(base + 7); m_triIds.push_back(base + 3); m_triIds.push_back(base + 1); // -X
			m_triIds.push_back(base + 7); m_triIds.push_back(base + 1); m_triIds.push_back(base + 5);
			//
			for(int i = 0; i < 8; i++)
				m_tris.push_back({ vec3(points[i]), color });
		}
	}

	void DebugDrawer::Circle(vec3 pos, float radius, vec4 color)
	{
		for (unsigned i = 0; i < m_cacheCircleVerts.size() - 1; i++) {
			auto& c2d = m_cacheCircleVerts[i];
			m_lines.push_back({ pos + vec3{ c2d.x * radius, 0.f, c2d.y * radius },color });
			auto& c2d2 = m_cacheCircleVerts[i + 1];
			m_lines.push_back({ pos + vec3{ c2d2.x * radius, 0.f, c2d2.y * radius } ,color });
		}
	}

	void DebugDrawer::Ring(vec3 pos, float radius, float thickness, vec4 color)
	{
		uint16_t base = (uint16_t)m_tris.size();
		uint16_t step = (uint16_t)m_cacheCircleVerts.size();
		const float radiusInner = radius - thickness;
		for (unsigned i = 0; i < m_cacheCircleVerts.size(); i++)
			m_tris.push_back({ pos + vec3{ m_cacheCircleVerts[i].x * radius, 0.f, m_cacheCircleVerts[i].y * radius }, color});
		for (unsigned i = 0; i < m_cacheCircleVerts.size(); i++)
			m_tris.push_back({ pos + vec3{ m_cacheCircleVerts[i].x * radiusInner, 0.f, m_cacheCircleVerts[i].y * radiusInner }, color });

		for (uint16_t i = 0; i < m_cacheCircleVerts.size() - 1; i++) {
			m_triIds.push_back(base + i);
			m_triIds.push_back(base + i + step);
			m_triIds.push_back(base + i + step + 1);
			m_triIds.push_back(base + i + step + 1);
			m_triIds.push_back(base + i + 1);
			m_triIds.push_back(base + i);
		}
	}


	void DebugDrawer::SubmitGizmos(Rendering::RCB& commands)
	{
		MICROPROFILE_SCOPEI("Rendering", "DebugGizmosSubmit", MP_ORANGE1);

		Rendering::VertexLayout vlayout;
		vlayout.AddStream({ Rendering::BGSS_POSITION, Rendering::BGVT_FLOAT, 3 });
		vlayout.AddStream({ Rendering::BGSS_COLOR0, Rendering::BGVT_FLOAT, 4 });

		//TODO: This copies from the vector to the buffer. It should just be able to use the vector without copy or delete
		if (!m_tris.empty())
		{
			auto vertOut = m_triVertices->SetData(commands, Rendering::BUH_DYNAMIC_DRAW, vlayout, (unsigned)m_tris.size());
			vertOut.CopyFrom(m_tris);
			auto indOut = m_triIndices->SetData(commands, Rendering::BUH_DYNAMIC_DRAW, (unsigned)m_triIds.size(), Rendering::BGVT_U16);
			indOut.CopyFrom(m_triIds);
		}
		if (!m_lines.empty())
		{ // Render lines
			auto vertOut = m_lineVertices->SetData(commands, Rendering::BUH_DYNAMIC_DRAW, vlayout, (unsigned)m_lines.size());
			vertOut.CopyFrom(m_lines);
		}
		if (!m_points.empty())
		{ // Render points
			auto vertOut = m_pointVertices->SetData(commands, Rendering::BUH_DYNAMIC_DRAW, vlayout, (unsigned)m_points.size());
			vertOut.CopyFrom(m_points);
		}
	}

	void DebugDrawer::ClearGizmos()
	{
		m_tris.clear();
		m_triIds.clear();
		m_lines.clear();
		m_points.clear();
	}

	void DebugDrawer::DrawGizmos(Rendering::Renderer& renderer, const mat4& viewMat, const mat4& projMat)
	{
		MICROPROFILE_SCOPEI("Rendering","DebugGizmos", MP_ORANGE1);
		MICROPROFILE_SCOPEGPUI("DebugGizmos", MP_ORANGE1);
	
		auto& rendererBuiltins = renderer.GetBuiltins();
		rendererBuiltins.Set("u_wvp", projMat * viewMat);
		rendererBuiltins.Set("u_w", mat4());
		if (!m_tris.empty())
		{ 
			renderer.EnableMaterial(*m_transpMat);
			renderer.Draw(*m_triPrimitives, (unsigned)m_triIds.size());
		}
		if (!m_lines.empty())
		{ // Render lines
			renderer.EnableMaterial(*m_colorMat);
			renderer.Draw(*m_linePrimitives, (unsigned)m_lines.size());
		}
		if (!m_points.empty())
		{ // Render points
			renderer.EnableMaterial(*m_colorMat);
			renderer.Draw(*m_pointPrimitives, (unsigned)m_points.size());
		}
	}
};