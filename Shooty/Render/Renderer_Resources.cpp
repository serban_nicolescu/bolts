#include "StdAfx.h"
#include "DebugDraw.h"
#include "Renderer.h"
#include "Rendering/Mesh.h"
#include "Rendering/MaterialNew.h"
#include "Assets/AssetManager.h"
#include "Helpers/MeshBuilder.h"

#include <bolts_assert.h>

namespace Bolts
{
	namespace Rendering
	{
		void Renderer::InitBuiltinResources(Bolts::assets::AssetManager& am)
		{
			// materials
			am.LoadAssetFile<Material>("standard.matpack");
			am.LoadAssetFile<Material>("blinn_phong.matpack");
			m_badMat = am.LoadAsset<Material>("BadMat");
			BASSERT(m_badMat);
			{
				MeshPtr quad = am.LoadAsset<Mesh>("FullscreenQuad");
				quad->subMeshes.push_back({
					MeshBuilder::FullscreenQuadWithUVS(*m_mainFrameCommandBuffer),
					"FullscreenQuad"
					});
			}
			// render systems
			m_shadows.InitBuiltinResources(am);
			m_renderToCubemap.InitBuiltinResources(am);
			m_postProcessing.InitBuiltinResources(am);
			// debug mats
			am.LoadAssetFile<Bolts::Rendering::Material>("widgets.matpack");
			m_debugDrawer = new DebugDrawer();
			m_debugDrawer->Init(*m_mainFrameCommandBuffer, am.LoadAsset<Bolts::Rendering::Material>("FlatVColor"), am.LoadAsset<Bolts::Rendering::Material>("FlatVColor#transp"));

			m_backend->ExecuteResourceCommands(m_mainFrameCommandBuffer);
		}
	};
};

