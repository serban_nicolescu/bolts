#include "StdAfx.h"
#include "PostProcessing.h"
#include "Renderer.h"
#include "Assets/AssetManager.h"

#include "Rendering/GPUProgram.h"
#include "Rendering/Texture.h"
#include "Rendering/MaterialNew.h"

#include "DebugDraw.h"
#include "imgui.h"
#include <glm/gtc/matrix_transform.hpp>

namespace Bolts {
	namespace Rendering {

		TexturePtr CreateTex(assets::AssetManager&	assetManager, RCB& commands, const char* name)
		{
			regHash_t texName{ name };
			auto outTex = new Texture(texName);
			outTex->Init(commands);
			assetManager.AddAsset(texName, Texture::c_assetType, outTex);
			return outTex;
		}

		resHandle_t CreateRT(RCB& commands)
		{
			auto fb = commands.CreateResource(RenderResourceType::FRAMEBUFFER);
			return fb;
		}

		void Resize(RCB& commands, RenderTarget& rt, uvec2 newRes, TextureFormat colorFmt, TextureFormat depthFmt = BTF_DEPTH16)
		{
			uvec3 res = { newRes.x * rt.sizeRatio, newRes.y * rt.sizeRatio, 1 };

			if (rt.texColor)
				rt.texColor->SetRTTexture(commands, BTT_2D, res, colorFmt);
			if (rt.texDepth)
				rt.texDepth->SetRTTexture(commands, BTT_2D, res, depthFmt);
			rt.resolution = { res.x, res.y, 1.f / res.x, 1.f / res.y };

			auto updateRTCommand = commands.AddCommand<command::UpdateRTBindings>(rt.rtHandle);
			//updateRTCommand->resolution = res; //NOTE: This is useless
			updateRTCommand->colorBindings[0] = rt.texColor->GetHandle();
			if (rt.texDepth)
				updateRTCommand->depthBinding = rt.texDepth->GetHandle();
			updateRTCommand->resolution = {res.x, res.y};
		}

		void RenderPass::Init(assets::AssetManager&	assetManager, const char* matName, uvec2 outRes, TextureFormat outFmt, const char* outName)
		{
			using namespace Rendering;
			mat = assetManager.LoadAsset<Material>(regHash_t(matName));

			auto& commands = *assetManager.GetCommandBuffer();
			regHash_t texName{ outName };
			outTex = new Texture(texName);
			outTex->Init(commands);
			outTex->SetRTTexture(commands, BTT_2D, uvec3{ outRes, 1 }, outFmt);
			assetManager.AddAsset(texName, Texture::c_assetType, outTex);
			texHandle = outTex->GetHandle();

			FB = commands.CreateResource(RenderResourceType::FRAMEBUFFER);
			auto updateRTCommand = commands.AddCommand<command::UpdateRTBindings>(FB);
			updateRTCommand->resolution = outRes;
			updateRTCommand->colorBindings[0] = texHandle;
		}

		void RenderPass::Init(assets::AssetManager& assetManager, const char * matName, uvec2 outRes, resHandle_t outT)
		{
			mat = assetManager.LoadAsset<Material>(regHash_t(matName));
			texHandle = outT;

			auto& commands = *assetManager.GetCommandBuffer();
			FB = commands.CreateResource(RenderResourceType::FRAMEBUFFER);
			auto updateRTCommand = commands.AddCommand<command::UpdateRTBindings>(FB);
			updateRTCommand->resolution = outRes;
			updateRTCommand->colorBindings[0] = outT;
		}

		void RenderPass::Render(Renderer& renderer,bool clear)
		{
			auto be = renderer.GetBackend();
			be->EnableRT(FB);
			if (clear) {
				be->SetClearColor(vec4_zero);
				be->Clear(FLAG_COLOR);
			}
			renderer.EnableMaterial(*mat);
			renderer.Draw(BGPT_TRIANGLES, 3);
			be->DisableRT();
		}

		PostProcessing::PostProcessing()
		{
		}

		PostProcessing::~PostProcessing()
		{
		}
		
		void PostProcessing::TweakSettings()
		{
			ImGui::PushID("Post Processing");
			ImGui::Text("Post Processing");
			ImGui::Checkbox("Enabled", &enabled);
			ImGui::Checkbox("TAA", &settings.enableTAA);
			if (settings.enableTAA) {
				ImGui::SameLine();
				ImGui::SliderFloat("Alpha", &settings.taaAlpha, 0.001f, 0.5f);
			}
			ImGui::Checkbox("Bloom", &settings.enableBloom);
			if (settings.enableBloom) {
				ImGui::SameLine();
				ImGui::PushItemWidth(100.f);
				ImGui::SliderFloat("Threshold", &settings.bloomThreshold, 0.f, 1.f);
				ImGui::PopItemWidth();
			}

			ImGui::PopID();
		}

		void PostProcessing::InitBuiltinResources(assets::AssetManager& assets)
		{
			assets.LoadAssetFile<Material>("post_processing.matpack");
			auto& commands = *assets.GetCommandBuffer();

			mainSceneRT[0].texColor = CreateTex(assets, commands, "MainColor1");
			mainSceneRT[0].texDepth = CreateTex(assets, commands, "MainDepth1");
			mainSceneRT[0].rtHandle = CreateRT(commands);
			mainSceneRT[1].texColor = CreateTex(assets, commands, "MainColor2");
			mainSceneRT[1].texDepth = CreateTex(assets, commands, "MainDepth2");
			mainSceneRT[1].rtHandle = CreateRT(commands);

			matFinalBlit = assets.LoadAsset<Material>(regHash_t("FinalBlit"));

			//ssao.Init(assets, "SSAOGen", maxRTResolution, TextureFormat::BTF_RGB, "SSAOOut");
			//ssao.mat = assets.LoadAsset<Material>(regHash_t("SSAOGen"));

			taa.rtSceneJittered.texColor = CreateTex(assets, commands, "MainColorJittered");
			taa.rtSceneJittered.texDepth = CreateTex(assets, commands, "MainDepthJittered");
			taa.rtSceneJittered.rtHandle = CreateRT(commands);
			taa.matAccumulate = assets.LoadAsset<Material>("TaaAccumulate");

			bloom.rt[0].sizeRatio = 0.5f;
			bloom.rt[0].texColor = CreateTex(assets, commands, "BloomRT0");
			bloom.rt[0].rtHandle = CreateRT(commands);
			bloom.rt[1].sizeRatio = 0.5f;
			bloom.rt[1].texColor = CreateTex(assets, commands, "BloomRT1");
			bloom.rt[1].rtHandle = CreateRT(commands);
			bloom.matThreshold = assets.LoadAsset<Material>("BloomThreshold");
			bloom.matBlurH = assets.LoadAsset<Material>("BloomBlurH");
			bloom.matBlurV = assets.LoadAsset<Material>("BloomBlurV");
		}

		void PostProcessing::ResizeRT(Renderer& rd)
		{
			auto& commands = rd.GetCommandBuffer();

			Resize(commands, mainSceneRT[0], maxRTResolution, BTF_RGB, BTF_DEPTH32F);
			Resize(commands, mainSceneRT[1], maxRTResolution, BTF_RGB, BTF_DEPTH32F);
			Resize(commands, taa.rtSceneJittered, maxRTResolution, BTF_RGB, BTF_DEPTH32F);
			Resize(commands, bloom.rt[0], maxRTResolution, BTF_RGB);
			Resize(commands, bloom.rt[1], maxRTResolution, BTF_RGB);
		}

		// Sub-sample positions for 8x TAA
		static const vec2 SAMPLE_LOCS_8[8] = {
			vec2(-7.0f, 1.0f) / 8.0f,
			vec2(-5.0f, -5.0f) / 8.0f,
			vec2(-1.0f, -3.0f) / 8.0f,
			vec2(3.0f, -7.0f) / 8.0f,
			vec2(5.0f, -1.0f) / 8.0f,
			vec2(7.0f, 7.0f) / 8.0f,
			vec2(1.0f, 3.0f) / 8.0f,
			vec2(-3.0f, 5.0f) / 8.0f };

		static const vec2 SAMPLE_LOCS_16[16] = {
			vec2(-8.0f, 0.0f) / 8.0f,
			vec2(-6.0f, -4.0f) / 8.0f,
			vec2(-3.0f, -2.0f) / 8.0f,
			vec2(-2.0f, -6.0f) / 8.0f,
			vec2(1.0f, -1.0f) / 8.0f,
			vec2(2.0f, -5.0f) / 8.0f,
			vec2(6.0f, -7.0f) / 8.0f,
			vec2(5.0f, -3.0f) / 8.0f,
			vec2(4.0f, 1.0f) / 8.0f,
			vec2(7.0f, 4.0f) / 8.0f,
			vec2(3.0f, 5.0f) / 8.0f,
			vec2(0.0f, 7.0f) / 8.0f,
			vec2(-1.0f, 3.0f) / 8.0f,
			vec2(-4.0f, 6.0f) / 8.0f,
			vec2(-7.0f, 8.0f) / 8.0f,
			vec2(-5.0f, 2.0f) / 8.0f };

		void PostProcessing::OnSubmit(Renderer& rd, detail::FPackage& fpack, detail::VPackage& viewPack)
		{
			//if ( glm::any(glm::greaterThan( rd.GetWindowSize(), maxRTResolution)))
			if (rd.GetWindowSize() != maxRTResolution)
			{
				//maxRTResolution = glm::max(rd.GetWindowSize(), maxRTResolution);
				maxRTResolution = rd.GetWindowSize();
				ResizeRT(rd);
				rd.FlushResourceCommands();
			}

			mainSceneResolutionPx = rd.GetWindowSize();
			mainResolution = { mainSceneResolutionPx.x, mainSceneResolutionPx.y, 1.f / mainSceneResolutionPx.x, 1.f / mainSceneResolutionPx.y };

			//jitter camera if using TAA
			if (enabled) {
				if (settings.enableTAA) {
					mainScenePrevious = mainScene;
					mainScene = (mainScene == mainSceneRT) ? &mainSceneRT[1] : &mainSceneRT[0];
					unsigned subsampleIdx = fpack.FrameNumber % 16;
					vec2 subpixelJitter = { mainResolution.z * SAMPLE_LOCS_16[subsampleIdx].x, mainResolution.w * SAMPLE_LOCS_16[subsampleIdx].y };
					subpixelJitter *= 0.5f;
					viewPack.ProjMatrix[2][0] += subpixelJitter.x;
					viewPack.ProjMatrix[2][1] += subpixelJitter.y; 
					//mat4 jitter;
					//jitter = glm::translate(jitter, vec3(subpixelJitter, 0.f));
					//viewPack.ProjMatrix = viewPack.ProjMatrix * jitter;
					rd.GetBackend()->EnableRT(taa.rtSceneJittered.rtHandle);
				}
				else {
					mainScene = &mainSceneRT[0];
					rd.GetBackend()->EnableRT(mainScene->rtHandle);
				}
				rd.GetBackend()->Clear( FLAG_COLOR_DEPTH);
			}
		}

		void PostProcessing::OnPostSubmit(Renderer& rd, detail::FPackage& /*frame*/)
		{
			if (enabled) {
				RenderState renderStateRTT; // used for RTT passes
				renderStateRTT.cullingMode = BGCM_NONE;
				renderStateRTT.isDepthTestEnabled = false;
				renderStateRTT.isDepthWriteEnabled = false;
				renderStateRTT.SetBlendMode(BGBM_NONE);
				
				if (settings.enableTAA && mainScenePrevious) {
					rd.GetBackend()->EnableRT(mainScene->rtHandle);
					rd.GetBackend()->Clear(FLAG_COLOR_DEPTH);

					taa.matAccumulate->EditParams().Set("u_alpha", settings.taaAlpha);
					taa.matAccumulate->EditParams().Set("u_currentFrame", taa.rtSceneJittered.texColor.get());
					taa.matAccumulate->EditParams().Set("u_accumulation", mainScenePrevious->texColor.get());
					taa.matAccumulate->SetRenderState(renderStateRTT);
					rd.EnableMaterial(*taa.matAccumulate);
					rd.Draw(BGPT_TRIANGLES, 3);

					static bool showBuffers = true;
					if (showBuffers) {
						ImGui::Text("MainScene");
						ImGui::Image((ImTextureID)(uintptr_t)taa.rtSceneJittered.texColor->GetHandle().d, { 256,256 });
						ImGui::Text("Accumulation");
						if (mainScene)
							ImGui::Image((ImTextureID)(uintptr_t)mainScene->texColor->GetHandle().d, { 256,256 });
					}
				}

				rd.GetBackend()->EnableRT(bloom.rt[0].rtHandle);
				rd.GetBackend()->SetClearColor(vec4_zero);
				rd.GetBackend()->Clear(FLAG_COLOR_DEPTH);
				if (settings.enableBloom) {
					bloom.matThreshold->EditParams().Set("u_threshold", settings.bloomThreshold);
					bloom.matThreshold->EditParams().Set("u_srcTex", mainScene->texColor.get());
					bloom.matThreshold->EditParams().Set("u_srcRes", mainScene->resolution);
					bloom.matThreshold->SetRenderState(renderStateRTT);
					rd.EnableMaterial(*bloom.matThreshold);
					rd.Draw(BGPT_TRIANGLES, 3);

					int numRepeats = 1;
					for (int i = numRepeats; i >= 0; i--) {
						rd.GetBackend()->EnableRT(bloom.rt[1].rtHandle);
						rd.GetBackend()->Clear(FLAG_COLOR_DEPTH);
						bloom.matBlurH->EditParams().Set("u_srcTex", bloom.rt[0].texColor.get());
						bloom.matBlurH->EditParams().Set("u_srcRes", bloom.rt[0].resolution);
						bloom.matBlurH->SetRenderState(renderStateRTT);
						rd.EnableMaterial(*bloom.matBlurH);
						rd.Draw(BGPT_TRIANGLES, 3);

						rd.GetBackend()->EnableRT(bloom.rt[0].rtHandle);
						rd.GetBackend()->Clear(FLAG_COLOR_DEPTH);
						bloom.matBlurV->EditParams().Set("u_srcTex", bloom.rt[1].texColor.get());
						bloom.matBlurV->EditParams().Set("u_srcRes", bloom.rt[1].resolution);
						bloom.matBlurV->SetRenderState(renderStateRTT);
						rd.EnableMaterial(*bloom.matBlurV);
						rd.Draw(BGPT_TRIANGLES, 3);
					}
				}
				bloom.texOut = bloom.rt[0].texColor; 
				 
				rd.GetBackend()->DisableRT();
				matFinalBlit->EditParams().Set("u_scene", mainScene->texColor.get());
				matFinalBlit->EditParams().Set("u_bloom", bloom.texOut.get());
				matFinalBlit->EditParams().Set("u_outputResolution", mainResolution);
				matFinalBlit->SetRenderState(renderStateRTT);
				rd.EnableMaterial(*matFinalBlit); 
				rd.Draw(BGPT_TRIANGLES, 3);
			}
		}

	};
};