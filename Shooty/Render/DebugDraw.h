#pragma once

#include "Rendering/RenderingCommon.h"
#include "Rendering/GPUProgramFwd.h"
#include "Rendering/MaterialNewFwd.h"
#include "Rendering/GPUBuffers.h"
#include "Rendering/MeshFwd.h"
#include "Renderer.h"
#include <bolts_core.h>
#include <vector>

namespace Bolts
{
	enum DebugDrawStyle
	{
		BDDS_WIREFRAME = 1, 
		BDDS_SOLID = 2, 
		BDDS_TRANSP = 4,
		BDDS_NODEPTH = 8
	};

	// Debug render helpers
	class DebugDrawer
	{
	public:
		DebugDrawer();
		~DebugDrawer();

		void	Init(Rendering::RCB& commands, Rendering::MaterialPtr colorMat, Rendering::MaterialPtr transpMat);

		void	Line(vec3 start, vec3 end, vec4 color);
		void	Point(vec3 pos, float size, vec4 color);
		void	OBB(vec3 extents, const mat4& transform, vec4 color, DebugDrawStyle);
		void	OBB(const vec3 points[8], vec4 color, DebugDrawStyle);

		void	Circle(vec3 pos, float radius, vec4 color);
		void	Ring(vec3 pos, float radius, float thickness, vec4 color);

		void	SubmitGizmos(Rendering::RCB& commands);
		void	DrawGizmos(Rendering::Renderer& renderer, const mat4& viewMat, const mat4& projMat);
		void	ClearGizmos();

	private:
		struct VertData {
			vec3 pos;
			vec4 color;
		};

		vec_t<VertData>					m_tris;
		vec_t<uint16_t>					m_triIds;
		Rendering::PrimitiveBufferPtr	m_triPrimitives;
		Rendering::VertexBufferPtr		m_triVertices;
		Rendering::IndexBufferPtr		m_triIndices;

		vec_t<VertData>					m_lines;
		Rendering::PrimitiveBufferPtr	m_linePrimitives;
		Rendering::VertexBufferPtr		m_lineVertices;

		vec_t<VertData>					m_points;
		Rendering::PrimitiveBufferPtr	m_pointPrimitives;
		Rendering::VertexBufferPtr		m_pointVertices;

		Rendering::MaterialPtr	m_colorMat;
		Rendering::MaterialPtr	m_transpMat;

		vec_t<vec2>		m_cacheCircleVerts;
	};

	DebugDrawer* DebugDraw();
};

