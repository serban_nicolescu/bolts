#include "StdAfx.h"
#include "DebugDraw.h"
#include "RenderToCubemap.h"
#include "Renderer.h"
#include "Assets/AssetManager.h"

#include "Rendering/GPUProgram.h"
#include "Rendering/Texture.h"
#include "Rendering/MaterialNew.h"

#include "imgui.h"

namespace Bolts {
	namespace Rendering {

		RenderToCubemapFeature::RenderToCubemapFeature()
		{
			renderCam.SetAspectRatio(1.f);
			//renderCam.SetCameraOrtho();
		}

		//TODO: Cleanup resources
		RenderToCubemapFeature::~RenderToCubemapFeature() = default;

		void RenderToCubemapFeature::TweakSettings()
		{
			ImGui::PushID("Cubemap Settings");
			ImGui::Text("Cubemap Settings");
			{
				int res = settings.resolution;
				ImGui::RadioButton("32", &settings.resolution, 32); ImGui::SameLine();
				ImGui::RadioButton("64", &settings.resolution, 64); ImGui::SameLine();
				ImGui::RadioButton("128", &settings.resolution, 128); ImGui::SameLine();
				ImGui::RadioButton("256", &settings.resolution, 256); ImGui::SameLine();
				ImGui::RadioButton("512", &settings.resolution, 512);
				if (res != settings.resolution)
					settings.resChanged = true;
				ImGui::SameLine(); ImGui::Text("Resolution");
			}

			ImGui::SliderFloat("Near Plane", &settings.nearPlane, 0.001f, 1.0f);
			ImGui::SliderFloat("Far Plane", &settings.farPlane, 1.1f, 1000.0f);
			ImGui::Checkbox("Generate mips", &settings.generateMips);
			ImGui::Checkbox("Always render", &settings.alwaysRender);
			
			ImGui::PopID();
		}

		void RenderToCubemapFeature::InitBuiltinResources(Bolts::assets::AssetManager& am)
		{
			RCB& commands = *am.GetCommandBuffer();
			
			regHash_t texName{ "RTCubemap" };
			cubemapTex = new Texture(texName);
			cubemapTex->Init(commands);
			am.AddAsset(texName, Texture::c_assetType, cubemapTex);

			depthTex = commands.CreateResource(RenderResourceType::IMAGE_BUFFER);
			//
			Resize(commands);
			//
			for (int i = 0; i < 6; i++)
			{
				faceFB[i] = commands.CreateResource(RenderResourceType::FRAMEBUFFER);
				auto updateRTCommand = commands.AddCommand<command::UpdateRTBindings>(faceFB[i]);
				updateRTCommand->resolution = uvec2(settings.resolution, settings.resolution);
				updateRTCommand->depthBinding = depthTex;
				updateRTCommand->colorBindings[0] = cubemapTex->GetHandle();
				updateRTCommand->boundTarget[0] = (uint8_t)i;
			}
		}

		void RenderToCubemapFeature::Resize(ResourceCommandBuffer& commands)
		{
			const uvec3 texDim = uvec3(settings.resolution, settings.resolution, 1);
			// textures
			cubemapTex->SetRTTexture(commands, BTT_CUBE, texDim, BTF_RGB);

			auto depthResizeCmd = commands.AddCommand<command::UpdateTextureData>(depthTex);
			depthResizeCmd->type = BTT_2D;
			depthResizeCmd->format = BTF_DEPTH16;
			depthResizeCmd->dimensions = texDim;
			// framebuffers
			for (int i = 0; i < 6; i++)
			{
				if (!faceFB[i].IsValid())
					continue;
				auto resizeCmd = commands.AddCommand<command::ResizeRT>(faceFB[i]);
				resizeCmd->newSize = uvec2(texDim);
			}
			settings.resChanged = false;
		}

		void RenderToCubemapFeature::PrepareView(Renderer& rd, detail::FPackage& , detail::VPackage& vpack, int faceIdx)
		{
			if (settings.resChanged) {
				Resize( rd.GetCommandBuffer() );
			}
			hasMips = false;
			// place virtual camera
			const vec3 viewDirs[6] = {
				{ 1.f, 0.f, 0.f},
				{ -1.f, 0.f, 0.f},
				{ 0.f, 1.f, 0.f},
				{ 0.f, -1.f, 0.f},
				{ 0.f, 0.f, 1.f},
				{ 0.f, 0.f, -1.f},
			};
			const vec3 upDirs[6] = {
				{ 0.f, -1.f, 0.f},
				{ 0.f, -1.f, 0.f},
				{ 0.f, 0.f, 1.f},
				{ 0.f, 0.f, -1.f},
				{ 0.f, -1.f, 0.f},
				{ 0.f, -1.f, 0.f},
			};
			renderCam.SetPosition(settings.position);
			renderCam.SetTarget(settings.position + viewDirs[faceIdx]);
			renderCam.SetUpVector(upDirs[faceIdx]); //NOTE: All up directions are inverted so rendering matches sampling coords
			renderCam.SetFov(90.f);
			renderCam.SetZNear(settings.nearPlane);
			renderCam.SetZFar(settings.farPlane);
			//
			vpack.SetCameraData(&renderCam);
			vpack.VOMask = REFLECTIONS_VIEW;
		}

		void RenderToCubemapFeature::OnSubmit(Renderer& rd, int faceIdx)
		{
			auto backend = rd.GetBackend();
			backend->EnableRT( faceFB[faceIdx]);
			backend->SetViewport({}, { settings.resolution, settings.resolution });
			backend->Clear( ClearFlags(FLAG_COLOR | FLAG_DEPTH));
		}

		void RenderToCubemapFeature::OnPostSubmit(Renderer& rd, int)
		{
			auto backend = rd.GetBackend();
			backend->DisableRT();
		}

		void RenderToCubemapFeature::OnMainSubmit(Renderer& rd, detail::FPackage&)
		{
			if (settings.generateMips && !hasMips) {
				cubemapTex->GenerateMipmaps(rd.GetCommandBuffer());
				//rd.GetCommandBuffer().AddCommand<command::GenerateTextureMips>(cubemapTex);
				hasMips = true;
			}
		}

	};
};