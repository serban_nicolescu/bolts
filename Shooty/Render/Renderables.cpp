#include "StdAfx.h"
#include "Renderer.h"
#include "Rendering/Mesh.h"
#include "Rendering/MaterialNew.h"
#include <bolts_assert.h>
#include <bolts_helpers.h>

namespace Bolts
{

	namespace Rendering
	{
		namespace {

			 // Renderables

			const uint32_t k_renderableIndexMask = 0x00FFFFFF;

			BOLTS_INLINE detail::RenderableType GetType(renderableHandle_t h) {
				return (detail::RenderableType) (h >> 24);
			}

			BOLTS_INLINE renderableHandle_t GetIdx(renderableHandle_t h) {
				return h & k_renderableIndexMask;
			}

			BOLTS_INLINE renderableHandle_t MakeHandle(uint32_t idx, detail::RenderableType t)
			{
				return (idx & k_renderableIndexMask) + (t << 24);
			}

			// culling & visibility
			static void GetVisibleVOs(const detail::VPackage& view, voListMask_t listMask, const detail::VisibilitySet&  vos, detail::visResults_t& outResults)
			{
				auto camVPMatrix = view.ProjMatrix * view.ViewMatrix;
				vec4 frustumPlanes[6];
				Camera::ExtractFrustumPlanes(camVPMatrix, frustumPlanes);

				const voListMask_t* visMasks = vos.voListMasks.data();
				const vec4* visSpheres = vos.visibilitySpheres.data();
				const size_t numV = vos.SizeActive();
				const vec3 camPos = view.CameraWorldPos;
				for (unsigned vIdx = 0; vIdx < numV; vIdx++) {
					if ((visMasks[vIdx] & listMask) == 0)
						continue;

					vec4 sphere = visSpheres[vIdx];
					char hitMask = 0;
					for (int i = 0; i < 6; i++)
					{
						float signedDist = glm::dot(vec3(frustumPlanes[i]), vec3(sphere)) + frustumPlanes[i].w;
						if (signedDist <= sphere.w) // If sphere intersects or is behind
							hitMask |= 1 << i;
					}
					if (hitMask == 0x3F) // Sphere is inside frustum
					{
						float distSqToCam = glm::length2(vec3(sphere) - camPos);
						outResults.emplace_back(detail::VisibilityTestResult{vIdx, distSqToCam});
					}
				}
			}
		};

		void detail::RenderableBase::SetWorldTransform(const mat4 &m)
		{
			m_worldTransform = m;
		}

		void detail::VPackage::Clear()
		{
			for(auto& rids : VisibleRenderables)
				rids.clear();
			NumVisibleRenderables = 0;
			for (auto& list : RenderLists)
				list.clear();
		}

		void detail::VPackage::SetCameraData(const Camera* cam)
		{
			ActiveCamera = cam;
			ViewMatrix = cam->GetViewMatrix();
			ProjMatrix = cam->GetProjectionMatrix();
			CameraWorldPos = cam->GetPosition();
		}

		void detail::FPackage::Clear()
		{
			MainView.Clear();
			ShadowsView.Clear();
			RenderToCubeView.Clear();
		}

		voHandle_t Renderer::AddVisibilityObject()
		{
			return m_vos.AddActive();
		}

		void Renderer::RemoveVisibilityObject(voHandle_t voID)
		{
			m_vos.Remove(voID);
		}

		void Renderer::RegisterVisibilityObject(voHandle_t voID)
		{
			m_vos.Activate(voID);
		}

		void Renderer::UnregisterVisibilityObject(voHandle_t voID)
		{
			m_vos.Deactivate(voID);
		}

		void Renderer::SetVOData(voHandle_t vID, const mat4 &w, float radius)
		{
			auto vidx = m_vos.GetIdx(vID);
			vec4 sphere = w[3];
			sphere.w = radius;
			m_vos.visibilitySpheres[vidx] = sphere;
		}

		void Renderer::SetVOTransformAndRadius(voHandle_t vID, const mat4& worldTransform, float radius)
		{
			auto vidx = m_vos.GetIdx(vID);
			vec4 newSphereData = worldTransform[3]; // Extract world position
			newSphereData.w = radius;
			m_vos.visibilitySpheres[vidx] = newSphereData;
		}

		void Renderer::SetVODebugString(voHandle_t vID, regHash_t h)
		{
			auto vidx = m_vos.GetIdx(vID);

			m_vos.voDebugString[vidx] = h;
		}

		/*void Renderer::SetVOTransform(voHandle_t vID, const mat4 &w)
		{
			auto vidx = m_vos.GetIdx(vID);
			auto& vo = m_vos.visibilityObjects[vidx];
			vo.SetWorldTransform(w);
			auto spherePtr = &m_vos.visibilitySpheres[vidx];
			float oldRadius = spherePtr->w;
			vec4 newSphereData = w[3];
			newSphereData.w = oldRadius;
			*spherePtr = newSphereData;
		}

		void Renderer::SetVORadius(voHandle_t vID, float radius)
		{
			auto vidx = m_vos.GetIdx(vID);
			m_vos.visibilitySpheres[vidx].w = radius;
		}*/

		void Renderer::SetVOLists(voHandle_t vID, voListMask_t newMask)
		{
			auto vidx = m_vos.GetIdx(vID);
			m_vos.voListMasks[vidx] = newMask;
		}

		void Renderer::RemoveRenderable(renderableHandle_t rid)
		{
			//TODO: Should unregister it from VOs
			switch (GetType(rid)) {
			case detail::STATIC_MESH:
				m_staticMeshes.Remove( GetIdx(rid));
				break;
			case detail::LIGHT:
				m_lights.Remove( GetIdx(rid));
				break;
			default:
				BASSERT(false);
			}
		}

		StaticMeshRenderable& Renderer::AddStaticMesh()
		{
			auto rid = m_staticMeshes.Add();
			StaticMeshRenderable& r = m_staticMeshes.Get(rid);
			r.m_id = MakeHandle(rid, detail::STATIC_MESH);
			return r;
		}
		StaticMeshRenderable& Renderer::GetStaticMesh(renderableHandle_t rid)
		{
			return m_staticMeshes.Get(GetIdx(rid));
		}

		LightRenderable& Renderer::AddLight()
		{
			auto rid = m_lights.Add();
			LightRenderable& r = m_lights.Get(rid);
			r.m_id = MakeHandle(rid, detail::LIGHT);
			return r;
		}

		LightRenderable& Renderer::GetLight(renderableHandle_t rid)
		{
			return m_lights.Get(GetIdx(rid));
		}

		detail::RenderableBase& Renderer::GetRenderableAny(renderableHandle_t rid) {
			switch (GetType(rid)) {
			case detail::STATIC_MESH:
				return GetStaticMesh(rid);
			case detail::LIGHT:
				return GetLight(rid);
			default:
				BASSERT(false);
				exit(0);
			}
		}

		//TODO: HOWTO attach a VO to multiple renderables ?
		void Renderer::AttachRenderable(voHandle_t vid, renderableHandle_t rid)
		{
			auto vidx = m_vos.GetIdx(vid);
			m_vos.voAttachments[vidx] = rid;
		}

		void Renderer::DetachRenderable(voHandle_t vid, renderableHandle_t)
		{
			auto vidx = m_vos.GetIdx(vid);
			m_vos.voAttachments[vidx] = 0;
		}

		void Renderer::CullVOS(detail::VPackage& view)
		{
			PROFILE_SCOPE("CullVOS");
			// Test all registered VOs for visibility
			view.VisibilityResultCache.clear();
			GetVisibleVOs(view, view.VOMask, m_vos, view.VisibilityResultCache);
			// Gather renderables from visible VOs
			BASSERT_ONLY(size_t maxVIdx = m_vos.voAttachments.size());
			auto voAttachments = m_vos.voAttachments.data();
			const int newVisibleRenderables = (int)view.VisibilityResultCache.size();
			view.NumVisibleRenderables += newVisibleRenderables;
			auto visibilityResults = view.VisibilityResultCache.data();
			for (int i = 0; i < newVisibleRenderables; i++) {
				detail::VisibilityTestResult& res = visibilityResults[i];
				BASSERT(res.vIdx < maxVIdx);
				renderableHandle_t rId = voAttachments[res.vIdx];
				view.VisibleRenderables[GetType(rId)].push_back( GetIdx(rId));
			}
		}

		//

		StaticMeshRenderable::StaticMeshRenderable() {}
		StaticMeshRenderable::~StaticMeshRenderable() {}

		void StaticMeshRenderable::OnVisibleInFrame(Renderer &, const detail::FPackage &, uint32_t frIdx)
		{
			frIdx;
		}

		void StaticMeshRenderable::OnVisibleInView(Renderer &, const detail::FPackage &, const detail::VPackage &, uint32_t vrIdx)
		{
			vrIdx;
		}

		void StaticMeshRenderable::SubmitMany(const vec_t<renderableHandle_t>& toSubmit, Renderer& renderer, const detail::FPackage& fpack, const detail::VPackage& vpack, const detail::RenderContext& ctx)
		{
			for (renderableHandle_t rid : toSubmit) {
				renderer.GetStaticMesh(rid).Submit(renderer, fpack, vpack, ctx);
			}
		}

		void StaticMeshRenderable::Submit(Renderer& renderer, const detail::FPackage& fpack, const detail::VPackage& /*vpack*/, const detail::RenderContext& ctx)
		{
			if (!m_mesh->IsValid() || m_materials.empty())
				return;
				
			//TODO: PERF drop the builtins intermediary, and just set values in the cbuffer
			auto& rendererBuiltins = renderer.GetBuiltins();

			for (auto& subMesh : m_mesh->subMeshes) {
				//instance matrices
				mat4 worldMatrix(glm::uninitialize);
				if (m_mesh->nodes)
					worldMatrix = GetWorldTransform() *m_mesh->nodes->transforms[0][subMesh.nodeIdx];
				else
					worldMatrix = GetWorldTransform();
				rendererBuiltins.Set("u_w", worldMatrix);
				//TODO: Remove this shadow hack
				if (!ctx.drawingShadows) {
					int matIdx = subMesh.matIdx < m_materials.size() ? subMesh.matIdx : 0;
					const Material* mat = m_materials[matIdx].get();
					BASSERT(mat);
					if (!mat || !mat->IsValid())
						continue;
					renderer.EnableMaterial(*mat);
				} else {
					//TODO: Select correct shadow shader based on material type
					renderer.QuickSubmitBuiltinUniforms(*fpack.ShadowDrawSolidsMaterial);
				}
				renderer.Draw(*subMesh.primitiveBuffer);
			}
		}

};
};
