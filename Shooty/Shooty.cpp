#include "StdAfx.h"
#include <System/system_win32.h>
#include <IO/FileProviderImpl.h>
#include <Rendering/GL/GL35Backend.h>
#include <chrono>

#include "Menus/EditorState.h"

#include <fstream>
#include <iostream>

#include <Assets/AssetManager.h>
#include <Assets/Loaders_Builtin.h>

#include <Helpers/imgui_impl_bolts.h>
#include "ImGuizmo/ImGuizmo.h"
#include <imgui.h>

#include <Profiler.h>
#include <microprofile.h>

#include "Screept.h"
#include "ImGuiGraph.h"

using namespace std;
using namespace Bolts;
using namespace Bolts::System;

IMGRAPH(nodes1, 3, 6);
IMGRAPH(nodes2, 1, 6);

class DemoGame: 
	public Bolts::Input::iWindowEventListener,
	public Bolts::Input::iMouseEventListener,
	public Bolts::Input::iKeyEventListener,
	public Bolts::Input::iCharEventListener
{
public:
	~DemoGame(){
		
		if (m_state)
			m_state->OnDestroy();
		delete m_state;
		
		ImGui_Shutdown(m_renderer.GetCommandBuffer());

		delete m_renderBackend;

		Win32System::Get().UnregisterWindowEventListener ( this);
		Win32System::Get().UnregisterMouseEventListener ( this);
		Win32System::Get().UnregisterKeyEventListener ( this);

		Bolts::Profiler::Shutdown();
	}

	void init(){
		MicroProfileOnThreadCreate("Main");
		MicroProfileSetEnableAllGroups(true);
		MicroProfileSetForceMetaCounters(true);

		Win32System::Get().InitApp();
		m_window1 = Win32System::Get().NewWindow("Shooty", 600, 600, System::Window::S_TITLE | System::Window::S_RESIZEABLE | System::Window::S_MAXIMIZE | System::Window::S_CLOSE);
		//m_window2 = m_System.NewWindow( "Demo Application2", 400, 400, System::Window::S_TITLE | System::Window::S_CLOSE | System::Window::S_RESIZEABLE);

		Win32System::Get().RegisterWindowEventListener ( this);
		Win32System::Get().RegisterMouseEventListener ( this);
		Win32System::Get().RegisterKeyEventListener ( this);
		Win32System::Get().RegisterCharEventListener(this);

		m_renderBackend = new Rendering::BackendOpenGL35();
		m_renderBackend->Init();
		m_renderer.InitRenderer(m_renderBackend);
		m_assetManager.SetCurrentCommandBuffer(m_renderBackend->CreateCommandBuffer());
		Bolts::RegisterBuiltinAssets(m_assetManager);// register asset loaders
		m_renderer.InitBuiltinResources(m_assetManager);

		Bolts::Profiler::Init(m_renderBackend);

		// init UI
		ImGui_Init( (void*)m_window1, m_renderer.GetCommandBuffer());

		imgraph_init(&nodes1);
		imgraph_insert(&nodes1, 0.f, &vec3_zero.x);
		vec3 v{0.25f, 0.5f, 0.75f};
		imgraph_insert(&nodes1, 0.5f, &v.x);
		imgraph_insert(&nodes1, 1.f, &vec3_one.x);
		imgraph_init(&nodes2);

		// Start editor
		m_state = new EditorState(m_window1, m_renderer, m_assetManager);
		m_state->OnRegister();
		/*{
			GameStatePtr_t statePtr( new BasicGS< GameplaySceneNewComp>( m_window1, m_renderer, m_resourceSystem ) );
			m_GameStates.Push( std::move(statePtr));
		}*/

	}

	bool update()
	{
		MICROPROFILE_SCOPEI("Main", "Frame", MP_YELLOW);

		Bolts::Profiler::Get().FrameStart();

		//static chrono::system_clock::time_point lastFrame = chrono::system_clock::now();
		//chrono::duration<double> dtSec = chrono::system_clock::now() - lastFrame;
		//lastFrame = chrono::system_clock::now();
		//m_deltaSecs = (float) (dtSec.count());
		static uint64_t lastFrameMicroSecs = Bolts::System::GetElapsedMicroseconds();
		uint64_t dtMicro = Bolts::System::GetElapsedMicroseconds() - lastFrameMicroSecs;
		lastFrameMicroSecs = Bolts::System::GetElapsedMicroseconds();
		m_deltaSecs = dtMicro * 0.000001f;

		Win32System::Get().Update( m_window1 );//Events will be triggered now

		char title[255];
		bool running = !m_bExiting;
		if (running)
		{
			snprintf(title, sizeof(title), "Shooty - FPS: %.2f - MS: %.2f", 1.0f / m_deltaSecs, m_deltaSecs * 1000.f);
			Win32System::Get().SetWindowTitle(m_window1, title);

			IO::CheckForSourceChanges();

			auto size = Win32System::Get().GetWindowSize(m_window1); 
			ImGui_NewFrame(m_deltaSecs, m_window1, size.x, size.y, size.x, size.y);
			ImGuizmo::BeginFrame();

			extern void ScreeptDemo();
			ScreeptDemo();

			imgraph_edit("Graph1", &nodes1, 0.25f, 0.75f);
			imgraph_edit("Graph2", &nodes2, 0.0f, 1.f, 0.f, 1.f);
			float val = 0.f;
			ImGui::DragFloat("Bla Test", &val);

			running = m_state->OnUpdate(m_deltaSecs);

			//Captures Events from the specified windows
			//m_System.Update( m_window2 );
			//{
			//	bool show_test_window = true;
			//	bool show_another_window = false;
			//	ImVec4 clear_color = ImColor( 114, 144, 154 );
			//	static float f = 0.0f;
			//	ImGui::Text( "Hello, world!" );
			//	ImGui::SliderFloat( "float", &f, 0.0f, 1.0f );
			//	ImGui::ColorEdit3( "clear color", (float*) &clear_color );
			//	if( ImGui::Button( "Test Window" ) ) show_test_window ^= 1;
			//	if( ImGui::Button( "Another Window" ) ) show_another_window ^= 1;
			//	ImGui::Text( "Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate );
			//}
			{
				PROFILE_SCOPE("Final Render Command Flush");
				//TODO: Hack. We shouldn't have to manually execute, but its important to be done here, so it's on the same frame as submission
				m_renderer.GetBackend()->ExecuteResourceCommands(m_assetManager.GetCommandBuffer());
				m_renderer.FlushResourceCommands();
			}

			// frame stats
			{
				if (ImGui::Begin("Gfx Stats")) {
					auto& fs = m_renderer.GetFrameStats();
					ImGui::Text("Culled:        %d", fs.numCulled);
					ImGui::Text("Visible:       %d", fs.numVisible);
					ImGui::Text("Submits:       %d", fs.numSubmits);
					ImGui::Text("Draw Commands: %d", fs.numDraws);
					ImGui::Text("Draw Batches:  %d", fs.numBatches);
					ImGui::Text("Primitives:    %d", fs.numPrimitives);
				}
				ImGui::End();
				if (ImGui::Begin("File Stats")) {
					auto& fs = IO::GetFrameStats();
					auto& fsMax = IO::GetFrameMaxStats();
					ImGui::Text("Reads:         %d %d", fs.numReads, fsMax.numReads);
					ImGui::Text("Disk hits:     %d %d", fs.numDiskReads, fsMax.numDiskReads);
					ImGui::Text("Frame reads:   %d %d", fs.numFrameBlobs, fsMax.numFrameBlobs);
					ImGui::Text("Frame bytes:   %d %d", fs.frameBlobMemory, fsMax.frameBlobMemory);
					ImGui::Text("Perm blobs:    %d", fs.numForeverBlobs);
					ImGui::Text("Perm bytes     %d", fs.foreverBlobMemory);
				}
				ImGui::End();
			}

			{
				PROFILE_SCOPE("ImGui Render");
				ImGui::Render();
				ImGui_RenderDrawData(*m_renderBackend);
			}
			//chrono::duration<double> actualDuration = chrono::system_clock::now() - lastFrame;
			//if (actualDuration.count() < 1.0 / 60.0) {
			//	PROFILE_SCOPE("FPS Limit Sleep");
			//	long long sleepMs = long long(floor(1000.0 * ((1.0 / 60.0) - actualDuration.count()))) - 1;
			//	std::this_thread::sleep_for( std::chrono::milliseconds( sleepMs));
			//}
			const uint64_t frameMicroS = 1e6 / 60.0;
			int64_t remainingMicroS = (frameMicroS - (Bolts::System::GetElapsedMicroseconds() - lastFrameMicroSecs)) - 7000;
			if (remainingMicroS > 1000) {
				PROFILE_SCOPE("FPS Limit Sleep");
				Bolts::System::SleepMicroseconds(remainingMicroS);
				//std::this_thread::sleep_for( std::chrono::milliseconds(remainingMs));
			}

			{
				PROFILE_SCOPE("Swap");
				Win32System::Get().SwapWindowBuffers();
				m_renderBackend->SetClearColor(Bolts::vec4(0.7f, 0.9f, 0.9f, 1.0f));
				m_renderBackend->Clear();
				m_renderBackend->SetClearColor(vec4_zero);
			}
		}

		Bolts::Profiler::Get().FrameEnd();

		if (!running) {
			m_renderBackend->ExecuteResourceCommands(m_assetManager.GetCommandBuffer());
			m_renderer.FlushResourceCommands();
		}

		Bolts::IO::FrameEnd();
		MicroProfileFlip(nullptr);

		return running;
	}

	void OnWindowEvent (Input::windowEvent_t event) override {
		//BOLTS_LDEBUG() << "[WindowEvent] " << event.type << " Values: " <<event.value1 << "x" << event.value2; 

		switch ( event.type){
		case Bolts::Input::WE_CLOSED:
			Win32System::Get().CloseWindow ( event.windowHandle );
			m_bExiting = true;
			break;
		case Bolts::Input::WE_RESIZED:
			break;
		}
	}

	bool OnMouseEvent (Input::mouseEvent_t event) override {
		return ImGui_MouseEvent(event);
	}

	bool OnKeyEvent (Input::keyEvent_t event) override {
		return ImGui_KeyEvent(event);
	}

	bool OnCharEvent(unsigned int c){
		return ImGui_CharCallback(c);
	}
private:
	//std::unique_ptr<EditorState>	m_state = nullptr;
	EditorState*					m_state = nullptr;
	//GameStateStack				m_GameStates;
	//InputManager					m_InputManager;	
	System::windowHandle_t			m_window1 = {};
	System::windowHandle_t			m_window2 = {};
	bool							m_bExiting = false;
	float							m_deltaSecs = 0.f;

	Rendering::RenderBackend*		m_renderBackend = nullptr;
	Rendering::Renderer				m_renderer;
	Bolts::assets::AssetManager		m_assetManager;
};

int main()//(int argc, char* argv[])
{
	// Nothing should happen here
	BASSERT_MSG(false, "Assertion is not hit");
	BASSERT_MSG(true, "Assertion is ignored");
	BASSERT(false);
	BASSERT(true);
	// Setup assertion handlers
	Bolts::Core::assert::AddAssertHandler(Bolts::Core::assert::CoutAH);
	Bolts::Core::assert::AddAssertHandler(Bolts::Core::assert::BreakpointAH);
	//
	Bolts::IO::Init();
	Bolts::System::OnAppStart();

	// Register data folder ( CWD )
	IO::GetFSSource().RegisterDir("./");

	extern void ScreeptTest();
	ScreeptTest();

#if 1
	DemoGame game;
	game.init();
	
	while ( game.update() );
#endif

	return 0;
}
