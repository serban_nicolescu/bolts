#pragma once
#include "Assets/Assets.h"


namespace Bolts {
	namespace Editor {

		class AssetsWindow
		{
		public:
			AssetsWindow(Bolts::AssetManager& am);
			~AssetsWindow();

			void DrawContent();
		private:
			Bolts::AssetManager& assetManager;

			char		nameFilter[25];
			uint32_t	typeFilter = 0;
		};

	};
};
