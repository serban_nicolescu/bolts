#pragma once

#include "Assets/Assets.h"

namespace Bolts
{
	namespace Editor {

		class AssetDetailsWindow
		{
		public:
			typedef assets::assetMixin::drawFunc_t drawFunc_t;

			void		DrawContent();
			regHash_t	GetAssetID() { return targetID; }
			void		SetAsset(assets::assetMixin* assetPtr, drawFunc_t* df)
			{
				if (!assetPtr) {
					targetAsset = nullptr;
					targetID = 0;
					drawFunc = nullptr;
				}
				else {
					targetAsset = assetPtr;
					targetID = assetPtr->GetAssetID();
					drawFunc = df;
				}
			}
			
		private:
			assets::assetPtr_t	targetAsset;
			regHash_t			targetID;
			drawFunc_t*			drawFunc = nullptr;
		};

	};
};