#include "stdafx.h"
#include "AssetsWindow.h"

#include "GUI/Properties.h"
#include <Assets/AssetManager.h>
#include <System/system.h>
#include <IO/FileIO.h>
#include <bolts_binarymap.h>
#include <imgui.h>

namespace Bolts {
	namespace Editor {

		AssetsWindow::AssetsWindow(AssetManager& am) :assetManager(am)
		{
		}

		AssetsWindow::~AssetsWindow()
		{
		}

		static Bolts::str_t s_lastUsedPath;

		static void LoadAssetFromFile(AssetManager& am, uint32_t assetType)
		{
			auto newPath = System::OpenFileDialog(s_lastUsedPath.c_str());
			if (newPath.empty())
				return;

			const char* filename = IO::filenameFromPath(newPath.c_str(), newPath.length());
			am.LoadAssetByType( regHash_t(filename), assetType);

			s_lastUsedPath = newPath;
		}

		void AssetsWindow::DrawContent()
		{
			static ImGuiTextFilter filter;
			//ImGui::Text("Filter usage:\n"
			//	"  \"\"         display all lines\n"
			//	"  \"xxx\"      display lines containing \"xxx\"\n"
			//	"  \"xxx,yyy\"  display lines containing \"xxx\" or \"yyy\"\n"
			//	"  \"-xxx\"     hide lines containing \"xxx\"");
			filter.Draw();
			ImGui::Separator();
			ImGui::Separator();

			static const char* s_assetNames[] = {
				"Animation Set",
				"Texture",
				"Mesh",
				"Material",
				"GPUProgram"
			};

			for (uint32_t assetType = 0; assetType < AssetManager::c_maxAssetTypeId; assetType++)
			{
				auto& assetMap = assetManager.GetLoadedAssets(assetType);
				if (assetMap.empty())
					continue;
				ImGui::PushID(assetType);
				
				uint32_t typeMask = 1 << assetType;
				ImGui::Text(s_assetNames[assetType]);
				if (ImGui::IsItemClicked())
					typeFilter ^= typeMask;

				ImGui::SameLine();
				if (ImGui::Button("O"))
					LoadAssetFromFile(assetManager, assetType);

				ImGui::Separator();
				if (typeFilter & typeMask) {
					ImGui::AlignTextToFramePadding();
					ImGui::Columns(3);
					ImGui::SetColumnWidth(0, 20);
					ImGui::SetColumnOffset(2, ImGui::GetWindowContentRegionMax().x - 45); //align to right

					for (auto& assetPair : assetMap) {
						auto& assetPtr = assetPair.second;
						bool hasName = assetPtr->GetAssetName() != nullptr;
						// filter out
						if (hasName && !filter.PassFilter(assetPtr->GetAssetName()))
							continue;

						ImGui::PushID(assetPair.first);
						// ref count
						ImGui::AlignTextToFramePadding();
						ImGui::Text("%d", assetPtr->GetRefCount()); ImGui::NextColumn(); 
						// name
						ImGui::AlignTextToFramePadding();
						if(hasName)
							ImGui::Text("%s", assetPtr->GetAssetName()); 
						else 
							ImGui::Text("%d", assetPtr->GetAssetID());
						ImGui::NextColumn();
						// buttons
						if (hasName && ImGui::Button("C"))
							ImGui::SetClipboardText(assetPtr->GetAssetName());
						ImGui::SameLine();
						if (ImGui::Button("D"))
							OpenAssetDetails(assetType, assetPtr->GetAssetID());
						ImGui::NextColumn();

						ImGui::PopID();
					}
					ImGui::Columns();
					ImGui::Separator();
				}
				ImGui::PopID();
			}

			//Combo buttons for asset type filter
			//get name & color from loader
			//List loaded assets by type, with copy-name & open details buttons
		}

	};
};