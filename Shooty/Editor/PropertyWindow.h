#pragma once

#include "GUI/Properties.h"
#include <bolts_binarymap.h>

class IProp;

class PropertyWindow
{
public:

	PropertyWindow();
	~PropertyWindow();

	bool Update();
	Bolts::BinaryMapView ViewData();
	void SetProps( propVec_t&& );
	const propVec_t& GetProps() const { return m_props; }

private:
	void NewPropWindow();

	Bolts::BinaryMap		m_data;
	Bolts::BinaryMapBuilder	m_builder;
	propVec_t				m_props;

	bool m_showNewPropPanel = false;

};