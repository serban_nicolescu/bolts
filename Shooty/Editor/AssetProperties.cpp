#include "StdAfx.h"
#include "Rendering/MaterialNew.h"
#include "Rendering/GPUProgram.h"
#include "Rendering/Mesh.h"
#include "Rendering/Texture.h"
#include "Rendering/AnimationSet.h"
#include "GUI/Properties.h"
#include <imgui.h>

namespace Bolts {
	namespace Rendering {

		char g_tempString[256];

		static void ImGuiTexPreview(Texture* tex) {
			if (!tex) {
				ImGui::TextDisabled("None");
				return;
			}

			ImGuiImage( tex->GetHandle().d, (int)ImGui::GetTextLineHeightWithSpacing(), (int)ImGui::GetTextLineHeightWithSpacing());
			if (ImGui::IsItemHovered()) {
				ImGui::BeginTooltip();

				uvec3 dims = tex->GetDimensions();
				TextureType texType = tex->GetType();
				ImGui::Text("%s [%dx%d %s] %s %s", tex->GetAssetName(), dims.x, dims.y, c_texFormatNames[int(tex->GetFormat())], c_texTypeNames[int(texType)], tex->HasMipmaps() ? "with mips" : "no mips");
				ImGui::TextDisabled("%.2f MB", float(tex->GetMemoryUsageBytes()) / (1024.f * 1024.f));
				//if(ImGui::IsItemHovered())
				ImGui::Image((ImTextureID)tex->GetHandle().d, { float(dims.x), float(dims.y)});

				ImGui::EndTooltip();
			}
		}

		static regHash_t ImGuiTexEdit(const char* label, Texture* tex) {
			regHash_t texID = tex ? tex->GetAssetID() : regHash_t();
			ImGuiEdit(label, &texID);
			ImGui::SameLine();

			ImGuiTexPreview(tex);
			
			return texID;
		}

		bool GPUProgram::DrawProperties() {
			bool anyChanges = false;

			for (int i = 0; i < c_maxNumUniformBuffers; i++)
			{
				if (!cbuffers[i].IsUsed())
					continue;
				auto& cbuffer = cbuffers[i];
				ImGui::Text("Buffer #%d - Uniforms: %d", i, cbuffer.numEntries);
				if ( auto name = GetHashString(cbuffer.bufferName)) {
					ImGui::Text("Name: %s", name);
				}
				ImGui::Separator();
			}

			return anyChanges;
		}

		bool Material::DrawProperties() {
			//regHash_t nameCpy = m_assetID;
			//sprintf(g_tempString, "%X", nameCpy.h);
			//const char* assetName = nameCpy.s ? nameCpy.s : g_tempString;
			//ImGuiDisplay(assetName);
			bool anyChanges = false;

			auto program = GetProgram();
			if (program)
			{
				ImGui::AlignTextToFramePadding();
				ImGui::Text("Shader: ");
				ImGui::SameLine();
				regHash_t name = program->GetAssetID();
				if (program->GetAssetName())
					ImGuiEdit(program->GetAssetName(), &name);
				else
					ImGuiEdit("???", &name);
				ImGui::SameLine();
				ImGui::PushID(this);
				if (ImGui::Button("<"))
					Bolts::OpenAssetDetails(GPUProgram::c_assetType, name.h);
				ImGui::PopID();
			}

			for (auto& propInfo : m_parameters.GetInfo())
			{
				regHash_t propNameHash = propInfo.key;
				sprintf(g_tempString, "%X", propNameHash.h);
				const char* propName = propNameHash.s ? propNameHash.s : g_tempString;
				switch (propInfo.type)
				{
				case SPT_FLOAT:
					anyChanges &= ImGuiEdit(propName, (float*)m_parameters.EditDataUnsafe(propInfo.dataOffset), 0.f, 1.f);
					break;
				case SPT_VEC2:
					anyChanges &= ImGuiEdit(propName, (vec2*)m_parameters.EditDataUnsafe(propInfo.dataOffset));
					break;
				case SPT_VEC3:
					anyChanges &= ImGuiEdit(propName, (vec3*)m_parameters.EditDataUnsafe(propInfo.dataOffset));
					break;
				case SPT_VEC4:
					anyChanges &= ImGuiEdit(propName, (vec4*)m_parameters.EditDataUnsafe(propInfo.dataOffset));
					break;
				case SPT_TEX:
				{
					auto pTexData = (MatParameterHolder::TextureParam*)m_parameters.EditDataUnsafe(propInfo.dataOffset);
					auto newTexId = ImGuiTexEdit(propName, pTexData->tex);
					//TODO: If you want to change this, compare new ID with old ID
					//if (!texInfo.tex || newTexId != texInfo.tex->GetAssetID())
					break;
				}
				default:
					ImGui::LabelText(propName, "Can't edit %s", c_SPTNames[propInfo.type]);
					break;
				}
			}
			return anyChanges;
		}

		bool Mesh::DrawProperties() 
		{
			// submeshes
			{
				static bool expanded = true;
				ImGui::AlignTextToFramePadding();
				ImGuiDisplay("Sub Meshes");
				ImGuiSameLine();
				if (ImGuiButton(expanded ?"-##sm" : "+##sm"))
					expanded = !expanded;
				ImGuiDisplaySep();
				if (expanded) {
					ImGuiIndent();
					const int numTotalColumns = 7;
					static unsigned showColumns = (1u << numTotalColumns) - 1;
					static const char* s_columnNames[numTotalColumns] = {
						"Name",
						"Material",
						"Verts",
						"Tris",
						"Data",
						"Extents",
						"Center"
					};
					for (int i = 0; i < numTotalColumns; i++)
					{
						if (i > 0)
							ImGui::SameLine();
						ImGui::PushID(i);
						ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(i / 10.0f, showColumns & (1u << i) ? 0.6f : 0.1f, 0.6f));
						ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(i / 10.0f, 0.7f, 0.7f));
						ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(i / 10.0f, 0.8f, 0.8f));
						char label[3];
						sprintf(label, "%c%c", s_columnNames[i][0], s_columnNames[i][1]);
						if (ImGui::Button(label))
							showColumns ^= 1u << i;
						ImGui::PopStyleColor(3);
						ImGui::PopID();
					}
					ImGui::Columns( countSetBits(showColumns) , "submeshes");
					// header
					for (int i = 0; i < numTotalColumns; i++) {
						if (showColumns & (1u << i)) {
							ImGuiDisplay(s_columnNames[i]); ImGui::NextColumn();
						}
					}
					ImGuiDisplaySep();
					// content
					for (auto& subMesh : subMeshes) {
						if (showColumns & (1u << 0)) {
							ImGuiDisplay(subMesh.name.s); ImGui::NextColumn();
						}
						if (showColumns & (1u << 1)) {
							ImGuiDisplay(materials[subMesh.matIdx].name.c_str()); ImGui::NextColumn();
						}
						if (showColumns & (1u << 2)) {
							ImGui::Text("%d", subMesh.primitiveBuffer->m_vertexBuffer->m_numVertices); ImGui::NextColumn();
						}
						if (showColumns & (1u << 3)) {
							ImGui::Text("%d", subMesh.primitiveBuffer->m_numElements); ImGui::NextColumn();
						}
						if (showColumns & (1u << 4)) {
							// data channels
							if (!subMesh.primitiveBuffer->m_vertexBuffer) {
								ImGui::Text("NONE!"); ImGui::NextColumn();
							} else {
								auto& vertLayout = subMesh.primitiveBuffer->m_vertexBuffer->m_layout;
								for (int l = 0; l < vertLayout.m_numStreams; l++) {
									switch (vertLayout.m_streams[l].semantic) {
									case BGSS_POSITION:
										ImGui::Text("P ");
										break;
									case BGSS_NORMALS:
										ImGui::Text("N ");
										break;
									case BGSS_TANGENTS:
										ImGui::Text("T ");
										break;
									case BGSS_TEXTURE01:
										ImGui::Text("UV01 ");
										break;
									case BGSS_TEXTURE23:
										ImGui::Text("UV23 ");
										break;
									case BGSS_COLOR0:
										ImGui::Text("C ");
										break;
									case BGSS_COLOR1:
										ImGui::Text("C2 ");
										break;
									}
									ImGui::SameLine();
								}
								ImGui::NextColumn();
							}
						}
						if (showColumns & (1u << 5)) {
							vec3 ext = subMesh.aabb.ext();
							ImGui::Text("%.3f %.3f %.3f", ext.x, ext.y, ext.z);
							if (ImGui::IsItemHovered()) {
								ImGui::BeginTooltip();
								ImGui::Text("%.3f %.3f %.3f", ext.x, ext.y, ext.z);
								ImGui::EndTooltip();
							}
							ImGui::NextColumn();
						}
						if (showColumns & (1u << 6)) {
							vec3 ctr = subMesh.aabb.center();
							ImGui::Text("%.3f %.3f %.3f", ctr.x, ctr.y, ctr.z);
							if (ImGui::IsItemHovered()) {
								ImGui::BeginTooltip();
								ImGui::Text("%.3f %.3f %.3f", ctr.x, ctr.y, ctr.z);
								ImGui::EndTooltip();
							}
							ImGui::NextColumn();
						}
					}
					ImGui::Columns();
					ImGuiUnindent();
				}
			}
			// nodes
			ImGuiDisplaySep();
			{
				static bool expanded = true;
				ImGui::AlignTextToFramePadding();
				ImGuiDisplay("Nodes");
				ImGuiSameLine();
				if (ImGuiButton(expanded ? "-##nd" : "+##nd"))
					expanded = !expanded;
				ImGuiDisplaySep();

				if (expanded) {
					ImGuiIndent();
					ImGui::Columns(3, "nodes");
					ImGuiDisplay("Name"); ImGui::NextColumn();
					ImGuiDisplay("Parent"); ImGui::NextColumn();
					ImGuiDisplay("Position"); ImGui::NextColumn();
					ImGuiDisplaySep();
					for (auto it = nodes->names->begin(); it != nodes->names->end(); ++it) {
						ImGui::PushID(it.Index());
						ImGuiDisplay(*it);
						ImGui::NextColumn();
						ImGui::Text("%d", nodes->parents->Get(it.Index())); ImGui::NextColumn();
						// Extract pos from matrix
						vec4 bla = nodes->transforms->Get(it.Index())[3];
						ImGui::PushItemWidth(-1);
						ImGuiEdit("##nodepos", &bla);
						ImGui::PopItemWidth();
						nodes->transforms->Get(it.Index())[3] = bla;
						ImGui::NextColumn();
						ImGui::PopID();
					}
					ImGui::Columns();
					ImGuiUnindent();
				}
			}

			return false;
		}

		bool AnimationSet::DrawProperties() 
		{
			if (!IsValid())
				return false;

			ImGui::Text("Clips");
			ImGui::Separator();
			static unsigned selectedClip = 0;
			const auto& clips = *data->clips;
			for (int i = 0; i < (int)clips.count; i++){
				ImGui::PushID(i);
				ImGui::Text(selectedClip == i ? "	%s":"%s",clips[i].clipName);
				if (ImGui::IsItemClicked())
					selectedClip = i;
				ImGui::PopID();
			}
			if (selectedClip > clips.count)
				selectedClip = 0;
			ImGui::Separator();
			ImGui::Text("Bones");
			ImGui::Separator();
			const auto& boneNames = *data->boneNameHashes;
			for (int i = 0; i < (int)boneNames.count; i++)
				if (auto name = GetHashString(boneNames[i]))
					ImGui::Text( name);
				else
					ImGui::Text("%#x", boneNames[i]);

			return false;
		}
	};
};