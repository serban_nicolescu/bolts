#include "StdAfx.h"
#include "PropertyWindow.h"
#include "GUI/Properties.h"
#include <imgui.h>

using namespace Bolts;

PropertyWindow::PropertyWindow()
{

}

PropertyWindow::~PropertyWindow()
{
}

bool PropertyWindow::Update()
{
	if(m_showNewPropPanel )
		NewPropWindow();

	// Draw properties
	ImGui::Begin( "Properties", nullptr, ImGuiWindowFlags_AlwaysAutoResize);
	if( ImGui::RadioButton( "Add Property", m_showNewPropPanel ) ) {
		m_showNewPropPanel = true;
	}
	bool isDirty = false;
	for( auto& prop : m_props ) {
		isDirty = isDirty | prop->Draw();
	}
	// if prop changed update data
	if( isDirty ) {
		m_builder.ResizeKeys( m_props.size() );
		for( auto& prop : m_props ) {
			prop->ToBinary( m_builder );
		}
		m_data = m_builder.Build();
	}
	ImGui::End();
	//
	return false;
}

void PropertyWindow::NewPropWindow()
{
	if( !ImGui::Begin( "Add new property", &m_showNewPropPanel ) ) {
		ImGui::End();
		return;
	}

	static char n[256];
	ImGui::InputText( "Prop name", n, sizeof( n ) );

	// Prop type selection
	static int item = PROP_FLOAT;
	const char* propNames[_PROP_COUNT];
	for (int i = 0; i < _PROP_COUNT; i++)
		propNames[i] = GetPropTypeName((PropType)i);
	ImGui::Combo( "Prop type", &item, propNames, _PROP_COUNT );
	if( strlen( n ) > 0 && ImGui::Button( "Add" ) ) {
		std::unique_ptr<IProp> pptr{ MakeProp( PropType(item)) };
		pptr->name = n;
		m_props.emplace_back( std::move( pptr ) );
	}

	ImGui::End();
}

BinaryMapView PropertyWindow::ViewData()
{
	return m_data;
}

void PropertyWindow::SetProps( propVec_t&& pv)
{
	m_props = std::move(pv);
}