#include "StdAfx.h"
#include "AssetDetailsWindow.h"
#include "GUI/Properties.h"
#include <imgui.h>

namespace Bolts {
	namespace Editor {

		void AssetDetailsWindow::DrawContent()
		{
			if (!targetAsset)
				return;
				
			if (targetAsset->GetAssetName())
				ImGuiDisplay( targetAsset->GetAssetName());

			ImGuiDisplaySep();
			ImGuiDisplaySep();
			if(targetAsset->GetAssetState() == assets::EAS_NOT_FOUND) {
				ImGuiDisplay("Asset not found.");
				return;
			}

			if (drawFunc) {
				drawFunc(targetAsset.get());
			}
		}
	};
};