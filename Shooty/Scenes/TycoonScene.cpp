#include "stdafx.h"

#include "Scenes/TycoonScene.h"
#include "Components/Space.h"
#include "Components/MeshComponent.h"

#include "Render/DebugDraw.h"
#include <Profiler.h>

#include <Helpers/MeshBuilder.h>
#include <Rendering/MaterialNew.h>
#include <Assets/AssetManager.h>

#include <imgui.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

using namespace Bolts;

//TODO
/*
- place camera in realisic position
- render quad-based terrain
- load terrain tex and keep CPU data
- render fake units

*/

namespace Tycoon
{
	struct Entity
	{
		vec2 pos;
		vec2 dir; // facing direction. normalised
		float scale = 1.f;
		float radiusW; // radius in world units

		Rendering::voHandle_t			voId;
		Rendering::renderableHandle_t	renderableId;
	};

	struct Unit : public Entity
	{
		int ID = -1;
		vec2 speed;
	};

	vec_t<Entity>	g_staticEntities;
	vec_t<Unit>		g_liveUnits;

	Rendering::MaterialPtr			terrainMaterialOld;
	Rendering::MaterialPtr			terrainMaterial;
	int								terrainResolution = 64;
	Rendering::MeshPtr				tileMesh;

	Rendering::Renderer*			g_renderer{};
	AssetManager*					g_assetManager{};

	Rendering::voHandle_t			terrainVO;
	Rendering::renderableHandle_t	terrainRenderable;

	const struct {
		float radius;
		regHash_t meshName;
		regHash_t materialName;
	} 
	s_randomEntities[4] = {
		{ 1.f,	"SM_Plains_Tree01.mesh", "IBTree1" },
		{ 0.2f,	"SM_Plains_Grass01.mesh", "IBPlant" },
		{ 0.5f,	"SM_Plains_Fern01.mesh", "IBPlant" },
		{ 0.5f,	"SM_Plains_Fern02.mesh", "IBPlant" },
	};

	void ReloadResources()
	{
		using namespace Rendering;

		tileMesh = g_assetManager->LoadAsset<Mesh>("TerrainTile");
		if (tileMesh->subMeshes.empty())
			tileMesh->subMeshes.resize(1);
		else
			tileMesh->subMeshes[0].primitiveBuffer->Release(*g_assetManager->GetCommandBuffer());

		tileMesh->subMeshes[0] = { MeshBuilder::GridIndexed(*g_assetManager->GetCommandBuffer(), terrainResolution), "Tile" };
		tileMesh->SetAssetState(assets::EAS_READY);
	}

	void InitResources()
	{
		using namespace Rendering;

		g_assetManager->LoadAssetFile<Rendering::Material>("tycoon.matpack");
		terrainMaterialOld = g_assetManager->LoadAsset<Rendering::Material>("TycoonTerrainOld");
		terrainMaterial = g_assetManager->LoadAsset<Rendering::Material>("TycoonTerrain");
		ReloadResources();

		{
			//terrain
			terrainVO = g_renderer->AddVisibilityObject();
			g_renderer->SetVOLists(terrainVO, (voListMask_t)(MAIN_VIEW | SHADOW_VIEW));
			g_renderer->SetVODebugString(terrainVO, "Terrain");
			auto& staticMeshRenderable = g_renderer->AddStaticMesh();
			terrainRenderable = staticMeshRenderable.GetRenderableID();
			g_renderer->AttachRenderable(terrainVO, terrainRenderable);

			float terrainScale = 15.f;
			mat4 t;
			t = glm::translate(t, vec3(terrainScale * -0.5f, terrainScale * -0.5f, 0.f));
			t = glm::scale(t, vec3(terrainScale, terrainScale, 2.f));
			g_renderer->SetVOTransformAndRadius( terrainVO, t, terrainScale * 1.5f);
			staticMeshRenderable.m_mesh = tileMesh;
			staticMeshRenderable.SetWorldTransform(t);

			auto& renderableMaterials = staticMeshRenderable.m_materials;
			renderableMaterials.resize(1);
			renderableMaterials[0] = terrainMaterial;
		}
	}

	void TycoonDraw(Rendering::Renderer& renderer, const Rendering::detail::FPackage& fpack, const Rendering::detail::VPackage& vpack, const Rendering::detail::RenderContext& ctx)
	{
		// render terrain
		//renderer.EnableMaterial( *g_unitRendering.terrainMaterialOld);
		//renderer.DrawInstanced( Rendering::BGPT_TRIANGLE_STRIP, 4, g_unitRendering.terrainResolution * g_unitRendering.terrainResolution);
		//renderer.EnableMaterial(*terrainMaterial);
		//renderer.Draw(*tileMesh, 0);
	}

	Unit& SpawnUnit()
	{
		static int uid = 0;
		g_liveUnits.emplace_back();
		auto& unit = g_liveUnits.back();
		unit.ID = uid++;
		return unit;
	}

	void DestroyEntity(const Entity& deadEntity)
	{
		g_renderer->RemoveRenderable(deadEntity.renderableId);
		g_renderer->RemoveVisibilityObject(deadEntity.voId);
	}

	void DestroyUnit(int idx)
	{
		auto& deadUnit = g_liveUnits[idx];
		
		DestroyEntity(deadUnit);

		std::swap(g_liveUnits[idx], g_liveUnits.back());
		g_liveUnits.pop_back();
	}
	
	void TycoonCleanup()
	{
		while (g_liveUnits.size())
			DestroyUnit(g_liveUnits.size() - 1);

		for ( const Entity& deadEntity: g_staticEntities)
			DestroyEntity(deadEntity);

		g_renderer->RemoveRenderable( terrainRenderable);
		g_renderer->RemoveVisibilityObject( terrainVO);
	}

	void GetEntityTransform(const Entity& entity, mat4& out)
	{
		mat4 m{};
		m = glm::translate(m, vec3(entity.pos, 0.f));
		m = glm::scale(m, vec3(entity.scale));
		out = m;
	}

	BOLTS_INLINE Unit& EditUnitByIdx(int idx)
	{
		 return g_liveUnits[idx];
	}

	void SetEntityMesh(Entity& entity, stringHash_t meshID, stringHash_t materialID)
	{
		using namespace Rendering;
		BASSERT(entity.voId == 0);
		entity.voId = g_renderer->AddVisibilityObject();
		
		g_renderer->SetVOLists(entity.voId, (voListMask_t)( MAIN_VIEW | SHADOW_VIEW));

		auto& staticMeshRenderable = g_renderer->AddStaticMesh();
		entity.renderableId = staticMeshRenderable.GetRenderableID();
		g_renderer->AttachRenderable(entity.voId, entity.renderableId);
		
		mat4 t;
		GetEntityTransform(entity, t);
		MeshPtr mesh = g_assetManager->LoadAsset<Mesh>(meshID);
		BASSERT(mesh->IsValid());
		vec3 stupid = glm::max(glm::abs(mesh->aabb.min), glm::abs(mesh->aabb.max));
		entity.radiusW = glm::length(stupid * entity.scale);
		g_renderer->SetVOTransformAndRadius(entity.voId, t, entity.radiusW);
		staticMeshRenderable.m_mesh = mesh;
		staticMeshRenderable.SetWorldTransform(t);
		
		MaterialPtr mat = g_assetManager->LoadAsset<Material>(materialID);
		BASSERT(mat->IsValid());
		auto& renderableMaterials = staticMeshRenderable.m_materials;
		auto meshMatCount = mesh->materials.size();
		if (meshMatCount != renderableMaterials.size()) {
			renderableMaterials.resize(meshMatCount);
			for (auto& matPtr : renderableMaterials)
				matPtr = mat;
		}
	}

	void SetEntityLight(Entity& entity, vec3 color, float radius)
	{
		using namespace Rendering;
		BASSERT(entity.voId == 0);
		entity.voId = g_renderer->AddVisibilityObject();

		g_renderer->SetVOLists(entity.voId, (voListMask_t)(MAIN_VIEW));

		auto& lightRenderable = g_renderer->AddLight();
		entity.renderableId = lightRenderable.GetRenderableID();
		g_renderer->AttachRenderable(entity.voId, entity.renderableId);

		mat4 t;
		GetEntityTransform(entity, t);
		entity.radiusW = radius * entity.scale * 2.f;
		g_renderer->SetVOTransformAndRadius(entity.voId, t, entity.radiusW);
		lightRenderable.SetWorldTransform(t);
		lightRenderable.m_radius = entity.radiusW;
		lightRenderable.m_color = color;
	}

	void SpawnStaticMeshes()
	{
		const vec2 worldExt{ 5.f };
		for (int i = 0; i < (int)countof(s_randomEntities); i++) { 
			const auto& entityDesc = s_randomEntities[i];
			//auto meshPtr = g_assetManager->LoadAsset<Rendering::Mesh>(entityDesc.meshName);
			//BASSERT(meshPtr->IsValid());
			//auto matPtr = g_assetManager->LoadAsset<Rendering::Mesh>(entityDesc.materialName);
			//BASSERT(matPtr->IsValid());

			float meshCount = (worldExt.x / entityDesc.radius) * (worldExt.y / entityDesc.radius);
			for (float offX = -worldExt.x; offX < worldExt.x; offX += entityDesc.radius)
				for (float offY = -worldExt.y; offY < worldExt.y; offY += entityDesc.radius) {
					g_staticEntities.emplace_back();
					Entity& spawnedEntity = g_staticEntities.back();
					spawnedEntity.dir = {1.f, 0.f};
					spawnedEntity.scale = 0.01f;
					spawnedEntity.radiusW = entityDesc.radius;
					spawnedEntity.pos = { offX, offY};
					SetEntityMesh(spawnedEntity, entityDesc.meshName.h, entityDesc.materialName.h);
				}
		}
	}

	void TickUnitMovement(float dt)
	{
		for (auto& unit : g_liveUnits) {
			if (unit.speed != vec2_zero)
				unit.pos += unit.speed*dt;
		}
	}

	void BounceUnitsOffWalls(vec2 arenaExtent)
	{
		for (auto& unit : g_liveUnits) {
			if (unit.pos.x > arenaExtent.x) {
				unit.pos.x = arenaExtent.x;
				unit.speed.x *= -1.f;
			}
			if (unit.pos.x < -arenaExtent.x) {
				unit.pos.x = -arenaExtent.x;
				unit.speed.x *= -1.f;
			}
			if (unit.pos.y > arenaExtent.y) {
				unit.pos.y = arenaExtent.y;
				unit.speed.y *= -1.f;
			}
			if (unit.pos.y < -arenaExtent.y) {
				unit.pos.y = -arenaExtent.y;
				unit.speed.y *= -1.f;
			}
		}
	}

	void UpdateUnitRendering()
	{
		for (auto& unit : g_liveUnits) {
			if (unit.voId) {
				mat4 t;
				GetEntityTransform(unit, t);
				g_renderer->SetVOTransformAndRadius(unit.voId, t, unit.radiusW);
				auto& genericRenderable = g_renderer->GetRenderableAny(unit.renderableId);
				genericRenderable.SetWorldTransform(t);
			}
		}
	}

	//Rendering::Renderer::customRenderlist_t g_customDraw = TycoonDraw;


	TycoonScene::TycoonScene(Rendering::Renderer& rnd, assets::AssetManager& rs, GOS& gos) :
		StandardScene_RenderingNew(rnd, rs), m_GOS(gos)
	{
	}

	TycoonScene::~TycoonScene() {

	}

	void TycoonScene::OnRegister()
	{
		g_renderer = &m_renderer;
		g_assetManager = &m_assetManager;
		//
		LoadResources();
		//
		//Cam Init
		m_mainCamera.SetUpVector(vec3(0.f, 1.f, 0.f));
		m_mainCamera.SetZNear(0.1f);
		m_mainCamera.SetZFar(100.f);
		m_mainCamera.SetPosition({ 0.f, 0.f, 50.f });
		m_mainCamera.SetTarget(vec3_zero);
		//
		SetupScene();
	}

	void TycoonScene::LoadResources()
	{
		InitResources();
	}

	void TycoonScene::SetupScene()
	{
		m_space = m_GOS.NewSpace();

		//m_renderer.AddCustomRenderlist(&g_customDraw);
		//SpawnStaticMeshes();
	}

	void TycoonScene::ClearScene()
	{
		//Clean up scene
		m_GOS.DestroySpace(m_space);
		m_space = nullptr;

		//m_renderer.RemoveCustomRenderlist(&g_customDraw);
	}

	void TycoonScene::OnDestroy()
	{
		m_GOS.DestroySpace(m_space);

		TycoonCleanup();
	}

	bool TycoonScene::OnUpdate(double dt)
	{
		using namespace Input;
		const float deltaT = (float)dt;

		{
			PROFILE_SCOPE("Update Units");
			TickUnitMovement(deltaT);
			BounceUnitsOffWalls(vec2{ 10.f });
			UpdateUnitRendering();
		}
		// camera control

		// gameplay control

		vec2 moveDir = vec2_zero;
		if (keyState.IsKeyDown(Input::keyCode_t('W')))
			moveDir.y += 1.f;
		if (keyState.IsKeyDown(Input::keyCode_t('S')))
			moveDir.y -= 1.f;
		if (keyState.IsKeyDown(Input::keyCode_t('A')))
			moveDir.x -= 1.f;
		if (keyState.IsKeyDown(Input::keyCode_t('D')))
			moveDir.x += 1.f;
		moveDir = glm::normalize(moveDir);

		if (mouseState.HasButtonBeenReleased(Input::B_LEFT))
		{
			// click ground to spawn units
			Ray r = Bolts::MakeRay(m_activeCamera->ProjectScreenCoords(mouseState.GetMousePosition()));
			float groundHitT = r.o.z * r.invN.z;
			if (groundHitT < 0.f) {
				vec2 hitLocation = vec2(r.o) - (groundHitT / vec2(r.invN));

				/*
				auto& spawnedUnit = SpawnUnit();
				spawnedUnit.pos = hitLocation;
				spawnedUnit.scale = 1.f;
				SetEntityLight(spawnedUnit, vec3(RandUnitFloat_G(), RandUnitFloat_G(), RandUnitFloat_G()), (0.5f + RandUnitFloat_G() * 0.5f) * 4.f);
				//spawnedUnit.scale = 0.2f;
				//SetEntityMesh(spawnedUnit, "UnitSphere", "Phong"); 
				vec2 d = RandUnitVec2_G();
				float s = RandUnitFloat_G();
				spawnedUnit.speed = d * (1.f + s * 2.f);
				*/
			}
		}

		//Tweakers
		if (ImGui::Begin("Scene"))
		{
			bool anyChanges = false;
			ImGui::Text("Terrain Resolution");
			anyChanges |= ImGui::RadioButton("32" , &terrainResolution, 32); ImGui::SameLine();
			anyChanges |= ImGui::RadioButton("64" , &terrainResolution, 64); ImGui::SameLine();
			anyChanges |= ImGui::RadioButton("128", &terrainResolution, 128); ImGui::SameLine();
			anyChanges |= ImGui::RadioButton("256", &terrainResolution, 256);
			if (anyChanges)
				ReloadResources();
			anyChanges = false;

			if (terrainMaterial && terrainMaterial->IsValid())
			{
				auto& materialParameters = terrainMaterial->EditParams();
				if (auto scale = materialParameters.TryEdit<vec3>("u_scale"))
				{
					anyChanges |= ImGui::DragFloat("Scale", &scale->x, 0.1f, 0.1f, 10.f); 
					if (anyChanges)
						scale->y = scale->x;
					ImGui::SameLine();
					anyChanges |= ImGui::DragFloat("Height", &scale->z, 0.1f, 0.1f, 10.f);
				}
				if (auto uvView = materialParameters.TryEdit<vec4>("u_uvScaleOffset"))
				{
					anyChanges |= ImGui::DragFloat4("UV View", &uvView->x, 0.001f, 0.f, 1.f);
				}
			}

			using namespace Rendering;
			static bool showTerrain = true;
			if (ImGui::Checkbox("Terrain", &showTerrain)) {
				if (showTerrain)
					g_renderer->SetVOLists(terrainVO, (voListMask_t)(MAIN_VIEW | SHADOW_VIEW));
				else 
					g_renderer->SetVOLists(terrainVO, NO_VIEW);
			}


		}
		ImGui::End();

		keyState.EventsHandled();
		mouseState.EventsHandled();
		return true;
	}

	void TycoonScene::OnMouseEvent(Input::mouseEvent_t event)
	{
		using namespace Input;

		mouseState.OnMouseEvent(event);
	}

	void TycoonScene::OnKeyEvent(Input::keyEvent_t event)
	{
		keyState.OnKeyEvent(event);
	}

};