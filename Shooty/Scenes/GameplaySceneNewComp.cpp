#include "stdafx.h"

#include "Scenes/GameplaySceneNewComp.h"
#include "Components/Space.h"
#include "Components/MeshComponent.h"
#include "Components/DirectionalLightComponent.h"
//TODO: Move to scene rendering sources
#include "Helpers/MeshBuilder.h"
#include "Rendering/MaterialNew.h"
#include <Assets/AssetManager.h>

#include "Serialization/PropertiesJSON.h"

#include <imgui.h>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

using namespace Bolts;

GameplaySceneNewComp::GameplaySceneNewComp(Bolts::Rendering::Renderer& rnd, Bolts::assets::AssetManager& rs, GOS& gos) :
	StandardScene_RenderingNew(rnd, rs), m_GOS(gos) 
{
}

void GameplaySceneNewComp::OnRegister()
{
	using namespace Bolts;
	//Cam Init
	m_mainCamera.SetUpVector(vec3(0.f, 1.f, 0.f));
	m_mainCamera.SetZNear(0.1f);
	m_mainCamera.SetZFar(100.f);
	//Resources
	InitMaterials();
	InitStaticMeshes();
	//
	SetupScene();
	PostSetupResourceInit();
}

void	GameplaySceneNewComp::InitMaterials()
{
	m_assetManager.LoadAssetFile<Bolts::Rendering::Material>("matcaps.matpack");
	m_assetManager.LoadAssetFile<Bolts::Rendering::Material>("house.matpack");
	m_assetManager.LoadAssetFile<Bolts::Rendering::Material>("house.matpack");//calling it twice just to test that things work correctly if we do it
	m_assetManager.LoadAssetFile<Bolts::Rendering::Material>("tests.matpack");
	m_assetManager.LoadAsset<Bolts::Rendering::Material>("PreethamSky");

	//Load House mats
	// 3D texture tests
	//{
	//	using namespace Bolts::Rendering;
	//	auto tex = m_ResourceSystem.TexMgr.GetTexture("MyFirst3DTex");
	//	auto& texData = tex->EditData();
	//	texData.format = BTF_RGBA;
	//	texData.dimensions = { 128, 128,128};
	//	texData.hasMips = false;
	//	texData.type = BTT_3D;

	//	const uint32_t dataSize = texData.GetMipLevelSize(0);
	//	texData.buffer.resize(dataSize);
	//	Bolts::color4* dataPtr = reinterpret_cast<Bolts::color4*>( texData.buffer.data());
	//	// Fill it with red
	//	for (unsigned i = 0; i < texData.dimensions.z; i++)
	//		for (unsigned j = 0; j < texData.dimensions.y; j++)
	//			for (unsigned k = 0; k < texData.dimensions.x; k++){
	//				dataPtr[i * texData.dimensions.x * texData.dimensions.y + j*texData.dimensions.x + k] = Bolts::color4(128, 0, 0, 255);
	//			}
	//	tex->SetState(Bolts::res::ERS_READY);
	//}
}

void GameplaySceneNewComp::InitStaticMeshes()
{
	using namespace Bolts;

	auto& commands = *m_assetManager.GetCommandBuffer();

	{	//Add cube mesh
		auto pmesh = m_assetManager.LoadAsset<Bolts::Rendering::Mesh>("UnitCube");
		pmesh->subMeshes.push_back({
			Bolts::MeshBuilder::CubeIndexed(commands, vec3(1.f)),
			"UnitCube"
			});
		pmesh->aabb.min = vec3(-0.5f);
		pmesh->aabb.max = vec3(0.5f);
		pmesh->materials.push_back({});
		pmesh->SetAssetState(assets::EAS_READY);
		pmesh = m_assetManager.LoadAsset<Bolts::Rendering::Mesh>("UnitSphere");
		pmesh->subMeshes.push_back({
			Bolts::MeshBuilder::SphereIndexed(commands, 1.f, 32, 32),
			"Sphere"
			});
		pmesh->aabb.min = vec3(-0.5f);
		pmesh->aabb.max = vec3(0.5f);
		pmesh->materials.push_back({});
		pmesh->SetAssetState(assets::EAS_READY);
	}
}

void GameplaySceneNewComp::SetupScene()
{
	using namespace Bolts;

	m_Space = m_GOS.NewSpace();
	//
	//Bolts::vec3 camPos = m_GameObjects[0]->TryGetComponent<CMgrMesh>()->GetMesh()->m_BBoxExtents;
	//camPos.x = camPos.x / 3.f;
	//camPos.y = camPos.y / 3.f;
	//camPos.z = camPos.z / 2.f;
	m_mainCamera.SetPosition({ -0.5f, 3.f, 5.f});
}

void GameplaySceneNewComp::PostSetupResourceInit()
{
}

void GameplaySceneNewComp::ClearScene()
{
	//Clean up scene
	m_GOS.DestroySpace(m_Space);
	m_Space = nullptr;
}

void GameplaySceneNewComp::OnDestroy()
{
	m_GOS.DestroySpace(m_Space);
}

bool GameplaySceneNewComp::OnUpdate(double dt)
{
	using namespace Bolts::Input;
	const float deltaT = (float)dt;
	//
	//m_GameObjects[1]->EditTransform().SetPosition(m_LightPosition);

	//
	////Update physics
	//bool m_IsPhysicsPaused = false;
	//double m_PhysicsSimSpeed = 1.f;
	//if ( !m_IsPhysicsPaused ) {
	//	//Update Physics
	//	static double l_physicsRemainingTime = 0.f;
	//	const double physicsDtSec = dt*m_PhysicsSimSpeed;
	//	const int maxSteps = 6;
	//	const double physicsTimeStep = ( 1.0 / 60.0 ) * m_PhysicsSimSpeed;

	//	l_physicsRemainingTime += physicsDtSec;
	//	if ( l_physicsRemainingTime > 0.f ) {
	//		int numSteps = m_GOS.GetPhysicsMgr().GetPhysicsWorld().Update( l_physicsRemainingTime, maxSteps, physicsTimeStep );
	//		l_physicsRemainingTime -= numSteps * physicsTimeStep;
	//	}
	//}

	//if (m_KeyState.KeyReleased(keyCode_t('H'))) {
	//	bool wasVis = m_GameObjects[0]->TryGetComponent<CMgrMesh>()->GetVisibility();
	//	m_GameObjects[0]->TryGetComponent<CMgrMesh>()->SetVisibility(!wasVis);
	//}

	//Move the light
	//if (m_KeyState.IsKeyDown(keyCode_t('L'))) {
	//	/*
	//	const float c_lightSpeed = 300.0;

	//	if ( m_KeyState.IsKeyDown( KC_LEFT ) )
	//		m_LightPosition.x -= deltaT * c_lightSpeed;
	//	if ( m_KeyState.IsKeyDown( KC_RIGHT ) )
	//		m_LightPosition.x += deltaT * c_lightSpeed;
	//	if ( m_KeyState.IsKeyDown( KC_UP ) )
	//		m_LightPosition.y += deltaT * c_lightSpeed;
	//	if ( m_KeyState.IsKeyDown( KC_DOWN ) )
	//		m_LightPosition.y -= deltaT * c_lightSpeed;
	//	*/
	//	const float c_turnSpeed = 3.f;
	//	static float hangle = 0.f;
	//	static float vangle = -30.f;
	//	if (m_KeyState.IsKeyDown(KC_LEFT))
	//		hangle += deltaT * c_turnSpeed;
	//	if (m_KeyState.IsKeyDown(KC_RIGHT))
	//		hangle -= deltaT * c_turnSpeed;
	//	if (m_KeyState.IsKeyDown(KC_UP))
	//		vangle += deltaT * c_turnSpeed;
	//	if (m_KeyState.IsKeyDown(KC_DOWN))
	//		vangle -= deltaT * c_turnSpeed;

	//	Bolts::quat q = glm::angleAxis(vangle, Bolts::vec3{ 0.f, 1.f, 0.f }) *
	//		glm::angleAxis(hangle, Bolts::vec3{ 0.f, 0.f, 1.f });
	//	//Move the light around
	//	m_GameObjects[4]->EditTransform().SetRotation(q);
	//}

	//{
	//	//Move the camera
	//	using namespace Bolts;
	//	const float c_CamSpeed = m_KeyState.IsKeyDown(Input::KC_SHIFT) ? 20.f : 2.f;
	//	if (m_KeyState.IsKeyDown(keyCode_t('W')))
	//		m_mainCamera.MoveForward(deltaT * c_CamSpeed);
	//	//if (m_KeyState.IsKeyDown(keyCode_t('Q')))
	//	//	m_mainCamera.Tilt(deltaT * 1.1f);
	//	//if (m_KeyState.IsKeyDown(keyCode_t('E')))
	//	//	m_mainCamera.Tilt(-deltaT * 1.1f);
	//	if (m_KeyState.IsKeyDown(keyCode_t('S')))
	//		m_mainCamera.MoveForward(-deltaT * c_CamSpeed);
	//	if (m_KeyState.IsKeyDown(keyCode_t('A')))
	//		m_mainCamera.MoveSideways(deltaT * c_CamSpeed);
	//	if (m_KeyState.IsKeyDown(keyCode_t('D')))
	//		m_mainCamera.MoveSideways(-deltaT * c_CamSpeed);
	//}
/*
	const Bolts::vec2 mouseMove = m_MouseState.GetMouseMoveAmount();

	if (m_MouseState.IsButtonDown(Bolts::Input::B_RIGHT)) {
		const float c_sensitivity = 0.8f;
		m_mainCamera.SpinRight(Bolts::c_PI * c_sensitivity * mouseMove.x);
		m_mainCamera.SpinUp(Bolts::c_PI * c_sensitivity * mouseMove.y);
	}*/

	// Sky Tweakers
	if (false) {
		if (ImGui::Begin("Sky Settings", nullptr, ImGuiWindowFlags_AlwaysAutoResize)) {
			ImGui::SliderFloat("Average Lum", &m_avgLuminance, 0.f, 10.f);
			ImGui::SliderFloat("Reileigh Coefficient", &m_reileighCoefficient, 0.f, 2.f);
			ImGui::SliderFloat("Mie Coefficient", &m_mieCoefficient, 0.f, 2.f);
			//ImGui::SliderFloat("Mie Directional G", &m_mieDirectionalG, 0.f, 2.f);
			ImGui::SliderFloat("Turbidity", &m_turbidity, 0.f, 10.f);
		}
		ImGui::End();
	}

	m_KeyState.EventsHandled();
	m_MouseState.EventsHandled();
	m_FrameCount++;
	return true;
}

void GameplaySceneNewComp::OnMouseEvent(Bolts::Input::mouseEvent_t event)
{
	using namespace Bolts::Input;

	m_MouseState.OnMouseEvent(event);
}

void GameplaySceneNewComp::OnKeyEvent(Bolts::Input::keyEvent_t event)
{
	m_KeyState.OnKeyEvent(event);
}

GameplaySceneNewComp::~GameplaySceneNewComp()
{

}
