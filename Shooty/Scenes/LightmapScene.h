#pragma once
#include "Menus/GameScene.h"
#include "Scenes/SceneStandard_NewRendering.h"
#include "Rendering/Mesh.h"
#include "Input/IOEvents.h"
#include "Helpers/Camera.h"

#include "Components/Space.h"
#include "Components/GOS.h"

struct lm_context;

using namespace Bolts;

class SceneLightmap:
	public IGameScene,
	public StandardScene_RenderingNew,
	public nonCopyable_t
{
public:
	SceneLightmap(Rendering::Renderer&, assets::AssetManager&, GOS&);
	~SceneLightmap();

	void	OnRegister() override;
	void	OnPause() override;
	void	OnResume() override;
	void	OnDestroy() override;//Called when popped

	bool	OnUpdate( double dt ) override;
	void	RenderLM(Rendering::Renderer&);
	//
	void	OnMouseEvent( Input::mouseEvent_t event ) override;//WARN: Can be called anytime
	void	OnKeyEvent( Input::keyEvent_t event ) override;//WARN: Can be called anytime
	//
	Space* GetActiveSpace() override { return m_Space; }
	CameraPtr GetActiveCamera() override { return m_activeCamera; }
private:
	void UpdateSelectedObj();
	void StartRendering();
	void FinishRendering();

	//Gameplay
	GOS&				m_GOS;
	Space*				m_Space;
	vec_t< goRef_t>		m_GameObjects;
	Input::KeyStateHolder	m_KeyState;
	Input::MouseStateHolder	m_MouseState;
	   
	Rendering::Renderer::renderHack_t m_drawHook;
	Rendering::MeshPtr	m_fullscreenQuadMesh;

	mat4				m_objWorld;
	vec_t<vec3>			m_lightmapData[2];
	Rendering::TexturePtr	m_lightmapTexture;
	Rendering::MeshPtr	m_lightmappedMesh;
	int					m_selectedObj = -1;
	lm_context*			m_context = nullptr;

	bool				m_done = false;
	int					m_raysPerFrame = 64;
	uvec2				m_resolution{128};
};
