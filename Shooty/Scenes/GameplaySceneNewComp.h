#pragma once
#include "Menus/GameScene.h"
#include "Scenes/SceneStandard_NewRendering.h"
#include "Rendering/Mesh.h"
#include "Input/IOEvents.h"
#include "Helpers/Camera.h"

#include "Components/Space.h"
#include "Components/GOS.h"

class GameplaySceneNewComp :
	public IGameScene,
	public StandardScene_RenderingNew,
	public Bolts::nonCopyable_t
{
public:
	GameplaySceneNewComp( Bolts::Rendering::Renderer&, Bolts::assets::AssetManager&, Bolts::GOS&);
	~GameplaySceneNewComp();

	void	OnRegister() override;
	void	OnPause() override {};
	void	OnResume() override {};
	void	OnDestroy() override;//Called when popped
	//
	bool	OnUpdate( double dt ) override;
	//void	Render(std::function<void()>& debugRenderCB) override;
	void	SetupScene();
	void	ClearScene();
	//
	Bolts::Space* GetActiveSpace() override { return m_Space; }
	Bolts::CameraPtr GetActiveCamera() override { return m_activeCamera; }
	//Input
	void	OnMouseEvent( Bolts::Input::mouseEvent_t event ) override;//WARN: Can be called anytime
	void	OnKeyEvent( Bolts::Input::keyEvent_t event ) override;//WARN: Can be called anytime
private:
	//Resources
	void	InitStaticMeshes();
	void	InitMaterials();
	void	PostSetupResourceInit();
	//Physics
	//void	InitPhysics();
	//Physics::shapeID_t AddBoxShape( Bolts::vec3 halfExtents );
	//Physics::shapeID_t AddSphereShape( float radius );
	//Physics::shapeID_t AddCylinderShape( float radius, float height );

	//Gameplay
	unsigned					m_FrameCount = 0;
	Bolts::GOS&					m_GOS;
	Bolts::Space*					m_Space;
	Bolts::vec_t< Bolts::goRef_t>	m_GameObjects;

	//Physics and collisions
	//PhysicsWorld					m_PhysicsWorld;
	//std::vector< Physics::shapeID_t> m_PhysicsShapes;
	Bolts::Input::KeyStateHolder	m_KeyState;
	Bolts::Input::MouseStateHolder	m_MouseState;
	
	// Sky Settings
	float m_avgLuminance = 3.8f;
	float m_reileighCoefficient = 1.0f;
	float m_mieCoefficient = 0.053f;
	float m_mieDirectionalG = 0.75f;
	float m_turbidity = 1.0f;
};
