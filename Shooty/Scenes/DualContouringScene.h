#pragma once
#include "Menus/GameScene.h"
#include "Scenes/SceneStandard_NewRendering.h"
#include "Rendering/Mesh.h"
#include "Input/IOEvents.h"
#include "Helpers/Camera.h"

#include "Components/Space.h"
#include "Components/GOS.h"

#include "DualContouringTest/DualContouring.h"

class SceneDualContouring :
	public IGameScene,
	public StandardScene_RenderingNew,
	public Bolts::nonCopyable_t
{
public:
	SceneDualContouring( Bolts::Rendering::Renderer&, Bolts::assets::AssetManager&, Bolts::GOS&);
	~SceneDualContouring();

	void	OnRegister() override;
	void	OnPause() override {};
	void	OnResume() override {};
	void	OnDestroy() override;//Called when popped
	//
	bool	OnUpdate( double dt ) override;
	//void	Render(std::function<void()>& debugRenderCB) override;
	void	SetupScene();
	//
	Bolts::Space* GetActiveSpace() override { return m_Space; }
	Bolts::CameraPtr GetActiveCamera() override { return m_activeCamera; }
	//Input
	void	OnMouseEvent( Bolts::Input::mouseEvent_t event ) override;//WARN: Can be called anytime
	void	OnKeyEvent( Bolts::Input::keyEvent_t event ) override;//WARN: Can be called anytime
private:
	//Gameplay
	unsigned						m_FrameCount = 0;
	Bolts::GOS&						m_GOS;
	Bolts::Space*					m_Space;
	Bolts::vec_t< Bolts::goRef_t>	m_GameObjects;
	//Physics and collisions
	//PhysicsWorld					m_PhysicsWorld;
	//std::vector< Physics::shapeID_t> m_PhysicsShapes;
	Bolts::Input::KeyStateHolder	m_KeyState;
	Bolts::Input::MouseStateHolder	m_MouseState;
	// 
	void GenerateDCMesh();

	bool m_showDebugHermiteData = true;
	int	m_hermiteDataResolution = 32;
	int m_intersectionMethod = 1; // Max is 4
	int	m_maxIntersectionSteps = 20;
	float m_intersectionDistanceTol = 0.0001f;

	dc::HermiteData				m_testHermiteData;
	Bolts::Rendering::MeshPtr	m_testHermiteDataMesh;
};
