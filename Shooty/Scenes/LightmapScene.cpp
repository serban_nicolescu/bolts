#include "stdafx.h"

#include "Scenes/LightmapScene.h"
#include "Components/Space.h"
#include "Components/MeshComponent.h"
#include "Rendering/MaterialNew.h"
#include "Rendering/Texture.h"
#include <Assets/AssetManager.h>
#include <System/system.h>

#include <rapidxml.hpp>

#define LIGHTMAPPER_IMPLEMENTATION
#include <GL/glew.h>
#include <lightmapper.h>
#include <Assets/MeshFile.h>

#include <imgui.h>

using namespace Bolts;

SceneLightmap::SceneLightmap(Rendering::Renderer& rnd, assets::AssetManager& rs, GOS& gos):
	StandardScene_RenderingNew(rnd, rs), m_GOS(gos)
{
}

SceneLightmap::~SceneLightmap()
{

}

void SceneLightmap::OnMouseEvent(Input::mouseEvent_t event)
{
	using namespace Input;

	m_MouseState.OnMouseEvent(event);
}

void SceneLightmap::OnKeyEvent(Input::keyEvent_t event)
{
	m_KeyState.OnKeyEvent(event);
}

void SceneLightmap::OnDestroy()
{
	//TODO: Destroy all RTs
	m_GOS.DestroySpace(m_Space);
}

void SceneLightmap::OnResume()
{
	m_renderer.AddRenderHack(&m_drawHook);
}

void SceneLightmap::OnPause()
{
	m_renderer.RemoveRenderHack(&m_drawHook);
}

void SceneLightmap::OnRegister()
{
	using namespace Bolts;
	using namespace Bolts::Rendering;
	//Cam Init
	m_mainCamera.SetUpVector( vec3( 0.f, 0.f, 1.f ) );
	m_mainCamera.SetZNear(0.01f);
	m_mainCamera.SetZFar( 10.f );
	// Setup scene
	m_Space = m_GOS.NewSpace();
	m_mainCamera.SetPosition({ 0.f, 1.f,0.f });
	m_GOS.Start(m_Space);

	// load resources
	m_assetManager.LoadAssetFile<Material>("2DGI.matpack");
	m_fullscreenQuadMesh = m_assetManager.LoadAsset<Mesh>("FullscreenQuad");

	m_drawHook = [this](Bolts::Rendering::Renderer& r) { RenderLM(r); };

	//mat = assetManager.LoadAsset<Material>(regHash_t(matName));

	auto& commands = *m_assetManager.GetCommandBuffer();
	regHash_t texName{ "TestLightmap" };
	m_lightmapTexture = new Texture(texName);
	m_lightmapTexture->Init(commands);
	m_lightmapTexture->SetAssetState(assets::EAS_READY);

	m_lightmapData[0].resize(m_resolution.x * m_resolution.y);
	for (vec3& v : m_lightmapData[0])
		v = vec3_one;
	auto texDataOut = m_lightmapTexture->SetTexture(commands, BTT_2D, uvec3{ m_resolution, 1 }, BTF_RGB16F, BTDF_FLOAT, false);
	texDataOut.CopyFrom(m_lightmapData[0]);

	m_assetManager.AddAsset(texName, Texture::c_assetType, m_lightmapTexture);
	
}

void SceneLightmap::UpdateSelectedObj()
{
	m_lightmappedMesh = nullptr;
	if (m_selectedObj != -1) {
		auto& go = m_Space->GetActiveObjects()[m_selectedObj];

		m_objWorld = go->GetTransform().GetAbsTransform();

		auto meshComp = go->TryGetComponent<CMgrMesh>();
		if (meshComp)
			m_lightmappedMesh = meshComp->GetMesh();
	}

	if (m_lightmappedMesh &&
		m_lightmappedMesh->subMeshes.empty())
		m_lightmappedMesh = nullptr;
}

void SceneLightmap::StartRendering()
{
	m_done = false;
	using namespace Bolts::Rendering;

	if (m_context) {
		lmDestroy(m_context);
		m_context = 0;
	}

	if (!m_lightmappedMesh)
		return;

	m_context = lmCreate(
		64,               // hemicube rendering resolution/quality
		0.001f, 100.0f,   // zNear, zFar
		1.0f, 1.0f, 1.0f, // sky/clear color
		2, 0.01f,          // hierarchical selective interpolation for speedup (passes, threshold)
		0.01f);            // modifier for camera-to-surface distance for hemisphere rendering.
						  // tweak this to trade-off between interpolated vertex normal quality and other artifacts (see declaration).
	BASSERT(m_context);

	m_lightmapData[0].resize(m_resolution.x * m_resolution.y);
	m_lightmapData[1].resize( m_lightmapData[0].size());
	memset(m_lightmapData[0].data(), 0, sizeof(m_lightmapData[0][0]) * m_lightmapData[0].size()); // clear lightmap to black
	lmSetTargetLightmap(m_context, &m_lightmapData[0][0].x, m_resolution.x, m_resolution.y, 3);

	auto& vbuffer = m_lightmappedMesh->subMeshes[0].primitiveBuffer->m_vertexBuffer;
	static constexpr uint8_t c_glVarSize[_BGVT_LAST] = { sizeof(float), 1, 1, 2, 2, 4, 4 };
	//BASSERT(vbuffer->m_layout.m_isSOA);
	int vertexSize = 0;
	int positionOffset = -1;
	int lightmapUVOffset = -1;
	int normalOffset = -1;
	for (auto attribInfo : vbuffer->m_layout.m_streams) {
		if (attribInfo.semantic == BGSS_POSITION)
			positionOffset = vertexSize;
		if (attribInfo.semantic == BGSS_NORMALS)
			normalOffset = vertexSize;
		if (attribInfo.semantic == BGSS_TEXTURE01)
			lightmapUVOffset = vertexSize; //TODO: Use UV1 when available
		vertexSize += attribInfo.componentNum * c_glVarSize[attribInfo.attributeType];
	}
	BASSERT(positionOffset != -1);
	BASSERT(lightmapUVOffset != -1);
	//TODO: This leaks memory as long as ReadAll doesn't check for Forever duplicates
	Blob::Reader meshData = IO::ReadAll(m_lightmappedMesh->GetAssetID(), IO::Forever);
	BASSERT(!meshData.IsEmpty());
	auto meshHeader = (const MeshHeader*)(meshData.Head());
	const char* vertices = meshHeader->subMeshes[0]->vertices->First();

	auto& ibuffer = m_lightmappedMesh->subMeshes[0].primitiveBuffer->m_indexBuffer;
	const char* indices = meshHeader->subMeshes[0]->indices->First();

	lmSetGeometry(m_context, &m_objWorld[0].x,
		LM_FLOAT, (uint8_t*)vertices + positionOffset, vertexSize,
		normalOffset != -1 ? LM_FLOAT : LM_NONE, (uint8_t*)vertices + normalOffset, vertexSize, // optional vertex normals for smooth surfaces
		LM_FLOAT, (uint8_t*)vertices + lightmapUVOffset, vertexSize,
		ibuffer->m_numIndices, ibuffer->m_indexType == BGVT_U16 ? LM_UNSIGNED_SHORT : LM_UNSIGNED_INT, indices);
}

void SceneLightmap::FinishRendering()
{
	if (m_context) {
		lmDestroy(m_context);
		m_context = 0;
	}
	BOLTS_LINFO() << "[Lightmap] Finished render. Post-processing ...";
	// postprocess and upload all lightmaps to the GPU now to use them for indirect lighting during the next bounce.
	// you can also call other lmImage* here to postprocess the lightmap.
	for (int i = 0; i < 4; i++) {
		lmImageDilate(&m_lightmapData[0][0].x, &m_lightmapData[1][0].x, m_resolution.x, m_resolution.y, 3);
		lmImageDilate(&m_lightmapData[1][0].x, &m_lightmapData[0][0].x, m_resolution.x, m_resolution.y, 3);
	}
	lmImageSmooth(&m_lightmapData[0][0].x, &m_lightmapData[1][0].x, m_resolution.x, m_resolution.y, 3);
	lmImageDilate(&m_lightmapData[1][0].x, &m_lightmapData[0][0].x, m_resolution.x, m_resolution.y, 3);
	// gamma correct and save lightmaps to disk
	//lmImagePower(mesh[i].lightmap, mesh[i].lightmapWidth, mesh[i].lightmapHeight, 3, 1.0f / 2.2f);
	//lmImageSaveTGAf(mesh[i].lightmapFilename, mesh[i].lightmap, mesh[i].lightmapWidth, mesh[i].lightmapHeight, 3);

	auto& commands = *m_assetManager.GetCommandBuffer();
	using namespace Bolts::Rendering;
	auto texDataOut = m_lightmapTexture->SetTexture(commands, BTT_2D, uvec3{ m_resolution, 1 }, BTF_RGB16F, BTDF_FLOAT, false);
	texDataOut.CopyFrom(m_lightmapData[0]);
	m_done = true;
	BOLTS_LINFO() << "[Lightmap] Done";
}

void SceneLightmap::RenderLM(Bolts::Rendering::Renderer& renderer)
{
	if (!m_context)
		return;

	const int numFrames = m_raysPerFrame;
	const float fFrames = 1.f / numFrames;
	static int frameCount = 0;
	frameCount = (frameCount + 1) % numFrames;

	auto be = renderer.GetBackend();

	// render all geometry to their lightmaps
	int vp[4];
	mat4 view, proj;
	vec3 cameraPos;
	int numPasses = 0;
	float lastProgress = 0.f;
	auto startTime = Bolts::System::GetElapsedMicroseconds();
	while (lmBegin(m_context, vp, &view[0][0], &proj[0][0], &cameraPos.x))
	{
		be->ClearShadowstate();
		numPasses++;
		// don't glClear on your own here!
		be->SetViewport({ vp[0] , vp[1] }, { vp[2] ,vp[3] });
		renderer.RenderCustomView(view, proj, cameraPos);
		float currentProgress = lmProgress(m_context);
		if (currentProgress > lastProgress + 0.2f) {
			BOLTS_LINFO() << "[Lightmap] Progress: " << currentProgress * 100.f << "%";
			lastProgress = currentProgress;
		}
		lmEnd(m_context);
	}
	float elapsedMs = Bolts::System::GetElapsedMs(startTime);
	BOLTS_LINFO() << "[Lightmap] " << numPasses << " passes took " << elapsedMs / 1000.f << " seconds";
	FinishRendering();
}

bool SceneLightmap::OnUpdate(double dt)
{
	using namespace Input;
	const float deltaT = (float)dt;

	m_KeyState.EventsHandled();
	m_MouseState.EventsHandled();

	// Tweakers
	if (ImGui::Begin("Lightmap Settings", nullptr, ImGuiWindowFlags_AlwaysAutoResize)) {
		auto& activeObjs = m_Space->GetActiveObjects();
		int s = m_selectedObj;
		ImGui::DragInt("Object", &s, 1.f, -1, activeObjs.size() - 1);
		if (s != m_selectedObj) {
			m_selectedObj = s;
			UpdateSelectedObj();
		}
		if (m_selectedObj != -1) {
			if (m_lightmappedMesh && m_lightmappedMesh->GetAssetName()) {
				ImGui::Text("Mesh: %s", m_lightmappedMesh->GetAssetName());
			}
		}

		int res = m_resolution.x;
		{
			ImGui::RadioButton("64", &res, 64); ImGui::SameLine();
			ImGui::RadioButton("128", &res, 128); ImGui::SameLine();
			ImGui::RadioButton("256", &res, 256); ImGui::SameLine();
			ImGui::RadioButton("512", &res, 512); ImGui::SameLine();
			ImGui::RadioButton("1024", &res, 1024); ImGui::SameLine();
			ImGui::RadioButton("2048", &res, 2048);
			ImGui::SameLine(); ImGui::Text("Resolution");
			m_resolution = uvec2{ (unsigned)res };
		}

		if (m_done) {
			ImGui::Image((ImTextureID)m_lightmapTexture->GetHandle().d, { (float)m_resolution.x, (float)m_resolution.y });
			if (ImGui::IsItemHovered()) {
				auto b1 = ImGui::GetItemRectMin();
				auto b2 = ImGui::GetItemRectMax();
				auto b3 = ImGui::GetCursorPos();
				auto b4 = ImGui::GetMousePos();
				auto b5 = ImGui::GetWindowPos();
				ImGui::BeginTooltip();
				ImGui::Image((ImTextureID)m_lightmapTexture->GetHandle().d, { (float)m_resolution.x * 4.f, (float)m_resolution.y *4.f });
				ImGui::EndTooltip();
			}
			if (ImGui::Button("Restart"))
				StartRendering();
		}
		else {
			if (m_context)
				ImGui::Text("Working ...");
			if (ImGui::Button("Start"))
				StartRendering();
		}

	//static int tex = 0;
	//static const char* s_texNames[] = { "Final","Radiance","Irradiance","Trace", "SDF", "Bla" };
	//auto bla = m_assetManager.LoadAsset<Bolts::Rendering::Texture>("gi_test1_alpha.tga");
	//const hash32_t s_textures[] = {
	//	m_presentResult.texHandle.d,
	//	m_updateResult.texHandle.d,
	//	m_accumGIPass.texHandle.d,
	//	m_raytracePass.texHandle.d,
	//	m_copySDF.texHandle.d,
	//	bla->GetHandle().d
	//};
	//ImGui::Combo("Texture", &tex, s_texNames, 6);
	//ImGui::Image((ImTextureID)s_textures[tex], { (float)m_giResolution.x, (float)m_giResolution.y});
	}
	ImGui::End();

	return true;
}


