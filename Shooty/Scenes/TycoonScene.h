#pragma once
#include "Menus/GameScene.h"
#include "Scenes/SceneStandard_NewRendering.h"
#include "Rendering/Mesh.h"
#include "Input/IOEvents.h"
#include "Helpers/Camera.h"

#include "Components/Space.h"
#include "Components/GOS.h"

using namespace Bolts;

namespace Tycoon
{
	class TycoonScene :
		public IGameScene,
		public StandardScene_RenderingNew,
		public nonCopyable_t
	{
	public:
		TycoonScene(Rendering::Renderer&, assets::AssetManager&, GOS&);
		~TycoonScene();

		void	OnRegister() override;
		void	OnPause() override {};
		void	OnResume() override {};
		void	OnDestroy() override;//Called when popped
		//
		bool	OnUpdate(double dt) override;
		void	SetupScene();
		void	ClearScene();
		//
		Space* GetActiveSpace() override { return m_space; }
		CameraPtr GetActiveCamera() override { return m_activeCamera; }
		//Input
		void	OnMouseEvent(Input::mouseEvent_t event) override;//WARN: Can be called anytime
		void	OnKeyEvent(Input::keyEvent_t event) override;//WARN: Can be called anytime
	private:
		//Resources
		void	LoadResources();
		//Gameplay
		GOS&					m_GOS;
		Space*					m_space = nullptr;

		Input::KeyStateHolder	keyState;
		Input::MouseStateHolder	mouseState;
	};
}

using TycoonScene = Tycoon::TycoonScene;
