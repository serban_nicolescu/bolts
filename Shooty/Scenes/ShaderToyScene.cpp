#include "stdafx.h"

#include "Scenes/ShaderToyScene.h"
#include "Components/Space.h"
#include "Components/MeshComponent.h"
#include "Rendering/MaterialNew.h"
#include "Rendering/Texture.h"
#include <Assets/AssetManager.h>

//#include "Serialization/PropertiesJSON.h"

#include <rapidxml.hpp>

#include <imgui.h>

using namespace Bolts;

SceneShaderToy::SceneShaderToy(Rendering::Renderer& rnd, assets::AssetManager& rs, GOS& gos):
	StandardScene_RenderingNew(rnd, rs), m_GOS(gos)
{
}

SceneShaderToy::~SceneShaderToy()
{

}

void SceneShaderToy::RenderPass::Init(Bolts::assets::AssetManager&	assetManager, const char* matName, Bolts::uvec2 outRes, Bolts::Rendering::TextureFormat outFmt, const char* outName)
{
	using namespace Rendering;
	mat = assetManager.LoadAsset<Material>( regHash_t(matName));

	auto& commands = *assetManager.GetCommandBuffer();
	regHash_t texName{ outName };
	outTex = new Texture(texName);
	outTex->Init(commands);
	outTex->SetRTTexture(commands, BTT_2D, uvec3{ outRes, 1 }, outFmt);
	assetManager.AddAsset(texName, Texture::c_assetType, outTex);
	BASSERT(outTex->GetAssetState() == assets::EAS_READY);
	texHandle = outTex->GetHandle();

	FB = commands.CreateResource(RenderResourceType::FRAMEBUFFER);
	auto updateRTCommand = commands.AddCommand<command::UpdateRTBindings>(FB);
	updateRTCommand->resolution = outRes;
	updateRTCommand->colorBindings[0] = texHandle;
}

void SceneShaderToy::RenderPass::Init(Bolts::assets::AssetManager & assetManager, const char * matName, Bolts::uvec2 outRes, Bolts::Rendering::TexturePtr outT)
{
	using namespace Rendering;
	mat = assetManager.LoadAsset<Material>(regHash_t(matName));
	outTex = outT;
	texHandle = outT->GetHandle();

	auto& commands = *assetManager.GetCommandBuffer();
	FB = commands.CreateResource(RenderResourceType::FRAMEBUFFER);
	auto updateRTCommand = commands.AddCommand<command::UpdateRTBindings>(FB);
	updateRTCommand->resolution = outRes;
	updateRTCommand->colorBindings[0] = texHandle;
}

void SceneShaderToy::RenderPass::Render(Bolts::Rendering::Renderer & renderer, Rendering::PrimitiveBuffer& fsMesh, bool clear)
{
	auto be = renderer.GetBackend();
	be->EnableRT(FB);
	if (clear) {
		be->SetClearColor(vec4_zero);
		be->Clear(Bolts::Rendering::FLAG_COLOR);
	}
	renderer.EnableMaterial(*mat);
	renderer.Draw(fsMesh, 0);
	be->DisableRT();
}

void SceneShaderToy::OnRegister()
{
	using namespace Bolts;
	using namespace Bolts::Rendering;
	//Cam Init
	m_mainCamera.SetUpVector( vec3( 0.f, 0.f, 1.f ) );
	m_mainCamera.SetZNear(0.01f);
	m_mainCamera.SetZFar( 10.f );
	// Setup scene
	m_Space = m_GOS.NewSpace();
	m_mainCamera.SetPosition({ 0.f, 1.f,0.f });
	m_GOS.Start(m_Space);

	// load resources
	m_assetManager.LoadAssetFile<Material>("2DGI.matpack");
	m_fullscreenQuadMesh = m_assetManager.LoadAsset<Mesh>("FullscreenQuad");


	m_drawHook = [this](Bolts::Rendering::Renderer& r) { Render2DGI(r); };

	// create render passes
	m_wallsPass.Init(m_assetManager, "2DGIWalls", m_giResolution, BTF_R, "2DGIWalls");
	m_generateSdfPass.Init(m_assetManager, "2DGenerateSDF", m_giResolution, BTF_R16F, "2DGISDF");
	m_raytracePass.Init(m_assetManager, "2DGIRaytraceSDF", m_giResolution, BTF_RGBA16F, "2DGIRaytrace");
	m_accumGIPass.Init(m_assetManager, "2DGIAccumulate", m_giResolution, BTF_RGBA16F, "2DGIAccum");
	m_inflateGIPass.Init(m_assetManager, "2DGIInflateIrr", m_giResolution, m_raytracePass.outTex);
	m_updateResult.Init(m_assetManager, "2DUpdateRadiance", m_giResolution, BTF_RGBA16F, "2DRadiance");
	
	// hook up inputs

	if (auto updateResultSceneTex = m_updateResult.mat->GetParams().TryGet<MatParameterHolder::TextureParam>("u_scene"))
	{
		BASSERT(updateResultSceneTex->tex);
		m_wallsPass.mat->EditParams().Set("u_scene", updateResultSceneTex->tex);
	}
	//auto& sceneEmission = m_updateResult.mat->GetParams().GetParam<MatParameterHolder::TexParamInfo>("u_sceneEmissive");
	//m_seedScene.mat->EditParams().Set("u_tex", sceneEmission.handle);
	m_generateSdfPass.mat->EditParams().Set("u_trace", m_wallsPass.outTex);
	m_generateSdfPass.mat->EditParams().Set("u_rayInfo", vec3{});
	m_raytracePass.mat->EditParams().Set("u_rayInfo", vec3{});
	m_raytracePass.mat->EditParams().Set("u_sdf", m_generateSdfPass.outTex);
	m_raytracePass.mat->EditParams().Set("u_radiance", m_updateResult.outTex);
	m_accumGIPass.mat->EditParams().Set("u_irr", m_raytracePass.outTex);
	m_inflateGIPass.mat->EditParams().Set("u_irr", m_accumGIPass.outTex);
	m_updateResult.mat->EditParams().Set("u_irr", m_inflateGIPass.outTex);
	m_inflateGIPass.mat->EditParams().Set("u_walls", m_wallsPass.outTex);

	m_copySDF.Init(m_assetManager, "CopyTexture#src_r", m_giResolution, BTF_RGB, "TexPreview");
	m_presentResult.Init(m_assetManager, "CopyTexture", m_giResolution, BTF_RGB, "2DGIRender");
	m_presentResult.mat->EditParams().Set("u_tex", m_updateResult.outTex);
}

void SceneShaderToy::OnDestroy()
{
	//TODO: Destroy all RTs
	m_GOS.DestroySpace(m_Space);
}

void SceneShaderToy::OnResume()
{
	m_renderer.AddRenderHack(&m_drawHook);
}

void SceneShaderToy::OnPause()
{
	m_renderer.RemoveRenderHack(&m_drawHook);
}


void SceneShaderToy::Render2DGI(Bolts::Rendering::Renderer& renderer)
{
	vec4 rtRes{ float(m_giResolution.x), float(m_giResolution.y), 1.f / float(m_giResolution.x), 1.f / float(m_giResolution.y) };
	m_raytracePass.mat->EditParams().Set("u_rtResolution", rtRes);
	m_generateSdfPass.mat->EditParams().Set("u_rtResolution", rtRes);
	m_accumGIPass.mat->EditParams().Set("u_rtResolution", rtRes);
	m_inflateGIPass.mat->EditParams().Set("u_rtResolution", rtRes);
	m_wallsPass.mat->EditParams().Set("u_rtResolution", rtRes);
	m_copySDF.mat->EditParams().Set("u_rtResolution", rtRes);
	m_updateResult.mat->EditParams().Set("u_rtResolution", rtRes);
	m_presentResult.mat->EditParams().Set("u_rtResolution", rtRes);

	const int numFrames = 8;
	const float fFrames = 1.f / numFrames;
	static int frameCount = 0;
	frameCount = (frameCount + 1) % numFrames;

	auto be = renderer.GetBackend();
	be->SetViewport({}, m_giResolution);

	// gen SDF
	if (!sdfDone) {
		sdfDone = true;

		// walls
		m_wallsPass.Render(renderer, *m_fullscreenQuadMesh->subMeshes[0].primitiveBuffer, /* clear = */ true);
		m_wallsPass.outTex->GenerateMipmaps(renderer.GetCommandBuffer());
		renderer.FlushResourceCommands();
		
		be->EnableRT(m_generateSdfPass.FB);
		{
			auto& rtParams = m_generateSdfPass.mat->EditParams();
			if (auto rayDirParam = rtParams.TryEdit<vec3>("u_rayInfo"))
			{
				be->SetClearColor(vec4(5000.f));
				be->Clear(Bolts::Rendering::FLAG_COLOR);
				const float numRays = (float)512;
				rayDirParam->z = 1.f / numRays;
				const float angleStep = (c_PI * 2.f) * rayDirParam->z;
				for (float i = 0.f; i < numRays; i += 1.f)
				{
					float angle = angleStep * i;
					rayDirParam->x = cos(angle);
					rayDirParam->y = sin(angle);

					renderer.EnableMaterial(*m_generateSdfPass.mat);
					renderer.Draw(*m_fullscreenQuadMesh->subMeshes[0].primitiveBuffer, 0);
				}
			}
		}
		be->DisableRT();

		//update preview texture
		m_copySDF.mat->EditParams().Set("u_tex", m_generateSdfPass.outTex);
		m_copySDF.Render(renderer, *m_fullscreenQuadMesh->subMeshes[0].primitiveBuffer, /* clear = */ true);
	}

	//raytrace
	be->EnableRT(m_raytracePass.FB);
	{
		auto& rtParams = m_raytracePass.mat->EditParams();
		if (auto rayDirParam = rtParams.TryEdit<vec3>("u_rayInfo"))
		{
			be->SetClearColor(vec4_zero);
			be->Clear(Bolts::Rendering::FLAG_COLOR);
			//static int rayNum = -1;
			//ImGui::SliderInt("Ray", &rayNum, 0, m_numberOfRays);
			const float numRays = (float)m_numberOfRays;
			rayDirParam->z = 1.f / numRays;
			const float angleStep = (c_PI * 2.f) * rayDirParam->z;
			const float angleOffset = angleStep * fFrames * frameCount;
			//float i = (float)rayNum;
			for (float i = 0.f; i < numRays; i += 1.f)
			{
				float angle = angleOffset + angleStep * i;
				rayDirParam->x = cos(angle);
				rayDirParam->y = sin(angle);

				renderer.EnableMaterial(*m_raytracePass.mat);
				renderer.Draw(*m_fullscreenQuadMesh->subMeshes[0].primitiveBuffer, 0);
			}
		}
	}
	be->DisableRT();
	// accumulate irradiance
	m_accumGIPass.Render(renderer, *m_fullscreenQuadMesh->subMeshes[0].primitiveBuffer, /* clear = */ false);

	// spread irradiance info over walls
	m_inflateGIPass.Render(renderer, *m_fullscreenQuadMesh->subMeshes[0].primitiveBuffer, /* clear = */ false);

	// work out radiance
	m_updateResult.Render(renderer, *m_fullscreenQuadMesh->subMeshes[0].primitiveBuffer, /* clear = */ true);

	// render result offscreen
	m_presentResult.Render(renderer, *m_fullscreenQuadMesh->subMeshes[0].primitiveBuffer, /* clear = */ true);
}

bool SceneShaderToy::OnUpdate( double dt )
{
	using namespace Input;
	const float deltaT = (float) dt;

	m_KeyState.EventsHandled();
	m_MouseState.EventsHandled();

	if (m_timePlaying){
		m_time += deltaT * m_timeSpeed;
		if (m_timeMax > 0.f && m_time >= m_timeMax)
			m_time = 0.f;
	}

	m_mouse = vec4();
	m_mouse.x = m_MouseState.GetMousePosition().x;
	m_mouse.y = m_MouseState.GetMousePosition().y;
	if ( m_MouseState.IsButtonDown( Input::B_RIGHT ) ) {
		m_mouse.z = m_MouseState.GetMousePosition().x;
		m_mouse.w = m_MouseState.GetMousePosition().y;
	}

	//if (m_KeyState.KeyReleased(Input::KC_ENTER))
	//	LoadToyFile(m_configName);

	// Tweakers
	ImGui::Begin("Toy Settings", nullptr, ImGuiWindowFlags_AlwaysAutoResize);
	//ImGui::Text("Current Time: %f", m_time);
	//ImGui::InputFloat("Time Speed", &m_timeSpeed);
	//ImGui::InputFloat("Clamp Time", &m_timeMax);
	//if (m_timeMax > 0.f)
	//	ImGui::SliderFloat("Time", &m_time, 0.f, m_timeMax);
	//if (ImGui::Button("PlayPause"))
	//	m_timePlaying = !m_timePlaying;
	//if (ImGui::Button("Reset"))
	//	m_time = 0.f;
	//ImGui::NewLine();
	//
	//if (ImGui::Button("Reload Toy File"))
	//	LoadToyFile(m_configName);

	{
		ImGui::RadioButton("8", &m_numberOfRays, 8); ImGui::SameLine();
		ImGui::RadioButton("16", &m_numberOfRays, 16); ImGui::SameLine();
		ImGui::RadioButton("32" , &m_numberOfRays, 32); ImGui::SameLine();
		ImGui::RadioButton("64" , &m_numberOfRays, 64); ImGui::SameLine();
		ImGui::RadioButton("128", &m_numberOfRays, 128); ImGui::SameLine();
		ImGui::RadioButton("256", &m_numberOfRays, 256); ImGui::SameLine();
		ImGui::RadioButton("512", &m_numberOfRays, 512); ImGui::SameLine();
		ImGui::RadioButton("1024", &m_numberOfRays, 1024); ImGui::SameLine();
		ImGui::RadioButton("2048", &m_numberOfRays, 2048);
		ImGui::SameLine(); ImGui::Text("Rays");
	}

	if (ImGui::Button("Wall SDF"))
		sdfDone = false;

	static int tex = 0;
	static const char* s_texNames[] = { "Final","Radiance","Irradiance","Trace", "SDF", "Bla" };

	auto bla = m_assetManager.LoadAsset<Bolts::Rendering::Texture>("gi_test1_alpha.tga");

	const hash32_t s_textures[] = {
		m_presentResult.texHandle.d,
		m_updateResult.texHandle.d,
		m_accumGIPass.texHandle.d,
		m_raytracePass.texHandle.d,
		m_copySDF.texHandle.d,
		bla->GetHandle().d
	};
	ImGui::Combo("Texture", &tex, s_texNames, 6);
	ImGui::Image((ImTextureID)s_textures[tex], { (float)m_giResolution.x, (float)m_giResolution.y});
	ImGui::End();

	m_FrameCount++;
	return true;
}

typedef const rapidxml::xml_node<char>* xmlNodePtr_t;
typedef const rapidxml::xml_attribute<char>* xmlAttPtr_t;

template<class T>
bool xml_node_is(const T* node, const char* name)
{
	return strncmp(node->name(), name, node->name_size()) == 0;
}

template<class T>
bool xml_value_is(const T* node, const char* val)
{
	return strncmp(node->value(), val, node->value_size()) == 0;
}

bool xml_prop_is(xmlNodePtr_t pNode, const char* name)
{
	if (auto att = pNode->first_attribute("name"))
		return xml_value_is(att, name);
	return false;
}

void copy_substr(const str_t& str, int startPos, int endPos, char* out)
{
	BASSERT(endPos > startPos);
	BASSERT(endPos <= str.size());
	BASSERT(out);
	BASSERT(endPos - startPos < 512);
	memcpy(out, str.data() + startPos, endPos - startPos);
	out[endPos] = 0;
}

bool xml_create_rtt(xmlNodePtr_t rtNode, AssetManager& assetMgr, hash_map_t< hash32_t, vec2>* relativeTexMap, uvec2 windowSize)
{
	using namespace Bolts;
	auto att = rtNode->first_attribute("name");
	if (att){
		//TODO:Restore
		/*
		char scratchPad[512];
		const stringHash_t texName = HashString(att->value(), att->value_size());
		auto texPtr = assetMgr.LoadAsset<Rendering::Texture>(texName);
		texPtr->SetAssetState(assets::EAS_READY);
		auto& texData = texPtr->EditData();
		for (auto prop = rtNode->first_node(); prop; prop = prop->next_sibling())
		{
			if (xml_prop_is(prop, "format")){
				if (xml_value_is(prop, "RGB"))
					texData.format = Rendering::BTF_RGB;
				else if (xml_value_is(prop, "RGBA"))
					texData.format = Rendering::BTF_RGBA;
				else if (xml_value_is(prop, "RGBA16F"))
					texData.format = Rendering::BTF_RGBA_16F;
				else if (xml_value_is(prop, "R"))
					texData.format = Rendering::BTF_R;
				else
					BASSERT_MSG(false, "Unknown RT format");
			}else if (xml_prop_is(prop, "size")){
				str_t propVal(prop->value(), prop->value_size());
				auto sep = propVal.find(';');
				if (sep == std::string::npos) //TODO: Do we support 1-dimensional textures ?
					continue; 
				texData.type = Rendering::BTT_2D;
				uvec3 val;
				copy_substr(propVal, 0, sep, scratchPad);
				val[0] = atoi(scratchPad);
				copy_substr(propVal, sep+1, propVal.size(), scratchPad);
				val[1] = atoi(scratchPad);
				if (propVal[sep - 1] == '%'){ // Relative size RT
					vec2 relativeFactor{ val.x / 100.f, val.y / 100.f };
					texData.dimensions = uvec3(relativeFactor.x * windowSize.x,relativeFactor.y * windowSize.y, 0);
					relativeTexMap->insert({ texName, relativeFactor });
				}
				else{
					texData.dimensions = val;
				}
			}

		}
		*/
		return true;
	}
	else{
		BOLTS_LERROR() << "TOY: \"rt\" node doesn't have a \"name\" attribute for the texture name";
		return false;
	}
}

void SceneShaderToy::LoadToyFile(const str_t& filename)
{
	auto blob = IO::ReadAll( HashString(filename), IO::Temp);
	if (blob.IsEmpty()) {
		BOLTS_LERROR() << "Couldn't find toy file: " << filename;
		return;
	}

	//m_passes.clear();
	// Parse .toy file
	Blob data;
	data.SetCapacity(blob.Remaining()+1);
	data.Store(blob.Head(), blob.Remaining());
	data.Store('\0');
	rapidxml::xml_document<> doc;
	try {
		doc.parse<0>((char*)data.Data());
	}
	catch (rapidxml::parse_error e){
		BOLTS_LERROR() << "Error parsing toy file: " << filename;
		BOLTS_LERROR() << "	" << e.what();
		BOLTS_LERROR() << "	" << e.where<char>();
		return;
	}
	auto toyNode = doc.first_node();
	for (auto node = toyNode->first_node(); node; node = node->next_sibling()){
		if (xml_node_is(node, "pass")){
			using namespace Bolts;
			auto att = node->first_attribute("mat");
			if (att){
				//TODO: Restore
				/*Rendering::RenderTargetPtr rt;
				auto outputNode = node->first_node("output");
				if (outputNode && outputNode->value_size() > 1){
					stringHash_t texName = HashString(outputNode->value(), outputNode->value_size());
					auto outTexPtr = m_assetManager.LoadAsset<Rendering::Texture>(texName);
					if (outTexPtr){
						rt = new Rendering::RenderTarget();
						Rendering::RenderTarget::RTSetup setup;
						setup.textureDimensions = uvec2(outTexPtr->GetDimensions()) ;
						setup.withDepth = false;
						setup.colorRTTextures.push_back(outTexPtr);
						rt->Setup(std::move(setup));
					}
				}
				auto mat = m_assetManager.LoadAsset<Rendering::Material>(HashString(att->value(), att->value_size()));
				m_passes.push_back({ mat, rt});*/
			}
			else{
				BOLTS_LERROR() << "TOY: \"pass\" node doesn't have a \"mat\" attribute with the material name";
			}
		}
		else if (xml_node_is(node, "matfile")){
			auto att = node->first_attribute("name");
			if (att){
				m_assetManager.LoadAssetFile<Rendering::Material>( HashString(att->value(), att->value_size()));
			}
			else{
				BOLTS_LERROR() << "TOY: \"matfile\" node doesn't have a \"name\" attribute with the material file name";
			}
		}
		else if (xml_node_is(node, "rt")){
			xml_create_rtt(node, m_assetManager, &m_bufferRelativeSizes, m_renderer.GetWindowSize());
		}
	}
}

void SceneShaderToy::OnMouseEvent( Input::mouseEvent_t event )
{
	using namespace Input;

	m_MouseState.OnMouseEvent( event );
}

void SceneShaderToy::OnKeyEvent( Input::keyEvent_t event )
{
	m_KeyState.OnKeyEvent( event );
}

