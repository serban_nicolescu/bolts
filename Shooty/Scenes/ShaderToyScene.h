#pragma once
#include "Menus/GameScene.h"
#include "Scenes/SceneStandard_NewRendering.h"
#include "Rendering/Mesh.h"
#include "Input/IOEvents.h"
#include "Helpers/Camera.h"

#include "Components/Space.h"
#include "Components/GOS.h"

class SceneShaderToy:
	public IGameScene,
	public StandardScene_RenderingNew,
	public Bolts::nonCopyable_t
{
public:
	SceneShaderToy(Bolts::Rendering::Renderer&, Bolts::assets::AssetManager&, Bolts::GOS&);
	~SceneShaderToy();

	void	OnRegister() override;
	void	OnPause() override;
	void	OnResume() override;
	void	OnDestroy() override;//Called when popped

	bool	OnUpdate( double dt ) override;
	void	Render2DGI(Bolts::Rendering::Renderer&);
	//
	void	OnMouseEvent( Bolts::Input::mouseEvent_t event ) override;//WARN: Can be called anytime
	void	OnKeyEvent( Bolts::Input::keyEvent_t event ) override;//WARN: Can be called anytime
	//
	Bolts::Space* GetActiveSpace() override { return m_Space; }
	Bolts::CameraPtr GetActiveCamera() override { return m_activeCamera; }
private:
	void LoadToyFile(const Bolts::str_t& filename);

	//Gameplay
	unsigned					m_FrameCount = 0;
	Bolts::GOS&					m_GOS;
	Bolts::Space*				m_Space;
	Bolts::vec_t< Bolts::goRef_t> m_GameObjects;
	Bolts::Input::KeyStateHolder	m_KeyState;
	Bolts::Input::MouseStateHolder	m_MouseState;

	struct FullScreenPass
	{
		Bolts::Rendering::MaterialPtr	mat;
	};

	Bolts::uvec2	m_prevWindowSize;


	Bolts::Rendering::MeshPtr	m_fullscreenQuadMesh;
	Bolts::Rendering::Renderer::renderHack_t m_drawHook;

	// Raytracing
	int				m_numberOfRays = 64;
	Bolts::uvec2	m_giResolution{1024};
	struct RenderPass
	{
		void Init(Bolts::assets::AssetManager&	assetManager, const char* matName, Bolts::uvec2 outRes, Bolts::Rendering::TextureFormat outFmt, const char* outName);
		void Init(Bolts::assets::AssetManager&	assetManager, const char* matName, Bolts::uvec2 outRes, Bolts::Rendering::TexturePtr outTex);
		void Render(Bolts::Rendering::Renderer& renderer, Bolts::Rendering::PrimitiveBuffer& fsMesh, bool clear);

		Bolts::Rendering::MaterialPtr	mat;
		Bolts::Rendering::TexturePtr	outTex;
		Bolts::Rendering::resHandle_t	texHandle;
		Bolts::Rendering::resHandle_t	FB;
	};

	bool sdfDone = false;
	RenderPass					m_wallsPass;
	RenderPass					m_generateSdfPass;
	RenderPass					m_copySDF; // for preview
	RenderPass					m_raytracePass;
	RenderPass					m_accumGIPass;
	RenderPass					m_inflateGIPass;
	RenderPass					m_updateResult;
	RenderPass					m_presentResult;

	//Bolts::str_t m_configName = "simple.toy";
	//Bolts::vec_t< FullScreenPass> m_passes;
	Bolts::hash_map_t< Bolts::hash32_t, Bolts::vec2> m_bufferRelativeSizes; // Holds the viewport-relative factor for textures. Used when changing window size

	bool	m_timePlaying = true;
	float	m_timeSpeed = 1.f;
	float	m_time = 0.f;
	float	m_timeMax = 100.f;
	Bolts::vec4 m_mouse;
};
