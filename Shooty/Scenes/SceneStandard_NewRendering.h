#pragma once
#include "Components/GameObject.h"
#include "Components/MeshComponent.h"
#include "Rendering/Mesh.h"
#include "Helpers/Camera.h"
#include <Assets/Assets.h>

class StandardScene_RenderingNew
{
protected:
	StandardScene_RenderingNew(Bolts::Rendering::Renderer& rnd, Bolts::assets::AssetManager& am) :
		m_renderer(rnd),
		m_assetManager(am) 
	{
		SetActiveCamera(m_mainCamera);
		m_mainCamera.onReferenceAdd();
	}

	void	SetActiveCamera(Bolts::Camera& cam) { m_activeCamera = &cam; }

	Bolts::Rendering::Renderer&		m_renderer;
	Bolts::assets::AssetManager&	m_assetManager;
	Bolts::Camera					m_mainCamera;
	Bolts::CameraPtr				m_activeCamera;
};
