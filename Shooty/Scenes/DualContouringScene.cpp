#include "stdafx.h"

#include "Scenes/DualContouringScene.h"
#include "Components/Space.h"
#include "Components/MeshComponent.h"
//TODO: Move to scene rendering sources
#include "Helpers/MeshBuilder.h"
#include "Rendering/MaterialNew.h"
#include <Assets/AssetManager.h>

#include "Serialization/PropertiesJSON.h"

#include <imgui.h>

using namespace Bolts;

SceneDualContouring::SceneDualContouring(Rendering::Renderer& rnd, assets::AssetManager& rs, GOS& gos) :
	StandardScene_RenderingNew(rnd, rs), m_GOS(gos)
{
}

SceneDualContouring::~SceneDualContouring()
{

}

void SceneDualContouring::OnRegister()
{
	using namespace Bolts;
	//Cam Init
	m_mainCamera.SetUpVector(vec3(0.f, 0.f, 1.f));
	m_mainCamera.SetZNear(0.01f);
	m_mainCamera.SetZFar(10.f);
	//
	SetupScene();
}

void SceneDualContouring::SetupScene()
{
	using namespace Bolts;

	m_Space = m_GOS.NewSpace();
	m_mainCamera.SetPosition({ 0.f, 1.f,0.f });
	//Spawn objects here
	GOPrototype meshObj;
	meshObj.m_components.push_back(MeshComponent::Tag);
	// Spawn some objects
	// Dual Contouring test object
	m_testHermiteDataMesh = m_assetManager.LoadAsset<Rendering::Mesh>("HermiteTest");
	m_GameObjects.push_back(m_GOS.SpawnGO(m_Space, meshObj));
	m_GameObjects.back()->TryGetComponent<CMgrMesh>()->SetMesh("HermiteTest");
	m_GameObjects.back()->TryGetComponent<CMgrMesh>()->SetMaterial(0, "Phong#green");
	//
	m_GOS.Start(m_Space);
}

void SceneDualContouring::GenerateDCMesh()
{
	using namespace Bolts;
	//TODO: Add time measuring code here
	// Dual Contouring test
	dc::GenHermiteData([](vec3 uv) {
		vec3 p = uv * 2.f - 1.f;
		auto noise = [](vec3 x) {
			vec3 p = floor(x);
			vec3 f = fract(x);
			f = f*f*(3.f - 2.f*f);

			float n = p.x + p.y*157.f + 113.f*p.z;

			vec4 v1 = fract(753.5453123f*sin(n + vec4(0.f, 1.f, 157.f, 158.f)));
			vec4 v2 = fract(753.5453123f*sin(n + vec4(113.f, 114.f, 270.f, 271.f)));
			vec4 v3 = mix(v1, v2, f.z);
			vec2 v4 = glm::mix(vec2(v3.x, v3.y), vec2(v3.z, v3.w), f.y);
			return glm::mix(v4.x, v4.y, f.x);
		};

		auto fnoise = [noise](vec3 p) {
			// random rotation reduces artifacts
			p = mat3(0.28862355854826727, 0.6997227302779844, 0.6535170557707412,
				0.06997493955670424, 0.6653237235314099, -0.7432683571499161,
				-0.9548821651308448, 0.26025457467376617, 0.14306504491456504)*p;
			return dot(vec4(noise(p), noise(p*2.f), noise(p*4.f), noise(p*8.f)),
				vec4(0.5, 0.25, 0.125, 0.06));
		};

		auto sdBox = [](vec3 p, vec3 s)
		{
			vec3 diffVec = abs(p) - s;
			float surfDiff_Outter = glm::length(glm::max(diffVec, 0.f));
			float surfDiff_Inner = glm::min(glm::max(diffVec.z, glm::max(diffVec.x, diffVec.y)), 0.f);
			return surfDiff_Outter + surfDiff_Inner;
		};
		auto sdTorus = [](vec3 p, vec2 t)
		{
			float distPtoTorusCircumference = glm::length(vec2(glm::length(vec2(p.x, p.z)) - t.x, p.y));
			return distPtoTorusCircumference - t.y;
		};

		return glm::min(sdTorus(p, { 0.5f, 0.1f }), sdBox(p, { 0.2f, 0.2f, 1.f })) + (fnoise(p)* 0.25f);
		//return sdTorus(p, { 0.5f, 0.1f });
		//return glm::length(p) - 0.25f;
		//return p.x;
	}, m_hermiteDataResolution, m_intersectionMethod, m_intersectionDistanceTol, m_maxIntersectionSteps, m_testHermiteData);
	dc::GenDCMesh(*m_assetManager.GetCommandBuffer(), m_testHermiteData, m_testHermiteDataMesh.get());
}

void SceneDualContouring::OnDestroy()
{
	m_GOS.DestroySpace(m_Space);
}

bool SceneDualContouring::OnUpdate(double dt)
{
	using namespace Input;
	const float deltaT = (float)dt;
	{
		//Move the camera
		using namespace Bolts;
		const float c_CamSpeed = m_KeyState.IsKeyDown(Input::KC_SHIFT) ? 1.f : 0.1f;
		if (m_KeyState.IsKeyDown(keyCode_t('W')))
			m_mainCamera.MoveForward(deltaT * c_CamSpeed);
		//if (m_KeyState.IsKeyDown(keyCode_t('Q')))
		//	m_mainCamera.Tilt(deltaT * 1.1f);
		//if (m_KeyState.IsKeyDown(keyCode_t('E')))
		//	m_mainCamera.Tilt(-deltaT * 1.1f);
		if (m_KeyState.IsKeyDown(keyCode_t('S')))
			m_mainCamera.MoveForward(-deltaT * c_CamSpeed);
		if (m_KeyState.IsKeyDown(keyCode_t('A')))
			m_mainCamera.MoveSideways(deltaT * c_CamSpeed);
		if (m_KeyState.IsKeyDown(keyCode_t('D')))
			m_mainCamera.MoveSideways(-deltaT * c_CamSpeed);
	}

	m_KeyState.EventsHandled();
	m_MouseState.EventsHandled();

	const vec2 mouseMove = m_MouseState.GetMouseMoveAmount();
	if (m_MouseState.IsButtonDown(Input::B_RIGHT)) {
		const float c_sensitivity = 0.8f;
		m_mainCamera.LookRight(c_PI * c_sensitivity * mouseMove.x);
		m_mainCamera.LookUp(c_PI * c_sensitivity * mouseMove.y);
	}
	// Tweakers
	ImGui::Begin("Mesh Generation Settings", nullptr, ImGuiWindowFlags_AlwaysAutoResize);
	ImGui::SliderInt("Volume Resolution", &m_hermiteDataResolution, 4, 256);
	ImGui::SliderInt("Intersection Method", &m_intersectionMethod, 1, 4);
	ImGui::SliderFloat("Intersection Tolerance", &m_intersectionDistanceTol, 0.1f, 0.000001f, "%.7f");
	ImGui::SliderInt("Intersection Max Steps", &m_maxIntersectionSteps, 1, 100);
	if (ImGui::Button("Update Mesh"))
		GenerateDCMesh();

	ImGui::End();

	m_FrameCount++;
	return true;
}

void SceneDualContouring::OnMouseEvent(Input::mouseEvent_t event)
{
	using namespace Input;

	m_MouseState.OnMouseEvent(event);
}

void SceneDualContouring::OnKeyEvent(Input::keyEvent_t event)
{
	m_KeyState.OnKeyEvent(event);
}

