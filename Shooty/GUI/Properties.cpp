#include "StdAfx.h"
#include "Properties.h"
#include <imgui.h>

//Template compilation times opt
//https://www.youtube.com/watch?v=NPWQ7xKfIHQ

using namespace Bolts;

propVec_t CopyPropVec(const propVec_t& source)
{
	propVec_t dest;
	for (auto& propPtr : source){
		dest.emplace_back(propPtr->Clone());
	}
	return dest;
}

void PropVecToBinary(const propVec_t& src, Bolts::BinaryMapBuilder& builder)
{
	builder.Clear();
	builder.ResizeKeys(src.size());
	for (auto& prop : src) {
		prop->ToBinary(builder);
	}
}

namespace Bolts {
	static void (*g_assetOpenCB) (uint32_t assetType, hash32_t assetID) = nullptr;

	void SetAssetOpenCallback( void cbFunc (uint32_t, hash32_t))
	{
		g_assetOpenCB = cbFunc;
	}

	void OpenAssetDetails(uint32_t assetType, hash32_t assetID)
	{
		if (g_assetOpenCB)
			g_assetOpenCB(assetType, assetID);
	}

	// Type rendering specialisations. Shared between value & array properties.
	// To be used by non-property rendering in the future ( E.g. component "dynamic" properties )
	//TODO: Global flags can be set to affect rendering. E.g. READ_ONLY, SLIDER, DRAG

	void ImGuiSameLine()
	{
		ImGui::SameLine();
	}

	void ImGuiIndent() 
	{
		ImGui::Indent();
	}

	void ImGuiUnindent()
	{
		ImGui::Unindent();
	}

	void ImGuiDisplay(const char * text)
	{
		ImGui::Text(text);
	}

	void ImGuiDisplaySep()
	{
		ImGui::Separator();
	}

	bool ImGuiButton(const char * label)
	{
		return ImGui::Button(label);
	}

	void ImGuiImage(uint32_t resId, int w, int h)
	{
		ImGui::Image((ImTextureID)resId, { (float)w, (float)h });
	}

	bool ImGuiEdit(const char* label, float* val, float vmin, float vmax) {
		return ImGui::SliderFloat(label, val, vmin, vmax);
	}

	bool ImGuiEdit(const char* label, int* val, int vmin, int vmax) {
		return ImGui::SliderInt(label, val, vmin, vmax);
	}

	bool ImGuiEdit(const char* label, bool* val) {
		return ImGui::Checkbox(label, val);
	}

	bool ImGuiEdit(const char* label, ivec2* val) {
		return ImGui::InputInt2(label, &val->x, ImGuiInputTextFlags_EnterReturnsTrue);
	}

	bool ImGuiEdit(const char* label, uvec2* val) {
		//TODO: Unsigned only input
		int v[2]; v[0] = val->x; v[1] = val->y;
		return ImGui::InputInt2(label, v, ImGuiInputTextFlags_EnterReturnsTrue);
	}
	bool ImGuiEdit(const char* label, vec2* val) {
		return ImGui::InputFloat2(label, &val->x, -1, ImGuiInputTextFlags_EnterReturnsTrue);
	}
	bool ImGuiEdit(const char* label, vec3* val) {
		return ImGui::InputFloat3(label, &val->x, -1, ImGuiInputTextFlags_EnterReturnsTrue);
	}
	bool ImGuiEdit(const char* label, vec4* val) {
		return ImGui::InputFloat4(label, &val->x, -1, ImGuiInputTextFlags_EnterReturnsTrue);
	}

	bool ImGuiEditAsColor(const char* label, vec3* val) {
		return ImGui::ColorEdit3(label, &val->x);
	}
	bool ImGuiEditAsColor(const char* label, vec4* val) {
		return ImGui::ColorEdit4(label, &val->x);
	}

	bool ImGuiEdit(const char* label, quat* val) {
		Bolts::vec3 rotDeg = glm::degrees(glm::eulerAngles(*val));
		if (ImGui::InputFloat3(label, &rotDeg.x, -1, ImGuiInputTextFlags_EnterReturnsTrue)) {
			*val = quat{ glm::radians(rotDeg) };
			return true;
		}
		return false;
	}

	bool ImGuiEdit(const char* label, str_t* val) {
		char buff[256];
		strcpy(buff, val->c_str());
		if (ImGui::InputText(label, buff, 256, ImGuiInputTextFlags_EnterReturnsTrue)) {
			*val = buff;
			return true;
		}
		else return false;
	}

	bool ImGuiEdit(const char* label, regHash_t* val) {
		char buff[256];
		const char* b = buff;
		if (val->s)
			strcpy(buff, val->s);
		else
			strcpy(buff, val->h ? "???" : "");
		if (ImGui::InputText(label, buff, 256, ImGuiInputTextFlags_EnterReturnsTrue)) {
			*val = regHash_t(b);
			return true;
		}
		else return false;
	}
};

// Value Properties

template<>
PropValue<bool>::PropValue() :IProp(TypeToPropType<ValT>::k_PropType), val(true)
{
}

void PropAsset::SetVal(const char* newName)
{
	assetID = { newName };
}
void PropAsset::SetVal(Bolts::str_t newName)
{
	assetID = { newName };
}
void PropAsset::SetVal(Bolts::hash32_t aID)
{
	if (assetID.h != aID) {
		assetID = { aID };
	}
}

template<class ValT, PropType PT>
bool PropValue<ValT, PT>::Draw()
{
	BASSERT(false);// Unimplemented
	ImGui::Text("%s property | %s", k_propTypeInfo[type].s, name.c_str());
	return false;
}

template bool PropValue<float>::Draw();
template bool PropValue<int32_t>::Draw();
template bool PropValue<Bolts::ivec2>::Draw();
template bool PropValue<Bolts::uvec2>::Draw();
template bool PropValue<bool>::Draw();
template bool PropValue<Bolts::vec3>::Draw();
template bool PropValue<Bolts::vec3, PROP_COLOR3>::Draw();
template bool PropValue<Bolts::vec4, PROP_COLOR4>::Draw();
template bool PropValue<Bolts::quat>::Draw();
template bool PropValue<Bolts::str_t>::Draw();

template<>
bool PropValue<float>::Draw() {
	return ImGuiEdit( name.c_str(), &val, 0.f, 10.f);
}

template<>
bool PropValue<int32_t>::Draw() {
	return ImGuiEdit( name.c_str(), &val, 0, 10 );
}

template<>
bool PropValue<bool>::Draw() {
	return ImGuiEdit(name.c_str(), &val);
}

template<>
bool PropValue<Bolts::ivec2>::Draw() {
	return ImGuiEdit(name.c_str(), &val);
}

template<>
bool PropValue<Bolts::uvec2>::Draw() {
	return ImGuiEdit(name.c_str(), &val);
}

template<>
bool PropValue<Bolts::vec3>::Draw() {
	return ImGuiEdit(name.c_str(), &val);
}

template<>
bool PropColor3::Draw() {
	return ImGuiEditAsColor(name.c_str(), &val);
}

template<>
bool PropColor4::Draw() {
	return ImGuiEditAsColor(name.c_str(), &val);
}

template<>
bool PropValue<Bolts::quat>::Draw() {
	return ImGuiEdit(name.c_str(), &val);
}

template<>
bool PropValue<Bolts::str_t>::Draw() {
	return ImGuiEdit(name.c_str(), &val);
}

bool PropAsset::Draw() {
	ImGui::PushID(this);
	if (ImGui::Button(">"))
		Bolts::OpenAssetDetails(assetType, assetID.h);
	ImGui::PopID();
	ImGui::SameLine();
	return ImGuiEdit(name.c_str(), &assetID);
}

// Prop Array

//TODO: BinaryMap array of strings
//void ::detail::PropValueArrayImpl<Bolts::str_t>::ToBinary(Bolts::BinaryMapBuilder& builder)
//{
//	builder.Store(Bolts::HashString(name), val);
//}



static char c_buff[128];

// Generic array drawing impl. Type specialisations can customise how individual array entries are handled
template<class D, class T, PropType PT>
bool ::detail::PropValueArrayImpl<D, T, PT>::Draw()
{
	bool anyChanges = false;
	// Header
	ImGui::LabelText(name.c_str(), "%s Num elements: %d", expanded ? "-" : "+", val.size());
	//ImGui::InvisibleButton("contextButton", { 100, ImGui::GetFrameHeight() });
	if (ImGui::IsItemClicked())
		expanded = !expanded;
	//TODO: This just needs a unique ID to work
	//if (ImGui::BeginPopupContextItem()) {
	//	if (ImGui::Button("Clear")) {
	//		val.clear();
	//		anyChanges = true;
	//	}
	//
	//	ImGui::EndPopup();
	//}
	// Elements
	if (expanded) {
		ImGui::Indent();
		int copiedIdx = -1;
		auto valData = val.data();
		for (unsigned i = 0; i < val.size(); i++) {
			// Label
			auto& v = valData[i];
			sprintf(c_buff, "%d", i);
			ImGui::PushID(&v);
			// Entry
			if (Derived()->DrawElement(c_buff, v))
				anyChanges = true;
			// Popup menu
			if (ImGui::BeginPopupContextItem(c_buff)) {
				if (ImGui::Button("Copy")) {
					copiedIdx = i;
				}
				if (ImGui::Button("Move Up")) {
					if (i > 0) {
						std::swap(val[i], val[i - 1]);
						anyChanges = true;
					}
				}
				if (ImGui::Button("Move Down")) {
					if (i < val.size() - 1) {
						std::swap(val[i], val[i + 1]);
						anyChanges = true;
					}
				}
				ImGui::EndPopup();
			}
			ImGui::PopID();
		}
		// Do copy if requested
		if (copiedIdx != -1) {
			val.emplace_back(val[copiedIdx]);
			anyChanges = true;
		}
		ImGui::Unindent();
	}

	return anyChanges;
}

template<class T, PropType ET, PropType AT>
bool PropValueArray<T, ET, AT>::DrawElement(const char* label, ValT& val)
{
	return ImGuiEdit(label, &val);
}

template<class T, PropType ET, PropType AT>
inline void PropValueArray<T, ET, AT>::ToBinary(Bolts::BinaryMapBuilder & builder)
{
	builder.Store(Bolts::HashString(name), val);
}

template<class T, PropType ET, PropType AT>
bool PropValueArray<T, ET, AT>::Draw()
{
	return PropValueArrayImpl::Draw();
}

template bool PropValueArray<float>::Draw();
template bool PropValueArray<int32_t>::Draw();
template bool PropValueArray<Bolts::ivec2>::Draw();
template bool PropValueArray<Bolts::uvec2>::Draw();
template bool PropValueArray<Bolts::vec3>::Draw();
template bool PropValueArray<Bolts::vec3, PROP_COLOR3>::Draw();
template bool PropValueArray<Bolts::vec4, PROP_COLOR4>::Draw();
template bool PropValueArray<Bolts::quat>::Draw();

template bool PropValueArray<float>::DrawElement(const char* label, ValT& val);
template bool PropValueArray<int32_t>::DrawElement(const char* label, ValT& val);
template bool PropValueArray<Bolts::ivec2>::DrawElement(const char* label, ValT& val);
template bool PropValueArray<Bolts::uvec2>::DrawElement(const char* label, ValT& val);
template bool PropValueArray<Bolts::vec3>::DrawElement(const char* label, ValT& val);
template bool PropValueArray<Bolts::vec3, PROP_COLOR3>::DrawElement(const char* label, ValT& val);
template bool PropValueArray<Bolts::vec4, PROP_COLOR4>::DrawElement(const char* label, ValT& val);
template bool PropValueArray<Bolts::quat>::DrawElement(const char* label, ValT& val);

template<> bool PropValueArray<float>::DrawElement(const char* label, ValT& val)
{
	return ImGuiEdit(label, &val, 0.f, 10.f);
}
template<> bool PropValueArray<int32_t>::DrawElement(const char* label, ValT& val)
{
	return ImGuiEdit(label, &val, 0, 10);
}
template<> bool PropValueArray<Bolts::vec3, PROP_COLOR3>::DrawElement(const char* label, ValT& val)
{
	return ImGuiEditAsColor(label, &val);
}
template<> bool PropValueArray<Bolts::vec4, PROP_COLOR4>::DrawElement(const char* label, ValT& val)
{
	return ImGuiEditAsColor(label, &val);
}

bool PropAssetV::Draw()
{
	return PropValueArrayImpl::Draw();
}

void PropAssetV::ToBinary(Bolts::BinaryMapBuilder & builder)
{
	Bolts::hash32_t* outArray = (Bolts::hash32_t*)builder.ReserveArray<Bolts::hash32_t>(Bolts::HashString(name), val.size());
	auto valData = val.data();
	for (unsigned i = 0; i < val.size(); i++) {
		outArray[i] = valData[i];
	}
}

bool PropAssetV::DrawElement(const char * label, ValT& val)
{
	if (ImGui::Button(">") && Bolts::g_assetOpenCB)
		Bolts::g_assetOpenCB(assetType, val.h);
	ImGui::SameLine();
	return ImGuiEdit(name.c_str(), &val);
}


///////////////////////////////

struct PropTypeInfo
{
	PropTypeInfo() = default;

	const char*		typeName = nullptr;
	hash32_t		nameHash = 0;

	IProp* (*ctor) () = nullptr;
	void (*visit) (IProp* iprop, IPropVisitor* visitor) = nullptr;
	void (*visitConst) (const IProp* iprop, IConstPropVisitor* visitor) = nullptr;
};

std::array<PropTypeInfo, _PROP_COUNT> k_propTypeInfo = {};

template<typename T>
struct RegisterProp
{
	RegisterProp(const char* typeName) {
		auto& info = k_propTypeInfo[T::PropT];
		info.typeName = typeName;
		info.nameHash = HashString(typeName);
		info.ctor = []() -> IProp* { return new T; };
		info.visit = [](IProp* iprop, IPropVisitor* visitor) {
			visitor->Visit(static_cast<T*>(iprop));
		};
		info.visitConst = [](const IProp* iprop, IConstPropVisitor* visitor) {
			visitor->Visit(static_cast<const T*>(iprop));
		};
	}
};

#define REGISTER_PROP(pname) \
	const RegisterProp<Prop##pname##>	Prop##pname##::Reg = #pname
#define REGISTER_PROP_ARRAY(pname) \
	const RegisterProp<Prop##pname##V>	Prop##pname##V::Reg = #pname"Array"

REGISTER_PROP(Float);
REGISTER_PROP(Int);
REGISTER_PROP(IVec2);
REGISTER_PROP(UVec2);
REGISTER_PROP(Bool);
REGISTER_PROP(Vec3);
REGISTER_PROP(Quat);
REGISTER_PROP(String);
REGISTER_PROP(Asset);
REGISTER_PROP(Color4);
REGISTER_PROP(Color3);

REGISTER_PROP_ARRAY(Float);
REGISTER_PROP_ARRAY(Int);
REGISTER_PROP_ARRAY(IVec2);
REGISTER_PROP_ARRAY(UVec2);
//REGISTER_PROP_ARRAY(Bool);
REGISTER_PROP_ARRAY(Vec3);
REGISTER_PROP_ARRAY(Quat);
//REGISTER_PROP_ARRAY(String);
REGISTER_PROP_ARRAY(Asset);
REGISTER_PROP_ARRAY(Color4);
REGISTER_PROP_ARRAY(Color3);

#undef REGISTER_PROP
#undef REGISTER_PROP_ARRAY

PropType GetPropTypeFromHash(Bolts::hash32_t typeHash)
{
	for (unsigned i = 0; i < k_propTypeInfo.size(); i++) {
		if (k_propTypeInfo[i].nameHash == typeHash)
			return PropType(i);
	}
	BASSERT(false);
	return _PROP_COUNT;
}

const char* GetPropTypeName(PropType type)
{
	BASSERT(type < _PROP_COUNT);
	return k_propTypeInfo[type].typeName;
}

Bolts::hash32_t GetPropTypeHash(PropType type)
{
	BASSERT(type < _PROP_COUNT);
	return k_propTypeInfo[type].nameHash;
}

IProp* MakeProp(Bolts::hash32_t typeHash)
{
	return MakeProp(GetPropTypeFromHash(typeHash));
}

IProp* MakeProp( PropType pt )
{
	if (pt < k_propTypeInfo.size() && k_propTypeInfo[pt].ctor) {
		return k_propTypeInfo[pt].ctor();
	}
	BASSERT( false );
	return nullptr;
}

IProp* PropVecGetProp(propVec_t & pvec, const char * searchedName)
{
	for (auto& iprop : pvec) {
		if (iprop->name == searchedName) {
			return iprop.get();
		}
	}
	return nullptr;
}
//
void VisitProp(IProp* iprop, IPropVisitor* visitor)
{
	if (!iprop || iprop->type >= k_propTypeInfo.size()) {
		BASSERT(false);
		return;
	}
	k_propTypeInfo[iprop->type].visit(iprop, visitor);
}

void VisitProp(const IProp* iprop, IConstPropVisitor* visitor)
{
	if (!iprop || iprop->type >= k_propTypeInfo.size()) {
		BASSERT(false);
		return;
	}
	k_propTypeInfo[iprop->type].visitConst(iprop, visitor);
}

static void PropCastTest()
{
	IProp* fprop = MakeProp(PROP_FLOAT);
	BASSERT(PropCast<float>(fprop));
	BASSERT(PropCast<PROP_FLOAT>(fprop));
	const IProp* cfprop = MakeProp(PROP_FLOAT);
	BASSERT(PropCast<float>(cfprop));
	BASSERT(PropCast<PROP_FLOAT>(cfprop));
	IProp* aprop = MakeProp(PROP_ASSET);
	BASSERT(PropCast<PROP_ASSET>(aprop));
}
