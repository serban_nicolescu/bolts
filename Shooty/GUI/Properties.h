#pragma once
#include <bolts_binarymap.h>
#include <bolts_core.h>
#include <memory>


namespace Bolts {
	void SetAssetOpenCallback(void cbFunc (uint32_t assetType, hash32_t assetID));
	void OpenAssetDetails(uint32_t assetType, hash32_t assetID);

	void ImGuiDisplay(const char* text);
	void ImGuiDisplaySep();
	void ImGuiSameLine();
	void ImGuiIndent();
	void ImGuiUnindent();
	bool ImGuiButton(const char* label);
	void ImGuiImage(uint32_t resId, int w, int h);

	// Type editing specialisations
	bool ImGuiEdit(const char* label, float* val, float vmin, float vmax);
	bool ImGuiEdit(const char* label, int* val, int vmin, int vmax);
	bool ImGuiEdit(const char* label, bool* val);
	bool ImGuiEdit(const char* label, ivec2* val);
	bool ImGuiEdit(const char* label, uvec2* val);
	bool ImGuiEdit(const char* label, vec2* val);
	bool ImGuiEdit(const char* label, vec3* val);
	bool ImGuiEdit(const char* label, vec4* val);
	bool ImGuiEditAsColor(const char* label, vec3* val);
	bool ImGuiEditAsColor(const char* label, vec4* val);
	bool ImGuiEdit(const char* label, quat* val);
	bool ImGuiEdit(const char* label, str_t* val);
	bool ImGuiEdit(const char* label, regHash_t* val);
};

enum PropType
{
	PROP_FLOAT,
	PROP_INT,
	PROP_IVEC2,
	PROP_UVEC2,
	PROP_BOOL,
	PROP_VEC3,
	PROP_QUAT,
	PROP_STRING,
	PROP_ASSET,
	PROP_COLOR4,
	PROP_COLOR3,
	_PROP_ARRAY_START,
	_PROP_ARRAY_LAST = _PROP_ARRAY_START*2 - 1,
	_PROP_COUNT
};

class IProp;
typedef Bolts::vec_t < std::unique_ptr<IProp> > propVec_t;

//
PropType		GetPropTypeFromHash(Bolts::hash32_t typeNameHash);
const char*		GetPropTypeName(PropType);
Bolts::hash32_t	GetPropTypeHash(PropType);

// Factory functions
IProp*		MakeProp(PropType pt);
IProp*		MakeProp(Bolts::hash32_t typeNameHash);

template <class T>
T* MakeDefaultProp(const char* iname, typename T::ValT ival)
{
	auto p = new T;
	p->name = iname;
	p->val = ival;
	return p;
}

template <class T>
T* MakeDefaultProp(const char* name)
{
	auto p = new T;
	p->name = name;
	return p;
}

template<typename T>
struct RegisterProp;

//

class IProp
{
public:
	IProp(PropType pt) :type(pt) {};
	virtual ~IProp(){};

	virtual void ToBinary(Bolts::BinaryMapBuilder& builder) = 0;
	//virtual void Serialize() = 0;
	virtual IProp* Clone() const = 0;
	virtual bool Draw() = 0;

	Bolts::str_t	name;
	PropType		type;
};

// Implementations

template<typename>
struct TypeToPropType {};
template<>
struct TypeToPropType < float > { static const PropType k_PropType = PROP_FLOAT; };
template<>
struct TypeToPropType < int32_t > { static const PropType k_PropType = PROP_INT; };
template<>
struct TypeToPropType < Bolts::ivec2 > { static const PropType k_PropType = PROP_IVEC2; };
template<>
struct TypeToPropType < Bolts::uvec2 > { static const PropType k_PropType = PROP_UVEC2; };
template<>
struct TypeToPropType < bool > { static const PropType k_PropType = PROP_BOOL; };
template<>
struct TypeToPropType <  Bolts::vec3 > { static const PropType k_PropType = PROP_VEC3; };
template<>
struct TypeToPropType <  Bolts::quat > { static const PropType k_PropType = PROP_QUAT; };
template<>
struct TypeToPropType <  Bolts::str_t > { static const PropType k_PropType = PROP_STRING; };
template<>
struct TypeToPropType <  Bolts::hash32_t > { static const PropType k_PropType = PROP_ASSET; };
template<>
struct TypeToPropType <  Bolts::regHash_t > { static const PropType k_PropType = PROP_ASSET; };

template<class T, PropType PT = TypeToPropType<T>::k_PropType>
class PropValue: public IProp
{
public:
	typedef T ValT;
	static const PropType PropT = PT;
	static const RegisterProp<PropValue> Reg;
	static PropValue* Cast(IProp* p) { return (p->type == PropT) ? static_cast<PropValue*>(p) : nullptr; }
	static const PropValue* Cast(const IProp* p) { return (p->type == PropT) ? static_cast<const PropValue*>(p) : nullptr; }

	PropValue() :IProp(PropT),val() {};

	void ToBinary( Bolts::BinaryMapBuilder& builder ) override
	{
		builder.Store(Bolts::HashString(name), val);
	}

	IProp* Clone() const override
	{
		auto temp = new PropValue<T, PT>();
		temp->name = name;
		temp->val = val;
		return temp;
	}

	void SetVal(ValT newV) { val = newV; }

	bool Draw() override;
	ValT val;
};

class PropAsset : public IProp
{
public:
	static const PropType PropT = PROP_ASSET;
	static PropAsset* Cast(IProp* p) { return (p->type == PropT) ? static_cast<PropAsset*>(p) : nullptr; }
	static const PropAsset* Cast(const IProp* p) { return (p->type == PropT) ? static_cast<const PropAsset*>(p) : nullptr; }
	static const RegisterProp<PropAsset> Reg;

	PropAsset() :IProp(PropT) {};

	void ToBinary(Bolts::BinaryMapBuilder& builder) override
	{
		builder.Store(Bolts::HashString(name), Bolts::hash32_t(assetID));
	}

	IProp* Clone() const override
	{
		auto temp = new PropAsset();
		*temp = *this;
		return temp;
	}

	bool Draw() override;
	void SetVal(const char*);
	void SetVal(Bolts::str_t);
	void SetVal(Bolts::hash32_t assetID);
	void SetAssetType(int at) { assetType = at; }

	Bolts::regHash_t	assetID;
	int					assetType = 0;
private:
	PropAsset & operator=(const PropAsset&) = default;
};

typedef PropValue<float>		PropFloat;
typedef PropValue<int32_t>		PropInt;
typedef PropValue<Bolts::uvec2>	PropUVec2;
typedef PropValue<Bolts::ivec2>	PropIVec2;
typedef PropValue<bool>			PropBool;
typedef PropValue<Bolts::vec3>	PropVec3;
typedef PropValue<Bolts::vec3, PROP_COLOR3>	PropColor3;
typedef PropValue<Bolts::vec4, PROP_COLOR4>	PropColor4;
typedef PropValue<Bolts::quat>	PropQuat;
typedef PropValue<Bolts::str_t>	PropString;

// Array Properties

template<typename T>
struct TypeToArrayPropType {
	static const PropType k_PropType = (PropType) ((typename TypeToPropType<T>::k_PropType) + _PROP_ARRAY_START);
};

namespace detail {
	template<class D, class T, PropType PT = TypeToArrayPropType<T>::k_PropType>
	class PropValueArrayImpl :public IProp
	{
	public:
		typedef T ValT;
		typedef Bolts::vec_t<ValT> ValArrayT;

		static const PropType PropT = PT; // this is the Array prop type
		static D*		Cast(IProp* p) { return (p->type == PropT) ? static_cast<D*>(p) : nullptr; }
		static const D*	Cast(const IProp* p) { return (p->type == PropT) ? static_cast<const D*>(p) : nullptr; }

		PropValueArrayImpl() :IProp(PropT) {};

		bool Draw() override;
		IProp* Clone() const override
		{
			auto temp = new D();
			*temp = *Derived();
			return temp;
		}

		size_t				GetValCount() const { return val.size(); }
		const ValArrayT&	GetVal() const { return val; }
		ValArrayT&			EditVal() { return val; }
		void				SetVal(const ValArrayT& newv) { val = newv; }

	protected:
		PropValueArrayImpl & operator=(const PropValueArrayImpl&) = default;

		ValArrayT val;
		bool expanded = false;
	private:
		const D* Derived() const { return static_cast<const D*>(this); }
		D* Derived() { return static_cast<D*>(this); }
	};
};

template<class T, PropType ET = TypeToPropType<T>::k_PropType, PropType AT = (PropType)(ET + _PROP_ARRAY_START)>
class PropValueArray :public ::detail::PropValueArrayImpl<PropValueArray<T,ET,AT>,T,AT>
{
public:
	friend class ::detail::PropValueArrayImpl<PropValueArray, T, AT>;
	static const RegisterProp<PropValueArray> Reg;

	bool Draw() override;
	void ToBinary(Bolts::BinaryMapBuilder& builder) override;
protected:
	bool DrawElement(const char* label, ValT& val);
};

class PropAssetV :public ::detail::PropValueArrayImpl<PropAssetV, Bolts::regHash_t>
{
public:
	friend class ::detail::PropValueArrayImpl<PropAssetV, Bolts::regHash_t>;
	static const RegisterProp<PropAssetV> Reg;

	bool Draw() override;
	void ToBinary(Bolts::BinaryMapBuilder& builder) override;

	uint32_t	assetType = (uint32_t)-1;
private:
	PropAssetV& operator=(const PropAssetV&) = default;

	bool DrawElement(const char* label, ValT& val);
};

// Array Property Types

typedef PropValueArray<float>						PropFloatV;
typedef PropValueArray<int32_t>						PropIntV;
typedef PropValueArray<Bolts::uvec2>				PropUVec2V;
typedef PropValueArray<Bolts::ivec2>				PropIVec2V;
//typedef PropValueArray<bool>						PropBoolV;
typedef PropValueArray<Bolts::vec3>					PropVec3V;
typedef PropValueArray<Bolts::vec3, PROP_COLOR3>	PropColor3V;
typedef PropValueArray<Bolts::vec4, PROP_COLOR4>	PropColor4V;
typedef PropValueArray<Bolts::quat>					PropQuatV;
//typedef PropValueArray<Bolts::str_t>				PropStringV;
//typedef PropValueArray<Bolts::regHash_t>			PropAssetV;

template<>
inline PropAssetV* MakeDefaultProp<PropAssetV>(const char* name) {
	static_assert(true, "don't use this");
	return nullptr;
}

template<>
inline PropAsset* MakeDefaultProp<PropAsset>(const char* name) {
	static_assert(true, "don't use this");
	return nullptr;
}

inline PropAsset* MakeDefaultPropAssetRef(const char* name, uint32_t type) {
	auto p = new PropAsset();
	p->name = name;
	p->assetType = type;
	return p;
}

inline PropAssetV* MakeDefaultPropAssetRefV(const char* name, uint32_t type) {
	auto p = new PropAssetV();
	p->name = name;
	p->assetType = type;
	return p;
}

// 

template<int>
struct PropTypeToValType {};
template<>
struct PropTypeToValType < PROP_FLOAT > { typedef PropFloat T; };
template<>
struct PropTypeToValType < PROP_INT > { typedef PropInt T; };
template<>
struct PropTypeToValType < PROP_IVEC2 > { typedef PropIVec2 T; };
template<>
struct PropTypeToValType < PROP_UVEC2 > { typedef PropUVec2 T; };
template<>
struct PropTypeToValType < PROP_BOOL > { typedef PropBool T; };
template<>
struct PropTypeToValType < PROP_VEC3 > { typedef PropVec3 T; };
template<>
struct PropTypeToValType < PROP_COLOR3 > { typedef PropColor3 T; };
template<>
struct PropTypeToValType < PROP_COLOR4 > { typedef PropColor4 T; };
template<>
struct PropTypeToValType < PROP_QUAT > { typedef PropQuat T; };
template<>
struct PropTypeToValType < PROP_STRING > { typedef PropString T; };
template<>
struct PropTypeToValType < PROP_ASSET > { typedef PropAsset T; };
template<>
struct PropTypeToValType < _PROP_ARRAY_START + PROP_FLOAT > { typedef PropFloatV T; };
template<>
struct PropTypeToValType < _PROP_ARRAY_START + PROP_INT > { typedef PropIntV T; };
template<>
struct PropTypeToValType < _PROP_ARRAY_START + PROP_IVEC2 > { typedef PropIVec2V T; };
template<>
struct PropTypeToValType < _PROP_ARRAY_START + PROP_UVEC2 > { typedef PropUVec2V T; };
//template<>
//struct PropTypeToValType < _PROP_ARRAY_START + PROP_BOOL > { typedef PropBoolV T; };
template<>
struct PropTypeToValType < _PROP_ARRAY_START + PROP_VEC3 > { typedef PropVec3V T; };
template<>
struct PropTypeToValType < _PROP_ARRAY_START + PROP_COLOR3 > { typedef PropColor3V T; };
template<>
struct PropTypeToValType < _PROP_ARRAY_START + PROP_COLOR4 > { typedef PropColor4V T; };
template<>
struct PropTypeToValType < _PROP_ARRAY_START + PROP_QUAT > { typedef PropQuatV T; };
//template<>
//struct PropTypeToValType < _PROP_ARRAY_START + PROP_STRING > { typedef PropStringV T; };
template<>
struct PropTypeToValType < _PROP_ARRAY_START + PROP_ASSET > { typedef PropAssetV T; };

template<PropType PT>
typename PropTypeToValType<PT>::T* PropCast(IProp* p) {
	return (p->type == PT ? static_cast< PropTypeToValType<PT>::T* >(p) : nullptr);
}
template<PropType PT>
const typename PropTypeToValType<PT>::T* PropCast(const IProp* p) {
	return (p->type == PT ? static_cast< const typename PropTypeToValType<PT>::T* >(p) : nullptr);
}
template<typename T>
PropValue<T>* PropCast(IProp* p) {
	return (p->type == TypeToPropType<T>::k_PropType ? static_cast< PropValue<T>* >(p) : nullptr);
}
template<typename T>
const PropValue<T>* PropCast(const IProp* p) {
	return (p->type == TypeToPropType<T>::k_PropType ? static_cast< const PropValue<T>* >(p) : nullptr);
}

// Prop Vectors

//TODO: All of these "propVec" helpers are a sign that I need a more unified way to deal with props
propVec_t CopyPropVec(const propVec_t&);

void PropVecToBinary(const propVec_t&, Bolts::BinaryMapBuilder&);

template<PropType PT>
auto PropVecGetProp(const propVec_t& pvec, const char* searchedName) -> decltype(PropCast<PT>(std::declval<const IProp*>()))
{
	for (auto const& iprop : pvec){
		if (iprop->name == searchedName){
			return PropCast<PT>(iprop.get());
		}
	}
	return nullptr;
}

template<PropType PT>
auto PropVecGetProp(propVec_t& pvec, const char* searchedName) -> decltype(PropCast<PT>(std::declval<IProp*>()))
{
	for (auto& iprop : pvec){
		if (iprop->name == searchedName){
			return PropCast<PT>(iprop.get());
		}
	}
	return nullptr;
}

IProp* PropVecGetProp(propVec_t& pvec, const char* searchedName);


// Visitors

class IPropVisitor
{
public:
	virtual void Visit(PropFloat*) {};
	virtual void Visit(PropInt*) {};
	virtual void Visit(PropIVec2*) {};
	virtual void Visit(PropUVec2*) {};
	virtual void Visit(PropBool*) {};
	virtual void Visit(PropVec3*) {};
	virtual void Visit(PropColor3*) {};
	virtual void Visit(PropColor4*) {};
	virtual void Visit(PropQuat*) {};
	virtual void Visit(PropString*) {};
	virtual void Visit(PropAsset*) {};

	virtual void Visit(PropFloatV*) {};
	virtual void Visit(PropIntV*) {};
	virtual void Visit(PropIVec2V*) {};
	virtual void Visit(PropUVec2V*) {};
	//virtual void Visit(PropBoolV*) {};
	virtual void Visit(PropVec3V*) {};
	virtual void Visit(PropColor3V*) {};
	virtual void Visit(PropColor4V*) {};
	virtual void Visit(PropQuatV*) {};
	//virtual void Visit(PropStringV*) {};
	virtual void Visit(PropAssetV*) {};
};

class IConstPropVisitor
{
public:
	virtual void Visit(const PropFloat*) {};
	virtual void Visit(const PropInt*) {};
	virtual void Visit(const PropIVec2*) {};
	virtual void Visit(const PropUVec2*) {};
	virtual void Visit(const PropBool*) {};
	virtual void Visit(const PropVec3*) {};
	virtual void Visit(const PropColor3*) {};
	virtual void Visit(const PropColor4*) {};
	virtual void Visit(const PropQuat*) {};
	virtual void Visit(const PropString*) {};
	virtual void Visit(const PropAsset*) {};

	virtual void Visit(const PropFloatV*) {};
	virtual void Visit(const PropIntV*) {};
	virtual void Visit(const PropIVec2V*) {};
	virtual void Visit(const PropUVec2V*) {};
	//virtual void Visit(const PropBoolV*) {};
	virtual void Visit(const PropVec3V*) {};
	virtual void Visit(const PropColor3V*) {};
	virtual void Visit(const PropColor4V*) {};
	virtual void Visit(const PropQuatV*) {};
	//virtual void Visit(const PropStringV*) {};
	virtual void Visit(const PropAssetV*) {};
};

void VisitProp(IProp*, IPropVisitor*);
void VisitProp(const IProp*, IConstPropVisitor*);