#include "StdAfx.h"
#include "ImGuiGraph.h"

#ifndef IMGUI_DEFINE_MATH_OPERATORS
#define IMGUI_DEFINE_MATH_OPERATORS
#endif
#include "imgui_internal.h"

#include <cstdint> // uint32_t

//example
//https://github.com/effekseer/ImGradientHDR

//TODO: non-linear interpolation
//TODO: edit multi-component values
//TODO: expanded graph on hover

// helpers

static inline float lerp(float a, float b, float t)
{
	return a + (b - a) * t;
}

static inline float min(float a, float b) { return a <= b ? a : b; }
static inline float max(float a, float b) { return a >= b ? a : b; }

static inline bool eqf(float a, float b, float eps) {
	return abs(a - b) < eps;
}

static inline bool eqf(float a, float b) {
	return abs(a - b) < 0.005f;
}


//

/*

static inline void setQNaN(float& f)
{
	*reinterpret_cast<uint32_t*>(&f) = 0xFF800000;
}

//NOTE: t=NaN marks uninitialised nodes. Not using builtin functions in case code is compiled with fast-math (untested)
static bool myIsQNaN(float f) 
{
	return (*reinterpret_cast<uint32_t*>(&f) == 0xFF800000);
}

// A graph is a sequence of nodes
// each node is a seq of floats, starting with the t coord, followed by components
//e.g. a 3-component graph: t0 x0 y0 z0 t1 x1 y1 z1 t2 x2 y2 z2 ...
// Nodes are sorted in ascending t order. The last node in a graph has setQNaN(t[N])

int imgraph_node_count(const float* nodes, int maxN, int nComp)
{
	int r = 0;
	int stride = nComp + 1;
	while (r < maxN && myIsQNaN(nodes[r++ * stride]));
	return r;
}

static inline void imgraph_read(const float* nodes, int nComp, int i, float* out) 
{
	memcpy(out, &nodes[(1 + nComp) * i + 1], 4 * nComp);
}


static inline void imgraph_interp(const float* nodes, int nComp, int i, float t, float* out) 
{
	int stride = (1 + nComp);
	const float* src = nodes + (stride * i);
	float t0 = src[0];
	float t1 = src[stride];
	float tnorm =  (t - t0) / (t1 - t0);
	for (int c = 0; c < nComp; c++)
		out[c] = lerp(src[c + 1], src[c + 1 + stride], tnorm);
}

// returns -1 0 when "t" is smaller than ti0
// returns ti0 maxN when "t" is after the last node
static void imgraph_find(const float* nodes, int maxN, int nComp, float t, int* ti0, int* ti1)
{
	int ti = 0;
	float ct;
	while (ti < maxN) {
		ct = nodes[ti * (1 + nComp)];
		if (myIsQNaN(ct) || ct > t)
			break;
		ti++;
	}
	*ti0 = ti - 1, *ti1 = ti;
	if (ti == 0) {
		*ti0 = 0; 
		*ti1 = -1;
	}
	if (myIsQNaN(ct))
		*ti1 = maxN;
}

void imgraph_eval(const float* nodes, int maxN, int nComp, float t, float* out)
{
	if (myIsQNaN(nodes[0])) { // empty graph
		memset(out, 0, 4 * nComp);
		return;
	}
	// find time
	int t0, t1;
	imgraph_find(nodes, maxN, nComp, t, &t0, &t1);
	//
	if (t1 < 0 || t1 == maxN)
		imgraph_read(nodes, nComp, t0, out);
	else
		imgraph_interp(nodes, nComp, t0, t, out);
}

void imgraph_insert(float* nodes, int maxN, int nComp, float t, const float* val)
{
	int t0, t1;
	imgraph_find(nodes, maxN, nComp, t, &t0, &t1);
	int stride = 1 + nComp;

	if (t1 < 0)
		t1 = 0

	//float t1t = nodes[stride * t1];
	//if ( !myIsQNaN(t1t) && t1t < t)
	//	t1++;
	//else {
	//	int t2 = t1 + 1;
	//	if (t2 < maxN)
	//		memcpy(&nodes[stride * t2], &nodes[stride * t1], 4 * stride);
	//}
	//if (t1 < maxN) {
	//	nodes[stride * t1] = t;
	//	memcpy(&nodes[stride * t1 + 1], val, 4 * nComp);
	//}
	
}


void imgraph_edit(const char* label, float* nodes, int maxN, int nComp, float tMin, float tMax)
{
	using namespace ImGui;

	if (nComp > 4)
		nComp = 4;

	ImVec2 frame_size = { 0.f, 0.f };
	float scale_min = FLT_MAX, scale_max = FLT_MAX;
	int sample_count = 32;

	ImGuiContext& g = *GImGui;
	ImGuiWindow* window = GetCurrentWindow();
	if (window->SkipItems)
		return;

	const ImGuiStyle& style = g.Style;
	const ImGuiID id = window->GetID(label);

	const ImVec2 label_size = CalcTextSize(label, NULL, true);
	if (frame_size.x == 0.0f)
		frame_size.x = CalcItemWidth();
	if (frame_size.y == 0.0f)
		frame_size.y = label_size.y + (style.FramePadding.y * 2);

	const ImRect frame_bb(window->DC.CursorPos, window->DC.CursorPos + frame_size);
	const ImRect inner_bb(frame_bb.Min + style.FramePadding, frame_bb.Max - style.FramePadding);
	const ImRect total_bb(frame_bb.Min, frame_bb.Max + ImVec2(label_size.x > 0.0f ? style.ItemInnerSpacing.x + label_size.x : 0.0f, 0));
	ItemSize(total_bb, style.FramePadding.y);
	if (!ItemAdd(total_bb, 0, &frame_bb))
		return;
	const bool hovered = ItemHoverable(frame_bb, id);

	// Determine scale from values if not specified
	if (scale_min == FLT_MAX || scale_max == FLT_MAX)
	{
		float v[8]; v[0] = v[4] = FLT_MAX;
		imgraph_eval(nodes, maxN, nComp, tMin, v);
		imgraph_eval(nodes, maxN, nComp, tMax, v + 4);
		float v_min = v[0];
		float v_max = v[0];
		for (int i = 0; i < nComp; i++) {
			v_min = ImMin(v_min, v[i]);
			v_min = ImMin(v_min, v[i+4]);
			v_max = ImMax(v_max, v[i]);
			v_max = ImMax(v_max, v[i+4]);
		}
		if (scale_min == FLT_MAX)
			scale_min = v_min;
		if (scale_max == FLT_MAX)
			scale_max = v_max;
	}

	RenderFrame(frame_bb.Min, frame_bb.Max, GetColorU32(ImGuiCol_FrameBg), true, style.FrameRounding);

	int idx_hovered = -1;
	int res_w = ImMin((int)frame_size.x, sample_count);
	int item_count = 1 + sample_count;

	if (scale_max == scale_min)
		scale_max = scale_min + 1.f;

	const float t_step = (tMax - tMin) / (float)res_w;
	const float inv_scale = (scale_min == scale_max) ? 0.0f : (1.0f / (scale_max - scale_min));
	const float tNorm = 1.f / (tMax - tMin);

	// Tooltip on hover
	if (hovered && inner_bb.Contains(g.IO.MousePos))
	{
		const float mouseT = lerp(tMin, tMax, ImClamp((g.IO.MousePos.x - inner_bb.Min.x) / (inner_bb.Max.x - inner_bb.Min.x), 0.0f, 0.9999f));

		float v[4];
		imgraph_eval(nodes, maxN, nComp, mouseT, v);
		//	const int v_idx = (int)(t * item_count);
		//	IM_ASSERT(v_idx >= 0 && v_idx < values_count);
		//
		//	const float v0 = values_getter(data, (v_idx + values_offset) % values_count);
		//	const float v1 = values_getter(data, (v_idx + 1 + values_offset) % values_count);
		//	if (plot_type == ImGuiPlotType_Lines)
		//		SetTooltip("%d: %8.4g\n%d: %8.4g", v_idx, v0, v_idx + 1, v1);
		//	else if (plot_type == ImGuiPlotType_Histogram)
		//		SetTooltip("%d: %8.4g", v_idx, v0);
		//	idx_hovered = v_idx;
		SetTooltip("%.3f : [%.3f]", mouseT, v[0]);

		if (IsMouseClicked(0)) {
			const float mouseV = ImClamp((g.IO.MousePos.y - inner_bb.Min.y) / (inner_bb.Max.y - inner_bb.Min.y), 0.0f, 0.9999f);
			v[0] = lerp(scale_min, scale_max, 1.0 - mouseV);
			imgraph_insert(nodes, maxN, nComp, mouseT, v);
		}
	}

	//ImGuiStorage* storage = window->DC.StateStorage;

	// values mode: draw colored lines
	// color mode: draw a bunch of solid colored rectangles
	const ImU32 col_base[4] = {
		IM_COL32(186,   0,   0, 255),
		IM_COL32(0, 186,   0, 255),
		IM_COL32(0,   0, 186, 255),
		IM_COL32(127, 127, 127, 255),
	};
	const ImU32 col_hovered[4] = {
		IM_COL32(255,   0,   0, 255),
		IM_COL32(0, 255,   0, 255),
		IM_COL32(0,   0, 255, 255),
		IM_COL32(127, 127, 127, 255),
	};
	//float histogram_zero_line_t = (scale_min * scale_max < 0.0f) ? (-scale_min * inv_scale) : (scale_min < 0.0f ? 0.0f : 1.0f);   // Where does the zero line stand

	float t0 = tMin;
	float v0[4];
	int ti = 0;
	{
		int i0 = 0;
		imgraph_find(nodes, maxN, nComp, tMin, &i0, &ti);
		if (i0 == ti)
			imgraph_read(nodes, nComp, i0, v0);
		else
			imgraph_interp(nodes, nComp, i0, t0, v0);
	}
	ImVec2 tp0[4];
	for (int i =0 ;i < nComp;i++)
		tp0[i] = ImVec2( (t0 - tMin) * tNorm, 1.0f - ImSaturate((v0[i] - scale_min) * inv_scale));// Point in the normalized space of our target rectangle

	int istride = (1 + nComp);
	while (true)
	{
		float t1;
		setQNaN(t1);
		if (ti < maxN)
			t1 = nodes[ti * istride];

		float v1[4];
		if (myIsQNaN(t1))
			imgraph_read(nodes, nComp, ti - 1, v1);
		else
			imgraph_interp(nodes, nComp, ti, t1, v1);

		bool stop = myIsQNaN(t1) || t1 > tMax;
		if (stop) {
			t1 = tMax;
		}
		ti++;

		ImVec2 tp1[4];
		for (int i = 0; i < nComp; i++)
		{
			tp1[i] = ImVec2((t1 - tMin) * tNorm, 1.0f - ImSaturate((v1[i] - scale_min) * inv_scale));// Point in the normalized space of our target rectangle

			// NB: Draw calls are merged together by the DrawList system. Still, we should render our batch are lower level to save a bit of CPU.
			ImVec2 pos0 = ImLerp(inner_bb.Min, inner_bb.Max, tp0[i]);
			ImVec2 pos1 = ImLerp(inner_bb.Min, inner_bb.Max, tp1[i]);
			window->DrawListInst.AddLine(pos0, pos1, col_base[i]);

			float sz = 1.1f;
			ImVec2 tp = pos1;
			if (!stop)
				window->DrawListInst.AddQuadFilled( { tp.x + sz, tp.y + 0.f}, { tp.x + 0.f, tp.y + sz}, { tp.x - sz, tp.y + 0.f}, { tp.x + 0.f, tp.y - sz}, col_hovered[i]);
			//ImVec2 pos1 = ImLerp(inner_bb.Min, inner_bb.Max, (plot_type == ImGuiPlotType_Lines) ? tp1 : ImVec2(tp1.x, histogram_zero_line_t));
			//if (plot_type == ImGuiPlotType_Lines)
			//{
			//	window->DrawList->AddLine(pos0, pos1, idx_hovered == v1_idx ? col_hovered : col_base);
			//}
			//else if (plot_type == ImGuiPlotType_Histogram)
			//{
			//	if (pos1.x >= pos0.x + 2.0f)
			//		pos1.x -= 1.0f;
			//	window->DrawList->AddRectFilled(pos0, pos1, idx_hovered == v1_idx ? col_hovered : col_base);
			//}

			tp0[i] = tp1[i];
		}

		t0 = t1;
		if (stop)
			break;
	}
	}

*/

void imgraph_init(ImGraph* g)
{
	float* nodes = imgraph_nodes(g);
	memset(nodes, 0, sizeof(float) * (g->Components + 1) * g->SizeMax);
}

static inline int clamp_idx(const ImGraph* g, int node) { return (node < 0 ? 0 : (node >= g->Size ? g->Size - 1 : node)); }

//returns the index of the first graph node before given t, or -1
static int imgraph_find(const ImGraph* g, float t)
{
	const float* nodes = imgraph_nodes(g);
	int ti = -1;
	for (int i = 0; i < g->Size; i++) {
		if (nodes[i * (1 + g->Components)] > t)
			break;
		ti = i;
	}
	return ti;
}


static void imgraph_read(const ImGraph* g, int n0, float* out)
{
	IM_ASSERT(n0 >= 0 && n0 < g->Size);
	memcpy(out, imgraph_data(g, n0), 4 * g->Components);
}

void imgraph_eval(const ImGraph* g, float t, float* out)
{
	if (g->Size == 0) { 
		memset(out, 0, 4 * g->Components);
		return;
	}
	if (g->Size == 1) { // fastpath for single value
		memcpy(out, imgraph_data(g, 0), 4 * g->Components);
		return;
	}
	// find node and lerp
	int ti = imgraph_find(g, t);
	const float* d0 = imgraph_data(g, clamp_idx(g, ti));
	const float* d1 = imgraph_data(g, clamp_idx(g, ti + 1));
	if (d0 == d1) {
		memcpy(out, d0, 4 * g->Components);
	} else {
		float t0 = d0[-1];
		float t1 = d1[-1];
		float tnorm = (t - t0) / (t1 - t0);
		for (int c = 0; c < g->Components; c++)
			out[c] = lerp(d0[c], d1[c], tnorm);
	}
}

void imgraph_insert(ImGraph* g, float t, const float* v)
{
	int tNode = imgraph_find(g, t) + 1;
	if (tNode >= g->SizeMax)
		return; // past the end
	g->Size = g->Size + 1 > g->SizeMax ? g->SizeMax : g->Size + 1;
	int vNode = clamp_idx(g, tNode);
	int toMove = g->Size - vNode - 1;
	if (toMove > 0) {
		float* from = imgraph_data(g, vNode) - 1;
		float* to = imgraph_data(g, vNode + 1) - 1;
		memmove(to, from, 4 * (1 + g->Components) * toMove);
	}
	float* data = imgraph_data(g, vNode);
	memcpy(data, v, 4 * g->Components);
	data[-1] = t;
}

bool imgraph_edit(const char* label, ImGraph* graph, float tMin, float tMax, float vMin, float vMax)
{
	using namespace ImGui;

	int numComp = graph->Components;
	if (numComp > 4)
		numComp = 4;

	ImVec2 frame_size = { 0.f, 0.f };

	ImGuiContext& g = *GImGui;
	ImGuiWindow* window = GetCurrentWindow();
	if (window->SkipItems)
		return false;

	const ImGuiStyle& style = g.Style;
	const ImGuiID id = window->GetID(label);

	ImVec2 label_size = CalcTextSize(label, NULL, true);
	label_size.y *= 2.f;
	if (frame_size.x == 0.0f)
		frame_size.x = CalcItemWidth();
	if (frame_size.y == 0.0f)
		frame_size.y = label_size.y + (style.FramePadding.y * 2);

	const ImVec2 labelSize = {0.f, 0.f};// CalcTextSize("00.000", NULL);
	const ImRect frame_bb(window->DC.CursorPos, window->DC.CursorPos + frame_size);
	const ImRect inner_bb(frame_bb.Min + style.FramePadding + ImVec2(labelSize.x, 0.f), frame_bb.Max - style.FramePadding - ImVec2(labelSize.x, 0.f));
	const ImRect total_bb(frame_bb.Min, frame_bb.Max + ImVec2(label_size.x > 0.0f ? style.ItemInnerSpacing.x + label_size.x : 0.0f, 0));
	ItemSize(total_bb, style.FramePadding.y);
	if (!ItemAdd(total_bb, 0, &frame_bb))
		return false;

	bool values_changed = false;
	const bool hovered = ItemHoverable(frame_bb, id);
	bool temp_input_is_active = TempInputIsActive(id);
	bool temp_input_start = false;
	if (!temp_input_is_active)
	{
		const bool focus_requested = FocusableItemRegister(window, id);
		const bool clicked = (hovered && g.IO.MouseClicked[0]);
		const bool double_clicked = (hovered && g.IO.MouseDoubleClicked[0]);
		if (focus_requested || clicked || double_clicked || g.NavActivateId == id || g.NavInputId == id)
		{
			SetActiveID(id, window);
			SetFocusID(id, window);
			FocusWindow(window);
			g.ActiveIdUsingNavDirMask = (1 << ImGuiDir_Left) | (1 << ImGuiDir_Right);
			if (focus_requested || (clicked && g.IO.KeyCtrl) || double_clicked || g.NavInputId == id)
			{
				temp_input_start = true;
				FocusableItemUnregister(window);
			}
		}
	}

	//BeginGroup(); // The only purpose of the group here is to allow the caller to query item data e.g. IsItemActive()
	//PushID(label);

	// Determine scale from values if not specified
	if (vMin == FLT_MAX || vMax == FLT_MAX)
	{
		float v_min = 0.f, v_max = 1.f;
		if (graph->Size > 0) {
			v_max = *imgraph_data(graph, 0);
			v_max = v_min;
			for (int i = 0; i < graph->Size; i++) {
				float* v = imgraph_data(graph, i);
				for (int c = 0; c < numComp; c++) {
					v_min = ImMin(v_min, v[c]);
					v_max = ImMax(v_max, v[c]);
				}
			}
			v_max += ImMax( v_max *1.05f, 0.01f); // allow some headroom
			v_min -= ImMax( v_min *1.05f, 0.01f); // allow some headroom
		}

		if (vMin == FLT_MAX)
			vMin = v_min;
		if (vMax == FLT_MAX)
			vMax = v_max;
	}

	// Draw frame
	const ImU32 frame_col = GetColorU32(g.ActiveId == id ? ImGuiCol_FrameBgActive : g.HoveredId == id ? ImGuiCol_FrameBgHovered : ImGuiCol_FrameBg);
	RenderNavHighlight(frame_bb, id);
	RenderFrame(frame_bb.Min, frame_bb.Max, frame_col, true, style.FrameRounding);

	ImGuiStorage* storage = window->DC.StateStorage;

	int hover_idx = storage->GetInt(id, -1 << 4); //node idx
	int hover_mask = hover_idx & 0x0000000F; // hovered channels
	hover_idx = hover_idx >> 4;

	if (vMax == vMin)
		vMax = vMin + 1.f;

	const float tNorm = 1.f / (tMax - tMin);
	const float vNorm = 1.f / (vMax - vMin);

	// interaction
	if (hovered && inner_bb.Contains(g.IO.MousePos) && hover_idx == -1)
	{
		float tPerX = 1.f / (inner_bb.Max.x - inner_bb.Min.x);
		float valuePerY = 1.f / (inner_bb.Max.y - inner_bb.Min.y);
		//TODO: freeze T sliding
		//TODO: edit nodes numerically
		const float mouseT = lerp(tMin, tMax, ImClamp( (g.IO.MousePos.x - inner_bb.Min.x) * tPerX, 0.0f, 0.9999f));
		const float mouseV = lerp(vMin, vMax, 1.0f - ImClamp((g.IO.MousePos.y - inner_bb.Min.y) * valuePerY, 0.0f, 0.9999f));
		
		float v[4];
		imgraph_eval(graph, mouseT, v);
		if (graph->Components == 1)
			SetTooltip("%.3f : [%.3f]", mouseT, v[0]);
		else if (graph->Components == 2)
			SetTooltip("%.3f : [%.3f] [%.3f]", mouseT, v[0], v[1]);
		else
			SetTooltip("%.3f : [%.3f,%.3f,%.3f]", mouseT, v[0], v[1], v[2]);

		// hover
		int mouseI0 = clamp_idx(graph, imgraph_find(graph, mouseT));
		int mouseI1 = clamp_idx(graph, mouseI0 + 1);

		hover_idx = eqf(mouseT, imgraph_time(graph, mouseI0), tPerX * 5.f) ? mouseI0 :
			eqf(mouseT, imgraph_time(graph, mouseI1), tPerX * 5.f) ? mouseI1 : -1;
		if (hover_idx > -1)
		{
			hover_mask = 0;
			if (!g.IO.KeyShift) // single channel select
				for (int c = 0; c < graph->Components; c++) {
					if (eqf(v[c], mouseV, valuePerY * 3.f))
					{
						hover_mask |= 1 << c;
						break;
					}
				}
			else
				hover_mask = (1 << graph->Components) - 1; // all

			if (!hover_mask)
				hover_idx = -1;

			storage->SetInt(id, (hover_idx << 4) | hover_mask); //node idx
		}

		// interaction

		// add new point
		if (g.IO.KeyCtrl && IsMouseClicked(0)) {
			v[0] = mouseV;
			imgraph_insert(graph, mouseT, v);
			values_changed = true;
		}
	}

	if (hover_idx != -1) {

		int dataStart = hover_idx * (1 + graph->Components);
		float* nodeT = imgraph_nodes(graph) + dataStart;

		// slide nodes horizontally
		ImRect grab_bb;
		if ( SliderBehavior(frame_bb, id, ImGuiDataType_Float, nodeT, &tMin, &tMax, ".3f", 1.f, ImGuiSliderFlags_None, &grab_bb)) {
			float t0 = imgraph_time(graph, clamp_idx(graph, hover_idx - 1));
			float t1 = imgraph_time(graph, clamp_idx(graph, hover_idx + 1));
			*nodeT = ImClamp(*nodeT, t0, t1); // clamp between neighbor nodes
			MarkItemEdited(id);

			values_changed = true;
		}

		if (grab_bb.Max.x > grab_bb.Min.x)
			window->DrawList->AddRectFilled(grab_bb.Min, grab_bb.Max, GetColorU32(g.ActiveId == id ? ImGuiCol_SliderGrabActive : ImGuiCol_SliderGrab), style.GrabRounding);

		// move values vertically
		float valT = 0.f;
		const float valMinDT = -1.f, valMaxDT = 1.f;
		if ( DragBehavior(id, ImGuiDataType_Float, &valT, 0.005f, &valMinDT, &valMaxDT, ".3f", 1.f, ImGuiDragFlags_Vertical)) {
			int dataStart = hover_idx * (1 + graph->Components) + 1;
			for (int c = 0; c < graph->Components; c++)
				if (hover_mask & (1 << c))
					imgraph_nodes(graph)[dataStart + c] = ImClamp(imgraph_nodes(graph)[dataStart + c] + valT, vMin, vMax);

			MarkItemEdited(id);
			values_changed = true;
		}

		bool beingDragged = g.ActiveId == id;
		if (!beingDragged)
			storage->SetInt(id, -1);
	}

	// Label
	if (label_size.x > 0.0f)
		RenderText(ImVec2(frame_bb.Max.x + style.ItemInnerSpacing.x, frame_bb.Min.y + style.FramePadding.y), label);

	// Value text
	/*
	char txt[32];
	int l = ImFormatString(txt, sizeof(txt), "%.3f", vMax);
	RenderText(frame_bb.Min, txt, txt + l, false);
	l = ImFormatString(txt, sizeof(txt), "%.3f", vMin);
	RenderText({ frame_bb.Min.x, frame_bb.Max.y - labelSize.y }, txt, txt + l, false);
	*/

	//PopID();
	//EndGroup();

	if (graph->Size == 0)
		return false;

	//ImGuiStorage* storage = window->DC.StateStorage;

	// values mode: draw colored lines
	// color mode: draw a bunch of solid colored rectangles
	const ImU32 col_base[4] = {
		IM_COL32(186,   0,   0, 128),
		IM_COL32(0, 186,   0, 128),
		IM_COL32(0,   0, 186, 128),
		IM_COL32(127, 127, 127, 128),
	};
	const ImU32 col_hovered[4] = {
		IM_COL32(255,   0,   0, 255),
		IM_COL32(0, 255,   0, 255),
		IM_COL32(0,   0, 255, 255),
		IM_COL32(127, 127, 127, 255),
	};
	//float histogram_zero_line_t = (scale_min * scale_max < 0.0f) ? (-scale_min * inv_scale) : (scale_min < 0.0f ? 0.0f : 1.0f);   // Where does the zero line stand
	
	float t0 = tMin;
	float v0[4];
	imgraph_eval(graph, t0, v0);
	ImVec2 tp0[4];
	for (int i = 0; i < numComp; i++)
		tp0[i] = ImVec2((t0 - tMin) * tNorm, 1.0f - ImSaturate((v0[i] - vMin) * vNorm));// Point in the normalized space of our target rectangle

	int iMin = imgraph_find(graph, tMin);
	int iMax = imgraph_find(graph, tMax);
	int numNodes = iMax - iMin + 1;
	//TODO: render color graphs
	int node = iMin;
	while (numNodes--) {
		int n0 = clamp_idx(graph, node);
		int n1 = clamp_idx(graph, node+1);

		float t1 = imgraph_time(graph, n1);
		float v1[4];
		if (node == iMax)
			t1 = tMax;

		//TODO: interpolate last point
		imgraph_read(graph, n1, v1);

		ImVec2 tp1[4];
		for (int i = 0; i < numComp; i++) {
			tp1[i] = ImVec2((t1 - tMin) * tNorm, 1.0f - ImSaturate((v1[i] - vMin) * vNorm));// Point in the normalized space of our target rectangle

			// NB: Draw calls are merged together by the DrawList system. Still, we should render our batch are lower level to save a bit of CPU.
			ImVec2 pos0 = ImLerp(inner_bb.Min, inner_bb.Max, tp0[i]);
			ImVec2 pos1 = ImLerp(inner_bb.Min, inner_bb.Max, tp1[i]);
			window->DrawListInst.AddLine(pos0, pos1, col_base[i]);
			//window->DrawListInst.AddQuadFilled(pos0, {pos0.x, inner_bb.Max.y}, { pos1.x, inner_bb.Max.y }, pos1, col_base[i]);

			// draw nodes
			bool is_hovered = n0 == hover_idx && (hover_mask & (1 << i));
			float sz = 3.1f;
			ImVec2 tp = pos0;
			
			ImU32 node_color = is_hovered ? col_hovered[i] : col_base[i];
			window->DrawListInst.AddQuadFilled({ tp.x + sz, tp.y + 0.f }, { tp.x + 0.f, tp.y + sz }, { tp.x - sz, tp.y + 0.f }, { tp.x + 0.f, tp.y - sz }, node_color);

			//ImVec2 pos1 = ImLerp(inner_bb.Min, inner_bb.Max, (plot_type == ImGuiPlotType_Lines) ? tp1 : ImVec2(tp1.x, histogram_zero_line_t));
			//if (plot_type == ImGuiPlotType_Lines)
			//{
			//	window->DrawList->AddLine(pos0, pos1, idx_hovered == v1_idx ? col_hovered : col_base);
			//}
			//else if (plot_type == ImGuiPlotType_Histogram)
			//{
			//	if (pos1.x >= pos0.x + 2.0f)
			//		pos1.x -= 1.0f;
			//	window->DrawList->AddRectFilled(pos0, pos1, idx_hovered == v1_idx ? col_hovered : col_base);
			//}

			tp0[i] = tp1[i];
		}
		node++;
	}

	/*
	for (int n = 0; n < res_w; n++)
	{
		const float t1 = t0 + t_step;
		//const int v1_idx = (int)(t0 * item_count + 0.5f);
		//IM_ASSERT(v1_idx >= 0 && v1_idx < values_count);
		float v1[4];
		imgraph_eval(nodes, maxN, nComp, t1, v1);
		ImVec2 tp1[4];
		for (int i = 0; i < nComp; i++) 
		{
			tp1[i] = ImVec2( (t1 - tMin) * tNorm, 1.0f - ImSaturate((v1[i] - scale_min) * inv_scale));// Point in the normalized space of our target rectangle

			// NB: Draw calls are merged together by the DrawList system. Still, we should render our batch are lower level to save a bit of CPU.
			ImVec2 pos0 = ImLerp(inner_bb.Min, inner_bb.Max, tp0[i]);
			ImVec2 pos1 = ImLerp(inner_bb.Min, inner_bb.Max, tp1[i]);
			window->DrawList->AddLine(pos0, pos1, col_base[i]);
			//ImVec2 pos1 = ImLerp(inner_bb.Min, inner_bb.Max, (plot_type == ImGuiPlotType_Lines) ? tp1 : ImVec2(tp1.x, histogram_zero_line_t));
			//if (plot_type == ImGuiPlotType_Lines)
			//{
			//	window->DrawList->AddLine(pos0, pos1, idx_hovered == v1_idx ? col_hovered : col_base);
			//}
			//else if (plot_type == ImGuiPlotType_Histogram)
			//{
			//	if (pos1.x >= pos0.x + 2.0f)
			//		pos1.x -= 1.0f;
			//	window->DrawList->AddRectFilled(pos0, pos1, idx_hovered == v1_idx ? col_hovered : col_base);
			//}

			tp0[i] = tp1[i];
		}
		t0 = t1;
	}
	*/

	/*
	// Text overlay
	if (overlay_text)
		RenderTextClipped(ImVec2(frame_bb.Min.x, frame_bb.Min.y + style.FramePadding.y), frame_bb.Max, overlay_text, NULL, NULL, ImVec2(0.5f, 0.0f));

	if (label_size.x > 0.0f)
		RenderText(ImVec2(frame_bb.Max.x + style.ItemInnerSpacing.x, inner_bb.Min.y), label);
		
	// Return hovered index or -1 if none are hovered.
	// This is currently not exposed in the public API because we need a larger redesign of the whole thing, but in the short-term we are making it available in PlotEx().
	return idx_hovered;
	*/

	return values_changed;
}