#include "StdAfx.h"
//#include "HeightMapMesh.h"
//#include "ImageGeneration/Image.h"
//
//using namespace Bolts::Rendering;
//using Bolts::vec3;
//
//namespace MeshGen
//{
//
////Index generation
//IndexBufferPtr GenerateTriangleMeshIndices(Bolts::Rendering::RCB&, const uvec2 &imgSize )
//{
//	//TODO: Use primitive restart IDs to enable using triangle strips
//	//Configure Index data
//	const uint32_t numIndices = ( imgSize.x - 1)* (imgSize.y -1)* 6;
//	IndexBuffer::dataBuffer_t	indexData;
//	indexData.resize ( numIndices * 4);// uint32_t indices
//	uint32_t* iDataPtr = (uint32_t*) indexData.data();
//	IndexBufferPtr iBuffer = new IndexBuffer( BGVT_U32);
//	iBuffer->SetNumberOfIndices( numIndices);
//	iBuffer->SetNumberOfPrimitives ( numIndices/3);
//	//Set index values
//	{
//		uint32_t bufferIndex = 0, quadStart = 0;
//		const uint32_t lineSize = imgSize.x;
//		while( bufferIndex < numIndices){
//			//for ( uint32_t quadStart = 0; bufferIndex < numIndices/3; quadStart += 1){
//			if (quadStart % lineSize == (lineSize - 1)){
//				quadStart++;
//			}
//			//Tri 1
//			iDataPtr[bufferIndex++] = quadStart + 1;
//			iDataPtr[bufferIndex++] = quadStart;
//			iDataPtr[bufferIndex++] = quadStart + lineSize;
//			//Tri 2
//			iDataPtr[bufferIndex++] = quadStart + lineSize;
//			iDataPtr[bufferIndex++] = quadStart + lineSize + 1;
//			iDataPtr[bufferIndex++] = quadStart + 1;
//			quadStart++;
//		}
//	}
//	iBuffer->SetData ( BUH_STATIC_DRAW, std::move(indexData));
//	return iBuffer;
//}
//
//
//Bolts::Rendering::PrimitiveBufferPtr FromHeightImg (Bolts::Rendering::RCB&, const Image1C& img, vec2 meshSize, uvec2 vertResolution)
//{
//	//Ignored for now
//	vertResolution;
//
//	const uvec2 imgSize = img.GetDimensions ();
//	const uint32_t	numVerts = imgSize.x *imgSize.y;
//	struct vertData_t
//	{
//		vertData_t():position(0.f),normals( 0.f, 1.f, 0.f) {}
//		vec3 position;
//		vec3 normals;
//	};
//
//	//Configure Vertex data
//	GPUBufferDescription desc;
//	desc.AddStream ( GPUBufferDescription::StreamDesc( BGSS_POSITION, BGVT_FLOAT, 3, 0 ) );
//	desc.AddStream ( GPUBufferDescription::StreamDesc( BGSS_NORMALS, BGVT_FLOAT, 3, 3 * sizeof( float ) ) );
//	VertexBufferPtr vBuffer = new VertexBuffer( desc );
//	vBuffer->SetNumberOfVertices ( numVerts);
//	vertData_t* vertData = new vertData_t[ numVerts];
//	//Set vertex data values
//	{
//		vec2 vertPos = meshSize / 2.f;
//		uint32_t bufferIndex = 0;
//		const float hStep = meshSize.x / imgSize.x;
//		const float vStep = meshSize.y / imgSize.y;
//		for ( uint16_t x = 0; x < imgSize.x; ++x){
//			for ( uint16_t y = 0; y < imgSize.y; ++y){
//				vertData[ bufferIndex].position = vec3( vertPos.x, img.GetValue (uvec2(x,y)), vertPos.y);
//				bufferIndex++;
//				vertPos.x -= hStep;
//			}
//			vertPos.x = meshSize.x / 2.f;
//			vertPos.y -= vStep;
//			//TODO: Calculate normals
//		}
//	}
//	//Send data
//	vBuffer->SetData ( BUH_STATIC_DRAW, (void*) vertData, sizeof( vertData_t ) * numVerts, false);
//	//Generate index data
//	IndexBufferPtr iBuffer = GenerateTriangleMeshIndices(imgSize);
//	//Configure primitive buffer
//	PrimitiveBufferPtr pbuffer = new PrimitiveBuffer();
//	pbuffer->m_vertexBuffer = vBuffer;
//	pbuffer->m_indexBuffer = iBuffer;
//	pbuffer->m_primitiveType = Bolts::Rendering::BGPT_TRIANGLES;
//	return pbuffer;
//}
//
//Bolts::Rendering::PrimitiveBufferPtr FromColorImg(Bolts::Rendering::RCB&, const Image3C& img, Bolts::vec2 meshSize )
//{
//	const uvec2 imgSize = img.GetDimensions ();
//	const uint32_t	numVerts = imgSize.x *imgSize.y;
//	struct vertData_t
//	{
//		vertData_t():position(0.f),color( 1.f) {}//TODO: default to an error color
//		vec3 position;
//		vec3 color;
//	};
//
//	//Configure Vertex data
//	GPUBufferDescription desc;
//	desc.AddStream ( GPUBufferDescription::StreamDesc( BGSS_POSITION, BGVT_FLOAT, 3, 0 ) );
//	desc.AddStream ( GPUBufferDescription::StreamDesc( BGSS_COLOR0, BGVT_FLOAT, 3, 3 * sizeof( float ) ) );
//	VertexBufferPtr vBuffer = new VertexBuffer( desc );
//	//vBuffer->SetNumberOfVertices ( numVerts);
//	vertData_t* vertData = new vertData_t[ numVerts];
//	//Set vertex data values
//	{
//		vec2 vertPos = meshSize / 2.f;
//		uint32_t bufferIndex = 0;
//		const float hStep = meshSize.x / imgSize.x;
//		const float vStep = meshSize.y / imgSize.y;
//		for ( uint16_t x = 0; x < imgSize.x; ++x){
//			for ( uint16_t y = 0; y < imgSize.y; ++y){
//				vertData[ bufferIndex].position = vec3( vertPos.x, 0.f, vertPos.y);
//				vertData[ bufferIndex].color = img.GetValue (uvec2(x,y));
//				bufferIndex++;
//				vertPos.x -= hStep;
//			}
//			vertPos.x = meshSize.x / 2.f;
//			vertPos.y -= vStep;
//		}
//	}
//	//Send data
//	vBuffer->SetData ( BUH_STATIC_DRAW, (void*) vertData, sizeof( vertData_t ) * numVerts, false);
//	//Generate index data
//	IndexBufferPtr iBuffer = GenerateTriangleMeshIndices(imgSize);
//	//Configure primitive buffer
//	PrimitiveBufferPtr pbuffer = new PrimitiveBuffer();
//	pbuffer->m_vertexBuffer = vBuffer;
//	pbuffer->m_indexBuffer = iBuffer;
//	pbuffer->m_primitiveType = Bolts::Rendering::BGPT_TRIANGLES;
//
//	return pbuffer;
//}
//
//
//}