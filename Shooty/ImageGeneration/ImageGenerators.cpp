#include "StdAfx.h"
#include "ImageGeneration/ImageGenerators.h"
#include "ImageGeneration/Image.h"
#include "ImageGeneration/NoiseFunctions.h"

using namespace Bolts;


template< class ImageT>
void ImageGen::GenGradient<ImageT>( ImageT& img, vec2 ulCorner, vec2 lrCorner, typename ImageT::value_t ulValue, typename ImageT::value_t lrValue )
{
	//Image::dataStorage_t& imgData = img.GetData ();
	const uvec2 ulCoords = img.InterpCoords (ulCorner);
	const uvec2 lrCoords = img.InterpCoords (lrCorner);
	const uvec2 intRect = ( lrCoords - ulCoords);
	const ImageT::value_t lerpValue = ulValue - lrValue;
	const vec2 stepSize( 0.70710678118f /intRect.x, 0.70710678118f /intRect.y);//Diagonal of one
	vec2 interpValue = vec2(0.f);

	for( uint32_t y = ulCoords.y; y < lrCoords.y; ++y){
		for( uint32_t x = ulCoords.x; x < lrCoords.x; ++x){
			interpValue.x += stepSize.x;
			const float alpha = glm::length (interpValue);
			img.SetValue ( uvec2( x,y), lrValue + alpha * lerpValue);
		}
		interpValue.x = 0.f;
		interpValue.y += stepSize.y;
	}
}


template< class ImageT>
void ImageGen::Fill<ImageT>( ImageT& img, vec2 ulCorner, vec2 lrCorner, typename ImageT::value_t value )
{
	//TODO: Validate values
	//Image::dataStorage_t& imgData = img.GetData ();
	const uvec2 ulCoords = img.InterpCoords (ulCorner);
	const uvec2 lrCoords = img.InterpCoords (lrCorner);
	//const ivec2 rectSize = lrCoords - ulCoords;
	//const vec2 stepSize = 1.f / (rectSize);
	//vec2 interpValue = vec2(0.f);
	for( uint32_t y = ulCoords.y; y < lrCoords.y; ++y){
		for( uint32_t x = ulCoords.x; x < lrCoords.x; ++x){
			img.SetValue ( uvec2( x,y), value);
		}
	}
}

template< class ImageT>
void ImageGen::Noise<ImageT>( ImageT& img, vec2 ulCorner, vec2 lrCorner, float freq, typename ImageT::value_t amplitude)
{
	//TODO: Move this to init step
	SimplexNoise::Init ();
	//TODO: Validate values
	const uvec2 ulCoords = img.InterpCoords (ulCorner);
	const uvec2 lrCoords = img.InterpCoords (lrCorner);
	const uvec2 intRect = ( lrCoords - ulCoords);
	const vec2 stepSize( freq /intRect.x, freq /intRect.y);
	vec2 interpValue = vec2(0.f);

	for( uint32_t y = ulCoords.y; y < lrCoords.y; ++y){
		for( uint32_t x = ulCoords.x; x < lrCoords.x; ++x){
			interpValue.x += stepSize.x;
			//const float alpha = glm::length (interpValue);
			img.SetValue ( uvec2( x,y), ((float) SimplexNoise::Eval ( interpValue.x, interpValue.y) *0.5f +0.5f) * amplitude);
		}
		interpValue.x = 0.f;
		interpValue.y += stepSize.y;
	}
}


template void ImageGen::GenGradient( Image1C& img, vec2 ulCorner, vec2 lrCorner, Image1C::value_t ulValue , Image1C::value_t lrValue );
template void ImageGen::GenGradient( Image3C& img, vec2 ulCorner, vec2 lrCorner, Image3C::value_t ulValue , Image3C::value_t lrValue );

template void ImageGen::Fill( Image1C& img, vec2 ulCorner, vec2 lrCorner, Image1C::value_t value );
template void ImageGen::Fill( Image3C& img, vec2 ulCorner, vec2 lrCorner, Image3C::value_t value );

template void ImageGen::Noise( Image1C& img, vec2 ulCorner, vec2 lrCorner, float freq, Image1C::value_t amplitude);
template void ImageGen::Noise( Image3C& img, vec2 ulCorner, vec2 lrCorner, float freq, Image3C::value_t amplitude);
