#ifndef NoiseFunctions_h__
#define NoiseFunctions_h__


namespace SimplexNoise {  // Simplex noise in 2D, 3D and 4D
	typedef double scalar;

	void	Init();
	// 2D simplex noise
	scalar	Eval(scalar xin, scalar yin);
};
#endif // NoiseFunctions_h__