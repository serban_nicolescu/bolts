#pragma once

#include <bolts_core.h>
using Bolts::vec2;

namespace ImageGen{

	template< class ImageT>
	void	GenGradient( ImageT&, vec2 ulCorner, vec2 lrCorner, typename ImageT::value_t ulValue, typename ImageT::value_t lrValue);
	template< class ImageT>
	void	Fill( ImageT&, vec2 ulCorner, vec2 lrCorner, typename ImageT::value_t value);
	template< class ImageT>
	void	Noise( ImageT&, vec2 ulCorner, vec2 lrCorner, float freq, typename ImageT::value_t amplitude);
}
