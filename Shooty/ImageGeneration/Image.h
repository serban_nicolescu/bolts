#pragma once

#include <bolts_core.h>
#include <vector>

using Bolts::vec2;
using Bolts::uvec2;

enum ImageFormat{ IF_UNKNOWN =0, IF_R, IF_RGB, IF_RGBA};

template< typename V>
class Image
{
public:
	typedef V						value_t;
	typedef std::vector<value_t>	dataStorage_t;

	Image(): m_data(),m_format( ValueTToEnum< value_t>::value ) {}
	//Setters
	void	SetDimensions( uvec2 newDims) { m_dimensions = newDims; m_data.resize ( newDims.x * newDims.y); }
	void	SetData( dataStorage_t&& newData) { m_data = newData; }
	void	SetData( const dataStorage_t& newData) { m_data = newData; }
	//Getters
	ImageFormat				GetFormat() const { return m_format; } 
	uvec2					GetDimensions() const { return m_dimensions; } 
	dataStorage_t&			GetData()	{ return m_data; }
	const dataStorage_t&	GetData() const { return m_data; }
	//Sampling Functions
	uvec2				InterpCoords( vec2 normCoords) const
	{
		return uvec2( 
			static_cast<unsigned> (m_dimensions.x * normCoords.x), 
			static_cast<unsigned> (m_dimensions.y * normCoords.y));
	}
	value_t			GetValue( uvec2 coords) const { return m_data[ coords.y * m_dimensions.x + coords.x];	}
	void				SetValue( uvec2 coords, value_t value) { m_data[ coords.y * m_dimensions.x + coords.x] = value; }
private:
	template< typename VType> struct ValueTToEnum { enum { value = IF_UNKNOWN }; };

	template<> struct ValueTToEnum< float>{
		static const ImageFormat value = IF_R;
	};
	template<> struct ValueTToEnum< Bolts::vec3>{
		static const ImageFormat value = IF_RGB;
	};
	template<> struct ValueTToEnum< Bolts::vec4>{
		static const ImageFormat value = IF_RGBA;
	};

	uvec2			m_dimensions;
	dataStorage_t	m_data;
	ImageFormat		m_format;
};

typedef Image<float>			Image1C;
typedef Image< Bolts::vec3 >	Image3C;
typedef Image< Bolts::vec4 >	Image4C;
