#pragma once

#include <ImageGeneration/Image.h>
#include <Rendering/GPUBuffers.h>
#include <bolts_core.h>

namespace MeshGen
{
	Bolts::Rendering::PrimitiveBufferPtr	FromHeightImg(Bolts::Rendering::RCB&, const Image1C& img, Bolts::vec2 meshSize, Bolts::uvec2 vertResolution);
	Bolts::Rendering::PrimitiveBufferPtr	FromColorImg(Bolts::Rendering::RCB&, const Image3C& img, Bolts::vec2 meshSize);
};
