#pragma once
#include "imgui.h"


struct ImGraph {
	short Size;
	short SizeMax;
	short Components;
	short Flags;
	float* Nodes;
};

#define IMGRAPH(var_name, num_components, size_max)  float val_##var_name [size_max * (1+num_components)] {}; ImGraph var_name{0, size_max, num_components, 0, val_##var_name};

inline float*		imgraph_nodes(ImGraph* g) { return g->Nodes; }
inline const float* imgraph_nodes(const ImGraph* g) { return g->Nodes; }

inline float		imgraph_time(const ImGraph* g, int node)	{ return *(imgraph_nodes(g) + node * (1 + g->Components)); }
inline const float* imgraph_data(const ImGraph* g, int node)	{ return imgraph_nodes(g) + node * (1 + g->Components) + 1; }
inline float*		imgraph_data(ImGraph* g, int node)			{ return imgraph_nodes(g) + node * (1 + g->Components) + 1; }

void imgraph_init(ImGraph* g);

void imgraph_insert(ImGraph* g, float t, const float* v);
bool imgraph_edit(const char* label, ImGraph* graph, float tMin, float tMax, float vMin = FLT_MAX, float vMax = FLT_MAX);