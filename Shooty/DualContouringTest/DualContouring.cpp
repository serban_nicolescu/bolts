#include "StdAfx.h"
#include "DualContouring.h"
#include <Rendering/Texture.h>
#include "Render/DebugDraw.h"
#include <Rendering/Mesh.h>
#include <Rendering/RenderBackend.h>

//Source: http://ngildea.blogspot.ro/2014/11/implementing-dual-contouring.html
#include "qef.h"

//References: http://www.frankpetterson.com/publications/dualcontour/dualcontour.pdf
//https://upvoid.com/devblog/2013/05/terrain-engine-part-1-dual-contouring/

// New method that also lists other options
//http://www.gdcvault.com/play/1023565/Math-for-Game-Programmers-Voxel
//See: http://josiahmanson.com/research/iso_simplicial/

//Marching cubes cu countour indicator function
// http://josiahmanson.com/research/contour_indicator/

namespace dc
{
	using namespace Bolts;

	void GenHermiteData(SDF3DFuncT func, int volResolution, int intersectionMethod, float distanceTol, int maxIterationSteps, HermiteData& out)
	{
		BASSERT_MSG( intersectionMethod > 0 && intersectionMethod < 5, "Unknown intersection method used in HermiteData generation");
		BASSERT(volResolution > 0);
		const uvec3 dims{ static_cast<unsigned>(volResolution) };
		const vec3 cellSize = vec3(1.f / dims.x, 1.f / dims.y, 1.f / dims.z); // The size in normalised coords of a single cell
		// Store sdf values at grid points
		Grid<float> sdfGrid;
		Grid<uint8_t>& matIDGrid = out.m_matids;
		sdfGrid.Resize(dims);
		out.Resize(dims - uvec3(1));

		const vec3 stepSize = cellSize;
		for (unsigned z = 0; z < dims.z; z++) {
			for (unsigned y = 0; y < dims.y; y++) {
				vec3 pointPos = { 0.f, stepSize.y * y, stepSize.z * z };
				for (unsigned x = 0; x < dims.x; x++) {
					pointPos.x += stepSize.x;
					const float val = func(pointPos);
					sdfGrid.Set({ x, y, z }) = val;
					matIDGrid.Set({ x, y, z }) = val < 0.f ? 1 : 0; // Set material ID to 1( solid ) if we're inside ( negative sdf value )
				}
			}
		}
		// Now search for edges that cross surface boundaries and find intersection points
		auto checkEdge = [&matIDGrid, &sdfGrid, stepSize, distanceTol, maxIterationSteps, intersectionMethod](SDF3DFuncT func, uvec3 gridPos, vec3 pointPos, unsigned axis, Grid<EdgeData>& edgeGrid) {
			const float distEps = distanceTol;// 0.00001f;
			const unsigned maxSteps = maxIterationSteps;//20;
			uvec3 gridNextPos = gridPos;
			gridNextPos[axis] += 1;
			if (matIDGrid.Get(gridPos) != matIDGrid.Get(gridNextPos)){
				// Edge detected, search for exact location
				const float dir = matIDGrid.Get(gridPos) == 0 ? 1.f : -1.f;
				unsigned steps = maxSteps;
				vec3 iPoint = pointPos; // Intersection point uvw
				float dist = 0.f; // Intersection point distance to surface
				switch (intersectionMethod)
				{
					case 1:{// Regula Falsi Method
						vec3 startPoint = pointPos;
						vec3 endPoint = pointPos;
						endPoint[axis] += stepSize[axis];
						float v1 = func(startPoint);
						float v2 = func(endPoint);
						dist = v1;
						while (steps && (abs(dist) > distEps)){
							iPoint = (startPoint*v2 - endPoint*v1) / (v2 - v1);
							dist = func(iPoint);
							if (dist * v2 > 0.f){
								v2 = dist;
								endPoint = iPoint;
								v1 *= 0.5f;
							}else{
								v1 = dist;
								startPoint = iPoint;
								v2 *= 0.5f;
							}
						}
						break;
					}
					case 2:	{// bisection
						vec3 startPoint = pointPos;
						vec3 endPoint = pointPos;
						endPoint[axis] += stepSize[axis];
						iPoint[axis] = (startPoint[axis] + endPoint[axis]) /2.f;
						dist = func(iPoint);
						while (steps && (abs(dist) > distEps)){
							iPoint[axis] = (startPoint[axis] + endPoint[axis]) / 2.f;
							dist = func(iPoint);
							if (dist*dir > 0.f)
								startPoint = iPoint;
							else
								endPoint = iPoint;
							steps--;
						}
						break;
					}
					case 3:	{ // Raytracing
						dist = func(iPoint);
						while (steps && (abs(dist) > distEps))
						{
							iPoint[axis] += dist * dir;
							dist = func(iPoint);
							steps--;
						}
						break;
					}
					case 4:	{ // Stupid fixed step search
						float itStepSize = stepSize[axis] / steps;
						dist = func(iPoint);
						float bestPoint = iPoint[axis];
						while (steps)
						{
							iPoint[axis] += itStepSize;
							float cval = func(iPoint);
							if (abs(cval) < dist){
								dist = abs(cval);
								bestPoint = iPoint[axis];
							}
							steps--;
						}
						iPoint[axis] = bestPoint;
						break;
					}
					default:
						BASSERT_MSG(false, "Unknown intersection method used in HermiteData generation");
				};
				//TODO: We could store the fact that the solution didn't converge in another per-edge variable. For debugging
				//BASSERT(steps);
				// Found edge, store t and normal
				auto& edge = edgeGrid.Set(gridPos);
				edge.t = glm::clamp((iPoint[axis] - pointPos[axis]) / stepSize[axis] , 0.f, 1.f);
				const float normEps = 0.0001f;
				float valX = func(iPoint + vec3(normEps, 0.f, 0.f));
				float valY = func(iPoint + vec3(0.f, normEps, 0.f));
				float valZ = func(iPoint + vec3(0.f, 0.f, normEps));
				edge.norm = glm::normalize(vec3(valX - dist, valY - dist, valZ - dist));
				/*float valX = func(halfPoint + vec3(normEps, 0.f, 0.f)) - func(halfPoint - vec3(normEps, 0.f, 0.f));
				float valY = func(halfPoint + vec3(0.f,normEps, 0.f)) - func(halfPoint - vec3(0.f,normEps, 0.f));
				float valZ = func(halfPoint + vec3(0.f,0.f, normEps)) - func(halfPoint - vec3(0.f, 0.f, normEps));
				edge.norm = glm::normalize(vec3(valX, valY, valZ));*/
			}
		};
		for (unsigned z = 0; z < dims.z - 1; z++) {
			for (unsigned y = 0; y < dims.y - 1; y++) {
				vec3 pointPos = { 0.f, stepSize.y * y, stepSize.z * z };
				for (unsigned x = 0; x < dims.x - 1; x++) {
					pointPos.x += stepSize.x;
					checkEdge(func, { x, y, z }, pointPos, 0, out.m_xedges);
					checkEdge(func, { x, y, z }, pointPos, 1, out.m_yedges);
					checkEdge(func, { x, y, z }, pointPos, 2, out.m_zedges);
				}
			}
		}
		//
	}

	void GenDCMesh(Bolts::Rendering::RCB& commands, const HermiteData& data, Bolts::Rendering::Mesh* outMesh)
	{
		Grid<vec3> featurePoints;
		uvec3 dims = data.m_matids.GetSize();
		featurePoints.Resize(dims - uvec3(-1));
		svd::QefSolver solver;
		const float svdTol = 1e-6f;
		const int numSweeps = 4;
		const float pinvTol = 1e-6f;
		// Debug draw things
		auto debugDraw = Bolts::DebugDraw();
		using namespace Bolts;
		const vec4 pointColor{ 0.f, 0.f, 0.5f, 1.f };
		const float voxelSize = 1.f / dims.x;
		//
		const uvec3 vertOffsets[8] = {
			{ 0, 0, 0 },
			{ 1, 0, 0 },
			{ 0, 1, 0 },
			{ 1, 1, 0 },
			{ 0, 0, 1 },
			{ 1, 0, 1 },
			{ 0, 1, 1 },
			{ 1, 1, 1 }
		};

		for (unsigned z = 0; z < dims.z - 1; z++) {
			for (unsigned y = 0; y < dims.y - 1; y++) {
				for (unsigned x = 0; x < dims.x - 1; x++) {
					bool needsWork = false;
					const uvec3 cellCorner{ x, y, z }; // We have grid corners in the upper left and back of cells. Any edge for grid cell x,y,z belongs to cell xyz
					uint8_t vertMats[8] = {
						data.m_matids.Get(cellCorner + vertOffsets[0]),
						data.m_matids.Get(cellCorner + vertOffsets[1]),
						data.m_matids.Get(cellCorner + vertOffsets[2]),
						data.m_matids.Get(cellCorner + vertOffsets[3]),
						data.m_matids.Get(cellCorner + vertOffsets[4]),
						data.m_matids.Get(cellCorner + vertOffsets[5]),
						data.m_matids.Get(cellCorner + vertOffsets[6]),
						data.m_matids.Get(cellCorner + vertOffsets[7]),
					};
					// X
					if (vertMats[0] != vertMats[1]){
						auto edge = data.m_xedges.Get(cellCorner + vertOffsets[0]);
						solver.add(edge.t, 0.f, 0.f, edge.norm.x, edge.norm.y, edge.norm.z);
						needsWork = true;
					}
					if (vertMats[2] != vertMats[3]){
						auto edge = data.m_xedges.Get(cellCorner + vertOffsets[2]);
						solver.add(edge.t, 1.f, 0.f, edge.norm.x, edge.norm.y, edge.norm.z);
						needsWork = true;
					}
					if (vertMats[4] != vertMats[5]){
						auto edge = data.m_xedges.Get(cellCorner + vertOffsets[4]);
						solver.add(edge.t, 0.f, 1.f, edge.norm.x, edge.norm.y, edge.norm.z);
						needsWork = true;
					}
					if (vertMats[6] != vertMats[7]){
						auto edge = data.m_xedges.Get(cellCorner + vertOffsets[6]);
						solver.add(edge.t, 1.f, 1.f, edge.norm.x, edge.norm.y, edge.norm.z);
						needsWork = true;
					}
					// Y
					if (vertMats[0] != vertMats[2]){
						auto edge = data.m_yedges.Get(cellCorner + vertOffsets[0]);
						solver.add(0.f, edge.t, 0.f, edge.norm.x, edge.norm.y, edge.norm.z);
						needsWork = true;
					}
					if (vertMats[4] != vertMats[6]){
						auto edge = data.m_yedges.Get(cellCorner + vertOffsets[4]);
						solver.add(0.f, edge.t, 1.f, edge.norm.x, edge.norm.y, edge.norm.z);
						needsWork = true;
					}
					if (vertMats[1] != vertMats[3]){
						auto edge = data.m_yedges.Get(cellCorner + vertOffsets[1]);
						solver.add(1.f, edge.t, 0.f, edge.norm.x, edge.norm.y, edge.norm.z);
						needsWork = true;
					}
					if (vertMats[5] != vertMats[7]){
						auto edge = data.m_yedges.Get(cellCorner + vertOffsets[5]);
						solver.add(1.f, edge.t, 1.f, edge.norm.x, edge.norm.y, edge.norm.z);
						needsWork = true;
					}
					// Z
					if (vertMats[0] != vertMats[4]){
						auto edge = data.m_zedges.Get(cellCorner + vertOffsets[0]);
						solver.add(0.f, 0.f, edge.t, edge.norm.x, edge.norm.y, edge.norm.z);
						needsWork = true;
					}
					if (vertMats[1] != vertMats[5]){
						auto edge = data.m_zedges.Get(cellCorner + vertOffsets[1]);
						solver.add(1.f, 0.f, edge.t, edge.norm.x, edge.norm.y, edge.norm.z);
						needsWork = true;
					}
					if (vertMats[2] != vertMats[6]){
						auto edge = data.m_zedges.Get(cellCorner + vertOffsets[2]);
						solver.add(0.f, 1.f, edge.t, edge.norm.x, edge.norm.y, edge.norm.z);
						needsWork = true;
					}
					if (vertMats[3] != vertMats[7]){
						auto edge = data.m_zedges.Get(cellCorner + vertOffsets[3]);
						solver.add(1.f, 1.f, edge.t, edge.norm.x, edge.norm.y, edge.norm.z);
						needsWork = true;
					}
					//
					if (needsWork)
					{
						svd::Vec3 qefOut;
						float error = solver.solve(qefOut, svdTol, numSweeps, pinvTol);
						vec3 fpOut = { qefOut.x, qefOut.y, qefOut.z };
						featurePoints.Set({ x, y, z }) = glm::clamp(fpOut, vec3(0.f), vec3(1.f));
						//featurePoints.Set({ x, y, z }) = glm::normalize(vec3{1.f, 1.f,1.f});
						solver.reset();

						vec3 cellPoint = vec3(x, y, z) + featurePoints.Get({ x, y, z });
						debugDraw->Point(cellPoint, 5.f, pointColor);
					}
				}
			}
		}
		// Generate quads for boundary-crossing edges
		//TODO: Need to smooth out normals for non-sharp edges. Maybe also index the mesh while we're at it
		struct VertData
		{
			vec3 pos;
			vec3 norm;
		};
		Bolts::vec_t <VertData> vbuffer;

		auto addQuad = [](Bolts::vec_t <VertData>& vbuffer, const vec3* verts, vec3 currentCellPos)
		{
			vec3 n1 = glm::cross(verts[0] - verts[2], verts[0] - verts[1]);
			vbuffer.push_back({ currentCellPos + verts[0], n1 });
			vbuffer.push_back({ currentCellPos + verts[2], n1 });
			vbuffer.push_back({ currentCellPos + verts[1], n1 });
			vec3 n2 = glm::cross(verts[0] - verts[1], verts[0] - verts[3]);
			vbuffer.push_back({ currentCellPos + verts[0], n2 });
			vbuffer.push_back({ currentCellPos + verts[1], n2 });
			vbuffer.push_back({ currentCellPos + verts[3], n2 });
		};
		const vec3 meshCenter = vec3{ dims } / 2.f;
		for (unsigned z = 0; z < dims.z - 1; z++) {
			for (unsigned y = 0; y < dims.y - 1; y++) {
				for (unsigned x = 0; x < dims.x - 1; x++) {
					const uvec3 cellCorner{ x, y, z };
					uint8_t vertMats[8] = {
						data.m_matids.Get(cellCorner),
						data.m_matids.Get(cellCorner + uvec3(1, 0, 0)),
						data.m_matids.Get(cellCorner + uvec3(0, 1, 0)),
						data.m_matids.Get(cellCorner + uvec3(0, 0, 1))
					};
					if (*reinterpret_cast<uint32_t*>(vertMats) == 0) // Early out if empty cell
						continue;
					vec3 currentCellPos{ 1.f };
					currentCellPos *= cellCorner;
					currentCellPos -= meshCenter;
					// X
					if (y > 0 && z > 0){
						if (vertMats[0] != vertMats[1]){
							vec3 verts[4] = {
								featurePoints.Get(cellCorner),
								featurePoints.Get(cellCorner + uvec3(0, -1, -1)) + vec3(0.f, -1.f, -1.f),
								featurePoints.Get(cellCorner + uvec3(0, -1, 0)) + vec3(0.f, -1.f, 0.f),
								featurePoints.Get(cellCorner + uvec3(0, 0, -1)) + vec3(0.f, 0.f, -1.f),
							};
							if (vertMats[0] == 0){ // Work out facing direction
								std::swap(verts[2], verts[3]);
							}
							addQuad(vbuffer, verts, currentCellPos);
						}
					}
					// Y
					if (x > 0 && z > 0){
						if (vertMats[0] != vertMats[2]){
							vec3 verts[4] = {
								featurePoints.Get(cellCorner),
								featurePoints.Get(cellCorner + uvec3(-1, 0, -1)) + vec3(-1.f, 0.f, -1.f),
								featurePoints.Get(cellCorner + uvec3(0, 0, -1)) + vec3(0.f, 0.f, -1.f),
								featurePoints.Get(cellCorner + uvec3(-1, 0, 0)) + vec3(-1.f, 0.f, 0.f),
							};
							if (vertMats[0] == 0){ // Work out facing direction
								std::swap(verts[2], verts[3]);
							}
							addQuad(vbuffer, verts, currentCellPos);
						}
					}
					// Z
					if (x > 0 && y > 0){
						if (vertMats[0] != vertMats[3]){
							vec3 verts[4] = {
								featurePoints.Get(cellCorner),
								featurePoints.Get(cellCorner + uvec3(-1, -1, 0)) + vec3(-1.f, -1.f, 0.f),
								featurePoints.Get(cellCorner + uvec3(-1, 0, 0)) + vec3(-1.f, 0.f, 0.f),
								featurePoints.Get(cellCorner + uvec3(0, -1, 0)) + vec3(0.f, -1.f, 0.f),
							};
							if (vertMats[0] == 0){ // Work out facing direction
								std::swap(verts[2], verts[3]);
							}
							addQuad(vbuffer, verts, currentCellPos);
						}
					}
				}
			}
		}
		// Normalize vertex positions
		auto vptr = vbuffer.data();
		for (unsigned i = 0; i < vbuffer.size(); i++)
			vptr[i].pos *= voxelSize;
		//TODO: Generate one sub-mesh per material index
		//TODO: Work out BB extents
		outMesh->subMeshes.clear();
		Rendering::VertexLayout desc;
		desc.AddStream({ Rendering::BGSS_POSITION, Rendering::BGVT_FLOAT, 3 });
		desc.AddStream({ Rendering::BGSS_NORMALS, Rendering::BGVT_FLOAT, 3 });
		//TODO: This copies data. We could do preliminary pass to see how many verts we need to avoid copy
		auto meshVertBuffer = new Rendering::VertexBuffer;
		auto vertOut = meshVertBuffer->SetData(commands, Bolts::Rendering::BUH_STATIC_DRAW, desc, vbuffer.size());
		vertOut.CopyFrom(vbuffer);

		auto meshPrimitives = new Rendering::PrimitiveBuffer;
		meshPrimitives->Build(commands, meshVertBuffer, nullptr, Rendering::BGPT_TRIANGLES);
		Bolts::Rendering::Mesh::SubMesh sm;
		sm.primitiveBuffer = meshPrimitives;
		outMesh->subMeshes.emplace_back(std::move(sm));
	}

	void DrawDebugHermiteData(const HermiteData& data, Bolts::vec3 position, float scale)
	{
		using namespace Bolts;

		auto debugDraw = Bolts::DebugDraw();

		const vec4 pointColor{ 0.f, 0.f, 0.5f, 1.f };
		const vec4 normalColor{ 0.5f, 0.f, 0.f, 1.f };
		uvec3 dims = data.m_matids.GetSize();
		for (unsigned z = 0; z < dims.z - 1; z++) {
			for (unsigned y = 0; y < dims.y - 1; y++) {
				for (unsigned x = 0; x < dims.x - 1; x++) {
					vec3 gridPoint = position + (scale * vec3(x, y, z));
					//debugDraw->DrawDebugPoint(gridPoint, scale, pointColor);

					auto edge = data.m_xedges.Get({ x, y, z });
					vec3 edgeDir = vec3(scale * edge.t, 0.f, 0.f);
					if (glm::length(edge.norm) > 0.f){
						vec3 edgePos = gridPoint + edgeDir;
						debugDraw->Point(gridPoint, scale, pointColor);
						debugDraw->Line(edgePos, edgePos + (edge.norm * scale * 10.f), normalColor);
					}
					edge = data.m_yedges.Get({ x, y, z });
					edgeDir = vec3(0.f, scale * edge.t, 0.f);
					if (glm::length(edge.norm) > 0.f){
						vec3 edgePos = gridPoint + edgeDir;
						debugDraw->Point(gridPoint, scale, pointColor);
						debugDraw->Line(edgePos, edgePos + (edge.norm * scale * 10.f), normalColor);
					}
					edge = data.m_zedges.Get({ x, y, z });
					edgeDir = vec3(0.f, 0.f, scale * edge.t);
					if (glm::length(edge.norm) > 0.f){
						vec3 edgePos = gridPoint + edgeDir;
						debugDraw->Point(gridPoint, scale, pointColor);
						debugDraw->Line(edgePos, edgePos + (edge.norm * scale * 10.f), normalColor);
					}
				}
			}
		}
	}

	void PreviewHermiteData(const HermiteData& data, Bolts::Rendering::TexturePtr texPtr)
	{
		//TODO:Restore
		/*
		using namespace Bolts::Rendering;
		auto& texData = texPtr->EditData();
		texData.type = BTT_3D;
		texData.format = BTF_RGBA;
		texData.hasMips = false;
		texData.dimensions = vec3(data.m_matids.GetSize());
		texData.buffer.resize(texData.GetMipLevelSize(0));
		//
		Bolts::color4* colorBuffer = reinterpret_cast<color4*>(texData.buffer.data());
		const uvec3 dims = data.m_matids.GetSize();
		for (unsigned z = 0; z < dims.z; z++) {
			for (unsigned y = 0; y < dims.y; y++) {
				for (unsigned x = 0; x < dims.x; x++) {
					colorBuffer[x + y*dims.x] = color4(data.m_matids.Get({ x, y, z }) == 1 ? 255 : 0);
				}
			}
		}
		*/
	}

};