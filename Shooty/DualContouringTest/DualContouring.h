#pragma once

#include <bolts_core.h>
#include <Rendering/TextureFwd.h>
#include <Rendering/MeshFwd.h>
#include <Rendering/RenderingCommon.h>

namespace dc
{
	struct EdgeData
	{
		Bolts::vec3 norm;
		float		t;
	};

	template< class DataT>
	struct Grid
	{
		//typedef EdgeData DataT;
		typedef Bolts::uvec3 DimT;
		
		void			Resize(DimT sizes)
		{
			m_data.resize(sizes.x * sizes.y * sizes.z);
			m_size = sizes;
		}
		DataT&			Set(DimT coords)
		{
			return m_data[GetOffset(coords)];
		}
		DataT	Get(DimT coords) const
		{
			return m_data[GetOffset(coords)];
		}
		DimT GetSize() const { return m_size; }
	private:
		uint32_t	GetOffset(DimT coords) const{
			return coords.x + (m_size.x * coords.y) + ( m_size.x * m_size.y * coords.z);
		}

		Bolts::vec_t<DataT> m_data;			
		DimT				m_size;
	};

	struct HermiteData
	{
		typedef Bolts::uvec3 DimT;

		void Resize(DimT sizes) // Number of cells on each axis
		{
			m_matids.Resize(sizes + DimT(1));
			m_xedges.Resize(sizes + DimT( 0, 1, 1));
			m_yedges.Resize(sizes + DimT( 1, 0, 1));
			m_zedges.Resize(sizes + DimT( 1, 1, 0));
		}

		Grid<uint8_t>	m_matids;
		Grid<EdgeData>	m_xedges;
		Grid<EdgeData>	m_yedges;
		Grid<EdgeData>	m_zedges;
	};

	typedef float SDF2DFuncT(Bolts::vec2);
	typedef float SDF3DFuncT(Bolts::vec3);

	void GenHermiteData(SDF3DFuncT func, int volResolution, int intersectionMethod, float distanceTolerance, int maxIterationSteps, HermiteData& out);
	void GenDCMesh(Bolts::Rendering::RCB&, const HermiteData& data, Bolts::Rendering::Mesh* outMesh);

	void DrawDebugHermiteData(const HermiteData& data, Bolts::vec3 position, float scale);

	void PreviewHermiteData(const HermiteData& data, Bolts::Rendering::TexturePtr tex);
};