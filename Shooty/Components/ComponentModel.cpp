#include "StdAfx.h"
#include "Components/ComponentModel.h"
#include "Components/ComponentProperties.h"
#include "Components/GameObject.h"

namespace Bolts {

void ObjectPropsMap::SetProps(componentTag_t tag, propVec_t&& props)
{
	BASSERT(m_propMap.count(tag) == 0);
	m_propMap.emplace(std::make_pair(tag, std::move(props)));
	//return ibpair.first->second;
}

const propVec_t* ObjectPropsMap::GetProps(componentTag_t tag) const
{
	auto it = m_propMap.find(tag);
	if (it != m_propMap.end()){
		return &it->second;
	}else{
		return nullptr;
	}
}

propVec_t* ObjectPropsMap::EditProps(componentTag_t tag)
{
	auto it = m_propMap.find(tag);
	if (it != m_propMap.end()){
		return &it->second;
	}
	else{
		return nullptr;
	}
}

Bolts::vec_t<componentTag_t> ObjectPropsMap::GetTags() const
{
	Bolts::vec_t<componentTag_t> r;
	r.reserve(m_propMap.size());
	for (auto const& pair : m_propMap)
		r.push_back(pair.first);
	return r;
}

//TODO: Move these into a GameObject editor-only file
componentTag_t GetObjectTag()
{
	return Bolts::FourCC<'g', 'o', 'b', 'j'>::v;
}

propVec_t MakePropsForGameObject()
{
	propVec_t r;
	r.emplace_back(MakeDefaultProp<PropVec3>("Position", {}));
	r.emplace_back(MakeDefaultProp<PropQuat>("Rotation", {}));
	r.emplace_back(MakeDefaultProp<PropVec3>("Scale", Bolts::vec3(1.f)));
	return r;
}

PropertyUpdateBindings RegisterGameObjectClassForSerialisation()
{
	PropertyUpdateBindings bindings;
	bindings.BindGetter("Position", BIND_MEMBER_GETTER( GOTransform, GetPosition()));
	bindings.BindGetter("Rotation", BIND_MEMBER_GETTER(GOTransform, GetRotation()));
	bindings.BindGetter("Scale", BIND_MEMBER_GETTER(GOTransform, GetScale()));
	return bindings;
}


};