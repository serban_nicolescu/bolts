#include "StdAfx.h"
#include "Components/LightmapComponent.h"
#include "Components/ComponentModel.h"
#include "Components/ComponentProperties.h"
#include "Assets/AssetManager.h"
#include "Rendering/MaterialNew.h"
#include <Rendering/Mesh.h>

namespace Bolts {

	template<>
	propVec_t MakePropsForCompTag< LightmapComponent::Tag >()
	{
		propVec_t r;
		{
			auto p = MakeDefaultProp<PropInt>("Resolution");
			p->SetVal(128);
			r.emplace_back(p);
		}
		return r;
	}

	template<>
	PropertyUpdateBindings RegisterComponentClassForSerialisation< LightmapComponent::Tag >()
	{
		PropertyUpdateBindings bindings;
		bindings.BindValue("Resolution", BIND_MEMBER_VALUE(LightmapComponent, m_resolution));
		return bindings;
	}

	LightmapComponent::LightmapComponent() {}
	LightmapComponent::~LightmapComponent() {}

	void LightmapComponent::Load(const BinaryMapView& data, CMgrLightmap* mgr)
	{
		m_mgr = mgr;
		m_renderer = &mgr->m_renderer;
		//
		/* bool flagsChanged = */ data.Get("Resolution", m_resolution);
	}
	void LightmapComponent::Reload(const BinaryMapView& data, CMgrLightmap* mgr)
	{
		Load(data, mgr);
	}

	void LightmapComponent::Start()
	{
	}

	void LightmapComponent::OnRemoved()
	{
	}

	void LightmapComponent::Update(float dt, unsigned frameCount)
	{
	}

	void CMgrLightmap::RegisterComponents(GOS & gos)
	{
		gos.RegisterComponentType(Tag, GetNameHash());
		gos.GetComponentModels().SetProps(Tag, MakePropsForCompTag<Tag>());
		gos.SetPropBindings(Tag, RegisterComponentClassForSerialisation<Tag>());
	}

};
