#pragma once

#include "Components/Components.h"
#include <bolts_core.h>
#include <bolts_binarymap.h>
#include <vector>
#include <memory>
#include <algorithm>
#include <functional>

namespace Bolts {

	class GameObject;
	class GOTransform;
	class GOS;

	struct goRef_t
	{
		template< typename T>
		friend struct ::std::hash;

		goRef_t() = default;
		goRef_t(const goRef_t&) = default;
		goRef_t(std::nullptr_t) : go(nullptr),generation(0) {}
		goRef_t(GameObject& igo); //Keeps object ptr

		operator bool();

		bool operator<(const goRef_t& other) const{
			return this->go < other.go;
		}
		bool operator==(const goRef_t& other) const {
			return (go == other.go && generation == other.generation);
		}
		bool operator==(const GameObject* other) const;
		GameObject* operator-> ();
		const GameObject* operator-> ( ) const;
		GameObject& operator* ( );
		const GameObject& operator* ( ) const;
	private:
		GameObject*	go = nullptr;
		goGeneration_t	generation = 0;
	};

	struct goTransformRef_t
	{
		goTransformRef_t() = default;
		goTransformRef_t(const goTransformRef_t&) = default;
		goTransformRef_t(GOTransform& igot);

		operator bool();
		GOTransform* operator-> ();
	private:
		GOTransform*	go = nullptr;
		goGeneration_t	generation = 0;
	};

	const goRef_t					c_invalidGORef{};
	const goTransformRef_t			c_invalidGOTranformRef{};


	class GOTransform{
	public:
		friend class GameObject;
		friend GOTransform operator* (const GOTransform& first, const GOTransform& other);

		GOTransform() :m_rotation(), m_position(), m_scale(1.f) {};
		GOTransform(vec3 pos) :m_rotation(), m_position(pos), m_scale(1.f) {};
		GOTransform(const GOTransform& other) :
			m_rotation(other.m_rotation),
			m_position(other.m_position),
			m_scale(other.m_scale)
		{};

		bool			IsDirty() const { return m_isDirty; }
		void			SetDirty() { m_isDirty = true; }
		const mat4&		GetAbsTransform() const;
		goGeneration_t	GetGenerationID() const { return m_generation; }

		void			SetPosition(vec3 newPos){
			m_position = newPos;
		}
		void			SetScale(vec3 newScale){
			m_scale = newScale;
		}
		void			SetRotation(quat newRot){
			m_rotation = newRot;
		}


		vec3	GetPosition() const { return m_position; }
		vec3	GetScale() const { return m_scale; }
		quat GetRotation() const { return m_rotation; }

		void		UpdateMats() const; ///> If dirty, updates mats
		void		Reload(const BinaryMapView&);
	private:
		mutable mat4	m_absTransform;
		quat			m_rotation;
		vec3			m_position;
		vec3			m_scale;
		goTransformRef_t	m_parent; ///> Null when no parent or the object is static
		goGeneration_t		m_generation = 0;
		mutable bool		m_isDirty = true; ///> True when the abs transform matrix needs updating
	};

	inline GOTransform operator* ( const GOTransform& first, const GOTransform& other );

	class GameObject
	{
	public:
		friend class GOS;
		friend class Space;

		typedef vec_t< goRef_t>	goChildList_t;

		GameObject();
		GameObject(const GameObject&) = delete;
		GameObject& operator= (const GameObject&) = delete;

		// Lifetime
		//void Register();
		//void Unregister();
		void			Reload(const BinaryMapView&);
		goGeneration_t	GetGenerationID() const { return m_generation; }

		bool			IsVisible() const { return m_isVisible; }
		void			SetVisible(bool isVisible) { m_isVisible = isVisible; }

		const GOTransform&	GetTransform() const { return m_transform; }
		bool				IsTransformDirty() const { return m_transform.IsDirty(); }
		GOTransform&		EditTransform() { m_transform.SetDirty(); return m_transform; }

		// Components
		struct compPair {
			componentRef_t ref;
			componentTag_t tag;
		};
		typedef vec_t<compPair>	goComponentList_t;

		const goComponentList_t& GetComponents() const { return m_components; }

		template<class CompMgrT>
		typename CompMgrT::comp_t* TryGetComponent() const
		{
			return CompMgrT::ref_t::TryGet(TryGetComponentRef(CompMgrT::Tag));
		}
		componentRef_t	TryGetComponentRef(componentTag_t tag) const
		{
			auto it = std::lower_bound(m_components.begin(), m_components.end(), tag, [](goComponentList_t::const_reference pair, componentTag_t val) {
				return pair.tag < val;
			});

			if (it != m_components.end() && it->tag == tag)
				return it->ref;
			else
				return componentRef_t();
		}

		// Children
		goRef_t	GetParent() const { return m_parent; }
		void	AddChild(goRef_t childRef);
		void	RemoveChild(goRef_t childRef);
		const goChildList_t& GetChildren() const { return m_children; }
		void	SetChildren(goChildList_t&&); ///> Sets the object's children list if not already set ( asserts if already set )
	protected:
		//void OnRegistered() { }
		void OnRemoved(Space* space);

		template< typename CompRefT>
		void AddComponent( CompRefT ref )
		{
			AddComponent( ref, CompRefT::Tag );
		}
		void AddComponent(componentRef_t ref, componentTag_t tag)
		{
			BASSERT(ref.IsValid());
			BASSERT(!TryGetComponentRef(tag).IsValid());
			m_components.push_back({ref, tag});
		}
		void SortComponents();
		void RemoveComponents(Space* space);
	private:
		//
		bool HasAnyChildren() const { return (m_children.size() > 0); }
		bool HasChild(goRef_t parentRef)
		{
			return (HasAnyChildren() && (std::find(m_children.begin(), m_children.end(), parentRef) != m_children.end()));
		}
		void SetParentInternal(goRef_t parentRef)
		{
			m_parent = parentRef;
		}

		bool				m_isVisible = true;
		goRef_t				m_parent;
		goChildList_t		m_children;
		goComponentList_t	m_components;
		GOTransform			m_transform;
		goGeneration_t		m_generation = 0;
	};
};

namespace std
{
	template <>
	struct hash<Bolts::goRef_t>
	{
		size_t operator()(const Bolts::goRef_t& k) const
		{
			return Bolts::hashCombine(hash<const Bolts::GameObject*>()(k.go), hash<Bolts::goGeneration_t>()(k.generation));
		}
	};
}
