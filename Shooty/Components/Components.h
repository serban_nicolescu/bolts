#pragma once

#include <bolts_core.h>

namespace Bolts {

	typedef uint32_t	componentTag_t;
	typedef uint32_t	componentID_t;

	class ICompMgr;

	struct componentRef_t
	{
	public:
		componentRef_t() {}
		componentRef_t(void* p, ICompMgr* m) :ptr(p), mgr(m) {}

		bool IsValid() const { return (mgr && (ptr != nullptr)); }
		ICompMgr* TryGetMgr() {	return mgr;	}
		void* GetUnsafePtr() { return ptr; }
	protected:
		void*		ptr = nullptr;
		ICompMgr*	mgr = nullptr;
	};

	template<class CompMgrT, class CompT>
	struct typedComponentRef_t : public componentRef_t
	{
	public:
		static const componentTag_t Tag = CompT::Tag;

		typedComponentRef_t( componentRef_t other ): componentRef_t( other ) {}
		typedComponentRef_t( CompT* inCmp, CompMgrT* inMgr ):componentRef_t{ inCmp, inMgr }  {}
		
		static typedComponentRef_t TryCast( componentRef_t ref )
		{
			if( ref.IsValid() && ( typedComponentRef_t( ref ).mgr->GetTag() == Tag ) )
				return typedComponentRef_t( ref );
			else
				return typedComponentRef_t();
		}

		static CompT* TryGet(componentRef_t ref)
		{
			if( ref.IsValid() && typedComponentRef_t( ref ).mgr->GetTag() == Tag )
				return typedComponentRef_t( ref ).TryGet();
			else
				return nullptr;
		}

		CompT*	TryGet(){
			BASSERT( mgr );
			return TryGetImpl< CompMgrT::k_refsAreSimple>();
		}

	private:
		template<bool>
		CompT*	TryGetImpl();

		template<>
		CompT*	TryGetImpl<false>()
		{
			return (static_cast<CompMgrT*>(mgr)->Get(ptr));
		}

		template<>
		CompT*	TryGetImpl<true>()
		{
			return static_cast<CompT*>( ptr );
		}
	};

	typedef uint32_t		goID_t;
	typedef unsigned short	goGeneration_t;

	static const componentID_t				c_invalidComponentID = 0;
};
