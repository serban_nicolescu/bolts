#include "StdAfx.h"
#include "Components/ComponentProperties.h"

namespace Bolts {
	void PropertyUpdateBindings::BindValue(const char * propName, ptrOffset_t  objPtrOffset, float *) {
		BindValueImpl<float>(propName, objPtrOffset);
	}
	void PropertyUpdateBindings::BindValue(const char * propName, ptrOffset_t  objPtrOffset, int *) {
		BindValueImpl<int>(propName, objPtrOffset);
	}
	void PropertyUpdateBindings::BindValue(const char * propName, ptrOffset_t  objPtrOffset, bool *) {
		BindValueImpl<bool>(propName, objPtrOffset);
	}
	void PropertyUpdateBindings::BindValue(const char * propName, ptrOffset_t  objPtrOffset, Bolts::vec3*) {
		BindValueImpl<Bolts::vec3>(propName, objPtrOffset);
	}
	/*void PropertyUpdateBindings::BindValue(const char * propName, ptrOffset_t  objPtrOffset, vec4*) {
		BindValueImpl<vec4>(propName, objPtrOffset);
	}*/
	void PropertyUpdateBindings::BindValue(const char * propName, ptrOffset_t  objPtrOffset, Bolts::quat*) {
		BindValueImpl<Bolts::quat>(propName, objPtrOffset);
	}

	template<typename T>
	void PropertyUpdateBindings::BindValueImpl(const char* propName, ptrOffset_t objPtrOffset)
	{
		ValueBinding newBinding;
		newBinding.propertyName = propName;
		newBinding.memberOffset = objPtrOffset;
		newBinding.updateFunction = [](const void* objectPtr, PropertyUpdateBindings::ptrOffset_t memberOffset, IProp* prop)
		{
			if (auto valProp = PropCast<T>(prop)) {
				valProp->val = *reinterpret_cast<const T*>(static_cast<const char*>(objectPtr) + memberOffset);
			}
		};
		m_valueBindings.emplace_back(std::move(newBinding));
	}
	
	void PropertyUpdateBindings::BindGetter(const char* propName, getterUpdateFunc* updateFunc)
	{
		GetterBinding newBinding;
		newBinding.propertyName = propName;
		newBinding.updateFunction = updateFunc;
		m_getterBindings.emplace_back(std::move(newBinding));
	}

	void PropertyUpdateBindings::RefreshProperties(propVec_t & propertyVector, const void * object) const
	{
		for (auto& valbind : m_valueBindings) {
			IProp* prop = PropVecGetProp(propertyVector, valbind.propertyName);
			if (prop)
			{
				valbind.updateFunction(object, valbind.memberOffset, prop);
			}
			else {
				//TODO: Error, property not found
			}
		}

		for (auto& getbind : m_getterBindings) {
			IProp* prop = PropVecGetProp(propertyVector, getbind.propertyName);
			if (prop)
			{
				getbind.updateFunction(object, prop);
			}
			else {
				//TODO: Error, property not found
			}
		}
	}

};