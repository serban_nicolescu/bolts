#pragma once

#include "GUI/Properties.h"
#include "Components/Components.h"
#include <bolts_core.h>


namespace Bolts
{

#define BIND_MEMBER_VALUE(className,memberName) offsetof(className,memberName), GetMemberType(&className::memberName)
#define BIND_MEMBER_GETTER(className,memberFunctionCall) \
	[](const void* object, IProp* prop) { if (auto valProp = PropCast<decltype(((className*)nullptr)->memberFunctionCall)>(prop)) valProp->SetVal(static_cast<const className*>(object)->memberFunctionCall); }
#define BIND_MEMBER_GETTER_TYPED(className, propType, memberFunctionCall) \
	[](const void* object, IProp* prop) { if (auto valProp = propType::Cast(prop)) valProp->SetVal(static_cast<const className*>(object)->memberFunctionCall); }

	class PropertyUpdateBindings
	{
	public:
		typedef unsigned ptrOffset_t;
		typedef void getterUpdateFunc(const void* object, IProp* prop);

		void BindValue(const char* propName, ptrOffset_t objPtrOffset, float*);
		void BindValue(const char* propName, ptrOffset_t objPtrOffset, int*);
		void BindValue(const char* propName, ptrOffset_t objPtrOffset, bool*);
		void BindValue(const char* propName, ptrOffset_t objPtrOffset, Bolts::vec3*);
		void BindValue(const char* propName, ptrOffset_t objPtrOffset, Bolts::quat*);
		//void BindValue(const char* propName, ptrOffset_t objPtrOffset, Bolts::hash32_t*);
		void BindGetter(const char* propName, getterUpdateFunc*); 
		//TODO: Add templated version that binds any struct by searching for its PropertyUpdateBindings
		//TODO: Add versions for binding arrays

		void RefreshProperties(propVec_t& propertyVector, const void* object) const;

	private:
		typedef void valueUpdateFunc(const void* object, ptrOffset_t memberOffset, IProp* prop);

		// Binds a member variable directly to a property. Property value is set from variable value
		struct ValueBinding
		{
			const char* propertyName;
			valueUpdateFunc* updateFunction;
			ptrOffset_t memberOffset;
		};
		//
		struct GetterBinding
		{
			const char* propertyName;
			getterUpdateFunc* updateFunction;
		};


		template<typename T>
		void BindValueImpl(const char* propName, ptrOffset_t objPtrOffset);

		Bolts::vec_t<ValueBinding> m_valueBindings;
		Bolts::vec_t<GetterBinding> m_getterBindings;
	};

};
