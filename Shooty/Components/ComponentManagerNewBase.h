#pragma once

#include "Components/Components.h"
#include "Components/Space.h"
#include "Components/GameObject.h"
#include <bolts_core.h>
#include <bolts_binarymap.h>

namespace Bolts {

	class ICompMgr
	{
	public:
		ICompMgr(componentTag_t tag, const char* name) :
			m_tag(tag), m_nameHash(stringHash_t(name)), m_name(name) {}
		virtual ~ICompMgr() {};

		unsigned				GetTag() inline const { return m_tag; }
		hash32_t				GetNameHash() inline const { return m_nameHash; }
		virtual unsigned		GetPriority( Space* ) const ///> Given this Space, returns the priority this mgr needs. Higher is later
		{
			return 0;
		}
		virtual void			RegisterComponents(GOS&) = 0;
		//
		virtual void Init() {};
		virtual void Release() {};
		virtual void RegisterSpace(Space* newSpace) = 0;
		virtual void ClearSpace(Space* newSpace) = 0;
		//
		virtual const char* GetComponentName(componentRef_t) { return m_name.c_str(); };
		//
		virtual	void LoadComponent(Space*, componentTag_t typeTag, const BinaryMapView& bm) = 0; ///> Pushes component on loaded stack to be popped by owning object
		virtual componentRef_t	PopComponent(Space*, goRef_t) = 0;///>Loads component instance from loaded stack for the given GO.
		virtual void RemoveComponent(Space*, componentRef_t) = 0;///> Marks component as removed and will call OnDisabled and OnReleased on it
		// LO now has all components and parent/children
		virtual void ProcessPendingStarts(Space*) = 0; // Calls starts on recently loaded components and enables them
		// LO has been registered to a Space so comps cand find other objects
		virtual void ProcessPendingEnables() = 0; // Adds/Removes components to update lists and calls OnEnabled/OnRemoved
		virtual	void ReloadComponent(componentRef_t, const BinaryMapView& bm) = 0; ///> Applies settings in binary map to an existing instance of a component
		virtual void UpdateComponents(float dt, unsigned frameNo) = 0;
		virtual void EnableComponent(componentRef_t) = 0;
		virtual void DisableComponent(componentRef_t) = 0;
	private:
		componentTag_t		m_tag;
		str_t				m_name;
		hash32_t			m_nameHash;
	};
	
};

