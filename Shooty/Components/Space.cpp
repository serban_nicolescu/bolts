#include "StdAfx.h"
#include "Components/ComponentManagerNewBase.h"
#include "Space.h"

namespace Bolts {

	Space::~Space()
	{
		RemoveGO(GetRoot());
		FlushPendingOperations();
		FlushPendingOperations();
	}

	// Space
	goRef_t Space::AddGO()
	{
		if (!m_goDeletedList.empty())
		{
			m_goToRegisterList.emplace_back(std::move(m_goDeletedList.back()));
			m_goDeletedList.pop_back();
		}
		else{
			m_goToRegisterList.emplace_back(new GameObject);
		}

		return *m_goToRegisterList.back();
	}

	void Space::RemoveGO(goRef_t ref)
	{
		static int depth = 0;
		BASSERT(ref);
		BASSERT(depth < 7);//this is a recursive function. it'll work, but it's better to avoid having too deep of a hierarchy
		depth++;

		// remove children first
		for (auto& childPtr : ref->GetChildren())
			RemoveGO(childPtr);

		// find obj ptr based on ref
		for (unsigned i = 0; i < m_goRegisteredList.size(); i++)
		{
			if (ref == m_goRegisteredList[i].get()) {
				std::swap(m_goRegisteredList[i], m_goRegisteredList.back());
				m_goToRemoveList.emplace_back(std::move(m_goRegisteredList.back()));
				m_goRegisteredList.pop_back();
				depth--;
				return;
			}
		}
		BASSERT_MSG(false, "Attempting to remove unregistered GO");

	}

	void Space::FlushPendingOperations()
	{
		//for (auto& obj : m_goToRegisterList)
		//{
		//	obj->OnRegistered();
		//}
		std::move(m_goToRegisterList.begin(), m_goToRegisterList.end(), std::back_inserter(m_goRegisteredList));
		m_goToRegisterList.clear();


		for (auto& obj : m_goToRemoveList)
		{
			obj->OnRemoved(this);
		}
		std::move(m_goToRemoveList.begin(), m_goToRemoveList.end(), std::back_inserter(m_goDeletedList));
		m_goToRemoveList.clear();
	}

	//void Space::UpdateObjectTransforms()
	//{
	//	for (unsigned idx = 0; idx < m_goTransformDirty.size(); idx++)
	//	{
	//		if (m_goTransformDirty[idx])
	//		{
	//			UpdateAbsTransform(idx);
	//		}
	//	}
	//}

	// Space


};