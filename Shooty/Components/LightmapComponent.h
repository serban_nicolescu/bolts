#pragma once

#include "Components/ComponentManagerGameplayBase.h"
#include "Components/Components.h"
#include "Rendering/Mesh.h"
#include "Assets/Assets.h"

namespace Bolts {
	class CMgrLightmap;

	class LightmapComponent
	{
	public:
		const static componentTag_t	Tag = FourCC<'l', 'm', 'a', 'p'>::v;

		LightmapComponent();
		~LightmapComponent();

		void Load(const BinaryMapView& data, CMgrLightmap* mgr);
		void Reload(const BinaryMapView& data, CMgrLightmap* mgr);

		void Loaded( goRef_t go ) { m_go = go; }
		void PreStart() {}
		void Start();

		void OnEnabled() {}
		void OnDisabled() {}
		void OnRemoved();

		void Update(float dt, unsigned frameCount);

		int32_t m_resolution = 128;
	private:
		goRef_t				m_go;
		CMgrLightmap*		m_mgr = nullptr;
		Rendering::Renderer* m_renderer = nullptr;
	};

	class CMgrLightmap:public detail::CMgrGameplayBase<CMgrLightmap, LightmapComponent>
	{
	public:
		friend class LightmapComponent;

		CMgrLightmap(assets::AssetManager& resources, Rendering::Renderer& newr) :CMgrGameplayBase("Lightmap"), m_assetManager(resources),m_renderer(newr){}
		virtual void RegisterComponents(GOS& gos) override;
		//virtual void UpdateComponents(float dt, unsigned frameNo) override;
	private:
		assets::AssetManager&	m_assetManager;
		Rendering::Renderer&	m_renderer;
	};

};