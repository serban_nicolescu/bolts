#include "StdAfx.h"
#include "Components/MeshComponent.h"
#include "Components/ComponentModel.h"
#include "Components/ComponentProperties.h"
#include "Assets/AssetManager.h"
#include "Rendering/MaterialNew.h"
#include <Rendering/Mesh.h>

namespace Bolts {

	template<>
	propVec_t MakePropsForCompTag< MeshComponent::Tag >()
	{
		propVec_t r;
		r.emplace_back( MakeDefaultPropAssetRef("Mesh Asset", Rendering::Mesh::c_assetType));
		//r.emplace_back( MakeDefaultPropAssetRef("Material Instance", Rendering::Material::c_assetType));
		r.emplace_back( MakeDefaultPropAssetRefV("Materials", Rendering::Material::c_assetType));
		{
			auto p = MakeDefaultProp<PropBool>("Static");
			p->SetVal(true);
			r.emplace_back(p);
		}
		{
			auto p = MakeDefaultProp<PropBool>("Visible");
			p->SetVal(true);
			r.emplace_back(p);
		}
		{
			auto p = MakeDefaultProp<PropBool>("Cast Shadows");
			p->SetVal(true);
			r.emplace_back(p);
		}
		return r;
	}

	template<>
	PropertyUpdateBindings RegisterComponentClassForSerialisation< MeshComponent::Tag >()
	{
		PropertyUpdateBindings bindings;
		bindings.BindGetter("Visible", BIND_MEMBER_GETTER(MeshComponent, GetVisibility()));
		bindings.BindGetter("Cast Shadows", BIND_MEMBER_GETTER(MeshComponent, IsShadowCaster()));
		bindings.BindGetter("Mesh Asset", BIND_MEMBER_GETTER_TYPED(MeshComponent, PropAsset, GetMeshAssetID()));

		bindings.BindGetter("Materials", [](const void* optr, IProp* pptr) {
			auto object = static_cast<const MeshComponent*>(optr);
			if (!object->GetMesh())
				return;
			if (auto prop = PropAssetV::Cast(pptr)) {
				auto& matVec = prop->EditVal();
				matVec.resize(object->GetNumMaterials());
				for (int i = 0; i < object->GetNumMaterials(); i++)
					matVec[i] = object->GetMaterialID(i);
			}
		});
		
		return bindings;
	}

	MeshComponent::MeshComponent() {}
	MeshComponent::~MeshComponent() {}

	void MeshComponent::SetMesh(regHash_t meshID)
	{
		auto meshPtr = m_mgr->m_assetManager.LoadAsset<Rendering::Mesh>(meshID);
		m_mesh = meshPtr;
		if (m_visibilityID != -1 && m_staticMeshID != -1)
			SetMeshInternal();
	}

	assetID_t MeshComponent::GetMeshAssetID() const
	{
		return (m_mesh ? m_mesh->GetAssetID() : 0); 
	}

	void MeshComponent::SetMaterial(int idx, regHash_t matID)
	{
		if (idx >= m_materials.size())
			m_materials.resize(idx + 1);
		//TODO: This needs to be setting material in the renderable, not the mesh itself
		auto matPtr = m_mgr->m_assetManager.LoadAsset<Rendering::Material>(matID);
		if (matPtr && matPtr->IsValid())
			m_materials[idx] = matPtr;

		m_matsChanged = true;
		//TODO: Log invalid material error
		//if (matPtr && matPtr->IsValid() && m_mesh) {
		//	for (auto& subMesh : m_mesh->m_subMeshes)
		//		subMesh.m_Material = matPtr;
		//}
	}

	assetID_t MeshComponent::GetMaterialID(int idx) const
	{
		if (idx >= m_materials.size())
			return {};

		Rendering::Material* mat = m_materials[idx].get();
		return mat ? mat->GetAssetID() : assetID_t{};
	}

	void MeshComponent::SetVisibility(bool v)
	{
		m_isVisible = v;
	}

	void MeshComponent::Load(const BinaryMapView& data, CMgrMesh* mgr)
	{
		m_mgr = mgr;
		m_renderer = &mgr->m_renderer;
		//
		bool flagsChanged = data.Get("Visible", m_isVisible);
		flagsChanged |= data.Get("Cast Shadows", m_castShadows);
		if (flagsChanged)
			UpdateRenderMasks();

		hash32_t meshID = 0;
		if (data.Get("Mesh Asset", meshID) && meshID) {
			SetMesh(meshID);
		}
		auto matArray = data.GetArray< hash32_t>("Materials");
		if (matArray.IsValid()) {
			unsigned numMats = matArray.GetSize();
			m_materials.resize(numMats);
			for (int i = numMats - 1; i >= 0; i--)
				SetMaterial(i, matArray[i]);
		}
	}
	void MeshComponent::Reload(const BinaryMapView& data, CMgrMesh* mgr)
	{
		Load(data, mgr);
	}

	void MeshComponent::Start()
	{
		using namespace Rendering;
		m_visibilityID = m_renderer->AddVisibilityObject();
		UpdateRenderMasks();
		auto& renderable = m_renderer->AddStaticMesh();
		m_staticMeshID = renderable.GetRenderableID();
		m_renderer->AttachRenderable(m_visibilityID, m_staticMeshID);
		if (!m_mesh)
			return;
		SetMeshInternal();
	}

	void MeshComponent::OnRemoved()
	{
		m_renderer->RemoveRenderable(m_staticMeshID);
		m_renderer->RemoveVisibilityObject(m_visibilityID);
	}

	void MeshComponent::Update(float dt, unsigned frameCount)
	{
		// position changes
		//TODO: Update visibility obj data only when something changes ( world transform or mesh )
		UpdateVOData();
		auto& renderable = m_renderer->GetStaticMesh(m_staticMeshID);
		renderable.SetWorldTransform(m_go->GetTransform().GetAbsTransform());
		// mesh changes
		uint8_t currentReload = m_mesh->GetReloadCount();
		if (currentReload != m_meshReload) {
			m_meshReload = currentReload;

			UpdateVOData();
			UpdateMaterialCount();
		}
		// mat changes
		if (m_matsChanged) {
			m_matsChanged = false;
			renderable.m_materials.resize(m_materials.size());
			for (int i = 0; i < m_materials.size(); i++)
				renderable.m_materials[i] = m_materials[i];
		}
	}

	void MeshComponent::SetMeshInternal()
	{
		UpdateVOData();
		auto& staticMesh = m_renderer->GetStaticMesh(m_staticMeshID);
		staticMesh.m_mesh = m_mesh;
		UpdateMaterialCount();
	}

	void MeshComponent::UpdateVOData()
	{
		auto& goTransform = m_go->GetTransform();
		vec3 stupid = glm::max(glm::abs(m_mesh->aabb.min), glm::abs(m_mesh->aabb.max));
		const float voRadius = glm::length(stupid * goTransform.GetScale());
		m_renderer->SetVOTransformAndRadius(m_visibilityID, goTransform.GetAbsTransform(), voRadius);
	}

	void MeshComponent::UpdateRenderMasks()
	{
		if (m_visibilityID == badHandle)
			return;
		using namespace Rendering;

		uint8_t m{};
		if (m_isVisible) {
			m = MAIN_VIEW | REFLECTIONS_VIEW;
			if (m_castShadows)
				m |= SHADOW_VIEW;
		}
		m_renderer->SetVOLists(m_visibilityID, (voListMask_t)m);
	}

	void MeshComponent::UpdateMaterialCount()
	{
		auto meshMatCount = m_mesh->materials.size();
		if (meshMatCount != m_materials.size()) {
			m_materials.resize(meshMatCount);
			int i = 0;
			for (auto& matPtr : m_materials) {
				if (!matPtr || !matPtr->IsValid()) {
					auto& meshMat = m_mesh->materials[i];
					SetMaterial(i, regHash_t(meshMat.name.empty() ? "BadMat" : meshMat.name));
				}
				i++;
			}
		}
	}

	//void CMgrMesh::UpdateComponents(float dt, unsigned frameNo)
	//{
	//	for (comp_t* compP : m_enabledComponents)
	//	{
	//		compP->
	//	}
	//}


	void CMgrMesh::RegisterComponents(GOS & gos)
	{
		gos.RegisterComponentType(Tag, GetNameHash());
		gos.GetComponentModels().SetProps(Tag, MakePropsForCompTag<Tag>());
		gos.SetPropBindings(Tag, RegisterComponentClassForSerialisation<Tag>());
	}

};
