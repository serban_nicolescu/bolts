#include "StdAfx.h"
#include "GOS.h"
#include "Components/ComponentManagerNewBase.h"
#include "Components/ComponentProperties.h"
#include <bolts_helpers.h>

namespace Bolts {

	GOS::GOS()
	{

	}

	GOS::~GOS()
	{
		Release();
	}

	void GOS::Init()
	{
		m_componentModels.SetProps(GetObjectTag(), MakePropsForGameObject());
		SetPropBindings(GetObjectTag(), RegisterGameObjectClassForSerialisation());
	}

	void GOS::RegisterCompMgr(ICompMgr* mgr)
	{
		m_managers.emplace_back(mgr);
	}

	void GOS::RegisterComponentType(componentTag_t compTag, stringHash_t mgrName)
	{
		BASSERT(m_componentTypeMap.count(compTag) == 0);
		for (auto& mgr : m_managers){
			if (mgr->GetNameHash() == mgrName){
				m_componentTypeMap[compTag] = mgr.get();
				return;
			}
		}
		BASSERT_MSG(m_componentTypeMap.count(compTag) == 1, "Error registering component tag. Comp manager not found.");
	}

	ICompMgr* GOS::FindComponentManagerForTag(componentTag_t tag)
	{
		ICompMgr* mgr = try_find(m_componentTypeMap, tag);
		BASSERT_MSG(mgr != nullptr, "Component manager not found");
		return mgr;
	}

	ICompMgr* GOS::FindComponentManagerByName(stringHash_t mgrName)
	{
		for (auto& mgr : m_managers){
			if (mgr->GetNameHash() == mgrName)
				return mgr.get();
		}
		BASSERT_MSG(false, "Component manager not found");
		return nullptr;
	}

	Space* GOS::NewSpace()
	{
		Space* sp = new Space();
		m_spaces.push_back(sp);

		for (auto& cmg : m_managers)
			cmg->RegisterSpace(sp);

		// Spawn root GO
		goRef_t root = SpawnGO(sp, m_rootObjPrototype);
		sp->SetRoot(root);
		Start(sp);
		//
		return sp;
	}

	void GOS::DestroySpace(Space* sp)
	{
		try_remove(m_spaces, sp);
		delete sp;
	}

	void GOS::Start( Space* s)
	{
		for( auto& cmg : m_managers )
			cmg->ProcessPendingStarts( s );

		s->FlushPendingOperations();
	}

	void GOS::Update( float dt, unsigned frameNo )
	{
		for (auto& cmg : m_managers)
			cmg->ProcessPendingEnables();

		for( auto& cmg : m_managers )
			cmg->UpdateComponents( dt, frameNo );

		for (Space* s : m_spaces)
			s->FlushPendingOperations();
	}

	void GOS::Release()
	{
		for (auto& cmg : m_managers) {
			cmg->ProcessPendingEnables();
			cmg->ProcessPendingEnables();
		}
		BASSERT(m_spaces.empty());
	}
	//
	goRef_t GOS::SpawnGO( Space* space, const GOPrototype& proto )
	{
		goRef_t newGo = space->AddGO();
		ReloadGO(space, newGo, proto);
		//TODO: This code doesn't really belong here
		if (space->GetRoot())
			space->GetRoot()->AddChild(newGo);
		return newGo;
	}

	void GOS::RemoveGO(Space* space, goRef_t goRef)
	{
		space->RemoveGO(goRef);
	}

	void GOS::ReloadGO(Space* space, goRef_t goRef, const GOPrototype& proto)
	{
		goRef->RemoveComponents(space);

		BinaryMapBuilder builder;
		ObjectPropsMap objectProperties;
		// Create components
		for (auto tag : proto.m_components) {
			const propVec_t* compProtoProps = proto.m_props.GetProps(tag);
			propVec_t compProps = CopyPropVec(*(compProtoProps ? compProtoProps : m_componentModels.GetProps(tag)));
			PropVecToBinary(compProps, builder);
			if (tag == GetObjectTag()) {
				goRef->Reload(builder.GetTempMap());
			}
			else {
				ICompMgr* cmgr = try_find(m_componentTypeMap, tag);
				BASSERT_MSG(cmgr, "Unregistered component tag requested when spawning object");
				if (cmgr) {
					cmgr->LoadComponent(space, tag, builder.GetTempMap());
					componentRef_t newComp = cmgr->PopComponent(space, goRef);
					goRef->AddComponent(newComp, tag);
				}
			}
			objectProperties.SetProps(tag, std::move(compProps));
		}
		goRef->SortComponents();
		//
		map_replace(m_objectProperties, goRef, std::move(objectProperties));
	}

	GOPrototype& GOS::BuildTempGOPrototypeSlow(goRef_t goRef)
	{
		auto objProps = GetObjectProperties(goRef);
		BASSERT(objProps);
		//TODO: Remove small allocations
		static GOPrototype g_proto;
		g_proto.Clear();
		g_proto.m_components = objProps->GetTags();
		for (componentTag_t cTag : g_proto.m_components) {
			auto propVecPtr = objProps->GetProps(cTag);
			BASSERT(propVecPtr);
			auto propVecCpy = CopyPropVec(*propVecPtr);
			g_proto.m_props.SetProps(cTag, std::move(propVecCpy));
		}
		return g_proto;
	}

	void GOS::AddComponent(Space* space, goRef_t goRef, componentTag_t cTag)
	{
		GOPrototype& g_proto = BuildTempGOPrototypeSlow(goRef);
		
		g_proto.m_components.push_back(cTag);//TODO: Add unique
		
		ReloadGO(space, goRef, g_proto);
	}

	void GOS::RemoveComponent(Space* space, goRef_t goRef, componentTag_t cTag)
	{
		GOPrototype& g_proto = BuildTempGOPrototypeSlow(goRef);

		try_remove(g_proto.m_components, cTag);

		ReloadGO(space, goRef, g_proto);
	}


	// Editor
	void GOS::SetPropBindings(componentTag_t tag, PropertyUpdateBindings&& bindings)
	{
		m_typeBindings.emplace(std::make_pair(tag, std::move(bindings)));
	}

	const PropertyUpdateBindings* GOS::GetPropBindings(componentTag_t tag) const 
	{
		const PropertyUpdateBindings* b = try_find(m_typeBindings, tag);
		BASSERT_MSG(b != nullptr, "Couldn't find prop bindings for component tag");
		return b;
	}

};