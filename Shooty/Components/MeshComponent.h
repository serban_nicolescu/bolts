#pragma once

#include "Components/ComponentManagerGameplayBase.h"
#include "Components/Components.h"
#include "Rendering/Mesh.h"
#include "Assets/Assets.h"

namespace Bolts {
	class CMgrMesh;

	class MeshComponent
	{
	public:
		using Mesh = Rendering::Mesh;
		using MeshPtr = Rendering::MeshPtr;

		const static componentTag_t	Tag = FourCC<'m', 'e', 's', 'h'>::v;

		MeshComponent();
		~MeshComponent();

		void			SetMesh(regHash_t meshID);
		MeshPtr			GetMesh() { return m_mesh; }
		const MeshPtr	GetMesh() const { return m_mesh; }
		assetID_t		GetMeshAssetID() const;
		void			SetMaterial(int idx, regHash_t matID);
		assetID_t		GetMaterialID(int idx) const;
		int				GetNumMaterials() const { return m_materials.size(); }
		void			SetVisibility(bool);
		bool			GetVisibility() const { return m_isVisible; }
		bool			IsShadowCaster() const { return m_castShadows; }

		void Load(const BinaryMapView& data, CMgrMesh* mgr);
		void Reload(const BinaryMapView& data, CMgrMesh* mgr);

		void Loaded( goRef_t go ) {
			m_go = go;
		}
		void PreStart() {}
		void Start();

		void OnEnabled() {}
		void OnDisabled() {}
		void OnRemoved();

		void Update(float dt, unsigned frameCount);

	private:
		void SetMeshInternal();
		void UpdateVOData();
		void UpdateRenderMasks();
		void UpdateMaterialCount();

		typedef vec_t<Rendering::MaterialPtr> matVec_t;
		MeshPtr				m_mesh;
		matVec_t			m_materials;
		goRef_t				m_go;
		CMgrMesh*			m_mgr = nullptr;
		Rendering::Renderer* m_renderer = nullptr;
		bool				m_isVisible = true;
		bool				m_castShadows = true;
		bool				m_matsChanged = false;
		uint8_t				m_meshReload = 0;
		Rendering::voHandle_t			m_visibilityID = -1;
		Rendering::renderableHandle_t	m_staticMeshID = -1;
	};

	class CMgrMesh:public detail::CMgrGameplayBase<CMgrMesh, MeshComponent>
	{
	public:
		friend class MeshComponent;

		CMgrMesh(assets::AssetManager& resources, Rendering::Renderer& newr) :CMgrGameplayBase("Mesh"), m_assetManager(resources),m_renderer(newr){}


		virtual void RegisterComponents(GOS& gos) override;
		//virtual void UpdateComponents(float dt, unsigned frameNo) override;
	private:
		assets::AssetManager&	m_assetManager;
		Rendering::Renderer&	m_renderer;
	};

};