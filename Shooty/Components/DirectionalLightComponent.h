#pragma once

#include "Components/ComponentManagerGameplayBase.h"
#include "Components/Components.h"
#include "Rendering/MeshFwd.h"

namespace Bolts {

	namespace assets {
		class AssetManager;
	}
	class CMgrLight;

	class DirectionalLightComponent
	{
	public:
		const static componentTag_t	Tag = Bolts::FourCC<'d', 'i', 'r', 'l'>::v;

		DirectionalLightComponent();
		~DirectionalLightComponent();

		void Load(const Bolts::BinaryMapView& data, CMgrLight* mgr);
		void Reload(const Bolts::BinaryMapView& data, CMgrLight* mgr);

		void Loaded(goRef_t go) {
			m_go = go;
		}
		void PreStart() {}
		void Start() {}

		void OnEnabled() {}
		void OnDisabled() {}
		void OnRemoved() {}

		void Update(float dt, unsigned frameCount);

		void SetLightColor(Bolts::vec3 c) { m_lightColor = c; }
		Bolts::vec3 GetLightColor() const { return m_lightColor; }
	private:
		Bolts::vec3		m_lightColor;
		goRef_t			m_go;
		CMgrLight*		m_mgr = nullptr;
	};

	class CMgrLight:public detail::CMgrGameplayBase<CMgrLight, DirectionalLightComponent>
	{
	public:
		friend class DirectionalLightComponent;

		CMgrLight(Bolts::assets::AssetManager& resources, Bolts::Rendering::Renderer& newr) :CMgrGameplayBase("Lights"), m_resources(resources), m_renderer(newr) {}


		virtual void RegisterComponents(GOS& gos) override;
		//virtual void UpdateComponents(float dt, unsigned frameNo) override;
	private:
		Bolts::assets::AssetManager&	m_resources;
		Bolts::Rendering::Renderer&	m_renderer;
	};

};