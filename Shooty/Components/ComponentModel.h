#pragma once

#include "GUI/Properties.h"
#include "Components/Components.h"

namespace Bolts
{
	class PropertyUpdateBindings;

	componentTag_t GetObjectTag();
	propVec_t MakePropsForGameObject();
	template< componentTag_t tag>
	propVec_t MakePropsForCompTag();
	
	PropertyUpdateBindings RegisterGameObjectClassForSerialisation();
	template< componentTag_t tag>
	PropertyUpdateBindings RegisterComponentClassForSerialisation();


	class ObjectPropsMap
	{
	public:
		ObjectPropsMap() = default;
		~ObjectPropsMap() = default;
		ObjectPropsMap(ObjectPropsMap&& other) = default;
		ObjectPropsMap& operator=(ObjectPropsMap&& other) = default;
		ObjectPropsMap(const ObjectPropsMap&) = delete;
		ObjectPropsMap& operator=(const ObjectPropsMap&) = delete;

		void					Clear() { m_propMap.clear(); }
		void					SetProps(componentTag_t tag, propVec_t&& props);
		propVec_t*				EditProps(componentTag_t);

		const propVec_t*		GetProps(componentTag_t) const;
		vec_t<componentTag_t>	GetTags() const;
	private:
		hash_map_t< componentTag_t, propVec_t> m_propMap;
	};

};