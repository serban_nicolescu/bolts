#include "StdAfx.h"
#include "GameObject.h"
#include "ComponentManagerNewBase.h"
#include <bolts_assert.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <algorithm>

namespace Bolts {
	
	// Game Object
	GameObject::GameObject() = default;

	void GameObject::AddChild(goRef_t child)
	{
		if (!HasChild(child))
		{
			m_children.push_back(child);
			if (child->GetParent())
				child->GetParent()->RemoveChild(child);
			child->SetParentInternal(*this);
		}
	}

	void GameObject::RemoveChild(goRef_t child)
	{
		auto it = std::find(m_children.begin(), m_children.end(), child);
		if (it != m_children.end())
			m_children.erase(it);
		//TODO: This should set root as child parent
		child->SetParentInternal(nullptr);
	}
	void GameObject::SetChildren(goChildList_t&& newChildren)
	{
		BASSERT(m_children.empty());
		m_children = std::move(newChildren);
	}

	void GameObject::SortComponents()
	{
		std::sort( m_components.begin(), m_components.end(), []( goComponentList_t::const_reference f, goComponentList_t::const_reference s ) {
			return f.tag < s.tag;
		} );
	}

	void GameObject::RemoveComponents(Space* space)
	{
		for (auto compPair : m_components) {
			ICompMgr* cmgr = compPair.ref.TryGetMgr();
			BASSERT(cmgr);
			if (cmgr)
				cmgr->RemoveComponent(space, compPair.ref);
		}
		m_components.clear();
	}

	void GameObject::OnRemoved(Space* space) {
		RemoveComponents(space);
		if (m_parent)
			m_parent->RemoveChild(*this);
		// Invalidate old refs
		m_generation++;
		m_transform.m_generation++;
	}

	// GO Ref Impl
	goRef_t::goRef_t(GameObject& igo):
		go(&igo), 
		generation(igo.GetGenerationID()) 
	{}

	bool goRef_t::operator == (const GameObject* other) const 
	{
		return (go && other && go == other && generation == other->GetGenerationID());
	}

	goRef_t::operator bool() 
	{
		return (go && go->GetGenerationID() == generation);
	}

	GameObject* goRef_t::operator-> ()
	{
		if (go && go->GetGenerationID() == generation)
			return go;
		else
			return nullptr;
	};
	const GameObject* goRef_t::operator-> ( ) const
	{
		if( go && go->GetGenerationID() == generation )
			return go;
		else
			return nullptr;
	};
	GameObject& goRef_t::operator* ( )
	{
		BASSERT( go && go->GetGenerationID() == generation );
		return *go;
	};
	const GameObject& goRef_t::operator* ( ) const
	{
		BASSERT( go && go->GetGenerationID() == generation );
		return *go;
	};
	//GO Transform Ref Impl
	goTransformRef_t::goTransformRef_t(GOTransform& igo) :go(&igo), generation(igo.GetGenerationID()) {}

	goTransformRef_t::operator bool() { return (go && go->GetGenerationID() == generation); }
	GOTransform* goTransformRef_t::operator-> ()
	{
		if (go && go->GetGenerationID() == generation)
			return go;
		else
			return nullptr;
	};


	const Bolts::mat4&	GOTransform::GetAbsTransform() const
	{
		if( m_isDirty )
			UpdateMats();
		return m_absTransform;
	}

	void GOTransform::UpdateMats() const
	{
		Bolts::mat4 temp = Bolts::mat4(1.f);
		temp = glm::translate(temp, m_position);
		temp = glm::scale(temp, m_scale);
		temp *= glm::toMat4(m_rotation);
		m_absTransform = temp;
		m_isDirty = false;
	}

	GOTransform operator* ( const GOTransform& first, const GOTransform& other )
	{
		GOTransform ret;
		ret.m_position = first.m_position + other.m_position;
		ret.m_rotation = first.m_rotation + other.m_rotation;
		ret.m_scale = first.m_scale; // DON'T INHERIT SCALE
		return ret;
	}

	void GOTransform::Reload(const Bolts::BinaryMapView& data)
	{
		SetDirty();
		data.Get("Position", m_position);
		data.Get("Scale", m_scale);
		data.Get("Rotation", m_rotation);
	}

	void GameObject::Reload(const Bolts::BinaryMapView& data)
	{
		m_transform.Reload(data);
	}

};