#pragma once

#include "Components/Components.h"
#include "Components/GameObject.h"
#include "Components/GOPrototype.h"
#include <bolts_core.h>

namespace Bolts {

	class ICompMgr;

	class Space
	{
	public:
		typedef vec_t< std::unique_ptr<GameObject>>	goArray_t;

		Space() = default;
		~Space();
		Space(const Space&) = delete;
		Space& operator= (const Space&) = delete;

		// Creation / Destruction
		goRef_t	AddGO(); // Deferred until next Flush
		void	RemoveGO(goRef_t ref); // Deferred until next Flush
		void	FlushPendingOperations();
		//
		goArray_t& GetActiveObjects() { return m_goRegisteredList; }
		const goArray_t& GetActiveObjects() const { return m_goRegisteredList; }
		//
		void	SetRoot(goRef_t r) { root = r; }
		goRef_t GetRoot() const { return root; }
	private:
		goArray_t	m_goToRemoveList;
		goArray_t	m_goToRegisterList;
		goArray_t	m_goRegisteredList;
		goArray_t	m_goDeletedList;
		//
		goRef_t root;
	};
};
