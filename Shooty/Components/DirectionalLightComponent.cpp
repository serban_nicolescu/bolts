#include "StdAfx.h"
#include "Components/DirectionalLightComponent.h"
#include "Components/ComponentModel.h"
#include "Components/ComponentProperties.h"

namespace Bolts {

	template<>
	propVec_t MakePropsForCompTag< DirectionalLightComponent::Tag >()
	{
		propVec_t r;
		IProp* p;
		p = MakeProp(PROP_COLOR3);
		p->name = "Light Color";
		r.emplace_back(p);
		return r;
	}

	template<>
	PropertyUpdateBindings RegisterComponentClassForSerialisation< DirectionalLightComponent::Tag >()
	{
		PropertyUpdateBindings bindings;
		bindings.BindGetter("Light Color", [](const void* object, IProp* prop) {
			auto comp = static_cast<const DirectionalLightComponent*>(object);
			if (auto valProp = PropCast<PROP_COLOR3>(prop))
				valProp->SetVal(comp->GetLightColor());
		});
		return bindings;
	}

	DirectionalLightComponent::DirectionalLightComponent() {}
	DirectionalLightComponent::~DirectionalLightComponent() {}

	void DirectionalLightComponent::Load(const Bolts::BinaryMapView& data, CMgrLight* mgr)
	{
		m_mgr = mgr;
		data.Get("Light Color", m_lightColor);
	}
	void DirectionalLightComponent::Reload(const Bolts::BinaryMapView& data, CMgrLight* mgr)
	{
		Load(data, mgr);
	}

	void DirectionalLightComponent::Update(float dt, unsigned frameCount)
	{
		using namespace Bolts;
		m_mgr->m_renderer.SetDirectionalLight( m_go->GetTransform().GetRotation() * vec3 {1.f,0.f,0.f}, m_lightColor);
	}

	void CMgrLight::RegisterComponents(GOS & gos)
	{
		gos.RegisterComponentType(Tag, GetNameHash());
		gos.GetComponentModels().SetProps(Tag, MakePropsForCompTag<Tag>());
		gos.SetPropBindings(Tag, RegisterComponentClassForSerialisation<Tag>());
	}
};
