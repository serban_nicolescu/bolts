#include "StdAfx.h"
#include "Components/GOS.h"
#include "Components/MeshComponent.h"
#include "Components/LightmapComponent.h"
#include "Components/DirectionalLightComponent.h"
#include "Assets/AssetManager.h"

namespace Bolts {

	using assets::AssetManager;

	template<typename T>
	void RegisterComponentManager(GOS& gos, AssetManager& res, Rendering::Renderer& newr)
	{
		auto cmgr = new T(res, newr);
		gos.RegisterCompMgr(cmgr);
		cmgr->RegisterComponents(gos);
	}

	void RegisterBuiltinComponentManagers(GOS& gos, AssetManager& res, Rendering::Renderer& newr)
	{
		RegisterComponentManager<CMgrMesh>(gos, res, newr);
		RegisterComponentManager<CMgrLight>(gos, res, newr);
		RegisterComponentManager<CMgrLightmap>(gos, res, newr);
	}

};