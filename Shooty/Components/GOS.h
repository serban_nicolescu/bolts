#pragma once
#include "Components/Components.h"
#include "Components/Space.h"
#include "Components/ComponentModel.h"
#include "Render/Renderer.h"
#include <bolts_helpers.h>

namespace Bolts {
	class PropertyUpdateBindings;

	class GOS
	{
	public:
		GOS();
		~GOS();

		void Init();
		void Update(float dt, unsigned frameNo);
		void Release();
		//
		void		RegisterCompMgr(ICompMgr*);
		void		RegisterComponentType(componentTag_t compTag, stringHash_t mgrName);
		ICompMgr*	FindComponentManagerForTag(componentTag_t tag);
		ICompMgr*	FindComponentManagerByName(stringHash_t mgrName);
		// Editor
		ObjectPropsMap&					GetComponentModels() { return m_componentModels; }
		ObjectPropsMap&					EditObjectProperties(goRef_t gor) { return m_objectProperties[gor]; }
		const ObjectPropsMap*			GetObjectProperties(const goRef_t gor) const { return try_find(m_objectProperties, gor); }
		void							SetPropBindings(componentTag_t, PropertyUpdateBindings&&);
		const PropertyUpdateBindings*	GetPropBindings(componentTag_t) const;
		//
		Space*	NewSpace();
		void	DestroySpace(Space*);
		void	Start(Space*);
		
		//
		goRef_t SpawnGO( Space*, const GOPrototype& );
		void	RemoveGO(Space*, goRef_t);
		void	ReloadGO(Space*, goRef_t, const GOPrototype&);
		void	AddComponent(Space*, goRef_t, componentTag_t);
		void	RemoveComponent(Space*, goRef_t, componentTag_t);

		vec_t<Space*>& GetSpaces() {return m_spaces;};
	private:
		GOPrototype& BuildTempGOPrototypeSlow(goRef_t goRef);

		vec_t< std::unique_ptr<ICompMgr>>		m_managers;
		hash_map_t< componentTag_t, ICompMgr*>	m_componentTypeMap;
		vec_t< Space*>							m_spaces;
		GOPrototype								m_rootObjPrototype;
		// Editor
		ObjectPropsMap							m_componentModels;
		hash_map_t<goRef_t, ObjectPropsMap>		m_objectProperties;
		hash_map_t<componentTag_t, PropertyUpdateBindings> m_typeBindings;
	};

	void RegisterBuiltinComponentManagers(GOS&, assets::AssetManager&, Rendering::Renderer&);
};