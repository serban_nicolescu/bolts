#pragma once

#include "Components/Components.h"
#include "Components/ComponentModel.h"

namespace Bolts {

	class GOPrototype
	{
	public:
		GOPrototype();

		void Clear() {
			m_components.clear();
			m_props.Clear();
		}

		Bolts::vec_t< componentTag_t>	m_components;
		ObjectPropsMap					m_props;
	};

};