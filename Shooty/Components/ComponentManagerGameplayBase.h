#pragma once

#include "Components/Components.h"
#include "Components/Space.h"
#include "Components/GameObject.h"
#include "Components/ComponentManagerNewBase.h"
#include "Components/GOS.h"
#include <bolts_core.h>
#include <bolts_helpers.h>
#include <bolts_binarymap.h>
#include <boost/pool/object_pool.hpp>
#include <boost/pool/poolfwd.hpp>
#include <boost/optional.hpp>
#include <boost/container/flat_map.hpp>


namespace Bolts
{
namespace detail
{
	//Specializations for the two pool types
	//TODO: Assert on failures
	template< typename T>
	struct ObjectPoolProxy
	{
		typedef boost::object_pool<T> pool_t;

		inline T* AllocComponent()
		{
			return m_ComponentPool.construct();
		}

		inline void DelComponent(T* pcmp)
		{
			return m_ComponentPool.destroy(pcmp);
		}

		pool_t m_ComponentPool;
	};

	template< typename T>
	struct PODPoolProxy
	{
		typedef ::boost::pool< ::boost::default_user_allocator_malloc_free> pool_t;

		PODPoolProxy() :m_ComponentPool(sizeof(T)) {}

		inline T* AllocComponent()
		{
			return static_cast<T*>(m_ComponentPool.malloc());
		}

		inline void DelComponent(T* pcmp)
		{
			return m_ComponentPool.free(pcmp);
		}

		pool_t m_ComponentPool;
	};

	//Template base class for component managers
	template< typename Derived, typename T>
	class CMgrGameplayBase : public ::Bolts::ICompMgr
	{
	public:
		// Template placeholders
		typedef ObjectPoolProxy<T>	poolType;
		//
		typedef T comp_t;
		typedef typedComponentRef_t<Derived,T>  ref_t;
		static const bool k_refsAreSimple = true;
		//
		static const componentTag_t Tag = comp_t::Tag;


		CMgrGameplayBase(const char* name) :ICompMgr(Tag, name) {}
		~CMgrGameplayBase() = default;
		//
		//static const Derived*	TryCast(const ICompMgr* icomp)  { return (Tag == icomp->GetTag() ? static_cast<Derived*>(icomp) : nullptr); }

		virtual void RegisterSpace(Space* newSpace) override
		{
			m_compsMap.insert(
				std::make_pair<>(newSpace, PerSpaceContainers())
			);
		}
		virtual void ClearSpace(Space* space) override
		{
			auto& perSpace = m_compsMap[space];
			for (comp_t* compP : perSpace.m_components)
				RemoveComponent(space, ref_t(compP, nullptr));
		}
		//
		virtual	void LoadComponent(Space* space, componentTag_t typeTag, const Bolts::BinaryMapView& bm) override
		{
			BASSERT(typeTag == Tag);
			BASSERT(m_compsMap.find(space) != m_compsMap.end());
			auto& comps = m_compsMap[space];
			comp_t* compP = m_compPool.AllocComponent();
			BASSERT(compP);

			compP->Load(bm, static_cast<Derived*>(this));
			comps.m_components.push_back(compP);
			comps.m_loadedComponents.push_back(compP);
		}

		virtual	void ReloadComponent(componentRef_t ref, const Bolts::BinaryMapView& bm) override
		{
			comp_t* compP = ref_t::TryGet(ref);
			BASSERT(compP);
			if (compP)
				compP->Reload(bm, static_cast<Derived*>(this));
		}

		virtual componentRef_t	PopComponent(Space* space, goRef_t go) override
		{
			BASSERT(m_compsMap.find(space) != m_compsMap.end());
			auto& comps = m_compsMap[space];
			BASSERT(!comps.m_loadedComponents.empty());

			comp_t* compP = comps.m_loadedComponents.back();
			compP->Loaded(go);
			comps.m_loadedComponents.pop_back();
			comps.m_toStartComps.push_back(compP);

			return componentRef_t( compP, this );
		}

		// LO now has all components and parent/children
		virtual void ProcessPendingStarts(Space* space) override
		{
			BASSERT(m_compsMap.find(space) != m_compsMap.end());
			auto& comps = m_compsMap[space];

			compVec_t tostart;
			std::swap(comps.m_toStartComps, tostart);

			for (comp_t* compP : tostart)
			{
				compP->PreStart();
			}
			for (comp_t* compP : tostart)
			{
				compP->Start();
			}
			for (comp_t* compP : tostart)
			{
				EnableComponent( ref_t(compP, static_cast<Derived*>(this) ));
			}
		}

		virtual void RemoveComponent(Space* space, componentRef_t cRef) override
		{
			BASSERT(m_compsMap.find(space) != m_compsMap.end());
			auto& comps = m_compsMap[space];
			comp_t* comp = ref_t::TryGet(cRef);
			BASSERT(comp);

			BASSERT(!Bolts::contains(comps.m_toRemoveComponents, comp));
			comps.m_toRemoveComponents.push_back(comp);
			DisableComponent(cRef);
		}

		virtual void ProcessPendingEnables() override
		{
			for (comp_t* compP : m_toEnableComponents)
			{
				compP->OnEnabled();
			}
			m_enabledComponents.insert(m_enabledComponents.end(), m_toEnableComponents.begin(), m_toEnableComponents.end());
			m_toEnableComponents.clear();
			//
			for (comp_t* compP : m_toDisableComponents)
			{
				compP->OnDisabled();
				bool wasRem = Bolts::try_remove(m_enabledComponents, compP);
				BASSERT(wasRem);
			}
			m_toDisableComponents.clear();
			//
			for (auto& spaceComps : m_compsMap)
			{
				auto& comps = spaceComps.second;
				for (comp_t* compP : comps.m_toRemoveComponents)
				{
					compP->OnRemoved();
					m_compPool.DelComponent(compP);
					bool wasRem = Bolts::try_remove(comps.m_components, compP);
					BASSERT(wasRem);
				}
				comps.m_toRemoveComponents.clear();
			}
		}

		// LO has been registered to a Space so comps can find other objects
		virtual void UpdateComponents(float dt, unsigned frameNo) override
		{
			for (comp_t* compP : m_enabledComponents)
			{
				compP->Update(dt, frameNo);
			}
		}
		//
		virtual void EnableComponent(componentRef_t cRef) override
		{
			comp_t* comp = ref_t::TryGet(cRef);
			BASSERT(comp);

			if (!Bolts::contains(m_enabledComponents, comp)) {
				m_toEnableComponents.push_back(comp);
				Bolts::try_remove(m_toDisableComponents, comp);
			}
		}
		virtual void DisableComponent(componentRef_t cRef) override
		{
			comp_t* comp = ref_t::TryGet(cRef);
			BASSERT(comp);

			if (Bolts::contains(m_enabledComponents, comp)) {
				m_toDisableComponents.push_back(comp);
				Bolts::try_remove(m_toEnableComponents, comp);
			}
		}

	protected:
		typedef vec_t<T*>		compVec_t;

		struct PerSpaceContainers
		{
			compVec_t		m_components; // Owns comp ptrs. If ptr is here it means that comp is still alive
			compVec_t		m_loadedComponents;
			compVec_t		m_toStartComps;
			compVec_t		m_toRemoveComponents;
		};

		poolType		m_compPool;
		boost::container::flat_map<Space*, PerSpaceContainers> m_compsMap;

		compVec_t		m_toDisableComponents;
		compVec_t		m_toEnableComponents;
		compVec_t		m_enabledComponents; // These get Update()
	};


	class CMgrTestImpl;
	class TestGameplayCompType
	{
	public:
		const static componentTag_t	Tag = Bolts::FourCC<'t', 'e', 's', 't'>::v;;

		void Load(const Bolts::BinaryMapView& data, CMgrTestImpl*) {}
		void Reload(const Bolts::BinaryMapView& data, CMgrTestImpl*) {}
		void Loaded(goRef_t go) {}

		void PreStart() {}
		void Start() {}

		void OnEnabled() {}
		void OnDisabled() {}
		void OnRemoved() {}

		void Update(float dt, unsigned frameCount);
	};

	// Test class
	class CMgrTestImpl :public CMgrGameplayBase<CMgrTestImpl, TestGameplayCompType>
	{
	public:
	};

};

};
