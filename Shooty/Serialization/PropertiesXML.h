#pragma once
#include "GUI/Properties.h"
#include <Components/GOS.h>

namespace rapidxml {
	template<class>
	class xml_document;
	template<class>
	class xml_node;
};

namespace Bolts {
	struct goRef_t;
}

namespace editor{
	typedef rapidxml::xml_document<char>	xmlDoc_t;
	typedef rapidxml::xml_node<char>		xmlElem_t;

	void SavePropToXML(xmlElem_t& elem, const IProp* ip);

	void SavePropsToXML(xmlElem_t& xmlOut, const propVec_t& props);
	bool LoadPropsFromXML(propVec_t& propsOut, const xmlElem_t& xml);

	void SaveObjectToXML(xmlElem_t& objectXML, const Bolts::GOS& GOS, const Bolts::goRef_t& goRef);
	void SaveObjectToXML(xmlDoc_t& objectXML, const Bolts::GOS& GOS, const Bolts::goRef_t& goRef);
	Bolts::goRef_t LoadObjectFromXML(Bolts::GOS& GOS, Bolts::Space* space, const xmlElem_t& xml);
	Bolts::goRef_t LoadObjectFromXML(Bolts::GOS& GOS, Bolts::Space* space, const xmlDoc_t& xml);

	void SaveSceneToXML(xmlDoc_t& xmlOut, const Bolts::GOS& GOS, const Bolts::Space* space);
	bool LoadSceneFromXML(Bolts::GOS& GOS, Bolts::Space* space, const xmlDoc_t& xml);
	
	xmlDoc_t& GetEmptyXMLDoc();

	bool SaveXMLFile(const char* filepath, const xmlDoc_t& doc);
	bool LoadXMLFile(const char* filepath, xmlDoc_t& doc);
	bool SaveXMLFile(const char* filepath, const propVec_t& props);
	bool LoadXMLFile(const char* filepath, propVec_t& doc);
};

