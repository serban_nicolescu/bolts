#include "StdAfx.h"
#include "PropertiesJSON.h"

#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>
#include <rapidjson/memorystream.h>
#include <rapidjson/writer.h>
#include <rapidjson/reader.h>
#include "rapidjson/filewritestream.h"
#include "rapidjson/filereadstream.h"

#define BOLTS_LOG_SCOPE "Properties"

void SavePropsToJSON(const char* filename, const propVec_t& props)
{
	BASSERT(filename);
	//
	FILE* fp = fopen(filename, "wb"); // non-Windows use "w"
	if (!fp){
		BOLTS_LERROR() << "Could not open file for saving properties: " << filename;
		return;
	}
	//
	rapidjson::Document d;
	d.SetObject();
	for (auto& propPtr : props)
	{
		PropToJSON(d,d, propPtr.get());
	}
	char writeBuffer[65536];
	rapidjson::FileWriteStream os(fp, writeBuffer, sizeof(writeBuffer));
	rapidjson::Writer<rapidjson::FileWriteStream> writer(os);
	d.Accept(writer);
	fclose(fp);
}

void PropToJSON(rapidjson::Document& doc, rapidjson::Value& objValue, const IProp* ip)
{
	using namespace Bolts;
	BASSERT(objValue.IsObject());
	auto& alloc = doc.GetAllocator();
	rapidjson::Value key(ip->name.c_str(), alloc);
	if (auto prop = PropCast<float>(ip)){
		rapidjson::Value val(prop->val);
		objValue.AddMember(key, val, alloc);
	}
	else if (auto prop = PropCast<int>(ip)){
		rapidjson::Value val(prop->val);
		objValue.AddMember(key, val, alloc);
	}
	else if (auto prop = PropCast<vec3>(ip)){
		rapidjson::Value val;
		val.SetArray();
		val.PushBack(prop->val[0], alloc);
		val.PushBack(prop->val[1], alloc);
		val.PushBack(prop->val[2], alloc);
		objValue.AddMember(key, val, alloc);
	}
	else if (auto prop = PropCast<str_t>(ip)){
		rapidjson::Value val(prop->val.c_str(), alloc);
		objValue.AddMember(key, val, alloc);
	}
	else{
		BASSERT_MSG(false,"Unhandled prop type when serialising JSON");
	}
}

/////////////

void LoadPropsFromJSON(const char* filename, propVec_t& propsOut)
{
	BASSERT(filename);
	using namespace Bolts;
	propsOut.clear();

	FILE* fp = fopen(filename, "rb"); // non-Windows use "r"
	if (!fp){
		BOLTS_LERROR() << "Could not open file for loading properties: " << filename;
		return;
	}
	char readBuffer[65536];
	rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));
	rapidjson::Document d;
	d.ParseStream(is);
	fclose(fp);
	//

	struct PropValueVisitor
	{
		Bolts::vec4 m_vecHolder;
		unsigned	m_currentVecIdx = unsigned(-1);
		const char* m_name = nullptr;
		IProp*		m_outProp = nullptr;
		
		bool Null() { return false; }
		bool Bool(bool b) { return false; }
		bool Int(int i) {
			if (m_currentVecIdx > 3) {
				//Not in an array. Just store the value
				m_outProp = MakeProp(TypeToPropType<int>::k_PropType);
				PropCast<int>(m_outProp)->val = i;
			}
			else {
				m_vecHolder[m_currentVecIdx] = i;
				m_currentVecIdx++;
			}
			
			return true;
		}
		bool Uint(unsigned u) { return false; }
		bool Int64(int64_t i) { return false; }
		bool Uint64(uint64_t u) { return false; }
		bool String(const char* str, rapidjson::SizeType length, bool copy) {
			m_outProp = MakeProp(TypeToPropType<str_t>::k_PropType);
			PropCast<str_t>(m_outProp)->val = str;
			return true;
		}
		//Vector
		bool StartArray() { m_currentVecIdx = 0; return true; }
		bool EndArray(rapidjson::SizeType elementCount) {
			if (m_currentVecIdx == 0) {
				BOLTS_LWARNING() << BOLTS_LOG_SCOPE << "Property JSON: Parameter ignored: " << m_name << " has empty Array value";
				return false;
			}
			switch (elementCount) {
			case 1:
				BOLTS_LWARNING() << BOLTS_LOG_SCOPE << "Material JSON: Parameter ignored: " << m_name << " has Array of 1 element";
				break;
			case 2:
				//m_ph.SetParam(nameHash, Bolts::vec2(m_vecHolder.x, m_vecHolder.y));
				BASSERT(false);
				break;
			case 3:
				m_outProp = MakeProp(TypeToPropType<vec3>::k_PropType);
				PropCast<vec3>(m_outProp)->val = Bolts::vec3(m_vecHolder.x, m_vecHolder.y, m_vecHolder.z);
				break;
			case 4:
				BASSERT(false);
				//m_ph.SetParam(nameHash, m_vecHolder);
				break;
			default:
				BOLTS_LWARNING() << BOLTS_LOG_SCOPE << "Material JSON: Parameter ignored: " << m_name << " has Array of 5+ elements";
				return false;
			}
			return true;
		}

		bool Double(double d) {
			if (m_currentVecIdx > 3) {
				//Not in an array. Just store the value
				m_outProp = MakeProp(TypeToPropType<float>::k_PropType);
				PropCast<float>(m_outProp)->val = d;
			} else {
				m_vecHolder[m_currentVecIdx] = d;
				m_currentVecIdx++;
			}
			return true;
		}

		bool StartObject() { return false; }
		bool Key(const char* , rapidjson::SizeType, bool) { return false; }
		bool EndObject(rapidjson::SizeType memberCount) { return false; }
	};


	for (rapidjson::Value::ConstMemberIterator itr = d.MemberBegin();
		itr != d.MemberEnd(); ++itr)
	{
		PropValueVisitor visitor;
		visitor.m_name = itr->name.GetString();
		itr->value.Accept(visitor);
		if (auto outProp = visitor.m_outProp){
			outProp->name = itr->name.GetString();
			propsOut.emplace_back(outProp);
		}
	}
	//
}