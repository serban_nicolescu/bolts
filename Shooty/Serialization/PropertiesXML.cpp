#include "StdAfx.h"
#include "PropertiesXML.h"
#include "IO/FileIO.h"
#include <rapidxml.hpp>
#include <rapidxml_print.hpp>
#include <algorithm>

#define BOLTS_LOG_SCOPE "Properties"

using namespace Bolts;
using namespace rapidxml;

namespace editor
{
	struct XMLSerialize : public IConstPropVisitor {

		char tempBuff[512];

		xmlDoc_t& doc;
		xmlElem_t* propNode;

		XMLSerialize(xmlElem_t& elem, const IProp* ip): doc( *elem.document())
		{
			propNode = doc.allocate_node(node_element, "prop", nullptr);
			auto propNameAtt = doc.allocate_attribute("name", doc.allocate_string(ip->name.c_str()));
			propNode->append_attribute(propNameAtt);
			propNode->append_attribute(doc.allocate_attribute("type", GetPropTypeName(ip->type)));

			VisitProp(ip, this);

			elem.append_node(propNode);
		}

		virtual void Visit(const PropFloat* prop) override {
			sprintf(tempBuff, "%.6f.", prop->val);
			auto valNode = doc.allocate_node(node_data, nullptr, doc.allocate_string(tempBuff));
			propNode->append_node(valNode);
		}
		virtual void Visit(const PropInt* prop) override {
			sprintf(tempBuff, "%d", prop->val);
			auto valNode = doc.allocate_node(node_data, nullptr, doc.allocate_string(tempBuff));
			propNode->append_node(valNode);
		}
		virtual void Visit(const PropIVec2* prop) override {
			sprintf(tempBuff, "%d;%d", prop->val.x, prop->val.y);
			auto valNode = doc.allocate_node(node_data, nullptr, doc.allocate_string(tempBuff));
			propNode->append_node(valNode);
		}
		virtual void Visit(const PropUVec2* prop) override {
			sprintf(tempBuff, "%u;%u", prop->val.x, prop->val.y);
			auto valNode = doc.allocate_node(node_data, nullptr, doc.allocate_string(tempBuff));
			propNode->append_node(valNode);
		}
		virtual void Visit(const PropBool* prop) override {
			auto valNode = doc.allocate_node(node_data, nullptr, prop->val ? "true" : "false");
			propNode->append_node(valNode);
		}
		virtual void Visit(const PropVec3* prop) override {
			sprintf(tempBuff, "%.6f;%.6f;%.6f", prop->val.x, prop->val.y, prop->val.z);
			auto valNode = doc.allocate_node(node_data, nullptr, doc.allocate_string(tempBuff));
			propNode->append_node(valNode);
		}
		virtual void Visit(const PropColor3* prop) override {
			sprintf(tempBuff, "%.6f;%.6f;%.6f", prop->val.x, prop->val.y, prop->val.z);
			auto valNode = doc.allocate_node(node_data, nullptr, doc.allocate_string(tempBuff));
			propNode->append_node(valNode);
		}
		virtual void Visit(const PropColor4* prop) override {
			sprintf(tempBuff, "%.6f;%.6f;%.6f;%.6f", prop->val.x, prop->val.y, prop->val.z, prop->val.w);
			auto valNode = doc.allocate_node(node_data, nullptr, doc.allocate_string(tempBuff));
			propNode->append_node(valNode);
		}
		virtual void Visit(const PropQuat* prop) override {
			sprintf(tempBuff, "%.6f;%.6f;%.6f;%.6f", prop->val.x, prop->val.y, prop->val.z, prop->val.w);
			auto valNode = doc.allocate_node(node_data, nullptr, doc.allocate_string(tempBuff));
			propNode->append_node(valNode);
		}
		virtual void Visit(const PropString* prop) override {
			sprintf(tempBuff, "%s", prop->val.c_str());
			auto valNode = doc.allocate_node(node_data, nullptr, doc.allocate_string(tempBuff));
			propNode->append_node(valNode);
		}
		virtual void Visit(const PropAsset* prop) override {
			sprintf(tempBuff, "%s", prop->assetID.s ? prop->assetID.s : "???");
			auto valNode = doc.allocate_node(node_data, nullptr, doc.allocate_string(tempBuff));
			sprintf(tempBuff, "%u", prop->assetID.h);
			propNode->append_attribute(doc.allocate_attribute("assetID", doc.allocate_string(tempBuff)));
			sprintf(tempBuff, "%u", prop->assetType);
			propNode->append_attribute(doc.allocate_attribute("assetType", doc.allocate_string(tempBuff)));
			propNode->append_node(valNode);
		}

		// Arrays

		void WriteArrayHeader(int size) {
			sprintf(tempBuff, "%d", size);
			propNode->append_attribute(doc.allocate_attribute("size", doc.allocate_string(tempBuff)));
		}

		virtual void Visit(const PropFloatV* prop) override {}
		virtual void Visit(const PropIntV* prop) override {}
		virtual void Visit(const PropIVec2V* prop) override {}
		virtual void Visit(const PropUVec2V* prop) override {}
		//virtual void Visit(const PropBoolV* prop) override {}
		virtual void Visit(const PropVec3V* prop) override {}
		virtual void Visit(const PropColor3V* prop) override {}
		virtual void Visit(const PropColor4V* prop) override {}
		virtual void Visit(const PropQuatV* prop) override {}
		//virtual void Visit(const PropStringV* prop) override {}
		virtual void Visit(const PropAssetV* prop) override {
			auto& values = prop->GetVal();
			sprintf(tempBuff, "%u", prop->assetType);
			propNode->append_attribute(doc.allocate_attribute("assetType", doc.allocate_string(tempBuff)));
			WriteArrayHeader(values.size());
			for (auto& val : values) {
				auto elemNode = doc.allocate_node(node_element, "v", nullptr);
				//
				sprintf(tempBuff, "%s", val.s ? val.s : "???");
				auto valNode = doc.allocate_node(node_data, nullptr, doc.allocate_string(tempBuff));
				sprintf(tempBuff, "%u", val.h);
				elemNode->append_attribute(doc.allocate_attribute("assetID", doc.allocate_string(tempBuff)));
				elemNode->append_node(valNode);
				//
				propNode->append_node(elemNode);
			}
		}
	};


	void SavePropToXML(xmlElem_t& elem, const IProp* ip)
	{
		XMLSerialize(elem, ip);
	}

	struct XMLDeserialize : public IPropVisitor {

		char tempBuff[512];

		const xmlElem_t& propNode;
		bool wasSuccesful = false;

		XMLDeserialize(const xmlElem_t& elem, IProp* iprop) : propNode(elem)
		{
			tempBuff[0] = '\0';
			VisitProp(iprop, this);
		}

		virtual void Visit(PropFloat* prop) override {
			sscanf(propNode.value(), "%f", &(prop->val));
		}
		virtual void Visit(PropInt* prop) override {
			sscanf(propNode.value(), "%d", &(prop->val));
		}
		virtual void Visit(PropIVec2* prop) override {
			sscanf(propNode.value(), "%d;%d", &(prop->val.x), &(prop->val.y));
		}
		virtual void Visit(PropUVec2* prop) override {
			sscanf(propNode.value(), "%u;%u", &(prop->val.x), &(prop->val.y));
		}
		virtual void Visit(PropBool* prop) override {
			sscanf(propNode.value(), "%s", tempBuff);
			prop->val = (strcmp(tempBuff, "true") == 0);
		}
		virtual void Visit(PropVec3* prop) override {
			sscanf(propNode.value(), "%f;%f;%f", &(prop->val.x), &(prop->val.y), &(prop->val.z));
		}
		virtual void Visit(PropColor3* prop) override {
			sscanf(propNode.value(), "%f;%f;%f", &(prop->val.x), &(prop->val.y), &(prop->val.z));
		}
		virtual void Visit(PropColor4* prop) override {
			sscanf(propNode.value(), "%f;%f;%f;%f", &(prop->val.x), &(prop->val.y), &(prop->val.z), &(prop->val.w));

		}
		virtual void Visit(PropQuat* prop) override {
			sscanf(propNode.value(), "%f;%f;%f;%f", &(prop->val.x), &(prop->val.y), &(prop->val.z), &(prop->val.w));
		}
		virtual void Visit(PropString* prop) override {
			sscanf(propNode.value(), "%s", tempBuff);
			prop->SetVal(tempBuff);
		}
		virtual void Visit(PropAsset* prop) override {
			sscanf(propNode.value(), "%s", tempBuff);
			prop->SetVal(tempBuff);
			if (auto typeAtt = propNode.first_attribute("assetType")) {
				sscanf(typeAtt->value(), "%u", &prop->assetType);
			}
			if (auto idAtt = propNode.first_attribute("assetID")) {
				Bolts::hash32_t aID;
				sscanf(idAtt->value(), "%u", &aID);
				prop->SetVal(aID);
			}
		}

		// Arrays

		int ReadArraySize() {
			auto sizeAttr = propNode.first_attribute("size");
			int s = 0;
			sscanf(sizeAttr->value(), "%d", &s);
			return s;
		}

		//virtual void Visit(PropBoolV* prop) override {}
		//virtual void Visit(const PropStringV* prop) override {}
		virtual void Visit(PropFloatV* prop) override { wasSuccesful = false; }
		virtual void Visit(PropIntV* prop) override { wasSuccesful = false; }
		virtual void Visit(PropIVec2V* prop) override { wasSuccesful = false; }
		virtual void Visit(PropUVec2V* prop) override { wasSuccesful = false; }
		virtual void Visit(PropVec3V* prop) override { wasSuccesful = false; }
		virtual void Visit(PropColor3V* prop) override { wasSuccesful = false; }
		virtual void Visit(PropColor4V* prop) override { wasSuccesful = false; }
		virtual void Visit(PropQuatV* prop) override { wasSuccesful = false; }
		virtual void Visit(PropAssetV* prop) override {
			auto& values = prop->EditVal();
			values.resize(ReadArraySize());
			if (auto typeAtt = propNode.first_attribute("assetType")) {
				sscanf(typeAtt->value(), "%u", &prop->assetType);
			}
			auto valNode = propNode.first_node();
			int i = 0;
			while (valNode) {
				auto& currentVal = values[i];
				//
				sscanf(valNode->value(), "%s", tempBuff);
				const char* strVal = tempBuff;//NOTE: Important that we erase char array size
				currentVal = regHash_t(strVal); 
				if (auto idAtt = valNode->first_attribute("assetID")) {
					Bolts::hash32_t aID;
					sscanf(idAtt->value(), "%u", &aID);
					currentVal = aID;
				}
				//
				valNode = valNode->next_sibling();
				i++;
			}
		}
	};

	IProp* LoadPropFromXML(const xmlElem_t& propNode)
	{
		auto nameAtt = propNode.first_attribute("name");
		auto typeAtt = propNode.first_attribute("type");
		if (!nameAtt) {
			BOLTS_LERROR() << "Couldn't find name field for property ";
			return nullptr;
		}
		if (!typeAtt) {
			BOLTS_LERROR() << "Couldn't find type field for property " << nameAtt->name();
			return nullptr;
		}
		//
		const Bolts::hash32_t propType = HashString(typeAtt->value(), typeAtt->value_size());
		IProp* iprop = MakeProp(propType);
		iprop->name = nameAtt->value();
		BASSERT(iprop);

		XMLDeserialize d(propNode, iprop);

		return iprop;
	}

	///////////////

	void SavePropsToXML(xmlElem_t& xmlOut, const propVec_t& props)
	{
		xmlDoc_t& d = *xmlOut.document();
		for (auto& propPtr : props)
		{
			SavePropToXML( xmlOut, propPtr.get());
		}
	}

	bool LoadPropsFromXML(propVec_t& propsOut, const xmlElem_t& xml)
	{
		for (auto propNode = xml.first_node(); propNode; propNode = propNode->next_sibling()) {
			if (auto iprop = LoadPropFromXML(*propNode)) {
				propsOut.emplace_back(iprop);
			}
		}
		return true;
	}

	void SaveObjectToXML(xmlElem_t& objectXML, const GOS& GOS, const goRef_t& goRef)
	{
		using namespace Bolts;
		using namespace rapidxml;
		xmlDoc_t& d = *objectXML.document();
		//
		auto objProps = GOS.GetObjectProperties(goRef);
		BASSERT(objProps);
		auto componentVector = objProps->GetTags();
		std::sort(componentVector.begin(), componentVector.end());
		// Save components
		for (auto tag : componentVector) {
			rapidxml::xml_node<>* compNode = nullptr;
			if (tag == GetObjectTag())
				compNode = d.allocate_node(rapidxml::node_element, "transform", nullptr);
			else {
				compNode = d.allocate_node(rapidxml::node_element, "comp", nullptr);
				char fourCC[5] = {};
				memcpy(fourCC, &tag, 4);
				compNode->append_attribute(d.allocate_attribute("type", d.allocate_string(fourCC)));
			}
			objectXML.append_node(compNode);
			SavePropsToXML(*compNode, *(objProps->GetProps(tag)));
		}

		// Save child objects
		for (auto childRef : goRef->GetChildren())
		{
			auto objNode = d.allocate_node(rapidxml::node_element, "object", nullptr);
			objectXML.append_node(objNode);
			SaveObjectToXML(*objNode, GOS, childRef);
		}
	}

	void SaveObjectToXML(xmlDoc_t& xmlOut, const GOS& GOS, const goRef_t& goRef)
	{
		auto objNode = xmlOut.allocate_node(rapidxml::node_element, "object", nullptr);
		xmlOut.append_node(objNode);
		SaveObjectToXML(*objNode, GOS, goRef);
	}

	goRef_t LoadObjectFromXML(GOS& GOS, Space* space, const xmlElem_t& objectXML)
	{
		if (strcmp("object", objectXML.name()) != 0) {
			BASSERT_MSG(false, "First GameObject node is not <object>");
			return {};
		}
		//TODO: Remove small allocations. the prop map is probably super-slow
		static GOPrototype g_proto; 
		g_proto.Clear();
		// populate prototype
		for (auto compNode = objectXML.first_node(); compNode; compNode = compNode->next_sibling()) {
			if (strcmp("object", compNode->name()) == 0)
				continue;
			componentTag_t cTag = 0;
			if (strcmp("transform", compNode->name()) == 0) {
				cTag = GetObjectTag();
			}
			else {
				auto attr = compNode->first_attribute("type");
				const char* fourCC = attr->value();
				if (attr->value_size() != 4) {
					BASSERT_MSG(false, "Component node has invalid type attribute");
					continue;
				}
				memcpy(&cTag, fourCC, 4);
			}
			propVec_t propVec;
			LoadPropsFromXML(propVec, *compNode);
			g_proto.m_props.SetProps(cTag, std::move(propVec));
		}
		g_proto.m_components = g_proto.m_props.GetTags();

		// spawn object
		auto goRef = GOS.SpawnGO(space, g_proto);

		// Load child objects
		for (auto objNode = objectXML.first_node("object"); objNode; objNode = objNode->next_sibling("object")) {
			goRef_t childRef = LoadObjectFromXML(GOS, space, *objNode);
			BASSERT(childRef);
			goRef->AddChild(childRef);
		}

		return goRef;
	}

	goRef_t LoadObjectFromXML(GOS& GOS, Space* space, const xmlDoc_t& xmlDoc)
	{
		auto rootObj = xmlDoc.first_node();
		return LoadObjectFromXML(GOS, space, *rootObj);
	}

	void SaveSceneToXML(xmlDoc_t& xmlOut, const GOS& GOS, const Space* space)
	{
		//TODO: root object needs to be treated specially and also saved
		using namespace Bolts;
		using namespace rapidxml;
		BASSERT(space);
		auto rootNode = xmlOut.allocate_node(rapidxml::node_element, "scene", nullptr);
		xmlOut.append_node(rootNode);
		// save only root children, since the root is special and never gets reloaded
		auto& rootChildren = space->GetRoot()->GetChildren();
		for (goRef_t child : rootChildren) {
			auto objNode = xmlOut.allocate_node(rapidxml::node_element, "object", nullptr);
			rootNode->append_node(objNode);
			SaveObjectToXML(*objNode, GOS, child);
		}
	}

	bool LoadSceneFromXML(GOS& GOS, Space* space, const xmlDoc_t& xml)
	{
		bool OK = true;
		auto rootNode = xml.first_node();
		for (auto objNode = rootNode->first_node(); objNode; objNode = objNode->next_sibling()) {
			OK &= LoadObjectFromXML(GOS, space, *objNode);
		}
		return OK;
	}

	xmlDoc_t& GetEmptyXMLDoc()
	{
		static xmlDoc_t g_doc;
		g_doc.remove_all_nodes();
		g_doc.remove_all_attributes();
		return g_doc;
	}


	bool SaveXMLFile(const char* filepath, const xmlDoc_t& doc)
	{
		FILE* fp = fopen(filepath, "wb"); // non-Windows use "w"
		if (!fp) {
			BOLTS_LERROR() << "Could not open file for saving: " << filepath;
			return false;
		}

		std::string s;
		rapidxml::print(std::back_inserter(s), doc, 0);
		fwrite(s.c_str(), s.length(), 1, fp);
		return fclose(fp) == 0; // 0 means OK
	}

	static std::unique_ptr<char[]> g_xmlText; // Keep mem around until next read

	bool LoadXMLFile(const char* filepath, xmlDoc_t& doc)
	{
		BASSERT(filepath);
		using namespace Bolts;
		size_t fsize = Bolts::IO::fileSize(filepath);
		if (fsize == 0) {
			BOLTS_LERROR() << "Could not open file for loading properties: " << filepath;
			return false;
		}
		g_xmlText.reset(new char[fsize + 1]);
		Bolts::IO::readFile(filepath, g_xmlText.get(), fsize + 1, Bolts::IO::EF_TEXT);
		g_xmlText[fsize] = '\0';
		doc.parse<0>(g_xmlText.get());
		return true;
	}

	bool SaveXMLFile(const char* filepath, const propVec_t& props)
	{
		xmlDoc_t& doc = GetEmptyXMLDoc();
		auto propsElem = doc.allocate_node(rapidxml::node_element, "props", nullptr);
		doc.append_node(propsElem);
		SavePropsToXML(*propsElem, props);
		return SaveXMLFile(filepath, doc);
	}

	bool LoadXMLFile(const char* filepath, propVec_t& props)
	{
		xmlDoc_t& doc = GetEmptyXMLDoc();
		if (!LoadXMLFile(filepath, doc))
			return false;

		auto propsElem = doc.first_node();
		if (!propsElem)
			return false;

		return LoadPropsFromXML(props, *propsElem);
	}
}