#pragma once

//#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>
#include "GUI/Properties.h"
//#include <rapidjson/memorystream.h>
//#include <rapidjson/encodedstream.h>

void PropToJSON( rapidjson::Document& doc, rapidjson::Value& objValue, const IProp* prop);

void SavePropsToJSON(const char* filename,const propVec_t&);
void LoadPropsFromJSON(const char* filename, propVec_t& out);



